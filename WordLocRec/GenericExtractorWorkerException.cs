﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WordLocRec
{
    class GenericExtractorWorkerException : Exception
    {
        public GenericExtractorWorkerException() : base() { }
        public GenericExtractorWorkerException(String message) : base(message) { }
        public GenericExtractorWorkerException(String message, Exception inner) : base(message, inner) { }
    }
}
