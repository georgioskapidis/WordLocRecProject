﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Emgu.CV.ML;
using Emgu.CV;
using System.Runtime.InteropServices;
using Emgu.CV.CvEnum;
using Emgu.CV.Structure;
using Emgu.CV.ML.MlEnum;
using System.Diagnostics;

namespace WordLocRec
{
    class MyERClassifierNM1 : MyERFilter.Callback
    {
        private Boost boost;
        //private Stopwatch evalWatch = new Stopwatch(), predictWatch = new Stopwatch();
        public MyERClassifierNM1(String filename)
        {
            boost = new Boost();
            FileStorage fs = new FileStorage(filename,FileStorage.Mode.Read);
            FileNode fn = fs.GetFirstTopLevelNode();

            boost.Read(fn);
            //FileStorage fs2 = new FileStorage("C://empty.xml", FileStorage.Mode.Write);
            //boost.Write(fs2);
            if (boost == null)
            {
                throw new Exception("Exception at reading the classifier (1) at file: " + filename);
            }
        }
        public override double Eval(ERStat stat)
        {
            //evalWatch.Start();
            Mat sample = new Mat(1, 4, DepthType.Cv32F, 1);
            float[] data = new float[4];
            GCHandle handle = GCHandle.Alloc(data, GCHandleType.Pinned);
            Mat m2 = new Mat(sample.Size, DepthType.Cv32F, 1, handle.AddrOfPinnedObject(), sample.Width);
            data[0] = (float)(stat.rect.Width) / (stat.rect.Height);
            data[1] = (float)Math.Sqrt((float)(stat.area)) / stat.perimeter;
            data[2] = (float)(1 - stat.euler);
            data[3] = stat.medCrossings;
            //evalWatch.Stop();

            //evalWatch.Start();
            //float[] data = new float[4];
            //data[0] = (float)(stat.rect.Width) / (stat.rect.Height);
            //data[1] = (float)Math.Sqrt((float)(stat.area)) / stat.perimeter;
            //data[2] = (float)(1 - stat.euler);
            //data[3] = stat.medCrossings;
            //Matrix<float> m2 = new Matrix<float>(data);
            //evalWatch.Stop();

            //predictWatch.Start();
            float votes = boost.Predict(m2,null,256|1);
            //predictWatch.Stop();

            //float votes = boost.Predict(m2);
            handle.Free();
            return (double)1 - (double)1 / (1 + Math.Exp(-2 * votes));
        }
        public override void PrintWatch()
        {
            //Debug.WriteLine("Eval watch: " + evalWatch.Elapsed);
            //Debug.WriteLine("Predict watch: " + predictWatch.Elapsed);
            //Debug.WriteLine("");
        }
    }
}
