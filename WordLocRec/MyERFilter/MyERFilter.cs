﻿using Emgu.CV;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WordLocRec
{
    public abstract class MyERFilter
    {
        public static void deleteERStatTree(ref ERStat root)
        {
            Queue<ERStat> toDelete = new Queue<ERStat>();
            toDelete.Enqueue(root);
            while (toDelete.Count > 0)
            {
                // TODO isws na mporw na kanw auto eksarxhs
                //ERStat n = toDelete.Dequeue();
                ERStat n = toDelete.Peek();
                ERStat c = n.child;

                if (c != null)
                {
                    toDelete.Enqueue(c);
                    ERStat sibling = c.next;
                    while (sibling != null)
                    {
                        toDelete.Enqueue(sibling);
                        sibling = sibling.next;
                    }
                }
                toDelete.Dequeue();
            }
        }

        public abstract class Callback
        {
            public abstract double Eval(ERStat stat);
            public abstract void PrintWatch();
        }

        public abstract void Run(InputArray image, ref List<ERStat> regions);

        public abstract void setCallback(Callback cb);
        public abstract void setThresholdDelta(int thresholdDelta);
        public abstract void setMinArea(float minArea);
        public abstract void setMaxArea(float maxArea);
        public abstract void setMinProbability(float minProbability);
        public abstract void setMinProbabilityDiff(float minProbabilityDiff);
        public abstract void setNonMaxSuppression(bool nonMaxSuppression);
        public abstract int getNumRejected();
    }
}
