﻿using Emgu.CV;
using Emgu.CV.CvEnum;
using Emgu.CV.ML;
using Emgu.CV.Structure;
using Emgu.CV.Util;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WordLocRec.DataStructs;
using WordLocRec.SVMStuff;
using WordLocRec.Tools;

namespace WordLocRec
{
    class CERFilter:MyERFilter
    {
        bool oppositeTree = false; bool ceCersAllowed = false;
        bool ndAllowed = true, cerAllowed = true; // svmFilterAllowed = false, nmSvmFilterAllowed = false;
        
        public override void Run(InputArray image, ref List<ERStat> _regions)
        {
            filterTimes = 0;
            if (image.GetMat().Depth != DepthType.Cv8U)
            {
                _regions = null;
                return;
            }
            if (oppositeTree)
            {
                CvInvoke.BitwiseNot(image.GetMat(), image.GetMat());
            }
            regions = _regions;
            regionMask = new Mat(image.GetMat().Rows + 2, image.GetMat().Cols + 2, DepthType.Cv8U, 1);

            if (regions.Count == 0)
            {
                erTreeExtract(image);

                if (oppositeTree)
                {
                    CvInvoke.BitwiseNot(image.GetMat(), image.GetMat());
                }

                Debug.WriteLine("ERs count after erTreeExtract: " + regions.Count);
                Debug.WriteLine("");
                if (cerAllowed)
                {
                    List<ERStat> temp2 = new List<ERStat>(regions);
                    regions = new List<ERStat>(temp2.Count);
                    erNonCerSuppression(temp2[0], null, null);

                    Debug.WriteLine("erNonCerSuppression called: " + filterTimes + " times");
                    Debug.WriteLine("ERs count after cer suppression:" + regions.Count);
                    filterTimes = 0;
                }              
                if (ndAllowed)
                {
                    int prevRegCountND = -1;
                    int counterNd = 0;
                    while (regions.Count != prevRegCountND)
                    {
                        counterNd++;
                        prevRegCountND = regions.Count;

                        List<ERStat> tempND = new List<ERStat>(regions);
                        regions = new List<ERStat>(tempND.Count);
                        erNDSearch(tempND[0], null, null);

                        tempND = new List<ERStat>(regions);
                        regions = new List<ERStat>(tempND.Count);
                        erDeleteNearDuplicate(tempND[0], null, null);
                        Debug.WriteLine("Near Duplicate suppression executed totally " + counterNd + " times");
                        Debug.WriteLine("erDeleteNearDuplicate called: " + filterTimes + " times");
                        Debug.WriteLine("ERs count after nd suppression: " + regions.Count);
                        filterTimes = 0;
                    }
                }
                
                if (nonMaxSuppression)
                {
                    erEvalTree(regions[0]);
                    Debug.WriteLine("erEvalTreeCalled: " + filterTimes + " times");
                    filterTimes = 0;

                    List<ERStat> tempNMS = new List<ERStat>(regions);
                    regions = new List<ERStat>(tempNMS.Count);
                    erSaveNMSOnly(tempNMS[0], null, null);

                    List<ERStat> temp = new List<ERStat>(regions);
                    regions = new List<ERStat>(temp.Count);
                    erTreeNonmaxSuppression(temp[0], null, null);

                    Debug.WriteLine("erTreeNonmaxSuppression called: " + filterTimes + " times");
                    Debug.WriteLine("ERs after NMS: " + regions.Count);
                    filterTimes = 0;
                }
            }
            else
            {
                if (regions[0].parent != null) return;

                List<ERStat> temp = new List<ERStat>(regions);
                regions = new List<ERStat>(temp.Count);
                erTreeFilter(image, temp[0], null, null);

                Debug.WriteLine("erTreeFilter called: " + filterTimes + " times");
                Debug.WriteLine("ERs after SVM filter: " + regions.Count);
            }
            _regions = regions;
            GC.Collect();
        }

        private void erTreeExtract(InputArray image)
        {
            Mat src = image.GetMat();
            if (src.Depth != DepthType.Cv8U)
            {
                return;
            }
            if (thresholdDelta > 1)
            {
                Image<Gray, byte> tempImage = src.ToImage<Gray, Byte>();
                tempImage = (tempImage / thresholdDelta) - 1;
                src = tempImage.GetInputArray().GetMat();
            }
            Byte[] imageData = src.GetData();
            int width = src.Cols, height = src.Rows;
            mWidth = width;
            mHeight = height;

            List<ERStat> erStack = new List<ERStat>();

            byte[,] quads = new byte[3, 4];

            quads[0, 0] = 1 << 3;
            quads[0, 1] = 1 << 2;
            quads[0, 2] = 1 << 1;
            quads[0, 3] = 1;
            quads[1, 0] = 1 << 2 | 1 << 1 | 1;
            quads[1, 1] = 1 << 3 | 1 << 1 | 1;
            quads[1, 2] = 1 << 3 | 1 << 2 | 1;
            quads[1, 3] = 1 << 3 | 1 << 2 | 1 << 1;
            quads[2, 0] = 1 << 2 | 1 << 1;
            quads[2, 1] = 1 << 3 | 1;

            bool[] accessiblePixelMask = new bool[width * height];
            bool[] accumulatedPixelMask = new bool[width * height];

            List<int>[] boundaryPixels = new List<int>[256];
            List<int>[] boundaryEdges = new List<int>[256];
            for (int i = 0; i < boundaryPixels.Length; i++)
            {
                boundaryPixels[i] = new List<int>();
                boundaryEdges[i] = new List<int>();
            }

            //add an empty ERStat as the root of the ERTree
            erStack.Add(new ERStat());

            int thresholdLevel = (255 / thresholdDelta) + 1;

            int currentPixel = 0;
            int currentEdge = 0;
            int currentLevel = imageData[0];
            accessiblePixelMask[0] = true;

            bool pushNewComponent = true;
            //initialization ends here

            for (; ; )
            {
                int x = currentPixel % width;
                int y = currentPixel / width;
                //Debug.WriteLine("Current pixel: " + currentPixel);
                //Debug.WriteLine("(y, x) = (" + y + ", " + x + ")");

                if (pushNewComponent)
                {
                    erStack.Add(new ERStat(currentLevel, currentPixel, x, y));
                }
                    
                pushNewComponent = false;

                #region Checking neighbour pixels for accessibility; if they've been checked before
                for (; currentEdge < 4; currentEdge++) // den exei arxh to current edge edw giati pairnei apo to boundary edges
                {
                    int neighbourPixel = currentPixel;

                    switch (currentEdge)
                    {
                        case 0: if (x - width + 1 < 0) neighbourPixel = currentPixel + 1; break; // deksi pixel
                        case 1: if (y - height + 1 < 0) neighbourPixel = currentPixel + width; break; // katw pixel
                        case 2: if (x > 0) neighbourPixel = currentPixel - 1; break; // aristero pixel
                        default: if (y > 0) neighbourPixel = currentPixel - width; break; // panw pixel
                    }
                    if (neighbourPixel != currentPixel && !accessiblePixelMask[neighbourPixel])
                    {
                        int neighbourLevel = imageData[neighbourPixel];
                        accessiblePixelMask[neighbourPixel] = true;

                        if (neighbourLevel >= currentLevel)
                        {
                            boundaryPixels[neighbourLevel].Add(neighbourPixel);
                            boundaryEdges[neighbourLevel].Add(0);

                            if (neighbourLevel < thresholdLevel)
                                thresholdLevel = neighbourLevel;
                        }
                        else
                        {
                            boundaryPixels[currentLevel].Add(currentPixel);
                            boundaryEdges[currentLevel].Add(currentEdge + 1);

                            if (currentLevel < thresholdLevel)
                                thresholdLevel = currentLevel;

                            currentPixel = neighbourPixel;
                            currentEdge = 0;
                            currentLevel = neighbourLevel;

                            pushNewComponent = true;
                            break;
                        }
                    }
                }
                #endregion

                if (pushNewComponent) continue;

                #region incrementaly computable descriptors

                int nonBoundaryNeighbours = 0;
                int nonBoundaryNeighboursHoriz = 0;

                byte[] quadBefore = new Byte[] { 0, 0, 0, 0 };
                byte[] quadAfter = new Byte[] { 0, 0, 0, 0 };
                quadAfter[0] = 1 << 1;
                quadAfter[1] = 1 << 3;
                quadAfter[2] = 1 << 2;
                quadAfter[3] = 1;

                for (int edge = 0; edge < 8; edge++)
                {
                    int neighbour4 = -1; // von Neumann neighborhood
                    int neighbour8 = -1; // Moore neighborhood
                    int cell = 0;
                    switch (edge)
                    {   //all the ifs check if the pixel belongs in the matrix boundary
                        case 0: if (x < width - 1) { neighbour4 = neighbour8 = currentPixel + 1; } cell = 5; break;
                        case 1: if ((x < width - 1) && (y < height - 1)) { neighbour8 = currentPixel + 1 + width; } cell = 8; break;
                        case 2: if (y < height - 1) { neighbour4 = neighbour8 = currentPixel + width; } cell = 7; break;
                        case 3: if ((x > 0) && (y < height - 1)) { neighbour8 = currentPixel - 1 + width; } cell = 6; break;
                        case 4: if (x > 0) { neighbour4 = neighbour8 = currentPixel - 1; } cell = 3; break;
                        case 5: if ((x > 0) && (y > 0)) { neighbour8 = currentPixel - 1 - width; } cell = 0; break;
                        case 6: if (y > 0) { neighbour4 = neighbour8 = currentPixel - width; } cell = 1; break;
                        default: if ((y > 0) && (x < width - 1)) { neighbour8 = currentPixel + 1 - width; } cell = 2; break;
                    }
                    if ((neighbour4 != -1) && (accumulatedPixelMask[neighbour4]) && (imageData[neighbour4] <= imageData[currentPixel]))
                    {
                        nonBoundaryNeighbours++;
                        if ((edge == 0) || (edge == 4))
                            nonBoundaryNeighboursHoriz++;
                    }

                    int pixValue = imageData[currentPixel] + 1;
                    if (neighbour8 != -1)
                        if (accumulatedPixelMask[neighbour8])
                            pixValue = imageData[neighbour8];

                    if (pixValue <= imageData[currentPixel])
                    {
                        switch (cell)
                        {
                            case 0:
                                quadBefore[3] = (byte)(quadBefore[3] | 1 << 3);
                                quadAfter[3] = (byte)(quadAfter[3] | 1 << 3);
                                break;
                            case 1:
                                quadBefore[3] = (byte)(quadBefore[3] | 1 << 2);
                                quadAfter[3] = (byte)(quadAfter[3] | 1 << 2);
                                quadBefore[0] = (byte)(quadBefore[0] | 1 << 3);
                                quadAfter[0] = (byte)(quadAfter[0] | 1 << 3);
                                break;
                            case 2:
                                quadBefore[0] = (byte)(quadBefore[0] | 1 << 2);
                                quadAfter[0] = (byte)(quadAfter[0] | 1 << 2);
                                break;
                            case 3:
                                quadBefore[3] = (byte)(quadBefore[3] | 1 << 1);
                                quadAfter[3] = (byte)(quadAfter[3] | 1 << 1);
                                quadBefore[2] = (byte)(quadBefore[2] | 1 << 3);
                                quadAfter[2] = (byte)(quadAfter[2] | 1 << 3);
                                break;
                            case 5:
                                quadBefore[0] = (byte)(quadBefore[0] | 1);
                                quadAfter[0] = (byte)(quadAfter[0] | 1);
                                quadBefore[1] = (byte)(quadBefore[1] | 1 << 2);
                                quadAfter[1] = (byte)(quadAfter[1] | 1 << 2);
                                break;
                            case 6:
                                quadBefore[2] = (byte)(quadBefore[2] | 1 << 1);
                                quadAfter[2] = (byte)(quadAfter[2] | 1 << 1);
                                break;
                            case 7:
                                quadBefore[2] = (byte)(quadBefore[2] | 1);
                                quadAfter[2] = (byte)(quadAfter[2] | 1);
                                quadBefore[1] = (byte)(quadBefore[1] | 1 << 1);
                                quadAfter[1] = (byte)(quadAfter[1] | 1 << 1);
                                break;
                            default:
                                quadBefore[1] = (byte)(quadBefore[1] | 1);
                                quadAfter[1] = (byte)(quadAfter[1] | 1);
                                break;
                        }
                    }

                } // endfor edge

                int[] cBefore = new int[] { 0, 0, 0 };
                int[] cAfter = new int[] { 0, 0, 0 };

                for (int p = 0; p < 3; p++)
                {
                    for (int q = 0; q < 4; q++)
                    {
                        if ((quadBefore[0] == quads[p, q]) && ((p < 2) || (q < 2)))
                            cBefore[p]++;
                        if ((quadBefore[1] == quads[p, q]) && ((p < 2) || (q < 2)))
                            cBefore[p]++;
                        if ((quadBefore[2] == quads[p, q]) && ((p < 2) || (q < 2)))
                            cBefore[p]++;
                        if ((quadBefore[3] == quads[p, q]) && ((p < 2) || (q < 2)))
                            cBefore[p]++;

                        if ((quadAfter[0] == quads[p, q]) && ((p < 2) || (q < 2)))
                            cAfter[p]++;
                        if ((quadAfter[1] == quads[p, q]) && ((p < 2) || (q < 2)))
                            cAfter[p]++;
                        if ((quadAfter[2] == quads[p, q]) && ((p < 2) || (q < 2)))
                            cAfter[p]++;
                        if ((quadAfter[3] == quads[p, q]) && ((p < 2) || (q < 2)))
                            cAfter[p]++;
                    }
                }

                int dC1 = cAfter[0] - cBefore[0];
                int dC2 = cAfter[1] - cBefore[1];
                int dC3 = cAfter[2] - cBefore[2];

                #endregion

                erAddPixel(erStack[erStack.Count - 1], x, y, nonBoundaryNeighbours, nonBoundaryNeighboursHoriz, dC1, dC2, dC3, currentPixel);

                accumulatedPixelMask[currentPixel] = true;

                if (thresholdLevel == (255 / thresholdDelta) + 1) // if all possible threshold levels have been processed signal the end
                {
                    //gia to erSave ksekinaei apo to teleutaio stoixeio kathws einai to root ER (efoson ta apothikeuei anapoda kata thn anakthsh. giayto otan diavazw to dentro ekswterika apo thn erTreeExtract ksekinaw apo to [0]. Giati tote to dentro exei apothikeutei kanonika sta @regions kai sto [0] einai to root!

                    // the last child is not merged anywhere so its dominant color must me calculated separetaly
                    if (ceCersAllowed)
                        CombineParentChildPixels(null, erStack[erStack.Count - 1]);                
                    if (oppositeTree)
                        erStack[erStack.Count - 1].level = 255 - erStack[erStack.Count - 1].level;
                    //calculateChildDominantColor(erStack[erStack.Count - 1]);
                    erSaveCersOnly(erStack[erStack.Count - 1], null, null); 
                   
                    //erSave(erStack[erStack.Count - 1], null, null);
                    //for (int r = 0; r < erStack.Count; r++)
                    //{
                    //    ERStat stat = erStack[r];
                    //    if (stat.crossings != null)
                    //    {
                    //        stat.crossings.Clear();
                    //        stat.crossings = null;
                    //    }
                    //    MyERFilter.deleteERStatTree(ref stat);
                    //}
                    //erStack.Clear();

                    return;
                }

                currentPixel = boundaryPixels[thresholdLevel][boundaryPixels[thresholdLevel].Count - 1];
                boundaryPixels[thresholdLevel].RemoveAt(boundaryPixels[thresholdLevel].Count - 1);
                currentEdge = boundaryEdges[thresholdLevel][boundaryEdges[thresholdLevel].Count - 1];
                boundaryEdges[thresholdLevel].RemoveAt(boundaryEdges[thresholdLevel].Count - 1);

                while ((thresholdLevel < (255 / thresholdDelta) + 1) && boundaryPixels[thresholdLevel].Count == 0)
                {
                    //c# tip!
                    // όταν το σύστημα κάνει έλεγχο σε μία πολλαπλή συνθήκη με && τότε αν ελέγξει το πρώτο μισό και
                    // έχει ως αποτέλεσμα "false" δεν ελέγχει την υπόλοιπη συνθήκη.
                    // is it safe??
                    thresholdLevel++;
                }

                int newLevel = imageData[currentPixel];
                if (newLevel != currentLevel)
                {
                    currentLevel = newLevel;

                    while (erStack[erStack.Count - 1].level < newLevel)
                    {
                        ERStat er = erStack[erStack.Count - 1];
                        erStack.RemoveAt(erStack.Count - 1);

                        if (newLevel < erStack[erStack.Count - 1].level)// auto uparxei gia thn periptwsh emfoleumenwn er poy apotelountai apo 1 mono pixel
                        {
                            erStack.Add(new ERStat(newLevel, currentPixel, currentPixel % width, currentPixel / width));
                            erMerge(erStack[erStack.Count - 1], er);
                            break;
                        }
                        erMerge(erStack[erStack.Count - 1], er);
                    }
                }
            }//end psycho for

        }
        private void erAddPixel(ERStat parent, int x, int y, int nonBoundaryNeighbours, int nonBoundaryNeighboursHoriz, int dC1, int dC2, int dC3, int currentPixel)
        {
            parent.area++;
            parent.perimeter += 4 - 2 * nonBoundaryNeighbours;
            if (ceCersAllowed) parent.pixels.Add(currentPixel);

            List<int> parentCrossings = parent.crossings;
            Rectangle parentRect = parent.rect;
            if (parentCrossings.Count > 0)
            {
                int TopHeightDifference = y - parentRect.Y;
                if (TopHeightDifference < 0) //to neo pixel vrisketai panw apo to rectangle
                    parentCrossings.Insert(0, 2);
                else if (y > parentRect.Bottom - 1) //to neo pixel vrisketai katw apo to rectangle
                    parentCrossings.Add(2);
                else //to neo pixel vrisketai entos toy ypsoys toy rectangle
                    parentCrossings[TopHeightDifference] += 2 - 2 * nonBoundaryNeighboursHoriz;
            }
            else
                parentCrossings.Add(2);

            parent.crossings = parentCrossings;
            parent.rect = parentRect;
            parent.euler += (dC1 - dC2 + 2 * dC3) / 4;

            int newX1 = Math.Min(parent.rect.X, x);
            int newY1 = Math.Min(parent.rect.Y, y);
            int newX2 = Math.Max(parent.rect.Right - 1, x);
            int newY2 = Math.Max(parent.rect.Bottom - 1, y);

            parent.rect.X = newX1;
            parent.rect.Y = newY1;
            parent.rect.Width = newX2 - newX1 + 1;
            parent.rect.Height = newY2 - newY1 + 1;

            parent.rawMoments[0] += x;
            parent.rawMoments[1] += y;

            parent.centralMoments[0] += x * x;
            parent.centralMoments[1] += x * y;
            parent.centralMoments[2] += y * y;
        }      
        private void erMerge(ERStat parent, ERStat child)
        {
            #region Cer history calculation for CECERS
            if (ceCersAllowed) CombineParentChildPixels(parent, child);
            #endregion

            parent.area += child.area;
            parent.perimeter += child.perimeter;

            for (int i = parent.rect.Y; i <= Math.Min(parent.rect.Bottom - 1, child.rect.Bottom - 1); i++)
                if (i - child.rect.Y >= 0)
                    parent.crossings[i - parent.rect.Y] += child.crossings[i - child.rect.Y];

            for (int i = parent.rect.Y - 1; i >= child.rect.Y; i--)
                if (i - child.rect.Y < child.crossings.Count)
                    parent.crossings.Insert(0, child.crossings[i - child.rect.Y]);
                else
                    parent.crossings.Insert(0, 0);

            for (int i = parent.rect.Bottom; i < child.rect.Y; i++)
                parent.crossings.Add(0);

            for (int i = Math.Max(parent.rect.Bottom, child.rect.Y); i <= child.rect.Bottom - 1; i++)
                parent.crossings.Add(child.crossings[i - child.rect.Y]);

            parent.euler += child.euler;

            int newX1 = Math.Min(parent.rect.X, child.rect.X);
            int newY1 = Math.Min(parent.rect.Y, child.rect.Y);
            int newX2 = Math.Max(parent.rect.Right - 1, child.rect.Right - 1);
            int newY2 = Math.Max(parent.rect.Bottom - 1, child.rect.Bottom - 1);
            parent.rect.X = newX1;
            parent.rect.Y = newY1;
            parent.rect.Width = newX2 - newX1 + 1;
            parent.rect.Height = newY2 - newY1 + 1;

            parent.rawMoments[0] += child.rawMoments[0];
            parent.rawMoments[1] += child.rawMoments[1];

            parent.centralMoments[0] += child.centralMoments[0];
            parent.centralMoments[1] += child.centralMoments[1];
            parent.centralMoments[2] += child.centralMoments[2];

            List<int> mCrossings = new List<int>();
            mCrossings.Add(child.crossings[(int)(child.rect.Height) / 6]);
            mCrossings.Add(child.crossings[(int)3 * (child.rect.Height) / 6]);
            mCrossings.Add(child.crossings[(int)5 * (child.rect.Height) / 6]);
            mCrossings.Sort();

            child.medCrossings = (float)mCrossings[1];

            child.crossings.Clear();
            child.crossings = null;

            child.level = child.level * thresholdDelta;
            if (oppositeTree)
                child.level = 255 - child.level;

            //if ((child.rect.Width <= 2 && child.rect.Height <= 2) || //rejected due to size restrictions
             //       (child.area < minArea * regionMask.Rows * regionMask.Cols) ||
            //        (child.area > maxArea * regionMask.Rows * regionMask.Cols))
            if (!ERUtils.SizeRestrictionsArea(child, new ERExtractor.ExtractorParameters(new ERFilterProps(this.thresholdDelta, this.minArea, this.maxArea, this.minProbability, this.minProbabilityDiff), regionMask.Cols, regionMask.Rows)))
            {
                numRejectedRegions++;

                if (child.prev != null)
                {
                    child.prev.next = child.next;
                }

                ERStat newChild = child.child;

                if (newChild != null)
                {
                    while (newChild.next != null)
                        newChild = newChild.next;
                    newChild.next = parent.child;
                    if (parent.child != null)
                        parent.child.prev = newChild;
                    parent.child = child.child;
                    child.child.parent = parent;
                }

                child = null;
            }
            else
            {
                if (classifier != null ? (child.probability >= minProbability) : true || nonMaxSuppression)
                {
                    numAcceptedRegions++;
                }
                else
                {
                    numToBeRejectedRegions++;
                    child.toBeRecected = true;
                }
                child.next = parent.child;
                if (parent.child != null)
                {
                    parent.child.prev = child;
                }
                parent.child = child;
                child.parent = parent;
            }                  
        }

        #region Methods for saving to @regions according to some characteristic
        
        private ERStat erSaveCersOnly(ERStat er, ERStat parent, ERStat prev)
        {        
            regions.Add(er);
            regions[regions.Count - 1].parent = parent;

            if (prev != null) prev.next = regions[regions.Count - 1];
            else if (parent != null) parent.child = regions[regions.Count - 1];

            ERStat oldPrev = null;
            ERStat thisER = regions[regions.Count - 1];

            // cer decision
            if (thisER.parent == null)
            {
                thisER.areaVariation = 1f;
                thisER.isCer = false;
            }
            else
            {
                ERStat tempParent = FindAGoodParent(thisER); // finds a parent with > level + 3
                
                if (tempParent.parent != null) // allagh wste na mhn sugrinei ta ERs poy einai monadika me to root ER kai xanei plhroforia
                {
                    thisER.areaVariation = (float)(tempParent.area - thisER.area) / thisER.area;
                    thisER.isCer = thisER.areaVariation < 0.5 ? true : false;
                }
                else
                {
                    thisER.areaVariation = 1f;
                    thisER.isCer = true;
                }
            }
            //end cer decision

            for (ERStat child = er.child; child != null; child = child.next)
                oldPrev = erSaveCersOnly(child, thisER, oldPrev);

            return thisER;
        } // swzei mono tis plhrofories gia ta CERs
        private void erEvalTree(ERStat er)
        {
            filterTimes++;
            er.probability = classifier.Eval(er);

            for (ERStat child = er.child; child != null; child = child.next)
            {
                erEvalTree(child);
            }
        }
        private ERStat erSaveNMSOnly(ERStat er, ERStat parent, ERStat prev) // idio me to arxiko erSave swzei mono tis plhrofories gia to NMS
        {
            regions.Add(er);
            regions[regions.Count - 1].parent = parent;

            if (prev != null) prev.next = regions[regions.Count - 1];
            else if (parent != null) parent.child = regions[regions.Count - 1];

            ERStat oldPrev = null;
            ERStat thisER = regions[regions.Count - 1];

            if (thisER.parent == null)
                thisER.probability = 0;

            if (nonMaxSuppression)
            {
                if (thisER.parent == null)
                {
                    thisER.maxProbabilityAncestor = thisER;
                    thisER.minProbabilityAncestor = thisER;
                }
                else
                {
                    thisER.maxProbabilityAncestor = (thisER.probability > parent.maxProbabilityAncestor.probability) ? thisER : parent.maxProbabilityAncestor;
                    thisER.minProbabilityAncestor = (thisER.probability < parent.minProbabilityAncestor.probability) ? thisER : parent.minProbabilityAncestor;

                    if ((thisER.maxProbabilityAncestor.probability > minProbability) && (thisER.maxProbabilityAncestor.probability - thisER.minProbabilityAncestor.probability > minProbabilityDiff))
                    {
                        thisER.maxProbabilityAncestor.local_maxima = true;
                        if ((thisER.parent.local_maxima) && (thisER.maxProbabilityAncestor == thisER))
                            thisER.parent.local_maxima = false;
                    }
                    else if (thisER.probability < thisER.parent.probability)
                        thisER.minProbabilityAncestor = thisER;
                    else if (thisER.probability > thisER.parent.probability)
                        thisER.maxProbabilityAncestor = thisER;
                }
            }

            for (ERStat child = er.child; child != null; child = child.next)
                oldPrev = erSaveNMSOnly(child, thisER, oldPrev);

            return thisER;
        }

        private ERStat erSave(ERStat er, ERStat parent, ERStat prev) // decide gia cers kai nms
        {
            regions.Add(er);
            regions[regions.Count - 1].parent = parent;

            if (prev != null) prev.next = regions[regions.Count - 1];
            else if (parent != null) parent.child = regions[regions.Count - 1];

            ERStat oldPrev = null;
            ERStat thisER = regions[regions.Count - 1];

            if (thisER.parent == null)
                thisER.probability = 0;

            // test for cer decision
            if (thisER.parent == null)
            {
                thisER.areaVariation = 1f;
                thisER.isCer = false;
            }
            else
            {
                ERStat tempParent = FindAGoodParent(thisER); // finds a parent with > level + 3
                if (tempParent != null)
                {
                    thisER.areaVariation = (float)(tempParent.area - thisER.area) / thisER.area;
                    thisER.isCer = thisER.areaVariation < 0.5 ? true : false;
                }
                else
                {
                    thisER.areaVariation = 1f;
                    thisER.isCer = false;
                }
            }
            //end test

            if (nonMaxSuppression)
            {
                if (thisER.parent == null)
                {
                    thisER.maxProbabilityAncestor = thisER;
                    thisER.minProbabilityAncestor = thisER;
                }
                else
                {
                    thisER.maxProbabilityAncestor = (thisER.probability > parent.maxProbabilityAncestor.probability) ? thisER : parent.maxProbabilityAncestor;
                    thisER.minProbabilityAncestor = (thisER.probability < parent.minProbabilityAncestor.probability) ? thisER : parent.minProbabilityAncestor;

                    if ((thisER.maxProbabilityAncestor.probability > minProbability) && (thisER.maxProbabilityAncestor.probability - thisER.minProbabilityAncestor.probability > minProbabilityDiff))
                    {
                        thisER.maxProbabilityAncestor.local_maxima = true;
                        if ((thisER.parent.local_maxima) && (thisER.maxProbabilityAncestor == thisER))
                            thisER.parent.local_maxima = false;
                    }
                    else if (thisER.probability < thisER.parent.probability)
                        thisER.minProbabilityAncestor = thisER;
                    else if (thisER.probability > thisER.parent.probability)
                        thisER.maxProbabilityAncestor = thisER;
                }
            }

            for (ERStat child = er.child; child != null; child = child.next)
                oldPrev = erSave(child, thisER, oldPrev);

            return thisER;
        } // swzei local_maxima kai CERs mazi
        private ERStat erNDSearch(ERStat er, ERStat parent, ERStat prev)
        {
            regions.Add(er);
            regions[regions.Count - 1].parent = parent;

            if (prev != null) prev.next = regions[regions.Count - 1];
            else if (parent != null) parent.child = regions[regions.Count - 1];

            ERStat oldPrev = null;
            ERStat thisER = regions[regions.Count - 1];

            // test for near duplicate decision
            if (thisER.parent == null)
            {
                thisER.nearDuplicate = false;
            }
            else
            {
                float areaVariation = (float)(parent.area - thisER.area) / thisER.area;
                if (areaVariation < 0.1) // parent and thisER are near duplicate
                {
                    if (thisER.areaVariation > parent.areaVariation)
                    {
                        thisER.nearDuplicate = true;
                    }
                    else
                    {
                        parent.nearDuplicate = true;
                    }
                }
            }

            for (ERStat child = er.child; child != null; child = child.next)
                oldPrev = erNDSearch(child, thisER, oldPrev);

            return thisER;
        } // swzei ta non near duplicates

        #endregion

        #region Methods to prune the tree

        private ERStat erTreeFilter(InputArray image, ERStat stat, ERStat parent, ERStat prev)
        {
            filterTimes++;
            using (Mat src = image.GetMat())
            {
                ERUtils.CalcNM2stats(src, ref stat);
            }

            if ((classifier != null) && (stat.parent != null))
                stat.probability = classifier.Eval(stat);

            if ( ( ((classifier != null) ? (stat.probability >= minProbability) : true) &&
                   ((stat.area >= minArea * regionMask.Rows * regionMask.Cols) &&
                    (stat.area <= maxArea * regionMask.Rows * regionMask.Cols))) ||
                   (stat.parent == null))
            {
                numAcceptedRegions++;
                ERStat demoStat = ERStat.DeepCopy(stat);
                regions.Add(demoStat);
                regions[regions.Count - 1].parent = parent;
                regions[regions.Count - 1].next = null;
                regions[regions.Count - 1].child = null;

                if (prev != null) prev.next = regions[regions.Count - 1];
                else if (parent != null) parent.child = regions[regions.Count - 1];

                ERStat oldPrev = null;
                ERStat thisEr = regions[regions.Count - 1];

                for (ERStat child = stat.child; child != null; child = child.next)
                    oldPrev = erTreeFilter(image, child, thisEr, oldPrev);

                return thisEr;
            }
            else
            {
                numRejectedRegions++;

                ERStat oldPrev = prev;

                for (ERStat child = stat.child; child != null; child = child.next)
                    oldPrev = erTreeFilter(image, child, parent, oldPrev);

                return oldPrev;
            }
        }
        private ERStat erNonCerSuppression(ERStat stat, ERStat parent, ERStat prev)
        {
            filterTimes++;
            if (stat.isCer || stat.parent == null )
            {
                ERStat demostat = ERStat.DeepCopy(stat);
                regions.Add(demostat);
                // prosthetei to neo komvo diagrafontas opoiadhpote sxesh me aderfia klp
                // gi ayto kanei tis sundeseis-aposundeseis sta plaisia ths listas "regions"
                // wste auth na apotelei to neo pruned dentro
                regions[regions.Count - 1].parent = parent;
                regions[regions.Count - 1].next = null;
                regions[regions.Count - 1].child = null;

                if (prev != null) prev.next = regions[regions.Count - 1];
                else if (parent != null) parent.child = regions[regions.Count - 1];

                ERStat oldPrev = null;
                ERStat thisER = regions[regions.Count - 1];

                for (ERStat child = stat.child; child != null; child = child.next)
                    oldPrev = erNonCerSuppression(child, thisER, oldPrev);

                return thisER;
            }
            else
            {
                numRejectedRegions++;
                numAcceptedRegions--;

                ERStat oldPrev = prev;

                for (ERStat child = stat.child; child != null; child = child.next)
                    oldPrev = erNonCerSuppression(child, parent, oldPrev);

                return oldPrev;
            }
        }
        private ERStat erDeleteNearDuplicate(ERStat stat, ERStat parent, ERStat prev)
        {
            filterTimes++;
            if (!stat.nearDuplicate || stat.parent == null)
            {
                ERStat demostat = ERStat.DeepCopy(stat);
                regions.Add(demostat);
                // prosthetei to neo komvo diagrafontas opoiadhpote sxesh me aderfia klp
                // gi ayto kanei tis sundeseis-aposundeseis sta plaisia ths listas "regions"
                // wste auth na apotelei to neo pruned dentro
                regions[regions.Count - 1].parent = parent;
                regions[regions.Count - 1].next = null;
                regions[regions.Count - 1].child = null;

                if (prev != null) prev.next = regions[regions.Count - 1];
                else if (parent != null) parent.child = regions[regions.Count - 1];

                ERStat oldPrev = null;
                ERStat thisER = regions[regions.Count - 1];

                for (ERStat child = stat.child; child != null; child = child.next)
                    oldPrev = erDeleteNearDuplicate(child, thisER, oldPrev);

                return thisER;
            }
            else
            {
                numRejectedRegions++;
                numAcceptedRegions--;

                ERStat oldPrev = prev;

                for (ERStat child = stat.child; child != null; child = child.next)
                    oldPrev = erDeleteNearDuplicate(child, parent, oldPrev);

                return oldPrev;
            }
        }
        private ERStat erTreeNonmaxSuppression(ERStat stat, ERStat parent, ERStat prev)
        {
            filterTimes++;
            if ((stat.local_maxima) || stat.parent == null)
            {
                ERStat demoStat = ERStat.DeepCopy(stat);
                regions.Add(demoStat);
                regions[regions.Count - 1].parent = parent;
                regions[regions.Count - 1].next = null;
                regions[regions.Count - 1].child = null;

                if (prev != null) prev.next = regions[regions.Count - 1];
                else if (parent != null) parent.child = regions[regions.Count - 1];

                ERStat oldPrev = null;
                ERStat thisER = regions[regions.Count - 1];

                for (ERStat child = stat.child; child != null; child = child.next)
                    oldPrev = erTreeNonmaxSuppression(child, thisER, oldPrev);

                return thisER; // kathe stat epistrefei ton eauto tou gia na exoun apo poy na deixnontai ta aderfia toy, an yparxoun
            }
            else
            {
                numRejectedRegions++;
                numAcceptedRegions--;

                ERStat oldPrev = prev;

                for (ERStat child = stat.child; child != null; child = child.next)
                    oldPrev = erTreeNonmaxSuppression(child, parent, oldPrev);

                return oldPrev;
            }
        }
        #endregion
 
        private ERStat FindAGoodParent(ERStat thisER)
        {
            int lvl = thisER.level;
            ERStat tmpParent = thisER.parent;
            while (tmpParent != null && tmpParent.level < thisER.level + 3)
            {
                if (tmpParent.parent != null) tmpParent = tmpParent.parent;
                else break;
            }
                
            return tmpParent;
        }

        private void CombineParentChildPixels(ERStat parent, ERStat child)
        {
            //if (child.pixelsInEachThreshold.Count > 0) // to child exei polla levels kai molis oloklhrwthike h anazhthsh mesa se auto
            //{
                child.pixelsInEachThreshold[0].AddRange(new List<int>(child.pixels));
            //}
            //else // to child den exei katholou emfoleumena ERs kai ayto einai to prwto level toy
            //{
            //    child.pixelsInEachThreshold.Add(new List<int>(child.pixels));
            //}
            child.pixels = new List<int>(); // svhse ta palia pixels
            if (parent != null) mergeParentChildPixelLists(parent, child);
            child.setSizeType();
        }
        private void mergeParentChildPixelLists(ERStat parent, ERStat child)
        {
            for (int i = 0; i < child.thresholdsInER.Count; i++)
            {
                mergePixels(parent, child.thresholdsInER[i], child.pixelsInEachThreshold[i]);
            }
        }
        private void mergePixels(ERStat parent, int childLevel, List<int> pixels)
        {
            int childInsertionLevel = parent.thresholdsInER.Count;
            bool newListRequired = true;

            for (int i = parent.thresholdsInER.Count - 1; i >= 0; i--)
            {
                if (parent.thresholdsInER[i] < childLevel)
                    continue;                
                else if (parent.thresholdsInER[i] > childLevel)
                {
                    childInsertionLevel = i + 1;
                    break;
                }
                else // if (parent.thresholdsInER[i] == childLevel)
                {
                    newListRequired = false;
                    childInsertionLevel = i;
                }
            }
            //if (childInsertionLevel == -1) // to child threshold einai mikrotero apo ola ta thresholds toy parent
            //{
            //    parent.pixelsInEachThreshold.Add(pixels);
            //}
            //else
            //{
            //    if (newListRequired)
            //        parent.pixelsInEachThreshold.Insert(childInsertionLevel, pixels);
            //    else
            //        parent.pixelsInEachThreshold[childInsertionLevel].AddRange(pixels);
            //}

            if (newListRequired)
            {
                if (childInsertionLevel == parent.thresholdsInER.Count)
                {
                    parent.thresholdsInER.Add(childLevel);
                    parent.pixelsInEachThreshold.Add(new List<int>(pixels));
                }
                else
                {
                    parent.thresholdsInER.Insert(childInsertionLevel, childLevel);
                    parent.pixelsInEachThreshold.Insert(childInsertionLevel, new List<int>(pixels));
                }                
            }
            else
            {
                parent.pixelsInEachThreshold[childInsertionLevel].AddRange(new List<int>(pixels));
            }

        }
        /// <summary>
        /// Repetitive colorEnhancement that uses the full binary mat of the er and adjusts it, instead of drawing a new one
        /// </summary>
        /// <param name="src"></param>
        /// <param name="toEnhance"></param>
        /// <param name="mats"></param>
        /// <returns></returns>
        public List<ERStat> generalizedColorEnhancement(Mat[] src, List<ERStat> toEnhance, ref VectorOfMat mats, ref VectorOfMat resMats)
        {
            if (!ceCersAllowed) return new List<ERStat>();
            if (src.Length < 3)
            {
                throw new Exception("not enough channels to compute dominant color");
            }
            if (toEnhance.Count != mats.Size)
            {
                throw new Exception("unequal mats and ers");
            }
            List<ERStat> colorEnhancedCers = new List<ERStat>();
            List<Byte[]> imageData = new List<byte[]>();
            for (int i = 0; i < src.Length; i++)
            {
                imageData.Add(src[i].GetData());
            }
            for (int i = 0; i < toEnhance.Count; i++)
            {
                ERStat enhanced = new ERStat();
                if (toEnhance[i].sizeType.Equals(ERStat.SizeType.SquareSmall) || toEnhance[i].sizeType.Equals(ERStat.SizeType.Thin))
                {
                    colorEnhancedCers.Add(enhanced);
                    resMats.Push(mats[i]);
                    continue;
                }

                List<int> sMed = calculateSmed(toEnhance[i], false);
                MCvScalar dominantColor = calcDominantColor(sMed, imageData);
                
                List<int> sMax = toEnhance[i].pixelsInEachThreshold[0];
                List<int> sMin = toEnhance[i].pixelsInEachThreshold[toEnhance[i].pixelsInEachThreshold.Count - 1];
                
                MCvScalar maxColor = calcDominantColor(sMax, imageData);
                MCvScalar minColor = calcDominantColor(sMin, imageData);

                if (SimilarColors(maxColor, dominantColor) && SimilarColors(minColor, dominantColor))
                {//add a dummy empty mat to correspond to the er mat for comparison
                    colorEnhancedCers.Add(enhanced);
                    resMats.Push(mats[i]);
                    continue;
                }                 
                else
                {
                    Mat temp = mats[i].Clone();
                    colorEnhancedCers.Add(CreateColorEnhancedCer2(toEnhance[i], dominantColor, ref temp, src));                 
                    resMats.Push(temp.Clone());
                    temp.Dispose();
                }
            }

            return colorEnhancedCers;
        }
        
        /// <summary>
        /// to m periexei stis theseis toy me timh 255 ta pixel toy bb poy anhkoyn sto er
        /// to mnot periexei ta pixel toy bb poy den anhkoyn sto er
        /// </summary>
        /// <param name="c"></param>
        /// <param name="dominantColor"></param>
        /// <param name="m"></param>
        /// <param name="src"></param>
        /// <returns></returns>
        private ERStat CreateColorEnhancedCer2(ERStat c, MCvScalar dominantColor, ref Mat m, Mat[] src)
        {
            Mat roi0Temp = new Mat(src[0], c.rect); // make 3 roi mats for the er values, 1 per channel
            Mat roi1Temp = new Mat(src[1], c.rect);
            Mat roi2Temp = new Mat(src[2], c.rect);

            Mat roi0 = new Mat();
            roi0Temp.CopyTo(roi0);

            Mat roi1 = new Mat();
            roi1Temp.CopyTo(roi1);

            Mat roi2 = new Mat();
            roi2Temp.CopyTo(roi2);

            Mat mNot = new Mat();
            CvInvoke.BitwiseNot(m, mNot);
            roi0.SetTo(new MCvScalar(0), mNot); // set all the pixels that dont belong in the er to zero
            roi1.SetTo(new MCvScalar(0), mNot);
            roi2.SetTo(new MCvScalar(0), mNot);

            Mat domColor0 = new Mat(m.Size, DepthType.Cv32F, 1);
            domColor0.SetTo(new MCvScalar(dominantColor.V0)); // make a mat of the dominant color of each channel gia tis prakseis
            Mat domColor1 = new Mat(m.Size, DepthType.Cv32F, 1);
            domColor1.SetTo(new MCvScalar(dominantColor.V1)); // make a mat of the dominant color of each channel gia tis prakseis
            Mat domColor2 = new Mat(m.Size, DepthType.Cv32F, 1);
            domColor2.SetTo(new MCvScalar(dominantColor.V2)); // make a mat of the dominant color of each channel gia tis prakseis

            Mat diff1 = new Mat(m.Size, DepthType.Cv32F, 1),
                diff2 = new Mat(m.Size, DepthType.Cv32F, 1),
                diff3 = new Mat(m.Size, DepthType.Cv32F, 1);
            CvInvoke.Subtract(roi0, domColor0, diff1, m, DepthType.Cv32F); // subtractions for color similarity
            roi0.Dispose();
            domColor0.Dispose();
            CvInvoke.Subtract(roi1, domColor1, diff2, m, DepthType.Cv32F);
            roi1.Dispose();
            domColor1.Dispose();
            CvInvoke.Subtract(roi2, domColor2, diff3, m, DepthType.Cv32F);
            roi2.Dispose();
            domColor2.Dispose();
            
            CvInvoke.Pow(diff1, 2, diff1); // put to power 2
            CvInvoke.Pow(diff2, 2, diff2);
            CvInvoke.Pow(diff3, 2, diff3);

            CvInvoke.Add(diff1, diff2, diff1); // add parts of sumation
            diff2.Dispose();
            CvInvoke.Add(diff1, diff3, diff1);
            diff3.Dispose();

            CvInvoke.Sqrt(diff1, diff1); // euclidian distance

            CvInvoke.Threshold(diff1, diff1, Math.Sqrt(300), 255, ThresholdType.BinaryInv); // keep only the pixels that have value less than sqrt(300)
            // sideeffect of the threshold is that the pixels that do not belong in the er area have value 0 which is less than the above threshold so they are included in the thresholded output
            diff1.ConvertTo(diff1, DepthType.Cv8U); // turn to byte mat
            CvInvoke.Subtract(diff1, mNot, m); // subtract the pixels that dont belong in the image to fix the sideeffect of the thresholding operation
            diff1.Dispose();
            mNot.Dispose();
            
            m.ConvertTo(m, DepthType.Cv8U);
            // gia na kataskeuasw to neo extremal region
            Byte[] data = m.GetData();
            ERStat result = erStatExtract(data, m.Cols, m.Rows);
            result.enhanced = true;

            return result;
        }
        /// <summary>
        /// decide whether to enhance cers or not and create the enhanced
        /// </summary>
        /// <param name="src">3 channel mat(CV8U) with all the input channels</param>
        /// <param name="toEnhance">the CERs to be enhanced</param>
        /// <returns>a list containing the enhanced CERs</returns>
        public List<ERStat> colorEnhancement(Mat[] src, List<ERStat> toEnhance, ref VectorOfMat mats)
        {
            if (!ceCersAllowed) return new List<ERStat>();
            if (src.Length < 3)
            {
                throw new Exception("not enough channels to compute dominant color");
            }
            List<ERStat> colorEnhancedCers = new List<ERStat>();
            List<Byte[]> imageData = new List<byte[]>();
            for (int i = 0; i < src.Length; i++)
            {
                imageData.Add(src[i].GetData());
            }
            for (int i = 0; i < toEnhance.Count; i++)
            {
                if (toEnhance[i].sizeType.Equals(ERStat.SizeType.SquareSmall) || toEnhance[i].sizeType.Equals(ERStat.SizeType.Thin))
                {
                    colorEnhancedCers.Add(new ERStat());
                    mats.Push(new Mat());
                    continue;
                }
                List<int> sMed = calculateSmed(toEnhance[i], false); // gia to reverse tree
                MCvScalar dominantColor = calcDominantColor(sMed, imageData);

                List<int> sMax = toEnhance[i].pixelsInEachThreshold[0];
                List<int> sMin = toEnhance[i].pixelsInEachThreshold[toEnhance[i].pixelsInEachThreshold.Count - 1];

                MCvScalar maxColor = calcDominantColor(sMax, imageData);
                MCvScalar minColor = calcDominantColor(sMin, imageData);

                if (SimilarColors(maxColor, dominantColor) && SimilarColors(minColor, dominantColor))
                {//add a dummy empty mat to correspond to the er mat for comparison
                    colorEnhancedCers.Add(new ERStat());
                    mats.Push(new Mat());
                    continue;
                }                 
                else
                {
                    Mat temp;
                    colorEnhancedCers.Add(CreateColorEnhancedCer(toEnhance[i], dominantColor, imageData, out temp));
                    mats.Push(temp.Clone());
                }
            }

            return colorEnhancedCers;
        }
        
        private List<int> calculateSmed(ERStat cer, bool descending)
        {
            List<int> sMed = new List<int>(); // a sorted list of pixels

            int totalArea = cer.area;
            int threshLvl;
            if (descending) threshLvl = 0; else threshLvl = cer.thresholdsInER.Count - 1;
            int remainingSpots = (int)Math.Ceiling(totalArea / 2.0); // 50% of pixels

            while (remainingSpots > 0)
            {
                if (cer.pixelsInEachThreshold[threshLvl].Count <= remainingSpots)
                {
                    sMed.AddRange(cer.pixelsInEachThreshold[threshLvl]);
                    remainingSpots -= cer.pixelsInEachThreshold[threshLvl].Count;
                    if (descending) threshLvl++; else threshLvl--;
                }
                else
                {
                    for (int i = 0; i < remainingSpots; i++)
                    {
                        sMed.Add(cer.pixelsInEachThreshold[threshLvl][i]);
                    }
                    remainingSpots = 0;
                }                   
            }
            return sMed;
        }

        /// <summary>
        /// calculates the average color of a list of pixels for each corresponding channel separately
        /// </summary>
        /// <param name="sMed">a list of pixels</param>
        /// <param name="imageData">the intensity values of the pixels in a 3 dimension byte array</param>
        /// <returns>A scalar with the average color for each channel</returns>
        private MCvScalar calcDominantColor(List<int> sMed, List<Byte[]> imageData)
        {
            MCvScalar dominantColor = new MCvScalar(0, 0, 0);
            for (int j = 0; j < sMed.Count; j++)
            {
                dominantColor.V0 += imageData[0][sMed[j]];
                dominantColor.V1 += imageData[1][sMed[j]];
                dominantColor.V2 += imageData[2][sMed[j]];
            }
            dominantColor.V0 /= sMed.Count();
            dominantColor.V1 /= sMed.Count();
            dominantColor.V2 /= sMed.Count();

            return dominantColor;
        }

        private bool SimilarColors(MCvScalar color1, MCvScalar color2)
        {
            double value = Math.Sqrt(Math.Pow(color1.V0 - color2.V0, 2.0) + Math.Pow(color1.V1 - color2.V1, 2.0) + Math.Pow(color1.V2 - color2.V2, 2.0));

            return value < Math.Sqrt(300);
        }

        private ERStat CreateColorEnhancedCer(ERStat c, MCvScalar dominantColor, List<Byte[]> imageData, out Mat m)
        {
            byte[] ceCerMask = new byte[imageData[0].Length];
            
            for (int i = 0; i < c.pixelsInEachThreshold.Count; i++) // for all the thresholds
            {
                for (int j = 0; j < c.pixelsInEachThreshold[i].Count; j++) // for all the pixels in each threshold
                {
                    int pi = c.pixelsInEachThreshold[i][j];
                    MCvScalar color = new MCvScalar(imageData[0][pi], imageData[1][pi], imageData[2][pi]);
                    if (SimilarColors(dominantColor, color))
                    {                  
                        ceCerMask[pi] = (byte)255;
                    }
                }
            }
            ERStat result = erStatExtract(ceCerMask, mWidth, mHeight);
            byte[,] roi = new byte[result.rect.Height, result.rect.Width];
            for (int y = 0; y < result.rect.Height; y++)
            {
                for (int x = 0; x < result.rect.Width; x++)
                {
                    int y2 = y + result.rect.Y;
                    int x2 = x + result.rect.X;
                    roi[y, x] = ceCerMask[y2 * mWidth + x2];
                }
            }
            Matrix<byte> temp = new Matrix<byte>(roi);
            m = new Mat(result.rect.Height, result.rect.Width, DepthType.Cv8U, 1);
            m.SetTo(temp);

            return result;
        }

        /// <summary>
        /// create an Extremal Region from the enhanced pixels only
        /// </summary>
        /// <param name="imageData"></param>
        /// <returns></returns>
        private ERStat erStatExtract(Byte[] imageData, int width, int height)
        {
            ERStat ceCer = new ERStat(255, 0, 0, 0);

            byte[,] quads = new byte[3, 4];
            quads[0, 0] = 1 << 3;
            quads[0, 1] = 1 << 2;
            quads[0, 2] = 1 << 1;
            quads[0, 3] = 1;
            quads[1, 0] = 1 << 2 | 1 << 1 | 1;
            quads[1, 1] = 1 << 3 | 1 << 1 | 1;
            quads[1, 2] = 1 << 3 | 1 << 2 | 1;
            quads[1, 3] = 1 << 3 | 1 << 2 | 1 << 1;
            quads[2, 0] = 1 << 2 | 1 << 1;
            quads[2, 1] = 1 << 3 | 1;

            bool[] accumulatedPixelMask = new bool[width * height];
            bool firstPixel = true;
            for (int currentPixel = 0; currentPixel < imageData.Count(); currentPixel++)
            {
                if (imageData[currentPixel] != 255) continue;
                
                int x = currentPixel % width;
                int y = currentPixel / width;
                if (firstPixel) { ceCer.rect = new Rectangle(x, y, 1, 1); firstPixel = false; }

                int nonBoundaryNeighbours = 0;
                int nonBoundaryNeighboursHoriz = 0;

                byte[] quadBefore = new Byte[] { 0, 0, 0, 0 };
                byte[] quadAfter = new Byte[] { 0, 0, 0, 0 };
                quadAfter[0] = 1 << 1;
                quadAfter[1] = 1 << 3;
                quadAfter[2] = 1 << 2;
                quadAfter[3] = 1;

                for (int edge = 0; edge < 8; edge++)
                {
                    int neighbour4 = -1; // von Neumann neighborhood
                    int neighbour8 = -1; // Moore neighborhood
                    int cell = 0;
                    switch (edge)
                    {   //all the ifs check if the pixel belongs in the matrix boundary
                        case 0: if (x < width - 1) { neighbour4 = neighbour8 = currentPixel + 1; } cell = 5; break;
                        case 1: if ((x < width - 1) && (y < height - 1)) { neighbour8 = currentPixel + 1 + width; } cell = 8; break;
                        case 2: if (y < height - 1) { neighbour4 = neighbour8 = currentPixel + width; } cell = 7; break;
                        case 3: if ((x > 0) && (y < height - 1)) { neighbour8 = currentPixel - 1 + width; } cell = 6; break;
                        case 4: if (x > 0) { neighbour4 = neighbour8 = currentPixel - 1; } cell = 3; break;
                        case 5: if ((x > 0) && (y > 0)) { neighbour8 = currentPixel - 1 - width; } cell = 0; break;
                        case 6: if (y > 0) { neighbour4 = neighbour8 = currentPixel - width; } cell = 1; break;
                        default: if ((y > 0) && (x < width - 1)) { neighbour8 = currentPixel + 1 - width; } cell = 2; break;
                    }
                    if ((neighbour4 != -1) && (accumulatedPixelMask[neighbour4]) && (imageData[neighbour4] <= imageData[currentPixel]))
                    {
                        nonBoundaryNeighbours++;
                        if ((edge == 0) || (edge == 4))
                            nonBoundaryNeighboursHoriz++;
                    }

                    int pixValue = imageData[currentPixel] + 1;
                    if (neighbour8 != -1)
                        if (accumulatedPixelMask[neighbour8])
                            pixValue = imageData[neighbour8];

                    if (pixValue <= imageData[currentPixel])
                    {
                        switch (cell)
                        {
                            case 0:
                                quadBefore[3] = (byte)(quadBefore[3] | 1 << 3);
                                quadAfter[3] = (byte)(quadAfter[3] | 1 << 3);
                                break;
                            case 1:
                                quadBefore[3] = (byte)(quadBefore[3] | 1 << 2);
                                quadAfter[3] = (byte)(quadAfter[3] | 1 << 2);
                                quadBefore[0] = (byte)(quadBefore[0] | 1 << 3);
                                quadAfter[0] = (byte)(quadAfter[0] | 1 << 3);
                                break;
                            case 2:
                                quadBefore[0] = (byte)(quadBefore[0] | 1 << 2);
                                quadAfter[0] = (byte)(quadAfter[0] | 1 << 2);
                                break;
                            case 3:
                                quadBefore[3] = (byte)(quadBefore[3] | 1 << 1);
                                quadAfter[3] = (byte)(quadAfter[3] | 1 << 1);
                                quadBefore[2] = (byte)(quadBefore[2] | 1 << 3);
                                quadAfter[2] = (byte)(quadAfter[2] | 1 << 3);
                                break;
                            case 5:
                                quadBefore[0] = (byte)(quadBefore[0] | 1);
                                quadAfter[0] = (byte)(quadAfter[0] | 1);
                                quadBefore[1] = (byte)(quadBefore[1] | 1 << 2);
                                quadAfter[1] = (byte)(quadAfter[1] | 1 << 2);
                                break;
                            case 6:
                                quadBefore[2] = (byte)(quadBefore[2] | 1 << 1);
                                quadAfter[2] = (byte)(quadAfter[2] | 1 << 1);
                                break;
                            case 7:
                                quadBefore[2] = (byte)(quadBefore[2] | 1);
                                quadAfter[2] = (byte)(quadAfter[2] | 1);
                                quadBefore[1] = (byte)(quadBefore[1] | 1 << 1);
                                quadAfter[1] = (byte)(quadAfter[1] | 1 << 1);
                                break;
                            default:
                                quadBefore[1] = (byte)(quadBefore[1] | 1);
                                quadAfter[1] = (byte)(quadAfter[1] | 1);
                                break;
                        }
                    }

                } // endfor edge

                int[] cBefore = new int[] { 0, 0, 0 };
                int[] cAfter = new int[] { 0, 0, 0 };

                for (int p = 0; p < 3; p++)
                {
                    for (int q = 0; q < 4; q++)
                    {
                        if ((quadBefore[0] == quads[p, q]) && ((p < 2) || (q < 2)))
                            cBefore[p]++;
                        if ((quadBefore[1] == quads[p, q]) && ((p < 2) || (q < 2)))
                            cBefore[p]++;
                        if ((quadBefore[2] == quads[p, q]) && ((p < 2) || (q < 2)))
                            cBefore[p]++;
                        if ((quadBefore[3] == quads[p, q]) && ((p < 2) || (q < 2)))
                            cBefore[p]++;

                        if ((quadAfter[0] == quads[p, q]) && ((p < 2) || (q < 2)))
                            cAfter[p]++;
                        if ((quadAfter[1] == quads[p, q]) && ((p < 2) || (q < 2)))
                            cAfter[p]++;
                        if ((quadAfter[2] == quads[p, q]) && ((p < 2) || (q < 2)))
                            cAfter[p]++;
                        if ((quadAfter[3] == quads[p, q]) && ((p < 2) || (q < 2)))
                            cAfter[p]++;
                    }
                }

                int dC1 = cAfter[0] - cBefore[0];
                int dC2 = cAfter[1] - cBefore[1];
                int dC3 = cAfter[2] - cBefore[2];

                erAddPixel2(ceCer, x, y, nonBoundaryNeighbours, nonBoundaryNeighboursHoriz, dC1, dC2, dC3, currentPixel);

                accumulatedPixelMask[currentPixel] = true;
            }
            List<int> mCrossings = new List<int>();
            mCrossings.Add(ceCer.crossings[(int)(ceCer.rect.Height) / 6]);
            mCrossings.Add(ceCer.crossings[(int)3 * (ceCer.rect.Height) / 6]);
            mCrossings.Add(ceCer.crossings[(int)5 * (ceCer.rect.Height) / 6]);
            mCrossings.Sort();

            ceCer.medCrossings = (float)mCrossings[1];

            ceCer.crossings.Clear();
            ceCer.crossings = null;

            if (classifier != null)
                ceCer.probability = classifier.Eval(ceCer);
            return ceCer;
        }
        private void erAddPixel2(ERStat parent, int x, int y, int nonBoundaryNeighbours, int nonBoundaryNeighboursHoriz, int dC1, int dC2, int dC3, int currentPixel)
        {
            parent.area++;
            parent.perimeter += 4 - 2 * nonBoundaryNeighbours;
            parent.pixels.Add(currentPixel);

            List<int> parentCrossings = parent.crossings;
            Rectangle parentRect = parent.rect;
            if (parentCrossings.Count > 0)
            {
                int TopHeightDifference = y - parentRect.Y;
                if (TopHeightDifference < 0) //to neo pixel vrisketai panw apo to rectangle
                    parentCrossings.Insert(0, 2);
                else if (y > parentRect.Bottom - 1) //to neo pixel vrisketai katw apo to rectangle
                {
                    //for (int i = 0; i < y - parentRect.Bottom - 2; i++)
                    //{
                    //    parentCrossings.Add(0);
                    //}
                    if (y - (parentRect.Bottom - 1) > 1) // sthn periptwsh poy to ceCer exei gamithei entelws kai exei svhstei oloklhrh grammh apo pixels
                    {
                        int horizontalGap = y - (parentRect.Bottom - 1) - 1;
                        for (int i = 0; i < horizontalGap; i++)
                            parentCrossings.Add(0);
                    }
                    parentCrossings.Add(2);
                }
                else //to neo pixel vrisketai entos toy ypsoys toy rectangle
                    parentCrossings[TopHeightDifference] += 2 - 2 * nonBoundaryNeighboursHoriz;
            }
            else
                parentCrossings.Add(2);

            parent.crossings = parentCrossings;
            parent.rect = parentRect;
            parent.euler += (dC1 - dC2 + 2 * dC3) / 4;

            int newX1 = Math.Min(parent.rect.X, x);
            int newY1 = Math.Min(parent.rect.Y, y);
            int newX2 = Math.Max(parent.rect.Right - 1, x);
            int newY2 = Math.Max(parent.rect.Bottom - 1, y);

            parent.rect.X = newX1;
            parent.rect.Y = newY1;
            parent.rect.Width = newX2 - newX1 + 1;
            parent.rect.Height = newY2 - newY1 + 1;

            parent.rawMoments[0] += x;
            parent.rawMoments[1] += y;

            parent.centralMoments[0] += x * x;
            parent.centralMoments[1] += x * y;
            parent.centralMoments[2] += y * y;
        }      

        private void calcChildDominantColor(ERStat child)
        {
            int thresholdLvlsCount = child.thresholdsInER.Count;

            int totalArea = child.area;
            int thislvlArea = child.pixelsInEachThreshold[0].Count;

            if (thislvlArea >= totalArea / 2.0)
            {
                child.domThresh = child.level;
            }
            else
            {
                child.domThresh = thislvlArea * child.level;
                int remainingSpots = (int)Math.Ceiling(child.area / 2.0 - thislvlArea);
                for (int i = 1; i < child.pixelsInEachThreshold.Count; i++)
                {
                    if (child.pixelsInEachThreshold[i].Count <= remainingSpots)
                    {
                        child.domThresh += child.pixelsInEachThreshold[i].Count * child.thresholdsInER[i];
                        remainingSpots -= child.pixelsInEachThreshold[i].Count;
                    }
                    else
                    {
                        child.domThresh += child.thresholdsInER[i] * remainingSpots;
                        break;
                    }
                }
                child.domThresh /= Math.Ceiling(child.area / 2.0);
            }
        }

        public CERFilter()
        {
            //enhanced = new List<CECER>();
            thresholdDelta = 1;
            minArea = 0f;
            maxArea = 1f;
            minProbability = 0f;
            nonMaxSuppression = false;
            minProbabilityDiff = 1f;
            numAcceptedRegions = 0;
            numRejectedRegions = 0;
        }

        #region global vars
        //public List<CECER> enhanced;
        public float minProbability;
        public bool nonMaxSuppression;
        public float minProbabilityDiff;

        protected int thresholdDelta;
        protected float maxArea, minArea;

        protected Callback classifier;

        protected int numRejectedRegions, numAcceptedRegions, numToBeRejectedRegions;
        private List<ERStat> regions;
        private Mat regionMask;
        private int mWidth, mHeight;

        #endregion

        static int filterTimes = 0;

        #region dummy
        public override void setCallback(Callback cb)
        {
            classifier = cb;
        }

        public override void setThresholdDelta(int thresholdDelta)
        {
            if (!(thresholdDelta > 0 && thresholdDelta <= 128))
                throw new Exception("Exception at ThresholdDelta");
            this.thresholdDelta = thresholdDelta;
        }

        public override void setMinArea(float minArea)
        {
            if (!(minArea >= 0 && minArea < maxArea))
                throw new Exception("Exception at minArea");
            this.minArea = minArea;
        }

        public override void setMaxArea(float maxArea)
        {
            if (!(maxArea <= 1)) throw new Exception("First Exception at maxArea");
            if (!(minArea < maxArea)) throw new Exception("Second Exception at maxArea");
            this.maxArea = maxArea;
        }

        public override void setMinProbability(float minProbability)
        {
            if (!((minProbability >= 0f) && (minProbability <= 1f))) throw new Exception("Exception at set min probability");
            this.minProbability = minProbability;
        }

        public override void setMinProbabilityDiff(float minProbabilityDiff)
        {
            if (!((minProbabilityDiff >= 0f) && (minProbabilityDiff <= 1f))) throw new Exception("Exception at set min probability diff");
            this.minProbabilityDiff = minProbabilityDiff;
        }

        public override void setNonMaxSuppression(bool nonMaxSuppression)
        {
            this.nonMaxSuppression = nonMaxSuppression;
        }

        public override int getNumRejected()
        {
            return numRejectedRegions;
        }

        public static CERFilter createERFilterNM1(Callback cb, int thresholdDelta, float minArea, float maxArea, float minProbability, bool nonMaxSuppression, float minProbabilityDiff)
        {
            if (!((minProbability >= 0f) && (minProbability <= 1f))) throw new Exception("Exception at createERFilterNM1 @minProbability");
            if (!((minArea < maxArea) && (minArea >= 0f) && (maxArea <= 1f))) throw new Exception("Exception at createERFilterNM1 @minArea and maxArea");
            if (!((thresholdDelta >= 0) && (thresholdDelta <= 128))) throw new Exception("Exception at createERFilterNM1 @thresholdDelta");
            if (!((minProbabilityDiff >= 0f) && (minProbabilityDiff <= 1f))) throw new Exception("Exception at createERFilterNM1 @minProbabilityDiff");

            CERFilter filter = new CERFilter();
            filter.setCallback(cb);
            filter.setThresholdDelta(thresholdDelta);
            filter.setMaxArea(maxArea);
            filter.setMinArea(minArea);
            filter.setMinProbability(minProbability);
            filter.setNonMaxSuppression(nonMaxSuppression);
            filter.setMinProbabilityDiff(minProbabilityDiff);

            return filter;
        }
        public static CERFilter createERFilterNM2(Callback cb, float minProbability)
        {
            if (!((minProbability >= 0f) && (minProbability <= 1f))) throw new Exception("Exception at createERFilterNM2 @minProbability");

            CERFilter filter = new CERFilter();

            filter.setCallback(cb);
            filter.setMinProbability(minProbability);
            return filter;
        }
        public static Callback loadClassifierNM1(String filename)
        {
            return new MyERClassifierNM1(filename);
        }
        public static Callback loadClassifierNM2(String filename)
        {
            return new MyERClassifierNM2(filename);
        }
        public static Callback loadDummyClassifier()
        {
            return new MyDummyClassifier();
        }
        #endregion
    }
}
