﻿using Emgu.CV;
using Emgu.CV.CvEnum;
using Emgu.CV.Structure;
using Emgu.CV.Text;
using Emgu.CV.Util;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WordLocRec
{
    class ERGroupingNM
    {
        public class LineEstimates
        {
            public float top1A0, top1A1, top2A0, top2A1, bottom1A0, bottom1A1, bottom2A0, bottom2A1;
            public int xMin, xMax, hMax;

            public bool Equals(LineEstimates e)
            {
                return ((this.top1A0 == e.top1A0) && (this.top1A1 == e.top1A1) && (this.top2A0 == e.top2A0)
                    && (this.top2A1 == e.top2A1) && (this.bottom1A0 == e.bottom1A0) && (this.bottom1A1 == e.bottom1A1)
                    && (this.bottom2A0 == e.bottom2A0) && (this.bottom2A1 == e.bottom2A1) && (this.xMin == e.xMin)
                    && (this.xMax == e.xMax) && (this.hMax == e.hMax));
            }
            /// <summary>
            /// Ακολουθεί τους μαθηματικούς τύπους του paper.
            /// </summary>
            /// <param name="a"></param>
            /// <param name="b"></param>
            /// <returns></returns>
            public static float DistanceLineEstimates(LineEstimates a, LineEstimates b)
            {
                if (!((a.hMax != 0) && (b.hMax != 0)))
                    throw new Exception("Exception thrown at DistanceLineEstimates");

                if (a.Equals(b)) return 0.0f;

                int xMin = Math.Min(a.xMin, b.xMin);
                int xMax = Math.Max(a.xMax, b.xMax);
                int hMax = Math.Max(a.hMax, b.hMax);

                float distTop = float.MaxValue; float distBottom = float.MaxValue;

                for (int i = 0; i < 2; i++)
                {
                    float topA0, topA1, bottomA0, bottomA1;

                    if (i == 0)
                    {
                        topA0 = a.top1A0;
                        topA1 = a.top1A1;
                        bottomA0 = a.bottom1A0;
                        bottomA1 = a.bottom1A1;
                    }
                    else
                    {
                        topA0 = a.top2A0;
                        topA1 = a.top2A1;
                        bottomA0 = a.bottom2A0;
                        bottomA1 = a.bottom2A1;
                    }
                    for (int j = 0; j < 2; j++)
                    {
                        float topB0, topB1, bottomB0, bottomB1;
                        if (j == 0)
                        {
                            topB0 = b.top1A0;
                            topB1 = b.top1A1;
                            bottomB0 = b.bottom1A0;
                            bottomB1 = b.bottom1A1;
                        }
                        else
                        {
                            topB0 = b.top2A0;
                            topB1 = b.top2A1;
                            bottomB0 = b.bottom2A0;
                            bottomB1 = b.bottom2A1;
                        }

                        float xMinDist = Math.Abs((topA0 + xMin * topA1) - (topB0 + xMin * topB1)); // verticalDist@xMin
                        float xMaxDist = Math.Abs((topA0 + xMax * topA1) - (topB0 + xMax * topB1)); // verticalDist@xMax
                        distTop = Math.Min(distTop, Math.Max(xMinDist, xMaxDist) / hMax);

                        xMinDist = Math.Abs((bottomA0 + xMin * bottomA1) - (bottomB0 + xMin * bottomB1));
                        xMaxDist = Math.Abs((bottomA0 + xMax * bottomA1) - (bottomB0 + xMax * bottomB1));
                        distBottom = Math.Min(distBottom, Math.Max(xMinDist, xMaxDist) / hMax);
                    }
                }

                return Math.Max(distBottom, distTop);
            }
        }

        public class RegionPair
        {
            public Tuple<int, int> a, b; //as Vec2i;

            public RegionPair(Tuple<int, int> a, Tuple<int, int> b)
            {
                this.a = a;
                this.b = b;
            }

            public bool Equals(RegionPair p1)
            {
                return (a.Item1 == p1.a.Item1 && a.Item2 == p1.a.Item2 && b.Item1 == p1.b.Item1 && b.Item2 == p1.b.Item2);
            } 
        }
        public class RegionTriplet
        {
            public Tuple<int, int> a, b, c;
            public LineEstimates lineEstimates = new LineEstimates();
            public RegionTriplet(Tuple<int, int> a, Tuple<int, int> b, Tuple<int, int> c)
            {
                this.a = a;
                this.b = b;
                this.c = c;
            }

            public bool Equals(RegionTriplet t1)
            {
                return (a.Item1 == t1.a.Item1 && a.Item2 == t1.a.Item2
                    && b.Item1 == t1.b.Item1 && b.Item2 == t1.b.Item2
                    && c.Item1 == t1.c.Item1 && c.Item2 == t1.c.Item2);
            }
        }

        public class RegionSequence
        {
            public List<RegionTriplet> triplets = new List<RegionTriplet>();
            public RegionSequence(RegionTriplet t)
            {
                triplets.Add(t);
            }
            public RegionSequence() { }
        }

        #region Methods
        /// <summary>
        /// η μέθοδος isvalidpair ανήκει στο paper κανονικά.
        /// με επιφύλαξη για τον κανόνα του transitive parent-child relationship
        /// ο οποίος εκφράζεται ως η πρώτη συνθήκη στη μέθοδο (minarearect) και δεν υπάρχουν 
        /// λεπτομέρειες υλοποίησης στο paper. 
        /// Θεωρητικά όμως, το τμήμα του minarearect αποτρέπει τον έλεγχο μεταξύ περιοχών
        /// που ανήκουν η μία μέσα στην άλλη. (overlapping)
        /// </summary>
        /// <param name="grey"></param>
        /// <param name="lab"></param>
        /// <param name="mask"></param>
        /// <param name="channels"></param>
        /// <param name="regions"></param>
        /// <param name="idx1"></param>
        /// <param name="idx2"></param>
        /// <returns></returns>
        public static bool isValidPair(Mat grey, Mat lab, Mat mask, VectorOfMat channels, List<ERStat>[] regions, Tuple<int, int> idx1, Tuple<int, int> idx2)
        {
            Rectangle minAreaRect = Rectangle.Union(regions[idx1.Item1][idx1.Item2].rect, regions[idx2.Item1][idx2.Item2].rect);

            if ((minAreaRect == regions[idx1.Item1][idx1.Item2].rect) || (minAreaRect == regions[idx2.Item1][idx2.Item2].rect))
                return false;

            ERStat i, j;
            if (regions[idx1.Item1][idx1.Item2].rect.X < regions[idx2.Item1][idx2.Item2].rect.X)
            {
                i = regions[idx1.Item1][idx1.Item2];
                j = regions[idx2.Item1][idx2.Item2];
            }
            else
            {
                i = regions[idx2.Item1][idx2.Item2];
                j = regions[idx1.Item1][idx1.Item2];
            }

            if (j.rect.X == i.rect.X)
                return false;

            float heightRatio = (float)Math.Min(i.rect.Height, j.rect.Height) / Math.Max(i.rect.Height, j.rect.Height);

            Point centerI = new Point(i.rect.X + i.rect.Width / 2, i.rect.Y + i.rect.Height / 2);
            Point centerJ = new Point(j.rect.X + j.rect.Width / 2, j.rect.Y + j.rect.Height / 2);
            float centroidAngle = (float)Math.Atan2((float)(centerJ.Y - centerI.Y), (float)(centerJ.X - centerI.X));

            int avgWidth = (i.rect.Width + j.rect.Width) / 2;
            float normDistance = (float)(j.rect.X - (i.rect.X + i.rect.Width)) / avgWidth;

            if ((heightRatio < Constants.PAIR_MIN_HEIGHT_RATIO) ||
                (centroidAngle < Constants.PAIR_MIN_CENTROID_ANGLE) ||
                (centroidAngle > Constants.PAIR_MAX_CENTROID_ANGLE) ||
                (normDistance < Constants.PAIR_MIN_REGION_DIST) ||
                (normDistance > Constants.PAIR_MAX_REGION_DIST))
                return false;

            if ((i.parent == null) || (j.parent == null))
                return false; // deprecate the root region!!!!

            i = regions[idx1.Item1][idx1.Item2];
            j = regions[idx2.Item1][idx2.Item2];

            Mat region = new Mat(mask, new Rectangle(i.rect.X, i.rect.Y, i.rect.Width + 2, i.rect.Height+ 2));
            region.SetTo(new MCvScalar(0));

            FloodFillType flags = (FloodFillType)(255 << 8) | FloodFillType.FixedRange | FloodFillType.MaskOnly;
            Rectangle rect = new Rectangle();

            CvInvoke.FloodFill(new Mat(channels[idx1.Item1], new Rectangle(i.rect.X, i.rect.Y, i.rect.Width, i.rect.Height)),
               region,
               new Point(i.pixel % grey.Cols - i.rect.X, i.pixel / grey.Cols - i.rect.Y),
               new MCvScalar(255),
               out rect,
               new MCvScalar(i.level),
               new MCvScalar(0),
               Connectivity.FourConnected,
               flags);
            rect.Width += 2;
            rect.Height += 2;

            Mat rectMask = new Mat(mask, new Rectangle(i.rect.X + 1, i.rect.Y + 1, i.rect.Width, i.rect.Height));

            MCvScalar mean = new MCvScalar();
            MCvScalar std = new MCvScalar();

            CvInvoke.MeanStdDev(new Mat(grey, i.rect), ref mean, ref std, rectMask);

            int greyMean1 = (int)mean.V0;

            CvInvoke.MeanStdDev(new Mat(lab, i.rect), ref mean, ref std, rectMask);

            float aMean1 = (float)mean.V1;
            float bMean1 = (float)mean.V2;

            region = new Mat(mask, new Rectangle(j.rect.X, j.rect.Y, j.rect.Width + 2, j.rect.Height + 2));
            region.SetTo(new MCvScalar(0));

            CvInvoke.FloodFill(new Mat(channels[idx2.Item1], new Rectangle(j.rect.X, j.rect.Y, j.rect.Width, j.rect.Height)),
                region,
                new Point(j.pixel % grey.Cols - j.rect.X, j.pixel / grey.Cols - j.rect.Y),
                new MCvScalar(255),
                out rect,
                new MCvScalar(j.level),
                new MCvScalar(0),
                Connectivity.FourConnected,
                flags);
            rect.Width += 2;
            rect.Height += 2;           

            rectMask = new Mat(mask, new Rectangle(j.rect.X + 1, j.rect.Y + 1, j.rect.Width, j.rect.Height));

            CvInvoke.MeanStdDev(new Mat(grey, j.rect), ref mean, ref std, rectMask);
            int greyMean2 = (int)mean.V0;

            CvInvoke.MeanStdDev(new Mat(lab, j.rect), ref mean, ref std, rectMask);
            float aMean2 = (float)mean.V1;
            float bMean2 = (float)mean.V2;

            if (Math.Abs(greyMean1 - greyMean2) > Constants.PAIR_MAX_INTENSITY_DIST)
                return false;

            if (Math.Sqrt(Math.Pow(aMean1 - aMean2, 2) + Math.Pow(bMean1 - bMean2, 2)) > Constants.PAIR_MAX_AB_DIST)
                return false;

            return true;        
        }
        /// <summary>
        /// Οι έλεγχοι εφόσον έχει το line estimate είναι όπως το paper. 
        /// Η κατεύθυνση της γραμμής είναι από αριστερά προς τα δεξιά, κάτι που αναφέρεται στο paper
        /// αλλά δεν περιγράφεται ρητά ο τρόπος που υλοποιείται. 
        /// Υπάρχει περιορισμός επικάλυψης μεταξύ περιοχών (parent-child?).
        /// Η μέθοδος ανήκει στο paper με επιφύλαξη για τους ελέγχους που δεν περιγράφονται ρητά.
        /// Οι γενικές ιδέες που αναφέρονται στο paper, πάντως, εφαρμόζονται.
        /// </summary>
        /// <param name="regions"></param>
        /// <param name="pair1"></param>
        /// <param name="pair2"></param>
        /// <param name="triplet"></param>
        /// <returns></returns>
        public static bool isValidTriplet(List<ERStat>[] regions, RegionPair pair1, RegionPair pair2, RegionTriplet triplet)
        {
            if (pair1.Equals(pair2)) return false;

            if ((pair1.a.Equals(pair2.a)) || (pair1.a.Equals(pair2.b)) || (pair1.b.Equals(pair2.a)) || (pair1.b.Equals(pair2.b)))
            {
                // To rect που ταυτίζεται για τα 2 ζευγάρια πρέπει να είναι ανάμεσα στα υπολειπόμενα 2 rect (1 απο
                // κάθε ζευγάρι). Άρα, ελέγχει την τιμή του Χ ώστε να βρει την τοποθεσία του rect που ταυτίζεται σε
                // σχέση με τα άλλα δύο. Αν βρίσκεται δεξιότερα από τα 2 ή αριστερότερα απο τα 2 τότε τα ζευγάρια
                // απορρίπτονται. Αν βρίσκεται ανάμεσα τους τότε δημιουργεί τριπλέτα.
                if (pair1.a.Equals(pair2.a))
                {
                    if ((regions[pair1.b.Item1][pair1.b.Item2].rect.X <= regions[pair1.a.Item1][pair1.a.Item2].rect.X)
                        && (regions[pair2.b.Item1][pair2.b.Item2].rect.X <= regions[pair1.a.Item1][pair1.a.Item2].rect.X))
                        return false;
                    if ((regions[pair1.b.Item1][pair1.b.Item2].rect.X >= regions[pair1.a.Item1][pair1.a.Item2].rect.X)
                        && (regions[pair2.b.Item1][pair2.b.Item2].rect.X >= regions[pair1.a.Item1][pair1.a.Item2].rect.X))
                        return false;

                    triplet.a = (regions[pair1.b.Item1][pair1.b.Item2].rect.X < regions[pair2.b.Item1][pair2.b.Item2].rect.X
                        ? pair1.b : pair2.b);
                    triplet.b = pair1.a;
                    triplet.c = (regions[pair1.b.Item1][pair1.b.Item2].rect.X > regions[pair2.b.Item1][pair2.b.Item2].rect.X
                        ? pair1.b : pair2.b);
                }
                else if (pair1.a.Equals(pair2.b))
                {
                    if ((regions[pair1.b.Item1][pair1.b.Item2].rect.X <= regions[pair1.a.Item1][pair1.a.Item2].rect.X)
                        && (regions[pair2.a.Item1][pair2.a.Item2].rect.X <= regions[pair1.a.Item1][pair1.a.Item2].rect.X))
                        return false;
                    if ((regions[pair1.b.Item1][pair1.b.Item2].rect.X >= regions[pair1.a.Item1][pair1.a.Item2].rect.X)
                        && (regions[pair2.a.Item1][pair2.a.Item2].rect.X >= regions[pair1.a.Item1][pair1.a.Item2].rect.X))
                        return false;

                    triplet.a = (regions[pair1.b.Item1][pair1.b.Item2].rect.X < regions[pair2.a.Item1][pair2.a.Item2].rect.X
                        ? pair1.b : pair2.a);
                    triplet.b = pair1.a;
                    triplet.c = (regions[pair1.b.Item1][pair1.b.Item2].rect.X > regions[pair2.a.Item1][pair2.a.Item2].rect.X
                        ? pair1.b : pair2.a);
                }
                else if (pair1.b.Equals(pair2.a))
                {
                    if ((regions[pair1.a.Item1][pair1.a.Item2].rect.X <= regions[pair1.b.Item1][pair1.b.Item2].rect.X)
                        && (regions[pair2.b.Item1][pair2.b.Item2].rect.X <= regions[pair1.b.Item1][pair1.b.Item2].rect.X))
                        return false;
                    if ((regions[pair1.a.Item1][pair1.a.Item2].rect.X >= regions[pair1.b.Item1][pair1.b.Item2].rect.X)
                        && (regions[pair2.b.Item1][pair2.b.Item2].rect.X >= regions[pair1.b.Item1][pair1.b.Item2].rect.X))
                        return false;

                    triplet.a = (regions[pair1.a.Item1][pair1.a.Item2].rect.X < regions[pair2.b.Item1][pair2.b.Item2].rect.X
                        ? pair1.a : pair2.b);
                    triplet.b = pair1.b;
                    triplet.c = (regions[pair1.a.Item1][pair1.a.Item2].rect.X > regions[pair2.b.Item1][pair2.b.Item2].rect.X
                        ? pair1.a : pair2.b);
                }
                else if (pair1.b.Equals(pair2.b))
                {
                    if ((regions[pair1.a.Item1][pair1.a.Item2].rect.X <= regions[pair1.b.Item1][pair1.b.Item2].rect.X)
                        && (regions[pair2.a.Item1][pair2.a.Item2].rect.X <= regions[pair1.b.Item1][pair1.b.Item2].rect.X))
                        return false;
                    if ((regions[pair1.a.Item1][pair1.a.Item2].rect.X >= regions[pair1.b.Item1][pair1.b.Item2].rect.X)
                        && (regions[pair2.a.Item1][pair2.a.Item2].rect.X >= regions[pair1.b.Item1][pair1.b.Item2].rect.X))
                        return false;

                    triplet.a = (regions[pair1.a.Item1][pair1.a.Item2].rect.X < regions[pair2.a.Item1][pair2.a.Item2].rect.X
                        ? pair1.a : pair2.a);
                    triplet.b = pair1.b;
                    triplet.c = (regions[pair1.a.Item1][pair1.a.Item2].rect.X > regions[pair2.a.Item1][pair2.a.Item2].rect.X
                        ? pair1.a : pair2.a);
                }
                // Τα rect που ανήκουν στο triplet δεν πρέπει να έχουν επικάλυψη μεταξύ τους, δλδ τα b ή c να έχουν το ίδιο
                // σημείο Χ με το a ή το a να έχει ίδιο σημείο τέλους (στα δεξιά) με ένα εκ των b ή c.
                // Η επικάλυψη αποφεύγεται με τον τρόπο αυτό εμμέσως.
                if (regions[triplet.a.Item1][triplet.a.Item2].rect.X == regions[triplet.b.Item1][triplet.b.Item2].rect.X
                 && regions[triplet.a.Item1][triplet.a.Item2].rect.X == regions[triplet.c.Item1][triplet.c.Item2].rect.X)
                    return false;
                if (regions[triplet.a.Item1][triplet.a.Item2].rect.Right == regions[triplet.b.Item1][triplet.b.Item2].rect.Right
                 && regions[triplet.a.Item1][triplet.a.Item2].rect.Right == regions[triplet.c.Item1][triplet.c.Item2].rect.Right)
                    return false;


                if (!FitLineEstimates(regions, triplet))
                    return false;

                if ((triplet.lineEstimates.bottom1A0 < triplet.lineEstimates.top1A0) ||
                    (triplet.lineEstimates.bottom1A0 < triplet.lineEstimates.top2A0) ||
                    (triplet.lineEstimates.bottom2A0 < triplet.lineEstimates.top1A0) ||
                    (triplet.lineEstimates.bottom2A0 < triplet.lineEstimates.top2A0))
                    return false;

                int centralHeight = (int)Math.Min(triplet.lineEstimates.bottom1A0, triplet.lineEstimates.bottom2A0)
                                  - (int)Math.Max(triplet.lineEstimates.top1A0, triplet.lineEstimates.top2A0);
                int topHeight = (int)Math.Abs(triplet.lineEstimates.top1A0 - triplet.lineEstimates.top2A0);
                int bottomHeight = (int)Math.Abs(triplet.lineEstimates.bottom1A0 - triplet.lineEstimates.bottom2A0);

                if (centralHeight == 0)
                    return false;

                float topHeightRatio = (float)topHeight / centralHeight;
                float bottomHeightRatio = (float)bottomHeight / centralHeight;

                if ((topHeightRatio > Constants.TRIPLET_MAX_DIST) || (bottomHeightRatio > Constants.TRIPLET_MAX_DIST))
                    return false;

                if (Math.Abs(triplet.lineEstimates.bottom1A1) > Constants.TRIPLET_MAX_SLOPE)
                    return false;

                return true;
            }

            return false;
        }

        public static bool isValidSequence(RegionSequence sequence1, RegionSequence sequence2)
        {
            for (int i = 0; i < sequence2.triplets.Count; i++)
            {
                for (int j = 0; j < sequence1.triplets.Count; j++)
                {
                    if (
                        (LineEstimates.
                        DistanceLineEstimates(sequence2.triplets[i].lineEstimates,
                                              sequence1.triplets[j].lineEstimates) < Constants.SEQUENCE_MAX_TRIPLET_DIST)
                        &&
                        SequenceDistance(sequence2.triplets[i].lineEstimates.xMin,sequence1.triplets[j].lineEstimates.xMax,
                                         sequence1.triplets[j].lineEstimates.xMin,sequence2.triplets[i].lineEstimates.xMax,
                                         sequence2.triplets[i].lineEstimates.hMax,sequence1.triplets[j].lineEstimates.hMax)
                                         < 3 * Constants.PAIR_MAX_REGION_DIST
                        )
                        return true;
                }
            }
            return false;
        }

        public static bool isValidSequenceStrict(RegionSequence sequence1, RegionSequence sequence2)
        {
            for (int i = 0; i < sequence2.triplets.Count; i++)
            {
                for (int j = 0; j < sequence1.triplets.Count; j++)
                {
                    if (
                        (LineEstimates.
                        DistanceLineEstimates(sequence2.triplets[i].lineEstimates,
                                              sequence1.triplets[j].lineEstimates) < Constants.SEQUENCE_MAX_TRIPLET_DIST)
                        &&
                        SequenceDistanceAbs(sequence2.triplets[i].lineEstimates.xMin, sequence1.triplets[j].lineEstimates.xMax,
                                         sequence1.triplets[j].lineEstimates.xMin, sequence2.triplets[i].lineEstimates.xMax,
                                         sequence2.triplets[i].lineEstimates.hMax, sequence1.triplets[j].lineEstimates.hMax)
                                         < 2 * Constants.PAIR_MAX_REGION_DIST
                        )
                        return true;
                }
            }
            return false;
        }

        /// <summary>
        /// Calculates the maximum horizontal distance between 2 sequences, normalized with the maximum vertical distance of the line estimates which were previously calculated
        /// </summary>
        /// <param name="xMinSeq2i"></param>
        /// <param name="xMaxSeq1j"></param>
        /// <param name="xMinSeq1j"></param>
        /// <param name="xMaxSeq2i"></param>
        /// <param name="hMaxSeq2"></param>
        /// <param name="hMaxSeq1"></param>
        /// <returns></returns>
        private static float SequenceDistance(int xMinSeq2i, int xMaxSeq1j, int xMinSeq1j, int xMaxSeq2i, int hMaxSeq2, int hMaxSeq1)
        {
            return (float)Math.Max((xMinSeq2i - xMaxSeq1j),
                                   (xMinSeq1j - xMaxSeq2i)) /
                           Math.Max(hMaxSeq2, hMaxSeq1);
        }
        private static float SequenceDistanceAbs(int xMinSeq2i, int xMaxSeq1j, int xMinSeq1j, int xMaxSeq2i, int hMaxSeq2, int hMaxSeq1)
        {
            return (float)Math.Max((Math.Abs(xMinSeq2i - xMaxSeq1j)),
                                   (Math.Abs(xMinSeq1j - xMaxSeq2i))) /
                          Math.Max(hMaxSeq2, hMaxSeq1);
        }
        //return (float)Math.Max((Math.Abs(sequence2.triplets[i].lineEstimates.xMin - sequence1.triplets[j].lineEstimates.xMax)),
        //                           (Math.Abs(sequence1.triplets[j].lineEstimates.xMin - sequence2.triplets[i].lineEstimates.xMax))) /
        //            Math.Max(sequence2.triplets[i].lineEstimates.hMax, sequence1.triplets[j].lineEstimates.hMax)
        //            < 3 * Constants.PAIR_MAX_REGION_DIST);
        public static bool haveCommonRegion(RegionSequence sequence1, RegionSequence sequence2)
        {
            for (int i = 0; i < sequence2.triplets.Count; i++)
            {
                for (int j = 0; j < sequence1.triplets.Count; j++)
                {
                    if (haveCommonRegion(sequence2.triplets[i], sequence1.triplets[j]))
                        return true;
                }
            }
                return false;
        }

        public static bool haveCommonRegion(RegionTriplet t1, RegionTriplet t2)
        {
            //if (t1.a == t2.a || t1.a == t2.b || t1.a == t2.c || t1.b == t2.a || t1.b == t2.b || t1.b == t2.c || t1.c == t2.a || t1.c == t2.b || t1.c == t2.c)
                //return true;
            if ((t1.a.Equals(t2.a)) || (t1.a.Equals(t2.b)) || (t1.a.Equals(t2.c)) ||
                (t1.b.Equals(t2.a)) || (t1.b.Equals(t2.b)) || (t1.b.Equals(t2.c)) ||
                (t1.c.Equals(t2.a)) || (t1.c.Equals(t2.b)) || (t1.c.Equals(t2.c)))
                return true;
            return false;
        }

        public static int sortCouples(Tuple<int, int, int> i, Tuple<int, int, int> j)
        {
            return i.Item1.CompareTo(j.Item1);
        }

        public static Rectangle[] DoHybridERGroupingNM(InputArray _img, VectorOfMat src, List<ERStat>[] regions, out List<List<Tuple<int, int>>> groups, out List<Rectangle> boxes, bool doFeedbackLoop)
        {
            if (src.GetInputArray().IsEmpty())
                throw new Exception("Exception at ERGROUPINGNM, src is empty");

            int numChannels = src.Size;

            Mat img = _img.GetMat();
            groups = new List<List<Tuple<int, int>>>();
            boxes = new List<Rectangle>();
            for (int c = 0; c < numChannels; c++)
            {
                //Debug.WriteLine("Processing channel {0}", c);
                // store indices to regions in a single list
                List<Tuple<int, int>> allRegions = new List<Tuple<int, int>>();
                for (int i = 0; i < regions[c].Count; i++)
                {
                    allRegions.Add(Tuple.Create(c, i));
                }

                List<RegionPair> validPairs = new List<RegionPair>();
                Mat mask = new Mat(img.Rows + 2, img.Cols + 2, DepthType.Cv8U, 1);
                mask.SetTo(new MCvScalar(0));

                Mat grey = new Mat(), lab = new Mat();
                CvInvoke.CvtColor(img, lab, ColorConversion.Rgb2Lab);
                CvInvoke.CvtColor(img, grey, ColorConversion.Rgb2Gray);

                for (int i = 0; i < allRegions.Count; i++)
                {
                    List<int> iSiblings = new List<int>();
                    int firstISiblingIdx = validPairs.Count;

                    for (int j = i + 1; j < allRegions.Count; j++)
                    {
                        if (isValidPair(grey, lab, mask, src, regions, allRegions[i], allRegions[j]))
                        {
                            bool isCycle = false;
                            for (int k = 0; k < iSiblings.Count; k++)
                            {
                                if (isValidPair(grey, lab, mask, src, regions, allRegions[j], allRegions[iSiblings[k]]))
                                {
                                    MCvPoint2D64f iCenter = new MCvPoint2D64f(regions[allRegions[i].Item1][allRegions[i].Item2].rect.X +
                                                              regions[allRegions[i].Item1][allRegions[i].Item2].rect.Width / 2,
                                                              regions[allRegions[i].Item1][allRegions[i].Item2].rect.Y +
                                                              regions[allRegions[i].Item1][allRegions[i].Item2].rect.Height / 2);
                                    MCvPoint2D64f jCenter = new MCvPoint2D64f(regions[allRegions[j].Item1][allRegions[j].Item2].rect.X +
                                                              regions[allRegions[j].Item1][allRegions[j].Item2].rect.Width / 2,
                                                              regions[allRegions[j].Item1][allRegions[j].Item2].rect.Y +
                                                              regions[allRegions[j].Item1][allRegions[j].Item2].rect.Height / 2);
                                    MCvPoint2D64f kCenter = new MCvPoint2D64f(regions[allRegions[iSiblings[k]].Item1][allRegions[iSiblings[k]].Item2].rect.X +
                                                              regions[allRegions[iSiblings[k]].Item1][allRegions[iSiblings[k]].Item2].rect.Width / 2,
                                                              regions[allRegions[iSiblings[k]].Item1][allRegions[iSiblings[k]].Item2].rect.Y +
                                                              regions[allRegions[iSiblings[k]].Item1][allRegions[iSiblings[k]].Item2].rect.Height / 2);
                                    Matrix<double> forNorm = new Matrix<double>(1, 2);
                                    MCvPoint2D64f p1 = iCenter - jCenter;
                                    forNorm[0, 0] = p1.X;
                                    forNorm[0, 1] = p1.Y;
                                    double norm1 = CvInvoke.Norm(forNorm);
                                    p1 = iCenter - kCenter;
                                    forNorm[0, 0] = p1.X;
                                    forNorm[0, 1] = p1.Y;
                                    double norm2 = CvInvoke.Norm(forNorm);
                                    if (norm1 < norm2)
                                    {
                                        // to pair [i,j] einai metagenestero apo to pair [i,k] g auto kanei mono enan elegxo,
                                        // alliws diathrei auto poy exei. epishs krataei to neo pair sta isibling se periptwsh
                                        // poy xreiastei na to antikatasthsei me kapoio akoma kontunotero. me ton tropo auto
                                        // to pio apomakrusmeno pair (k) xanetai kai den thewreitai pleon ws valid pair toy i.
                                        validPairs[firstISiblingIdx + k] = new RegionPair(allRegions[i], allRegions[j]);
                                        iSiblings[k] = j;
                                    }
                                    isCycle = true;
                                    break;
                                }
                            }
                            if (!isCycle)
                            {
                                validPairs.Add(new RegionPair(allRegions[i], allRegions[j]));
                                iSiblings.Add(j);
                            }
                        }
                    }
                }

                List<RegionTriplet> validTriplets = new List<RegionTriplet>();
                for (int i = 0; i < validPairs.Count; i++)
                {
                    for (int j = i + 1; j < validPairs.Count; j++)
                    {
                        RegionTriplet validTriplet = new RegionTriplet(Tuple.Create(0, 0), Tuple.Create(0, 0), Tuple.Create(0, 0));
                        if (isValidTriplet(regions, validPairs[i], validPairs[j], validTriplet))
                        {
                            validTriplets.Add(validTriplet);
                        }
                    }
                }

                List<RegionSequence> validSequences = new List<RegionSequence>();
                List<RegionSequence> pendingSequences = new List<RegionSequence>();

                for (int i = 0; i < validTriplets.Count; i++)
                {
                    pendingSequences.Add(new RegionSequence(validTriplets[i]));
                }

                for (int i = 0; i < pendingSequences.Count; i++)
                {
                    bool expanded = false;
                    for (int j = i + 1; j < pendingSequences.Count; j++)
                    {
                        if (isValidSequence(pendingSequences[i], pendingSequences[j]))
                        {
                            expanded = true;
                            pendingSequences[i].triplets.InsertRange(0, pendingSequences[j].triplets);
                            pendingSequences.RemoveAt(j);
                            j--;
                        }
                    }
                    if (expanded)
                    {
                        validSequences.Add(pendingSequences[i]);
                    }
                }

                for (int i = 0; i < validSequences.Count; i++)
                {
                    for (int j = i + 1; j < validSequences.Count; j++)
                    {
                        if (haveCommonRegion(validSequences[i], validSequences[j]))
                        {
                            if (validSequences[i].triplets.Count < validSequences[j].triplets.Count)
                            {
                                validSequences.RemoveAt(i);
                                i--;
                                break;
                            }
                            else
                            {
                                validSequences.RemoveAt(j);
                                j--;
                            }
                        }
                    }
                }
                
                if (doFeedbackLoop)
                {
                    ERFilterNM1 er1 = new ERFilterNM1(Constants.CLASSIFIER1PATH, 1, 0.005f, 0.3f, 0f, false, 0f);
                    //MyERFilter erFilter = MyERFilterNM.createERFilterNM1(MyERFilterNM.loadDummyClassifier(),
                    //                                                1,
                    //                                                0.005f,
                    //                                                0.3f,
                    //                                                0f,
                    //                                                false,
                    //                                                0.000001f);
                    for (int i = 0; i < validSequences.Count; i++)
                    {
                        List<Point> bboxPoints = new List<Point>();
                        Rectangle rect;
                        for (int j = 0; j < validSequences[i].triplets.Count; j++)
                        {
                            rect = regions[validSequences[i].triplets[j].a.Item1][validSequences[i].triplets[j].a.Item2].rect;
                            bboxPoints.Add(rect.Location);
                            bboxPoints.Add(new Point(rect.Right, rect.Bottom));
                            rect = regions[validSequences[i].triplets[j].b.Item1][validSequences[i].triplets[j].b.Item2].rect;
                            bboxPoints.Add(rect.Location);
                            bboxPoints.Add(new Point(rect.Right, rect.Bottom));
                            rect = regions[validSequences[i].triplets[j].c.Item1][validSequences[i].triplets[j].c.Item2].rect;
                            bboxPoints.Add(rect.Location);
                            bboxPoints.Add(new Point(rect.Right, rect.Bottom));
                        }

                        rect = CvInvoke.BoundingRectangle(new VectorOfPoint(bboxPoints.ToArray()));
                        rect.X = Math.Max(rect.X - 10, 0);
                        rect.Y = Math.Max(rect.Y - 10, 0);
                        rect.Width = Math.Min(rect.Width + 20, src[c].Cols - rect.X);
                        rect.Height = Math.Min(rect.Height + 20, src[c].Rows - rect.Y);

                        List<ERStat> auxRegions = new List<ERStat>();
                        VectorOfERStat aux = new VectorOfERStat();
                        Mat tmp = new Mat(src[c], rect);
                        er1.Run(tmp, aux);

                        for (int j = 0; j < aux.Size; j++)
                        {
                            auxRegions.Add(ERStat.TransformMCvERStat(aux[j]));
                        }


                        for (int r = 0; r < auxRegions.Count; r++)
                        {
                            if ((auxRegions[r].rect.Y == 0) || (auxRegions[r].rect.Bottom >= tmp.Rows))
                                continue;

                            auxRegions[r].rect = new Rectangle(auxRegions[r].rect.X + rect.X, auxRegions[r].rect.Y + rect.Y, auxRegions[r].rect.Width, auxRegions[r].rect.Height);
                            auxRegions[r].pixel = ((auxRegions[r].pixel / tmp.Cols) + rect.Y) * src[c].Cols + (auxRegions[r].pixel % tmp.Cols) + rect.X;
    
                            bool overlaps = false;
                            for (int j = 0; j < validSequences[i].triplets.Count; j++)
                            {
                                Rectangle minAreaRectA = Rectangle.Union(regions[validSequences[i].triplets[j].a.Item1][validSequences[i].triplets[j].a.Item2].rect, auxRegions[r].rect);
                                Rectangle minAreaRectB = Rectangle.Union(regions[validSequences[i].triplets[j].b.Item1][validSequences[i].triplets[j].b.Item2].rect, auxRegions[r].rect);
                                Rectangle minAreaRectC = Rectangle.Union(regions[validSequences[i].triplets[j].c.Item1][validSequences[i].triplets[j].c.Item2].rect, auxRegions[r].rect);

                                if ((minAreaRectA == auxRegions[r].rect) ||
                                    (minAreaRectB == auxRegions[r].rect) ||
                                    (minAreaRectC == auxRegions[r].rect) ||
                                    (minAreaRectA == regions[validSequences[i].triplets[j].a.Item1][validSequences[i].triplets[j].a.Item2].rect) ||
                                    (minAreaRectB == regions[validSequences[i].triplets[j].b.Item1][validSequences[i].triplets[j].b.Item2].rect) ||
                                    (minAreaRectC == regions[validSequences[i].triplets[j].c.Item1][validSequences[i].triplets[j].c.Item2].rect))
                                {
                                    overlaps = true;
                                    break;
                                }
                            }

                            if (!overlaps)
                            {
                                List<Tuple<int, int, int>> leftCouples = new List<Tuple<int, int, int>>(), rightCouples = new List<Tuple<int, int, int>>();
                                regions[c].Add(auxRegions[r]);
                                for (int j = 0; j < validSequences[i].triplets.Count; j++)
                                {
                                    if (isValidPair(grey, lab, mask, src, regions, validSequences[i].triplets[j].a, Tuple.Create(c, regions[c].Count - 1)))
                                    {
                                        if (regions[validSequences[i].triplets[j].a.Item1][validSequences[i].triplets[j].a.Item2].rect.X > auxRegions[r].rect.X)
                                            rightCouples.Add(Tuple.Create(regions[validSequences[i].triplets[j].a.Item1][validSequences[i].triplets[j].a.Item2].rect.X - auxRegions[r].rect.X, validSequences[i].triplets[j].a.Item1, validSequences[i].triplets[j].a.Item2));
                                        else
                                            leftCouples.Add(Tuple.Create(auxRegions[r].rect.X - regions[validSequences[i].triplets[j].a.Item1][validSequences[i].triplets[j].a.Item2].rect.X, validSequences[i].triplets[j].a.Item1, validSequences[i].triplets[j].a.Item2));
                                    }
                                    if (isValidPair(grey, lab, mask, src, regions, validSequences[i].triplets[j].b, Tuple.Create(c, regions[c].Count - 1)))
                                    {
                                        if (regions[validSequences[i].triplets[j].b.Item1][validSequences[i].triplets[j].b.Item2].rect.X > auxRegions[r].rect.X)
                                            rightCouples.Add(Tuple.Create(regions[validSequences[i].triplets[j].b.Item1][validSequences[i].triplets[j].b.Item2].rect.X - auxRegions[r].rect.X, validSequences[i].triplets[j].b.Item1, validSequences[i].triplets[j].b.Item2));
                                        else
                                            leftCouples.Add(Tuple.Create(auxRegions[r].rect.X - regions[validSequences[i].triplets[j].b.Item1][validSequences[i].triplets[j].b.Item2].rect.X, validSequences[i].triplets[j].b.Item1, validSequences[i].triplets[j].b.Item2));
                                    }
                                    if (isValidPair(grey, lab, mask, src, regions, validSequences[i].triplets[j].c, Tuple.Create(c, regions[c].Count - 1)))
                                    {
                                        if (regions[validSequences[i].triplets[j].c.Item1][validSequences[i].triplets[j].c.Item2].rect.X > auxRegions[r].rect.X)
                                            rightCouples.Add(Tuple.Create(regions[validSequences[i].triplets[j].c.Item1][validSequences[i].triplets[j].c.Item2].rect.X - auxRegions[r].rect.X, validSequences[i].triplets[j].c.Item1, validSequences[i].triplets[j].c.Item2));
                                        else
                                            leftCouples.Add(Tuple.Create(auxRegions[r].rect.X - regions[validSequences[i].triplets[j].c.Item1][validSequences[i].triplets[j].c.Item2].rect.X, validSequences[i].triplets[j].c.Item1, validSequences[i].triplets[j].c.Item2));
                                    }
                                }

                                // make the new ER part of a triplet and check if line estimates is consistent with the sequence
                                List<RegionTriplet> newValidTriplets = new List<RegionTriplet>();
                                if (leftCouples.Count != 0 && rightCouples.Count != 0)
                                {
                                    leftCouples.Sort(sortCouples);
                                    rightCouples.Sort(sortCouples);
                                    RegionPair pair1 = new RegionPair(Tuple.Create(leftCouples[0].Item2, leftCouples[0].Item3), Tuple.Create(c, regions[c].Count - 1));
                                    RegionPair pair2 = new RegionPair(Tuple.Create(c, regions[c].Count - 1), Tuple.Create(rightCouples[0].Item2, rightCouples[0].Item3));
                                    RegionTriplet triplet = new RegionTriplet(Tuple.Create(0, 0), Tuple.Create(0, 0), Tuple.Create(0, 0));
                                    if (isValidTriplet(regions, pair1, pair2, triplet))
                                    {
                                        newValidTriplets.Add(triplet);
                                    }
                                }
                                else if (rightCouples.Count >= 2)
                                {
                                    rightCouples.Sort(sortCouples);
                                    RegionPair pair1 = new RegionPair(Tuple.Create(c, regions[c].Count - 1), Tuple.Create(rightCouples[0].Item2, rightCouples[0].Item3));
                                    RegionPair pair2 = new RegionPair(Tuple.Create(rightCouples[0].Item2, rightCouples[0].Item3), Tuple.Create(rightCouples[1].Item2, rightCouples[1].Item3));
                                    RegionTriplet triplet = new RegionTriplet(Tuple.Create(0, 0), Tuple.Create(0, 0), Tuple.Create(0, 0));
                                    if (isValidTriplet(regions, pair1, pair2, triplet))
                                    {
                                        newValidTriplets.Add(triplet);
                                    }
                                }
                                else if (leftCouples.Count >= 2)
                                {
                                    leftCouples.Sort(sortCouples);
                                    RegionPair pair1 = new RegionPair(Tuple.Create(leftCouples[1].Item2, leftCouples[1].Item3), Tuple.Create(leftCouples[0].Item2, leftCouples[0].Item3));
                                    RegionPair pair2 = new RegionPair(Tuple.Create(leftCouples[0].Item2, leftCouples[0].Item3), Tuple.Create(c, regions[c].Count - 1));
                                    RegionTriplet triplet = new RegionTriplet(Tuple.Create(0, 0), Tuple.Create(0, 0), Tuple.Create(0, 0));
                                    if (isValidTriplet(regions, pair1, pair2, triplet))
                                    {
                                        newValidTriplets.Add(triplet);
                                    }
                                }
                                else
                                {
                                    // no possible triplets found
                                    continue;
                                }

                                // check if line estimates is consistent with the sequence
                                for (int t = 0; t < newValidTriplets.Count; t++)
                                {
                                    RegionSequence sequence = new RegionSequence(newValidTriplets[t]);
                                    if (isValidSequence(validSequences[i], sequence))
                                    {
                                        validSequences[i].triplets.Add(newValidTriplets[t]);
                                    }
                                }
                            }
                        }
                    }
                }
                // prepare the sequences for output
                for (int i = 0; i < validSequences.Count; i++)
                {
                    List<Point> bboxPoints = new List<Point>();
                    List<Tuple<int, int>> groupRegions = new List<Tuple<int, int>>();

                    for (int j = 0; j < validSequences[i].triplets.Count; j++)
                    {
                        int prevSize = groupRegions.Count;
                        if (!groupRegions.Exists(x => x == validSequences[i].triplets[j].a)) // shmainei x tetoio wste x == me th malakia poy psaxnw na vrw
                            groupRegions.Add(validSequences[i].triplets[j].a);
                        if (!groupRegions.Exists(x => x == validSequences[i].triplets[j].b))
                            groupRegions.Add(validSequences[i].triplets[j].b);
                        if (!groupRegions.Exists(x => x == validSequences[i].triplets[j].c))
                            groupRegions.Add(validSequences[i].triplets[j].c);

                        for (int k = prevSize; k < groupRegions.Count; k++)
                        {
                            Rectangle temp = regions[groupRegions[k].Item1][groupRegions[k].Item2].rect;
                            bboxPoints.Add(temp.Location);
                            bboxPoints.Add(new Point(temp.Right, temp.Bottom));
                        }
                    }

                    groups.Add(groupRegions);
                    boxes.Add(CvInvoke.BoundingRectangle(new VectorOfPoint(bboxPoints.ToArray())));
                }
            } // end for all the channels
            return boxes.ToArray();
        }

        public static Rectangle[] DoERGroupingNM(InputArray _img, VectorOfMat src, List<ERStat>[] regions, out List<List<Tuple<int, int>>> groups, out List<Rectangle> boxes, bool doFeedbackLoop)
        {
            if (src.GetInputArray().IsEmpty())
                throw new Exception("Exception at ERGROUPINGNM, src is empty");

            int numChannels = src.Size;

            Mat img = _img.GetMat();
            groups = new List<List<Tuple<int, int>>>();
            boxes = new List<Rectangle>();
            for (int c = 0; c < numChannels; c++)
            {
                //Debug.WriteLine("Processing channel {0}",c);
                // store indices to regions in a single list
                List<Tuple<int, int>> allRegions = new List<Tuple<int, int>>();
                for (int i = 0; i < regions[c].Count; i++)
                {
                    allRegions.Add(Tuple.Create(c, i));
                }

                List<RegionPair> validPairs = new List<RegionPair>();
                Mat mask = new Mat(img.Rows + 2, img.Cols + 2, DepthType.Cv8U, 1);
                mask.SetTo(new MCvScalar(0));

                Mat grey = new Mat(), lab = new Mat();
                //CvInvoke.CvtColor(img, lab, ColorConversion.Rgb2Lab);
                //CvInvoke.CvtColor(img, grey, ColorConversion.Rgb2Gray);
                CvInvoke.CvtColor(img, lab, ColorConversion.Bgr2Lab);
                CvInvoke.CvtColor(img, grey, ColorConversion.Bgr2Gray);

                for (int i = 0; i < allRegions.Count; i++)
                {
                    List<int> iSiblings = new List<int>();
                    int firstISiblingIdx = validPairs.Count;
                    for (int j = i + 1; j < allRegions.Count; j++)
                    {
                        if (isValidPair(grey, lab, mask, src, regions, allRegions[i], allRegions[j]))
                        {
                            bool isCycle = false;
                            for (int k = 0; k < iSiblings.Count; k++)
                            {
                                if (isValidPair(grey, lab, mask, src, regions, allRegions[j], allRegions[iSiblings[k]]))
                                {
                                    Point i_Center = new Point(regions[allRegions[i].Item1][allRegions[i].Item2].rect.X +
                                                              regions[allRegions[i].Item1][allRegions[i].Item2].rect.Width / 2,
                                                              regions[allRegions[i].Item1][allRegions[i].Item2].rect.Y +
                                                              regions[allRegions[i].Item1][allRegions[i].Item2].rect.Height / 2);
                                    Point j_Center = new Point(regions[allRegions[j].Item1][allRegions[j].Item2].rect.X +
                                                              regions[allRegions[j].Item1][allRegions[j].Item2].rect.Width / 2,
                                                              regions[allRegions[j].Item1][allRegions[j].Item2].rect.Y +
                                                              regions[allRegions[j].Item1][allRegions[j].Item2].rect.Height / 2);
                                    Point k_Center = new Point(regions[allRegions[iSiblings[k]].Item1][allRegions[iSiblings[k]].Item2].rect.X + regions[allRegions[iSiblings[k]].Item1][allRegions[iSiblings[k]].Item2].rect.Width / 2, regions[allRegions[iSiblings[k]].Item1][allRegions[iSiblings[k]].Item2].rect.Y + regions[allRegions[iSiblings[k]].Item1][allRegions[iSiblings[k]].Item2].rect.Height / 2);

                                    Matrix<double> forNorm = new Matrix<double>(1,2);
                                    //MCvPoint2D64f p1 = iCenter - jCenter;
                                    Point p1 = new Point(i_Center.X - j_Center.X, i_Center.Y - j_Center.Y);
                                    forNorm[0,0] = p1.X;
                                    forNorm[0,1] = p1.Y;
                                    double norm1 = CvInvoke.Norm(forNorm);
                                    //p1 = iCenter - kCenter;
                                    p1 = new Point(i_Center.X - k_Center.X, i_Center.Y - k_Center.Y);
                                    forNorm[0,0] = p1.X;
                                    forNorm[0,1] = p1.Y;
                                    double norm2 = CvInvoke.Norm(forNorm);
                                    if (norm1 < norm2)
                                    {
                                        // to pair [i,j] einai metagenestero apo to pair [i,k] g auto kanei mono enan elegxo,
                                        // alliws diathrei auto poy exei. epishs krataei to neo pair sta isibling se periptwsh
                                        // poy xreiastei na to antikatasthsei me kapoio akoma kontunotero. me ton tropo auto
                                        // to pio apomakrusmeno pair (k) xanetai kai den thewreitai pleon ws valid pair toy i.
                                        validPairs[firstISiblingIdx + k] = new RegionPair(allRegions[i], allRegions[j]);
                                        iSiblings[k] = j;
                                    }
                                    isCycle = true;
                                    break;
                                }
                            }
                            if (!isCycle)
                            {
                                validPairs.Add(new RegionPair(allRegions[i], allRegions[j]));
                                iSiblings.Add(j);
                            }
                        }
                    }
                }
                List<RegionTriplet> validTriplets = new List<RegionTriplet>();
                for (int i = 0; i < validPairs.Count; i++){
                    for (int j = i + 1; j < validPairs.Count; j++){
                        RegionTriplet validTriplet = new RegionTriplet(Tuple.Create(0,0),Tuple.Create(0,0),Tuple.Create(0,0));
                        if (isValidTriplet(regions,validPairs[i],validPairs[j], validTriplet)){
                            validTriplets.Add(validTriplet);
                        }
                    }
                }

                List<RegionSequence> validSequences = new List<RegionSequence>();
                List<RegionSequence> pendingSequences = new List<RegionSequence>();

                for (int i = 0; i < validTriplets.Count; i++)
                {
                    pendingSequences.Add(new RegionSequence(validTriplets[i]));
                }

                for (int i = 0; i < pendingSequences.Count; i++)
                {
                    bool expanded = false;
                    for (int j = i + 1; j < pendingSequences.Count; j++)
                    {
                        if (isValidSequence(pendingSequences[i], pendingSequences[j]))
                        {
                            expanded = true;
                            pendingSequences[i].triplets.InsertRange(0, pendingSequences[j].triplets);
                            pendingSequences.RemoveAt(j);
                            j--;
                        }
                    }
                    if (expanded)
                    {
                        validSequences.Add(pendingSequences[i]);
                    }
                }

                for (int i = 0; i < validSequences.Count; i++)
                {
                    for (int j = i + 1; j < validSequences.Count; j++)
                    {
                        if (haveCommonRegion(validSequences[i], validSequences[j]))
                        {
                            if (validSequences[i].triplets.Count < validSequences[j].triplets.Count)
                            {
                                validSequences.RemoveAt(i);
                                i--;
                                break;
                            }
                            else
                            {
                                validSequences.RemoveAt(j);
                                j--;
                            }
                        }
                    }
                }

                if (doFeedbackLoop)
                {
                    MyERFilter erFilter = MyERFilterNM.createERFilterNM1(MyERFilterNM.loadDummyClassifier(), 1, 0.005f, 0.3f, 0, false, 0);
                    //MyERFilter erFilter = MyERFilterNM.createERFilterNM1(MyERFilterNM.loadClassifierNM1(Constants.CLASSIFIER1PATH),
                    //                                                1,
                    //                                                0.005f,
                    //                                                0.3f,
                    //                                                0f,
                    //                                                false,
                    //                                                0.0000000000000001f);
                    for (int i = 0; i < validSequences.Count; i++)
                    {
                        List<Point> bboxPoints = new List<Point>();
                        Rectangle rect;
                        for (int j = 0; j < validSequences[i].triplets.Count; j++)
                        {
                            rect = regions[validSequences[i].triplets[j].a.Item1][validSequences[i].triplets[j].a.Item2].rect;
                            bboxPoints.Add(rect.Location);
                            bboxPoints.Add(new Point(rect.Right, rect.Bottom));
                            rect = regions[validSequences[i].triplets[j].b.Item1][validSequences[i].triplets[j].b.Item2].rect;
                            bboxPoints.Add(rect.Location);
                            bboxPoints.Add(new Point(rect.Right, rect.Bottom));
                            rect = regions[validSequences[i].triplets[j].c.Item1][validSequences[i].triplets[j].c.Item2].rect;
                            bboxPoints.Add(rect.Location);
                            bboxPoints.Add(new Point(rect.Right, rect.Bottom));
                        }

                        rect = CvInvoke.BoundingRectangle(new VectorOfPoint(bboxPoints.ToArray()));
                        rect.X = Math.Max(rect.X - 10, 0);
                        rect.Y = Math.Max(rect.Y - 10, 0);
                        rect.Width = Math.Min(rect.Width + 20, src[c].Cols - rect.X);
                        rect.Height = Math.Min(rect.Height + 20, src[c].Rows - rect.Y);

                        List<ERStat> auxRegions = new List<ERStat>();
                        Mat tmp = new Mat(src[c], rect);
                        erFilter.Run(tmp.GetInputArray(), ref auxRegions);
                        //Rectangle[] rects = new Rectangle[auxRegions.Count];
                        //for (int k = 0; k < auxRegions.Count; k++)
                        //{
                        //    rects[k] = auxRegions[k].rect;
                        //}
                        //return rects;


                        for (int r = 0; r < auxRegions.Count; r++)
                        {
                            if ((auxRegions[r].rect.Y == 0) || (auxRegions[r].rect.Bottom >= tmp.Rows))
                                continue;

                            auxRegions[r].rect = new Rectangle(auxRegions[r].rect.X + rect.X, auxRegions[r].rect.Y + rect.Y, auxRegions[r].rect.Width, auxRegions[r].rect.Height);
                            auxRegions[r].pixel = ((auxRegions[r].pixel / tmp.Cols) + rect.Y) * src[c].Cols + (auxRegions[r].pixel % tmp.Cols) + rect.X;

                            bool overlaps = false;
                            for (int j = 0; j < validSequences[i].triplets.Count; j++)
                            {
                                Rectangle minAreaRectA = Rectangle.Union(regions[validSequences[i].triplets[j].a.Item1][validSequences[i].triplets[j].a.Item2].rect, auxRegions[r].rect);
                                Rectangle minAreaRectB = Rectangle.Union(regions[validSequences[i].triplets[j].b.Item1][validSequences[i].triplets[j].b.Item2].rect, auxRegions[r].rect);
                                Rectangle minAreaRectC = Rectangle.Union(regions[validSequences[i].triplets[j].c.Item1][validSequences[i].triplets[j].c.Item2].rect, auxRegions[r].rect);

                                if ((minAreaRectA == auxRegions[r].rect) ||
                                    (minAreaRectB == auxRegions[r].rect) ||
                                    (minAreaRectC == auxRegions[r].rect) ||
                                    (minAreaRectA == regions[validSequences[i].triplets[j].a.Item1][validSequences[i].triplets[j].a.Item2].rect) ||
                                    (minAreaRectB == regions[validSequences[i].triplets[j].b.Item1][validSequences[i].triplets[j].b.Item2].rect) ||
                                    (minAreaRectC == regions[validSequences[i].triplets[j].c.Item1][validSequences[i].triplets[j].c.Item2].rect))
                                {
                                    overlaps = true;
                                    break;
                                }
                            }

                            if (!overlaps)
                            {
                                List<Tuple<int, int, int>> leftCouples = new List<Tuple<int, int, int>>(), rightCouples = new List<Tuple<int, int, int>>();
                                regions[c].Add(auxRegions[r]);
                                for (int j = 0; j < validSequences[i].triplets.Count; j++)
                                {
                                    if (isValidPair(grey, lab, mask, src, regions, validSequences[i].triplets[j].a, Tuple.Create(c, regions[c].Count - 1)))
                                    {
                                        if (regions[validSequences[i].triplets[j].a.Item1][validSequences[i].triplets[j].a.Item2].rect.X > auxRegions[r].rect.X)
                                            rightCouples.Add(Tuple.Create(regions[validSequences[i].triplets[j].a.Item1][validSequences[i].triplets[j].a.Item2].rect.X - auxRegions[r].rect.X, validSequences[i].triplets[j].a.Item1, validSequences[i].triplets[j].a.Item2));
                                        else
                                            leftCouples.Add(Tuple.Create(auxRegions[r].rect.X - regions[validSequences[i].triplets[j].a.Item1][validSequences[i].triplets[j].a.Item2].rect.X, validSequences[i].triplets[j].a.Item1, validSequences[i].triplets[j].a.Item2));
                                    }
                                    if (isValidPair(grey, lab, mask, src, regions, validSequences[i].triplets[j].b, Tuple.Create(c, regions[c].Count - 1)))
                                    {
                                        if (regions[validSequences[i].triplets[j].b.Item1][validSequences[i].triplets[j].b.Item2].rect.X > auxRegions[r].rect.X)
                                            rightCouples.Add(Tuple.Create(regions[validSequences[i].triplets[j].b.Item1][validSequences[i].triplets[j].b.Item2].rect.X - auxRegions[r].rect.X, validSequences[i].triplets[j].b.Item1, validSequences[i].triplets[j].b.Item2));
                                        else
                                            leftCouples.Add(Tuple.Create(auxRegions[r].rect.X - regions[validSequences[i].triplets[j].b.Item1][validSequences[i].triplets[j].b.Item2].rect.X, validSequences[i].triplets[j].b.Item1, validSequences[i].triplets[j].b.Item2));
                                    }
                                    if (isValidPair(grey, lab, mask, src, regions, validSequences[i].triplets[j].c, Tuple.Create(c, regions[c].Count - 1)))
                                    {
                                        if (regions[validSequences[i].triplets[j].c.Item1][validSequences[i].triplets[j].c.Item2].rect.X > auxRegions[r].rect.X)
                                            rightCouples.Add(Tuple.Create(regions[validSequences[i].triplets[j].c.Item1][validSequences[i].triplets[j].c.Item2].rect.X - auxRegions[r].rect.X, validSequences[i].triplets[j].c.Item1, validSequences[i].triplets[j].c.Item2));
                                        else
                                            leftCouples.Add(Tuple.Create(auxRegions[r].rect.X - regions[validSequences[i].triplets[j].c.Item1][validSequences[i].triplets[j].c.Item2].rect.X, validSequences[i].triplets[j].c.Item1, validSequences[i].triplets[j].c.Item2));
                                    }
                                }

                                // make the new ER part of a triplet and check if line estimates is consistent with the sequence
                                List<RegionTriplet> newValidTriplets = new List<RegionTriplet>();
                                if (leftCouples.Count != 0 && rightCouples.Count != 0)
                                {
                                    leftCouples.Sort(sortCouples);
                                    rightCouples.Sort(sortCouples);
                                    RegionPair pair1 = new RegionPair(Tuple.Create(leftCouples[0].Item2, leftCouples[0].Item3), Tuple.Create(c, regions[c].Count - 1));
                                    RegionPair pair2 = new RegionPair(Tuple.Create(c, regions[c].Count - 1), Tuple.Create(rightCouples[0].Item2, rightCouples[0].Item3));
                                    RegionTriplet triplet = new RegionTriplet(Tuple.Create(0, 0), Tuple.Create(0, 0), Tuple.Create(0, 0));
                                    if (isValidTriplet(regions, pair1, pair2, triplet))
                                    {
                                        newValidTriplets.Add(triplet);
                                    }
                                }
                                else if (rightCouples.Count >= 2)
                                {
                                    rightCouples.Sort(sortCouples);
                                    RegionPair pair1 = new RegionPair(Tuple.Create(c, regions[c].Count - 1), Tuple.Create(rightCouples[0].Item2, rightCouples[0].Item3));
                                    RegionPair pair2 = new RegionPair(Tuple.Create(rightCouples[0].Item2, rightCouples[0].Item3), Tuple.Create(rightCouples[1].Item2, rightCouples[1].Item3));
                                    RegionTriplet triplet = new RegionTriplet(Tuple.Create(0, 0), Tuple.Create(0, 0), Tuple.Create(0, 0));
                                    if (isValidTriplet(regions, pair1, pair2, triplet))
                                    {
                                        newValidTriplets.Add(triplet);
                                    }
                                }
                                else if (leftCouples.Count >= 2)
                                {
                                    leftCouples.Sort(sortCouples);
                                    RegionPair pair1 = new RegionPair(Tuple.Create(leftCouples[1].Item2, leftCouples[1].Item3), Tuple.Create(leftCouples[0].Item2, leftCouples[0].Item3));
                                    RegionPair pair2 = new RegionPair(Tuple.Create(leftCouples[0].Item2, leftCouples[0].Item3), Tuple.Create(c, regions[c].Count - 1));
                                    RegionTriplet triplet = new RegionTriplet(Tuple.Create(0, 0), Tuple.Create(0, 0), Tuple.Create(0, 0));
                                    if (isValidTriplet(regions, pair1, pair2, triplet))
                                    {
                                        newValidTriplets.Add(triplet);
                                    }
                                }
                                else
                                {
                                    // no possible triplets found
                                    continue;
                                }

                                // check if line estimates is consistent with the sequence
                                for (int t = 0; t < newValidTriplets.Count; t++)
                                {
                                    RegionSequence sequence = new RegionSequence(newValidTriplets[t]);
                                    if (isValidSequence(validSequences[i], sequence))
                                    {
                                        validSequences[i].triplets.Add(newValidTriplets[t]);
                                    }                                    
                                }
                            }
                        }
                    }
                }
                // prepare the sequences for output
                for (int i = 0; i < validSequences.Count; i++)
                {
                    List<Point> bboxPoints = new List<Point>();
                    List<Tuple<int, int>> groupRegions = new List<Tuple<int, int>>();

                    for (int j = 0; j < validSequences[i].triplets.Count; j++)
                    {
                        int prevSize = groupRegions.Count; // epeidh to region mporei na yparxei polles fores elegxei na dei an to exei prosthesei hdh sto groupregions.
                        if (!groupRegions.Exists(x => x == validSequences[i].triplets[j].a)) // shmainei x tetoio wste x == me th malakia poy psaxnw na vrw
                            groupRegions.Add(validSequences[i].triplets[j].a);
                        if (!groupRegions.Exists(x => x == validSequences[i].triplets[j].b))
                            groupRegions.Add(validSequences[i].triplets[j].b);
                        if (!groupRegions.Exists(x => x == validSequences[i].triplets[j].c))
                            groupRegions.Add(validSequences[i].triplets[j].c);

                        for (int k = prevSize; k < groupRegions.Count; k++)
                        {
                            Rectangle temp = regions[groupRegions[k].Item1][groupRegions[k].Item2].rect;
                            bboxPoints.Add(temp.Location);
                            bboxPoints.Add(new Point(temp.Right, temp.Bottom));
                        }
                    }

                    groups.Add(groupRegions);
                    boxes.Add(CvInvoke.BoundingRectangle(new VectorOfPoint(bboxPoints.ToArray())));
                }
            } // end for all the channels
            return boxes.ToArray();
        }

        public static Rectangle[] DoERGroupingNMModified(InputArray _img, VectorOfMat src, List<ERStat>[] regions, out List<List<Tuple<int, int>>> groups, out List<Rectangle> boxes, bool doFeedbackLoop)
        {
            if (src.GetInputArray().IsEmpty())
                throw new Exception("Exception at ERGROUPINGNM, src is empty");

            int numChannels = src.Size;

            Mat img = _img.GetMat();
            groups = new List<List<Tuple<int, int>>>();
            boxes = new List<Rectangle>();
            for (int c = 0; c < numChannels; c++)
            {
                //Debug.WriteLine("Processing channel {0}",c);
                // store indices to regions in a single list
                List<Tuple<int, int>> allRegions = new List<Tuple<int, int>>();
                for (int i = 0; i < regions[c].Count; i++)
                {
                    allRegions.Add(Tuple.Create(c, i));
                }

                List<RegionPair> validPairs = new List<RegionPair>();
                Mat mask = new Mat(img.Rows + 2, img.Cols + 2, DepthType.Cv8U, 1);
                mask.SetTo(new MCvScalar(0));

                Mat grey = new Mat(), lab = new Mat();
                CvInvoke.CvtColor(img, lab, ColorConversion.Rgb2Lab);
                CvInvoke.CvtColor(img, grey, ColorConversion.Rgb2Gray);
                //CvInvoke.CvtColor(img, lab, ColorConversion.Bgr2Lab);
                //CvInvoke.CvtColor(img, grey, ColorConversion.Bgr2Gray);

                for (int i = 0; i < allRegions.Count; i++)
                {
                    List<int> iSiblings = new List<int>();
                    int firstISiblingIdx = validPairs.Count;
                    for (int j = i + 1; j < allRegions.Count; j++)
                    {
                        if (isValidPair(grey, lab, mask, src, regions, allRegions[i], allRegions[j]))
                        {
                            bool isCycle = false;
                            for (int k = 0; k < iSiblings.Count; k++)
                            {
                                if (isValidPair(grey, lab, mask, src, regions, allRegions[j], allRegions[iSiblings[k]]))
                                {
                                    Point i_Center = new Point(regions[allRegions[i].Item1][allRegions[i].Item2].rect.X +
                                                              regions[allRegions[i].Item1][allRegions[i].Item2].rect.Width / 2,
                                                              regions[allRegions[i].Item1][allRegions[i].Item2].rect.Y +
                                                              regions[allRegions[i].Item1][allRegions[i].Item2].rect.Height / 2);
                                    Point j_Center = new Point(regions[allRegions[j].Item1][allRegions[j].Item2].rect.X +
                                                              regions[allRegions[j].Item1][allRegions[j].Item2].rect.Width / 2,
                                                              regions[allRegions[j].Item1][allRegions[j].Item2].rect.Y +
                                                              regions[allRegions[j].Item1][allRegions[j].Item2].rect.Height / 2);
                                    Point k_Center = new Point(regions[allRegions[iSiblings[k]].Item1][allRegions[iSiblings[k]].Item2].rect.X +
                                                              regions[allRegions[iSiblings[k]].Item1][allRegions[iSiblings[k]].Item2].rect.Width / 2,
                                                              regions[allRegions[iSiblings[k]].Item1][allRegions[iSiblings[k]].Item2].rect.Y +
                                                              regions[allRegions[iSiblings[k]].Item1][allRegions[iSiblings[k]].Item2].rect.Height / 2);                                  
                                    Matrix<double> forNorm = new Matrix<double>(1, 2);
                                    Point p1 = new Point(i_Center.X - j_Center.X, i_Center.Y - j_Center.Y);
                                    forNorm[0, 0] = p1.X;
                                    forNorm[0, 1] = p1.Y;
                                    double norm1 = CvInvoke.Norm(forNorm);
                                    p1 = new Point(i_Center.X - k_Center.X, i_Center.Y - k_Center.Y);
                                    forNorm[0, 0] = p1.X;
                                    forNorm[0, 1] = p1.Y;
                                    double norm2 = CvInvoke.Norm(forNorm);
                                    if (norm1 < norm2)
                                    {
                                        // to pair [i,j] einai metagenestero apo to pair [i,k] g auto kanei mono enan elegxo,
                                        // alliws diathrei auto poy exei. epishs krataei to neo pair sta isibling se periptwsh
                                        // poy xreiastei na to antikatasthsei me kapoio akoma kontunotero. me ton tropo auto
                                        // to pio apomakrusmeno pair (k) xanetai kai den thewreitai pleon ws valid pair toy i.
                                        validPairs[firstISiblingIdx + k] = new RegionPair(allRegions[i], allRegions[j]);
                                        iSiblings[k] = j;
                                    }
                                    isCycle = true;
                                    break;
                                }
                            }
                            if (!isCycle)
                            {
                                validPairs.Add(new RegionPair(allRegions[i], allRegions[j]));
                                iSiblings.Add(j);
                            }
                        }
                    }
                }
                List<RegionTriplet> validTriplets = new List<RegionTriplet>();
                for (int i = 0; i < validPairs.Count; i++)
                {
                    for (int j = i + 1; j < validPairs.Count; j++)
                    {
                        RegionTriplet validTriplet = new RegionTriplet(Tuple.Create(0, 0), Tuple.Create(0, 0), Tuple.Create(0, 0));
                        if (isValidTriplet(regions, validPairs[i], validPairs[j], validTriplet))
                        {
                            validTriplets.Add(validTriplet);
                        }
                    }
                }

                List<RegionSequence> validSequences = new List<RegionSequence>();
                List<RegionSequence> pendingSequences = new List<RegionSequence>();

                for (int i = 0; i < validTriplets.Count; i++)
                {
                    pendingSequences.Add(new RegionSequence(validTriplets[i]));
                }

                for (int i = 0; i < pendingSequences.Count; i++)
                {
                    bool expanded = false;
                    for (int j = i + 1; j < pendingSequences.Count; j++)
                    {
                        if (isValidSequenceStrict(pendingSequences[i], pendingSequences[j]))
                        {
                            expanded = true;
                            pendingSequences[i].triplets.InsertRange(0, pendingSequences[j].triplets);
                            pendingSequences.RemoveAt(j);
                            j--;
                        }
                    }
                    if (expanded)
                    {
                        validSequences.Add(pendingSequences[i]);
                    }
                }

                for (int i = 0; i < validSequences.Count; i++)
                {
                    for (int j = i + 1; j < validSequences.Count; j++)
                    {
                        if (haveCommonRegion(validSequences[i], validSequences[j]))
                        {
                            if (validSequences[i].triplets.Count < validSequences[j].triplets.Count)
                            {
                                validSequences.RemoveAt(i);
                                i--;
                                break;
                            }
                            else
                            {
                                validSequences.RemoveAt(j);
                                j--;
                            }
                        }
                    }
                }

                if (doFeedbackLoop)
                {
                    MyERFilter erFilter = MyERFilterNM.createERFilterNM1(MyERFilterNM.loadDummyClassifier(), 1, 0.005f, 0.3f, 0, false, 0);
                    //MyERFilter erFilter = MyERFilterNM.createERFilterNM1(MyERFilterNM.loadClassifierNM1(Constants.CLASSIFIER1PATH),
                    //                                                1,
                    //                                                0.005f,
                    //                                                0.3f,
                    //                                                0f,
                    //                                                false,
                    //                                                0.0000000000000001f);
                    for (int i = 0; i < validSequences.Count; i++)
                    {
                        List<Point> bboxPoints = new List<Point>();
                        Rectangle rect;
                        for (int j = 0; j < validSequences[i].triplets.Count; j++)
                        {
                            rect = regions[validSequences[i].triplets[j].a.Item1][validSequences[i].triplets[j].a.Item2].rect;
                            bboxPoints.Add(rect.Location);
                            bboxPoints.Add(new Point(rect.Right, rect.Bottom));
                            rect = regions[validSequences[i].triplets[j].b.Item1][validSequences[i].triplets[j].b.Item2].rect;
                            bboxPoints.Add(rect.Location);
                            bboxPoints.Add(new Point(rect.Right, rect.Bottom));
                            rect = regions[validSequences[i].triplets[j].c.Item1][validSequences[i].triplets[j].c.Item2].rect;
                            bboxPoints.Add(rect.Location);
                            bboxPoints.Add(new Point(rect.Right, rect.Bottom));
                        }

                        rect = CvInvoke.BoundingRectangle(new VectorOfPoint(bboxPoints.ToArray()));
                        rect.X = Math.Max(rect.X - 10, 0);
                        rect.Y = Math.Max(rect.Y - 10, 0);
                        rect.Width = Math.Min(rect.Width + 20, src[c].Cols - rect.X);
                        rect.Height = Math.Min(rect.Height + 20, src[c].Rows - rect.Y);

                        List<ERStat> auxRegions = new List<ERStat>();
                        Mat tmp = new Mat(src[c], rect);
                        erFilter.Run(tmp.GetInputArray(), ref auxRegions);
                        //Rectangle[] rects = new Rectangle[auxRegions.Count];
                        //for (int k = 0; k < auxRegions.Count; k++)
                        //{
                        //    rects[k] = auxRegions[k].rect;
                        //}
                        //return rects;


                        for (int r = 0; r < auxRegions.Count; r++)
                        {
                            if ((auxRegions[r].rect.Y == 0) || (auxRegions[r].rect.Bottom >= tmp.Rows))
                                continue;

                            auxRegions[r].rect = new Rectangle(auxRegions[r].rect.X + rect.X, auxRegions[r].rect.Y + rect.Y, auxRegions[r].rect.Width, auxRegions[r].rect.Height);
                            auxRegions[r].pixel = ((auxRegions[r].pixel / tmp.Cols) + rect.Y) * src[c].Cols + (auxRegions[r].pixel % tmp.Cols) + rect.X;

                            bool overlaps = false;
                            for (int j = 0; j < validSequences[i].triplets.Count; j++)
                            {
                                Rectangle minAreaRectA = Rectangle.Union(regions[validSequences[i].triplets[j].a.Item1][validSequences[i].triplets[j].a.Item2].rect, auxRegions[r].rect);
                                Rectangle minAreaRectB = Rectangle.Union(regions[validSequences[i].triplets[j].b.Item1][validSequences[i].triplets[j].b.Item2].rect, auxRegions[r].rect);
                                Rectangle minAreaRectC = Rectangle.Union(regions[validSequences[i].triplets[j].c.Item1][validSequences[i].triplets[j].c.Item2].rect, auxRegions[r].rect);

                                if ((minAreaRectA == auxRegions[r].rect) ||
                                    (minAreaRectB == auxRegions[r].rect) ||
                                    (minAreaRectC == auxRegions[r].rect) ||
                                    (minAreaRectA == regions[validSequences[i].triplets[j].a.Item1][validSequences[i].triplets[j].a.Item2].rect) ||
                                    (minAreaRectB == regions[validSequences[i].triplets[j].b.Item1][validSequences[i].triplets[j].b.Item2].rect) ||
                                    (minAreaRectC == regions[validSequences[i].triplets[j].c.Item1][validSequences[i].triplets[j].c.Item2].rect))
                                {
                                    overlaps = true;
                                    break;
                                }
                            }

                            if (!overlaps)
                            {
                                List<Tuple<int, int, int>> leftCouples = new List<Tuple<int, int, int>>(), rightCouples = new List<Tuple<int, int, int>>();
                                regions[c].Add(auxRegions[r]);
                                for (int j = 0; j < validSequences[i].triplets.Count; j++)
                                {
                                    if (isValidPair(grey, lab, mask, src, regions, validSequences[i].triplets[j].a, Tuple.Create(c, regions[c].Count - 1)))
                                    {
                                        if (regions[validSequences[i].triplets[j].a.Item1][validSequences[i].triplets[j].a.Item2].rect.X > auxRegions[r].rect.X)
                                            rightCouples.Add(Tuple.Create(regions[validSequences[i].triplets[j].a.Item1][validSequences[i].triplets[j].a.Item2].rect.X - auxRegions[r].rect.X, validSequences[i].triplets[j].a.Item1, validSequences[i].triplets[j].a.Item2));
                                        else
                                            leftCouples.Add(Tuple.Create(auxRegions[r].rect.X - regions[validSequences[i].triplets[j].a.Item1][validSequences[i].triplets[j].a.Item2].rect.X, validSequences[i].triplets[j].a.Item1, validSequences[i].triplets[j].a.Item2));
                                    }
                                    if (isValidPair(grey, lab, mask, src, regions, validSequences[i].triplets[j].b, Tuple.Create(c, regions[c].Count - 1)))
                                    {
                                        if (regions[validSequences[i].triplets[j].b.Item1][validSequences[i].triplets[j].b.Item2].rect.X > auxRegions[r].rect.X)
                                            rightCouples.Add(Tuple.Create(regions[validSequences[i].triplets[j].b.Item1][validSequences[i].triplets[j].b.Item2].rect.X - auxRegions[r].rect.X, validSequences[i].triplets[j].b.Item1, validSequences[i].triplets[j].b.Item2));
                                        else
                                            leftCouples.Add(Tuple.Create(auxRegions[r].rect.X - regions[validSequences[i].triplets[j].b.Item1][validSequences[i].triplets[j].b.Item2].rect.X, validSequences[i].triplets[j].b.Item1, validSequences[i].triplets[j].b.Item2));
                                    }
                                    if (isValidPair(grey, lab, mask, src, regions, validSequences[i].triplets[j].c, Tuple.Create(c, regions[c].Count - 1)))
                                    {
                                        if (regions[validSequences[i].triplets[j].c.Item1][validSequences[i].triplets[j].c.Item2].rect.X > auxRegions[r].rect.X)
                                            rightCouples.Add(Tuple.Create(regions[validSequences[i].triplets[j].c.Item1][validSequences[i].triplets[j].c.Item2].rect.X - auxRegions[r].rect.X, validSequences[i].triplets[j].c.Item1, validSequences[i].triplets[j].c.Item2));
                                        else
                                            leftCouples.Add(Tuple.Create(auxRegions[r].rect.X - regions[validSequences[i].triplets[j].c.Item1][validSequences[i].triplets[j].c.Item2].rect.X, validSequences[i].triplets[j].c.Item1, validSequences[i].triplets[j].c.Item2));
                                    }
                                }

                                // make the new ER part of a triplet and check if line estimates is consistent with the sequence
                                List<RegionTriplet> newValidTriplets = new List<RegionTriplet>();
                                if (leftCouples.Count != 0 && rightCouples.Count != 0)
                                {
                                    leftCouples.Sort(sortCouples);
                                    rightCouples.Sort(sortCouples);
                                    RegionPair pair1 = new RegionPair(Tuple.Create(leftCouples[0].Item2, leftCouples[0].Item3), Tuple.Create(c, regions[c].Count - 1));
                                    RegionPair pair2 = new RegionPair(Tuple.Create(c, regions[c].Count - 1), Tuple.Create(rightCouples[0].Item2, rightCouples[0].Item3));
                                    RegionTriplet triplet = new RegionTriplet(Tuple.Create(0, 0), Tuple.Create(0, 0), Tuple.Create(0, 0));
                                    if (isValidTriplet(regions, pair1, pair2, triplet))
                                    {
                                        newValidTriplets.Add(triplet);
                                    }
                                }
                                else if (rightCouples.Count >= 2)
                                {
                                    rightCouples.Sort(sortCouples);
                                    RegionPair pair1 = new RegionPair(Tuple.Create(c, regions[c].Count - 1), Tuple.Create(rightCouples[0].Item2, rightCouples[0].Item3));
                                    RegionPair pair2 = new RegionPair(Tuple.Create(rightCouples[0].Item2, rightCouples[0].Item3), Tuple.Create(rightCouples[1].Item2, rightCouples[1].Item3));
                                    RegionTriplet triplet = new RegionTriplet(Tuple.Create(0, 0), Tuple.Create(0, 0), Tuple.Create(0, 0));
                                    if (isValidTriplet(regions, pair1, pair2, triplet))
                                    {
                                        newValidTriplets.Add(triplet);
                                    }
                                }
                                else if (leftCouples.Count >= 2)
                                {
                                    leftCouples.Sort(sortCouples);
                                    RegionPair pair1 = new RegionPair(Tuple.Create(leftCouples[1].Item2, leftCouples[1].Item3), Tuple.Create(leftCouples[0].Item2, leftCouples[0].Item3));
                                    RegionPair pair2 = new RegionPair(Tuple.Create(leftCouples[0].Item2, leftCouples[0].Item3), Tuple.Create(c, regions[c].Count - 1));
                                    RegionTriplet triplet = new RegionTriplet(Tuple.Create(0, 0), Tuple.Create(0, 0), Tuple.Create(0, 0));
                                    if (isValidTriplet(regions, pair1, pair2, triplet))
                                    {
                                        newValidTriplets.Add(triplet);
                                    }
                                }
                                else
                                {
                                    // no possible triplets found
                                    continue;
                                }

                                // check if line estimates is consistent with the sequence
                                for (int t = 0; t < newValidTriplets.Count; t++)
                                {
                                    RegionSequence sequence = new RegionSequence(newValidTriplets[t]);
                                    if (isValidSequenceStrict(validSequences[i], sequence))
                                    {
                                        validSequences[i].triplets.Add(newValidTriplets[t]);
                                    }
                                }
                            }
                        }
                    }
                }
                // prepare the sequences for output
                for (int i = 0; i < validSequences.Count; i++)
                {
                    List<Point> bboxPoints = new List<Point>();
                    List<Tuple<int, int>> groupRegions = new List<Tuple<int, int>>();

                    for (int j = 0; j < validSequences[i].triplets.Count; j++)
                    {
                        int prevSize = groupRegions.Count;
                        if (!groupRegions.Exists(x => x == validSequences[i].triplets[j].a)) // shmainei x tetoio wste x == me th malakia poy psaxnw na vrw
                            groupRegions.Add(validSequences[i].triplets[j].a);
                        if (!groupRegions.Exists(x => x == validSequences[i].triplets[j].b))
                            groupRegions.Add(validSequences[i].triplets[j].b);
                        if (!groupRegions.Exists(x => x == validSequences[i].triplets[j].c))
                            groupRegions.Add(validSequences[i].triplets[j].c);

                        for (int k = prevSize; k < groupRegions.Count; k++)
                        {
                            Rectangle temp = regions[groupRegions[k].Item1][groupRegions[k].Item2].rect;
                            bboxPoints.Add(temp.Location);
                            bboxPoints.Add(new Point(temp.Right, temp.Bottom));
                        }
                    }

                    groups.Add(groupRegions);
                    boxes.Add(CvInvoke.BoundingRectangle(new VectorOfPoint(bboxPoints.ToArray())));
                }
            } // end for all the channels
            return boxes.ToArray();
        }
        public static Rectangle[] DoMultiChannelERGNM(InputArray _img, VectorOfMat src, List<ERStat>[] regions, out List<List<Tuple<int, int>>> groups, out List<Rectangle> boxes, bool doFeedbackLoop)
        {
               if (src.GetInputArray().IsEmpty())
                throw new Exception("Exception at ERGROUPINGNM, src is empty");

            int numChannels = src.Size;

            Mat img = _img.GetMat();
            groups = new List<List<Tuple<int, int>>>();
            boxes = new List<Rectangle>();
            //List<RegionTriplet> validTriplets = new List<RegionTriplet>();
            List<Tuple<int, int>> allRegions = new List<Tuple<int, int>>();
            for (int c = 0; c < numChannels; c++)
            {
                //Debug.WriteLine("Processing channel {0}",c);
                // store indices to regions in a single list
                //List<Tuple<int, int>> allRegions = new List<Tuple<int, int>>();
                for (int i = 0; i < regions[c].Count; i++)
                {
                    allRegions.Add(Tuple.Create(c, i));
                }
            }

                List<RegionPair> validPairs = new List<RegionPair>();
                Mat mask = new Mat(img.Rows + 2, img.Cols + 2, DepthType.Cv8U, 1);
                mask.SetTo(new MCvScalar(0));

                Mat grey = new Mat(), lab = new Mat();
                CvInvoke.CvtColor(img, lab, ColorConversion.Rgb2Lab);
                CvInvoke.CvtColor(img, grey, ColorConversion.Rgb2Gray);

                for (int i = 0; i < allRegions.Count; i++)
                {
                    List<int> iSiblings = new List<int>();
                    int firstISiblingIdx = validPairs.Count;

                    for (int j = i + 1; j < allRegions.Count; j++)
                    {
                        if (isValidPair(grey, lab, mask, src, regions, allRegions[i], allRegions[j]))
                        {
                            bool isCycle = false;
                            for (int k = 0; k < iSiblings.Count; k++)
                            {
                                if (isValidPair(grey, lab, mask, src, regions, allRegions[j], allRegions[iSiblings[k]]))
                                {
                                    MCvPoint2D64f iCenter = new MCvPoint2D64f(regions[allRegions[i].Item1][allRegions[i].Item2].rect.X +
                                                              regions[allRegions[i].Item1][allRegions[i].Item2].rect.Width / 2,
                                                              regions[allRegions[i].Item1][allRegions[i].Item2].rect.Y +
                                                              regions[allRegions[i].Item1][allRegions[i].Item2].rect.Height / 2);
                                    MCvPoint2D64f jCenter = new MCvPoint2D64f(regions[allRegions[j].Item1][allRegions[j].Item2].rect.X +
                                                              regions[allRegions[j].Item1][allRegions[j].Item2].rect.Width / 2,
                                                              regions[allRegions[j].Item1][allRegions[j].Item2].rect.Y +
                                                              regions[allRegions[j].Item1][allRegions[j].Item2].rect.Height / 2);
                                    MCvPoint2D64f kCenter = new MCvPoint2D64f(regions[allRegions[iSiblings[k]].Item1][allRegions[iSiblings[k]].Item2].rect.X +
                                                              regions[allRegions[iSiblings[k]].Item1][allRegions[iSiblings[k]].Item2].rect.Width / 2,
                                                              regions[allRegions[iSiblings[k]].Item1][allRegions[iSiblings[k]].Item2].rect.Y +
                                                              regions[allRegions[iSiblings[k]].Item1][allRegions[iSiblings[k]].Item2].rect.Height / 2);
                                    Matrix<double> forNorm = new Matrix<double>(1,2);
                                    MCvPoint2D64f p1 = iCenter - jCenter;
                                    forNorm[0,0] = p1.X;
                                    forNorm[0,1] = p1.Y;
                                    double norm1 = CvInvoke.Norm(forNorm);
                                    p1 = iCenter - kCenter;
                                    forNorm[0,0] = p1.X;
                                    forNorm[0,1] = p1.Y;
                                    double norm2 = CvInvoke.Norm(forNorm);
                                    if (norm1 < norm2)
                                    {
                                        // to pair [i,j] einai metagenestero apo to pair [i,k] g auto kanei mono enan elegxo,
                                        // alliws diathrei auto poy exei. epishs krataei to neo pair sta isibling se periptwsh
                                        // poy xreiastei na to antikatasthsei me kapoio akoma kontunotero. me ton tropo auto
                                        // to pio apomakrusmeno pair (k) xanetai kai den thewreitai pleon ws valid pair toy i, 
                                        // giati exei vrei ena allo region (j) poy einai kontinotero sto i apo to k.
                                        validPairs[firstISiblingIdx + k] = new RegionPair(allRegions[i], allRegions[j]);
                                        iSiblings[k] = j;
                                    }
                                    // isCycle = true shmainei oti den exoyme kapoio kainourio valid pair sto i
                                    // alla oti elexthike ena neo pithano zeugos i,j me ta i,k kai eite egine
                                    // kapoia antikatastash zeugous me neotero eite oxi. se kathe periptwsh omws
                                    // efoson to region j kanei pair me kapoio apo ta k tote den yparxei austhra 
                                    // kainourio zeugari toy i. legontas austhra ennooume sthn perioxh (xwrika - sumfwna me toys kanones
                                    // toy isValidPair) gurw apo to i tetoio wste na mhn exei vrethei prohgoumenws 
                                    // kapoio allo egkuro pithano zeugari.
                                    isCycle = true;
                                    break;
                                }
                            }
                            if (!isCycle)
                            {
                                validPairs.Add(new RegionPair(allRegions[i], allRegions[j]));
                                iSiblings.Add(j);
                            }
                        }
                    }
                }

                List<RegionTriplet> validTriplets = new List<RegionTriplet>();
                for (int i = 0; i < validPairs.Count; i++){
                    for (int j = i + 1; j < validPairs.Count; j++){
                        RegionTriplet validTriplet = new RegionTriplet(Tuple.Create(0,0),Tuple.Create(0,0),Tuple.Create(0,0));
                        if (isValidTriplet(regions,validPairs[i],validPairs[j], validTriplet)){
                            validTriplets.Add(validTriplet);
                        }
                    }
                }

                List<RegionSequence> validSequences = new List<RegionSequence>();
                List<RegionSequence> pendingSequences = new List<RegionSequence>();

                for (int i = 0; i < validTriplets.Count; i++)
                {
                    pendingSequences.Add(new RegionSequence(validTriplets[i]));
                }

                for (int i = 0; i < pendingSequences.Count; i++)
                {
                    bool expanded = false;
                    for (int j = i + 1; j < pendingSequences.Count; j++)
                    {
                        if (isValidSequence(pendingSequences[i], pendingSequences[j]))
                        {
                            expanded = true;
                            pendingSequences[i].triplets.InsertRange(0, pendingSequences[j].triplets);
                            pendingSequences.RemoveAt(j);
                            j--;
                        }
                    }
                    if (expanded)
                    {
                        validSequences.Add(pendingSequences[i]);
                    }
                }

                for (int i = 0; i < validSequences.Count; i++)
                {
                    for (int j = i + 1; j < validSequences.Count; j++)
                    {
                        if (haveCommonRegion(validSequences[i], validSequences[j]))
                        {
                            if (validSequences[i].triplets.Count < validSequences[j].triplets.Count)
                            {
                                validSequences.RemoveAt(i);
                                i--;
                                break;
                            }
                            else
                            {
                                validSequences.RemoveAt(j);
                                j--;
                            }
                        }
                    }
                }

                #region feedback loop
                //if (doFeedbackLoop)
                //{
                //    MyERFilter erFilter = MyERFilterNM.createERFilterNM1(MyERFilterNM.loadDummyClassifier(), 1, 0.005f, 0.3f, 0, false, 0);
                //    //MyERFilter erFilter = MyERFilterNM.createERFilterNM1(MyERFilterNM.loadClassifierNM1(Constants.CLASSIFIER1PATH),
                //    //                                                1,
                //    //                                                0.005f,
                //    //                                                0.3f,
                //    //                                                0f,
                //    //                                                false,
                //    //                                                0.0000000000000001f);
                //    for (int i = 0; i < validSequences.Count; i++)
                //    {
                //        List<Point> bboxPoints = new List<Point>();
                //        Rectangle rect;
                //        for (int j = 0; j < validSequences[i].triplets.Count; j++)
                //        {
                //            rect = regions[validSequences[i].triplets[j].a.Item1][validSequences[i].triplets[j].a.Item2].rect;
                //            bboxPoints.Add(rect.Location);
                //            bboxPoints.Add(new Point(rect.Right, rect.Bottom));
                //            rect = regions[validSequences[i].triplets[j].b.Item1][validSequences[i].triplets[j].b.Item2].rect;
                //            bboxPoints.Add(rect.Location);
                //            bboxPoints.Add(new Point(rect.Right, rect.Bottom));
                //            rect = regions[validSequences[i].triplets[j].c.Item1][validSequences[i].triplets[j].c.Item2].rect;
                //            bboxPoints.Add(rect.Location);
                //            bboxPoints.Add(new Point(rect.Right, rect.Bottom));
                //        }

                //        rect = CvInvoke.BoundingRectangle(new VectorOfPoint(bboxPoints.ToArray()));
                //        rect.X = Math.Max(rect.X - 10, 0);
                //        rect.Y = Math.Max(rect.Y - 10, 0);
                //        rect.Width = Math.Min(rect.Width + 20, src[c].Cols - rect.X);
                //        rect.Height = Math.Min(rect.Height + 20, src[c].Rows - rect.Y);

                //        List<ERStat> auxRegions = new List<ERStat>();
                //        Mat tmp = new Mat(src[c],rect);
                //        erFilter.Run(tmp.GetInputArray(), ref auxRegions);
                //        //Rectangle[] rects = new Rectangle[auxRegions.Count];
                //        //for (int k = 0; k < auxRegions.Count; k++)
                //        //{
                //        //    rects[k] = auxRegions[k].rect;
                //        //}
                //        //return rects;


                //        for (int r = 0; r < auxRegions.Count; r++)
                //        {
                //            if ((auxRegions[r].rect.Y == 0) || (auxRegions[r].rect.Bottom >= tmp.Rows))
                //                continue;

                //            auxRegions[r].rect = new Rectangle(auxRegions[r].rect.X + rect.X, auxRegions[r].rect.Y + rect.Y, auxRegions[r].rect.Width, auxRegions[r].rect.Height);
                //            auxRegions[r].pixel = ((auxRegions[r].pixel / tmp.Cols) + rect.Y) * src[c].Cols + (auxRegions[r].pixel % tmp.Cols) + rect.X;

                //            bool overlaps = false;
                //            for (int j = 0; j < validSequences[i].triplets.Count; j++)
                //            {
                //                Rectangle minAreaRectA = Rectangle.Union(regions[validSequences[i].triplets[j].a.Item1][validSequences[i].triplets[j].a.Item2].rect, auxRegions[r].rect);
                //                Rectangle minAreaRectB = Rectangle.Union(regions[validSequences[i].triplets[j].b.Item1][validSequences[i].triplets[j].b.Item2].rect, auxRegions[r].rect);
                //                Rectangle minAreaRectC = Rectangle.Union(regions[validSequences[i].triplets[j].c.Item1][validSequences[i].triplets[j].c.Item2].rect, auxRegions[r].rect);

                //                if ((minAreaRectA == auxRegions[r].rect) ||
                //                    (minAreaRectB == auxRegions[r].rect) ||
                //                    (minAreaRectC == auxRegions[r].rect) ||
                //                    (minAreaRectA == regions[validSequences[i].triplets[j].a.Item1][validSequences[i].triplets[j].a.Item2].rect) ||
                //                    (minAreaRectB == regions[validSequences[i].triplets[j].b.Item1][validSequences[i].triplets[j].b.Item2].rect) ||
                //                    (minAreaRectC == regions[validSequences[i].triplets[j].c.Item1][validSequences[i].triplets[j].c.Item2].rect))
                //                {
                //                    overlaps = true;
                //                    break;
                //                }
                //            }

                //            if (!overlaps)
                //            {
                //                List<Tuple<int, int, int>> leftCouples = new List<Tuple<int, int, int>>(), rightCouples = new List<Tuple<int, int, int>>();
                //                regions[c].Add(auxRegions[r]);
                //                for (int j = 0; j < validSequences[i].triplets.Count; j++)
                //                {
                //                    if (isValidPair(grey, lab, mask, src, regions, validSequences[i].triplets[j].a, Tuple.Create(c, regions[c].Count - 1)))
                //                    {
                //                        if (regions[validSequences[i].triplets[j].a.Item1][validSequences[i].triplets[j].a.Item2].rect.X > auxRegions[r].rect.X)
                //                            rightCouples.Add(Tuple.Create(regions[validSequences[i].triplets[j].a.Item1][validSequences[i].triplets[j].a.Item2].rect.X - auxRegions[r].rect.X, validSequences[i].triplets[j].a.Item1, validSequences[i].triplets[j].a.Item2));
                //                        else
                //                            leftCouples.Add(Tuple.Create(auxRegions[r].rect.X - regions[validSequences[i].triplets[j].a.Item1][validSequences[i].triplets[j].a.Item2].rect.X, validSequences[i].triplets[j].a.Item1, validSequences[i].triplets[j].a.Item2));
                //                    }
                //                    if (isValidPair(grey, lab, mask, src, regions, validSequences[i].triplets[j].b, Tuple.Create(c, regions[c].Count - 1)))
                //                    {
                //                        if (regions[validSequences[i].triplets[j].b.Item1][validSequences[i].triplets[j].b.Item2].rect.X > auxRegions[r].rect.X)
                //                            rightCouples.Add(Tuple.Create(regions[validSequences[i].triplets[j].b.Item1][validSequences[i].triplets[j].b.Item2].rect.X - auxRegions[r].rect.X, validSequences[i].triplets[j].b.Item1, validSequences[i].triplets[j].b.Item2));
                //                        else
                //                            leftCouples.Add(Tuple.Create(auxRegions[r].rect.X - regions[validSequences[i].triplets[j].b.Item1][validSequences[i].triplets[j].b.Item2].rect.X, validSequences[i].triplets[j].b.Item1, validSequences[i].triplets[j].b.Item2));
                //                    }
                //                    if (isValidPair(grey, lab, mask, src, regions, validSequences[i].triplets[j].c, Tuple.Create(c, regions[c].Count - 1)))
                //                    {
                //                        if (regions[validSequences[i].triplets[j].c.Item1][validSequences[i].triplets[j].c.Item2].rect.X > auxRegions[r].rect.X)
                //                            rightCouples.Add(Tuple.Create(regions[validSequences[i].triplets[j].c.Item1][validSequences[i].triplets[j].c.Item2].rect.X - auxRegions[r].rect.X, validSequences[i].triplets[j].c.Item1, validSequences[i].triplets[j].c.Item2));
                //                        else
                //                            leftCouples.Add(Tuple.Create(auxRegions[r].rect.X - regions[validSequences[i].triplets[j].c.Item1][validSequences[i].triplets[j].c.Item2].rect.X, validSequences[i].triplets[j].c.Item1, validSequences[i].triplets[j].c.Item2));
                //                    }
                //                }

                //                // make the new ER part of a triplet and check if line estimates is consistent with the sequence
                //                List<RegionTriplet> newValidTriplets = new List<RegionTriplet>();
                //                if (leftCouples.Count != 0 && rightCouples.Count != 0)
                //                {
                //                    leftCouples.Sort(sortCouples);
                //                    rightCouples.Sort(sortCouples);
                //                    RegionPair pair1 = new RegionPair(Tuple.Create(leftCouples[0].Item2, leftCouples[0].Item3), Tuple.Create(c, regions[c].Count - 1));
                //                    RegionPair pair2 = new RegionPair(Tuple.Create(c, regions[c].Count - 1), Tuple.Create(rightCouples[0].Item2, rightCouples[0].Item3));
                //                    RegionTriplet triplet = new RegionTriplet(Tuple.Create(0, 0), Tuple.Create(0, 0), Tuple.Create(0, 0));
                //                    if (isValidTriplet(regions, pair1, pair2, triplet))
                //                    {
                //                        newValidTriplets.Add(triplet);
                //                    }
                //                }
                //                else if (rightCouples.Count >= 2)
                //                {
                //                    rightCouples.Sort(sortCouples);
                //                    RegionPair pair1 = new RegionPair(Tuple.Create(c, regions[c].Count - 1), Tuple.Create(rightCouples[0].Item2, rightCouples[0].Item3));
                //                    RegionPair pair2 = new RegionPair(Tuple.Create(rightCouples[0].Item2, rightCouples[0].Item3), Tuple.Create(rightCouples[1].Item2, rightCouples[1].Item3));
                //                    RegionTriplet triplet = new RegionTriplet(Tuple.Create(0, 0), Tuple.Create(0, 0), Tuple.Create(0, 0));
                //                    if (isValidTriplet(regions, pair1, pair2, triplet))
                //                    {
                //                        newValidTriplets.Add(triplet);
                //                    }
                //                }
                //                else if (leftCouples.Count >= 2)
                //                {
                //                    leftCouples.Sort(sortCouples);
                //                    RegionPair pair1 = new RegionPair(Tuple.Create(leftCouples[1].Item2, leftCouples[1].Item3), Tuple.Create(leftCouples[0].Item2, leftCouples[0].Item3));
                //                    RegionPair pair2 = new RegionPair(Tuple.Create(leftCouples[0].Item2, leftCouples[0].Item3), Tuple.Create(c, regions[c].Count - 1));
                //                    RegionTriplet triplet = new RegionTriplet(Tuple.Create(0, 0), Tuple.Create(0, 0), Tuple.Create(0, 0));
                //                    if (isValidTriplet(regions, pair1, pair2, triplet))
                //                    {
                //                        newValidTriplets.Add(triplet);
                //                    }
                //                }
                //                else
                //                {
                //                    // no possible triplets found
                //                    continue;
                //                }

                //                // check if line estimates is consistent with the sequence
                //                for (int t = 0; t < newValidTriplets.Count; t++)
                //                {
                //                    RegionSequence sequence = new RegionSequence(newValidTriplets[t]);
                //                    if (isValidSequence(validSequences[i], sequence))
                //                    {
                //                        validSequences[i].triplets.Add(newValidTriplets[t]);
                //                    }                                    
                //                }
                //            }
                //        }
                //    }
                //}
                #endregion
                // prepare the sequences for output
                for (int i = 0; i < validSequences.Count; i++)
                {
                    List<Point> bboxPoints = new List<Point>();
                    List<Tuple<int, int>> groupRegions = new List<Tuple<int, int>>();

                    for (int j = 0; j < validSequences[i].triplets.Count; j++)
                    {
                        int prevSize = groupRegions.Count;
                        if (!groupRegions.Exists(x => x == validSequences[i].triplets[j].a)) // shmainei x tetoio wste x == me th malakia poy psaxnw na vrw
                            groupRegions.Add(validSequences[i].triplets[j].a);
                        if (!groupRegions.Exists(x => x == validSequences[i].triplets[j].b))
                            groupRegions.Add(validSequences[i].triplets[j].b);
                        if (!groupRegions.Exists(x => x == validSequences[i].triplets[j].c))
                            groupRegions.Add(validSequences[i].triplets[j].c);

                        for (int k = prevSize; k < groupRegions.Count; k++)
                        {
                            Rectangle temp = regions[groupRegions[k].Item1][groupRegions[k].Item2].rect;
                            bboxPoints.Add(temp.Location);
                            bboxPoints.Add(new Point(temp.Right, temp.Bottom));
                        }
                    }

                    groups.Add(groupRegions);
                    boxes.Add(CvInvoke.BoundingRectangle(new VectorOfPoint(bboxPoints.ToArray())));
                }
            //} // end for all the channels
            return boxes.ToArray();
        }

        public static void FitLine(Point p1, Point p2, out float a0, out float a1)
        {
            if (!(p1.X != p2.X))
                throw new Exception("Exception at FitLine");

            a1 = (float)(p2.Y - p1.Y) / (p2.X - p1.X);
            a0 = a1 * -1 * p1.X + p1.Y;
        }

        public static void FitLineOLS(Point p1, Point p2, Point p3, out float a0, out float a1)
        {
            float sumX = (float)(p1.X + p2.X + p3.X);
            float sumY = (float)(p1.Y + p2.Y + p3.Y);
            float sumXY = (float)(p1.X * p1.Y + p2.X * p2.Y + p3.X * p3.Y);
            float sumX2 = (float)(p1.X * p1.X + p2.X * p2.X + p3.X * p3.X);

            a0 = (float)(sumY * sumX2 - sumX * sumXY) / (3 * sumX2 - sumX * sumX);
            a1 = (float)(3 * sumXY - sumX * sumY) / (3 * sumX2 - sumX * sumX);
        }

        public static float FitLineLMS(Point p1, Point p2, Point p3, out float a0, out float a1)
        {
            a0 = -1; // intercept - tomh
            a1 = 0;  // slope - klish
            float lA0, lA1, bestSlope = float.MaxValue, err = 0;

            if (p1.X != p2.X)
            {
                FitLine(p1, p2, out lA0, out lA1);

                if (Math.Abs(lA1) < bestSlope)
                {
                    bestSlope = Math.Abs(lA1);
                    a0 = lA0;
                    a1 = lA1;
                    err = (p3.Y - (a0 + a1 * p3.X));
                }
            }

            if (p1.X != p3.X)
            {
                FitLine(p1, p3, out lA0, out lA1);

                if (Math.Abs(lA1) < bestSlope)
                {
                    bestSlope = Math.Abs(lA1);
                    a0 = lA0;
                    a1 = lA1;
                    err = (p2.Y - (a0 + a1 * p2.X));
                }
            }

            if (p2.X != p3.X)
            {
                FitLine(p2, p3, out lA0, out lA1);

                if (Math.Abs(lA1) < bestSlope)
                {
                    bestSlope = Math.Abs(lA1);
                    a0 = lA0;
                    a1 = lA1;
                    err = (p1.Y - (a0 + a1 * p1.X));
                }
            }

            return err;
        }

        public static bool FitLineEstimates(List<ERStat>[] regions, RegionTriplet triplet)
        {
            List<Rectangle> charBoxes = new List<Rectangle>();
            charBoxes.Add(regions[triplet.a.Item1][triplet.a.Item2].rect);
            charBoxes.Add(regions[triplet.b.Item1][triplet.b.Item2].rect);
            charBoxes.Add(regions[triplet.c.Item1][triplet.c.Item2].rect);

            triplet.lineEstimates.xMin = Math.Min(Math.Min(charBoxes[0].X, charBoxes[1].X), charBoxes[2].X);
            triplet.lineEstimates.xMax = Math.Max(Math.Max(charBoxes[0].Right, charBoxes[1].Right), charBoxes[2].Right);
            triplet.lineEstimates.hMax = Math.Max(Math.Max(charBoxes[0].Height, charBoxes[1].Height), charBoxes[2].Height);

            // fit one bottom line

            float err = FitLineLMS(new Point(charBoxes[0].Right, charBoxes[0].Bottom),
                                   new Point(charBoxes[1].Right, charBoxes[1].Bottom),
                                   new Point(charBoxes[2].Right, charBoxes[2].Bottom),
                                   out triplet.lineEstimates.bottom1A0,
                                   out triplet.lineEstimates.bottom1A1);
            if ((triplet.lineEstimates.bottom1A0 == -1) && (triplet.lineEstimates.bottom1A1 == 0))
                return false;

            // slope for all lines must be the same
            triplet.lineEstimates.bottom2A1 = triplet.lineEstimates.bottom1A1;
            triplet.lineEstimates.top1A1 = triplet.lineEstimates.bottom1A1;
            triplet.lineEstimates.top2A1 = triplet.lineEstimates.bottom1A1;

            if (Math.Abs(err) > (float)triplet.lineEstimates.hMax / 6)
            {
                // we need two different bottom lines
                triplet.lineEstimates.bottom2A0 = triplet.lineEstimates.bottom1A0 + err;
            }
            else
            {
                //second line is the same
                triplet.lineEstimates.bottom2A0 = triplet.lineEstimates.bottom1A0;
            }

            // fit one top line within the two (y) - closer coordinates

            int d12 = Math.Abs(charBoxes[0].Y - charBoxes[1].Y);
            int d13 = Math.Abs(charBoxes[0].Y - charBoxes[2].Y);
            int d23 = Math.Abs(charBoxes[1].Y - charBoxes[2].Y);

            if ((d12 < d13) && (d12 < d23))
            {
                Point p = new Point((charBoxes[0].X + charBoxes[1].X) / 2, (charBoxes[0].Y + charBoxes[1].Y) / 2);
                triplet.lineEstimates.top1A0 = triplet.lineEstimates.bottom1A0 +
                    (p.Y - (triplet.lineEstimates.bottom1A0 + p.X * triplet.lineEstimates.bottom1A1));
                p = charBoxes[2].Location;
                err = (p.Y - (triplet.lineEstimates.top1A0 + p.X * triplet.lineEstimates.top1A1));
            }
            else if (d13 < d23)
            {
                Point p = new Point((charBoxes[0].X + charBoxes[2].X) / 2, (charBoxes[0].Y + charBoxes[2].Y) / 2);
                triplet.lineEstimates.top1A0 = triplet.lineEstimates.bottom1A0 +
                    (p.Y - (triplet.lineEstimates.bottom1A0 + p.X * triplet.lineEstimates.bottom1A1));
                p = charBoxes[1].Location;
                err = (p.Y - (triplet.lineEstimates.top1A0 + p.X * triplet.lineEstimates.top1A1));
            }
            else
            {
                Point p = new Point((charBoxes[1].X + charBoxes[2].X) / 2, (charBoxes[1].Y + charBoxes[2].Y) / 2);
                triplet.lineEstimates.top1A0 = triplet.lineEstimates.bottom1A0 +
                    (p.Y - (triplet.lineEstimates.bottom1A0 + p.X * triplet.lineEstimates.bottom1A1));
                p = charBoxes[0].Location;
                err = (p.Y - (triplet.lineEstimates.top1A0 + p.X * triplet.lineEstimates.top1A1));
            }

            if (Math.Abs(err) > (float)triplet.lineEstimates.hMax / 6)
            {
                // we need two different top lines
                triplet.lineEstimates.top2A0 = triplet.lineEstimates.top1A0 + err;
            }
            else
            {
                // second top line is the same line
                triplet.lineEstimates.top2A0 = triplet.lineEstimates.top1A0;
            }

            return true;
        }
        
        #endregion
    }


}
