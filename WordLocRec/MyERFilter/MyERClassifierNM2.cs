﻿using Emgu.CV;
using Emgu.CV.CvEnum;
using Emgu.CV.ML;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace WordLocRec
{
    class MyERClassifierNM2 : MyERFilter.Callback
    {
        private Boost boost;
        public override void PrintWatch()
        {
            
        }
        public MyERClassifierNM2(String filename)
        {
            boost = new Boost();
            FileStorage fs = new FileStorage(filename,FileStorage.Mode.Read);
            FileNode fn = fs.GetFirstTopLevelNode();
            boost.Read(fn);
            if (boost == null)
            {
                throw new Exception("Exception at reading the classifier (1) at file: " + filename);
            }
        }
        public override double Eval(ERStat stat)
        {
            Mat sample = new Mat(1, 7, DepthType.Cv32F, 1);
            float[] data = new float[7];
            GCHandle handle = GCHandle.Alloc(data, GCHandleType.Pinned);
            Mat m2 = new Mat(sample.Size, DepthType.Cv32F, 1, handle.AddrOfPinnedObject(), sample.Width);
            data[0] = (float)(stat.rect.Width) / (stat.rect.Height);
            data[1] = (float)Math.Sqrt((float)(stat.area)) / stat.perimeter;
            data[2] = (float)(1 - stat.euler);
            data[3] = stat.medCrossings;
            data[4] = stat.holeAreaRatio;
            data[5] = stat.convexHullRatio;
            data[6] = stat.numInflexionPoints;

            float votes = boost.Predict(m2, null, 256 | 1);
            //float votes1 = boost.Predict(m2);
            handle.Free();
            return (double)1 - (double)1 / (1 + Math.Exp(-2 * votes));
        }
    }
}
