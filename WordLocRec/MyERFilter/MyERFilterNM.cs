﻿using Emgu.CV;
using Emgu.CV.CvEnum;
using Emgu.CV.Structure;
using Emgu.CV.Util;
using Nito;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WordLocRec
{
    class MyERFilterNM : MyERFilter
    {
        //test variables
        //private Stopwatch addPixelWatch = new Stopwatch();
        //private Stopwatch mergeWatch = new Stopwatch();
        //private Stopwatch insideWatch = new Stopwatch();
        //private Stopwatch saveAndExitWatch = new Stopwatch();
        //private int pixelCounter = 0, saveCounter = 0;

        public MyERFilterNM()
        {
            thresholdDelta = 1;
            minArea = 0f;
            maxArea = 1f;
            minProbability = 0f;
            nonMaxSuppression = false;
            minProbabilityDiff = 1f;
            numAcceptedRegions = 0;
            numRejectedRegions = 0;
        }

        public float minProbability;
        public bool nonMaxSuppression;
        public float minProbabilityDiff;

        protected int thresholdDelta;
        protected float maxArea, minArea;

        protected Callback classifier;

        protected int numRejectedRegions, numAcceptedRegions;
        private List<ERStat> regions;
        private Mat regionMask;

        public override void setCallback(Callback cb)
        {
            classifier = cb;
        }
        public override void setThresholdDelta(int thresholdDelta)
        {
            if (!(thresholdDelta > 0 && thresholdDelta <= 128))
                throw new Exception("Exception at ThresholdDelta");
            this.thresholdDelta = thresholdDelta;
        }
        public override void setMinArea(float minArea)
        {
            if (!(minArea >= 0 && minArea < maxArea))
                throw new Exception("Exception at minArea");
            this.minArea = minArea;
        }
        public override void setMaxArea(float maxArea)
        {
            if (!(maxArea <= 1)) throw new Exception("First Exception at maxArea");
            if (!(minArea < maxArea)) throw new Exception("Second Exception at maxArea");
            this.maxArea = maxArea;
        }
        public override void setMinProbability(float minProbability)
        {
            if (!((minProbability >= 0f) && (minProbability <= 1f))) throw new Exception("Exception at set min probability");
            this.minProbability = minProbability;
        }
        public override void setMinProbabilityDiff(float minProbabilityDiff)
        {
            if (!((minProbabilityDiff >= 0f) && (minProbabilityDiff <= 1f))) throw new Exception("Exception at set min probability diff");
            this.minProbabilityDiff = minProbabilityDiff;
        }
        public override void setNonMaxSuppression(bool nonMaxSuppression)
        {
            this.nonMaxSuppression = nonMaxSuppression;
        }
        public override int getNumRejected()
        {
            return numRejectedRegions;
        }

        public override void Run(InputArray image, ref List<ERStat> _regions)
        {
            if (image.GetMat().Depth != DepthType.Cv8U)
            {
                _regions = null;
                return;
            }

            regions = _regions;
            regionMask = new Mat(image.GetMat().Rows + 2, image.GetMat().Cols + 2, DepthType.Cv8U, 1);

            if (regions.Count == 0)
            {
                erTreeExtract(image);
                if (nonMaxSuppression)
                {
                    List<ERStat> temp = new List<ERStat>(regions);
                    regions = new List<ERStat>(temp.Count);
                    erTreeNonmaxSuppression(temp[0], null, null);
                }
            }
            else
            {
                if (regions[0].parent != null) return;

                List<ERStat> temp = new List<ERStat>(regions);
                regions = new List<ERStat>(temp.Count);
                erTreeFilter(image, temp[0], null, null);
            }
            _regions = regions;

            //PrintWatches();
            //saveAndExitWatch.Reset();
            //mergeWatch.Reset();
            //addPixelWatch.Reset();
            //insideWatch.Reset();
        }
        //private void PrintWatches()
        //{
        //    Debug.WriteLine("-----Printing Watches-----");
        //    Debug.WriteLine("Merge and change level: " + saveAndExitWatch.Elapsed);
        //    Debug.WriteLine("Checking for accessibility : " + mergeWatch.Elapsed);
        //    Debug.WriteLine(" Part of the above (calculations) : " + insideWatch.Elapsed);
        //    Debug.WriteLine("Calculate neighbours and quads: " + addPixelWatch.Elapsed);
        //}

        private void erTreeExtract(InputArray image)
        {
            Mat src = image.GetMat();
            if (src.Depth != DepthType.Cv8U)
            {
                return;
            }

            if (thresholdDelta > 1)
            {
                Image<Gray, Byte> tempImage = src.ToImage<Gray, Byte>();
                tempImage = (tempImage / thresholdDelta) - 1;
                src = tempImage.GetInputArray().GetMat();
            }
            Byte[] imageData = src.GetData();
            int width = src.Cols, height = src.Rows;

            List<ERStat> erStack = new List<ERStat>();

            byte[,] quads = new byte[3, 4];

            quads[0, 0] = 1 << 3;
            quads[0, 1] = 1 << 2;
            quads[0, 2] = 1 << 1;
            quads[0, 3] = 1;
            quads[1, 0] = 1 << 2 | 1 << 1 | 1;
            quads[1, 1] = 1 << 3 | 1 << 1 | 1;
            quads[1, 2] = 1 << 3 | 1 << 2 | 1;
            quads[1, 3] = 1 << 3 | 1 << 2 | 1 << 1;
            quads[2, 0] = 1 << 2 | 1 << 1;
            quads[2, 1] = 1 << 3 | 1;

            bool[] accessiblePixelMask = new bool[width * height];
            bool[] accumulatedPixelMask = new bool[width * height];

            List<int>[] boundaryPixels = new List<int>[256];
            List<int>[] boundaryEdges = new List<int>[256];
            for (int i = 0; i < boundaryPixels.Length; i++)
			{
                boundaryPixels[i] = new List<int>();
                boundaryEdges[i] = new List<int>();
			}

            //add an empty ERStat as the root of the ERTree
            erStack.Add(new ERStat());

            int thresholdLevel = (255 / thresholdDelta) + 1;

            int currentPixel = 0;
            int currentEdge = 0;
            int currentLevel = imageData[0];
            accessiblePixelMask[0] = true;

            bool pushNewComponent = true;
            //initialization ends here

            for (; ; )
            {
                int x = currentPixel % width;
                int y = currentPixel / width;
                //Debug.WriteLine("Current pixel: " + currentPixel);
                //Debug.WriteLine("(y, x) = (" + y + ", " + x + ")");


                if (pushNewComponent)
                    erStack.Add(new ERStat(currentLevel, currentPixel, x, y));
                pushNewComponent = false;

                #region Checking neighbour pixels for accessibility; if they've been checked before
                for ( ; currentEdge < 4 ; currentEdge++) // den exei arxh to current edge edw giati pairnei apo to boundary edges
                {
                    int neighbourPixel = currentPixel;

                    switch (currentEdge)
                    {
                        case 0: if (x - width + 1 < 0) neighbourPixel = currentPixel + 1; break; // deksi pixel
                        case 1: if (y - height + 1 < 0) neighbourPixel = currentPixel + width; break; // katw pixel
                        case 2: if (x > 0) neighbourPixel = currentPixel - 1; break; // aristero pixel
                        default: if (y > 0) neighbourPixel = currentPixel - width; break; // panw pixel
                    }
                    if (neighbourPixel != currentPixel && !accessiblePixelMask[neighbourPixel])
                    {
                        int neighbourLevel = imageData[neighbourPixel];
                        accessiblePixelMask[neighbourPixel] = true;

                        if (neighbourLevel >= currentLevel)
                        {
                            boundaryPixels[neighbourLevel].Add(neighbourPixel);
                            boundaryEdges[neighbourLevel].Add(0);

                            if (neighbourLevel < thresholdLevel)
                                thresholdLevel = neighbourLevel;
                        }
                        else
                        {
                            boundaryPixels[currentLevel].Add(currentPixel);
                            boundaryEdges[currentLevel].Add(currentEdge + 1);

                            if (currentLevel < thresholdLevel)
                                thresholdLevel = currentLevel;

                            currentPixel = neighbourPixel;
                            currentEdge = 0;
                            currentLevel = neighbourLevel;

                            pushNewComponent = true;
                            break;
                        }
                    }
                }
                #endregion

                if (pushNewComponent) continue;

                #region incrementaly computable descriptors
                
                int nonBoundaryNeighbours = 0;
                int nonBoundaryNeighboursHoriz = 0;

                byte[] quadBefore = new Byte[] { 0, 0, 0, 0 };
                byte[] quadAfter = new Byte[] { 0, 0, 0, 0 };
                quadAfter[0] = 1 << 1;
                quadAfter[1] = 1 << 3;
                quadAfter[2] = 1 << 2;
                quadAfter[3] = 1;

                for (int edge = 0; edge < 8; edge++)
                {
                    int neighbour4 = -1; // von Neumann neighborhood
                    int neighbour8 = -1; // Moore neighborhood
                    int cell = 0;
                    switch (edge)
                    {   //all the ifs check if the pixel belongs in the matrix boundary
                        case 0: if (x < width - 1) { neighbour4 = neighbour8 = currentPixel + 1; } cell = 5; break;
                        case 1: if ((x < width - 1) && (y < height - 1)) { neighbour8 = currentPixel + 1 + width; } cell = 8; break;
                        case 2: if (y < height - 1) { neighbour4 = neighbour8 = currentPixel + width; } cell = 7; break;
                        case 3: if ((x > 0) && (y < height - 1)) { neighbour8 = currentPixel - 1 + width; } cell = 6; break;
                        case 4: if (x > 0) { neighbour4 = neighbour8 = currentPixel - 1; } cell = 3; break;
                        case 5: if ((x > 0) && (y > 0)) { neighbour8 = currentPixel - 1 - width; } cell = 0; break;
                        case 6: if (y > 0) { neighbour4 = neighbour8 = currentPixel - width; } cell = 1; break;
                        default: if ((y > 0) && (x < width - 1)) { neighbour8 = currentPixel + 1 - width; } cell = 2; break;
                    }
                    if ((neighbour4 != -1) && (accumulatedPixelMask[neighbour4]) && (imageData[neighbour4] <= imageData[currentPixel]))
                    {
                        nonBoundaryNeighbours++;
                        if ((edge == 0) || (edge == 4))
                            nonBoundaryNeighboursHoriz++;
                    }

                    int pixValue = imageData[currentPixel] + 1;
                    if (neighbour8 != -1)
                        if (accumulatedPixelMask[neighbour8])
                            pixValue = imageData[neighbour8];

                    if (pixValue <= imageData[currentPixel])
                    {
                        switch (cell)
                        {
                            case 0:
                                quadBefore[3] = (byte)(quadBefore[3] | 1 << 3);
                                quadAfter[3] = (byte)(quadAfter[3] | 1 << 3);
                                break;
                            case 1:
                                quadBefore[3] = (byte)(quadBefore[3] | 1 << 2);
                                quadAfter[3] = (byte)(quadAfter[3] | 1 << 2);
                                quadBefore[0] = (byte)(quadBefore[0] | 1 << 3);
                                quadAfter[0] = (byte)(quadAfter[0] | 1 << 3);
                                break;
                            case 2:
                                quadBefore[0] = (byte)(quadBefore[0] | 1 << 2);
                                quadAfter[0] = (byte)(quadAfter[0] | 1 << 2);
                                break;
                            case 3:
                                quadBefore[3] = (byte)(quadBefore[3] | 1 << 1);
                                quadAfter[3] = (byte)(quadAfter[3] | 1 << 1);
                                quadBefore[2] = (byte)(quadBefore[2] | 1 << 3);
                                quadAfter[2] = (byte)(quadAfter[2] | 1 << 3);
                                break;
                            case 5:
                                quadBefore[0] = (byte)(quadBefore[0] | 1);
                                quadAfter[0]  = (byte)(quadAfter[0] | 1);
                                quadBefore[1] = (byte)(quadBefore[1] | 1 << 2);
                                quadAfter[1]  = (byte)(quadAfter[1] | 1 << 2);
                                break;
                            case 6:
                                quadBefore[2] = (byte)(quadBefore[2] | 1 << 1);
                                quadAfter[2]  = (byte)(quadAfter[2]  | 1 << 1);
                                break;
                            case 7:
                                quadBefore[2] = (byte)(quadBefore[2] | 1);
                                quadAfter[2]  = (byte)(quadAfter[2]  | 1);
                                quadBefore[1] = (byte)(quadBefore[1] | 1 << 1);
                                quadAfter[1]  = (byte)(quadAfter[1]  | 1 << 1);
                                break;
                            default:
                                quadBefore[1] = (byte)(quadBefore[1] | 1);
                                quadAfter[1]  = (byte)(quadAfter[1] | 1);
                                break;
                        }
                    }

                } // endfor edge

                int[] cBefore = new int[] { 0, 0, 0 };
                int[] cAfter = new int[] { 0, 0, 0 };

                for (int p = 0; p < 3; p++)
                {
                    for (int q = 0; q < 4; q++)
                    {
                        if ((quadBefore[0] == quads[p, q]) && ((p < 2) || (q < 2)))
                            cBefore[p]++;
                        if ((quadBefore[1] == quads[p, q]) && ((p < 2) || (q < 2)))
                            cBefore[p]++;
                        if ((quadBefore[2] == quads[p, q]) && ((p < 2) || (q < 2)))
                            cBefore[p]++;
                        if ((quadBefore[3] == quads[p, q]) && ((p < 2) || (q < 2)))
                            cBefore[p]++;

                        if ((quadAfter[0] == quads[p, q]) && ((p < 2) || (q < 2)))
                            cAfter[p]++;
                        if ((quadAfter[1] == quads[p, q]) && ((p < 2) || (q < 2)))
                            cAfter[p]++;
                        if ((quadAfter[2] == quads[p, q]) && ((p < 2) || (q < 2)))
                            cAfter[p]++;
                        if ((quadAfter[3] == quads[p, q]) && ((p < 2) || (q < 2)))
                            cAfter[p]++;
                    }
                }

                int dC1 = cAfter[0] - cBefore[0];
                int dC2 = cAfter[1] - cBefore[1];
                int dC3 = cAfter[2] - cBefore[2];

                #endregion

                erAddPixel(erStack[erStack.Count - 1], x, y, nonBoundaryNeighbours, nonBoundaryNeighboursHoriz, dC1, dC2, dC3);
                
                accumulatedPixelMask[currentPixel] = true;

                if (thresholdLevel == (255/thresholdDelta) + 1) // if all possible threshold levels have been processed signal the end
                {
                    erSave(erStack[erStack.Count - 1], null, null);

                    //for (int r = 0; r < erStack.Count; r++)
                    //{
                    //    ERStat stat = erStack[r];
                    //    if (stat.crossings != null)
                    //    {
                    //        stat.crossings.Clear();
                    //        stat.crossings = null;
                    //    }
                    //    MyERFilter.deleteERStatTree(ref stat);
                    //}
                    //erStack.Clear();
                    
                    return;
                }

                currentPixel = boundaryPixels[thresholdLevel][boundaryPixels[thresholdLevel].Count - 1];
                boundaryPixels[thresholdLevel].RemoveAt(boundaryPixels[thresholdLevel].Count - 1);
                currentEdge = boundaryEdges[thresholdLevel][boundaryEdges[thresholdLevel].Count - 1];
                boundaryEdges[thresholdLevel].RemoveAt(boundaryEdges[thresholdLevel].Count - 1);

                while ((thresholdLevel < (255 / thresholdDelta) + 1) && boundaryPixels[thresholdLevel].Count == 0)
                {
                    //c# tip!
                    // όταν το σύστημα κάνει έλεγχο σε μία πολλαπλή συνθήκη με && τότε αν ελέγξει το πρώτο μισό και
                    // έχει ως αποτέλεσμα "false" δεν ελέγχει την υπόλοιπη συνθήκη.
                    // is it safe??
                    thresholdLevel++;
                }

                int newLevel = imageData[currentPixel];
                if (newLevel != currentLevel)
                {
                    currentLevel = newLevel;

                    while (erStack[erStack.Count - 1].level < newLevel)
                    {
                        ERStat er = erStack[erStack.Count - 1];
                        erStack.RemoveAt(erStack.Count - 1);

                        if (newLevel < erStack[erStack.Count - 1].level)// auto uparxei gia thn periptwsh emfoleumenwn er poy apotelountai apo 1 mono pixel
                        {
                            erStack.Add(new ERStat(newLevel, currentPixel, currentPixel % width, currentPixel / width));
                            erMerge(erStack[erStack.Count - 1], er);
                            break;
                        }
                        erMerge(erStack[erStack.Count - 1], er);
                    }
                }
            }//end psycho for

        } 
        private void erAddPixel(ERStat parent, int x, int y, int nonBoundaryNeighbours, int nonBoundaryNeighboursHoriz, int dC1, int dC2, int dC3)
        {           
            parent.area++;
            parent.perimeter += 4 - 2 * nonBoundaryNeighbours;

            List<int> parentCrossings = parent.crossings;
            Rectangle parentRect = parent.rect;
            if (parentCrossings.Count > 0)
            {
                int TopHeightDifference = y - parentRect.Y;
                if (TopHeightDifference < 0) //to neo pixel vrisketai panw apo to rectangle
                    parentCrossings.Insert(0, 2);
                else if (y > parentRect.Bottom - 1) //to neo pixel vrisketai katw apo to rectangle
                    parentCrossings.Add(2);
                else //to neo pixel vrisketai entos toy ypsoys toy rectangle
                    parentCrossings[TopHeightDifference] += 2 - 2 * nonBoundaryNeighboursHoriz;
            }
            else
                parentCrossings.Add(2);

            parent.crossings = parentCrossings;
            parent.rect = parentRect;  
            parent.euler += (dC1 - dC2 + 2 * dC3) / 4;
     
            int newX1 = Math.Min(parent.rect.X, x);
            int newY1 = Math.Min(parent.rect.Y, y);
            int newX2 = Math.Max(parent.rect.Right - 1, x);
            int newY2 = Math.Max(parent.rect.Bottom - 1, y);

            parent.rect.X = newX1;
            parent.rect.Y = newY1;
            parent.rect.Width = newX2 - newX1 + 1;
            parent.rect.Height = newY2 - newY1 + 1;

            parent.rawMoments[0] += x;
            parent.rawMoments[1] += y;

            parent.centralMoments[0] += x * x;
            parent.centralMoments[1] += x * y;
            parent.centralMoments[2] += y * y;
        }        
        private void erMerge(ERStat parent, ERStat child)
        {
            parent.area += child.area;
            parent.perimeter += child.perimeter;

            for (int i = parent.rect.Y; i <= Math.Min(parent.rect.Bottom - 1, child.rect.Bottom - 1); i++)
                if (i - child.rect.Y >= 0) 
                    parent.crossings[i - parent.rect.Y] += child.crossings[i - child.rect.Y];

            for (int i = parent.rect.Y - 1; i >= child.rect.Y; i--)
                if (i - child.rect.Y < child.crossings.Count)
                    parent.crossings.Insert(0, child.crossings[i - child.rect.Y]);
                else
                    parent.crossings.Insert(0, 0);

            for (int i = parent.rect.Bottom; i < child.rect.Y; i++)
                parent.crossings.Add(0);

            for (int i = Math.Max(parent.rect.Bottom, child.rect.Y); i <= child.rect.Bottom - 1; i++)
                parent.crossings.Add(child.crossings[i - child.rect.Y]);

            parent.euler += child.euler;

            int newX1 = Math.Min(parent.rect.X, child.rect.X);
            int newY1 = Math.Min(parent.rect.Y, child.rect.Y);
            int newX2 = Math.Max(parent.rect.Right - 1, child.rect.Right - 1);
            int newY2 = Math.Max(parent.rect.Bottom - 1, child.rect.Bottom - 1);
            parent.rect.X = newX1;
            parent.rect.Y = newY1;
            parent.rect.Width = newX2 - newX1 + 1;
            parent.rect.Height = newY2 - newY1 + 1;

            parent.rawMoments[0] += child.rawMoments[0];
            parent.rawMoments[1] += child.rawMoments[1];

            parent.centralMoments[0] += child.centralMoments[0];
            parent.centralMoments[1] += child.centralMoments[1];
            parent.centralMoments[2] += child.centralMoments[2];

            List<int> mCrossings = new List<int>();
            mCrossings.Add(child.crossings[(int)(child.rect.Height) / 6]);
            mCrossings.Add(child.crossings[(int)3*(child.rect.Height) / 6]);
            mCrossings.Add(child.crossings[(int)5*(child.rect.Height) / 6]);
            mCrossings.Sort();

            child.medCrossings = (float)mCrossings[1];

            child.crossings.Clear();
            child.crossings = null;

            child.level = child.level * thresholdDelta;

            if (classifier != null)
                child.probability = classifier.Eval(child);
            
            if ( (((classifier != null)?(child.probability >= minProbability):true)||(nonMaxSuppression)) &&
                 ((child.area >= (minArea*regionMask.Rows * regionMask.Cols)) &&
                  (child.area <= (maxArea*regionMask.Rows * regionMask.Cols)) &&
                  (child.rect.Width > 2) && (child.rect.Height > 2)))
            {
                numAcceptedRegions++;

                child.next = parent.child;
                if (parent.child != null)
                {
                    parent.child.prev = child;
                }
                parent.child = child;
                child.parent = parent;
            }
            else // otan svhnei ena ER tote krataei ta children toy sto dentro
                // apla xanetai to ER kai tpt allo den allazei sto dentro
            {
                numRejectedRegions++;

                if (child.prev != null)
                {
                    child.prev.next = child.next;
                }

                ERStat newChild = child.child;

                if (newChild != null)
                {
                    while (newChild.next != null)
                        newChild = newChild.next;
                    newChild.next = parent.child;
                    if (parent.child != null)
                        parent.child.prev = newChild;
                    parent.child = child.child;
                    child.child.parent = parent;
                }

                //if (child.crossings != null)
                //{
                    //child.crossings.Clear();
                    //child.crossings = null;
                //}
                child = null;
            }
        }
        private ERStat erSave(ERStat er, ERStat parent, ERStat prev)
        {
            regions.Add(er);
            regions[regions.Count - 1].parent = parent;

            if (prev != null) // to er exei panw apo ena aderfia
                prev.next = regions[regions.Count - 1];
            else if (parent != null)
                parent.child = regions[regions.Count - 1];

            ERStat oldPrev = null;
            ERStat thisER = regions[regions.Count - 1];

            if (thisER.parent == null) //parent den exei mono to dummy ER sto intensity 256 
                thisER.probability = 0;

            if (nonMaxSuppression)
            {
                if (thisER.parent == null)
                {
                    thisER.maxProbabilityAncestor = thisER;
                    thisER.minProbabilityAncestor = thisER;
                }
                else
                {
                    thisER.maxProbabilityAncestor = (thisER.probability > parent.maxProbabilityAncestor.probability) ? thisER : parent.maxProbabilityAncestor;
                    thisER.minProbabilityAncestor = (thisER.probability < parent.minProbabilityAncestor.probability) ? thisER : parent.minProbabilityAncestor;

                    if ((thisER.maxProbabilityAncestor.probability > minProbability) && (thisER.maxProbabilityAncestor.probability - thisER.minProbabilityAncestor.probability > minProbabilityDiff))
                    {
                        thisER.maxProbabilityAncestor.local_maxima = true;
                        if ((thisER.parent.local_maxima) && (thisER.maxProbabilityAncestor == thisER))
                            thisER.parent.local_maxima = false;
                    }
                    else if (thisER.probability < thisER.parent.probability)
                        thisER.minProbabilityAncestor = thisER;
                    else if (thisER.probability > thisER.parent.probability)
                        thisER.maxProbabilityAncestor = thisER;
                }
            }

            for (ERStat child = er.child; child != null; child = child.next)
                oldPrev = erSave(child, thisER, oldPrev);

            return thisER;
        }
        private ERStat erTreeFilter(InputArray image, ERStat stat, ERStat parent, ERStat prev)
        {
            Mat src = image.GetMat();
            
            if (src.Depth != DepthType.Cv8U)
            {
                return null;
            }
            Mat regToFill = new Mat(src, new Rectangle(stat.rect.X, stat.rect.Y, stat.rect.Width, stat.rect.Height));
            Mat region = new Mat(regionMask, new Rectangle(stat.rect.X, stat.rect.Y, stat.rect.Width + 2, stat.rect.Height + 2));
            region.SetTo(new MCvScalar(0));
            int newMaskVal = 255;
            FloodFillType flags = (FloodFillType) (255<<8) | FloodFillType.FixedRange | FloodFillType.MaskOnly;
            Rectangle rect = new Rectangle();


            CvInvoke.FloodFill(regToFill,
                region,
                new Point(stat.pixel % src.Cols - stat.rect.X, stat.pixel / src.Cols - stat.rect.Y),
                new MCvScalar(newMaskVal),
                out rect,
                new MCvScalar(stat.level),
                new MCvScalar(0),
                Connectivity.FourConnected,
                flags);

            rect.Width += 2;
            rect.Height += 2;
            region = new Mat(region, rect);


            VectorOfVectorOfPoint contours = new VectorOfVectorOfPoint(); 
            
            Mat hierarchy = new Mat();

            CvInvoke.FindContours(region, contours, hierarchy, RetrType.Tree, ChainApproxMethod.ChainApproxNone, new Point(0, 0));

            VectorOfPoint contourPoly = new VectorOfPoint();
            CvInvoke.ApproxPolyDP(contours[0], contourPoly, (float)Math.Min(rect.Width, rect.Height) / 17, true);

            bool wasConvex = false;
            int numInflextionPoints = 0;

            for (int p = 0; p < contourPoly.Size; p++)
            {
                int pPrev = p - 1;
                int pNext = p + 1;
                if (pPrev == -1) pPrev = contourPoly.Size - 1;
                if (pNext == contourPoly.Size) pNext = 0;

                double angleNext = Math.Atan2((contourPoly[pNext].Y - contourPoly[p].Y),
                                              (contourPoly[pNext].X - contourPoly[p].X));
                double anglePrev = Math.Atan2((contourPoly[pPrev].Y - contourPoly[p].Y),
                                              (contourPoly[pPrev].X - contourPoly[p].X));

                if (angleNext < 0) angleNext += 2d * Math.PI;

                double angle = angleNext - anglePrev;

                if (angle > 2d * Math.PI) angle -= 2d * Math.PI;
                else if (angle < 0) angle = 2d * Math.PI + Math.Abs(angle);

                if (p > 0)
                    if (((angle > Math.PI) && (!wasConvex)) || ((angle < Math.PI) && (wasConvex))) numInflextionPoints++;

                wasConvex = (angle > Math.PI);
            }
            Rectangle trivial;
            CvInvoke.FloodFill(region, null, new Point(0, 0), new MCvScalar(255), out trivial, new MCvScalar(), new MCvScalar());

            int holesArea = region.Cols * region.Rows - CvInvoke.CountNonZero(region);

            VectorOfPoint hull = new VectorOfPoint();
            CvInvoke.ConvexHull(contours[0], hull, false);
            int hullArea = (int)CvInvoke.ContourArea(hull);

            stat.holeAreaRatio = (float)holesArea / stat.area;
            stat.convexHullRatio = (float)hullArea / (float)CvInvoke.ContourArea(contours[0]);
            stat.numInflexionPoints = (float)numInflextionPoints;


            if ((classifier != null) && (stat.parent != null))
                stat.probability = classifier.Eval(stat);

            if ( ( ((classifier != null)?(stat.probability >= minProbability):true) &&
                  ((stat.area >= minArea*regionMask.Rows*regionMask.Cols) &&
                   (stat.area <= maxArea*regionMask.Rows*regionMask.Cols)) ) ||
                (stat.parent == null))
            {
                numAcceptedRegions++;
                ERStat demoStat = ERStat.DeepCopy(stat);
                regions.Add(demoStat);
                regions[regions.Count - 1].parent = parent;
                regions[regions.Count - 1].next = null;
                regions[regions.Count - 1].child = null;

                if (prev != null) prev.next = regions[regions.Count - 1];
                else if (parent != null) parent.child = regions[regions.Count - 1];

                ERStat oldPrev = null;
                ERStat thisEr = regions[regions.Count - 1];

                for (ERStat child = stat.child; child != null; child = child.next)
                    oldPrev = erTreeFilter(image, child, thisEr, oldPrev);

                return thisEr;
            }
            else
            {
                numRejectedRegions++;

                ERStat oldPrev = prev;

                for (ERStat child = stat.child; child != null; child = child.next)
                    oldPrev = erTreeFilter(image, child, parent, oldPrev);

                return oldPrev;
            }
        }
        private ERStat erTreeNonmaxSuppression(ERStat stat, ERStat parent, ERStat prev)
        {//to non max suppression to kanei anadromika gia na ksanadhmiourghsei to grafhma mono me ta local maxima
         //alliws tha mporouse apla na kanei mia epanalhpsh sth lista me ta regions kai na diagrapsei ta local maxima = false
            if ((stat.local_maxima) || (stat.parent == null))
            {
                ERStat demoStat = ERStat.DeepCopy(stat);
                regions.Add(demoStat);
                regions[regions.Count - 1].parent = parent;
                regions[regions.Count - 1].next = null;
                regions[regions.Count - 1].child = null;

                if (prev != null) prev.next = regions[regions.Count - 1]; // gia ta paidia poy exasan ta aderfia toys
                else if (parent != null) parent.child = regions[regions.Count - 1]; // gia tous goneis poy exasan ta paidia toys
                //sto vwmo tou non maximal suppression

                ERStat oldPrev = null;
                ERStat thisER = regions[regions.Count - 1];

                for (ERStat child = stat.child; child != null; child = child.next)
                    oldPrev = erTreeNonmaxSuppression(child, thisER, oldPrev);

                return thisER;
            }
            else
            {
                numRejectedRegions++;
                numAcceptedRegions--;

                ERStat oldPrev = prev;

                for (ERStat child = stat.child; child != null; child = child.next)
                    oldPrev = erTreeNonmaxSuppression(child, parent, oldPrev);

                return oldPrev;
            }
        }

        public static MyERFilter createERFilterNM1(Callback cb, int thresholdDelta, float minArea, float maxArea, float minProbability, bool nonMaxSuppression, float minProbabilityDiff)
        {
            if (!((minProbability >= 0f) && (minProbability <= 1f))) throw new Exception("Exception at createERFilterNM1 @minProbability");
            if (!((minArea < maxArea) && (minArea >= 0f) && (maxArea <= 1f))) throw new Exception("Exception at createERFilterNM1 @minArea and maxArea");
            if (!((thresholdDelta >= 0) && (thresholdDelta <= 128))) throw new Exception("Exception at createERFilterNM1 @thresholdDelta");
            if (!((minProbabilityDiff >= 0f) && (minProbabilityDiff <= 1f))) throw new Exception("Exception at createERFilterNM1 @minProbabilityDiff");

            MyERFilterNM filter = new MyERFilterNM();
            filter.setCallback(cb);
            filter.setThresholdDelta(thresholdDelta);
            filter.setMaxArea(maxArea);
            filter.setMinArea(minArea);
            filter.setMinProbability(minProbability);
            filter.setNonMaxSuppression(nonMaxSuppression);
            filter.setMinProbabilityDiff(minProbabilityDiff);

            return filter;
        }

        public static MyERFilter createERFilterNM2(Callback cb, float minProbability)
        {
            if (!((minProbability >= 0f) && (minProbability <= 1f))) throw new Exception("Exception at createERFilterNM2 @minProbability");

            MyERFilterNM filter = new MyERFilterNM();

            filter.setCallback(cb);
            filter.setMinProbability(minProbability);
            return filter;
        }

        public static Callback loadClassifierNM1(String filename)
        {
            return new MyERClassifierNM1(filename);
        }
        public static Callback loadClassifierNM2(String filename)
        {
            return new MyERClassifierNM2(filename);
        }
        public static Callback loadDummyClassifier()
        {
            return new MyDummyClassifier();
        }
    }
}
