﻿using Emgu.CV.Text;
using System;
using System.Collections.Generic;
using System.Drawing;
using WordLocRec.DataStructs;

namespace WordLocRec
{
    public class ERStat
    {
        public enum SizeType
        {
            Undefined,
            Thin,
            Fill,
            SquareLarge,
            SquareSmall,
            Long
        }
        public void setSizeType()
        {
            double aspectRatio = (double)rect.Width / rect.Height;
            if (aspectRatio < 0.4)
            {
                sizeType = SizeType.Thin;
            }
            else if (aspectRatio > 1.6)
            {
                sizeType = SizeType.Long;
            }
            else // square i.e. 0.4 <= Ar <= 1.6
            {
                double fillRate = (double)area / (rect.Width * rect.Height);
                if (fillRate > 0.7)
                {
                    sizeType = SizeType.Fill;
                }
                else if (fillRate <= 0.7 && rect.Width >= 12 && rect.Height >= 12)
                {
                    sizeType = SizeType.SquareLarge;
                }
                else
                {
                    sizeType = SizeType.SquareSmall;
                }
            }

        }
        public static ERStat TransformMCvERStat(MCvERStat stat)
        {
            ERStat er = new ERStat(stat.Level, stat.Pixel, 0, 0);
            er.area = stat.Area;
            er.perimeter = stat.Perimeter;
            er.euler = stat.Euler;
            er.probability = stat.probability;
            er.parent = stat.ParentPtr == IntPtr.Zero ? null : new ERStat();             
            er.child = null;
            er.next = null;
            er.prev = null;
            er.local_maxima = stat.LocalMaxima == (byte)1 ? true : false;
            er.maxProbabilityAncestor = null;
            er.minProbabilityAncestor = null;
            er.rect = stat.Rect;
            er.rawMoments = new double[2];
            er.rawMoments[0] = stat.RawMoments0;
            er.rawMoments[1] = stat.RawMoments1;
            er.centralMoments = new double[3];
            er.centralMoments[0] = stat.CentralMoments0;
            er.centralMoments[1] = stat.CentralMoments1;
            er.centralMoments[2] = stat.CentralMoments2;
            //er.crossings = null;
            er.medCrossings = stat.MedCrossings;
            er.pixels = null;
            er.holeAreaRatio = stat.HoleAreaRatio;
            er.convexHullRatio = stat.ConvexHullRatio;
            er.numInflexionPoints = stat.NumInflexionPoints;
            //er.parentFromMcv = stat.ParentPtr == IntPtr.Zero ? false : true;
            return er;
        }
        public static void ERtoMCvERStat(ref MCvERStat mcver, ERStat stat)
        {
            mcver.Area = stat.area;
            mcver.CentralMoments0 = stat.centralMoments[0];
            mcver.CentralMoments1 = stat.centralMoments[1];
            mcver.CentralMoments2 = stat.centralMoments[2];
            mcver.ConvexHullRatio = stat.convexHullRatio;
            mcver.Euler = stat.euler;
            mcver.HoleAreaRatio = stat.holeAreaRatio;
            mcver.Level = stat.level;
            mcver.LocalMaxima = stat.local_maxima == true ? (byte)1 : (byte)0;
            mcver.MedCrossings = stat.medCrossings;
            mcver.NumInflexionPoints = stat.numInflexionPoints;
            mcver.Perimeter = stat.perimeter;
            mcver.Pixel = stat.pixel;
            mcver.probability = stat.probability;
            mcver.RawMoments0 = stat.rawMoments[0];
            mcver.RawMoments1 = stat.rawMoments[1];
            mcver.Rect = stat.rect;
            // hope i wont have to recreate the tree
            mcver.ChildPtr = IntPtr.Zero;
            mcver.Crossings = IntPtr.Zero;
            mcver.MaxProbabilityAncestor = IntPtr.Zero;
            mcver.MinProbabilityAncestor = IntPtr.Zero;
            mcver.NextPtr = IntPtr.Zero;
            mcver.ParentPtr = stat.parent != null ? new IntPtr(1000000) : IntPtr.Zero;
            mcver.Pixels = IntPtr.Zero;
            mcver.PrevPtr = IntPtr.Zero;
        }
        public static ERStat DeepCopy(ERStat toCopy){
            ERStat er = new ERStat(toCopy.level, toCopy.pixel, 0, 0);
            er.area = toCopy.area;
            er.perimeter = toCopy.perimeter;
            er.euler = toCopy.euler;
            er.probability = toCopy.probability;
            er.parent = toCopy.parent;
            er.child = toCopy.child;
            er.next = toCopy.next;
            er.prev = toCopy.prev;
            er.local_maxima = toCopy.local_maxima;
            er.maxProbabilityAncestor = toCopy.maxProbabilityAncestor;
            er.minProbabilityAncestor = toCopy.minProbabilityAncestor;
            er.rect = toCopy.rect;
            er.rawMoments = toCopy.rawMoments;
            er.centralMoments = toCopy.centralMoments;
            er.crossings = toCopy.crossings;
            er.medCrossings = toCopy.medCrossings;
            er.pixels = toCopy.pixels;
            er.holeAreaRatio = toCopy.holeAreaRatio;
            er.convexHullRatio = toCopy.convexHullRatio;
            er.numInflexionPoints = toCopy.numInflexionPoints;
            //er.parentFromMcv = toCopy.parentFromMcv;
            er.areaVariation = toCopy.areaVariation;
            er.isCer = toCopy.isCer;
            er.lvlNum = toCopy.lvlNum;
            er.fDC = toCopy.fDC;
            er.nearDuplicate = toCopy.nearDuplicate;

            er.domThresh = toCopy.domThresh;
            er.thresholdsInER = toCopy.thresholdsInER;
            er.pixelsInEachThreshold = toCopy.pixelsInEachThreshold;
            er.sizeType = toCopy.sizeType;
            er.enhanced = toCopy.enhanced;

            er.svmResponce = toCopy.svmResponce;
            er.significantPaths = toCopy.significantPaths;

            er.ffo = toCopy.ffo;
            //er.binaryMask = toCopy.binaryMask;

            return er;
        }
        public ERStat(int level = 256, int pixel = 0, int x = 0, int y = 0)
        {
            this.level = level;
            this.pixel = pixel;

            area = 0;
            perimeter = 0;
            euler = 0;
            probability = 1.0;
            parent = null;
            child = null;
            next = null;
            prev = null;
            local_maxima = false;
            maxProbabilityAncestor = null;
            minProbabilityAncestor = null;
            areaVariation = 0;
            lvlNum = new List<Tuple<int, int>>();

            rect = new Rectangle(x, y, 1, 1);
            rawMoments[0] = 0.0;
            rawMoments[1] = 0.0;
            centralMoments[0] = 0.0;
            centralMoments[1] = 0.0;
            centralMoments[2] = 0.0;
            //crossings = new Deque<int>();
            //crossings.AddToBack(0);
            crossings = new List<int>();
            crossings.Add(0);

            thresholdsInER.Add(level);
            pixelsInEachThreshold.Add(new List<int>());
            sizeType = SizeType.Undefined;

            svmResponce = float.MinValue;
        }

        public int pixel;
        public int level;

        public int area, perimeter, euler;
        public Rectangle rect;
        public double[] rawMoments = new double[2];
        public double[] centralMoments = new double[3];
        //public Deque<int> crossings;
        public List<int> crossings;
        public float medCrossings;

        public float holeAreaRatio;
        public float convexHullRatio;
        public float numInflexionPoints;

        public List<int> pixels = new List<int>();

        public double probability;
        public float svmResponce;

        public ERStat parent, child, next, prev;

        public bool local_maxima;
        public ERStat maxProbabilityAncestor, minProbabilityAncestor;

        public bool toBeRecected = false;
        public float areaVariation;
        public bool isCer = false;
        public bool nearDuplicate = false;
        public List<Tuple<int, int>> lvlNum; // to be removed
        public double fDC; // to be removed

        // p.x. an to thresholdsInEr[5] = 70; tote pixelsInEachThreshold[5] einai mia lista me pixel poy exoun thn timh 70 sthn eikona
        public List<int> thresholdsInER = new List<int>(); // kathe threshold einai monadiko
        public List<List<int>> pixelsInEachThreshold = new List<List<int>>(); // kathe lista antistoixei se ena threshold
        public double domThresh; // to be removed
        public SizeType sizeType;
        public bool enhanced = false;
        //public Boolean parentFromMcv = false;

        public int significantPaths;
        public FullFeatureObject ffo;

        //public byte[] binaryMask;
        public bool Equals(ERStat er)
        {
            if (this.level.Equals(er.level) && this.pixel.Equals(er.pixel) && this.rect.Equals(er.rect)) return true;
            return false;
        }
    }
}
