﻿//using Emgu.CV.Structure;
//using System;
//using System.Collections.Generic;
//using System.Diagnostics;
//using System.Drawing;
//using System.Linq;
//using System.Text;
//using System.Threading.Tasks;

//namespace WordLocRec
//{
//    public class MySubdiv
//    {
//        public List<Vertex> vtx = new List<Vertex>();
//        //! All of the edges
//        public List<QuadEdge> qedges = new List<QuadEdge>();
//        public int freeQEdge = -1;
//        public int freePoint = -1;
//        public bool validGeometry = false;

//        int recentEdge = -1;
//        //! Top left corner of the bounding rect
//        PointF topLeft = new PointF();
//        //! Bottom right corner of the bounding rect
//        PointF bottomRight = new PointF();
//        public enum PTLOC
//        { PTLOC_ERROR        = -2, //!< Point location error
//           PTLOC_OUTSIDE_RECT = -1, //!< Point outside the subdivision bounding rect
//           PTLOC_INSIDE       = 0, //!< Point inside some facet
//           PTLOC_VERTEX       = 1, //!< Point coincides with one of the subdivision vertices
//           PTLOC_ON_EDGE      = 2  //!< Point on some edge
//         };

//        public enum Around
//        { NEXT_AROUND_ORG   = 0x00,
//           NEXT_AROUND_DST   = 0x22,
//           PREV_AROUND_ORG   = 0x11,
//           PREV_AROUND_DST   = 0x33,
//           NEXT_AROUND_LEFT  = 0x13,
//           NEXT_AROUND_RIGHT = 0x31,
//           PREV_AROUND_LEFT  = 0x20,
//           PREV_AROUND_RIGHT = 0x02
//         };
//        public int nextEdge(int edge)
//        {
//            Debug.Assert(((int)(edge >> 2) < qedges.Count));
//            return qedges[edge >> 2].next[edge & 3];
//        }
//        public int rotateEdge(int edge, int rotate)
//        {
//            return (edge & ~3) + ((edge + rotate) & 3);
//        }
//        public int symEdge(int edge)
//        {
//            return edge ^ 2;
//        }
//        public int getEdge(int edge, int nextEdgeType)
//        {
//            Debug.Assert((int)(edge >> 2) < qedges.Count);
//            edge = qedges[edge >> 2].next[(edge + nextEdgeType) & 3];
//            return (edge & ~3) + ((edge + (nextEdgeType >> 4)) & 3);
//        }

//        public int edgeOrg(int edge, ref PointF orgpt)
//        {
//            Debug.Assert((int)(edge >> 2) < qedges.Count);
//            int vidx = qedges[edge >> 2].pt[edge & 3];
//            if (orgpt.X != float.Epsilon && orgpt.Y != float.Epsilon)
//            {
//                Debug.Assert((int)vidx < vtx.Count);
//                orgpt = vtx[vidx].pt;
//            }
//            return vidx;
//        }

//        public int edgeDst(int edge, ref PointF dstpt)
//        {
//            Debug.Assert((int)(edge >> 2) < qedges.Count);
//            int vidx = qedges[edge >> 2].pt[(edge + 2) & 3];
//            if( dstpt.X != float.Epsilon && dstpt.Y != float.Epsilon )
//            {
//                Debug.Assert((int)vidx < vtx.Count);
//                dstpt = vtx[vidx].pt;
//            }
//            return vidx;
//        }


//        public PointF getVertex(int vertex, ref int firstEdge)
//        {
//            Debug.Assert((int)vertex < vtx.Count);
//            //if( firstEdge != null)
//                firstEdge = vtx[vertex].firstEdge;
//            return vtx[vertex].pt;
//        }

//        public MySubdiv()
//        {
//            validGeometry = false;
//            freeQEdge = 0;
//            freePoint = 0;
//            recentEdge = 0;
//        }

//        public MySubdiv(Rectangle rect)
//        {
//            validGeometry = false;
//            freeQEdge = 0;
//            freePoint = 0;
//            recentEdge = 0;

//            initDelaunay(rect);
//        }

//        public class QuadEdge
//        {
//            public int[] next = new int[4];
//            public int[] pt = new int[4];
//            public QuadEdge()
//            {
//                for (int i = 0; i < 4; i++)
//                {
//                    next[i] = 0;
//                    pt[i] = 0;
//                }
//            }

//            public QuadEdge(int edgeIdx)
//            {
//                Debug.Assert((edgeIdx & 3) == 0);

//                for (int i = 0; i < 4; i++)
//                {
//                    pt[i] = 0;
//                }
//                next[0] = edgeIdx;
//                next[1] = edgeIdx + 3;
//                next[2] = edgeIdx + 2;
//                next[3] = edgeIdx + 1;
//            }

//            public bool isFree()
//            {
//                return next[0] <= 0;
//            }
//        }

//        public class Vertex
//        {
//            public int firstEdge;
//            public int type;
//            public PointF pt;
//            public Vertex()
//            {
//                firstEdge = 0;
//                type = -1;
//            }
//            public Vertex(PointF _pt, bool _isVirtual, int _firstEdge)
//            {
//                firstEdge = _firstEdge;
//                type = _isVirtual == true ? 1 : 0;
//                pt = _pt;
//            }
//            public bool isVirtual()
//            {
//                return type > 0;
//            }
//            public bool isFree()
//            {
//                return type < 0;
//            }
//        }

//        public void splice( int edgeA, int edgeB )
//        {
//            int a_next = qedges[edgeA >> 2].next[edgeA & 3];
//            int b_next = qedges[edgeB >> 2].next[edgeB & 3];
//            int a_rot = rotateEdge(a_next, 1);
//            int b_rot = rotateEdge(b_next, 1);
//            int a_rot_next = qedges[a_rot >> 2].next[a_rot & 3];
//            int b_rot_next = qedges[b_rot >> 2].next[b_rot & 3];
//            //swap edges
//            int temp = qedges[edgeA >> 2].next[edgeA & 3];
//            qedges[edgeA >> 2].next[edgeA & 3] = qedges[edgeB >> 2].next[edgeB & 3];
//            qedges[edgeB >> 2].next[edgeB & 3] = temp;
//            // swap a rot
//            temp = qedges[a_rot >> 2].next[a_rot & 3];
//            qedges[a_rot >> 2].next[a_rot & 3] = qedges[b_rot >> 2].next[b_rot & 3];
//            qedges[b_rot >> 2].next[b_rot & 3] = temp;
//        }

//        public void setEdgePoints(int edge, int orgPt, int dstPt)
//        {
//            qedges[edge >> 2].pt[edge & 3] = orgPt;
//            qedges[edge >> 2].pt[(edge + 2) & 3] = dstPt;
//            vtx[orgPt].firstEdge = edge;
//            vtx[dstPt].firstEdge = edge ^ 2;
//        }

//        public int connectEdges( int edgeA, int edgeB )
//        {
//            int edge = newEdge();

//            splice(edge, getEdge(edgeA, (int)Around.NEXT_AROUND_LEFT));
//            splice(symEdge(edge), edgeB);
//            PointF nopoint = new PointF(float.Epsilon, float.Epsilon);
//            setEdgePoints(edge, edgeDst(edgeA, ref nopoint), edgeOrg(edgeB, ref nopoint));
//            return edge;
//        }

//        public void swapEdges( int edge )
//        {
//            int sedge = symEdge(edge);
//            int a = getEdge(edge, (int)Around.PREV_AROUND_ORG);
//            int b = getEdge(sedge, (int)Around.PREV_AROUND_ORG);

//            splice(edge, a);
//            splice(sedge, b);
//            PointF nopoint = new PointF(float.Epsilon, float.Epsilon);
//            setEdgePoints(edge, edgeDst(a, ref nopoint), edgeDst(b, ref nopoint));

//            splice(edge, getEdge(a, (int)Around.NEXT_AROUND_LEFT));
//            splice(sedge, getEdge(b, (int)Around.NEXT_AROUND_LEFT));
//        }

//        public static double triangleArea(PointF a, PointF b, PointF c)
//        {
//            return ((double)b.X - a.X) * ((double)c.Y - a.Y) - ((double)b.Y - a.Y) * ((double)c.X - a.X);
//        }

//        public int isRightOf(PointF pt, int edge)
//        {
//            PointF org = new PointF(), dst = new PointF();
//            edgeOrg(edge, ref org);
//            edgeDst(edge, ref dst);
//            double cw_area = triangleArea( pt, dst, org );

//            return cw_area > 0 ? 1 : -1;
//        }

//        public int newEdge()
//        {
//            if( freeQEdge <= 0 )
//            {
//                qedges.Add(new QuadEdge());
//                freeQEdge = (int)(qedges.Count-1);
//            }
//            int edge = freeQEdge*4;
//            freeQEdge = qedges[edge >> 2].next[1];
//            qedges[edge >> 2] = new QuadEdge(edge);
//            return edge;
//        }

//        public void deleteEdge(int edge)
//        {
//            Debug.Assert((int)(edge >> 2) < (int)qedges.Count);
//            splice(edge, getEdge(edge, (int)Around.PREV_AROUND_ORG));
//            int sedge = symEdge(edge);
//            splice(sedge, getEdge(sedge, (int)Around.PREV_AROUND_ORG));

//            edge >>= 2;
//            qedges[edge].next[0] = 0;
//            qedges[edge].next[1] = freeQEdge;
//            freeQEdge = edge;
//        }

//        public int newPoint(PointF pt, bool isvirtual, int firstEdge = 0)
//        {
//            if( freePoint == 0 )
//            {
//                vtx.Add(new Vertex());
//                freePoint = (int)(vtx.Count-1);
//            }
//            int vidx = freePoint;
//            freePoint = vtx[vidx].firstEdge;
//            vtx[vidx] = new Vertex(pt, isvirtual, firstEdge);

//            return vidx;
//        }

//        public void deletePoint(int vidx)
//        {
//            Debug.Assert((int)vidx < vtx.Count );
//            vtx[vidx].firstEdge = freePoint;
//            vtx[vidx].type = -1;
//            freePoint = vidx;
//        }

//        public int locate(PointF pt, ref int _edge, ref int _vertex)
//        {
//            int vertex = 0;

//            int i, maxEdges = (int)(qedges.Count * 4);

//            if (qedges.Count < 4)
//                throw new Exception("Subdivision is empty");

//            if (pt.X < topLeft.X || pt.Y < topLeft.Y || pt.X >= bottomRight.X || pt.Y >= bottomRight.Y)
//                throw new Exception("out of range");

//            int edge = recentEdge;
//            Debug.Assert(edge > 0);
            
//            int location = (int)PTLOC.PTLOC_ERROR;

//            int right_of_curr = isRightOf(pt, edge);
//            if( right_of_curr > 0 )
//            {
//                edge = symEdge(edge);
//                right_of_curr = -right_of_curr;
//            }

//            for( i = 0; i < maxEdges; i++ )
//            {
//                int onext_edge = nextEdge( edge );
//                int dprev_edge = getEdge(edge, (int)Around.PREV_AROUND_DST);

//                int right_of_onext = isRightOf( pt, onext_edge );
//                int right_of_dprev = isRightOf( pt, dprev_edge );

//                if( right_of_dprev > 0 )
//                {
//                    if( right_of_onext > 0 || (right_of_onext == 0 && right_of_curr == 0) )
//                    {
//                        location = (int)PTLOC.PTLOC_INSIDE;
//                        break;
//                    }
//                    else
//                    {
//                        right_of_curr = right_of_onext;
//                        edge = onext_edge;
//                    }
//                }
//                else
//                {
//                    PointF nopoint = new PointF(float.Epsilon, float.Epsilon);
//                    if( right_of_onext > 0 )
//                    {
//                        if( right_of_dprev == 0 && right_of_curr == 0 )
//                        {
//                            location = (int)PTLOC.PTLOC_INSIDE;
//                            break;
//                        }
//                        else
//                        {
//                            right_of_curr = right_of_dprev;
//                            edge = dprev_edge;
//                        }
//                    }
//                    else if( right_of_curr == 0 &&
//                            isRightOf( vtx[edgeDst(onext_edge, ref nopoint)].pt, edge ) >= 0 )
//                    {
//                        edge = symEdge( edge );
//                    }
//                    else
//                    {
//                        right_of_curr = right_of_onext;
//                        edge = onext_edge;
//                    }
//                }
//            }

//            recentEdge = edge;

//            if( location == (int)PTLOC.PTLOC_INSIDE )
//            {
//                PointF org_pt = new PointF(), dst_pt = new PointF();
//                edgeOrg(edge, ref org_pt);
//                edgeDst(edge, ref dst_pt);

//                double t1 = Math.Abs( pt.X - org_pt.X);
//                t1 += Math.Abs( pt.Y - org_pt.Y );
//                double t2 = Math.Abs( pt.X - dst_pt.X );
//                t2 += Math.Abs( pt.Y - dst_pt.Y );
//                double t3 = Math.Abs( org_pt.X - dst_pt.X );
//                t3 += Math.Abs( org_pt.Y - dst_pt.Y );

//                if( t1 < float.Epsilon )
//                {
//                    location = (int)PTLOC.PTLOC_VERTEX;
//                    PointF nopoint = new PointF(float.Epsilon, float.Epsilon);
//                    vertex = edgeOrg( edge, ref nopoint );
//                    edge = 0;
//                }
//                else if( t2 < float.Epsilon )
//                {
//                    location = (int)PTLOC.PTLOC_VERTEX;
//                    PointF nopoint = new PointF(float.Epsilon, float.Epsilon);
//                    vertex = edgeDst( edge, ref nopoint );
//                    edge = 0;
//                }
//                else if( (t1 < t3 || t2 < t3) &&
//                        Math.Abs( triangleArea( pt, org_pt, dst_pt )) < float.Epsilon )
//                {
//                    location = (int)PTLOC.PTLOC_ON_EDGE;
//                    vertex = 0;
//                }
//            }

//            if( location == (int)PTLOC.PTLOC_ERROR )
//            {
//                edge = 0;
//                vertex = 0;
//            }

//            _edge = edge;
//            _vertex = vertex;

//            return location;
//        }

//        public void getEdgeList(ref List<MCvScalar> edgeList)
//        {
//            edgeList.Clear();

//            for( int i = 4; i < qedges.Count; i++ )
//            {
//                if( qedges[i].isFree() )
//                    continue;
//                if( qedges[i].pt[0] > 0 && qedges[i].pt[2] > 0 )
//                {
//                    PointF org = vtx[qedges[i].pt[0]].pt;
//                    PointF dst = vtx[qedges[i].pt[2]].pt;
//                    edgeList.Add(new MCvScalar(org.X, org.Y, dst.X, dst.Y));
//                }
//            }
//        }

//        public int isPtInCircle3( PointF pt, PointF a, PointF b, PointF c)
//        {
//            const double eps = float.Epsilon*0.125;
//            double val = ((double)a.X * a.X + (double)a.Y * a.Y) * triangleArea( b, c, pt );
//            val -= ((double)b.X * b.X + (double)b.Y * b.Y) * triangleArea( a, c, pt );
//            val += ((double)c.X * c.X + (double)c.Y * c.Y) * triangleArea( a, b, pt );
//            val -= ((double)pt.X * pt.X + (double)pt.Y * pt.Y) * triangleArea( a, b, c );

//            return val > eps ? 1 : val < -eps ? -1 : 0;
//        }

//        public int insert(PointF pt)
//        {
//            int curr_point = 0, curr_edge = 0, deleted_edge = 0;
//            int location = locate( pt, ref curr_edge, ref curr_point );

//            if( location == (int)PTLOC.PTLOC_ERROR )
//                throw new Exception("bad size");

//            if( location == (int)PTLOC.PTLOC_OUTSIDE_RECT )
//                throw new Exception(" out of range");

//            if( location == (int)PTLOC.PTLOC_VERTEX )
//                return curr_point;

//            if( location == (int)PTLOC.PTLOC_ON_EDGE )
//            {
//                deleted_edge = curr_edge;
//                recentEdge = getEdge( curr_edge, (int)Around.PREV_AROUND_ORG );
//                curr_edge = recentEdge;
//                deleteEdge(deleted_edge);
//            }
//            else if( location == (int)PTLOC.PTLOC_INSIDE )
//                ;
//            else
//                throw new Exception("Subdiv2D::locate returned invalid location = " + location);

//            Debug.Assert( curr_edge != 0 );
//            validGeometry = false;

//            curr_point = newPoint(pt, false);
//            int base_edge = newEdge();
//            PointF nopoint = new PointF(float.Epsilon, float.Epsilon);
//            int first_point = edgeOrg(curr_edge, ref nopoint);
//            setEdgePoints(base_edge, first_point, curr_point);
//            splice(base_edge, curr_edge);

//            do
//            {
//                base_edge = connectEdges( curr_edge, symEdge(base_edge) );
//                curr_edge = getEdge(base_edge, (int)Around.PREV_AROUND_ORG);
//            }
//            while( edgeDst(curr_edge, ref nopoint) != first_point );

//            curr_edge = getEdge( base_edge, (int)Around.PREV_AROUND_ORG );

//            int i, max_edges = (int)(qedges.Count*4);

//            for( i = 0; i < max_edges; i++ )
//            {
//                int temp_dst = 0, curr_org = 0, curr_dst = 0;
//                int temp_edge = getEdge( curr_edge, (int)Around.PREV_AROUND_ORG );

//                temp_dst = edgeDst( temp_edge , ref nopoint);
//                curr_org = edgeOrg( curr_edge , ref nopoint );
//                curr_dst = edgeDst( curr_edge , ref nopoint);

//                if( isRightOf( vtx[temp_dst].pt, curr_edge ) > 0 &&
//                   isPtInCircle3( vtx[curr_org].pt, vtx[temp_dst].pt,
//                                 vtx[curr_dst].pt, vtx[curr_point].pt ) < 0 )
//                {
//                    swapEdges( curr_edge );
//                    curr_edge = getEdge( curr_edge, (int)Around.PREV_AROUND_ORG );
//                }
//                else if( curr_org == first_point )
//                    break;
//                else
//                    curr_edge = getEdge( nextEdge( curr_edge ), (int)Around.PREV_AROUND_LEFT );
//            }

//            return curr_point;
//        }

//        public void insert(ref List<PointF> ptvec)
//        {
//            for( int i = 0; i < ptvec.Count; i++ )
//                insert(ptvec[i]);
//        }

//        public void initDelaunay( Rectangle rect )
//        {
//            float big_coord = 3f * Math.Max( rect.Width, rect.Height );
//            float rx = (float)rect.X;
//            float ry = (float)rect.Y;

//            vtx = new List<Vertex>();
//            qedges = new List<QuadEdge>();

//            recentEdge = 0;
//            validGeometry = false;

//            topLeft = new PointF( rx, ry );
//            bottomRight = new PointF( rx + rect.Width, ry + rect.Height );

//            PointF ppA = new PointF( rx + big_coord, ry );
//            PointF ppB = new PointF( rx, ry + big_coord );
//            PointF ppC = new PointF( rx - big_coord, ry - big_coord );

//            vtx.Add(new Vertex());
//            qedges.Add(new QuadEdge());

//            freeQEdge = 0;
//            freePoint = 0;

//            int pA = newPoint(ppA, false);
//            int pB = newPoint(ppB, false);
//            int pC = newPoint(ppC, false);

//            int edge_AB = newEdge();
//            int edge_BC = newEdge();
//            int edge_CA = newEdge();

//            setEdgePoints( edge_AB, pA, pB );
//            setEdgePoints( edge_BC, pB, pC );
//            setEdgePoints( edge_CA, pC, pA );

//            splice( edge_AB, symEdge( edge_CA ));
//            splice( edge_BC, symEdge( edge_AB ));
//            splice( edge_CA, symEdge( edge_BC ));

//            recentEdge = edge_AB;
//        }

//        public void clearVoronoi()
//        {
//            int i, total = qedges.Count;

//            for( i = 0; i < total; i++ )
//                qedges[i].pt[1] = qedges[i].pt[3] = 0;

//            total = vtx.Count;
//            for( i = 0; i < total; i++ )
//            {
//                if( vtx[i].isVirtual() )
//                    deletePoint((int)i);
//            }

//            validGeometry = false;
//        }

//        public static PointF computeVoronoiPoint(PointF org0, PointF dst0, PointF org1, PointF dst1)
//        {
//            double a0 = dst0.X - org0.X;
//            double b0 = dst0.Y - org0.Y;
//            double c0 = -0.5*(a0 * (dst0.X + org0.X) + b0 * (dst0.Y + org0.Y));

//            double a1 = dst1.X - org1.X;
//            double b1 = dst1.Y - org1.Y;
//            double c1 = -0.5*(a1 * (dst1.X + org1.X) + b1 * (dst1.Y + org1.Y));

//            double det = a0 * b1 - a1 * b0;

//            if( det != 0 )
//            {
//                det = 1f / det;
//                return new PointF((float)((b0 * c1 - b1 * c0) * det),
//                               (float)((a1 * c0 - a0 * c1) * det));
//            }

//            return new PointF(float.MaxValue, float.MaxValue);
//        }

//        public void calcVoronoi()
//        {
//            // check if it is already calculated
//            if( validGeometry )
//                return;

//            clearVoronoi();
//            int i, total = (int)qedges.Count;

//            // loop through all quad-edges, except for the first 3 (#1, #2, #3 - 0 is reserved for "NULL" pointer)
//            for( i = 4; i < total; i++ )
//            {
//                QuadEdge quadedge = qedges[i];

//                if( quadedge.isFree() )
//                    continue;

//                int edge0 = (int)(i*4);
//                PointF org0 = new PointF(), dst0 = new PointF(), org1 = new PointF(), dst1 = new PointF();

//                if( quadedge.pt[3] != 0)
//                {
//                    int edge1 = getEdge( edge0, (int)Around.NEXT_AROUND_LEFT );
//                    int edge2 = getEdge( edge1, (int)Around.NEXT_AROUND_LEFT );

//                    edgeOrg(edge0, ref org0);
//                    edgeDst(edge0, ref dst0);
//                    edgeOrg(edge1, ref org1);
//                    edgeDst(edge1, ref dst1);

//                    PointF virt_point = computeVoronoiPoint(org0, dst0, org1, dst1);

//                    if( Math.Abs( virt_point.X ) < float.MaxValue * 0.5 &&
//                       Math.Abs( virt_point.Y ) < float.MaxValue * 0.5 )
//                    {
//                        quadedge.pt[3] = qedges[edge1 >> 2].pt[3 - (edge1 & 2)] =
//                        qedges[edge2 >> 2].pt[3 - (edge2 & 2)] = newPoint(virt_point, true);
//                    }
//                }

//                if( quadedge.pt[1] != 0)
//                {
//                    int edge1 = getEdge( edge0, (int)Around.NEXT_AROUND_RIGHT );
//                    int edge2 = getEdge( edge1, (int)Around.NEXT_AROUND_RIGHT );

//                    edgeOrg(edge0, ref org0);
//                    edgeDst(edge0, ref dst0);
//                    edgeOrg(edge1, ref org1);
//                    edgeDst(edge1, ref dst1);

//                    PointF virt_point = computeVoronoiPoint(org0, dst0, org1, dst1);

//                    if( Math.Abs( virt_point.X ) < float.MaxValue * 0.5 &&
//                       Math.Abs( virt_point.Y ) < float.MaxValue * 0.5 )
//                    {
//                        quadedge.pt[1] = qedges[edge1 >> 2].pt[1 + (edge1 & 2)] =
//                        qedges[edge2 >> 2].pt[1 + (edge2 & 2)] = newPoint(virt_point, true);
//                    }
//                }
//            }

//            validGeometry = true;
//        }

//        public static int isRightOf2(ref PointF pt, ref PointF org, ref PointF diff )
//        {
//            double cw_area = ((double)org.X - pt.X)*diff.Y - ((double)org.Y - pt.Y)*diff.X;
//            return cw_area > 0 ? 1 : -1;
//            //return (cw_area > 0) - (cw_area < 0);
//        }

//        public int findNearest(PointF pt, ref PointF nearestPt)
//        {
//            if( !validGeometry )
//                calcVoronoi();

//            int vertex = 0, edge = 0;
//            int loc = locate( pt, ref edge, ref vertex );

//            if (loc != (int)PTLOC.PTLOC_ON_EDGE && loc != (int)PTLOC.PTLOC_INSIDE)
//                return vertex;

//            vertex = 0;

//            PointF start = new PointF();
//            edgeOrg(edge, ref start);
//            PointF diff = new PointF(pt.X - start.X, pt.Y - start.Y);

//            edge = rotateEdge(edge, 1);

//            int i, total = (int)vtx.Count;

//            for( i = 0; i < total; i++ )
//            {
//                PointF t = new PointF();

//                for(;;)
//                {
//                    Debug.Assert( edgeDst(edge, ref t) > 0 );
//                    if( isRightOf2(ref t, ref start, ref diff ) >= 0 )
//                        break;

//                    edge = getEdge( edge, (int)Around.NEXT_AROUND_LEFT );
//                }

//                for(;;)
//                {
//                    Debug.Assert( edgeOrg( edge, ref t ) > 0 );

//                    if( isRightOf2(ref t, ref start, ref diff ) < 0 )
//                        break;

//                    edge = getEdge( edge, (int)Around.PREV_AROUND_LEFT );
//                }

//                PointF tempDiff = new PointF();
//                edgeDst(edge, ref tempDiff);
//                edgeOrg(edge, ref t);
//                tempDiff.X -= t.X;
//                tempDiff.Y -= t.Y;

//                if( isRightOf2( ref pt, ref t, ref tempDiff ) >= 0 )
//                {
//                    PointF nopoint = new PointF(float.Epsilon, float.Epsilon);
//                    vertex = edgeOrg(rotateEdge( edge, 3 ), ref nopoint);
//                    break;
//                }

//                edge = symEdge( edge );
//            }

//            if( (nearestPt.X != float.Epsilon && nearestPt.Y != float.Epsilon) && vertex > 0 )
//                nearestPt = vtx[vertex].pt;

//            return vertex;
//        }
       
//        public class Vec6f
//        {
//            float V0,V1,V2,V3,V4,V5;
//            public Vec6f(float v0, float v1, float v2, float v3, float v4, float v5){
//                this.V0 = v0;
//                this.V1 = v1;
//                this.V2 = v2;
//                this.V3 = v3;
//                this.V4 = v4;
//                this.V5 = v5;
//            }
//        }
//        public void getTriangleList(List<Vec6f> triangleList)
//        {
//            triangleList = new List<Vec6f>();
//            int i, total = (int)(qedges.Count*4);
//            List<bool> edgemask = new List<bool>();
//            for (int j = 0; j < total; j++)
//            {
//                edgemask.Add(false);
//            }

//            for (i = 4; i < total; i += 2)
//            {
//                if (edgemask[i])
//                    continue;
//                PointF a = new PointF(), b = new PointF(), c = new PointF();
//                int edge = i;
//                edgeOrg(edge, ref a);
//                edgemask[edge] = true;
//                edge = getEdge(edge, (int)Around.NEXT_AROUND_LEFT);
//                edgeOrg(edge, ref b);
//                edgemask[edge] = true;
//                edge = getEdge(edge, (int)Around.NEXT_AROUND_LEFT);
//                edgeOrg(edge, ref c);
//                edgemask[edge] = true;
//                triangleList.Add(new Vec6f(a.X, a.Y, b.X, b.Y, c.X, c.Y));
//            }
//        }

//        public void getVoronoiFacetList(ref List<int> idx,
//                                   ref List<List<PointF>> facetList,
//                                   ref List<PointF> facetCenters)
//        {
//            calcVoronoi();
//            facetList.Clear();
//            facetCenters.Clear();

//            List<PointF> buf = new List<PointF>();

//            int i, total;
//            if( idx.Count == 0 ){
//                i = 4;
//                total = vtx.Count;
//            }     
//            else
//            {
//                i = 0;
//                total = idx.Count;
//            }
           
//            for( ; i < total; i++ )
//            {
//                int k = idx.Count == 0? (int)i : idx[i];

//                if( vtx[k].isFree() || vtx[k].isVirtual() )
//                    continue;
//                int edge = rotateEdge(vtx[k].firstEdge, 1), t = edge;

//                // gather points
//                buf.Clear();
//                PointF nopoint = new PointF(float.Epsilon, float.Epsilon);
//                do
//                {
//                    buf.Add(vtx[edgeOrg(t, ref nopoint)].pt);
//                    t = getEdge( t, (int)Around.NEXT_AROUND_LEFT );
//                }
//                while( t != edge );

//                facetList.Add(buf);
//                facetCenters.Add(vtx[k].pt);
//            }
//        }
//        public void checkSubdiv()
//        {
//            int i, j, total = (int)qedges.Count;

//            for( i = 0; i < total; i++ )
//            {
//                QuadEdge qe = qedges[i];

//                if( qe.isFree() )
//                    continue;

//                for( j = 0; j < 4; j++ )
//                {
//                    int e = (int)(i*4 + j);
//                    int o_next = nextEdge(e);
//                    int o_prev = getEdge(e, (int)Around.PREV_AROUND_ORG );
//                    int d_prev = getEdge(e, (int)Around.PREV_AROUND_DST );
//                    int d_next = getEdge(e, (int)Around.NEXT_AROUND_DST );

//                    // check points
//                    PointF nopoint = new PointF(float.Epsilon, float.Epsilon);
//                    Debug.Assert( edgeOrg(e, ref nopoint) == edgeOrg(o_next, ref nopoint));
//                    Debug.Assert( edgeOrg(e, ref nopoint) == edgeOrg(o_prev, ref nopoint));
//                    Debug.Assert( edgeDst(e, ref nopoint) == edgeDst(d_next, ref nopoint));
//                    Debug.Assert( edgeDst(e, ref nopoint) == edgeDst(d_prev, ref nopoint));

//                    if( j % 2 == 0 )
//                    {
//                        Debug.Assert(edgeDst(o_next, ref nopoint) == edgeOrg(d_prev, ref nopoint));
//                        Debug.Assert(edgeDst(o_prev, ref nopoint) == edgeOrg(d_next, ref nopoint));
//                        Debug.Assert(getEdge(getEdge(getEdge(e, (int)Around.NEXT_AROUND_LEFT), (int)Around.NEXT_AROUND_LEFT), (int)Around.NEXT_AROUND_LEFT) == e);
//                        Debug.Assert(getEdge(getEdge(getEdge(e, (int)Around.NEXT_AROUND_RIGHT), (int)Around.NEXT_AROUND_RIGHT), (int)Around.NEXT_AROUND_RIGHT) == e);
//                    }
//                }
//    }
//}


//    }
//}
