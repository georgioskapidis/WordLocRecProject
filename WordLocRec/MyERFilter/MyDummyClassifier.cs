﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WordLocRec
{
    class MyDummyClassifier : MyERFilter.Callback
    {
        public MyDummyClassifier() { }

        public override double Eval(ERStat stat)
        {
            if (stat.area == 0)
                return 0d;
            else
                return 1d;
        }
        public override void PrintWatch()
        {
            
        }
    }
}
