﻿//using Emgu.CV;
//using Emgu.CV.CvEnum;
//using Emgu.CV.ML;
//using Emgu.CV.Structure;
//using Emgu.CV.Util;
//using System;
//using System.Collections.Generic;
//using System.Diagnostics;
//using System.Drawing;
//using System.Linq;
//using System.Text;
//using System.Threading.Tasks;
//using WordLocRec.Tools;

//namespace WordLocRec
//{
//    class ERGroupingKaratzas
//    {
//        #region variables and enums
//        public static double RELATIVE_ERROR_FACTOR = 100d;
//        public static int MAX_GROUP_ELEMENTS = 50;
//        public static int TABSIZE = 10000;
//        public static double M_LN10 = 2.30258509299404568401799145468436421;
//        public static double[] inv = new double[TABSIZE]; /* table to keep computed inverse values */
//        public enum method_codes
//        {
//            METHOD_METR_SINGLE = '0',
//            METHOD_METR_AVERAGE = '1'
//        };
//        public enum metrics
//        {
//            // metrics
//            METRIC_EUCLIDEAN       =  0,
//            METRIC_CITYBLOCK       =  1,
//            METRIC_SEUCLIDEAN      =  2,
//            METRIC_SQEUCLIDEAN     =  3
//        };
//        #endregion

//        static bool double_equal(double a, double b)
//        {
//            double abs_diff, aa, bb, abs_max;

//            /* trivial case */
//            if (a == b) return true;

//            abs_diff = Math.Abs(a - b);
//            aa = Math.Abs(a);
//            bb = Math.Abs(b);
//            abs_max = aa > bb ? aa : bb;

//            /* DBL_MIN is the smallest normalized number, thus, the smallest
//               number whose relative error is bounded by DBL_EPSILON. For
//               smaller numbers, the same quantization steps as for DBL_MIN
//               are used. Then, for smaller numbers, a meaningful "relative"
//               error should be computed by dividing the difference by DBL_MIN. */
//            if (abs_max < Double.MinValue) abs_max = double.MinValue;

//            /* equal if relative error <= factor x eps */
//            return (abs_diff / abs_max) <= (RELATIVE_ERROR_FACTOR * double.Epsilon);
//        }
//        static double log_gamma_lanczos(double x)
//        {
//            double[] q = new double[7] { 75122.6331530, 80916.6278952, 36308.2951477,
//                                    8687.24529705, 1168.92649479, 83.8676043424,
//                                    2.50662827511 };
//            double a = (x+0.5) * Math.Log(x+5.5) - (x+5.5);
//            double b = 0.0;
//            int n;

//            for(n=0;n<7;n++)
//            {
//                a -= Math.Log( x + (double) n );
//                b += q[n] * Math.Pow( x, (double) n );
//            }
//            return a + Math.Log(b);
//        }
//        static double log_gamma_windschitl(double x)
//        {
//            return 0.918938533204673 + (x - 0.5) * Math.Log(x) - x
//                   + 0.5 * x * Math.Log(x * Math.Sinh(1 / x) + 1 / (810.0 * Math.Pow(x, 6.0)));
//        }
//        static double log_gamma(double x)
//        {
//            double result = x > 15 ? log_gamma_windschitl(x) : log_gamma_lanczos(x);
//            return result;
//        }
        
//        static double NFA(int n, int k, double p, double logNT)
//        {
//            double tolerance = 0.1;       /* an error of 10% in the result is accepted */
//            double log1term, term, bin_term, mult_term, bin_tail, err, p_term;
//            int i;

//            if (p <= 0)
//                p = Double.MinValue;
//            if (p >= 1)
//                p = 1 - double.Epsilon;

//            /* check parameters */
//            if (n < 0 || k < 0 || k > n || p <= 0.0 || p >= 1.0)
//            {
//                //CV_Error(Error::StsBadArg, "erGrouping wrong n, k or p values in NFA call!");
//                throw new Exception("An error");
//            }

//            /* trivial cases */
//            if (n == 0 || k == 0) return -logNT;
//            if (n == k) return -logNT - (double)n * Math.Log10(p);

//            /* probability term */
//            p_term = p / (1.0 - p);

//            /* compute the first term of the series */
//            log1term = log_gamma((double)n + 1.0) - log_gamma((double)k + 1.0)
//                        - log_gamma((double)(n - k) + 1.0)
//                        + (double)k * Math.Log(p) + (double)(n - k) * Math.Log(1.0 - p);
//            term = Math.Exp(log1term);

//            /* in some cases no more computations are needed */
//            if (double_equal(term, 0.0))              /* the first term is almost zero */
//            {
//                if ((double)k > (double)n * p)     /* at begin or end of the tail?  */
//                    return -log1term / M_LN10 - logNT;  /* end: use just the first term  */
//                else
//                    return -logNT;                      /* begin: the tail is roughly 1  */
//            }

//            /* compute more terms if needed */
//            bin_tail = term;
//            for (i = k + 1; i <= n; i++)
//            {
//                bin_term = (double)(n - i + 1) * (i < TABSIZE ?
//                            (inv[i] != 0.0 ? inv[i] : (inv[i] = 1.0 / (double)i)) :
//                            1.0 / (double)i);

//                mult_term = bin_term * p_term;
//                term *= mult_term;
//                bin_tail += term;
//                if (bin_term < 1.0)
//                {
//                    err = term * ((1.0 - Math.Pow(mult_term, (double)(n - i + 1))) /
//                                    (1.0 - mult_term) - 1.0);
//                    if (err < tolerance * Math.Abs(-Math.Log10(bin_tail) - logNT) * bin_tail) break;
//                }
//            }
//            return -Math.Log10(bin_tail) - logNT;
//        }

//        public class Minibox
//        {
//            List<float> edge_begin = new List<float>();
//            List<float> edge_end = new List<float>();
//            bool initialized;

//            public Minibox()
//            {
//                initialized = false;
//            }
//            public void checkIn(List<float> p)
//            {
//                if (!initialized)
//                {
//                    for (int i = 0; i < p.Count; i++)
//                    {
//                        edge_begin.Add(p[i]);
//                        edge_end.Add(p[i] + 0.00000001f);
//                        initialized = true;
//                    }
//                }
//                else
//                {
//                    for (int i = 0; i < p.Count; i++)
//                    {
//                        edge_begin[i] = Math.Min(p[i], edge_begin[i]);
//                        edge_end[i] = Math.Max(p[i], edge_end[i]);
//                    }
//                }
//            }
//            public double volume()
//            {
//                double volume = 1;
//                for (int i = 0; i < edge_begin.Count; i++)
//                {
//                    volume = volume * (edge_end[i] - edge_begin[i]);
//                }
//                return volume;
//            }
//        }
        
//        public class node
//        {
//            public int node1, node2;
//            public double dist;
//            public static bool operator <(node a, node b)
//            {
//                return a.dist < b.dist;
//            }
//            public static bool operator >(node a, node b)
//            {
//                return a.dist > b.dist;
//            }
//        }

//        // missing auto array pointer

//        public class cluster_result
//        {
//            List<node> Z;
//            int pos;

//            public cluster_result(int size)
//            {
//                Z = new List<node>(size);
//                pos = 0;
//            }
//            public void append(int node1, int node2, double dist)
//            {
//                Z[pos].node1 = node1;
//                Z[pos].node2 = node2;
//                Z[pos].dist = dist;
//                pos++;
//            }

//            public node get(int idx)
//            {
//                return Z[idx];
//            }
//            public void sqrt()
//            {
//                for (int i = 0; i < pos; i++)
//                {
//                    Z[i].dist = Math.Sqrt(Z[i].dist);
//                }
//            }
//            public void sqrt(double d)
//            {
//                sqrt();
//            }

//            public void StableSort()
//            {
//                Z.OrderBy(x => x);
//            }
//        }

//        public class doubly_linked_list
//        {
//            public int start;
//            public List<int> succ;
//            private List<int> pred;

//            public doubly_linked_list(int size)
//            {
//                succ = new List<int>(size + 1);
//                pred = new List<int>(size + 1);

//                for (int i = 0; i < size; i++)
//                {
//                    pred[i + 1] = i;
//                    succ[i] = i + 1;
//                }
//                start = 0;
//            }
//            public void Remove(int idx)
//            {
//                if (idx == start)
//                {
//                    start = succ[idx]; // dld assign sto epomeno
//                }
//                else
//                {
//                    succ[pred[idx]] = succ[idx];
//                    pred[succ[idx]] = pred[idx];
//                }
//                succ[idx] = 0;
//            }
//            public bool isInactive(int idx)
//            {
//                return (succ[idx] == 0);
//            }
//        }

//        public class union_find
//        {
//            private List<int> parent;
//            int nextParent;

//            public void init(int size)
//            {
//                parent = new List<int>(2 * size - 1);
//                nextParent = size;
//            }

//            public int Find(int idx)
//            {
//                if (parent[idx] != 0)
//                {
//                    int p = idx;
//                    idx = parent[idx];
//                    if (parent[idx] != 0)
//                    {
//                        do
//                        {
//                            idx = parent[idx];
//                        } while (parent[idx] != 0);
//                        do
//                        {
//                            int temp = parent[p];
//                            parent[p] = idx;
//                            p = temp;
//                        } while (parent[p] != idx);
//                    }
//                }
//                return idx;
//            }
//            public void Union(int node1, int node2)
//            {
//                parent[node1] = parent[node2] = nextParent++;
//            }
//        }       

//        public static void MST_linkage_core_vector(int N, ref dissimilarity dist, ref cluster_result Z2)
//        {
//            int i;
//            int idx2;
//            doubly_linked_list activeNodes = new doubly_linked_list(N);
//            List<double> d = new List<double>(N);
//            for (int di = 0; di < N; di++)
//                d[di] = 0d;


//            int prev_node;
//            double min;

//            //first iteration

//            idx2 = 1;
//            d[1] = dist.parenth(0, 1);
//            min = d[1];
//            for (i = 2; min != min && i < N; i++)
//            {
//                min = dist.parenth(0, i);
//                d[i] = min;
//                idx2 = i;
//            }

//            for (; i < N; i++)
//            {
//                d[i] = dist.parenth(0, i);
//                if (d[i] < min)
//                {
//                    min = d[i];
//                    idx2 = i;
//                }
//            }
//            Z2.append(0, idx2, min);

//            for (int j = 1; j < N - 1; j++)
//            {
//                prev_node = idx2;
                
//                activeNodes.Remove(prev_node);

//                idx2 = activeNodes.succ[0];
//                min = d[idx2];

//                for (i = idx2; min != min && i < N; i = activeNodes.succ[i]) // for NaN eliminations
//                {
//                    min = d[i] = dist.parenth(i, prev_node);
//                    idx2 = i;
//                }

//                for (; i < N; i = activeNodes.succ[i])
//                {
//                    double tmp = dist.parenth(i, prev_node);
//                    if (d[i] > tmp)
//                        d[i] = tmp;
//                    if (d[i] < min)
//                    {
//                        min = d[i];
//                        idx2 = i;
//                    }
//                }
//                Z2.append(prev_node, idx2, min);
//            }
//        }

//        public class linkage_output
//        {
//            private double[] Z;
//            private int pos;

//            public linkage_output(double[] Z)
//            {
//                this.Z = Z;
//                pos = 0;
//            }

//            public void Append(int node1, int node2, double dist, double size)
//            {
//                if (node1 < node2)
//                {
//                    Z[pos++] = (double)node1;
//                    Z[pos++] = (double)node2;
//                }
//                else
//                {
//                    Z[pos++] = (double)node2;
//                    Z[pos++] = (double)node1;
//                }
//                Z[pos++] = dist;
//                Z[pos++] = size;
//            }
//        }

//        public static void generateDendrogram(double[] Z, ref cluster_result Z2, int N)
//        {
//            union_find nodes = new union_find();
//            // sto sorting isws na prepei na afairesw to teleutaio stoixeio toy pinaka prin to efarmosw
//            Z2.StableSort();

//            nodes.init(N);
            
//            linkage_output output = new linkage_output(Z);
//            int node1, node2;
            
//            for (int i = 0; i < N - 1; i++)
//            {
//                node1 = nodes.Find(Z2.get(i).node1);
//                node2 = nodes.Find(Z2.get(i).node2);

//                nodes.Union(node1, node2);

//                output.Append(node1, node2, Z2.get(i).dist, 2 * sizeof(int));
//            }
//        }

//        public class dissimilarity 
//        {
//            public double[] Xa;
//            public List<double> Xnew;
//            public int dim; // size_t saves many statis_cast<> in products
//            public int N;
//            public List<int> members;

//            //cluster_result[] postprocessfn;
//            //void (cluster_result::*postprocessfn) (const double) const;
//            Action<double> postprocessfn;
//            public delegate void postprocessFun(double d);
//            double postprocessarg;

//            //double (dissimilarity::*distfn) (const int_fast32_t, const int_fast32_t) const;
//            public delegate double distFun(int i, int j);
//            Func<int, int, double> distfn;

//            List<double> precomputed;

//            double[] V;
//            double[] V_data;

//            public dissimilarity (double[] _Xa, int _Num, int _dim, List<int>_members, int method, int metric, bool temp_point_array)
//            {
//                this.Xa = _Xa;
//                this.dim = _dim;
//                this.N = _Num;
//                this.V = null;
//                this.postprocessfn = null;
//                this.members = _members;
    
//                switch ((method_codes)method) 
//                {
//                    case method_codes.METHOD_METR_SINGLE: // only single linkage allowed here but others may come...
//                        postprocessfn = null; // default
//                        switch ((metrics)metric)
//                        {
//                            case metrics.METRIC_EUCLIDEAN:
//                                set_euclidean();
//                                break;
//                            case metrics.METRIC_SEUCLIDEAN:
//                                distfn = sqeuclidean;
//                                break;
//                            case metrics.METRIC_SQEUCLIDEAN:
//                                distfn = sqeuclidean;
//                                break;
//                            case metrics.METRIC_CITYBLOCK:
//                                set_cityblock();
//                                break;
//                        }
//                        break;
//                    default:
//                        postprocessfn = null; // default
//                        switch ((metrics)metric)
//                        {
//                            case metrics.METRIC_EUCLIDEAN:
//                                set_euclidean();
//                                break;
//                            case metrics.METRIC_SEUCLIDEAN:
//                                distfn = sqeuclidean;
//                                break;
//                            case metrics.METRIC_SQEUCLIDEAN:
//                                distfn = sqeuclidean;
//                                break;
//                            case metrics.METRIC_CITYBLOCK:
//                                set_cityblock();
//                                break;
//                        }
//                        break;
//                }

//                if (temp_point_array)
//                {
//                    Xnew = new List<double>((N-1) * dim);
//                }
//            }

//            ~dissimilarity()
//            {
//                V = null;
//            }
            
//            public double parenth(int i, int j)
//            {
//                return (this.distfn)(i,j);
//            }

//            public double X (int i, int j)
//            {
//                return Xa[i * dim + j];
//            }

//            //public bool Xb (int i, int j)
//            //{
//            //    return reinterpret_cast<bool>(Xa)[i * dim + j];
//            //}

//            //public double Xptr(int i, int j)
//            //{
//            //    return Xa + i * dim + j;
//            //}

//            public void postprocess(cluster_result Z2)
//            {
//                if (postprocessfn != null)
//                {
//                    Z2.sqrt();
//                    //(Z2.postprocessfn)(postprocessarg);
//                }
//            }

//            public double sqeuclidean(int i, int j)
//            {
//                double sum = 0;
//                //double[] Pi = Xa + i * dim;
//                //double[] Pj = Xa + j * dim;
//                for (int k = 0; k < dim; k++)
//                {
//                    double diff = Xa[i * dim + k] - Xa[j * dim + k];
//                    //double diff = Pi[k] - Pj[k];
//                    sum += diff * diff;
//                }
//                return sum;
//            }
//            private void set_euclidean()
//            {
//                distfn = sqeuclidean;
//                //postprocessfn = cluster_result.sqrt;
//            }

//            private void set_cityblock()
//            {
//                distfn = cityblock;
//            }

//            private double seuclidean(int i, int j)
//            {
//                double sum = 0;
//                for (int k=0; k<dim; k++)
//                {
//                    double diff = X(i, k) - X(j, k);
//                    sum += diff * diff / V_data[k];
//                }
//                return sum;
//            }

//            private double cityblock(int i, int j)
//            {
//                double sum = 0;
//                for (int k=0; k<dim; k++)
//                {
//                    sum += Math.Abs(X(i, k) - X(j, k));
//                }
//                return sum;
//            }
//        }

//        public static int linkage_vector(double[] X, int N, int dim, double[] Z, int method, int metric)
//        {

//            Debug.Assert(N >=1);
//            Debug.Assert(N <= int.MaxValue / 4);
//            Debug.Assert(dim >= 1);

//            try
//            {
//                cluster_result Z2 = new cluster_result(N-1);
//                List<int> members = new List<int>();
//                dissimilarity dist = new dissimilarity(X, N, dim, members, method, metric, false);
//                MST_linkage_core_vector(N, ref dist, ref Z2);
//                dist.postprocess(Z2);
//                generateDendrogram(Z, ref Z2, N);
//            } // try
//            catch (OutOfMemoryException)
//            {
//                Debug.Write("Not enough Memory for erGrouping hierarchical clustering structures!");
//            }
//            catch(Exception e)
//            {
//                Debug.WriteLine("Uncaught exception in erGrouping!");
//                Debug.WriteLine(e.ToString());
//            }
//            return 0;
//        }


//        public class ERFeatures
//        {
//            public int area;
//            public Point center;
//            public Rectangle rect;
//            public float intensityMean;
//            public float intensityStd;
//            public float boundaryIntensityMean;
//            public float boundaryIntensityStd;
//            public double strokeMean;
//            public double strokeStd;
//            public double gradientMean;
//            public double gradientStd;
//            public double axialRatio;
//            public double convexHullRatio;
//            public int convexities;
//            public double[] huMoments = new double[7];
//        }
//        public class HClusters
//        {
//            public int num_elem;
//            public List<int> elements;
//            public int nfa;
//            public float dist, dist_ext;
//            public double volume, volume_ext;
//            public List<List<float>> points;
//            public bool max_meaningful;
//            public List<int> max_in_branch;
//            public int min_nfa_in_branch, node1, node2;
//            public double probability;

//        }
        

//        public static bool guo_hall_thinning(Mat img, ref Mat skeleton)
//        {
//            Debug.Assert(img.NumberOfChannels == 1 && img.Depth == DepthType.Cv8U);
//            byte[] imgData = img.GetData();
//            byte img_ptr = 0;
//            byte[] skelData = new byte[img.GetData().Length];

//            for (int row = 0; row < img.Rows; ++row)
//            {
//                for (int col = 0; col < img.Cols; ++col)
//                {
//                    if (img_ptr > 0 && img_ptr < imgData.Length)
//                    {
//                        int key = row * img.Cols + col;
//                        if ((col > 0 && imgData[img_ptr] != imgData[key - 1]) ||
//                            (col < img.Cols-1 && imgData[img_ptr] != imgData[key + 1]) ||
//                            (row > 0 && imgData[img_ptr] != imgData[key - img.Cols]) ||
//                            (row < img.Rows-1 && imgData[img_ptr] != imgData[key + img.Cols]))
//                        {
//                            skelData[img_ptr] = 255;
//                        }
//                        else
//                        {
//                            skelData[img_ptr] = 128;
//                        }
//                    }
//                    img_ptr++;
//                }
//            }

//            int max_iters = 10000;
//            int niters = 0;
//            bool changed = false;
//            int skelCols = img.Cols;
//            int skelRows = img.Rows;
//            /// list of keys to set to 0 at the end of the iteration
//            List<int> cols_to_set = new List<int>();
//            List<int> rows_to_set = new List<int>();

//            while (changed && niters < max_iters)
//            {
//                changed = false;
//                for (short iter = 0; iter < 2; ++iter)
//                {
//                    //uchar *skeleton_ptr = skeleton.data; use skelData instead
//                    int skeleton_ptr = 0;
//                    rows_to_set.Clear();
//                    cols_to_set.Clear();
//                    // for each point in skeleton, check if it needs to be changed
//                    for (int row = 0; row < skeleton.Rows; ++row)
//                    {
//                        for (int col = 0; col < skeleton.Cols; ++col)
//                        {
//                            if (skelData[skeleton_ptr] == 255)
//                            {
//                                bool p2, p3, p4, p5, p6, p7, p8, p9;
//                                p2 = (skelData[(row-1) * skelCols + col]) > 0;
//                                p3 = (skelData[(row-1) * skelCols + col+1]) > 0;
//                                p4 = (skelData[row     * skelCols + col+1]) > 0;
//                                p5 = (skelData[(row+1) * skelCols + col+1]) > 0;
//                                p6 = (skelData[(row+1) * skelCols + col]) > 0;
//                                p7 = (skelData[(row+1) * skelCols + col-1]) > 0;
//                                p8 = (skelData[row     * skelCols + col-1]) > 0;
//                                p9 = (skelData[(row-1) * skelCols + col-1]) > 0;

//                                int C  = ((!p2 & (p3 | p4)) ? 1 : 0) + ((!p4 & (p5 | p6)) ? 1 : 0) +
//                                    ((!p6 & (p7 | p8)) ? 1 : 0) + ((!p8 & (p9 | p2)) ? 1 : 0);
//                                int N1 = ((p9 | p2) ? 1 : 0) + ((p3 | p4) ? 1 : 0) + ((p5 | p6) ? 1 : 0) + ((p7 | p8) ? 1 : 0);
//                                int N2 = ((p2 | p3) ? 1 : 0) + ((p4 | p5) ? 1 : 0) + ((p6 | p7) ? 1 : 0) + ((p8 | p9) ? 1 : 0);
//                                int N  = N1 < N2 ? N1 : N2;
//                                int m  = iter == 0 ? (((p6 | p7 | !p9) & p8) ? 1 : 0) : (!((p2 | p3 | !p5) & p4) ? 1 : 0);

//                                if ((C == 1) && (N >= 2) && (N <= 3) && (m == 0))
//                                {
//                                    cols_to_set.Add(col);
//                                    rows_to_set.Add(row);
//                                }
//                            }
//                            skeleton_ptr++;
//                        }
//                    }

//                    // set all points in rows_to_set (of skel)
//                    int rows_to_set_size = (int)rows_to_set.Count;
//                    for (int pt_idx = 0; pt_idx < rows_to_set_size; ++pt_idx)
//                    {
//                        if (!changed)
//                            changed = (skelData[rows_to_set[pt_idx] * skelCols + cols_to_set[pt_idx]]) > 0;

//                        int key = rows_to_set[pt_idx] * skelCols + cols_to_set[pt_idx];
//                        skelData[key] = 0;
                        
//                        if (cols_to_set[pt_idx] > 0 && skelData[key - 1] == 128) // left
//                            skelData[key - 1] = 255;
//                        if (cols_to_set[pt_idx] < skelCols-1 && skelData[key + 1] == 128) // right
//                            skelData[key + 1] = 255;
//                        if (rows_to_set[pt_idx] > 0 && skelData[key - skelCols] == 128) // up
//                            skelData[key - skelCols] = 255;
//                        if (rows_to_set[pt_idx] < skelRows-1 && skelData[key + skelCols] == 128) // down
//                            skelData[key + skelCols] = 255;
//                    }

//                    if ((niters++) >= max_iters) // we have done!
//                    break;
//                }
//            }
//            skeleton.SetTo(skelData);
//            CvInvoke.Threshold(skeleton, skeleton, 0.5d, 255, ThresholdType.Binary);
//            //skeleton = (skeleton != 0);
//            return true;
//        }

//        public static float extractFeatures(ref Mat bgr, ref Mat channel, List<ERStat> regions, ref List<ERFeatures> features)
//        {
//            // assert correct image type
//            Debug.Assert(( channel.Depth == DepthType.Cv8U && channel.NumberOfChannels == 1 ) && ( bgr.Depth == DepthType.Cv8U && bgr.NumberOfChannels == 3));
//            Debug.Assert( channel.Size.Equals(bgr.Size));

//            if (regions.Count == 0 || features.Count != 0) return 0f;

//            Mat grey = new Mat();
//            CvInvoke.CvtColor(bgr, grey, ColorConversion.Bgr2Gray);

//            Mat gradient_magnitude = new Mat();
//            ColorConversionWrapper.ColorConversions(bgr, ColorConversionWrapper.ManualConversions.Gradient, ref gradient_magnitude);

//            Mat region_mask = new Mat(grey.Rows+2, grey.Cols+2, DepthType.Cv8U, 1);
//            region_mask.SetTo(new MCvScalar(0));

//            float max_stroke = 0;

//            for (int r=0; r < regions.Count; r++)
//            {
//                ERFeatures f = new ERFeatures();
//                ERStat stat = regions[r];

//                f.area = stat.area;
//                f.rect = stat.rect;
//                f.center = new Point(f.rect.X + (f.rect.Width / 2), f.rect.Y + (f.rect.Height / 2));

//                if (regions[r].parent != null)
//                {

//                    //Fill the region and calculate features
//                    Mat region = new Mat(region_mask, new Rectangle(stat.rect.X, stat.rect.Y, stat.rect.Width + 2, stat.rect.Height + 2));
//                    region.SetTo(new MCvScalar(0));
//                    Mat regToFill = new Mat(channel, new Rectangle(stat.rect.X, stat.rect.Y, stat.rect.Width, stat.rect.Height));
                    
//                    FloodFillType flags = (FloodFillType)(255 << 8) | FloodFillType.FixedRange | FloodFillType.MaskOnly;
//                    Rectangle rect = new Rectangle();

//                    Utils.FloodFill(regToFill, region, new Point(stat.pixel % channel.Cols - stat.rect.X, stat.pixel / channel.Cols - stat.rect.Y), new MCvScalar(255), out rect, new MCvScalar(stat.level), new MCvScalar(0),Connectivity.FourConnected, flags);

//                    rect.Width += 2;
//                    rect.Height += 2;
                    
//                    Mat rect_mask = new Mat(region_mask, new Rectangle(stat.rect.X + 1, stat.rect.Y + 1, stat.rect.Width, stat.rect.Height));

//                    Mat greyRoi = new Mat(grey, stat.rect);
//                    MCvScalar mean = new MCvScalar(),std = new MCvScalar();
//                    CvInvoke.MeanStdDev(greyRoi, ref mean, ref std, rect_mask);
//                    f.intensityMean = (float)mean.V0;
//                    f.intensityStd  = (float)std.V0;

//                    Mat tmp = new Mat(), bw = new Mat();
//                    rect_mask.CopyTo(bw);
//                    CvInvoke.DistanceTransform(bw, tmp, null, DistType.L1, 3);
//                    //distanceTransform(bw, tmp, DIST_L1,3); //L1 gives distance in round integers while L2 floats

//                    // Add border because if region span all the image size skeleton will crash
//                    CvInvoke.CopyMakeBorder(bw, bw, 5, 5, 5, 5, BorderType.Constant, new MCvScalar(0));

//                    Mat skeleton = new Mat(bw.Height, bw.Width, DepthType.Cv8U, 1);
//                    guo_hall_thinning(bw, ref skeleton);
                    
//                    Mat mask = new Mat();

//                    Mat skeletonNoBorder = new Mat(skeleton, new Rectangle(5, 5, bw.Cols - 10, bw.Rows - 10));
//                    skeletonNoBorder.CopyTo(mask);

//                    skeletonNoBorder = new Mat(bw, new Rectangle(5, 5, bw.Cols - 10, bw.Rows - 10));
//                    skeletonNoBorder.CopyTo(bw);

//                    CvInvoke.MeanStdDev(tmp, ref mean, ref std, mask);
//                    f.strokeMean = mean.V0;
//                    f.strokeStd  = std.V0;

//                    if (f.strokeMean > max_stroke)
//                        max_stroke = (float)f.strokeMean;

//                    Mat element = CvInvoke.GetStructuringElement(ElementShape.Rectangle, new Size(5,5), new Point(2,2));

//                    // to boundary isoutai me: dilate(img) - img 
//                    MCvScalar defBorderValue = CvInvoke.MorphologyDefaultBorderValue;
//                    CvInvoke.Dilate(rect_mask, tmp, element, new Point(-1, -1), 1, BorderType.Constant, defBorderValue);
//                    CvInvoke.AbsDiff(tmp, rect_mask, tmp);
                    
//                    CvInvoke.MeanStdDev( greyRoi, ref mean, ref std, tmp);
//                    f.boundaryIntensityMean = (float)mean.V0;
//                    f.boundaryIntensityStd  = (float)std.V0;

//                    Mat tmp2 = new Mat();
//                    CvInvoke.Dilate(rect_mask, tmp, element, new Point(-1, -1), 1, BorderType.Constant, defBorderValue);
//                    CvInvoke.Erode(rect_mask, tmp2, element, new Point(-1, -1), 1, BorderType.Constant, defBorderValue);
//                    CvInvoke.AbsDiff(tmp, tmp2, tmp);

//                    // gia to gradient: dilate(img) - erode(img)
//                    Mat gradRoi = new Mat(gradient_magnitude, stat.rect);
//                    CvInvoke.MeanStdDev(gradRoi, ref mean, ref std, tmp);
//                    f.gradientMean = mean.V0;
//                    f.gradientStd  = std.V0;

//                    CvInvoke.CopyMakeBorder(bw, bw, 5, 5, 5, 5, BorderType.Constant, new MCvScalar(0));

//                    VectorOfVectorOfPoint contours0 = new VectorOfVectorOfPoint();
//                    Mat hierarchy = new Mat();

//                    CvInvoke.FindContours(bw, contours0, hierarchy, RetrType.Tree, ChainApproxMethod.ChainApproxSimple);


//                    RotatedRect rrect = CvInvoke.MinAreaRect(contours0[0]);

//                    f.axialRatio = Math.Max(rrect.Size.Width, rrect.Size.Height) / Math.Min(rrect.Size.Width, rrect.Size.Height);

//                    MCvMoments mu = CvInvoke.Moments(contours0[0]);
//                    Mat hu = new Mat();
//                    CvInvoke.HuMoments(mu, hu);
//                    hu.CopyTo(f.huMoments);

//                    VectorOfPoint hull = new VectorOfPoint();
//                    CvInvoke.ConvexHull(contours0[0], hull);

//                    f.convexHullRatio = (float)CvInvoke.ContourArea(hull) / CvInvoke.ContourArea(contours0[0]);
//                    //vector<Vec4i> cx;
                    
//                    Mat cx = new Mat();
//                    VectorOfInt hull_idx = new VectorOfInt();
//                    //TODO check epsilon parameter of approxPolyDP (set empirically) : we want more precission
//                    //     if the region is very small because otherwise we'll loose all the convexities
//                    VectorOfPoint contourPoly = new VectorOfPoint();
//                    CvInvoke.ApproxPolyDP(contours0[0], contourPoly, (float)Math.Min(rrect.Size.Width, rrect.Size.Height) / 17, true);

//                    CvInvoke.ConvexHull(contours0[0], hull_idx, false, false);

//                    f.convexities = 0;

//                    if (hull_idx.Size > 2)
//                        if (contours0[0].Size > 3)
//                            CvInvoke.ConvexityDefects(contours0[0], hull_idx, cx);
//                            //convexityDefects(contours0[0],hull_idx,cx);
//                    f.convexities = (int)cx.Rows;

//                    rect_mask.SetTo(new MCvScalar(0));

//                } else {

//                    f.intensityMean = 0;
//                    f.intensityStd  = 0;

//                    f.strokeMean = 0;
//                    f.strokeStd = 0;

//                    f.boundaryIntensityMean = 0;
//                    f.boundaryIntensityStd  = 0;

//                    f.gradientMean = 0;
//                    f.gradientStd  = 0;
//                }

//                features.Add(f);
//            }

//            return max_stroke;
//        }


//        public class MaxMeaningfulClustering
//        {
//            public int Method, Metric;

//            private double minProbability;
//            private List<ERFeatures> regions;
//            private Size imSize;
//            private Boost boost;

//            public MaxMeaningfulClustering(int Method, int Metric, List<ERFeatures> regions, Size imSize, String filename, double minProbability)
//            {
//                this.minProbability = minProbability;
//                boost = new Boost();
//                FileStorage fs = new FileStorage(filename, FileStorage.Mode.Read);
//                FileNode fn = fs.GetFirstTopLevelNode();
//                boost.Read(fn);
//                if (boost == null)
//                {
//                    throw new Exception("Exception at reading the classifier (1) at file: " + filename);
//                }
//                this.imSize = imSize;
//                this.regions = regions;
//                this.Method = Method;
//                this.Metric = Metric;

//            }
            

//            public void operParenthesis(double[] data, int num, int dim, int method, int metric, ref List<List<int>> meaningfulClusters)
//            {
//                double[] Z = new double[(num - 1) * 4];

//                linkage_vector(data, num, dim, Z, method, metric);

//                List<HClusters> mergeInfo = new List<HClusters>();

//                buildMergeInfo(Z, data, num, dim, false, ref mergeInfo, ref meaningfulClusters);

//            }

//            // helper functions
//            public void buildMergeInfo(double[] Z, double[] X, int N, int dim, bool use_full_merge_rule, ref List<HClusters> merge_info, ref List<List<int>> meaningful_clusters)
//            {
//                // walk the whole dendogram
//                for (int i = 0; i < (N - 1) * 4; i = i + 4)
//                {
//                    HClusters cluster = new HClusters();
//                    cluster.num_elem = (int)Z[i + 3]; //number of elements

//                    int node1 = (int)Z[i];
//                    int node2 = (int)Z[i + 1];
//                    float dist = (float)Z[i + 2];

//                    if (node1 < N)
//                    {
//                        List<float> point = new List<float>();
//                        for (int n = 0; n < dim; n++)
//                            point.Add((float)X[node1 * dim + n]);
//                        cluster.points.Add(point);
//                        cluster.elements.Add((int)node1);
//                    }
//                    else
//                    {
//                        for (int ii = 0; ii < (int)merge_info[node1 - N].points.Count; ii++)
//                        {
//                            cluster.points.Add(merge_info[node1 - N].points[ii]);
//                            cluster.elements.Add(merge_info[node1 - N].elements[ii]);
//                        }
//                        //update the extended volume of node1 using the dist where this cluster merge with another
//                        merge_info[node1 - N].dist_ext = dist;
//                    }
//                    if (node2 < N)
//                    {
//                        List<float> point = new List<float>();
//                        for (int n = 0; n < dim; n++)
//                            point.Add((float)X[node2 * dim + n]);
//                        cluster.points.Add(point);
//                        cluster.elements.Add((int)node2);
//                    }
//                    else
//                    {
//                        for (int ii = 0; ii < (int)merge_info[node2 - N].points.Count; ii++)
//                        {
//                            cluster.points.Add(merge_info[node2 - N].points[ii]);
//                            cluster.elements.Add(merge_info[node2 - N].elements[ii]);
//                        }

//                        //update the extended volume of node2 using the dist where this cluster merge with another
//                        merge_info[node2 - N].dist_ext = dist;
//                    }

//                    Minibox mb = new Minibox();
//                    for (int ii = 0; ii < (int)cluster.points.Count; ii++)
//                    {
//                        mb.checkIn(cluster.points[ii]); // thelei prosoxh h epistrofh tou check in
//                    }

//                    cluster.dist = dist;
//                    cluster.volume = mb.volume();
//                    if (cluster.volume >= 1)
//                        cluster.volume = 0.999999;
//                    if (cluster.volume == 0)
//                        cluster.volume = 0.001;

//                    cluster.volume_ext = 1;

//                    if (node1 >= N)
//                    {
//                        merge_info[node1 - N].volume_ext = cluster.volume;
//                    }
//                    if (node2 >= N)
//                    {
//                        merge_info[node2 - N].volume_ext = cluster.volume;
//                    }

//                    cluster.node1 = node1;
//                    cluster.node2 = node2;

//                    merge_info.Add(cluster);

//                }

//                for (int i = 0; i < (int)merge_info.Count; i++)
//                {

//                    merge_info[i].nfa = nfa((float)merge_info[i].volume,
//                                                merge_info[i].num_elem, N);

//                    merge_info[i].probability = probability(ref merge_info[i].elements);
//                    int node1 = merge_info[i].node1;
//                    int node2 = merge_info[i].node2;

//                    if ((node1 < N) && (node2 < N))
//                    {
//                        // both nodes are individual samples (nfa=1) : each cluster is max.
//                        merge_info[i].max_meaningful = true;
//                        merge_info[i].max_in_branch.Add(i);
//                        merge_info[i].min_nfa_in_branch = merge_info[i].nfa;
//                    }
//                    else
//                    {
//                        if ((node1 >= N) && (node2 >= N))
//                        {
//                            // both nodes are "sets" : we must evaluate the merging condition
//                            if ((((use_full_merge_rule) &&
//                                     ((merge_info[i].nfa < merge_info[node1 - N].nfa + merge_info[node2 - N].nfa) &&
//                                     (merge_info[i].nfa < Math.Min(merge_info[node1 - N].min_nfa_in_branch,
//                                                                merge_info[node2 - N].min_nfa_in_branch)))) ||
//                                   ((!use_full_merge_rule) &&
//                                     ((merge_info[i].nfa < Math.Min(merge_info[node1 - N].min_nfa_in_branch,
//                                                                 merge_info[node2 - N].min_nfa_in_branch)))))
//                                 && (merge_info[i].probability > minProbability))
//                            {
//                                merge_info[i].max_meaningful = true;
//                                merge_info[i].max_in_branch.Add(i);
//                                merge_info[i].min_nfa_in_branch = merge_info[i].nfa;
//                                for (int k = 0; k < (int)merge_info[node1 - N].max_in_branch.Count; k++)
//                                    merge_info[merge_info[node1 - N].max_in_branch[k]].max_meaningful = false;
//                                for (int k = 0; k < (int)merge_info[node2 - N].max_in_branch.Count; k++)
//                                    merge_info[merge_info[node2 - N].max_in_branch[k]].max_meaningful = false;
//                            }
//                            else
//                            {
//                                merge_info[i].max_meaningful = false;
//                                merge_info[i].max_in_branch.AddRange(merge_info[node1 - N].max_in_branch);
//                                merge_info[i].max_in_branch.AddRange(merge_info[node2 - N].max_in_branch);

//                                if (merge_info[i].nfa < Math.Min(merge_info[node1 - N].min_nfa_in_branch,
//                                                                merge_info[node2 - N].min_nfa_in_branch))

//                                    merge_info[i].min_nfa_in_branch = merge_info[i].nfa;
//                                else
//                                    merge_info[i].min_nfa_in_branch = Math.Min(merge_info[node1 - N].min_nfa_in_branch,
//                                                                              merge_info[node2 - N].min_nfa_in_branch);
//                            }
//                        }
//                        else
//                        {

//                            //one of the nodes is a "set" and the other is an individual sample : check merging condition
//                            if (node1 >= N)
//                            {
//                                if ((merge_info[i].nfa < merge_info[node1 - N].nfa + 1) &&
//                                    (merge_info[i].nfa < merge_info[node1 - N].min_nfa_in_branch) &&
//                                    (merge_info[i].probability > minProbability))
//                                {
//                                    merge_info[i].max_meaningful = true;
//                                    merge_info[i].max_in_branch.Add(i);
//                                    merge_info[i].min_nfa_in_branch = merge_info[i].nfa;
//                                    for (int k = 0; k < (int)merge_info[node1 - N].max_in_branch.Count; k++)
//                                        merge_info[merge_info[node1 - N].max_in_branch[k]].max_meaningful = false;
//                                }
//                                else
//                                {
//                                    merge_info[i].max_meaningful = false;
//                                    merge_info[i].max_in_branch.AddRange(merge_info[node1 - N].max_in_branch);
//                                    merge_info[i].min_nfa_in_branch = Math.Min(merge_info[i].nfa,
//                                                                              merge_info[node1 - N].min_nfa_in_branch);
//                                }
//                            }
//                            else
//                            {
//                                if ((merge_info[i].nfa < merge_info[node2 - N].nfa + 1) &&
//                                    (merge_info[i].nfa < merge_info[node2 - N].min_nfa_in_branch) &&
//                                    (merge_info[i].probability > minProbability))
//                                {
//                                    merge_info[i].max_meaningful = true;
//                                    merge_info[i].max_in_branch.Add(i);
//                                    merge_info[i].min_nfa_in_branch = merge_info[i].nfa;
//                                    for (int k = 0; k < (int)merge_info[node2 - N].max_in_branch.Count; k++)
//                                        merge_info[merge_info[node2 - N].max_in_branch[k]].max_meaningful = false;
//                                }
//                                else
//                                {
//                                    merge_info[i].max_meaningful = false;
//                                    merge_info[i].max_in_branch.AddRange(merge_info[node2 - N].max_in_branch);
//                                    merge_info[i].min_nfa_in_branch = Math.Min(merge_info[i].nfa,
//                                    merge_info[node2 - N].min_nfa_in_branch);
//                                }
//                            }
//                        }
//                    }
//                }
//            }

//            // calculate the number of false alarms
//            public int nfa(float sigma, int k, int N)
//            {
//                return (int)(-1 * NFA(N, k, (double)sigma, 0));
//            }

//            // calculate the probability of a group being a text group
//            public double probability(ref List<int> cluster)
//            {
//                if (cluster.Count > MAX_GROUP_ELEMENTS)
//                {
//                    return 0f;
//                }
//                List<float> sample = new List<float>();
//                sample.Add(cluster.Count);

//                Matrix<float> diameters = new Matrix<float>(cluster.Count, 1, 1);
//                Matrix<float> strokes = new Matrix<float>(cluster.Count, 1, 1);
//                Matrix<float> gradients = new Matrix<float>(cluster.Count, 1, 1);
//                Matrix<float> fg_intensities = new Matrix<float>(cluster.Count, 1, 1);
//                Matrix<float> bg_intensities = new Matrix<float>(cluster.Count, 1, 1);
//                Matrix<float> axial_ratios = new Matrix<float>(cluster.Count, 1, 1);
//                Matrix<float> chull_ratios = new Matrix<float>(cluster.Count, 1, 1);
//                Matrix<float> convexities = new Matrix<float>(cluster.Count, 1, 1);
//                MySubdiv subdiv = new MySubdiv(new Rectangle(0, 0, imSize.Width, imSize.Height));
//                List<List<Point>> forest = new List<List<Point>>();
//                float maxAvgOverlap = 0;
                
//                for (int i = cluster.Count - 1; i >= 0; i--) // gia ola ta ERs sto cluster
//                {
//                    diameters[i, 0] = (float)Math.Max(regions[i].rect.Width, regions[i].rect.Height);
//                    strokes[i, 0] = (float)regions[i].strokeMean;
//                    gradients[i, 0] = (float)regions[i].gradientMean;
//                    fg_intensities[i, 0] = (float)regions[i].intensityMean;
//                    bg_intensities[i, 0] = (float)regions[i].boundaryIntensityMean;
//                    axial_ratios[i, 0] = (float)regions[i].axialRatio;
//                    chull_ratios[i, 0] = (float)regions[i].convexHullRatio;
//                    convexities[i, 0] = (float)regions[i].convexities;

//                    PointF p = new PointF(regions[cluster[i]].rect.X + regions[cluster[i]].rect.Width / 2, regions[cluster[i]].rect.Y + regions[cluster[i]].rect.Height / 2);

//                    subdiv.insert(p);

//                    forest[i].Add(new Point((int)p.X, (int)p.Y));

//                    float avgOverlap = 0;

//                    for (int j = 0; j < cluster.Count; j++)
//                    {
//                        if (j != i)
//                        {
//                            Rectangle intersection = Rectangle.Intersect(regions[cluster[i]].rect, regions[cluster[j]].rect);
//                            int areaInter = intersection.Width * intersection.Height;
//                            int areaI = regions[cluster[i]].rect.Width * regions[cluster[i]].rect.Height;
//                            int areaJ = regions[cluster[j]].rect.Width * regions[cluster[j]].rect.Height;

//                            if (areaInter > 0)
//                            {
//                                float overlap = (float)(areaInter / Math.Min(areaI, areaJ));
//                                avgOverlap += overlap;
//                            }
//                        }
//                    }
//                    avgOverlap /= (cluster.Count - 1);
//                    if (avgOverlap > maxAvgOverlap)
//                        maxAvgOverlap = avgOverlap;
//                }

//                MCvScalar mean = new MCvScalar(), std = new MCvScalar();
//                CvInvoke.MeanStdDev(diameters, ref mean, ref std); // pairnei to mean kai to std olwn twn major axis
//                sample.Add((float)(std.V0 / mean.V0)); // major axis coefficient of variation
//                float diameter_mean = (float)mean.V0;
//                CvInvoke.MeanStdDev(strokes, ref mean, ref std); // mean kai std toy mean stroke toy kathe ER toy cluster
//                sample.Add((float)(std.V0 / mean.V0)); // stroke widths coefficient of variation
//                CvInvoke.MeanStdDev(gradients, ref mean, ref std); // mean kai std toy mean gradient toy kathe ER toy cluster
//                sample.Add((float)std.V0); // standard deviation of gradients
//                CvInvoke.MeanStdDev(fg_intensities, ref mean, ref std); // mean kai std toy mean intensity toy kathe ER toy cluster
//                sample.Add((float)std.V0); // standard deviation of foreground pixel intensities
//                CvInvoke.MeanStdDev(bg_intensities, ref mean, ref std); // mean kai std toy mean intensity toy background toy kathe ER toy cluster
//                sample.Add((float)std.V0);

//                // begin kruskal algorithm to find the MST

//                // TODO: implement dummy subdiv for edge creation // done Untested just 10h work :P
//                List<MCvScalar> edgeList = new List<MCvScalar>();
//                subdiv.getEdgeList(ref edgeList);

//                edgeList.Sort(edge_comp);

//                List<MCvScalar> mst_edges = new List<MCvScalar>();
//                List<float> edge_distances = new List<float>();

//                for (int k = (int)edgeList.Count - 1; k >= 0; k--)
//                {
//                    MCvScalar e = edgeList[k];
//                    Point pt0 = new Point((int)Math.Round(e.V0), (int)Math.Round(e.V1));
//                    Point pt1 = new Point((int)Math.Round(e.V2), (int)Math.Round(e.V3));
//                    int tree_pt0 = find_vertex(ref forest, ref pt0);
//                    int tree_pt1 = find_vertex(ref forest, ref pt1);
//                    if (((pt0.X > 0) && (pt0.X < imSize.Width) && (pt0.Y > 0) && (pt0.Y < imSize.Height) &&
//                         (pt1.X > 0) && (pt1.X < imSize.Width) && (pt1.Y > 0) && (pt1.Y < imSize.Height)) &&
//                            (tree_pt0 != tree_pt1))
//                    {
//                        mst_edges.Add(e);
//                        forest[tree_pt0].InsertRange(0, forest[tree_pt1]);
//                        forest.RemoveAt(tree_pt1);
//                        edge_distances.Add((float)Norm(pt0, pt1));
//                    }
//                    if (mst_edges.Count == cluster.Count - 1)
//                        break;
//                }

//                /* End Kruskal algorithm */

//                List<float> angles = new List<float>();
//                for (int k=0; k<mst_edges.Count; k++)
//                {
//                    MCvScalar q = mst_edges[k];
//                    Point q_pt0 = new Point((int)Math.Round(q.V0), (int)Math.Round(q.V1));
//                    Point q_pt1 = new Point((int)Math.Round(q.V2), (int)Math.Round(q.V3));
//                    for (int j=k+1; j<mst_edges.Count; j++)
//                    {
//                        MCvScalar t = mst_edges[j];
//                        Point t_pt0 = new Point((int)Math.Round(t.V0), (int)Math.Round(t.V1));
//                        Point t_pt1 = new Point((int)Math.Round(t.V2), (int)Math.Round(t.V3));
//                        if(q_pt0 == t_pt0)
//                            angles.Add((float)getAngleABC(q_pt1, q_pt0 , t_pt1));
//                        if(q_pt0 == t_pt1)
//                            angles.Add((float)getAngleABC(q_pt1, q_pt0 , t_pt0));
//                        if(q_pt1 == t_pt0)
//                            angles.Add((float)getAngleABC(q_pt0, q_pt1 , t_pt1));
//                        if(q_pt1 == t_pt1)
//                            angles.Add((float)getAngleABC(q_pt0, q_pt1 , t_pt0));
//                    }
//                }
//                //cout << "we have " << angles.size() << " angles " << endl;
//                //for (int kk=0; kk<angles.size(); kk++)
//                //  cout << angles[kk] << " ";
//                //cout << endl;
                
//                Matrix<float> anglesMat = new Matrix<float>(angles.ToArray());
//                Matrix<float> edgeDistMat = new Matrix<float>(edge_distances.ToArray());

//                CvInvoke.MeanStdDev( anglesMat, ref mean, ref std );
//                sample.Add((float)std.V0);
//                sample.Add((float)mean.V0);
//                CvInvoke.MeanStdDev( edgeDistMat, ref mean, ref std );
//                sample.Add((float)(std.V0/mean.V0));
//                sample.Add((float)(mean.V0/diameter_mean));

//                CvInvoke.MeanStdDev( axial_ratios, ref mean, ref std );
//                sample.Add((float)mean.V0);
//                sample.Add((float)std.V0);

//                /// Calculate average shape self-similarity
//                double avg_shape_match = 0;
//                double eps = 1e-5;
//                int num_matches = 0, sma, smb;
//                for (int i=0; i<cluster.Count; i++)
//                {
//                    for (int j=i+1; j<cluster.Count; j++)
//                    {
//                        for (int h=0; h<7; h++)
//                        {
//                            double ama = Math.Abs(regions[cluster[i]].huMoments[h]);
//                            double amb = Math.Abs(regions[cluster[j]].huMoments[h]);

//                            if( regions[cluster[i]].huMoments[h] > 0 )
//                                sma = 1;
//                            else if( regions[cluster[i]].huMoments[h] < 0 )
//                                sma = -1;
//                            else
//                                sma = 0;
//                            if( regions[cluster[j]].huMoments[h] > 0 )
//                                smb = 1;
//                            else if( regions[cluster[j]].huMoments[h] < 0 )
//                                smb = -1;
//                            else
//                                smb = 0;

//                            if( ama > eps && amb > eps )
//                            {
//                                ama = 1.0 / (sma * Math.Log10( ama ));
//                                amb = 1.0 / (smb * Math.Log10( amb ));
//                                avg_shape_match += Math.Abs( -ama + amb );
//                            }
//                        }
//                        num_matches++;
//                    }
//                }

//                sample.Add((float)(avg_shape_match/num_matches));

//                sample.Add(maxAvgOverlap);

//                CvInvoke.MeanStdDev( chull_ratios, ref mean, ref std );
//                sample.Add((float)mean.V0);
//                sample.Add((float)std.V0);

//                CvInvoke.MeanStdDev( convexities, ref mean, ref std );
//                sample.Add((float)mean.V0);
//                sample.Add((float)std.V0);

//                Matrix<float> sampl = new Matrix<float>(sample.ToArray());

//                float votes_group = boost.Predict( sampl, null, 256 | 1);

//                return (double)1-(double)1/(1+Math.Exp(-2*votes_group));
//            }

//            #region utility functions for MST
//            private static int edge_comp(MCvScalar i, MCvScalar j)
//            {
//                Point a = new Point((int)Math.Round(i.V0), (int)Math.Round(i.V1));
//                Point b = new Point((int)Math.Round(i.V2), (int)Math.Round(i.V3));

//                double eDist_i = Norm(a, b);
//                a = new Point((int)Math.Round(j.V0), (int)Math.Round(j.V1));
//                b = new Point((int)Math.Round(j.V2), (int)Math.Round(j.V3));
//                double eDist_j = Norm(a, b);
//                return eDist_i.CompareTo(eDist_j);
//            }
//            /// <summary>
//            /// Calculates Norm(a-b) which is equal to Norm(b-a) just sayings
//            /// </summary>
//            /// <param name="a"></param>
//            /// <param name="b"></param>
//            /// <returns></returns>
//            private static double Norm(Point a, Point b)
//            {
//                Point ab = new Point(a.X - b.X, a.Y - b.Y);
//                return Norm(ab);
//            }
//            private static double Norm(Point ab)
//            {
//                return Math.Sqrt(Math.Pow(ab.X, 2) + Math.Pow(ab.Y, 2));
//            }

//            private static int find_vertex(ref List<List<Point>> forest, ref Point p)
//            {
//                for (int i = 0; i < forest.Count; i++)
//                {
//                    for (int j = 0; j < forest[i].Count; j++)
//                    {
//                        if (forest[i][j] == p) return i;
//                    }
//                }
//                return -1;
//            }

//            private static int getAngleABC(Point a, Point b, Point c)
//            {
//                Point ab = new Point(b.X - a.X, b.Y - a.Y);
//                Point cb = new Point(b.X - c.X, b.Y - c.Y);

//                // dot product
//                float dot = (float)(ab.X * cb.X + ab.Y * cb.Y);

//                // length square of both vectors
//                float abSqr = (float)(ab.X * ab.X + ab.Y * ab.Y);
//                float cbSqr = (float)(cb.X * cb.X + cb.Y * cb.Y);

//                // square of cosine of needed angle
//                float cosSqr = dot * dot / abSqr / cbSqr;

//                // this is a known trigonometric equality
//                // cos(alpha * 2) = [ cos(alpha)^2 * 2 - 1]
//                float cos2 = 2 * cosSqr - 1;

//                // Here's the only invocation of the heavy function.
//                // It's a good idea to check explicitly if cos2 is within [-1 .. 1] range

//                const float pi = 3.141592f;

//                float alpha2 =
//                    (cos2 <= -1) ? pi :
//                    (cos2 >= 1) ? 0 :
//                    (float)Math.Acos(cos2);

//                float rslt = alpha2 / 2;

//                float rs = (float)(rslt * 180.0 / pi);


//                // Now revolve the ambiguities.
//                // 1. If dot product of two vectors is negative - the angle is definitely
//                // above 90 degrees. Still we have no information regarding the sign of the angle.

//                // NOTE: This ambiguity is the consequence of our method: calculating the cosine
//                // of the double angle. This allows us to get rid of calling sqrt.

//                if (dot < 0)
//                    rs = 180 - rs;

//                // 2. Determine the sign. For this we'll use the Determinant of two vectors.
//                float det = (float)(ab.X * cb.Y - ab.Y * cb.X);
//                if (det < 0)
//                    rs = -rs;

//                return Math.Abs((int)Math.Floor(rs + 0.5));
//            }
//            #endregion
//        }

//        public static void erGroupingGK(InputArray _image, VectorOfMat src, List<ERStat>[] regions, out List<List<Tuple<int, int>>> groups, out List<Rectangle> boxes, String classifier, float minProbability)
//            {
//                if (_image.GetMat().Depth != Emgu.CV.CvEnum.DepthType.Cv8U || _image.GetChannels() != 3) throw new Exception("gtp input ston karatza");

//                Mat image = _image.GetMat();
//                Mat gray = new Mat();
//                CvInvoke.CvtColor(image, gray, Emgu.CV.CvEnum.ColorConversion.Bgr2Gray);
//                // skip assertions twra
//                groups = new List<List<Tuple<int, int>>>();
//                boxes = new List<Rectangle>();

//                for (int c = 0; c < src.Size; c++)
//                {
//                    Mat channel = src[c];
//                    // assert channel is cv8u
//                    if (regions[c].Count < 3) continue;

//                    List<List<int>> meaningfulClusters = new List<List<int>>();

//                    List<ERFeatures> features = new List<ERFeatures>();
//                    float maxStroke = extractFeatures(ref image, ref channel, regions[c], ref features);

//                    // find the max meaningful clusters in the learned feature space

//                    int N = regions[c].Count;
//                    int dim = 7;

//                    double[] data = new double[N * dim];
//                    float weight_param1 = 1.00f;
//                    float weight_param2 = 0.65f;
//                    float weight_param3 = 0.65f;
//                    float weight_param4 = 0.49f;
//                    float weight_param5 = 0.67f;
//                    float weight_param6 = 0.91f;
//                    // features normalized to their largest values
//                    // auta ta features einai gia thn euresh twn text group kai oxi twn xarakthrwn sto eswteriko
//                    int count = 0;
//                    for (int i = 0; i < regions[c].Count; i++)
//                    {
//                        data[count] = (features[i].center.X / channel.Cols) * weight_param1;
//                        data[count + 1] = (features[i].center.Y / channel.Rows) * weight_param1;
//                        data[count + 2] = (features[i].intensityMean / 255) * weight_param2;
//                        data[count + 3] = (features[i].boundaryIntensityMean / 255) * weight_param3;
//                        data[count + 4] = (Math.Max(features[i].rect.Height, features[i].rect.Width) / Math.Max(channel.Rows, channel.Cols)) * weight_param5;
//                        data[count + 5] = (features[i].strokeMean / maxStroke) * weight_param6;
//                        data[count + 6] = (features[i].gradientMean / 255) * weight_param4;

//                        count += dim;
//                    }

//                    MaxMeaningfulClustering mmClustering = new MaxMeaningfulClustering((int)method_codes.METHOD_METR_SINGLE, (int)metrics.METRIC_SEUCLIDEAN, features, new Size(channel.Cols, channel.Rows), classifier, minProbability);

//                    mmClustering.operParenthesis(data, N, dim, (int)method_codes.METHOD_METR_SINGLE, (int)metrics.METRIC_SEUCLIDEAN, ref meaningfulClusters);

//                    for (int k = 0; k < meaningfulClusters.Count; k++)
//                    {
//                        if (meaningfulClusters[k].Count > 2)
//                        {
//                            Rectangle groupRect = features[meaningfulClusters[k][0]].rect;
//                            List<Tuple<int, int>> group = new List<Tuple<int, int>>();
//                            group.Add(Tuple.Create<int, int>(c, meaningfulClusters[k][0]));

//                            for (int l = 1; l < meaningfulClusters[k].Count; l++)
//                            {
//                                groupRect = Rectangle.Union(groupRect, features[meaningfulClusters[k][l]].rect);
//                                group.Add(Tuple.Create<int, int>(c, meaningfulClusters[k][l]));
//                            }
//                            boxes.Add(groupRect);
//                            groups.Add(group);
//                        }
//                    }

//                    meaningfulClusters.Clear();
//                    features.Clear();

//                }

//            }
//    }
//}
