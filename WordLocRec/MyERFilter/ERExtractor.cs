﻿using Emgu.CV;
using Emgu.CV.ML;
using Emgu.CV.Structure;
using Emgu.CV.Util;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text;
using WordLocRec.DataStructs;
using WordLocRec.Grouping;
using WordLocRec.SVMStuff;
using WordLocRec.Tools;

namespace WordLocRec
{
    public class ERExtractor
    {
        [ThreadStatic]
        private static List<ERStat> regions; // static variable of the extractor that contains the final regions
        SizeRestriction sr;

        public delegate bool SizeRestriction(ERStat stat, ExtractorParameters par);
        public class ExtractorParameters
        {
            public int ImageWidth { get; set; }
            public int ImageHeight { get; set; }
            public double MinArea { get; set; }
            public double MaxArea { get; set; }
            public int ThresholdDelta { get; set; }
            public double MinProbability { get; set; }
            public double MinProbabilityDifference { get; set; }
            public ExtractorParameters(int thresholdDelta, double minArea, double maxArea, double minProbability, double minProbabilityDifference, int imageWidth, int imageHeight)
            {
                //TODO: add limitations for the parameters
                this.ThresholdDelta = thresholdDelta;
                this.MinArea = minArea;
                this.MaxArea = maxArea;
                this.MinProbability = minProbability;
                this.MinProbabilityDifference = minProbabilityDifference;
                this.ImageWidth = imageWidth;
                this.ImageHeight = imageHeight;
            }
            public ExtractorParameters(ERFilterProps props, int imageWidth, int imageHeight)
            {
                if (props != null)
                {
                    this.ThresholdDelta = props.thresholdDelta;
                    this.MinArea = props.minArea;
                    this.MaxArea = props.maxArea;
                    this.MinProbability = props.minProbability;
                    this.MinProbabilityDifference = props.minProbabilityDifference;
                }
                else
                {
                    this.ThresholdDelta = 1;
                    this.MinArea = 0.00005;
                    this.MaxArea = 0.13;
                    this.MinProbability = 0.5;
                    this.MinProbabilityDifference = 0;
                }
                this.ImageWidth = imageWidth;
                this.ImageHeight = imageHeight;
            }
        }

        public ExtractorParameters ep;
        public ERExtractor(ExtractorParameters ep, SizeRestriction sr)
        {
            this.ep = ep;
            this.sr = sr;
        }

        private void erMerge(ERStat parent, ERStat child, bool savePixels, bool oppositeTree /*, int width, int height, byte[] currentBinaryMask*/)
        {
            #region Cer history calculation for CECERS
            //mergeWatch.Start();
            if (savePixels) ERUtils.CombineParentChildPixels(ref parent, ref child);
            //mergeWatch.Stop();

            #endregion
            //child.binaryMask = Utils.ArrayCopyBigToSmall(currentBinaryMask, width, child.rect);
            //child.binaryMask = new byte[child.rect.Height * child.rect.Width];

            //for (int i = 0; i < child.rect.Height; i++)
            //{
            //    Array.Copy(currentBinaryMask, (i + child.rect.Y) * width + child.rect.X, child.binaryMask, i * child.rect.Width, child.rect.Width);
            //}

            ERUtils.AddAreaPerimeterEuler(ref parent, ref child);

            ERUtils.MergeCrossings(ref parent, ref child);

            ERUtils.ReshapeBoundingBox(ref parent, child.rect.X, child.rect.Y, child.rect.Right - 1, child.rect.Bottom - 1);

            ERUtils.ConsumeMoments(ref parent, (int)child.rawMoments[0], (int)child.rawMoments[1], (int)child.centralMoments[0], (int)child.centralMoments[1], (int)child.centralMoments[2]);

            ERUtils.CalcMedCrossings(ref child);

            child.level = child.level * ep.ThresholdDelta;
            if (oppositeTree)
                child.level = 255 - child.level;

            if (sr(child, ep))
            {
                ERUtils.AcceptChild(ref parent, ref child);
            }
            else
            {
                ERUtils.RejectChild(ref parent, ref child);
            }
        }

        #region Save the Cers

        private static ERStat erSaveCersOnly(ERStat er, ERStat parent, ERStat prev)
        {
            regions.Add(er);
            regions[regions.Count - 1].parent = parent;

            if (prev != null) prev.next = regions[regions.Count - 1];
            else if (parent != null) parent.child = regions[regions.Count - 1];

            ERStat oldPrev = null;
            ERStat thisER = regions[regions.Count - 1];

            // cer decision
            if (thisER.parent == null)
            {
                thisER.areaVariation = 1f;
                thisER.isCer = false;
            }
            else
            {
                ERStat tempParent = ERUtils.FindAGoodParent(ref thisER); // finds a parent with > level + 3

                if (tempParent.parent != null) // allagh wste na mhn sugrinei ta ERs poy einai monadika me to root ER kai xanei plhroforia
                {
                    thisER.areaVariation = (float)(tempParent.area - thisER.area) / thisER.area;
                    thisER.isCer = thisER.areaVariation < 0.5 ? true : false;
                }
                else
                {
                    thisER.areaVariation = 1f;
                    thisER.isCer = true;
                }
            }
            //end cer decision

            for (ERStat child = er.child; child != null; child = child.next)
                oldPrev = erSaveCersOnly(child, thisER, oldPrev);

            return thisER;
        } // swzei mono tis plhrofories gia ta CERs

        #endregion

        public List<ERStat> extractERs(IInputArray source, bool keepErPixels, bool oppositeTree)
        {
            Mat src = source.GetInputArray().GetMat();

            if (src.NumberOfChannels > 1) throw new Exception("More than one channels of src image at erExtract");
            if (src.Depth != Emgu.CV.CvEnum.DepthType.Cv8U) throw new Exception("Expected byte image; found " + src.Depth + " instead");

            // apply the step in the image, beforehand
            ERUtils.ConvertToDelta(ref src, ep.ThresholdDelta);

            using (src)
            {
                // initialize algorithm variables
                int thresholdLevel = (255 / ep.ThresholdDelta) + 1;
                int width = src.Cols, height = src.Rows;
                byte[,] quads = ERUtils.InitializeQuads();
                bool[] accessiblePixelMask = new bool[width * height];
                bool[] accumulatedPixelMask = new bool[width * height];
                List<int>[] boundaryPixels = new List<int>[256]; // TODO: combine boundaryPixels and boundaryEdges in Tuple
                List<int>[] boundaryEdges = new List<int>[256];
                for (int i = 0; i < boundaryPixels.Length; i++)
                {
                    boundaryPixels[i] = new List<int>();
                    boundaryEdges[i] = new List<int>();
                }

                // initialize stats and regions
                regions = new List<ERStat>();
                List<ERStat> stats = new List<ERStat>();
                stats.Add(new ERStat());

                Byte[] imageData = src.GetData();

                // indexing variables
                int currentPixel = 0; // to index toy pixel poy elegxetai sthn parousa epanalhpsh
                int currentEdge = 0; // to edge poy elegxetai sthn ekastote ypo-epanalhpsh toy "SearchNeighbours" gia to current pixel
                int currentLevel = imageData[0]; // to level toy current pixel
                accessiblePixelMask[0] = true;

                // if this is true, a new component is added to the list, if this is false, the checked pixel is added to the topmost component
                bool pushNewComponent = true;

                //byte[] currentBinaryMask = new byte[width * height];

                for (; ; )
                {
                    // get the indices of the pixel to be evaluated
                    int x = currentPixel % width;
                    int y = currentPixel / width;

                    if (pushNewComponent) // add the new component at the end of the list
                    {
                        stats.Add(new ERStat(currentLevel, currentPixel, x, y));
                        pushNewComponent = false;
                    }

                    pushNewComponent = ERUtils.SearchNeighbours(x, y, ref currentPixel, ref currentEdge, ref currentLevel, ref thresholdLevel, width, height, ref imageData, ref accessiblePixelMask, ref boundaryPixels, ref boundaryEdges);

                    if (pushNewComponent) continue; // restart for the new pixel at the lower level

                    // calculate the incrementally computable descriptors
                    IncrementalyComputableDescriptors icd = new IncrementalyComputableDescriptors();

                    icd = ERUtils.ComputeDescriptors(x, y, width, height, currentPixel, ref imageData, accumulatedPixelMask, quads);

                    ERStat parent = stats[stats.Count - 1];
                    //ERUtils.erAddPixel(ref parent, x, y, icd, currentPixel, ref currentBinaryMask, keepErPixels, imageData[currentPixel]);
                    ERUtils.erAddPixel(ref parent, x, y, icd, currentPixel, keepErPixels);

                    // wont bother again with this pixel
                    accumulatedPixelMask[currentPixel] = true;

                    // the threshold level is fixed from either the previous iteration or inside the "SearchNeighbours" function.
                    // if all possible threshold levels have been processed signal the end
                    if (thresholdLevel == (255 / ep.ThresholdDelta) + 1)
                    {
                        //gia to erSave: ksekinaei apo to teleutaio stoixeio kathws einai to root ER (efoson ta apothikeuei anapoda kata thn anakthsh. giayto otan diavazw to dentro ekswterika apo thn erTreeExtract ksekinaw apo to [0]. Giati tote to dentro exei apothikeutei kanonika sta @regions kai sto [0] einai to root!

                        // the last child is not merged anywhere so its dominant color must me calculated separetaly
                        if (keepErPixels)
                        {
                            ERStat child = stats[stats.Count - 1];
                            ERUtils.IncludeNewChildPixels(ref child);
                            //child.binaryMask = Utils.ArrayCopyBigToSmall(currentBinaryMask, width, child.rect);                      
                        }


                        if (oppositeTree)
                            stats[stats.Count - 1].level = 255 - stats[stats.Count - 1].level;

                        erSaveCersOnly(stats[stats.Count - 1], null, null);

                        return regions;
                    }

                    // threshold level != 256 so there are still pixels left to process
                    currentPixel = boundaryPixels[thresholdLevel][boundaryPixels[thresholdLevel].Count - 1];
                    boundaryPixels[thresholdLevel].RemoveAt(boundaryPixels[thresholdLevel].Count - 1);
                    currentEdge = boundaryEdges[thresholdLevel][boundaryEdges[thresholdLevel].Count - 1];
                    boundaryEdges[thresholdLevel].RemoveAt(boundaryEdges[thresholdLevel].Count - 1);

                    // calculate the threshold level for the next iteration
                    while ((thresholdLevel < (255 / ep.ThresholdDelta) + 1) && boundaryPixels[thresholdLevel].Count == 0)
                    {
                        //c# tip!
                        // όταν το σύστημα κάνει έλεγχο σε μία πολλαπλή συνθήκη με && τότε αν ελέγξει το πρώτο μισό και
                        // έχει ως αποτέλεσμα "false" δεν ελέγχει την υπόλοιπη συνθήκη.
                        // is it safe??
                        thresholdLevel++;
                    }

                    int newLevel = imageData[currentPixel];
                    if (newLevel != currentLevel)
                    {
                        currentLevel = newLevel;

                        while (stats[stats.Count - 1].level < newLevel)
                        {
                            ERStat er = stats[stats.Count - 1];
                            stats.RemoveAt(stats.Count - 1);

                            if (newLevel < stats[stats.Count - 1].level)// auto uparxei gia thn periptwsh emfoleumenwn er poy apotelountai apo 1 mono pixel
                            {
                                stats.Add(new ERStat(newLevel, currentPixel, currentPixel % width, currentPixel / width));
                                erMerge(stats[stats.Count - 1], er, keepErPixels, oppositeTree);
                                break;
                            }
                            erMerge(stats[stats.Count - 1], er, keepErPixels, oppositeTree);
                        }
                    }
                }
            }
        }

        public static void ExecuteDefaultExtractorPipelineNoGrouping(Mat mImage, VectorOfMat channels, Constants.FeatureType f1, Constants.FeatureType f2, out List<ERStat>[] regions, out List<ERStat>[] feedbackRegions, ERFilterProps props = null, String cl1 = "", String cl2 = "", bool strictNMS = true, bool oppositeChannel = false)
        {
            if (cl1 == "") cl1 = Constants.COLOR_DISTANCE_CLASSIFIER;
            if (cl2 == "") cl2 = Constants.SVM_NM;
            ERExtractor.ExtractorParameters ep = new ERExtractor.ExtractorParameters(props, mImage.Cols, mImage.Rows);
            ERExtractor ere = new ERExtractor(ep, ERUtils.SizeRestrictionsArea);
            regions = new List<ERStat>[channels.Size];
            feedbackRegions = new List<ERStat>[channels.Size];

            //StringBuilder sb = new StringBuilder();

            float probability = props == null ? 0.2f : props.minProbability;
            bool keepErPixels = f1.HasFlag(Constants.FeatureType.ColorConsistencyLab) || f2.HasFlag(Constants.FeatureType.ColorConsistencyLab);
            RecursiveFunctions.PruneDesicitonFunction pdf;
            if(strictNMS) pdf = RecursiveFunctions.erSaveNMSOnlyStrict;
            else pdf = RecursiveFunctions.erSaveNMSOnly;
            for (int i = 0; i < channels.Size; i++)
            {
                regions[i] = ere.extractERs(channels[i], keepErPixels, false);
                //sb.AppendLine("Ch: " + 1 + " after extraction: " + regions[i].Count);
                regions[i] = RecursiveFunctions.NonCerSuppression(regions[i]);
                //sb.AppendLine("After cer suppression: " + regions[i].Count);
                regions[i] = RecursiveFunctions.NearDuplicateSuppression(regions[i], true);
                //sb.AppendLine("After nd suppression: " + regions[i].Count);
                if (channels.Size > 1) oppositeChannel = i % 2 == 1 ? true : false;
                regions[i] = ERUtils.TwoClassSvmPruning(channels[i], mImage, regions[i], cl1, oppositeChannel, f1);
                regions[i] = RecursiveFunctions.SuppressWrong(regions[i], RecursiveFunctions.erProbabilityPruning, probability);
                //sb.AppendLine("After color pruning: " + regions[i].Count);
                feedbackRegions[i] = new List<ERStat>(regions[i]);
                regions[i] = ERUtils.TwoClassSvmPruning(channels[i], mImage, regions[i], cl2, oppositeChannel, f2);
                regions[i] = RecursiveFunctions.SuppressWrong(regions[i], RecursiveFunctions.erResponsePruning);
                feedbackRegions[i] = RecursiveFunctions.SuppressCorrect(feedbackRegions[i], RecursiveFunctions.erResponsePruningOpposite);
                //sb.AppendLine("After char pruning: " + regions[i].Count + " feedback: " + feedbackRegions[i].Count);
                regions[i] = RecursiveFunctions.FillRateSuppression(regions[i]);
                feedbackRegions[i] = RecursiveFunctions.FillRateSuppression(feedbackRegions[i]);
                //sb.AppendLine("After fill rate suppression: " + regions[i].Count + " feedback: " + feedbackRegions[i].Count);

                regions[i] = RecursiveFunctions.SuppressNonMaximal(regions[i], pdf);
                feedbackRegions[i] = RecursiveFunctions.SuppressNonMaximal(feedbackRegions[i], pdf);
                //sb.AppendLine("After nms suppression: " + regions[i].Count + " feedback: " + feedbackRegions[i].Count);
            }
            GC.Collect();
            //return sb.ToString();
        }
        public static void ExecuteDefaultExtractorPipelineNMGrouping(Mat mImage, VectorOfMat channels, Constants.FeatureType f1, Constants.FeatureType f2, out List<ERStat>[] regions, out List<List<Tuple<int, int>>> groups, out Rectangle[] wordRects)
        {
            List<ERStat>[] feedbackRegions;
            ExecuteDefaultExtractorPipelineNoGrouping(mImage, channels, f1, f2, out regions, out feedbackRegions);

            // nm grouping
            List<Rectangle> boxes;
            wordRects = ERGroupingNM.DoERGroupingNM(mImage.GetInputArray(), channels, regions, out groups, out boxes, true);
            wordRects = GroupingUtils.SouloupwseRects(wordRects, mImage.Rows, mImage.Cols);
        }

        /// <summary>
        /// ExecuteDefaultExtractorPipelineNoGrouping -> ChineseGroupingPerChannel -> Souloupwse Rects
        /// </summary>
        /// <param name="mImage"></param>
        /// <param name="channels"></param>
        /// <param name="f1"></param>
        /// <param name="f2"></param>
        /// <param name="linesPerChannel"></param>
        /// <param name="props"></param>
        public static void ExecuteDefaultExtractorPipelineLineGrouping(Mat mImage, VectorOfMat channels, Constants.FeatureType f1, Constants.FeatureType f2, out List<ChineseGrouping.Line[]> linesPerChannel, ERFilterProps props = null)
        {
            //StringBuilder sb = new StringBuilder();
            List<ERStat>[] regions, feedbackRegions;
            //sb.AppendLine(ExecuteDefaultExtractorPipelineNoGrouping(mImage, channels, f1, f2, out regions, out feedbackRegions, props));

            ExecuteDefaultExtractorPipelineNoGrouping(mImage, channels, f1, f2, out regions, out feedbackRegions, props);
            linesPerChannel = GroupingUtils.ChineseGroupingPerChannel(mImage, channels, regions, feedbackRegions);
            foreach (ChineseGrouping.Line[] lines in linesPerChannel)
                foreach (ChineseGrouping.Line l in lines)
                    l.wordRect = GroupingUtils.SouloupwseRects(l.wordRect, mImage.Rows, mImage.Cols);

            //return sb.ToString();
        }

        public static void ExecuteDefaultExtractorNMGroupingAndWordTrimming(Mat mImage, VectorOfMat channels, Constants.FeatureType f1, Constants.FeatureType f2, Constants.FeatureType f3, out List<ERStat>[] regions, out List<List<Tuple<int, int>>> groups, out Rectangle[] wordRects, String karatzaPath)
        {
            ExecuteDefaultExtractorPipelineNMGrouping(mImage, channels, f1, f2, out regions, out groups, out wordRects);

            List<Tuple<List<ERStat>, int>> erListPerWordRect = WordStat.CreateErListPerWordRect(regions, groups);
            //WordStat[] wordStats = WordStat.CreateMultiple(wordRects, erListPerWordRect, mImage, channels, "");
            SimpleWordStat[] wordStats = SimpleWordStat.CreateMultiple(wordRects, erListPerWordRect, mImage, channels, "");

            //wordRects = ERUtils.WordStatKaratzasPruning(wordStats, wordRects, mImage.Size, false);
            wordRects = ERUtils.WordStatSvmPruning<SVMSimpleWordStatExample>(wordStats, karatzaPath, wordRects, f3, false, 0.5f);
        }

        public static void ExecuteDefaultExtractorLineGroupingAndWordTrimming<T, R>(Mat mImage, VectorOfMat channels, Constants.FeatureType f1, Constants.FeatureType f2, Constants.FeatureType f3, String wordClassifierPath, out Rectangle[] wordRects)
            where T : SVMExample, new()
            where R : BaseFeature
        {
            List<ChineseGrouping.Line[]> linesPerChannel;
            ExecuteDefaultExtractorPipelineLineGrouping(mImage, channels, f1, f2, out linesPerChannel);

            List<Rectangle> wRects;
            List<ChineseGrouping.Line> allLines;
            List<Tuple<List<ERStat>, int>> erListPerWordRect = LineWordStat.CreateErListPerWordRect(linesPerChannel, out wRects, out allLines);

            BaseFeature[] wordStats;
            //extract the features
            if (typeof(R).Equals(typeof(WordStat)))
                wordStats = WordStat.CreateMultiple(wRects.ToArray(), erListPerWordRect, mImage, channels, "");
            else if (typeof(R).Equals(typeof(SimpleWordStat)))
                wordStats = SimpleWordStat.CreateMultiple(wRects.ToArray(), erListPerWordRect, mImage, channels, "");
            else //if (typeof(R).Equals(typeof(LineWordStat)))
                wordStats = LineWordStat.CreateMultiple(wRects.ToArray(), allLines.ToArray(), erListPerWordRect, mImage, channels, "");

            wordRects = ERUtils.WordStatSvmPruning<T>(wordStats, wordClassifierPath, wRects.ToArray(), f3, false, 0.5f);
        }
    }
}