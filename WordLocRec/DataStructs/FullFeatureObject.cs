﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;
using WordLocRec.Tools;

namespace WordLocRec.DataStructs
{
    public class FullFeatureObject : BaseFeature
    {
        public bool exampleType;
        public NMFeatureObject nmFeature;
        public String imageName; // full path
        public String channel; //e.g. Pii0F, BgrGrayscaleB, HSVHueB etc. model = [colorspace][channelID][Forward-Backward]

        // features to relocate er for more information if needed
        public Rectangle rect; // x,y,width,height, position of the bb in the image
        public int startPixel; // starting pixel
        public int imWidth;
        public int imHeight;
        public int thresholdLevel; // all the connected pixels in the rect below or equal to this threshold are included in the er

        public float ConvexityDefects;
        public float StrokeWidthERWidthRatio;
        //ExtraFeatures
        //public int numOfMainHistIntensitiesBB; // number of different intensities needed to assign at least 85% of all the pixels in the bb
        //public int numOfMainHistIntensitiesER; // number of different intensities needed to assign at least 85% of all the pixels in the er
        public float erbbIntensitiesRatio;
        public float dist2MainIntensitiesBB; // distance of the two main intensities of the histogram
        public float percBBPixelsAt2MainIntensities;
        public float bbStdDev;
        
        public float colorDistERBB, colorDistERBBLab, colorDistERBBBgr, colorConsistencyLab;

        public float significantPaths, skelContourRatio, skelAreaRatio, areaBbRatio;

        public static FullFeatureObject FFOFactoryFromObject(FullFeatureObject palio)
        {
            FullFeatureObject ffo = new FullFeatureObject();
            ffo.areaBbRatio = palio.areaBbRatio;
            ffo.bbStdDev = palio.bbStdDev;
            ffo.channel = palio.channel;
            ffo.colorConsistencyLab = palio.colorConsistencyLab;
            ffo.colorDistERBB = palio.colorDistERBB;
            ffo.colorDistERBBBgr = palio.colorDistERBBBgr;
            ffo.colorDistERBBLab = palio.colorDistERBBLab;
            ffo.ConvexityDefects = palio.ConvexityDefects;
            ffo.dist2MainIntensitiesBB = palio.dist2MainIntensitiesBB;
            ffo.erbbIntensitiesRatio = palio.erbbIntensitiesRatio;
            ffo.exampleType = palio.exampleType;
            ffo.imageName = palio.imageName;
            ffo.imHeight = palio.imHeight;
            ffo.imWidth = palio.imWidth;
            ffo.nmFeature = palio.nmFeature;
            ffo.percBBPixelsAt2MainIntensities = palio.percBBPixelsAt2MainIntensities;
            ffo.probability = palio.probability;
            ffo.rect = palio.rect;
            ffo.significantPaths = palio.significantPaths;
            ffo.skelAreaRatio = palio.skelAreaRatio;
            ffo.skelContourRatio = palio.skelContourRatio;
            ffo.startPixel = palio.startPixel;
            ffo.StrokeWidthERWidthRatio = palio.StrokeWidthERWidthRatio;
            ffo.svmResponse = palio.svmResponse;
            ffo.thresholdLevel = palio.thresholdLevel;
            ffo.wordRect = palio.wordRect;
            ffo.WordRect = palio.WordRect;
            return ffo;
        }

        public FullFeatureObject()
        {
            exampleType = false;

            nmFeature = new NMFeatureObject();
            imageName = "";
            
            rect = new Rectangle();
            startPixel = -1;
            imWidth = -1;
            imHeight = -1;
            thresholdLevel = -1;
            channel = "";

            dist2MainIntensitiesBB = -1f;
            erbbIntensitiesRatio = -1f;
            percBBPixelsAt2MainIntensities = -1f;
            bbStdDev = -1f;

            colorDistERBB = 0f;
            colorDistERBBLab = 0f;
            colorDistERBBBgr = 0f;
            colorConsistencyLab = 0f;

            ConvexityDefects = 0f;
            StrokeWidthERWidthRatio = 0f;
        }

        public FullFeatureObject(bool exampleType, NMFeatureObject nmFeatureObj, String imName, Rectangle r, int startPixel, int imWidth, int imHeight, int thresholdLvl, String channel, float dist2IntBB, float percERPixels, float erbbIntensitiesRatio, float bbStdDev )
        {
            this.exampleType = exampleType;            
            
            this.imageName = imName;
            this.rect = r;
            this.startPixel = startPixel;
            this.imWidth = imWidth;
            this.imHeight = imHeight;
            this.thresholdLevel = thresholdLvl;
            this.channel = channel;

            this.dist2MainIntensitiesBB = dist2IntBB;
            this.percBBPixelsAt2MainIntensities = percERPixels;
            this.erbbIntensitiesRatio = erbbIntensitiesRatio;
            this.bbStdDev = bbStdDev;
        }
        public void NormalizeL2()
        {
            float norm = (float)Math.Sqrt(Math.Pow(dist2MainIntensitiesBB, 2) + Math.Pow(percBBPixelsAt2MainIntensities, 2) + Math.Pow(erbbIntensitiesRatio, 2) + Math.Pow(bbStdDev, 2));

            this.dist2MainIntensitiesBB /= norm;
            this.percBBPixelsAt2MainIntensities /= norm;
            this.erbbIntensitiesRatio /= norm;
            this.bbStdDev /= norm;

            this.nmFeature.NormalizeL2();
        }
        public void Standardize(FullFeatureObject mean, FullFeatureObject std, Constants.FeatureType ft)
        {
            if (ft.HasFlag(Constants.FeatureType.NM))
                this.nmFeature.Standardize(mean.nmFeature, std.nmFeature);
            if (ft.HasFlag(Constants.FeatureType.ColorDistPii))
                colorDistERBB = Utils.Standardize(colorDistERBB, mean.colorDistERBB, std.colorDistERBB);
            if (ft.HasFlag(Constants.FeatureType.ColorDistLab))
                colorDistERBBLab = Utils.Standardize(colorDistERBBLab, mean.colorDistERBBLab, std.colorDistERBBLab);
            if (ft.HasFlag(Constants.FeatureType.ColorDistBgr))
                colorDistERBBBgr = Utils.Standardize(colorDistERBBBgr, mean.colorDistERBBBgr, std.colorDistERBBBgr);
            if (ft.HasFlag(Constants.FeatureType.ColorConsistencyLab))
                colorConsistencyLab = Utils.Standardize(colorConsistencyLab, mean.colorConsistencyLab, std.colorConsistencyLab);
            if (ft.HasFlag(Constants.FeatureType.SignificantPaths))
            {
                significantPaths = Utils.Standardize(significantPaths, mean.significantPaths, std.significantPaths);
                skelContourRatio = Utils.Standardize(skelContourRatio, mean.skelContourRatio, std.skelContourRatio);
                skelAreaRatio = Utils.Standardize(skelAreaRatio, mean.skelAreaRatio, std.skelAreaRatio);
                areaBbRatio = Utils.Standardize(areaBbRatio, mean.areaBbRatio, std.areaBbRatio);
            }
            if (ft.HasFlag(Constants.FeatureType.NumberOfHoles))
                nmFeature.numOfHoles = Utils.Standardize(nmFeature.numOfHoles, mean.nmFeature.numOfHoles, std.nmFeature.numOfHoles);
            if (ft.HasFlag(Constants.FeatureType.ConvexityDefects))
                ConvexityDefects = Utils.Standardize(ConvexityDefects, mean.ConvexityDefects, std.ConvexityDefects);
            if (ft.HasFlag(Constants.FeatureType.StrokeRatio))
                StrokeWidthERWidthRatio = Utils.Standardize(StrokeWidthERWidthRatio, mean.StrokeWidthERWidthRatio, std.StrokeWidthERWidthRatio);
        }

        public NMFeatureObject getNMFeatureObject()
        {
            return nmFeature;
        }
        public HistogramCalculations.HistFeatures getHistFeaturesObject()
        {
            return new HistogramCalculations.HistFeatures(dist2MainIntensitiesBB, percBBPixelsAt2MainIntensities, erbbIntensitiesRatio, bbStdDev);
        }
        public void setHistFeaturesObject(HistogramCalculations.HistFeatures hf)
        {
            this.dist2MainIntensitiesBB = hf.distanceIntensityBB;
            this.percBBPixelsAt2MainIntensities = hf.percOfBBPixelsAt2MainIntensities;
            this.erbbIntensitiesRatio = hf.erbbIntensityRatio;
            this.bbStdDev = hf.stdDevFullRange;
        }

        public override List<float> ToFloatList()
        {
            List<float> data = nmFeature.ToFloatList();

            data.Add(colorDistERBB);
            data.Add(colorDistERBBLab);
            data.Add(colorDistERBBBgr);
            data.Add(colorConsistencyLab);
            data.Add(significantPaths);
            data.Add(skelContourRatio);
            data.Add(skelAreaRatio);
            data.Add(areaBbRatio);
            data.Add(ConvexityDefects);
            data.Add(StrokeWidthERWidthRatio);

            return data;
        }
        public static FullFeatureObject FromFloatList(List<float> floatList)
        {
            FullFeatureObject ffo = new FullFeatureObject();
            ffo.nmFeature = new NMFeatureObject(floatList[0], floatList[1], floatList[2], floatList[3], floatList[4], floatList[5], floatList[6]);
            ffo.colorDistERBB = floatList[7];
            ffo.colorDistERBBLab = floatList[8];
            ffo.colorDistERBBBgr = floatList[9];
            ffo.colorConsistencyLab = floatList[10];
            ffo.significantPaths = floatList[11];
            ffo.skelContourRatio = floatList[12];
            ffo.skelAreaRatio = floatList[13];
            ffo.areaBbRatio = floatList[14];
            ffo.ConvexityDefects = floatList[15];
            ffo.StrokeWidthERWidthRatio = floatList[16];

            return ffo;
        }

        public override string ToFlatString()
        {
            throw new NotImplementedException();
        }
        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            if (nmFeature != null)
                sb.AppendLine(nmFeature.ToString());
                sb.AppendLine("Convexity Defects: " + ConvexityDefects);
                sb.AppendLine("Stroke Width ER Width Ratio: " + StrokeWidthERWidthRatio);
                sb.AppendLine("Pii distance: " + colorDistERBB);
                sb.AppendLine("Lab distance: " + colorDistERBBLab);
                sb.AppendLine("Bgr distance: " + colorDistERBBBgr);
                sb.AppendLine("Lab color consistency: " + colorConsistencyLab);
                sb.AppendLine("Significant Paths: " + significantPaths);
                sb.AppendLine("Skel contour Ratio: " + skelContourRatio);
                sb.AppendLine("Skel area Ratio: " + skelAreaRatio);
                sb.AppendLine("Area bb Ratio: " + areaBbRatio);
            return sb.ToString();
        }

        public static List<FullFeatureObject> LoadFullFeatureObjects(String[] filenames)
        {
            List<FullFeatureObject> listFFO = new List<FullFeatureObject>();

            for (int i = 0; i < filenames.Length; i++)
            {
                FullFeatureObject fullFeatureObject = IOUtils.FromXml<FullFeatureObject>(filenames[i]);
                listFFO.Add(fullFeatureObject);
            }
            return listFFO;
        }
    }
}
