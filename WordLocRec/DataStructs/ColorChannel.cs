﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WordLocRec.DataStructs
{
    public class ColorChannel
    {
        public enum AvailableColorSpaces
        {
            HSV,
            BGR,
            LAB,
            XYZ,
            Grayscale,
            Pii,
            Distances,
            SingleGrad,
            MultiGrad,
            MultiSobelGrad
        }
        public enum Channel
        {
            Channel1 = 1,
            Channel2 = 2,
            Channel3 = 4,

            AllChannels = Channel1 | Channel2 | Channel3
        }

        public AvailableColorSpaces Space { get; set; }
        public Channel Kanali { get; set; }
        public bool ForwardProjection { get; set; }
        public bool InverseProjection { get; set; }
        public double Scale { get; set; }

        public bool ValidCombination = false;

        public ColorChannel(AvailableColorSpaces space, Channel channel, bool forward, bool inverse, double scale)
        {
            this.Space = space;
            this.Kanali = channel;
            this.ForwardProjection = forward;
            this.InverseProjection = inverse;
            this.Scale = scale;
            IsValidCombination();
        }
        /// <summary>
        /// Check if at least one projection is needed
        /// </summary>
        /// <returns></returns>
        public bool IsValidCombination()
        {
            if (!ForwardProjection && !InverseProjection) return false;
            return true;
        }

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(Space.ToString() + ", " + Kanali.ToString() + ", " + (ForwardProjection ? "for, " : "") + (InverseProjection ? "inv, " : "") + "Scale: " + Scale);
            return sb.ToString();
        }
    }
}
