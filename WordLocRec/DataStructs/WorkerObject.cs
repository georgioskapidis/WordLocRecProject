﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WordLocRec.Tools;
using WordLocRec.Workers;

namespace WordLocRec.DataStructs
{
    public class WorkerObject
    {
        public ERFilterProps props;
        public String filename;
        public List<ColorChannel> channelProjections;
        public Constants.ColorSpace colorspace;
        public Constants.VisChannels channels;
        public Constants.Gradients gradients;
        public Constants.GroupingMethod grouping;
        public Constants.FeatureType f1, f2;
        public Constants.WordFeatureType wordFeatureType;
        public bool combineChannels;
        public bool smoothing;
        public bool gammaCorrection;
        public bool strictNMS;
        public float gamma;
        public int smoothX, smoothY;
        public ImageProcessingUtils.FilteringFunction fun;

        public WorkerObject(ERFilterProps props, String filename)
        {
            this.props = props;
            this.filename = filename;
        }

        public WorkerObject(ERFilterProps props, String filename, List<ColorChannel> channelProj, ImageProcessingUtils.FilteringFunction fun, Constants.FeatureType f1, Constants.FeatureType f2, Constants.WordFeatureType wordFeatureType, bool strictNMS, bool combineChannels, bool gammaCorrection = false, bool smoothing = false, Constants.GroupingMethod groupingMethod = Constants.GroupingMethod.ExhaustiveSearch, float gamma = 2.2f, int smoothX = 5, int smoothY = 5)
            : this(props, filename)
        {
            this.fun = fun;
            this.f1 = f1;
            this.f2 = f2;
            this.wordFeatureType = wordFeatureType;
            this.strictNMS = strictNMS;
            this.combineChannels = combineChannels;
            this.gamma = gamma;
            this.smoothX = smoothX;
            this.smoothY = smoothY;
            this.channelProjections = channelProj;
            this.smoothing = smoothing;
            this.gammaCorrection = gammaCorrection;
            this.grouping = groupingMethod;
        }

        public WorkerObject(ERFilterProps props, String filename, Constants.ColorSpace space, Constants.VisChannels channeles, Constants.Gradients grads, bool gammaCorrection = false, bool smoothing = false, Constants.GroupingMethod gMethod = Constants.GroupingMethod.ExhaustiveSearch)
            :this(props, filename)
        {
            this.colorspace = space;
            this.channels = channeles;
            this.gradients = grads;
            this.smoothing = smoothing;
            this.grouping = gMethod;
            this.gammaCorrection = gammaCorrection;
        }

        public WorkerObject(ERFilterProps props, String filename, Constants.ColorSpace colorspace, Constants.VisChannels channels, Constants.Gradients gradients, ImageProcessingUtils.FilteringFunction fun, Constants.FeatureType f1, Constants.FeatureType f2, Constants.WordFeatureType wordFeatureType, bool strictNMS, bool combineChannels, bool gammaCorrection = false, bool smoothing = false, Constants.GroupingMethod groupingMethod = Constants.GroupingMethod.ExhaustiveSearch, float gamma = 2.2f, int smoothX = 5, int smoothY = 5) : this(props, filename, colorspace, channels, gradients, gammaCorrection, smoothing, groupingMethod)
        {
            this.fun = fun;
            this.f1 = f1;
            this.f2 = f2;
            this.wordFeatureType = wordFeatureType;
            this.strictNMS = strictNMS;
            this.combineChannels = combineChannels;
            this.gamma = gamma;
            this.smoothX = smoothX;
            this.smoothY = smoothY;
        }
    }
}
