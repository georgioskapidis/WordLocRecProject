﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WordLocRec.DataStructs
{    
    public class PathStats
    {
        public int NumberOfPaths { get; private set; }
        
        public List<int> PathLengths;
        
        public int Intersections { get; set; }

        /// <summary>
        /// Constructor for a PathStats object
        /// </summary>
        public PathStats()
        {
            NumberOfPaths = 0;
            PathLengths = new List<int>();
            Intersections = 0;
        }

        /// <summary>
        /// Adds a new path in the current PathStats object
        /// </summary>
        public void AddNewPath()
        {
            PathLengths.Add(0);
            NumberOfPaths++;
        }

        public void IncreaseLastPathLength()
        {
            PathLengths[NumberOfPaths - 1]++;
        }

        public void RemoveLastPath()
        {
            NumberOfPaths--;
            PathLengths.RemoveAt(NumberOfPaths);
        }

        public int GetTotalPixelsCount()
        {
            int sum = 0;
            foreach (var length in PathLengths)
            {
                sum += length;
            }
            return sum;
        }

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendLine("PathStats include: ");
            sb.AppendLine(NumberOfPaths + " Paths ");
            for (int i = 0; i < PathLengths.Count; i++)
            {
                sb.AppendLine("Length of Path " + i + " is: " + PathLengths[i]);
            }
            return sb.ToString();
        }
    }
}
