﻿using Emgu.CV;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WordLocRec.DataStructs
{
    class CECER
    {
        public CECER(int level, double dc)
        {
            this.level = level;
            this.dc = dc;
            area = 0;
        }
        //public Mat m;
        public int level;
        public double dc;
        //public Rectangle boundingBox;
        public int area;
    }
}
