﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WordLocRec.DataStructs
{
    public class CCStats
    {
        public double RatioWH
        {
            get;
            set;
        }
        public double RatioPixel
        { get; set; }
        public int CCCount { get; set; }
        public int InflexionPoints { get; set; }
    }
}
