﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WordLocRec.DataStructs
{
    class ConnComp
    {
        //private int _label;
        //private Color _color;
        //private List<Point> _points;
        public Rectangle BoundingRect
        {
            get;
            private set;
        }
        public int Label 
        {
            get;
            private set; 
        }
        protected List<Point> Points
        {
            get;
            private set;
        }
        public Color Color
        {
            get;
            private set;
        }
        
        public ConnComp(int label, Color color)
        {
            this.Label = label;
            this.Color = color;
            this.Points = new List<Point>();
        }

        public void AddPoint(int x, int y)
        {
            Points.Add(new Point(x, y));
        }
        public void AddPointList(List<Point> pList)
        {
            Points.AddRange(pList);
        }
        public void RemoveAllPoints()
        {
            Points = new List<Point>();
        }
        public List<Point> GetPointList()
        {
            return Points;
        }
        public void SetRectangle(Rectangle rect)
        {
            BoundingRect = rect;
        }
    }
}
