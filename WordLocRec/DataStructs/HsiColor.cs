﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WordLocRec
{
    class HsiColor
    {
        private float _h;
        public float H
        {
            get { return this._h; }
        }
        private float _s;
        public float S
        {
            get { return this._s; }
        }
        private float _i;
        public float I
        {
            get { return this._i; }
        }

        private HsiColor(float hue, float saturation, float intensity)
        {
            if (hue <0f || 360f <=hue)
            {
                throw new ArgumentException("problem with hue", "hue");
            }
            if (saturation < 0f || 1f < saturation)
            {
                throw new ArgumentException("problem with saturation", "saturation");
            }
            if (intensity < 0f || 1f < intensity)
            {
                throw new ArgumentException("problema with intensity", "intensity");
            }

            this._h = hue;
            this._s = saturation;
            this._i = intensity;
        }
        public static HsiColor FromRGB(Color rgb)
        {
            float r = (float)rgb.R / 255f;
            float g = (float)rgb.G / 255f;
            float b = (float)rgb.B / 255f;

            float max = Math.Max(r, Math.Max(g, b));
            float min = Math.Min(r, Math.Min(g, b));

            float intensity = (r + g + b) / 3f;

            float hue, saturation;

            if (max == min)
            {
                //undefined
                hue = 0f;
                saturation = 0f;
            }
            else
            {
                float c = max - min;

                if (max == r)
                {
                    hue = (g - b) / c;
                }
                else if (max == g)
                {
                    hue = (b - r) / c + 2f;
                }
                else
                {
                    hue = (r - g) / c + 4f;
                }
                hue *= 60f;
                if (hue < 0f)
                {
                    hue += 360f;
                }

                saturation = 1f- min/intensity;
            }
            return new HsiColor(hue, saturation, intensity);
        }
    }
}
