﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WordLocRec.DataStructs
{
    public class IncrementalyComputableDescriptors
    {
        public int NonBoundaryNeighbours { get; set; } // for the crossings calculations
        public int NonBoundaryNeighboursHor { get; set; }
        public int dC1 { get; set; } // Euler features from the quads
        public int dC2 { get; set; }
        public int dC3 { get; set; }

        public IncrementalyComputableDescriptors()
        {
        }
    }
    
}
