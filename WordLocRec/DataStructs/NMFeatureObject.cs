﻿using Emgu.CV;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WordLocRec.DataStructs
{
    public class NMFeatureObject
    {
        public float aspectRatio; // rect.width / rect.height
        public float compactness; // sqrt(area) / perimeter
        public float numOfHoles; // 1 - euler number
        public float medianOfCrossings;
        public float holeAreaRatio;
        public float convexHullRatio;
        public float numInflextionPoints;

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendLine("Aspect ratio: " + aspectRatio);
            sb.AppendLine("Compactness: " + compactness);
            sb.AppendLine("Number of Holes: " + numOfHoles);
            sb.AppendLine("Median of Crossings: " + medianOfCrossings);
            sb.AppendLine("Hole area ratio: " + holeAreaRatio);
            sb.AppendLine("Convex hull ratio: " + convexHullRatio);
            sb.AppendLine("Inflexion Points: " + numInflextionPoints);
            return sb.ToString();
        }
        public List<float> ToFloatList()
        {
            List<float> data = new List<float>();
            data.Add(aspectRatio);
            data.Add(compactness);
            data.Add(numOfHoles);
            data.Add(medianOfCrossings);
            data.Add(holeAreaRatio);
            data.Add(convexHullRatio);
            data.Add(numInflextionPoints);

            return data;
        }

        public NMFeatureObject()
        {
            aspectRatio = 0f;
            compactness = 0f;
            numOfHoles = 0f;
            medianOfCrossings = 0f;
            holeAreaRatio = 0f;
            convexHullRatio = 0f;
            numInflextionPoints = 0f;
        }
        public NMFeatureObject(float aspectRatio, float compactness, float numOfHoles, float medianOfCrossings, float holeAreaRatio, float convexHullRatio, float numInflextionPoints)
        {
            this.aspectRatio = aspectRatio;
            this.compactness = compactness;
            this.numOfHoles = numOfHoles;
            this.medianOfCrossings = medianOfCrossings;
            this.holeAreaRatio = holeAreaRatio;
            this.convexHullRatio = convexHullRatio;
            this.numInflextionPoints = numInflextionPoints;
        }

        public void NormalizeL2()
        {
            float norm = (float)Math.Sqrt(Math.Pow(aspectRatio, 2) + Math.Pow(compactness, 2) + Math.Pow(numOfHoles, 2) + Math.Pow(medianOfCrossings, 2) + Math.Pow(holeAreaRatio, 2) + Math.Pow(convexHullRatio, 2) + Math.Pow(numInflextionPoints, 2));

            this.aspectRatio /= norm;
            this.compactness /= norm;
            this.numOfHoles /= norm;
            this.medianOfCrossings /= norm;
            this.holeAreaRatio /= norm;
            this.convexHullRatio /= norm;
            this.numInflextionPoints /= norm;
        }

        public void Standardize(NMFeatureObject mean, NMFeatureObject std)
        {
            aspectRatio = Utils.Standardize(aspectRatio, mean.aspectRatio, std.aspectRatio);
            compactness = Utils.Standardize(compactness, mean.compactness, std.compactness);
            numOfHoles = Utils.Standardize(numOfHoles, mean.numOfHoles, std.numOfHoles);
            medianOfCrossings = Utils.Standardize(medianOfCrossings, mean.medianOfCrossings, std.medianOfCrossings);
            holeAreaRatio = Utils.Standardize(holeAreaRatio, mean.holeAreaRatio, std.holeAreaRatio);
            convexHullRatio = Utils.Standardize(convexHullRatio, mean.convexHullRatio, std.convexHullRatio);
            numInflextionPoints = Utils.Standardize(numInflextionPoints, mean.numInflextionPoints, std.numInflextionPoints);
        }
    }
}
