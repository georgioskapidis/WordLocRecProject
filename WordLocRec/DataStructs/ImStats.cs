﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WordLocRec.DataStructs
{
    public class ImStats
    {
        int MedianIntensity { get; set; }
        int MeanIntensity { get; set; }
        int RangeIntensity { get; set; }
        int VarianceIntensity { get; set; }
        int ccCount { get; set; }
        int percPixelsCanny { get; set; }
        // perc of pixels belonging to a cc after canny to the total number of pixels in an image


    }
}
