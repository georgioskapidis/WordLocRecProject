﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WordLocRec.DataStructs
{
    public class ERFilterProps
    {
        public int thresholdDelta = 1;
        public float minArea = 0.00025f;
        public float maxArea = 0.13f;
        public float minProbability = 0.4f;
        public float minProbabilityDifference = 0.1f;
        public bool nonMaxSuppression = true;

        public ERFilterProps() { }
        public ERFilterProps(int thresholdDelta, float minArea, float maxArea, float minProbability, float minProbDiff)
        {
            this.thresholdDelta = thresholdDelta;
            this.minArea = minArea;
            this.maxArea = maxArea;
            this.minProbability = minProbability;
            this.minProbabilityDifference = minProbDiff;
        }
    }
}
