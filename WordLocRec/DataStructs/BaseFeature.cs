﻿using Emgu.CV;
using Emgu.CV.Util;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace WordLocRec.DataStructs
{
    public abstract class BaseFeature
    {
        [XmlIgnore]
        public float probability { get; set; }
        [XmlIgnore]
        public float svmResponse { get; set; }

        public Rectangle wordRect { get; set; }
        public Rectangle WordRect { get; set; }

        public abstract List<float> ToFloatList();

        public abstract String ToFlatString();
    }
}
