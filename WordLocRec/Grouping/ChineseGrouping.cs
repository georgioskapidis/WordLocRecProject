﻿using Emgu.CV;
using Emgu.CV.Structure;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WordLocRec.Tools;

namespace WordLocRec.Grouping
{
    public class ChineseGrouping
    {
        public class LineEstimate
        {
            public float interceptT0, interceptT1, interceptB0, interceptB1, interceptC, slope, maxErrB, maxErrT;
            public LineEstimate()
            {
                interceptT0 = -1;
                interceptT1 = -1;
                interceptB0 = -1;
                interceptB1 = -1;
                interceptC = -1;
                slope = 0;
                maxErrB = float.MaxValue;
                maxErrT = float.MaxValue;
            }
            public LineEstimate(float interceptB0, float interceptB1, float interceptT0, float interceptT1, float slope, float maxErrB, float maxErrT):this()
            {
                this.interceptB0 = interceptB0;
                this.interceptB1 = interceptB1;
                this.interceptT0 = interceptT0;
                this.interceptT1 = interceptT1;
                this.slope = slope;
                this.maxErrB = maxErrB;
                this.maxErrT = maxErrT;
            }            
        }
        public class Line
        {
            public bool Contains(Line b)
            {
                if (this.wordRect.Contains(b.wordRect)) return true;
                return false;
            }
            public bool PartiallyIncludes(Line b)
            {
                Rectangle intersection = Rectangle.Intersect(wordRect, b.wordRect);
                if (intersection.Width * intersection.Height < 0.5 * b.wordRect.Width * b.wordRect.Height)
                    return false;
                else if (intersection.Width * intersection.Height > 0.95 * b.wordRect.Width * b.wordRect.Height) return true;

                int counter = 0;
                foreach (ERStat er in ersOfLine)
                {
                    if (intersection.Contains(er.rect)) counter++;
                }
                return counter >= ersOfLine.Count / 2f;
            }

            private static int TDx = 2;
            private static float TTHETAinRads = 0.261799388f;
            private float TRh, TRv, TDc;

            public float lineFeature = 0;

            public LineEstimate lineEstimates;

            public List<ERStat> ersOfLine;
            private List<float> thetas;
            float lastTheta;
            public Rectangle wordRect;
            private int IndexToAdd = -1;
            
            private float MeanPiiValue, LastPiiValue;
            private int LineFrontPixels;
            //private float meanDx; // only to be used to stop the additions of the misclassified regions on the right of the line

            public Line()
            {
                ersOfLine = new List<ERStat>();
                thetas = new List<float>();
                lastTheta = 0f;
                LineFrontPixels = 0;
                MeanPiiValue = 0;
            }
            public void AddER(ERStat er, Mat channel, Mat src3Channels)
            {
                if (Includes(er)) return;
                if (IndexToAdd != -1)
                {
                    ersOfLine.Insert(IndexToAdd, ERStat.DeepCopy(er));
                    thetas.Insert(IndexToAdd, lastTheta);
                    IndexToAdd = -1;
                }
                else
                {
                    ersOfLine.Add(ERStat.DeepCopy(er));
                    thetas.Add(lastTheta);
                }

                if (ersOfLine.Count > 1)
                {
                    MeanPiiValue = MeanPiiValue * LineFrontPixels + LastPiiValue * er.area;
                    LineFrontPixels += er.area;
                    MeanPiiValue /= LineFrontPixels;
                }
                else
                {
                    using (Mat mask = Utils.ErMaskFloodFill(channel, ersOfLine[0], false))
                    {
                        MeanPiiValue = ERUtils.ColorDistanceMatValue(mask, src3Channels, ersOfLine[0].rect, new MCvScalar(0, 0, 0), ERUtils.EuclidianColorDistance);
                        LineFrontPixels += ersOfLine[0].area;
                    }
                }
                    
                // worst double sort ever :(
                var res = ersOfLine.Zip(thetas, (a, b) => new
                {
                    er = a,
                    theta = b
                }).OrderBy(x => x.er.rect.X);

                //ersOfLine.Sort((x, y) => x.rect.X.CompareTo(y.rect.X));
                ersOfLine = new List<ERStat>();
                thetas = new List<float>();
                foreach (var r in res)
                {
                    ersOfLine.Add(r.er);
                    thetas.Add(r.theta);
                }

                // debug stuff

                wordRect = ersOfLine[0].rect;
                for (int i = 0; i < ersOfLine.Count; i++)
                {
                    wordRect = Rectangle.Union(wordRect, ersOfLine[i].rect);
                }

            }

            public bool CanAdd(ERStat er, Mat src3Channels, Mat channel)
            {
                ERStat rightmost = ersOfLine[ersOfLine.Count - 1];
                ERStat leftOfRighmost = null;
                if (ersOfLine.Count > 1)
                {
                    leftOfRighmost = ersOfLine[ersOfLine.Count - 2];
                    if (CanMerge(er, leftOfRighmost, ersOfLine.Count - 2 == 0, src3Channels, channel))
                    {
                        IndexToAdd = ersOfLine.Count - 1;
                        return true;
                    }
                }
                if (CanMerge(er, rightmost, ersOfLine.Count - 1 == 0, src3Channels, channel)) return true;
                return false;
            }
            
            public bool CanAddAnywhereInLine(ERStat er, Mat src3Channels, Mat channel)
            {
                IndexToAdd = -1;
                for (int i = 1; i < ersOfLine.Count; i++)
                {
                    ERStat leftInLine = ersOfLine[i - 1];
                    ERStat rightInLine = ersOfLine[i];

                    if (CanMergeBetween(er, leftInLine, rightInLine, i - 1, src3Channels, channel))
                    {
                        IndexToAdd = i;
                        return true;
                    }                     
                }
                ERStat rightMost = ersOfLine[ersOfLine.Count - 1];
                if (CanMergeRight(er, rightMost, src3Channels, channel))
                    return true;
                return false;
            }

            private bool CanMerge(ERStat er, ERStat lineER, bool firstInLine, Mat src3Channels, Mat channel)
            {
                int hC = er.rect.Height, hCi = lineER.rect.Height;
                int wC = er.rect.Width, wCi = lineER.rect.Width;
                int yMinC = er.rect.Top, yMinCi = lineER.rect.Top;
                int yMaxC = er.rect.Bottom, yMaxCi = lineER.rect.Bottom;
                int xMinC = er.rect.Left, xMinCi = lineER.rect.Left;
                int xMaxC = er.rect.Right, xMaxCi = lineER.rect.Right;

                // left overlap
                int leftOverlap = xMinC - xMaxCi;
                if ((leftOverlap < 0 && Math.Abs(leftOverlap) > wCi * 0.5)) return false;
                //interval 
                float Dx = (float)(leftOverlap) / hCi;
                // horizontal overlap
                float Dh = (float)(leftOverlap) / wC;
                //height differences
                float Rh = (float)Math.Min(hCi, hC) / Math.Max(hCi, hC);
                //vertical overlap
                float Rv = ((float)Math.Min(yMaxC, yMaxCi) - Math.Max(yMinC, yMinCi)) / Math.Max(hCi, hC);
                //color difference
                float Dc;
                //float Dclab;
                using (Mat mask1 = Utils.ErMaskFloodFill(channel, er, false))
                using (Mat mask2 = Utils.ErMaskFloodFill(channel, lineER, false))
                {
                    Dc = ERUtils.ColorDistance2Mats(mask1, mask2, src3Channels, er.rect, lineER.rect, ERUtils.EuclidianColorDistance);

                    LastPiiValue = ERUtils.ColorDistanceMatValue(mask1, src3Channels, er.rect, new MCvScalar(0, 0, 0), ERUtils.EuclidianColorDistance);
                    //Dclab = ERUtils.ColorDistance2Mats(mask1, mask2, src3Channels, er.rect, lineER.rect, ERUtils.LabColorDistance);
                }
                    
                //orientation change
                float thetaTop = (float)Math.Abs(Math.Atan(((double)(yMinC - yMinCi) / (Dx + wC / 2d + wCi / 2d))));
                float thetaBot = (float)Math.Abs(Math.Atan(((double)(yMaxC - yMaxCi) / (Dx + wC / 2d + wCi / 2d))));
                float theta, dTheta;
                if (firstInLine)
                {
                    theta = Math.Min(thetaTop, thetaBot);
                    dTheta = theta;
                }
                else
                {
                    float dThetaTop = Math.Abs(thetas[thetas.Count - 1] - thetaTop);
                    float dThetaBot = Math.Abs(thetas[thetas.Count - 1] - thetaBot);
                    dTheta = Math.Min(dThetaTop, dThetaBot);
                    theta = dThetaTop < dThetaBot ? thetaTop : thetaBot;
                }
                // dynamic threshold calculations
                TRh = (float)Math.Min(0.2 * Math.Abs(Dx) + 0.4, 0.8);
                TRv = (float)Math.Min(0.25 * Math.Abs(Dx) + 0.3, 0.8);
                TDc = 300 - 100 * Math.Abs(Dx);
                //float TDclab = 12;

                if (Dx < TDx && Rh > TRh && Rv > TRv && Dc < TDc && dTheta < TTHETAinRads && Dh > -0.75 && Math.Abs(LastPiiValue - MeanPiiValue) < 0.2 * MeanPiiValue)
                {
                    lastTheta = theta;
                    return true;
                }

                return false;
            }
            
            private bool CanMergeBetween(ERStat er, ERStat leftInLine, ERStat rightInLine, int indexOfLeft, Mat src3Channels, Mat channel)
            {
                int hC = er.rect.Height, hCl = leftInLine.rect.Height, hCr = rightInLine.rect.Height;
                int wC = er.rect.Width, wCl = leftInLine.rect.Width, wCr = rightInLine.rect.Width;
                int yMinC = er.rect.Top, yMinCl = leftInLine.rect.Top, yMinCr = rightInLine.rect.Top;
                int yMaxC = er.rect.Bottom, yMaxCl = leftInLine.rect.Bottom, yMaxCr = rightInLine.rect.Bottom;
                int xMinC = er.rect.Left, xMinCl = leftInLine.rect.Left, xMinCr = rightInLine.rect.Left;
                int xMaxC = er.rect.Right, xMaxCl = leftInLine.rect.Right, xMaxCr = rightInLine.rect.Right;


                // left overlap
                int leftOverlap = xMinC - xMaxCl;
                if ((leftOverlap < 0 && Math.Abs(leftOverlap) > wCl * 0.1)) return false;
                // right overlap
                int rightOverlap = xMinCr - xMaxC;
                if (rightOverlap < 0 && Math.Abs(rightOverlap) > wCr * 0.1) return false;

                // h can merge alla gtp giati viazomai kai de thelw na xalasw ta last theta
                //interval 
                float Dx = (float)(leftOverlap) / hCl;
                float Dxr = (float)(rightOverlap) / hCr;
                // horizontal overlap
                float Dh = (float)(leftOverlap) / wC;
                //height differences
                float Rh = (float)Math.Min(hCl, hC) / Math.Max(hCl, hC);
                //vertical overlap
                float Rv = ((float)Math.Min(yMaxC, yMaxCl) - Math.Max(yMinC, yMinCl)) / Math.Max(hCl, hC);
                //color difference
                float Dc, leftPiiValue, rightPiiValue;
                //float Dclab;
                using (Mat mask1 = Utils.ErMaskFloodFill(channel, er, false))
                using (Mat mask2 = Utils.ErMaskFloodFill(channel, leftInLine, false))
                using (Mat mask3 = Utils.ErMaskFloodFill(channel, rightInLine, false))
                {
                    
                    Dc = ERUtils.ColorDistance2Mats(mask1, mask2, src3Channels, er.rect, leftInLine.rect, ERUtils.EuclidianColorDistance);
                    //Dclab = ERUtils.ColorDistance2Mats(mask1, mask2, src3Channels, er.rect, lineER.rect, ERUtils.LabColorDistance);
                    LastPiiValue = ERUtils.ColorDistanceMatValue(mask1, src3Channels, er.rect, new MCvScalar(0, 0, 0), ERUtils.EuclidianColorDistance);
                    leftPiiValue = ERUtils.ColorDistanceMatValue(mask2, src3Channels, leftInLine.rect, new MCvScalar(0, 0, 0), ERUtils.EuclidianColorDistance);

                    rightPiiValue = ERUtils.ColorDistanceMatValue(mask3, src3Channels, rightInLine.rect, new MCvScalar(0, 0, 0), ERUtils.EuclidianColorDistance);

                    Tuple<float,float> meanStd = Utils.ComputeMeanStd(new float[] { leftPiiValue, rightPiiValue, MeanPiiValue });
                    if (Math.Abs(LastPiiValue - meanStd.Item1) > 3 * meanStd.Item2) 
                        return false;
                }

                //orientation change
                float thetaTopL = (float)Math.Abs(Math.Atan(((double)(yMinC - yMinCl) / (Dx + wC / 2d + wCl / 2d))));
                float thetaTopR = (float)Math.Abs(Math.Atan(((double)(yMinC - yMinCr) / (Dxr + wC / 2d + wCr / 2d))));
                float thetaBotL = (float)Math.Abs(Math.Atan(((double)(yMaxC - yMaxCl) / (Dx + wC / 2d + wCl / 2d))));
                float thetaBotR = (float)Math.Abs(Math.Atan(((double)(yMaxC - yMaxCr) / (Dxr + wC / 2d + wCr / 2d))));
                float theta, dTheta, thetaL, thetaR;
                if (indexOfLeft == 0)
                {
                    thetaL = Math.Min(thetaTopL, thetaBotL);
                    thetaR = Math.Min(thetaTopR, thetaBotR);
                    theta = Math.Min(thetaL, thetaR);
                    dTheta = theta;
                }
                else
                {
                    float dThetaTop = Math.Min(Math.Abs(thetas[indexOfLeft] - thetaTopL), Math.Abs(thetas[indexOfLeft] - thetaTopR));
                    float dThetaBot = Math.Min(Math.Abs(thetas[indexOfLeft] - thetaBotL), Math.Abs(thetas[indexOfLeft] - thetaBotR));
                    dTheta = Math.Min(dThetaTop, dThetaBot);
                    theta = dThetaTop < dThetaBot ? Math.Min(thetaTopL, thetaTopR) : Math.Min(thetaBotL, thetaBotR);
                }
                // dynamic threshold calculations
                TRh = (float)Math.Min(0.3 * Math.Abs(Dx) + 0.3, 0.8);
                TRv = (float)Math.Min(0.25 * Math.Abs(Dx) + 0.3, 0.8);
                TDc = -100 * Math.Abs(Dx) + 300;
                //float TDclab = 12;

                if (Dx < TDx && Rh > TRh && Rv > TRv && Dc < TDc && dTheta < TTHETAinRads && Dh > -0.75 && Math.Abs(LastPiiValue - MeanPiiValue) < 0.2 * MeanPiiValue)
                {
                    lastTheta = theta;
                    // o pinakas thetas se kathe thesh exei to theta tou er sto indexOfLeft me to er sto epomeno index (sto ersOfLine).
                    return true;
                }

                return false;

            }

            private bool CanMergeRight(ERStat er, ERStat rightMost, Mat src3Channels, Mat channel)
            {
                int hC = er.rect.Height, hCl = rightMost.rect.Height;
                int wC = er.rect.Width, wCl = rightMost.rect.Width;
                int yMinC = er.rect.Top, yMinCl = rightMost.rect.Top;
                int yMaxC = er.rect.Bottom, yMaxCl = rightMost.rect.Bottom;
                int xMinC = er.rect.Left, xMinCl = rightMost.rect.Left;
                int xMaxC = er.rect.Right, xMaxCl = rightMost.rect.Right;

                // left overlap
                int leftOverlap = xMinC - xMaxCl;
                if ((leftOverlap < 0 && Math.Abs(leftOverlap) > wCl * 0.1)) return false;
                //interval 
                float Dx = (float)(leftOverlap) / hCl;
                // horizontal overlap
                float Dh = (float)(leftOverlap) / wC;
                //height differences
                float Rh = (float)Math.Min(hCl, hC) / Math.Max(hCl, hC);
                //vertical overlap
                float Rv = ((float)Math.Min(yMaxC, yMaxCl) - Math.Max(yMinC, yMinCl)) / Math.Max(hCl, hC);
                //color difference
                float Dc, leftPiiValue;
                //float Dclab;
                using (Mat mask1 = Utils.ErMaskFloodFill(channel, er, false))
                using (Mat mask2 = Utils.ErMaskFloodFill(channel, rightMost, false))
                {
                    Dc = ERUtils.ColorDistance2Mats(mask1, mask2, src3Channels, er.rect, rightMost.rect, ERUtils.EuclidianColorDistance);

                    LastPiiValue = ERUtils.ColorDistanceMatValue(mask1, src3Channels, er.rect, new MCvScalar(0, 0, 0), ERUtils.EuclidianColorDistance);
                    leftPiiValue = ERUtils.ColorDistanceMatValue(mask2, src3Channels, rightMost.rect, new MCvScalar(0, 0, 0), ERUtils.EuclidianColorDistance);

                    Tuple<float, float> meanStd = Utils.ComputeMeanStd(new float[] { leftPiiValue, MeanPiiValue });
                    if (Math.Abs(LastPiiValue - meanStd.Item1) > 3 * meanStd.Item2)
                        return false;
                }

                //orientation change
                float thetaTop = (float)Math.Abs(Math.Atan(((double)(yMinC - yMinCl) / (Dx + wC / 2d + wCl / 2d))));
                float thetaBot = (float)Math.Abs(Math.Atan(((double)(yMaxC - yMaxCl) / (Dx + wC / 2d + wCl / 2d))));
                float dThetaTop = Math.Abs(thetas[thetas.Count - 1] - thetaTop);
                float dThetaBot = Math.Abs(thetas[thetas.Count - 1] - thetaBot);
                float dTheta = Math.Min(dThetaTop, dThetaBot);
                float theta = dThetaTop < dThetaBot ? thetaTop : thetaBot;
                
                // dynamic threshold calculations
                TRh = (float)Math.Min(0.2 * Math.Abs(Dx) + 0.4, 0.8);
                TRv = (float)Math.Min(0.25 * Math.Abs(Dx) + 0.3, 0.8);
                TDc = 300 - 100 * Math.Abs(Dx);
                //float TDclab = 12;

                if (Dx < TDx && Rh > TRh && Rv > TRv && Dc < TDc && dTheta < TTHETAinRads && Dh > -0.75 && Math.Abs(LastPiiValue - MeanPiiValue) < 0.2 * MeanPiiValue)
                {
                    lastTheta = theta;
                    return true;
                }

                return false;
            }
            
            public Rectangle LineToRectangle()
            {
                wordRect = new Rectangle();
                if (IsValidLine())
                {
                    wordRect = ersOfLine[0].rect;
                    for (int i = 0; i < ersOfLine.Count; i++)
                    {
                        wordRect = Rectangle.Union(wordRect, ersOfLine[i].rect);
                    }
                }
                return wordRect;
            }
            public bool IsValidLine()
            {
                return ersOfLine.Count > 2;
            }
            public bool Includes(ERStat er)
            {
                foreach (ERStat lEr in ersOfLine)
                {
                    if (lEr.Equals(er)) return true;
                }
                return false;
            }

            public LineEstimate LineLMS()
            {
                if (!IsValidLine()) return null;
                List<Point> pointsTL = new List<Point>(), pointsBL = new List<Point>();
                int hMax = -1;
                for (int i = 0; i < ersOfLine.Count; i++)
                {
                    pointsBL.Add(new Point(ersOfLine[i].rect.Left, ersOfLine[i].rect.Bottom));
                    pointsTL.Add(new Point(ersOfLine[i].rect.Left, ersOfLine[i].rect.Top));
                    if (ersOfLine[i].rect.Height > hMax) hMax = ersOfLine[i].rect.Height;
                }
                float interceptB0, interceptB1, interceptT0, interceptT1, slope;
                slope = GetBestSlope(pointsBL);
                float maxErrB = GetLines(pointsBL, slope, hMax, out interceptB0, out interceptB1);
                float maxErrT = GetLines(pointsTL, slope, hMax, out interceptT0, out interceptT1);

                lineFeature = Math.Max(maxErrB / hMax, maxErrT / hMax);
                lineEstimates = new LineEstimate(interceptB0, interceptB1, interceptT0, interceptT1, slope, maxErrB, maxErrT);
               
                return lineEstimates;
            }

            public static bool LinesFit(Line a, Line b)
            {
                float aLeftestX = a.wordRect.Left;
                float aRightestX = a.wordRect.Right;
                float bLeftestX = b.wordRect.Left;
                float bRightestX = b.wordRect.Right;

                float horizontalOverlap = (Math.Min(aRightestX, bRightestX) - Math.Max(aLeftestX, bLeftestX)) / Math.Min(a.wordRect.Width, b.wordRect.Width);
                if (horizontalOverlap < 0.4) return false;

                float aTop = Math.Min(a.lineEstimates.interceptT0, a.lineEstimates.interceptT1);
                float aBot = Math.Max(a.lineEstimates.interceptB0, a.lineEstimates.interceptB1);
                float bTop = Math.Min(b.lineEstimates.interceptT0, b.lineEstimates.interceptT1);
                float bBot = Math.Max(b.lineEstimates.interceptB0, b.lineEstimates.interceptB1);

                // check if second line is between top two or bottom two lines (if they exist). if true then dont mix them because second line just happens to be in the area of the first
                //if (a.wordRect.Contains(b.wordRect))
                //{
                    if (a.lineEstimates.interceptT0 != a.lineEstimates.interceptT1)
                        if (bBot < Math.Max(a.lineEstimates.interceptT0, a.lineEstimates.interceptT1)) return false;
                    if (a.lineEstimates.interceptB0 != a.lineEstimates.interceptB1)
                        if (bTop > Math.Min(a.lineEstimates.interceptB0, a.lineEstimates.interceptB1)) return false;
                //}
                //else if (b.wordRect.Contains(a.wordRect))
                //{
                    if (b.lineEstimates.interceptT0 != b.lineEstimates.interceptT1)
                        if (aBot < Math.Max(b.lineEstimates.interceptT0, b.lineEstimates.interceptT1)) return false;
                    if (b.lineEstimates.interceptB0 != b.lineEstimates.interceptB1)
                        if (aTop > Math.Min(a.lineEstimates.interceptB0, a.lineEstimates.interceptB1)) return false;
                //}


                float verticalOverlap = (Math.Min(aBot, bBot) - Math.Max(aTop, bTop)) / Math.Min(a.wordRect.Height, b.wordRect.Height);
                if (verticalOverlap < 0.3) return false;
               
                return true;
            }

            public static float GetBestSlope(List<Point> points)
            {
                float slope = 0;  // slope - klish
                int nPoints = points.Count;
                if (nPoints == 0) return -1;

                float xMean = 0, yMean = 0, sumMulXY = 0, sumXSquares = 0;
                float[] distX = new float[nPoints], distY = new float[nPoints], mulDistXY = new float[nPoints], distX2 = new float[nPoints];

                for (int i = 0; i < nPoints; i++)
                {
                    xMean += points[i].X;
                    yMean += points[i].Y;
                }
                xMean /= nPoints;
                yMean /= nPoints;

                for (int i = 0; i < nPoints; i++)
                {
                    distX[i] = points[i].X - xMean;
                    distY[i] = points[i].Y - yMean;
                    mulDistXY[i] = distX[i] * distY[i];
                    distX2[i] = distX[i] * distX[i];
                    sumMulXY += mulDistXY[i];
                    sumXSquares += distX2[i];
                }
                sumMulXY /= nPoints;
                sumXSquares /= nPoints;

                slope = sumMulXY / sumXSquares;
                return slope;
            }
            public static float GetLines(List<Point> points, float slope, int hMax, out float intercept0, out float intercept1)
            {
                intercept0 = 0; intercept1 = 0;
                float err, maxErr = float.MinValue;
                int[] pointsY = new int[points.Count];
                for (int i = 0; i < points.Count; i++)
                {
                    pointsY[i] = points[i].Y;
                }
                using (Matrix<int> poiY = new Matrix<int>(pointsY))
                {
                    double min, max;
                    CvInvoke.MinMaxIdx(poiY, out min, out max, null, null);
                    Mat m = new Mat();
                    CvInvoke.Normalize(poiY, m, 0, 255, Emgu.CV.CvEnum.NormType.MinMax, Emgu.CV.CvEnum.DepthType.Cv8U);
                    ColorQuantizationWrapper.ColorQuantizationControler(m, 2, ref m);
                    CvInvoke.Normalize(m, m, 0, 1, Emgu.CV.CvEnum.NormType.MinMax, Emgu.CV.CvEnum.DepthType.Cv8U);
                    CvInvoke.Threshold(m, m, 0, 255, Emgu.CV.CvEnum.ThresholdType.Binary);
                    MCvScalar m1 = new MCvScalar(), m2 = new MCvScalar();
                    m1 = CvInvoke.Mean(poiY, m);
                    CvInvoke.BitwiseNot(m, m);
                    m2 = CvInvoke.Mean(poiY, m);
                    intercept0 = (float)m1.V0;
                    intercept1 = (float)m2.V0;

                    for (int i = 0; i < poiY.Rows; i++) // λιγο ντοκουμεντεισιον δε βλαπτει γαμω το κεφαλι σου. η απόσταση κάθε σημείου δεν έχει σημασία από ποια γραμμή θα υπολογιστεί, αφού αν υπάρχει η αποιτούμενη απόσταση για error τότε θα υπολογιστεί όταν θα ελέγχεται με τη λάθος γραμμή. Αν δεν υπάρχει το errer όποιος έλεγχος και να γίνει δε θα έχει σημασία.
                    {
                        err = Math.Abs(poiY[i, 0] - (intercept0 + slope * points[i].X));
                        if (err > maxErr) maxErr = err;
                    }
                    if (maxErr < hMax / 6f)
                    {
                        intercept0 = Math.Max(intercept0, intercept1);
                        intercept1 = intercept0;
                    }
                    else
                    {
                        maxErr = 0;
                        for (int i = 0; i < poiY.Rows; i++)
                        {
                            err = Math.Min(Math.Abs(poiY[i, 0] - (intercept0 + slope * points[i].X)), Math.Abs(poiY[i, 0] - (intercept1 + slope * points[i].X)));
                            if (err > maxErr) maxErr = err;
                        }
                    }
                    m.Dispose();
                    return maxErr;
                }
            }
        }
        private class ERComparer : Comparer<ERStat>
        {
            /// <summary>
            /// Use this comparer to put one er before another in a list according to: 
            /// <para />
            /// 1) position of X coord ("leftest" goes first)
            /// <para />
            /// 2) if x is the same then biggest area goes first
            /// <para />
            /// 3) if all are the same then check for parent child relationship, parent goes first
            /// </summary>
            /// <param name="er1"></param>
            /// <param name="er2"></param>
            /// <returns></returns>
            public override int Compare(ERStat er1, ERStat er2)
            {
                if (er1.rect.X.CompareTo(er2.rect.X) != 0)
                {
                    return er1.rect.X.CompareTo(er2.rect.X);
                }
                else if ((er1.rect.Width * er1.rect.Height).CompareTo(er2.rect.Width * er2.rect.Height) != 0)
                {
                    return ((er1.rect.Width * er1.rect.Height).CompareTo(er2.rect.Width * er2.rect.Height)) * (-1);
                }
                else if (er1.area.CompareTo(er2.area) != 0)
                {
                    return (er1.area.CompareTo(er2.area)) * (-1);
                }
                else if (er1.parent != null && er1.parent.Equals(er2))
                    return 1;
                else if (er2.parent != null && er2.parent.Equals(er1))
                    return -1;
                else return 0;
            }
        }
        public class LineComparer : Comparer<Line>
        {
            public override int Compare(Line x, Line y)
            {
                if (x.wordRect.Width * x.wordRect.Height > y.wordRect.Width * y.wordRect.Height)
                    return 1;
                else return -1;
            }
        }
        public class RectComparer : Comparer<Rectangle>
        {
            public override int Compare(Rectangle x, Rectangle y)
            {
                if (x.Width * x.Height > y.Width * y.Height)
                    return 1;
                else return -1;
            }
        }

        public static void Print(List<ERStat> regs)
        {
            List<Tuple<ERStat, int>> regInd = new List<Tuple<ERStat, int>>();
            for (int i = 0; i < regs.Count; i++)
                regInd.Add(Tuple.Create<ERStat, int>(ERStat.DeepCopy(regs[i]), i));
            for (int i = 0; i < regInd.Count; i++)
            {
                Debug.WriteLine("at pos: " + i + "erstat rect: " + regInd[i].Item1.rect.ToString() + " index: " + regInd[i].Item2);
            }
            regInd.Sort((x, y) => x.Item1.rect.X.CompareTo(y.Item1.rect.X));
            for (int i = 0; i < regInd.Count; i++)
            {
                Debug.WriteLine("at pos: " + i + "erstat rect: " + regInd[i].Item1.rect.ToString() + " index: " + regInd[i].Item2);
            }
        }

        public static List<Line> DoChineseGroupings(List<ERStat> regs, IInputArray input3Channels, IInputArray inputChannel)
        {
            using (Mat src1Channel = inputChannel.GetInputArray().GetMat())
            using (Mat src3Channels = input3Channels.GetInputArray().GetMat())
            {
                if (src1Channel.NumberOfChannels > 1) throw new Exception("lathos kanalia, prepei na einai 1");
                if (src3Channels.NumberOfChannels != 3) throw new Exception("lathos kanalia, prepei na einai 3");

                //Print(regs);

                List<ERStat> regions = new List<ERStat>(regs);

                //RecursiveFunctions.TreeSanityCheck(regions[0]);
                regions.Sort(new ERComparer());
                //regions = RegionsInclusionCheck(regions);
                List<Line> lines = new List<Line>();

                for (int i = 0; i < regions.Count; i++)
                {
                    if (regions[i].parent == null) continue;
                    ERStat thisEr = regions[i];
                    ERStat parent = thisEr.parent;
                    bool erAddedToLine = false;
                    // check if the parent of "thisEr" exists inside any line. if it does dont bother with the region and just skip it.
                    if (parent != null)
                        foreach (Line l in lines) 
                            if (l.Includes(parent))
                            {
                                RemoveFromTree(regions, ref i, ref thisEr, parent);
                                erAddedToLine = true;
                                break;
                            }
                    if (erAddedToLine) continue;
                    foreach (Line l in lines)
                    {
                        if (l.CanAdd(thisEr, src3Channels, src1Channel))
                        {
                            // to trito kommati ths if einai se periptwsh poy o parent einai META to child mesa sto "regions"
                            if (parent != null && parent.parent != null && l.CanAdd(parent, src3Channels, src1Channel))
                            {
                                RemoveFromTree(regions, ref i, ref thisEr, parent);
                                thisEr = parent;
                            }
                            l.AddER(thisEr, src1Channel, src3Channels);
                            erAddedToLine = true;
                            break;
                        }
                    }             
                    if (!erAddedToLine)
                    {
                        lines.Add(new Line());
                        lines[lines.Count - 1].AddER(thisEr, src1Channel, src3Channels);
                    }
                }
                List<ERStat> removedRegions = RetrieveLostRegions(lines);
                lines = FinalizeLines(lines);
                lines = MixLines(lines, src3Channels, src1Channel);

                lines = FeedBackLoop(lines, removedRegions, src3Channels, src1Channel);

                return lines;
            }
        }
        
        private static List<ERStat> RetrieveLostRegions(List<Line> lines)
        {
            List<ERStat> regions = new List<ERStat>();

            foreach (Line l in lines)
                if (!l.IsValidLine())
                    regions.AddRange(l.ersOfLine);
            return regions;
        }

        /// <summary>
        /// Must be run once per channel. Fills the lines with possible misclassifications of the character classifier.
        /// </summary>
        /// <param name="lines">The lines that have been found from the channel</param>
        /// <param name="input3Channels">The 3 channel image for calculations</param>
        /// <param name="inputChannel">The main channel</param>
        /// <returns></returns>
        public static List<Line> FeedBackLoop(List<Line> lines, List<ERStat> removedRegions, IInputArray input3Channels, IInputArray inputChannel)
        {
            if (removedRegions == null || removedRegions.Count == 0) return lines;
            removedRegions.Sort(new ERComparer());
            using (Mat src3Channel = input3Channels.GetInputArray().GetMat(), src1Channel = inputChannel.GetInputArray().GetMat())
            {
                for (int i = 0; i < removedRegions.Count; i++)
                {
                    for (int j = 0; j < lines.Count; j++)
                    {
                        ERStat thisEr = removedRegions[i];
                        if (lines[j].CanAddAnywhereInLine(thisEr, src3Channel, src1Channel))
                        {
                            ERStat parent = thisEr.parent;
                            if (parent != null && parent.parent != null && lines[j].Includes(parent))
                                RemoveFromTree(removedRegions, ref i, ref thisEr, parent);
                            else lines[j].AddER(thisEr, src1Channel, src3Channel);
                            break;
                        }
                    }
                }
                lines = FinalizeLines(lines);
                lines = MixLines(lines, src3Channel, src1Channel);
                return lines;
            }
        }

        /// <summary>
        /// Clear the list of lines by checking for valid and invalid lines
        /// Calculate line features i.e. lineToRectangle, and line LMS for the valid lines
        /// </summary>
        /// <param name="lines">The initial list of lines - valid and invalid</param>
        /// <returns>The valid lines with the calculated features.</returns>
        private static List<Line> FinalizeLines(List<Line> lines)
        {
            for (int i = 0; i < lines.Count; i++)
            {
                if (lines[i].IsValidLine())
                {
                    lines[i].LineToRectangle();
                    lines[i].LineLMS();
                }
                else
                {
                    lines.RemoveAt(i);
                    i--;
                }
            }
            return lines;
        }

        /// <summary>
        /// Check if any two lines can be mixed together into one, to avoid overlaps in the final results.
        /// If so mix the two, delete them and add the combination of the two to the final list of lines.
        /// </summary>
        /// <param name="lines">The input lines</param>
        /// <param name="src3Channel">The color image</param>
        /// <param name="src1Channel">The 1 channel</param>
        /// <returns></returns>
        private static List<Line> MixLines(List<Line> lines, Mat src3Channel, Mat src1Channel)
        {
            for (int i = 0; i < lines.Count; i++)
            {
                for (int j = i + 1; j < lines.Count; j++)
                {
                    if (DoubleLines(lines[i], lines[j]) && Line.LinesFit(lines[i], lines[j]))
                    {
                        Line l = MixDoubles(lines[i], lines[j], src3Channel, src1Channel);
                        lines.RemoveAt(j);
                        lines.RemoveAt(i);
                        i--;
                        l.LineToRectangle();
                        l.LineLMS();
                        lines.Add(l);
                        break;
                    }
                }
            }
            return lines;
        }

        /// <summary>
        /// Check if two lines can be mixed or not.
        /// </summary>
        /// <param name="lineA"></param>
        /// <param name="lineB"></param>
        /// <returns>True if they can, false otherwise.</returns>
        private static bool DoubleLines(Line lineA, Line lineB, float perc = 0.3f)
        {
            Rectangle intersection = Rectangle.Intersect(lineA.wordRect, lineB.wordRect);
            if (lineA.wordRect.Contains(lineB.wordRect) || lineB.wordRect.Contains(lineA.wordRect) ||
                intersection.Width * intersection.Height > perc * lineA.wordRect.Width * lineA.wordRect.Height ||
                intersection.Width * intersection.Height > perc * lineB.wordRect.Width * lineB.wordRect.Height)
            {
                return true;
            }
            return false;
        }

        /// <summary>
        /// Mix the Extremal Regions of two lines, to create a combination of the two.
        /// </summary>
        /// <param name="lineA"></param>
        /// <param name="lineB"></param>
        /// <param name="src3Channels"></param>
        /// <param name="src1Channel"></param>
        /// <returns></returns>
        private static Line MixDoubles(Line lineA, Line lineB, Mat src3Channels, Mat src1Channel)
        {
            List<ERStat> ersOfA = new List<ERStat>(lineA.ersOfLine);
            List<ERStat> ersOfB = new List<ERStat>(lineB.ersOfLine);

            for (int i = 0; i < ersOfA.Count; i++)
            {
                for (int j = 0; j < ersOfB.Count; j++)
                {
                    if (ersOfA[i].rect.Contains(ersOfB[j].rect)||(ersOfB[j].rect.Contains(ersOfA[i].rect)))
                    if (ersOfA[i].probability > ersOfB[j].probability)
                    {
                        ersOfB.RemoveAt(j);
                        j--;
                        break;
                    }
                    else
                    {
                        ersOfA.RemoveAt(i);
                        i--;
                        break;
                    }
                }
            }
            List<ERStat> allERs = new List<ERStat>();
            allERs.AddRange(ersOfA);
            allERs.AddRange(ersOfB);

            allERs.Sort(new ERComparer());
            Line l = new Line();
            l.AddER(allERs[0], src1Channel, src3Channels);

            for (int i = 1; i < allERs.Count; i++)
            {
                ERStat thisEr = allERs[i];
                //if (l.CanAdd(allERs[i], src3Channels, src1Channel))
                    l.AddER(thisEr, src1Channel, src3Channels);
            }
            return l;

        }

        /// <summary>
        /// Removes a region from the list and the tree. To remove from the tree it connects all the children with the new parents and vice versa, new sibling connections as well.
        /// </summary>
        /// <param name="regions"></param>
        /// <param name="i"></param>
        /// <param name="thisEr"></param>
        /// <param name="parent"></param>
        private static void RemoveFromTree(List<ERStat> regions, ref int i, ref ERStat thisEr, ERStat parent)
        {
            if (parent != null) // added gia to inclusion check
            {
                if (parent.child != null && parent.child.Equals(thisEr))
                {
                    if (thisEr.child != null) parent.child = thisEr.child;
                    else if (thisEr.next != null) parent.child = thisEr.next;
                    else if (thisEr.prev != null)
                        parent.child = thisEr.prev; // should never get in here
                    else parent.child = null;
                } 
            }
            if (thisEr.prev != null && thisEr.prev.next != null && thisEr.prev.next.Equals(thisEr)) thisEr.prev.next = thisEr.next;
            if (thisEr.next != null && thisEr.next.prev != null && thisEr.next.prev.Equals(thisEr)) thisEr.next.prev = thisEr.prev;
            if (thisEr.child != null && thisEr.child.parent.Equals(thisEr))
            {
                thisEr.child.parent = thisEr.parent;
                while (thisEr.child.next != null && thisEr.child.next.parent != null && thisEr.child.next.parent.Equals(thisEr))
                {
                    thisEr.child.next.parent = thisEr.parent;
                }
            }
            thisEr = null;
            regions.RemoveAt(i);
            i--;
        }
    }
}
