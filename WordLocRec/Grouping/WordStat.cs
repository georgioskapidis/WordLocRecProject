﻿using Emgu.CV;
using Emgu.CV.Structure;
using Emgu.CV.Util;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using WordLocRec.DataStructs;

namespace WordLocRec.Grouping
{
    public class WordStat : BaseFeature
    {
        //public Rectangle WordRect { get; set; }

        [XmlIgnore]
        public List<ERStat> ERs;
        [XmlIgnore]
        public List<WordERFeatures> features;


        public int numberOfERs;
        public Size imageSize;

        public String imageName;
        public String channel;

        public float FGIntensitiesStd { get; set; }
        public float BGIntensitiesStd { get; set; }
        public float MajorAxisCoeffVariation { get; set; }
        public float StrokeWidthsCoeffVariation { get; set; }
        public float MeanGradientStd { get; set; }
        public float AspectRatioCoeffVariation { get; set; }
        public float HuMomentsAvgEuclideanDistance { get; set; }
        public float ConvexHullCompactnessMean { get; set; }
        public float ConvexHullCompactnessStd { get; set; }
        public float ConvexityDefectsCoeffVariation { get; set; }
        public MSTFeatures mstFeatures { get; set; }

        public class MSTFeatures
        {
            protected class Edge
            {
                public Point start;
                public Point end;
                public double EdgeLength { get; private set; }
                public Edge(Point a, Point b)
                {
                    start = a;
                    end = b;
                    EdgeLength = Norm(a, b);
                }
                /// <summary>
                /// Calculates Norm(a-b) which is equal to Norm(b-a) just sayings
                /// </summary>
                /// <param name="a"></param>
                /// <param name="b"></param>
                /// <returns></returns>
                private static double Norm(Point a, Point b)
                {
                    Point ab = new Point(a.X - b.X, a.Y - b.Y);
                    return Norm(ab);
                }
                private static double Norm(Point ab)
                {
                    return Math.Sqrt(Math.Pow(ab.X, 2) + Math.Pow(ab.Y, 2));
                }
            }
            public float AnglesMean { get; set; }
            public float AnglesStd { get; set; }
            public float EdgeWidthsCoeffVariation { get; set; }
            public float EdgeDistancesMeanDiametersMeanRatio { get; set; }

            private Size bounds;
            public MSTFeatures()
            {
                AnglesMean = 0;
                AnglesStd = 0;
                EdgeWidthsCoeffVariation = 0;
                EdgeDistancesMeanDiametersMeanRatio = 0;
            }
            public MSTFeatures(List<ERStat> ers, Size bounds)
            {
                this.bounds = bounds;
                List<Point> vertices = MakeVertices(ers); 
                List<Edge> edges = MakeEdges(vertices);
                List<List<Point>> forest = MakeForest(vertices); 
                edges.Sort(edgeComparer);

                List<float> mstEdgeDistances;
                List<Edge> mstEdges = ExtractMSTKruskal(ers, bounds, edges, forest, out mstEdgeDistances);
                List<float> angles = CalcMSTAngles(mstEdges);

                Tuple<float, float> anglesMeanStd = Utils.ComputeMeanStd(angles.ToArray());
                AnglesMean = anglesMeanStd.Item1;
                AnglesStd = anglesMeanStd.Item2;

                Tuple<float, float> edgeDistancesMeanStd = Utils.ComputeMeanStd(mstEdgeDistances.ToArray());
                EdgeWidthsCoeffVariation = edgeDistancesMeanStd.Item2 / edgeDistancesMeanStd.Item1;

                List<float> diameters = GetDiameters(ers);
                EdgeDistancesMeanDiametersMeanRatio = edgeDistancesMeanStd.Item1 / Utils.ComputeMean(diameters.ToArray()); // TODO: σε μία άλλη ζωή που θα ξανασχοληθώ με αυτά, αντί για mean, να πάρω την αναλογία των coeff. Έχει πιο πολύ νόημα για την αποτύπωση διαφοροποίησης μεταξύ των bb των er και των αποστάσεων μεταξύ τους. π.χ. λόγος κοντά στο 1 σημαίνει ότι όσο διαφορετικά μεταξύ τους είναι τα bb άλλο τόσο διαφορετικές μεταξύ τους είναι και οι αποστάσεις, άρα ίσως περιγράφουν πράγματα που ισαπέχουν π.χ. κάγκελα, τούβλα, παράθυρα. Ακόμα καλύτερα, κάποιος άλλος τρόπος συσχέτισης των δύο coeff για να μην συσχετίζονται οι περιπτώσεις που έχουν πολύ μεγάλες τιμές και οι δύο όροι με τις περιπτώσεις που έχουν πολύ μικρή τιμή και οι δύο όροι.
            }
            private List<float> GetDiameters(List<ERStat> ers)
            {
                List<float> diameters = new List<float>();
                foreach (ERStat er in ers)
                {
                    diameters.Add((float)Math.Sqrt(Math.Pow(er.rect.Width, 2) + Math.Pow(er.rect.Height, 2)));
                }
                return diameters;
            }
            private List<float> CalcMSTAngles(List<Edge> mstEdges)
            {
                List<float> angles = new List<float>();
                for (int k = 0; k < mstEdges.Count; k++)
                {
                    Edge q = mstEdges[k];
                    Point q_pt0 = q.start;
                    Point q_pt1 = q.end;
                    for (int j = k + 1; j < mstEdges.Count; j++)
                    {
                        Edge t = mstEdges[j];
                        Point t_pt0 = t.start;
                        Point t_pt1 = t.end;
                        if (q_pt0 == t_pt0)
                            angles.Add((float)GetAngleABC(q_pt1, q_pt0, t_pt1));
                        if (q_pt0 == t_pt1)
                            angles.Add((float)GetAngleABC(q_pt1, q_pt0, t_pt0));
                        if (q_pt1 == t_pt0)
                            angles.Add((float)GetAngleABC(q_pt0, q_pt1, t_pt1));
                        if (q_pt1 == t_pt1)
                            angles.Add((float)GetAngleABC(q_pt0, q_pt1, t_pt0));
                    }
                }
                return angles;
            }
            private static int GetAngleABC(Point a, Point b, Point c)
            {
                Point ab = new Point(b.X - a.X, b.Y - a.Y);
                Point cb = new Point(b.X - c.X, b.Y - c.Y);

                // dot product
                float dot = (float)(ab.X * cb.X + ab.Y * cb.Y);

                // length square of both vectors
                float abSqr = (float)(ab.X * ab.X + ab.Y * ab.Y);
                float cbSqr = (float)(cb.X * cb.X + cb.Y * cb.Y);

                // square of cosine of needed angle
                float cosSqr = dot * dot / abSqr / cbSqr;

                // this is a known trigonometric equality
                // cos(alpha * 2) = [ cos(alpha)^2 * 2 - 1]
                float cos2 = 2 * cosSqr - 1;

                // Here's the only invocation of the heavy function.
                // It's a good idea to check explicitly if cos2 is within [-1 .. 1] range

                const float pi = 3.141592f;

                float alpha2 =
                    (cos2 <= -1) ? pi :
                    (cos2 >= 1) ? 0 :
                    (float)Math.Acos(cos2);

                float rslt = alpha2 / 2;

                float rs = (float)(rslt * 180.0 / pi);


                // Now revolve the ambiguities.
                // 1. If dot product of two vectors is negative - the angle is definitely
                // above 90 degrees. Still we have no information regarding the sign of the angle.

                // NOTE: This ambiguity is the consequence of our method: calculating the cosine
                // of the double angle. This allows us to get rid of calling sqrt.

                if (dot < 0)
                    rs = 180 - rs;

                // 2. Determine the sign. For this we'll use the Determinant of two vectors.
                float det = (float)(ab.X * cb.Y - ab.Y * cb.X);
                if (det < 0)
                    rs = -rs;

                return Math.Abs((int)Math.Floor(rs + 0.5));
            }
            private List<Edge> ExtractMSTKruskal(List<ERStat> ers, Size bounds, List<Edge> edges, List<List<Point>> forest, out List<float> edgeDistances)
            {
                List<Edge> mstEdges = new List<Edge>();
                edgeDistances = new List<float>();

                // kruskal
                for (int i = 0; i < edges.Count; i++)
                {
                    Point start = edges[i].start;
                    Point end = edges[i].end;

                    int treeIndexStart = forest.FindIndex(byPoint(start)); // works!
                    int treeIndexEnd = forest.FindIndex(byPoint(end));

                    if (((start.X > 0) && (start.X < bounds.Width) && (start.Y > 0) && (start.Y < bounds.Height) &&
                         (end.X > 0) && (end.X < bounds.Width) && (end.Y > 0) && (end.Y < bounds.Height)) &&
                            (treeIndexStart != treeIndexEnd))
                    {
                        mstEdges.Add(edges[i]);
                        forest[treeIndexStart].InsertRange(0, forest[treeIndexEnd]);
                        forest.RemoveAt(treeIndexEnd);
                        edgeDistances.Add((float)edges[i].EdgeLength);
                    }
                    if (mstEdges.Count == ers.Count - 1)
                        break;
                }
                return mstEdges;
            }

            private Predicate<List<Point>> byPoint(Point start)
            {
                return delegate(List<Point> l)
                {
                    return l.Contains(start);
                };
            }
            private List<Edge> MakeEdges(List<Point> vertices)
            {
                List<Edge> edges = new List<Edge>();
                for (int i = 0; i < vertices.Count; i++)
                {
                    for (int j = i + 1; j < vertices.Count; j++)
                    {
                        edges.Add(new Edge(vertices[i], vertices[j]));
                    }
                }
                return edges;
            }
            private List<Point> MakeVertices(List<ERStat> ers)
            {
                List<Point> points = new List<Point>();
                for (int i = 0; i < ers.Count; i++)
                {
                    points.Add(GetCenterOfRect(ers[i].rect));
                }
                return points;
            }
            private List<List<Point>> MakeForest(List<Point> vertices)
            {
                List<List<Point>> forest = new List<List<Point>>();

                foreach (Point p in vertices)
                {
                    forest.Add(new List<Point>());
                    forest[forest.Count - 1].Add(p);
                }
                return forest;
            }
            private Point GetCenterOfRect(Rectangle rectangle)
            {
                return new Point(rectangle.X + rectangle.Width / 2, rectangle.Y + rectangle.Height / 2);
            }
            private static int edgeComparer(Edge a, Edge b)
            {
                return a.EdgeLength.CompareTo(b.EdgeLength);
            }
        }
        public WordStat()
        {
            WordRect = new Rectangle();
            imageSize = new Size();
            numberOfERs = 0;
            FGIntensitiesStd = 0;
            BGIntensitiesStd = 0;
            MajorAxisCoeffVariation = 0;
            StrokeWidthsCoeffVariation = 0;
            MeanGradientStd = 0;
            AspectRatioCoeffVariation = 0;
            HuMomentsAvgEuclideanDistance = 0;
            ConvexHullCompactnessMean = 0;
            ConvexHullCompactnessStd = 0;
            ConvexityDefectsCoeffVariation = 0;
            mstFeatures = new MSTFeatures();
        }
        public WordStat(Rectangle wordRect, IEnumerable<ERStat> erstats, Size imSize) : this()
        {
            this.imageSize = imSize;
            this.WordRect = wordRect;
            ERs = new List<ERStat>();
            foreach (ERStat er in erstats)
                ERs.Add(er);
            numberOfERs = ERs.Count;
        }

        public static WordStat FromFloatList(List<float> floatList)
        {
            WordStat ws = new WordStat();
            ws.FGIntensitiesStd = floatList[0];
            ws.BGIntensitiesStd = floatList[1];
            ws.MajorAxisCoeffVariation = floatList[2];
            ws.StrokeWidthsCoeffVariation = floatList[3];
            ws.MeanGradientStd = floatList[4];
            ws.AspectRatioCoeffVariation = floatList[5];
            ws.HuMomentsAvgEuclideanDistance = floatList[6];
            ws.ConvexHullCompactnessMean = floatList[7];
            ws.ConvexHullCompactnessStd = floatList[8];
            ws.ConvexityDefectsCoeffVariation = floatList[9];
            ws.mstFeatures.AnglesMean = floatList[10];
            ws.mstFeatures.AnglesStd = floatList[11];
            ws.mstFeatures.EdgeWidthsCoeffVariation = floatList[12];
            ws.mstFeatures.EdgeDistancesMeanDiametersMeanRatio = floatList[13];

            return ws;
        }

        /// <summary>
        /// One big bad methods to calculate all the features of a word rectangle. Not broken to pieces in order to save iterations.
        /// </summary>
        /// <param name="wordErFeatures">The ER features that are needed to calculate the WordStat features.</param>
        public void ComputeWordFeatures(List<WordERFeatures> wordErFeatures)
        {// ta vazw ola se mia for gia na glutwsw epanalhpseis
            this.features = new List<WordERFeatures>(wordErFeatures);

            float[] meanIntensitiesFG = new float[wordErFeatures.Count];
            float[] meanIntensitiesBG = new float[wordErFeatures.Count];
            float[] majorAxes = new float[wordErFeatures.Count];
            float[] strokeWidths = new float[wordErFeatures.Count];
            float[] meanGradients = new float[wordErFeatures.Count];
            float[] aspectRatios = new float[wordErFeatures.Count];
            float[] convexHullCompactnesses = new float[wordErFeatures.Count];
            float[] convexityDefectss = new float[wordErFeatures.Count];
            for (int i = 0; i < wordErFeatures.Count; i++)
            {
                meanIntensitiesFG[i] = wordErFeatures[i].ForegroundIntensity;
                meanIntensitiesBG[i] = wordErFeatures[i].BackgroundIntensity;
                majorAxes[i] = wordErFeatures[i].MajorAxis;
                strokeWidths[i] = wordErFeatures[i].StrokeWidth;
                meanGradients[i] = wordErFeatures[i].GradientMagnitude;
                aspectRatios[i] = wordErFeatures[i].AspectRatio;
                convexHullCompactnesses[i] = wordErFeatures[i].ConvexHullCompactness;
                convexityDefectss[i] = wordErFeatures[i].ConvexityDefects;
            }
            FGIntensitiesStd = Utils.ComputeStd(meanIntensitiesFG);
            BGIntensitiesStd = Utils.ComputeStd(meanIntensitiesBG);
            MajorAxisCoeffVariation = Utils.ComputeCoeffOfVariation(majorAxes);
            StrokeWidthsCoeffVariation = Utils.ComputeCoeffOfVariation(strokeWidths);
            MeanGradientStd = Utils.ComputeStd(meanGradients);
            AspectRatioCoeffVariation = Utils.ComputeCoeffOfVariation(aspectRatios);
            HuMomentsAvgEuclideanDistance = ComputeSimilarityAverageDistance(wordErFeatures);
            Tuple<float, float> convexHullCompactnessMeanStd = Utils.ComputeMeanStd(convexHullCompactnesses);
            ConvexHullCompactnessMean = convexHullCompactnessMeanStd.Item1;
            ConvexHullCompactnessStd = convexHullCompactnessMeanStd.Item2;
            ConvexityDefectsCoeffVariation = Utils.ComputeCoeffOfVariation(convexityDefectss);

            mstFeatures = new MSTFeatures(ERs, imageSize);
        }

        /// <summary>
        /// Convenience method to calculate all the WordStats from an image at once.
        /// </summary>
        /// <param name="wordRects">The word rectangles found in the image.</param>
        /// <param name="erListPerWordRect">The list of Tuples. Each Tuple contains the List of ERs that comprise the word rect and the index of the channel that they are found on.</param>
        /// <param name="mImage">The original image in bgr.</param>
        /// <param name="channels">The channels on which ER extraction occurs.</param>
        /// <param name="imageFile">The string of the image path.</param>
        /// <returns>The array of WordStat that are calculated.</returns>
        public static WordStat[] CreateMultiple(Rectangle[] wordRects, List<Tuple<List<ERStat>,int>> erListPerWordRect, Mat mImage, VectorOfMat channels, String imageFile)
        {
            WordStat[] wordStats = new WordStat[wordRects.Length];
            for (int i = 0; i < wordRects.Length; i++)
            {
                wordStats[i] = new WordStat(wordRects[i], erListPerWordRect[i].Item1, mImage.Size);
                WordERFeatures[] wordERFeatures = new WordERFeatures[erListPerWordRect[i].Item1.Count];
                for (int j = 0; j < erListPerWordRect[i].Item1.Count; j++)
                {
                    wordERFeatures[j] = new WordERFeatures(erListPerWordRect[i].Item1[j], channels[erListPerWordRect[i].Item2], mImage);
                }
                wordStats[i].ComputeWordFeatures(wordERFeatures.ToList());
                wordStats[i].imageName = imageFile;
                wordStats[i].channel = erListPerWordRect[i].Item2 == 0 ? "BgrGrayscaleF" : "BgrGrayscaleB";
            }
            return wordStats;
        }

        public static List<Tuple<List<ERStat>, int>> CreateErListPerWordRect(List<ERStat>[] regions, List<List<Tuple<int,int>>> groups, int channelNum = -1)
        {
            List<Tuple<List<ERStat>, int>> erListPerWordRect = new List<Tuple<List<ERStat>, int>>();
            foreach (List<Tuple<int, int>> wordERs in groups)
            {
                List<ERStat> tmp = new List<ERStat>();
                for (int i = 0; i < wordERs.Count; i++)
                {
                    tmp.Add(regions[wordERs[i].Item1][wordERs[i].Item2]);
                }
                erListPerWordRect.Add(Tuple.Create<List<ERStat>, int>(new List<ERStat>(tmp), channelNum != -1 ? channelNum : wordERs[0].Item1));
            }
            return erListPerWordRect;
        }

        /// <summary>
        /// Turn this WordStat object to a list of floats with the following order:
        /// <para />
        /// 0: FGIntensitiesStd, 1: BGIntensitiesStd, 2: MajorAxisCoeffVariation, 3: StrokeWidthsCoeffVariation, 4: MeanGradientStd, 5: AspectRatioCoeffVariation, 6: HuMomentsAvgEuclideanDistance, 7: ConvexHullCompactnessMean, 8: ConvexHullCompactnessStd, 9: ConvexityDefectsCoeffVariation, 10: mstAnglesMean, 11: mstAnglesStd, 12: mstEdgeWidthsCoeffVariation, 13: mstEdgeDistancesMeanDiametersMeanRatio
        /// </summary>
        /// <returns>The list of floats of count = 14</returns>
        public override List<float> ToFloatList()
        {
            List<float> result = new List<float>();
            result.Add(FGIntensitiesStd);
            result.Add(BGIntensitiesStd);
            result.Add(MajorAxisCoeffVariation);
            result.Add(StrokeWidthsCoeffVariation);
            result.Add(MeanGradientStd);
            result.Add(AspectRatioCoeffVariation);
            result.Add(HuMomentsAvgEuclideanDistance);
            result.Add(ConvexHullCompactnessMean);
            result.Add(ConvexHullCompactnessStd);
            result.Add(ConvexityDefectsCoeffVariation);
            result.Add(mstFeatures.AnglesMean);
            result.Add(mstFeatures.AnglesStd);
            result.Add(mstFeatures.EdgeWidthsCoeffVariation);
            result.Add(mstFeatures.EdgeDistancesMeanDiametersMeanRatio);
            return result;
        }
        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendLine("FG intensities STD: " + FGIntensitiesStd)
              .AppendLine("BG intensities STD: " + BGIntensitiesStd)
              .AppendLine("Major Axis CoV: " + MajorAxisCoeffVariation)
              .AppendLine("Stroke Widths CoV: " + StrokeWidthsCoeffVariation)
              .AppendLine("Mean Gradient STD: " + MeanGradientStd)
              .AppendLine("Aspect Ratio CoV: " + AspectRatioCoeffVariation)
              .AppendLine("Hu Moments Avg EuclideanDistance: " + HuMomentsAvgEuclideanDistance)
              .AppendLine("ConvexHullCompactnessMean: " + ConvexHullCompactnessMean)
              .AppendLine("ConvexHullCompactnessStd: " + ConvexHullCompactnessStd)
              .AppendLine("ConvexityDefects CoV: " + ConvexityDefectsCoeffVariation)
              .AppendLine("Mst angles mean: " + mstFeatures.AnglesMean)
              .AppendLine("Mst angles std: " + mstFeatures.AnglesStd)
              .AppendLine("Mst EdgeWidths CoV: " + mstFeatures.EdgeWidthsCoeffVariation)
              .AppendLine("Mst EdgeDistances: " + mstFeatures.EdgeDistancesMeanDiametersMeanRatio)
              .AppendLine();
            return sb.ToString();
        }
        public override string ToFlatString()
        {
            StringBuilder sb = new StringBuilder();
            int columnLength = 20;
            sb.Append(IOUtils.CreateColumn(FGIntensitiesStd.ToString(), columnLength, "_"));
            sb.Append(IOUtils.CreateColumn(BGIntensitiesStd.ToString(), columnLength, "_"));
            sb.Append(IOUtils.CreateColumn(MajorAxisCoeffVariation.ToString(), columnLength, "_"));
            sb.Append(IOUtils.CreateColumn(StrokeWidthsCoeffVariation.ToString(), columnLength, "_"));
            sb.Append(IOUtils.CreateColumn(MeanGradientStd.ToString(), columnLength, "_"));
            sb.Append(IOUtils.CreateColumn(AspectRatioCoeffVariation.ToString(), columnLength, "_"));
            sb.Append(IOUtils.CreateColumn(HuMomentsAvgEuclideanDistance.ToString(), columnLength, "_"));
            sb.Append(IOUtils.CreateColumn(ConvexHullCompactnessMean.ToString(), columnLength, "_"));
            sb.Append(IOUtils.CreateColumn(ConvexHullCompactnessStd.ToString(), columnLength, "_"));
            sb.Append(IOUtils.CreateColumn(ConvexityDefectsCoeffVariation.ToString(), columnLength, "_"));
            sb.Append(IOUtils.CreateColumn(mstFeatures.AnglesMean.ToString(), columnLength, "_"));
            sb.Append(IOUtils.CreateColumn(mstFeatures.AnglesStd.ToString(), columnLength, "_"));
            sb.Append(IOUtils.CreateColumn(mstFeatures.EdgeWidthsCoeffVariation.ToString(), columnLength, "_"));
            sb.Append(IOUtils.CreateColumn(mstFeatures.EdgeDistancesMeanDiametersMeanRatio.ToString(), columnLength, "_"));
            sb.AppendLine();

            return sb.ToString();
        }

        public void Standardize(WordStat mean, WordStat std)
        {
            FGIntensitiesStd = Utils.Standardize(FGIntensitiesStd, mean.FGIntensitiesStd, std.FGIntensitiesStd);
            BGIntensitiesStd = Utils.Standardize(BGIntensitiesStd, mean.BGIntensitiesStd, std.BGIntensitiesStd);
            MajorAxisCoeffVariation = Utils.Standardize(MajorAxisCoeffVariation, mean.MajorAxisCoeffVariation, std.MajorAxisCoeffVariation);
            StrokeWidthsCoeffVariation = Utils.Standardize(StrokeWidthsCoeffVariation, mean.StrokeWidthsCoeffVariation, std.StrokeWidthsCoeffVariation);
            MeanGradientStd = Utils.Standardize(MeanGradientStd, mean.MeanGradientStd, std.MeanGradientStd);
            AspectRatioCoeffVariation = Utils.Standardize(AspectRatioCoeffVariation, mean.AspectRatioCoeffVariation, std.AspectRatioCoeffVariation);
            HuMomentsAvgEuclideanDistance = Utils.Standardize(HuMomentsAvgEuclideanDistance, mean.HuMomentsAvgEuclideanDistance, std.HuMomentsAvgEuclideanDistance);
            ConvexHullCompactnessMean = Utils.Standardize(ConvexHullCompactnessMean, mean.ConvexHullCompactnessMean, std.ConvexHullCompactnessMean);
            ConvexHullCompactnessStd = Utils.Standardize(ConvexHullCompactnessStd, mean.ConvexHullCompactnessStd, std.ConvexHullCompactnessStd);
            ConvexityDefectsCoeffVariation = Utils.Standardize(ConvexityDefectsCoeffVariation, mean.ConvexityDefectsCoeffVariation, std.ConvexityDefectsCoeffVariation);
            mstFeatures.AnglesMean = Utils.Standardize(mstFeatures.AnglesMean, mean.mstFeatures.AnglesMean, std.mstFeatures.AnglesMean);
            mstFeatures.AnglesStd = Utils.Standardize(mstFeatures.AnglesStd, mean.mstFeatures.AnglesStd, std.mstFeatures.AnglesStd);
            mstFeatures.EdgeWidthsCoeffVariation = Utils.Standardize(mstFeatures.EdgeWidthsCoeffVariation, mean.mstFeatures.EdgeWidthsCoeffVariation, std.mstFeatures.EdgeWidthsCoeffVariation);
            mstFeatures.EdgeDistancesMeanDiametersMeanRatio = Utils.Standardize(mstFeatures.EdgeDistancesMeanDiametersMeanRatio, mean.mstFeatures.EdgeDistancesMeanDiametersMeanRatio, std.mstFeatures.EdgeDistancesMeanDiametersMeanRatio);
        }

        #region HuSimilarityMeasure
        /// <summary>
        /// Calculates the average similarity between all possible pairs of ERs in the word rectangle. Similarity is based on Hu Moments
        /// </summary>
        /// <param name="features">The list of ER features</param>
        /// <returns>The average intra-similarity of the ERs.</returns>
        private static float ComputeSimilarityAverageDistance(List<WordERFeatures> features)
        {
            float avgSimilarity = 0f;
            int numMatches = 0;
            for (int i = 0; i < features.Count; i++)
            {
                for (int j = i + 1; j < features.Count; j++)
                {
                    avgSimilarity += (float)Utils.SimilarityDistance(features[i].HuMoments, features[j].HuMoments);
                    numMatches++;
                }
            }
            return avgSimilarity / numMatches;
        }
        
        #endregion

    }
}
