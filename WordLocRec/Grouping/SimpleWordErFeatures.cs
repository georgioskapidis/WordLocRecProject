﻿using Emgu.CV;
using Emgu.CV.CvEnum;
using Emgu.CV.Structure;
using Emgu.CV.Util;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WordLocRec.Tools;

namespace WordLocRec.Grouping
{
    public class SimpleWordErFeatures
    {
        public float ForegroundIntensityMean { get; private set; }

        public float OuterBoundaryIntensityMean { get; private set; }

        public float ForegroundBackgroundLabDistance { get; private set; }

        public float PerimetricGradientMagnitudeMean { get; private set; }

        public float MeanStrokeWidth { get; private set; }

        public float ConvexityDefects { get; private set; }

        public float ConvexHullCompactness { get; private set; }

        public Point Center { get; private set; }

        public SimpleWordErFeatures(ERStat er, IInputArray imageChannel, IInputArray allChannels)
        {
            using (Mat src = imageChannel.GetInputArray().GetMat())
            {
                using (Mat src3Channels = allChannels.GetInputArray().GetMat())
                using (Mat erMask = Utils.ErMaskFloodFill(src, er))
                {
                    ForegroundIntensityMean = (float)ERUtils.CalcMeanColor(src, erMask, er.rect).V0;
                    if (float.IsNaN(ForegroundIntensityMean)) ForegroundIntensityMean = 0;

                    Mat outerBoundaryMask = new Mat();
                    Utils.GetOuterBoundary(erMask, ref outerBoundaryMask);
                    OuterBoundaryIntensityMean = (float)ERUtils.CalcMeanColor(src, outerBoundaryMask, er.rect).V0;
                    if (float.IsNaN(OuterBoundaryIntensityMean)) OuterBoundaryIntensityMean = 0;
                    outerBoundaryMask.Dispose();

                    using (Mat lab = new Mat())
                    {
                        CvInvoke.CvtColor(src3Channels, lab, ColorConversion.Bgr2Lab);
                        ForegroundBackgroundLabDistance = ERUtils.ColorDistanceERBB(src, lab, er, ERUtils.LabColorDistance);
                    }

                    Mat perimetricBoundary = new Mat();
                    Utils.GetPerimetricBoundary(erMask, ref perimetricBoundary);
                    Mat gradientMagnitude = new Mat();
                    ColorConversionWrapper.ColorConversions(src3Channels, ColorConversionWrapper.ManualConversions.Gradient, ref gradientMagnitude);
                    PerimetricGradientMagnitudeMean = (float)ERUtils.CalcMeanColor(gradientMagnitude, perimetricBoundary, er.rect).V0; //changed erMask to perimetricBoundary at 2nd variable, gia na allaksei o ypologismos toy gradient apo olh thn epifaneia toy er sto boundary
                    if (float.IsNaN(PerimetricGradientMagnitudeMean)) PerimetricGradientMagnitudeMean = 0;
                    gradientMagnitude.Dispose();
                    perimetricBoundary.Dispose();

                    MeanStrokeWidth = StrokeUtils.MeanStrokeWidth(erMask);
                    if (float.IsNaN(MeanStrokeWidth)) MeanStrokeWidth = 0;

                    using (VectorOfVectorOfPoint contours = Utils.CalcContours(erMask))
                    {
                        if (contours.Size > 0)
                        {
                            ConvexHullCompactness = Utils.CalcConvexHullAreaRatio((float)Utils.CalcConvexHullArea(contours), (float)Utils.CalcContourArea(contours));
                            if (float.IsNaN(ConvexHullCompactness)) ConvexHullCompactness = 0;
                            if (float.IsInfinity(ConvexHullCompactness)) ConvexHullCompactness = 10;

                            ConvexityDefects = Utils.CalcConvexityDefects(contours, Utils.CalcConvexHullPoints(contours));
                            if (float.IsNaN(ConvexityDefects)) ConvexityDefects = 0;
                        }
                    }
                }
            }
            Center = new Point(er.rect.X + er.rect.Width / 2, er.rect.Y + er.rect.Height / 2);
        }
    }
}
