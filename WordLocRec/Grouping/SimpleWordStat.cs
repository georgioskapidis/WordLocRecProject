﻿using Emgu.CV;
using Emgu.CV.Util;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using WordLocRec.DataStructs;

namespace WordLocRec.Grouping
{
    public class SimpleWordStat : BaseFeature
    {
        //public Rectangle WordRect { get; set; }

        [XmlIgnore]
        public List<ERStat> ERs;
        [XmlIgnore]
        public List<SimpleWordErFeatures> features;

        public int numberOfERs;
        public Size imageSize;

        public String imageName;
        public String channel;

        public float MeanForegroundIntensitiesCoV { get; set; }

        public float MeanOuterBoundaryIntensitiesCoV { get; set; }

        public float ForegroundBackgroundLabDistanceCoV { get; set; }

        public float PerimetricGradientMagnitudeMeanCoV { get; set; }

        public float MeanStrokeWidthCoV { get; set; }

        public float ConvexHullCompactnessMean { get; set; }
        public float ConvexHullCompactnessStd { get; set; }

        public float ConvexityDefectsCoV { get; set; }

        public float VerticalCenterDistanceCoV { get; set; }

        public float HorizontalCenterDistancesCoV { get; set; }

        public SimpleWordStat()
        {
            WordRect = new Rectangle();
            imageSize = new Size();
            numberOfERs = 0;
            MeanForegroundIntensitiesCoV = 0;
            MeanOuterBoundaryIntensitiesCoV = 0;
            ForegroundBackgroundLabDistanceCoV = 0;
            PerimetricGradientMagnitudeMeanCoV = 0;
            MeanStrokeWidthCoV = 0;
            ConvexHullCompactnessMean = 0;
            ConvexHullCompactnessStd = 0;
            ConvexityDefectsCoV = 0;
            VerticalCenterDistanceCoV = 0;
            HorizontalCenterDistancesCoV = 0;
        }

        public SimpleWordStat(Rectangle wordRect, IEnumerable<ERStat> erstats, Size imSize) : this()
        {
            this.imageSize = imSize;
            this.WordRect = wordRect;
            ERs = new List<ERStat>();
            foreach (ERStat er in erstats)
                ERs.Add(er);
            numberOfERs = ERs.Count;
        }

        public void Compute(List<SimpleWordErFeatures> erFeatures)
        {
            this.features = new List<SimpleWordErFeatures>(erFeatures);

            float[] FGIntensitiesMean = new float[erFeatures.Count];
            float[] OuterBoundaryIntensitiesMean = new float[erFeatures.Count];
            float[] LabDistancies = new float[erFeatures.Count];
            float[] PerimetricGradientMagnituesMean = new float[erFeatures.Count];
            float[] MeanStrokeWidths = new float[erFeatures.Count];
            float[] ConvexHullRatios = new float[erFeatures.Count];
            float[] ConvexityDefects = new float[erFeatures.Count];
            float[] VerticalCenterLocations = new float[erFeatures.Count];
            float[] HorizontalCenterLocations = new float[erFeatures.Count];

            for (int i = 0; i < erFeatures.Count; i++)
            {
                FGIntensitiesMean[i] = erFeatures[i].ForegroundIntensityMean;
                OuterBoundaryIntensitiesMean[i] = erFeatures[i].OuterBoundaryIntensityMean;
                LabDistancies[i] = erFeatures[i].ForegroundBackgroundLabDistance;
                PerimetricGradientMagnituesMean[i] = erFeatures[i].PerimetricGradientMagnitudeMean;
                MeanStrokeWidths[i] = erFeatures[i].MeanStrokeWidth;
                ConvexHullRatios[i] = erFeatures[i].ConvexHullCompactness;
                ConvexityDefects[i] = erFeatures[i].ConvexityDefects;
                VerticalCenterLocations[i] = erFeatures[i].Center.Y;
                HorizontalCenterLocations[i] = erFeatures[i].Center.X;
            }

            MeanForegroundIntensitiesCoV = Utils.ComputeCoeffOfVariation(FGIntensitiesMean);
            MeanOuterBoundaryIntensitiesCoV = Utils.ComputeCoeffOfVariation(OuterBoundaryIntensitiesMean);
            ForegroundBackgroundLabDistanceCoV = Utils.ComputeCoeffOfVariation(LabDistancies);
            PerimetricGradientMagnitudeMeanCoV = Utils.ComputeCoeffOfVariation(PerimetricGradientMagnituesMean);
            MeanStrokeWidthCoV = Utils.ComputeCoeffOfVariation(MeanStrokeWidths);

            Tuple<float, float> convexHullCompactnessMeanStd = Utils.ComputeMeanStd(ConvexHullRatios);
            ConvexHullCompactnessMean = convexHullCompactnessMeanStd.Item1;
            ConvexHullCompactnessStd = convexHullCompactnessMeanStd.Item2;
            ConvexityDefectsCoV = Utils.ComputeCoeffOfVariation(ConvexityDefects);

            VerticalCenterDistanceCoV = Utils.ComputeCoeffOfVariation(VerticalCenterLocations); // why not normalize with region height?
            HorizontalCenterDistancesCoV = Utils.ComputeCoeffOfVariation(CalcInnerDistancesChain(HorizontalCenterLocations));

        }
        public static float[] CalcInnerDistancesChain(float[] horizontalDistances)
        {
            List<float> distances = horizontalDistances.ToList();
            distances.Sort();

            float[] dists = new float[horizontalDistances.Length - 1];
            for (int i = 0; i < horizontalDistances.Length - 1; i++)
            {
                dists[i] = Math.Abs(distances[i] - distances[i + 1]);
            }
            return dists;
        }
        public static SimpleWordStat[] CreateMultiple(Rectangle[] wordRects, List<Tuple<List<ERStat>, int>> erListPerWordRect, Mat mImage, VectorOfMat channels, String imageFile)
        {
            SimpleWordStat[] wordStats = new SimpleWordStat[wordRects.Length];
            for (int i = 0; i < wordRects.Length; i++)
            {
                wordStats[i] = new SimpleWordStat(wordRects[i], erListPerWordRect[i].Item1, mImage.Size);
                SimpleWordErFeatures[] wordERFeatures = new SimpleWordErFeatures[erListPerWordRect[i].Item1.Count];
                for (int j = 0; j < erListPerWordRect[i].Item1.Count; j++)
                {
                    wordERFeatures[j] = new SimpleWordErFeatures(erListPerWordRect[i].Item1[j], channels[erListPerWordRect[i].Item2], mImage);
                }
                wordStats[i].Compute(wordERFeatures.ToList());
                wordStats[i].imageName = imageFile;
                wordStats[i].channel = erListPerWordRect[i].Item2 == 0 ? "BgrGrayscaleF" : "BgrGrayscaleB";
            }
            return wordStats;
        }

        public static List<Tuple<List<ERStat>, int>> CreateErListPerWordRect(List<ERStat>[] regions, List<List<Tuple<int, int>>> groups)
        {
            List<Tuple<List<ERStat>, int>> erListPerWordRect = new List<Tuple<List<ERStat>, int>>();
            foreach (List<Tuple<int, int>> wordERs in groups)
            {
                List<ERStat> tmp = new List<ERStat>();
                for (int i = 0; i < wordERs.Count; i++)
                {
                    tmp.Add(regions[wordERs[i].Item1][wordERs[i].Item2]);
                }
                erListPerWordRect.Add(Tuple.Create<List<ERStat>, int>(new List<ERStat>(tmp), wordERs[0].Item1));
            }
            return erListPerWordRect;
        }

        public override List<float> ToFloatList()
        {
            List<float> result = new List<float>();
            result.Add(MeanForegroundIntensitiesCoV);
            result.Add(MeanOuterBoundaryIntensitiesCoV);
            result.Add(ForegroundBackgroundLabDistanceCoV);
            result.Add(PerimetricGradientMagnitudeMeanCoV);
            result.Add(MeanStrokeWidthCoV);
            result.Add(ConvexHullCompactnessMean);
            result.Add(ConvexHullCompactnessStd);
            result.Add(ConvexityDefectsCoV);
            result.Add(VerticalCenterDistanceCoV);
            result.Add(HorizontalCenterDistancesCoV);
            return result;
        }
        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendLine("Mean FG intensities CoV: " + MeanForegroundIntensitiesCoV)
              .AppendLine("Mean Outer boundary intensities CoV: " + MeanOuterBoundaryIntensitiesCoV)
              .AppendLine("Foreground Background Lab Distance CoV: " + ForegroundBackgroundLabDistanceCoV)
              .AppendLine("Perimetric Gradient Magnitude Mean CoV: " + PerimetricGradientMagnitudeMeanCoV)
              .AppendLine("Mean Stroke Width CoV: " + MeanStrokeWidthCoV)
              .AppendLine("Convex Hull Compactness Mean: " + ConvexHullCompactnessMean)
              .AppendLine("Convex Hull Compactness STD: " + ConvexHullCompactnessStd)
              .AppendLine("Convexity Defects CoV: " + ConvexityDefectsCoV)
              .AppendLine("Vertical Center Distance CoV: " + VerticalCenterDistanceCoV)
              .AppendLine("Horizontal Center Distances CoV: " + HorizontalCenterDistancesCoV)
              .AppendLine();
            return sb.ToString();
        }

        public override string ToFlatString()
        {
            StringBuilder sb = new StringBuilder();
            int columnLength = 20;
            sb.Append(IOUtils.CreateColumn(MeanForegroundIntensitiesCoV.ToString(), columnLength, "_"));
            sb.Append(IOUtils.CreateColumn(MeanOuterBoundaryIntensitiesCoV.ToString(), columnLength, "_"));
            sb.Append(IOUtils.CreateColumn(ForegroundBackgroundLabDistanceCoV.ToString(), columnLength, "_"));
            sb.Append(IOUtils.CreateColumn(PerimetricGradientMagnitudeMeanCoV.ToString(), columnLength, "_"));
            sb.Append(IOUtils.CreateColumn(MeanStrokeWidthCoV.ToString(), columnLength, "_"));
            sb.Append(IOUtils.CreateColumn(ConvexHullCompactnessMean.ToString(), columnLength, "_"));
            sb.Append(IOUtils.CreateColumn(ConvexHullCompactnessStd.ToString(), columnLength, "_"));
            sb.Append(IOUtils.CreateColumn(VerticalCenterDistanceCoV.ToString(), columnLength, "_"));
            sb.Append(IOUtils.CreateColumn(HorizontalCenterDistancesCoV.ToString(), columnLength, "_"));
            sb.AppendLine();
            return sb.ToString();

        }

        public void Standardize(SimpleWordStat mean, SimpleWordStat std)
        {
            MeanForegroundIntensitiesCoV = MeanForegroundIntensitiesCoV.Standardize(mean.MeanForegroundIntensitiesCoV, std.MeanForegroundIntensitiesCoV);
            MeanOuterBoundaryIntensitiesCoV = MeanOuterBoundaryIntensitiesCoV.Standardize(mean.MeanOuterBoundaryIntensitiesCoV, std.MeanOuterBoundaryIntensitiesCoV);
            ForegroundBackgroundLabDistanceCoV = ForegroundBackgroundLabDistanceCoV.Standardize(mean.ForegroundBackgroundLabDistanceCoV, std.ForegroundBackgroundLabDistanceCoV);
            PerimetricGradientMagnitudeMeanCoV = PerimetricGradientMagnitudeMeanCoV.Standardize(mean.PerimetricGradientMagnitudeMeanCoV, std.PerimetricGradientMagnitudeMeanCoV);
            MeanStrokeWidthCoV = MeanStrokeWidthCoV.Standardize(mean.MeanStrokeWidthCoV, std.MeanStrokeWidthCoV);
            ConvexHullCompactnessMean = ConvexHullCompactnessMean.Standardize(mean.ConvexHullCompactnessMean, std.ConvexHullCompactnessMean);
            ConvexHullCompactnessStd = ConvexHullCompactnessStd.Standardize(mean.ConvexHullCompactnessStd, std.ConvexHullCompactnessStd);
            ConvexityDefectsCoV = ConvexityDefectsCoV.Standardize(mean.ConvexityDefectsCoV, std.ConvexityDefectsCoV);
            VerticalCenterDistanceCoV = VerticalCenterDistanceCoV.Standardize(mean.VerticalCenterDistanceCoV, std.VerticalCenterDistanceCoV);
            HorizontalCenterDistancesCoV = HorizontalCenterDistancesCoV.Standardize(mean.HorizontalCenterDistancesCoV, std.HorizontalCenterDistancesCoV);
        }

        public static SimpleWordStat FromFloatList(List<float> floatList)
        {
            if (floatList.Count != 10) throw new Exception("@SimpleWordStat:FromFloatList, wrong input floatList object or type");
            SimpleWordStat sws = new SimpleWordStat();
            sws.MeanForegroundIntensitiesCoV = floatList[0];
            sws.MeanOuterBoundaryIntensitiesCoV = floatList[1];
            sws.ForegroundBackgroundLabDistanceCoV = floatList[2];
            sws.PerimetricGradientMagnitudeMeanCoV = floatList[3];
            sws.MeanStrokeWidthCoV = floatList[4];
            sws.ConvexHullCompactnessMean = floatList[5];
            sws.ConvexHullCompactnessStd = floatList[6];
            sws.ConvexityDefectsCoV = floatList[7];
            sws.VerticalCenterDistanceCoV = floatList[8];
            sws.HorizontalCenterDistancesCoV = floatList[9];
            return sws;
        }
    }
}
