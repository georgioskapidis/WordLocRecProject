﻿using Emgu.CV;
using Emgu.CV.Structure;
using Emgu.CV.Util;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using WordLocRec.DataStructs;
using WordLocRec.Tools;

namespace WordLocRec.Grouping
{
    public class LineWordStat : BaseFeature
    {
        //public Rectangle wordRect; apisteuto! sthn anadomhsh toy antikeimenoy vrhke to wordRect sto BaseFeature!!!

        [XmlIgnore]
        public List<ERStat> ERs;
        [XmlIgnore]
        public List<WordERFeatures> feature;

        public int numberOfERs;
        public Size imageSize;

        public String imageName;
        public String channel;

        public float MeanStrokeWidthCoV { get; set; }
        public float MeanLabFGBGDistanceCoV { get; set; }
        public float LabConsistencyAmongERs { get; set; }
        public float MaxVerticalError { get; set; } // apo line estimates

        public float WidthCoV { get; set; }
        public float PercentageHorizontalOverlap { get; set; }
        public float PercentageUncovered { get; set; }

        public float AvgHuDistEucl { get; set; }

        public override List<float> ToFloatList()
        {
            List<float> feature = new List<float>();
            feature.Add(MeanStrokeWidthCoV);
            feature.Add(MeanLabFGBGDistanceCoV);
            feature.Add(LabConsistencyAmongERs);
            feature.Add(MaxVerticalError);
            feature.Add(WidthCoV);
            feature.Add(PercentageHorizontalOverlap);
            feature.Add(PercentageUncovered);
            feature.Add(AvgHuDistEucl);
            return feature;
        }
        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendLine("Mean Stroke Width CoV: " + MeanStrokeWidthCoV)
              .AppendLine("Mean Lab FGBG Distance CoV: " + MeanLabFGBGDistanceCoV)
              .AppendLine("Lab Constistency Among ERs: " + LabConsistencyAmongERs)
              .AppendLine("Max Vertical Error: " + MaxVerticalError)
              .AppendLine("Width CoV: " + WidthCoV)
              .AppendLine("Percentage Horizontal Overlap: " + PercentageHorizontalOverlap + " %")
              .AppendLine("Percentage Uncovered: " + PercentageUncovered + " %")
              .AppendLine("Avg Euclidean Hu Distance: " + AvgHuDistEucl)
              .AppendLine();
            return sb.ToString();
        }
        public override string ToFlatString()
        {
            StringBuilder sb = new StringBuilder();
            int columnLength = 20;
            sb.Append(IOUtils.CreateColumn(MeanStrokeWidthCoV.ToString(), columnLength, "_"));
            sb.Append(IOUtils.CreateColumn(MeanLabFGBGDistanceCoV.ToString(), columnLength, "_"));
            sb.Append(IOUtils.CreateColumn(LabConsistencyAmongERs.ToString(), columnLength, "_"));
            sb.Append(IOUtils.CreateColumn(MaxVerticalError.ToString(), columnLength, "_"));
            sb.Append(IOUtils.CreateColumn(WidthCoV.ToString(), columnLength, "_"));
            sb.Append(IOUtils.CreateColumn(PercentageHorizontalOverlap.ToString(), columnLength, "_"));
            sb.Append(IOUtils.CreateColumn(PercentageUncovered.ToString(), columnLength, "_"));
            sb.Append(IOUtils.CreateColumn(AvgHuDistEucl.ToString(), columnLength, "_"));
            return sb.ToString();
        }

        public LineWordStat()
        {
            wordRect = new Rectangle();
            imageSize = new Size();
            numberOfERs = 0;
            MeanStrokeWidthCoV = 0;
            MeanLabFGBGDistanceCoV = 0;
            LabConsistencyAmongERs = 0;
            MaxVerticalError = 0;
            WidthCoV = 0;
            PercentageHorizontalOverlap = 0;
            AvgHuDistEucl = 0;
        }
        public LineWordStat(Rectangle wordRect, IEnumerable<ERStat> erstats, Size imSize) : this()
        {
            this.imageSize = imSize;
            this.wordRect = wordRect;
            this.ERs = new List<ERStat>();
            foreach (ERStat er in erstats)
                ERs.Add(er);
            this.numberOfERs = ERs.Count;
        }

        public static LineWordStat FromFloatList(List<float> floatList)
        {
            LineWordStat lws = new LineWordStat();
            lws.MeanStrokeWidthCoV = floatList[0];
            lws.MeanLabFGBGDistanceCoV = floatList[1];
            lws.LabConsistencyAmongERs = floatList[2];
            lws.MaxVerticalError = floatList[3];
            lws.WidthCoV = floatList[4];
            lws.PercentageHorizontalOverlap = floatList[5];
            lws.PercentageUncovered = floatList[6];
            lws.AvgHuDistEucl = floatList[7];
            return lws;
        }

        public static LineWordStat[] CreateMultiple(Rectangle[] wordRects, ChineseGrouping.Line[] lines, List<Tuple<List<ERStat>, int>> erListPerWordRect, Mat mImage, VectorOfMat channels, String imageFile)
        {
            LineWordStat[] wordStats = new LineWordStat[wordRects.Length];
            for (int i = 0; i < wordRects.Length; i++)
            {
                wordStats[i] = new LineWordStat(wordRects[i], erListPerWordRect[i].Item1, mImage.Size);
                LineWordFeatures[] lineWordFeatures = new LineWordFeatures[erListPerWordRect[i].Item1.Count];
                for (int j = 0; j < erListPerWordRect[i].Item1.Count; j++)
                {   
                    lineWordFeatures[j] = new LineWordFeatures(erListPerWordRect[i].Item1[j], channels[erListPerWordRect[i].Item2], mImage);
                }
                wordStats[i].ComputeFeatures(lines[i], lineWordFeatures.ToList());
                wordStats[i].imageName = imageFile;
                wordStats[i].channel = erListPerWordRect[i].Item2 == 0 ? "BgrGrayscaleF" : "BgrGrayscaleB";
            }
            return wordStats;
        }

        public void Standardize(LineWordStat mean, LineWordStat std)
        {
            MeanStrokeWidthCoV = MeanStrokeWidthCoV.Standardize(mean.MeanStrokeWidthCoV, std.MeanStrokeWidthCoV);
            MeanLabFGBGDistanceCoV = MeanLabFGBGDistanceCoV.Standardize(mean.MeanLabFGBGDistanceCoV, std.MeanLabFGBGDistanceCoV);
            LabConsistencyAmongERs = LabConsistencyAmongERs.Standardize(mean.LabConsistencyAmongERs, std.LabConsistencyAmongERs);
            MaxVerticalError = MaxVerticalError.Standardize(mean.MaxVerticalError, std.MaxVerticalError);
            WidthCoV = WidthCoV.Standardize(mean.WidthCoV, std.WidthCoV);
            PercentageHorizontalOverlap = PercentageHorizontalOverlap.Standardize(mean.PercentageHorizontalOverlap, std.PercentageHorizontalOverlap);
            PercentageUncovered = PercentageUncovered.Standardize(mean.PercentageUncovered, std.PercentageUncovered);
            AvgHuDistEucl = AvgHuDistEucl.Standardize(mean.AvgHuDistEucl, std.AvgHuDistEucl);
        }

        public void ComputeFeatures(ChineseGrouping.Line line, List<LineWordFeatures> lineWordFeatures)
        {
            if (numberOfERs != lineWordFeatures.Count) throw new Exception("Gamw to kefali soy, prosexe ta inputs"); 
            float[] MeanStrokeWidths = new float[lineWordFeatures.Count];
            float[] MeanLabDistances = new float[lineWordFeatures.Count];
            MCvScalar[] LabMeans = new MCvScalar[lineWordFeatures.Count];
            float[] Widths = new float[lineWordFeatures.Count];
            double[][] huMomentss = new double[lineWordFeatures.Count][];

            for (int i = 0; i < lineWordFeatures.Count; i++)
            {
                MeanStrokeWidths[i] = lineWordFeatures[i].MeanStrokeWidth;
                MeanLabDistances[i] = lineWordFeatures[i].ForegroundBackgroundLabDistance;
                Widths[i] = lineWordFeatures[i].Width;
                LabMeans[i] = lineWordFeatures[i].meanLab;
                huMomentss[i] = lineWordFeatures[i].HuMoments;
            }
            MeanStrokeWidthCoV = Utils.ComputeCoeffOfVariation(MeanStrokeWidths);
            MeanLabFGBGDistanceCoV = Utils.ComputeCoeffOfVariation(MeanLabDistances);
            LabConsistencyAmongERs = ComputeLabConstistency(LabMeans);

            WidthCoV = Utils.ComputeCoeffOfVariation(Widths);
            MaxVerticalError = line.lineFeature;

            float percZero;
            PercentageHorizontalOverlap = ComputeOverlaps(ERs, out percZero);
            PercentageUncovered = percZero;

            AvgHuDistEucl = ComputeHuDistance(huMomentss);
        }

        private float ComputeLabConstistency(MCvScalar[] labMeans)
        {
            float labConstistency = 0; // smaller is more similar
            int matches = 0;
            for (int i = 0; i < labMeans.Length; i++)
            {
                for (int j = i + 1; j < labMeans.Length; j++)
                {
                    labConstistency += ERUtils.LabColorDistance(labMeans[i], labMeans[j]);
                    matches++;
                }
            }
            return labConstistency / matches;
        }

        private float ComputeOverlaps(List<ERStat> ers, out float percZero)
        {
            float percOverLap = 0;
            percZero = 0;
            int[] overlaps = new int[wordRect.Width];
            for (int i = 0; i < ers.Count; i++)
            {
                for (int j = 0; j < ers[i].rect.Width; j++)
                {
                    if ((ers[i].rect.X - wordRect.X) + j < overlaps.Length)
                        overlaps[(ers[i].rect.X - wordRect.X) + j]++;
                }
                    
            }
            int countZeros = 0, countOnes = 0, countOverlaps = 0;
            for (int i = 0; i < overlaps.Length; i++)
            {
                if (overlaps[i] == 0) countZeros++;
                else if (overlaps[i] == 1) countOnes++;
                else countOverlaps++;
            }
            percOverLap = (float)countOverlaps / (overlaps.Length - countZeros);
            percZero = (float)countZeros / overlaps.Length;
            return percOverLap;
        }


        private float ComputeHuDistance(double[][] humoments)
        {
            float avgSimilarity = 0f;
            int numMatches = 0;
            for (int i = 0; i < humoments.Length; i++)
            {
                for (int j = i + 1; j < humoments.Length; j++)
                {
                    avgSimilarity += (float)Utils.SimilarityDistance(humoments[i], humoments[j]);
                    //avgSimilarityWeird += (float)Utils.WeirdDistance(humoments[i], humoments[j]);
                    //avgSimilaritySo += (float)Utils.soDistance(humoments[i], humoments[j]);
                    numMatches++;
                }
            }
            return avgSimilarity / numMatches;
        }

        public static List<Tuple<List<ERStat>, int>> CreateErListPerWordRect(List<ChineseGrouping.Line[]> linesPerChannel, out List<Rectangle> wordRects, out List<ChineseGrouping.Line> allLines)
        {
            wordRects = new List<Rectangle>();
            allLines = new List<ChineseGrouping.Line>();
            List<Tuple<List<ERStat>, int>> erListPerWordRect = new List<Tuple<List<ERStat>, int>>();
            for (int i = 0; i < linesPerChannel.Count; i++)
            {
                for (int j = 0; j < linesPerChannel[i].Length; j++)
                {
                    erListPerWordRect.Add(Tuple.Create<List<ERStat>, int>(new List<ERStat>(linesPerChannel[i][j].ersOfLine), i));
                    wordRects.Add(linesPerChannel[i][j].wordRect);
                    allLines.Add(linesPerChannel[i][j]);
                }
            }
            return erListPerWordRect;
        }
    }
}
