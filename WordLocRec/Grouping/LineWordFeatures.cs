﻿using Emgu.CV;
using Emgu.CV.CvEnum;
using Emgu.CV.Structure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WordLocRec.Tools;

namespace WordLocRec.Grouping
{
    public class LineWordFeatures
    {
        public float ForegroundBackgroundLabDistance { get; private set; }
        public float MeanStrokeWidth { get; private set; }
        public float Width { get; private set; }
        public MCvScalar meanLab { get; private set; }
        public double[] HuMoments { get; private set; }


        public LineWordFeatures(ERStat er, IInputArray imageChannel, IInputArray allChannels)
        {
            using (Mat src = imageChannel.GetInputArray().GetMat())
            {
                using (Mat src3Channels = allChannels.GetInputArray().GetMat())
                using (Mat erMask = Utils.ErMaskFloodFill(src, er))
                {
                    using (Mat lab = new Mat())
                    {
                        CvInvoke.CvtColor(src3Channels, lab, ColorConversion.Bgr2Lab);
                        ForegroundBackgroundLabDistance = ERUtils.ColorDistanceERBB(src, lab, er, ERUtils.LabColorDistance);

                        // calc the mean color for the er pixels for each channel
                        meanLab = ERUtils.CalcMeanColor(lab, erMask, er.rect);
                        
                    }
                    MeanStrokeWidth = StrokeUtils.MeanStrokeWidth(erMask);
                    if (float.IsNaN(MeanStrokeWidth)) MeanStrokeWidth = 0;
                    Width = er.rect.Width;
                    //HuMoments = Utils.CalcHuMoments(erMask);
                    HuMoments = Utils.CalcImprovedHuMoments(erMask);
                }
            }
        }

    }
}
