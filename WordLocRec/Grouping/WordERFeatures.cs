﻿using Emgu.CV;
using Emgu.CV.Structure;
using Emgu.CV.Util;
using System;
using System.Drawing;
using System.Text;
using WordLocRec.SVMStuff;
using WordLocRec.Tools;

namespace WordLocRec.Grouping
{
    /// <summary>
    /// Contains the features of an Extremal Region that is part of a WordRectangle
    /// </summary>
    public class WordERFeatures
    {
        /// <summary>
        /// Mean intensity of the foreground er pixels.
        /// </summary>
        public float ForegroundIntensity { get; private set; }
        /// <summary>
        /// Mean intensity of the background er pixels.
        /// </summary>
        public float BackgroundIntensity { get; private set; }
        /// <summary>
        /// The major axis of the bb of the er.
        /// </summary>
        public float MajorAxis { get; private set; }
        /// <summary>
        /// Mean stroke width of the er.
        /// </summary>
        public float StrokeWidth { get; private set; }
        /// <summary>
        /// Mean gradient magnitude of all the pixels of the er. TODO Option 2: Use pixels of the relation (erDilate - erErode)
        /// </summary>
        public float GradientMagnitude { get; private set; }
        /// <summary>
        /// The aspect ratio of the bb of the er. Different from OpenCV.
        /// </summary>
        public float AspectRatio { get; private set; }
        /// <summary>
        /// The array of the 7 hu moment invariants.
        /// </summary>
        public double[] HuMoments { get; private set; }
        /// <summary>
        /// HullArea / ContourArea of the er pixels.
        /// </summary>
        public float ConvexHullCompactness { get; private set; }
        /// <summary>
        /// The number of Convexities found in the er.
        /// </summary>
        public float ConvexityDefects { get; private set; } // check if this is equal to inflexion points

        public WordERFeatures(ERStat er, IInputArray imageChannel, IInputArray allChannels)
        {
            using (Mat src = imageChannel.GetInputArray().GetMat())
            using (Mat src3Channels = allChannels.GetInputArray().GetMat())
            {
                using (Mat erMask = Utils.ErMaskFloodFill(src, er))
                {
                    //SaveConvexityOnMat(erMask.Clone());

                    ForegroundIntensity = (float)ERUtils.CalcMeanColor(src, erMask, er.rect).V0;
                    if (float.IsNaN(ForegroundIntensity)) ForegroundIntensity = 0;

                    using (Mat erMaskOpposite = new Mat())
                    {
                        CvInvoke.BitwiseNot(erMask, erMaskOpposite);
                        BackgroundIntensity = (float)ERUtils.CalcMeanColor(src, erMaskOpposite, er.rect).V0;
                        if (float.IsNaN(BackgroundIntensity)) BackgroundIntensity = 0;
                    }

                    MajorAxis = Math.Max(er.rect.Width, er.rect.Height);
                    if (float.IsNaN(MajorAxis)) MajorAxis = 0;

                    StrokeWidth = StrokeUtils.MeanStrokeWidth(erMask);
                    if (float.IsNaN(StrokeWidth)) StrokeWidth = 0;

                    Mat gradientMagnitude = new Mat();
                    ColorConversionWrapper.ColorConversions(src3Channels, ColorConversionWrapper.ManualConversions.Gradient, ref gradientMagnitude);
                    GradientMagnitude = (float)ERUtils.CalcMeanColor(gradientMagnitude, erMask, er.rect).V0;
                    if (float.IsNaN(GradientMagnitude)) GradientMagnitude = 0;
                    gradientMagnitude.Dispose();

                    AspectRatio = (float)er.rect.Width / er.rect.Height;
                    if (float.IsNaN(AspectRatio)) AspectRatio = 0;

                    HuMoments = Utils.CalcImprovedHuMoments(erMask);

                    using (VectorOfVectorOfPoint contours = Utils.CalcContours(erMask))
                    {
                        if (contours.Size > 0)
                        {
                            ConvexHullCompactness = Utils.CalcConvexHullAreaRatio((float)Utils.CalcConvexHullArea(contours), (float)Utils.CalcContourArea(contours));
                            if (float.IsNaN(ConvexHullCompactness)) ConvexHullCompactness = 0;
                            if (float.IsInfinity(ConvexHullCompactness)) ConvexHullCompactness = 10;

                            ConvexityDefects = Utils.CalcConvexityDefects(contours, Utils.CalcConvexHullPoints(contours));
                            if (float.IsNaN(ConvexityDefects)) ConvexityDefects = 0;
                        }
                    }
                }
            }     
        }
        private static void SaveConvexityOnMat(Mat erMask)
        {
            VectorOfVectorOfPoint contours = Utils.CalcContours(erMask.Clone());
            VectorOfInt hullIdx = new VectorOfInt();
            CvInvoke.ConvexHull(contours[0], hullIdx, true);

            VectorOfMat mats = new VectorOfMat();
            using (Mat defects = new Mat())
            {                
                if (hullIdx.Size > 2)
                    if (contours[0].Size > 3)
                    {
                        CvInvoke.ConvexityDefects(contours[0], hullIdx, defects);
                    }
                using (Matrix<int> def = new Matrix<int>(defects.Rows, defects.Cols, defects.NumberOfChannels))
                {
                    defects.CopyTo(def);
                    Image<Gray, byte> res = new Image<Gray,byte>(erMask.Cols, erMask.Rows);
                    Utils.MatToGrayImageC(erMask, out res);
                    for (int i = 0; i < def.Height; i++)
                    {
                        int startIdx = def.Data[i, 0];
                        int endIdx = def.Data[i, 1];
                        int farthestPtIdx = def.Data[i, 2];
                        double fixPtDepth = def.Data[i, 3] / 256.0;
                        if (fixPtDepth > 1)
                        {
                            Gray g = new Gray(15 * (i + 1));
                            res.Draw(new Point[] { contours[0][startIdx], contours[0][endIdx], contours[0][farthestPtIdx] }, g, 1);
                            //res.Draw(new Point[] { contours[0][farthestPtIdx] }, g, 1);
                        }

                    }
                    mats.Push(res.Mat);                  
                }
            }
            //IOUtils.AppendVectorOfByteMatToFile(mats, "C:\\Users\\George\\Desktop\\convexities.txt", erMask.Rows, erMask.Cols);
        }
        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendLine("WordERFeatures include:")
              .AppendLine("Foreground intensity mean: " + ForegroundIntensity)
              .AppendLine("Background intensity mean: " + BackgroundIntensity)
              .AppendLine("Major Axis: " + MajorAxis)
              .AppendLine("Stroke width mean: " + StrokeWidth)
              .AppendLine("Boundary gradient magnitude mean: " + GradientMagnitude)
              .AppendLine("Aspect ratio: " + AspectRatio)
              .AppendLine("7 Hu moments");
            for (int i = 0; i < HuMoments.Length; i++)
            {
                sb.AppendLine("moment " + (i + 1) + ": " + HuMoments[i]);
            }
            sb.AppendLine("Convex Hull Compactness: " + ConvexHullCompactness)
              .AppendLine("Convexity Defects: " + ConvexityDefects);

            return sb.ToString();
        }
    }
}
