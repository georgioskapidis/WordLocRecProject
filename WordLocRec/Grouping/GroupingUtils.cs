﻿using Emgu.CV;
using Emgu.CV.Text;
using Emgu.CV.Util;
using System;
using System.Linq;
using System.Collections.Generic;
using System.Drawing;
using WordLocRec.Forms;
using WordLocRec.Tools;

namespace WordLocRec.Grouping
{
    class GroupingUtils
    {
        /// <summary>
        /// Group rectangles per channel using native OpenCv methods i.e. Exhaustive Search and Karatzas
        /// </summary>
        /// <param name="orIm">The original image</param>
        /// <param name="channels">The extraction channels</param>
        /// <param name="regions">The extremal regions to be grouped</param>
        /// <param name="hasOpposites">Consider inverse channels for the grouping calculations</param>
        /// <param name="groupMethod"></param>
        /// <returns></returns>
        //native only
        public static List<Rectangle[]> RectangleGroupingPerChannel(IInputArray orIm, ref VectorOfMat channels, VectorOfERStat[] regions, bool hasOpposites, Constants.GroupingMethod groupMethod = Constants.GroupingMethod.ExhaustiveSearch)
        {
            Mat imgInput = orIm.GetInputArray().GetMat();
            using (Mat orimOpposite = new Mat())
            {
                if (hasOpposites)
                    CvInvoke.BitwiseNot(orIm, orimOpposite);

                int size = channels.Size;
                List<Rectangle[]> rects = new List<Rectangle[]>();
                for (int i = 0; i < size; i++)
                {
                    imgInput = (hasOpposites)? (i % 2 == 0) ? orIm.GetInputArray().GetMat() : orimOpposite.GetInputArray().GetMat(): orIm.GetInputArray().GetMat();
                    VectorOfMat chann = new VectorOfMat();
                    chann.Push(channels[i]);
                    VectorOfERStat[] stats = new VectorOfERStat[1];
                    stats[0] = regions[i];

                    if (groupMethod.Equals(Constants.GroupingMethod.Karatzas))
                        rects.Add(ERFilter.ERGrouping(imgInput, chann, stats, ERFilter.GroupingMethod.OrientationAny, Constants.CLASSIFIERGROUPINGPATH, 0.7f));
                    else//if (groupMethod.Equals(Constants.GroupingMethod.ExhaustiveSearch))
                        rects.Add(ERFilter.ERGrouping(imgInput, chann, stats, ERFilter.GroupingMethod.OrientationHoriz, null, 0.7f));
                    
                    chann.Dispose();
                }
                return rects;
            }
        }

        /// <summary>
        /// Group rectangles per channel using Translated OpenCV and own methods. i.e. Exhaustive Search and Line grouping
        /// </summary>
        /// <param name="orIm"></param>
        /// <param name="channels"></param>
        /// <param name="regions"></param>
        /// <param name="strictGrouping"></param>
        /// <param name="hasOpposites"></param>
        /// <param name="groupMethod"></param>
        /// <param name="feedbackRegions"></param>
        /// <returns></returns>
        // translated only
        public static List<Rectangle[]> RectangleGroupingPerChannel(IInputArray orIm, ref VectorOfMat channels, List<ERStat>[] regions, bool strictGrouping, bool hasOpposites, bool feedbackGrouping, out List<Tuple<List<ERStat>,int>> erListPerWordRect, out List<ChineseGrouping.Line[]> linesPerChannel, Constants.GroupingMethod groupMethod = Constants.GroupingMethod.ExhaustiveSearch, List<ERStat>[] feedbackRegions = null)
        {
            List<Rectangle[]> rects = new List<Rectangle[]>();
            linesPerChannel = null;
            if (groupMethod.Equals(Constants.GroupingMethod.chinese))
            {
                linesPerChannel = ChineseGroupingPerChannel(orIm, channels, regions, feedbackRegions, feedbackGrouping);
                foreach (ChineseGrouping.Line[] lines in linesPerChannel) // souloupwma
                    foreach (ChineseGrouping.Line l in lines)
                        l.wordRect = GroupingUtils.SouloupwseRects(l.wordRect, orIm.GetInputArray().GetSize().Height, orIm.GetInputArray().GetSize().Width);

                List<ChineseGrouping.Line> allLines;
                List<Rectangle> wRects;
                erListPerWordRect = LineWordStat.CreateErListPerWordRect(linesPerChannel, out wRects, out allLines);

                for (int i = 0; i < channels.Size; i++)
                {
                    rects.Add(new Rectangle[linesPerChannel[i].Length]);
                    for (int j = 0; j < linesPerChannel[i].Length; j++)
                        rects[i][j] = linesPerChannel[i][j].wordRect;
                }
            }
            else
            {
                erListPerWordRect = new List<Tuple<List<ERStat>, int>>();
                Mat imgInput = orIm.GetInputArray().GetMat();
                using (Mat orimOpposite = new Mat())
                {
                    if (hasOpposites)
                    {
                        CvInvoke.BitwiseNot(orIm, orimOpposite);
                    }
                    List<List<Tuple<int, int>>> groups;
                    List<Rectangle> rectangles;
                    for (int i = 0; i < channels.Size; i++)
                    {
                        imgInput = (hasOpposites) ? (i % 2 == 0) ? orIm.GetInputArray().GetMat() : orimOpposite.GetInputArray().GetMat() : orIm.GetInputArray().GetMat();
                        using (VectorOfMat chann = new VectorOfMat())
                        {
                            chann.Push(channels[i]);
                            List<ERStat>[] stats = new List<ERStat>[1];
                            stats[0] = new List<ERStat>(regions[i]);
                            //rects.Add(ERGroupingNM.DoHybridERGroupingNM(_originalImage.GetInputArray(), chann, stats, out groups, out rectangles, true));
                            if (strictGrouping)
                            {
                                rects.Add(ERGroupingNM.DoERGroupingNMModified(imgInput.GetInputArray(), chann, stats, out groups, out rectangles, feedbackGrouping));
                            }
                            else // normal Exhaustive Search per channel
                            {
                                Rectangle[] rs = new Rectangle[0];
                                //if (groupMethod.Equals(Constants.GroupingMethod.ExhaustiveSearch))
                                //{
                                rs = ERGroupingNM.DoERGroupingNM(imgInput.GetInputArray(), chann, stats, out groups, out rectangles, feedbackGrouping);
                                //}
                                //else if (groupMethod.Equals(Constants.GroupingMethod.Karatzas))
                                //{
                                //    ERGroupingKaratzas.erGroupingGK(imgInput.GetInputArray(), chann, stats, out groups, out rectangles, Constants.CLASSIFIERGROUPINGPATH, 0.7f);
                                //    rs = rectangles.ToArray();
                                //}
                                rects.Add(SouloupwseRects(rs, imgInput.Rows, imgInput.Cols));
                            }
                            erListPerWordRect.AddRange(WordStat.CreateErListPerWordRect(stats, groups, i));
                        }
                    }
                }    
            }
            return rects;
        }

        public static List<ChineseGrouping.Line[]> ChineseGroupingPerChannel(IInputArray inputSrc, VectorOfMat channels, List<ERStat>[] regions, List<ERStat>[] feedbackRegions, bool doFeedback = true)
        {
            List<ChineseGrouping.Line[]> linesPerChannel = new List<ChineseGrouping.Line[]>();
            Mat mImage = new Mat();
            using (Mat mImageBgr = inputSrc.GetInputArray().GetMat())
            {
                ColorConversionWrapper.ColorConversions(mImageBgr, ColorConversionWrapper.ManualConversions.Pii, ref mImage);
            }
            for (int i = 0; i < channels.Size; i++)
            {
                List<Rectangle> rects = new List<Rectangle>();
                List<ChineseGrouping.Line> lines = ChineseGrouping.DoChineseGroupings(regions[i], mImage, channels[i]);
                if (doFeedback && feedbackRegions != null) lines = ChineseGrouping.FeedBackLoop(lines, feedbackRegions[i], mImage, channels[i]);
                linesPerChannel.Add(lines.ToArray());
                //foreach (ChineseGrouping.Line l in lines)
                   // FormUtils.VisualizeErPixels(channels[i], l.ersOfLine);
            }
            mImage.Dispose();
            return linesPerChannel;
        }

        public static List<Rectangle> CombineChannelRectangles(List<ChineseGrouping.Line[]> linesPerChannel)
        {
            List<ChineseGrouping.Line> allLines = new List<ChineseGrouping.Line>();
            for (int chan = 0; chan < linesPerChannel.Count; chan++)
            {
                allLines.AddRange(linesPerChannel[chan]);
            }
            allLines.Sort(new ChineseGrouping.LineComparer());

            for (int i = 0; i < allLines.Count; i++)
            {
                for (int j = i + 1; j < allLines.Count; j++)
                {
                    if (allLines[j].Contains(allLines[i]) || allLines[j].PartiallyIncludes(allLines[i])) // if smaller is inside bigger or overlapping
                    {
                        allLines.RemoveAt(i); i--; break;
                    }
                }
            }
            List<Rectangle> rects = new List<Rectangle>();
            foreach (var l in allLines)
                rects.Add(l.wordRect);
            return rects;
        }
        public static List<Rectangle> CombineChannelRectangles(List<Rectangle[]> rects, List<List<List<ERStat>>> erListPerWordRectPerChannel) // outer list - channel, middle list - wordrect of channel - inner list - er of wordrect of channel
        {
            List<Rectangle> allWordRects = new List<Rectangle>();
            List<List<ERStat>> allERListsPerWordRects = new List<List<ERStat>>();
            for (int chan = 0; chan < rects.Count; chan++)
            {
                allWordRects.AddRange(rects[chan]);
            }
            foreach (List<List<ERStat>> v in erListPerWordRectPerChannel)
                allERListsPerWordRects.AddRange(v);

            var res = allWordRects.Zip(allERListsPerWordRects, (a, b) => new
            {
                wordrect = a,
                erlist = b
            }).OrderBy((x) => x.wordrect.Width * x.wordrect.Height);

            allWordRects = new List<Rectangle>();
            allERListsPerWordRects = new List<List<ERStat>>();
            foreach (var r in res)
            {
                allWordRects.Add(r.wordrect);
                allERListsPerWordRects.Add(r.erlist);
            }
            
            for (int i = 0; i < allWordRects.Count; i++)
            {
                for (int j = i + 1; j < allWordRects.Count; j++)
                {
                    if (allWordRects[j].Contains(allWordRects[i]) || PartiallyIncludes(allWordRects[j], allWordRects[i], allERListsPerWordRects[i]))
                    {
                        allWordRects.RemoveAt(i);
                        allERListsPerWordRects.RemoveAt(i);
                        i--;
                        break;
                    }
                }
            }
            return allWordRects;
        }
        public static bool PartiallyIncludes(Rectangle a, Rectangle b, List<ERStat> ersOfb)
        {
            Rectangle intersection = Rectangle.Intersect(a, b);
            if (intersection.Width * intersection.Height < 0.5 * b.Width * b.Height)
                return false;
            else if (intersection.Width * intersection.Height > 0.95 * b.Width * b.Height) return true;

            int counter = 0;
            foreach (ERStat er in ersOfb)
            {
                if (intersection.Contains(er.rect)) counter++;
            }
            return counter >= ersOfb.Count / 2f;
        }


        public static List<Rectangle[]> ERRectanglesPerChannel(IInputArray inputSrc, VectorOfMat channels, List<ERStat>[] regions, List<ERStat>[] feedbackRegions, bool doFeedback = true)
        {
            List<Rectangle[]> rects = new List<Rectangle[]>();
            Mat mImage = new Mat();
            using (Mat mImageBgr = inputSrc.GetInputArray().GetMat())
                ColorConversionWrapper.ColorConversions(mImageBgr, ColorConversionWrapper.ManualConversions.Pii, ref mImage);

            for (int i = 0; i < channels.Size; i++)
            {
                List<ChineseGrouping.Line> lines = ChineseGrouping.DoChineseGroupings(regions[i], mImage, channels[i]);
                if (doFeedback && feedbackRegions != null) lines = ChineseGrouping.FeedBackLoop(lines, feedbackRegions[i], mImage, channels[i]);
                    
                List<Rectangle> rs = new List<Rectangle>();
                foreach (ChineseGrouping.Line l in lines)
                {
                    foreach (ERStat er in l.ersOfLine)
                        rs.Add(er.rect);
                    //WordLocRec.Grouping.ChineseGrouping.LineEstimate le = l.LineLMS();
                    //rs.Add(new Rectangle(new Point(l.wordRect.Left, (int)le.interceptB0), new Size(l.wordRect.Width, 1)));
                    //rs.Add(new Rectangle(new Point(l.wordRect.Left, (int)le.interceptB1), new Size(l.wordRect.Width, 1)));
                    //rs.Add(new Rectangle(new Point(l.wordRect.Left, (int)le.interceptT0), new Size(l.wordRect.Width, 1)));
                    //rs.Add(new Rectangle(new Point(l.wordRect.Left, (int)le.interceptT1), new Size(l.wordRect.Width, 1)));
                    //List<LineWordFeatures> features = new List<LineWordFeatures>();
                    List<WordERFeatures> wFeatures = new List<WordERFeatures>();
                    //List<SimpleWordErFeatures> sFeatures = new List<SimpleWordErFeatures>();
                    foreach (ERStat er in l.ersOfLine)
                    {
                    //    features.Add(new LineWordFeatures(er, channels[i], mImage));
                          wFeatures.Add(new WordERFeatures(er, channels[i], mImage));
                    //    sFeatures.Add(new SimpleWordErFeatures(er, channels[i], mImage));
                    }
                    //LineWordStat lws = new LineWordStat(l.wordRect, l.ersOfLine, mImage.Size);
                    //lws.ComputeFeatures(l, features);
                    WordStat ws = new WordStat(l.wordRect, l.ersOfLine, mImage.Size);
                    ws.ComputeWordFeatures(wFeatures);
                    //SimpleWordStat sws = new SimpleWordStat(l.wordRect, l.ersOfLine, mImage.Size);
                    //sws.Compute(sFeatures);
                }

                rects.Add(rs.ToArray());
            }
            return rects;
        }

        public static Rectangle[] SouloupwseRects(Rectangle[] rects, int rows, int cols)
        {
            for (int i = 0; i < rects.Length; i++)
            {
                rects[i] = SouloupwseRects(rects[i], rows, cols);
            }
            return rects;
        }
        public static Rectangle SouloupwseRects(Rectangle rect, int rows, int cols)
        {
            if (rect.Right >= cols) rect.Width = cols - 1 - rect.X;
            if (rect.Bottom >= rows) rect.Height = rows - 1 - rect.Y;

            return rect;
        }

        public static Rectangle[] TrimRects(Rectangle[] rects, int rows, int cols, float timioPososto, ref List<int> trimmedIndices)
        {
            List<Rectangle> rrs = new List<Rectangle>();

            for (int i = 0; i < rects.Length; i++)
            {
                if (rects[i].Width * rects[i].Height > timioPososto * rows * cols)
                {
                    rrs.Add(rects[i]);
                }
                else { trimmedIndices.Add(i); }
            }

            return rrs.ToArray();
        }
        public static Rectangle TrimRectsToCorrect(Rectangle[] rects, Rectangle wanted, out int correctIndice)
        {
            correctIndice = -1;
            for (int i = 0; i < rects.Length; i++)
            {
                if (rects[i].Equals(wanted))
                {
                    correctIndice = i;
                    return wanted;
                }
            }
            return new Rectangle(0,0,0,0);
        }
        
        /// <summary>
        /// Creates a rectangle per Extremal Region and packs them in a List of Rectangle Arrays.
        /// In each Array the Rectangles of the Extremal Regions of one channel are packed.
        /// Used for Ungrouped viewing
        /// </summary>
        /// <param name="regions">List of Translated ERStat objects</param>
        /// <returns></returns>
        public static List<Rectangle[]> ERStatsToRects(List<ERStat>[] regions)
        {
            List<Rectangle[]> rects = new List<Rectangle[]>();
            for (int i = 0; i < regions.Length; i++)
            {
                rects.Add(new Rectangle[regions[i].Count]);
                for (int j = 0; j < regions[i].Count; j++)
                {
                    rects[i][j] = regions[i][j].rect;
                }
            }
            return rects;
        }
        
        /// <summary>
        /// Creates a rectangle per Extremal Region and packs them in a List of Rectangle Arrays.
        /// In each Array the Rectangles of the Extremal Regions of one channel are packed.
        /// Used for Ungrouped viewing (used for native methods)
        /// </summary>
        /// <param name="regions">VectorOfERStat objects from EmguCV</param>
        /// <returns></returns>
        public static List<Rectangle[]> ERStatsToRects(VectorOfERStat[] regions)
        {
            List<Rectangle[]> rects = new List<Rectangle[]>();
            for (int i = 0; i < regions.Length; i++)
            {
                rects.Add(new Rectangle[regions[i].Size]);
                for (int j = 0; j < regions[i].Size; j++)
                {
                    rects[i][j] = regions[i][j].Rect;
                }
            }
            return rects;
        }

        /// <summary>
        /// Pack all the er rectangles to a single List
        /// </summary>
        /// <param name="regions"></param>
        /// <returns></returns>
        public static List<Rectangle> ERStatsToRecties(VectorOfERStat[] regions)
        {
            List<Rectangle> recties = new List<Rectangle>();
            List<Rectangle[]> rects = ERStatsToRects(regions);
            foreach (Rectangle[] rr in rects)
                recties.AddRange(rr);
            //for (int i = 0; i < regions.Length; i++)
            //    for (int j = 0; j < regions[i].Size; j++)
            //        recties.Add(regions[i][j].Rect);

            return recties;
        }
        public static List<Rectangle> ERStatsToRecties(List<ERStat>[] regions)
        {
            List<Rectangle> recties = new List<Rectangle>();
            List<Rectangle[]> rects = ERStatsToRects(regions);
            foreach (Rectangle[] rr in rects)
                recties.AddRange(rr);

            return recties;
        }

        /// <summary>
        /// Inverse conversion, from the Translated to the Native OpenCv objects.
        /// <para />
        /// To be used for the Karatza grouping when the Extraction method is Translated.
        /// </summary>
        /// <param name="vec"></param>
        /// <param name="regions"></param>
        public static void ArrayListErstatsToArrVector(ref VectorOfERStat[] vec, List<ERStat>[] regions)
        {
            for (int i = 0; i < regions.Length; i++)
            {
                VectorOfERStat cc = new VectorOfERStat();
                vec[i] = new VectorOfERStat();
                MCvERStat[] mc = new MCvERStat[regions[i].Count];
                for (int j = 0; j < regions[i].Count; j++)
                {
                    MCvERStat mc0 = new MCvERStat();
                    ERStat.ERtoMCvERStat(ref mc0, regions[i][j]);
                    mc[j] = mc0;
                }
                
                cc.Push(mc);
                vec[i].Push(cc.ToArray());
                
            }
        }
        public static void ERStatsToVectorOfMCvERstat(ref VectorOfERStat vec, List<ERStat> regions)
        {
            MCvERStat[] mc = new MCvERStat[regions.Count];
            for (int i = 0; i < regions.Count; i++)
            {
                ERStat.ERtoMCvERStat(ref mc[i], regions[i]);
            }
            vec.Push(mc);
        }

        #region 2ndStageGrouping

        public static byte[,] CreateHeatMap(Rectangle[] rects, int rows, int cols)
        {
            byte[,] heatMap = new byte[rows, cols];
            foreach (Rectangle r in rects)
            {
                for (int y = r.Y; y < r.Bottom; y++)
                    for (int x = r.X; x < r.Right; x++)
                        heatMap[y, x]++;
            }
            return heatMap;
        }

        public static Rectangle[] UniteRects(Rectangle[] rects, byte[,] uhm, int rows, int cols)
        {
            byte[,] heatMap = CreateHeatMap(rects, rows, cols);

            List<Rectangle> goodRectangles = new List<Rectangle>();
            bool deleted;
            foreach (Rectangle r in rects)
            {
                deleted = true;
                int area = 0; // area not covered by any other rectangle
                for (int y = r.Y; y < r.Bottom; y++) //polla mporw na ta kanw me linq gia taxuthta
                {
                    for (int x = r.X; x < r.Right; x++)
                    {
                        if (heatMap[y, x] == 1)
                        {
                            area++;
                        }
                    }
                }
                if ((float)area / (r.Height * r.Width) > 0.01) deleted = false; // pososto monadikou area gia to rect poy elegxetai
                if (deleted)
                {
                    for (int y = r.Y; y < r.Bottom; y++)
                        for (int x = r.X; x < r.Right; x++)
                            heatMap[y, x]--;
                }
                else
                    goodRectangles.Add(r);
            }
            List<float> scores = new List<float>();
            for (int i = 0; i < goodRectangles.Count; i++)
            {
                scores.Add(new float());
                for (int y = goodRectangles[i].Y; y < goodRectangles[i].Bottom; y++)
                {
                    for (int x = goodRectangles[i].X; x < goodRectangles[i].Right; x++)
                    {
                        scores[i] += uhm[y, x];
                    }
                }
                scores[i] /= goodRectangles[i].Height * goodRectangles[i].Width;
            }

            for (int i = 0; i < goodRectangles.Count; i++)
            {
                for (int j = 0; j < goodRectangles.Count; j++)
                {
                    if (j == i) continue;
                    if (goodRectangles[i].IntersectsWith(goodRectangles[j]))
                    {
                        Rectangle intersection = Rectangle.Intersect(goodRectangles[i], goodRectangles[j]);
                        int areaI, areaJ, areaInter;
                        areaI = goodRectangles[i].Width * goodRectangles[i].Height;
                        areaJ = goodRectangles[j].Width * goodRectangles[j].Height;
                        areaInter = intersection.Width * intersection.Height;
                        float interScore = 0f;
                        for (int y = intersection.Y; y < intersection.Bottom; y++)
                        {
                            for (int x = intersection.X; x < intersection.Right; x++)
                            {
                                interScore += uhm[y, x];
                            }
                        }
                        interScore /= intersection.Height * intersection.Width;
                        if ((float)areaInter / areaJ >= 0.7)
                        {
                            if (interScore / scores[j] > 0.8)
                            {
                                goodRectangles.RemoveAt(j);
                                scores.RemoveAt(j);
                                if (j < i) i--;
                                j--;

                                continue;
                            }
                            else
                            {
                                Rectangle union = Rectangle.Union(goodRectangles[i], goodRectangles[j]);
                                goodRectangles.RemoveAt(i);
                                goodRectangles.Insert(i, union);
                                goodRectangles.RemoveAt(j);

                                float oldScoreI = scores[i];
                                float oldScoreJ = scores[j];
                                scores.RemoveAt(i);
                                scores.Insert(i, oldScoreI + (oldScoreJ - interScore));
                                scores.RemoveAt(j);

                                //i--;
                                j = -1;
                            }
                        }

                    }
                }
            }

            return goodRectangles.ToArray();
        }

        #endregion

        public static Rectangle[] UpscaleRectangles(Rectangle[] rects, double scale, int times, Size bounds)
        {
            for (int t = 0; t < times; t++)
            {
                for (int i = 0; i < rects.Length; i++)
                {
                    rects[i] = UpscaleRectangle(rects[i], scale);
                }
            }
            return SouloupwseRects(rects, bounds.Height, bounds.Width);
        }
        public static Rectangle UpscaleRectangle(Rectangle rect, double scale)
        {
            rect.X = (int)(rect.X * scale);
            rect.Y = (int)(rect.Y * scale);
            rect.Height = (int)(rect.Height * scale);
            rect.Width = (int)(rect.Width * scale);
            return rect;
        }
    }
}
