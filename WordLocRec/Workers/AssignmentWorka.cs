﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using WordLocRec.DataStructs;

namespace WordLocRec.Workers
{
    class AssignmentWorka
    {
        AutoResetEvent _resetEvent = new AutoResetEvent(false);
        BackgroundWorker[] slaves;

        bool lastEvent = false;
        string cl1, cl2, cl3;

        String outputFolder;

        private int totalFilesCount;
        private int type;
        private WorkerObject wo;
        List<string> filesToBeReprocessed;
        public BackgroundWorker CreateAssignmentWorker(String cl1, String cl2, String cl3, String output, WorkerObject wo, int type)
        {
            this.cl1 = cl1;
            this.cl2 = cl2;
            this.cl3 = cl3;
            this.outputFolder = output;
            this.type = type;
            this.wo = wo;

            BackgroundWorker assignmentWorker = new BackgroundWorker();
            assignmentWorker.DoWork += AssignmentWorkerDoWork;
            assignmentWorker.WorkerReportsProgress = true;
            assignmentWorker.WorkerSupportsCancellation = true;
            
            return assignmentWorker;
        }
        private void AssignmentWorkerDoWork(object sender, DoWorkEventArgs e)
        {
            int threadsAllowed = Constants.MAX_EMGU_THREADS;
            BackgroundWorker master = sender as BackgroundWorker;

            String[] filenames = (String[])e.Argument;
            filesToBeReprocessed = new List<string>();

            assignmentInnerDoWork(threadsAllowed, filenames, ref master, ref e);

            if (filesToBeReprocessed.Count > 0)
            {
                assignmentInnerDoWork(1, filesToBeReprocessed.ToArray(), ref master, ref e);
            }
        }
        private void AssignmentWorkerDoWorkOld(object sender, DoWorkEventArgs e)
        {
            int threadsAllowed = Constants.MAX_EMGU_THREADS;
            BackgroundWorker master = sender as BackgroundWorker;
            slaves = new BackgroundWorker[threadsAllowed];
            String[] filenames = (String[])e.Argument;
            filesToBeReprocessed = new List<string>();


            for (int i = 0; i < threadsAllowed; i++)
            {
                slaves[i] = new GenericExtractorWorker().InitializeSlave(slaves[i], cl1, cl2, cl3, outputFolder, type);
                slaves[i].RunWorkerCompleted += AssignmentWorka_RunWorkerCompleted;
            }

            totalFilesCount = filenames.Length;
            bool processed;
            int filesProcessed = 0;
            while (filesProcessed < totalFilesCount)
            {
                if (master.CancellationPending)
                {
                    e.Cancel = true;
                    foreach (var s in slaves)
                        if (s.IsBusy) _resetEvent.WaitOne();
                    return;
                }
                processed = false;
                while (!processed)
                {
                    for (int i = 0; i < threadsAllowed; i++)
                    {
                        if (!slaves[i].IsBusy)
                        {
                            slaves[i].RunWorkerAsync(new WorkerObject(wo.props, filenames[filesProcessed], wo.colorspace, wo.channels, wo.gradients, wo.fun, wo.f1, wo.f2, wo.wordFeatureType, wo.strictNMS, wo.combineChannels, wo.gammaCorrection, wo.smoothing, wo.grouping, wo.gamma, wo.smoothX, wo.smoothY));
                            
                            filesProcessed++;
                            processed = true;
                            master.ReportProgress((100 * filesProcessed) / totalFilesCount, "Extracting...");
                            break;
                        }
                    }
                    if (!processed)
                    {
                        _resetEvent.WaitOne();
                        break;
                    }
                }
            }
            lastEvent = true;
            _resetEvent.WaitOne();
        }

        private void assignmentInnerDoWork(int threadsAllowed, String[] filenames, ref BackgroundWorker master, ref DoWorkEventArgs e)
        {
            slaves = new BackgroundWorker[threadsAllowed];
            for (int i = 0; i < threadsAllowed; i++)
            {
                slaves[i] = new GenericExtractorWorker().InitializeSlave(slaves[i], cl1, cl2, cl3, outputFolder, type);
                slaves[i].RunWorkerCompleted += AssignmentWorka_RunWorkerCompleted;
            }

            totalFilesCount = filenames.Length;
            bool processed;
            int filesProcessed = 0;

            while (filesProcessed < totalFilesCount)
            {
                processed = false;
                while (!processed)
                {
                    if (master.CancellationPending)
                    {
                        e.Cancel = true;
                        foreach (var s in slaves)
                            if (s.IsBusy) _resetEvent.WaitOne();
                        return;
                    }
                    for (int i = 0; i < threadsAllowed; i++)
                    {
                        if (!slaves[i].IsBusy)
                        {
                            slaves[i].RunWorkerAsync(new WorkerObject(wo.props, filenames[filesProcessed], wo.channelProjections, wo.fun, wo.f1, wo.f2, wo.wordFeatureType, wo.strictNMS, wo.combineChannels, wo.gammaCorrection, wo.smoothing, wo.grouping, wo.gamma, wo.smoothX, wo.smoothY));

                            filesProcessed++;
                            processed = true;
                            master.ReportProgress((100 * filesProcessed) / totalFilesCount, "Extracting...");
                            break;
                        }
                    }
                    if (!processed)
                    {
                        _resetEvent.WaitOne();
                        break;
                    }
                }
            }
            lastEvent = true;
            _resetEvent.WaitOne();

        }

        private void AssignmentWorka_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                filesToBeReprocessed.Add((String)e.Error.Message);
            }

            if (!lastEvent)
                _resetEvent.Set();
            else
            {
                foreach (var s in slaves)
                {
                    if (s.IsBusy) return;
                }
                _resetEvent.Set();
            }
        }
    }
}
