﻿using Emgu.CV;
using Emgu.CV.Structure;
using Emgu.CV.Text;
using Emgu.CV.Util;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WordLocRec.DataStructs;
using WordLocRec.Tools;

namespace WordLocRec.Workers
{
    class NMCannnyWorker
    {
        public String classifier1Path, classifier2Path;
        public string tryName;
        public BackgroundWorker InitializeSlave(BackgroundWorker worker, String cl1, String cl2, string tryName)
        {
            classifier1Path = cl1;
            classifier2Path = cl2;
            this.tryName = tryName;
            worker = new BackgroundWorker();
            worker.WorkerSupportsCancellation = false;
            worker.WorkerReportsProgress = false;
            worker.DoWork += NMWorkerDoWork;
            return worker;
        }

        private void NMWorkerDoWork(object sender, DoWorkEventArgs e)
        {
            BackgroundWorker slave = sender as BackgroundWorker;

            WorkerObject wObj = (WorkerObject)e.Argument;

            String filename = wObj.filename;
            ERFilterProps props = wObj.props;

            Image<Bgr, Byte> mImage = new Image<Bgr, byte>(filename);

            // done extracting channels

            ERFilterNM1 er1 = new ERFilterNM1(classifier1Path, props.thresholdDelta, props.minArea, props.maxArea, props.minProbability, props.nonMaxSuppression, props.minProbabilityDifference);
            ERFilterNM2 er2 = new ERFilterNM2(classifier2Path);

            Image<Gray, byte> edges = mImage.Canny(150, 150);
            VectorOfMat channels = new VectorOfMat();
                //Utils.CreateXXChannels(mImage.GetInputArray().GetMat(), wObj.colorspace, wObj.channels, false);
            channels.Push(CvInvoke.CvArrToMat(edges));
            Mat mat = new Mat();
            CvInvoke.BitwiseNot(channels[channels.Size - 1], mat);
            channels.Push(mat);

            VectorOfERStat[] regions = new VectorOfERStat[channels.Size];
            for (int i = 0; i < channels.Size; i++)
            {
                regions[i] = new VectorOfERStat();
                er1.Run(channels[i], regions[i]);
                er2.Run(channels[i], regions[i]);
            }
            Rectangle[] rects = new Rectangle[0];
            try
            {
                rects = ERFilter.ERGrouping(mImage.Clone(), channels, regions, ERFilter.GroupingMethod.OrientationHoriz, null, 0.7f);
                for (int i = 0; i < rects.Length; i++)
                {
                    if (rects[i].Right >= mImage.Cols) rects[i].Width = mImage.Cols - 1 - rects[i].X;
                    if (rects[i].Bottom >= mImage.Rows) rects[i].Height = mImage.Rows - 1 - rects[i].Y;
                }
            }
            catch (CvException cve)
            {
                Debug.WriteLine("");
                Debug.WriteLine(cve.ToString());
                Debug.WriteLine("@ image: " + filename);
                Debug.WriteLine("");
            }
            finally
            {
                IOUtils.CreateResultsFile(filename, rects, tryName);
            }
        }
    }
}
