﻿using Emgu.CV;
using Emgu.CV.CvEnum;
using Emgu.CV.Text;
using Emgu.CV.Util;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using WordLocRec.DataStructs;
using WordLocRec.Grouping;
using WordLocRec.Tools;

namespace WordLocRec.Workers
{
    /// <summary>
    /// CER worker is using Translated ER extraction and Grouping.
    /// Main addition to NMTranslatedWorker is the use of the CERFilter class
    /// that supports CER pruning and (eventually) Color Enhancement.
    /// Optional addition is 2ndStage Grouping as always.
    /// </summary>
    class NMCERWorker:BackgroundWorker
    {
        public String classifier1Path, classifier2Path;
        public String tryName;
        private AssignmentWorker aw;

        public BackgroundWorker InitializeSlave(BackgroundWorker worker, String cl1, String cl2, string tryName, AssignmentWorker aw)
        {
            this.aw = aw;
            classifier1Path = cl1;
            classifier2Path = cl2;
            this.tryName = tryName;
            worker = new BackgroundWorker();
            worker.WorkerSupportsCancellation = false;
            worker.WorkerReportsProgress = false;
            worker.DoWork += NMWorkerDoWork;
            worker.RunWorkerCompleted += NMWorkerCompleted;
            return worker;
        }
        private void NMWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            this.Dispose(true);
            //nm.AnotherOneBitesTheDust(); //worker is done
            aw.dustBiting(this);
        }

        private void NMWorkerDoWork(object sender, DoWorkEventArgs e)
        {
            BackgroundWorker slave = sender as BackgroundWorker;

            WorkerObject wObj = (WorkerObject)e.Argument;

            String filename = wObj.filename;
            ERFilterProps props = wObj.props;
            Constants.ColorSpace colorspace = wObj.colorspace;
            Constants.VisChannels channelsFlag = wObj.channels;
            Constants.Gradients flagGradient = wObj.gradients;
            Constants.GroupingMethod flagGrouping = wObj.grouping;
            bool AllowSmoothing = wObj.smoothing;

            Mat mImage = new Mat(filename, LoadImageType.Unchanged);

            CERFilter filter1 = CERFilter.createERFilterNM1(CERFilter.loadClassifierNM1(classifier1Path),
                                                                 props.thresholdDelta,
                                                                 props.minArea,
                                                                 props.maxArea,
                                                                 props.minProbability,
                                                                 props.nonMaxSuppression,
                                                                 props.minProbabilityDifference);
            CERFilter filter2 = CERFilter.createERFilterNM2(CERFilter.loadClassifierNM2(classifier2Path), 0.5f);

            if (AllowSmoothing)
            {
                ImageProcessingUtils.FilterOneLine(ref mImage, 5, 5, ImageProcessingUtils.FilterWienerC);
            }

            VectorOfMat channels = new VectorOfMat();
            Utils.CreateXXChannels(ref channels, mImage, colorspace, channelsFlag, false, flagGradient);
            
            List<ERStat>[] regions = new List<ERStat>[channels.Size];
            Rectangle[] rects = new Rectangle[0], rects1 = new Rectangle[0];
            try
            {
                for (int i = 0; i < channels.Size; i++)
                {
                    regions[i] = new List<ERStat>();
                    filter1.Run(channels[i].GetInputArray(), ref regions[i]);
                    filter2.Run(channels[i].GetInputArray(), ref regions[i]);
                }

                VectorOfERStat[] stats = new VectorOfERStat[regions.Length];
                GroupingUtils.ArrayListErstatsToArrVector(ref stats, regions);

                if (flagGrouping.HasFlag(Constants.GroupingMethod.ExhaustiveSearch))
                    rects = ERFilter.ERGrouping(mImage, channels, stats, ERFilter.GroupingMethod.OrientationHoriz);
                else
                    rects = ERFilter.ERGrouping(mImage, channels, stats, ERFilter.GroupingMethod.OrientationAny, Constants.CLASSIFIERGROUPINGPATH, 0.5f);

                GroupingUtils.SouloupwseRects(rects, mImage.Rows, mImage.Cols);

                //****2nd stage grouping****//
                //List<Rectangle> ungroupedRectsList = new List<Rectangle>();
                //for (int i = 0; i < channels.Size; i++)
                //{
                //    for (int j = 0; j < regions[i].Count; j++)
                //    {
                //        ungroupedRectsList.Add(regions[i][j].rect);
                //    }
                //}
                //Rectangle[] ungroupedRects = GroupingUtils.SouloupwseRects(ungroupedRectsList.ToArray(), mImage.Rows, mImage.Cols);
                //byte[,] ungroupedHeatMap = GroupingUtils.CreateHeatMap(ungroupedRects.ToArray(), mImage.Rows, mImage.Cols);
                //rects1 = GroupingUtils.UniteRects(rects, ungroupedHeatMap, mImage.Rows, mImage.Cols);

                IOUtils.CreateResultsFile(filename, rects, tryName);
            }
            catch (Exception cve)
            {
                Debug.WriteLine(cve.ToString());
                Debug.WriteLine("@ image: " + filename);
                Debug.WriteLine("");
                return;
            }
            finally
            {
                mImage = null;
                channels = null;
                filter1 = null;
                filter2 = null;
                //GC.Collect();
            }
        }
    }
}
