﻿using Emgu.CV;
using Emgu.CV.CvEnum;
using Emgu.CV.Util;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using WordLocRec.DataStructs;
using WordLocRec.Grouping;
using WordLocRec.Tools;

namespace WordLocRec.Workers
{
    class ERWorker:BackgroundWorker
    {
        public String tryName, classifierPath, classifier2Path;
        private AssignmentWorker aw;

        public BackgroundWorker InitializeSlave(BackgroundWorker w, String classifier, String classifier2, String tryName, AssignmentWorker aw)
        {
            classifierPath = classifier;
            classifier2Path = classifier2;
            this.tryName = tryName;
            this.aw = aw;

            w = new BackgroundWorker();
            w.WorkerSupportsCancellation = false;
            w.WorkerReportsProgress = false;
            w.DoWork += WorkerDoWork;
            w.RunWorkerCompleted += WorkerCompleted;

            return w;
        }

        private void WorkerDoWork(object sender, DoWorkEventArgs e)
        {
            WorkerObject wObj = (WorkerObject)e.Argument;

            String filename = wObj.filename;
            //ERFilterProps props = wObj.props;
            Constants.ColorSpace colorspace = wObj.colorspace;
            Constants.VisChannels channelsFlag = wObj.channels;
            Constants.Gradients flagGradient = wObj.gradients;
            Constants.GroupingMethod groupMethod = wObj.grouping;
            bool ApplySmoothing = wObj.smoothing;
            bool ApplyGamma = wObj.gammaCorrection;
            bool OppositesIncluded = wObj.channels.HasFlag(Constants.VisChannels.Channel1Opp) || wObj.channels.HasFlag(Constants.VisChannels.Channel2Opp) || wObj.channels.HasFlag(Constants.VisChannels.Channel3Opp) || wObj.channels.HasFlag(Constants.VisChannels.Channel4Opp);
            bool oppositeChannel = false;

            Mat m = new Mat(filename, LoadImageType.Unchanged);

            VectorOfMat channels = new VectorOfMat();
            if (ApplyGamma)
            {
                ImageProcessingUtils.CorrectionOneLine(ref m, 2.2d);
            }
            if (ApplySmoothing)
            {
                ImageProcessingUtils.FilterOneLine(ref m, 5, 5, ImageProcessingUtils.FilterWienerC);
            }
            Utils.CreateXXChannels(ref channels, m, colorspace, channelsFlag, false, flagGradient);

            List<ERStat>[] regions = new List<ERStat>[channels.Size];
            Rectangle[] rects = new Rectangle[0], rects1 = new Rectangle[0];


            try
            {
                ERExtractor.ExtractorParameters ep = new ERExtractor.ExtractorParameters(1, 0.00005, 0.5, 0.5, 0, m.Cols, m.Rows);
                ERExtractor ere = new ERExtractor(ep, ERUtils.SizeRestrictionsArea);

                for (int i = 0; i < channels.Size; i++)
                {
                    regions[i] = ere.extractERs(channels[i], false, false);
                    regions[i] = RecursiveFunctions.NonCerSuppression(regions[i]);
                    regions[i] = RecursiveFunctions.NearDuplicateSuppression(regions[i], true);

                    if (OppositesIncluded)
                    {
                        oppositeChannel = i % 2 == 1 ? true : false;// the odd numbered channels are the opposites
                    }

                    regions[i] = ERUtils.TwoClassSvmPruning(channels[i], m, regions[i], classifierPath, oppositeChannel, Constants.FeatureType.ColorDistLab | Constants.FeatureType.ColorDistPii | Constants.FeatureType.StandarizedPerFeature);
                    regions[i] = RecursiveFunctions.SuppressWrong(regions[i], RecursiveFunctions.erProbabilityPruning, 0.2f);

                    regions[i] = ERUtils.TwoClassSvmPruning(channels[i], m, regions[i], classifier2Path, oppositeChannel, Constants.FeatureType.NM);
                    regions[i] = RecursiveFunctions.SuppressWrong(regions[i], RecursiveFunctions.erResponsePruning);
                    
                }

                List<List<Tuple<int, int>>> groups;
                List<Rectangle> rectangles;
                rects = ERGroupingNM.DoERGroupingNM(m.GetInputArray(), channels, regions, out groups, out rectangles, true);

                GroupingUtils.SouloupwseRects(rects, m.Rows, m.Cols);

                IOUtils.CreateResultsFile(filename, rects, tryName);
            }
            catch (Exception cve)
            {
                Debug.WriteLine(cve.ToString());
                Debug.WriteLine("@ image: " + filename);
                Debug.WriteLine("");
                return;
            }
            finally
            {
                m.Bitmap.Dispose();
                m.Dispose();
                for (int i = 0; i < channels.Size; i++)
                {
                    channels[i].Bitmap.Dispose();
                    channels[i].Dispose();
                }
                channels.Dispose();
                GC.Collect();
            }
            
        }

        private void WorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            this.Dispose(true);
            //nm.AnotherOneBitesTheDust(); //worker is done
            aw.dustBiting(this);
        }
    }
}
