﻿using Emgu.CV;
using Emgu.CV.CvEnum;
using Emgu.CV.Structure;
using Emgu.CV.Text;
using Emgu.CV.Util;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WordLocRec.DataStructs;
using WordLocRec.Grouping;
using WordLocRec.Tools;

namespace WordLocRec.Workers
{
    /// <summary>
    /// Hybrid worker uses native extraction of ER's
    /// and Translated Grouping.
    /// Optionally add 2nd stage grouping
    /// Used to run quick tests for grouping methods and additions.
    /// </summary>
    class NMHybridWorker:BackgroundWorker
    {
        public String classifier1Path, classifier2Path;
        public String tryName;
        private AssignmentWorker aw;

        public BackgroundWorker InitializeSlave(BackgroundWorker worker, String cl1, String cl2, String tryName, AssignmentWorker aw)
        {
            this.aw = aw;
            classifier1Path = cl1;
            classifier2Path = cl2;
            this.tryName = tryName;
            worker = new BackgroundWorker();
            worker.WorkerSupportsCancellation = false;
            worker.WorkerReportsProgress = false;
            worker.DoWork += NMWorkerDoWork;
            worker.RunWorkerCompleted += NMWorkerCompleted;
            return worker;
        }

        private void NMWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            this.Dispose(true);
            //nm.AnotherOneBitesTheDust(); //worker is done
            aw.dustBiting(this);
        }

        private void NMWorkerDoWork(object sender, DoWorkEventArgs e)
        {
            BackgroundWorker slave = sender as BackgroundWorker;

            WorkerObject wObj = (WorkerObject)e.Argument;

            String filename = wObj.filename;
            ERFilterProps props = wObj.props;
            Constants.ColorSpace colorspace = wObj.colorspace;
            Constants.VisChannels channelsFlag = wObj.channels;
            Constants.Gradients flagGradient = wObj.gradients;
            bool ApplySmoothing = wObj.smoothing;

            Mat mImage = new Mat(filename, LoadImageType.Unchanged);

            ERFilterNM1 er1 = new ERFilterNM1(classifier1Path, props.thresholdDelta, props.minArea, props.maxArea, props.minProbability, props.nonMaxSuppression, props.minProbabilityDifference);
            ERFilterNM2 er2 = new ERFilterNM2(classifier2Path, 0.5f);

            //VectorOfMat channels = Utils.CombineHueChannels(mImage.GetInputArray().GetMat());
            VectorOfMat channels = new VectorOfMat();

            try
            {
                if (ApplySmoothing)
                {
                    ImageProcessingUtils.FilterOneLine(ref mImage, 5, 5, ImageProcessingUtils.FilterWienerC);
                }

                Utils.CreateXXChannels(ref channels, mImage, colorspace, channelsFlag, false, flagGradient);

                VectorOfERStat[] regions = new VectorOfERStat[channels.Size];
                for (int i = 0; i < channels.Size; i++)
                {
                    regions[i] = new VectorOfERStat();
                    er1.Run(channels[i], regions[i]);
                    er2.Run(channels[i], regions[i]);
                }

                // Transform MCvERstat to ERstat for Translated Grouping
                List<ERStat>[] mRegions = new List<ERStat>[channels.Size];
                List<Rectangle> ungroupedRectsList = new List<Rectangle>(); //2nd stage
                for (int i = 0; i < channels.Size; i++)
                {
                    mRegions[i] = new List<ERStat>();
                    for (int j = 0; j < regions[i].Size; j++)
                    {
                        mRegions[i].Add(ERStat.TransformMCvERStat(regions[i][j]));
                        ungroupedRectsList.Add(regions[i][j].Rect); //2nd stage
                    }
                }

                Rectangle[] rects = new Rectangle[0], rects1 = new Rectangle[0];
                List<List<Tuple<int, int>>> groups;
                List<Rectangle> rectangles;

                //rects = ERFilter.ERGrouping(mImage.Clone(), channels, regions, ERFilter.GroupingMethod.OrientationHoriz, null, 0.7f);
                //rects = ERGroupingNM.DoMultiChannelERGNM(mImage.GetInputArray(), channels, mRegions, out groups, out rectangles, true);

                rects = ERGroupingNM.DoERGroupingNM(mImage.GetInputArray(), channels, mRegions, out groups, out rectangles, true);
                rects = GroupingUtils.SouloupwseRects(rects, mImage.Rows, mImage.Cols);
                
                //****2nd stage grouping****//
                Rectangle[] ungroupedRects = GroupingUtils.SouloupwseRects(ungroupedRectsList.ToArray(), mImage.Rows, mImage.Cols);
                byte[,] ungroupedHeatMap = GroupingUtils.CreateHeatMap(ungroupedRects.ToArray(), mImage.Rows, mImage.Cols);
                rects1 = GroupingUtils.UniteRects(rects, ungroupedHeatMap, mImage.Rows, mImage.Cols);


                //IOUtils.CreateResultsFile(filename, rects, tryName);
                IOUtils.CreateResultsFile(filename, rects1, tryName); //2nd stage

            }                    
            catch (Exception cve)
            {
                Debug.WriteLine("");
                Debug.WriteLine(cve.ToString());
                Debug.WriteLine("@ image: " + filename);
                Debug.WriteLine("");
            }
            finally
            {
                mImage.Dispose();
                er1.Dispose();
                er2.Dispose();
                for (int i = 0; i < channels.Size; i++)
                    channels[i].Dispose();
                channels.Dispose();
            }
        }
    }
}
