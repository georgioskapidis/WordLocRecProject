﻿using Emgu.CV;
using Emgu.CV.Structure;
using Emgu.CV.Util;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WordLocRec.DataStructs;
using WordLocRec.Grouping;
using WordLocRec.Tools;

namespace WordLocRec.Workers
{
    class NMTranslatedWorker
    {
        public String classifier1Path, classifier2Path;
        public String tryName;
        private MainForm nm;

        public BackgroundWorker InitializeSlave(BackgroundWorker worker, String cl1, String cl2, string tryName, MainForm nm)
        {
            this.nm = nm;
            classifier1Path = cl1;
            classifier2Path = cl2;
            this.tryName = tryName;
            worker = new BackgroundWorker();
            worker.WorkerSupportsCancellation = false;
            worker.WorkerReportsProgress = false;
            worker.DoWork += NMWorkerDoWork;
            worker.RunWorkerCompleted += NMWorkerCompleted;
            return worker;
        }
        private void NMWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            GC.Collect();
            //nm.AnotherOneBitesTheDust(); //worker is done
        }

        private void NMWorkerDoWork(object sender, DoWorkEventArgs e)
        {
            BackgroundWorker slave = sender as BackgroundWorker;

            WorkerObject wObj = (WorkerObject)e.Argument;

            String filename = wObj.filename;
            ERFilterProps props = wObj.props;
            Constants.ColorSpace colorspace = wObj.colorspace;
            Constants.VisChannels channelsFlag = wObj.channels;

            Image<Bgr, Byte> mImage = new Image<Bgr, byte>(filename);

            MyERFilter filter1 = MyERFilterNM.createERFilterNM1(MyERFilterNM.loadClassifierNM1(classifier1Path),
                                                                 props.thresholdDelta,
                                                                 props.minArea,
                                                                 props.maxArea,
                                                                 props.minProbability,
                                                                 true,
                                                                 props.minProbabilityDifference);
            MyERFilter filter2 = MyERFilterNM.createERFilterNM2(MyERFilterNM.loadClassifierNM2(classifier2Path), 0.3f);

            VectorOfMat channels = new VectorOfMat();
                //Utils.CreateXXChannels(mImage.GetInputArray().GetMat(), wObj.colorspace, wObj.channels, false);
            Utils.CreateXXChannels(ref channels, mImage.GetInputArray().GetMat(), colorspace, channelsFlag, false);

            List<ERStat>[] regions = new List<ERStat>[channels.Size];
            for (int i = 0; i < channels.Size; i++)
            {
                regions[i] = new List<ERStat>();
                filter1.Run(channels[i].GetInputArray(), ref regions[i]);
                filter2.Run(channels[i].GetInputArray(), ref regions[i]);
            }
            Rectangle[] rects = new Rectangle[0];
            try
            {
                List<List<Tuple<int, int>>> groups;
                List<Rectangle> rectangles;
                rects = ERGroupingNM.DoERGroupingNM(mImage.GetInputArray(), channels, regions, out groups, out rectangles, true);

                GroupingUtils.SouloupwseRects(rects, mImage.Rows, mImage.Cols);
            }
            catch (CvException cve)
            {
                if (Constants.VERBOSE)
                {
                    Debug.WriteLine("");
                    Debug.WriteLine(cve.ToString());
                    Debug.WriteLine("@ image: " + filename);
                    Debug.WriteLine(""); 
                }
            }
            finally
            {
                IOUtils.CreateResultsFile(filename, rects, tryName);
            }
        }
    }
}
