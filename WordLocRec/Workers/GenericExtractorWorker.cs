﻿using Emgu.CV;
using Emgu.CV.CvEnum;
using Emgu.CV.ML;
using Emgu.CV.Text;
using Emgu.CV.Util;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WordLocRec.DataStructs;
using WordLocRec.Grouping;
using WordLocRec.SVMStuff;
using WordLocRec.Tools;

namespace WordLocRec.Workers
{
    class GenericExtractorWorker : BackgroundWorker
    {
        string cl1, cl2, cl3, outputFolder;

        public BackgroundWorker InitializeSlave(BackgroundWorker worker, String cl1, String cl2, String cl3, String outputFolder, int type)
        {
            this.cl1 = cl1;
            this.cl2 = cl2;
            this.cl3 = cl3;
            this.outputFolder = outputFolder;

            if (worker != null) worker.Dispose();
            worker = new BackgroundWorker();
            worker.WorkerSupportsCancellation = false;
            worker.WorkerReportsProgress = false;

            if (type == 0)
            {
                worker.DoWork += worker_EreDoWork;
            }
            else if (type == 1)
            {
                worker.DoWork += worker_NMDoWork;
            }
            else if (type == 2)
                worker.DoWork += worker_CERDoWork;
            else
                worker.DoWork += worker_TranslatedDoWork;
            return worker;
        }

        private void worker_EreDoWork(object sender, DoWorkEventArgs e)
        {
            BackgroundWorker slave = sender as BackgroundWorker;
            WorkerObject wObj = (WorkerObject)e.Argument;

            String filename = wObj.filename;
            ERFilterProps props = wObj.props;
            List<ColorChannel> channelProj = wObj.channelProjections;
            Constants.GroupingMethod grouping = wObj.grouping;
            Constants.FeatureType f1 = wObj.f1;
            Constants.FeatureType f2 = wObj.f2;
            Constants.WordFeatureType wordFeature = wObj.wordFeatureType;
            bool StrictNMS = wObj.strictNMS;
            bool CombineResults = wObj.combineChannels;
            bool AllowSmoothing = wObj.smoothing;
            bool AllowGammaCorrection = wObj.gammaCorrection;

            Mat mImage = new Mat(filename, LoadImageType.Unchanged);
            int downscaleSteps = 0, initialWidth = mImage.Width, initialHeight = mImage.Height;
            while (mImage.Width * mImage.Height > 2600000)
            {
                CvInvoke.Resize(mImage, mImage, new Size((int)(0.5 * mImage.Width), (int)(0.5 * mImage.Height)));
                downscaleSteps++;
            }

            if (AllowGammaCorrection)
                ImageProcessingUtils.CorrectionOneLine(ref mImage, wObj.gamma);
            if (AllowSmoothing)
                ImageProcessingUtils.FilterOneLine(ref mImage, wObj.smoothX, wObj.smoothY, wObj.fun);

            VectorOfMat channels = new VectorOfMat();
            //Utils.CreateXXChannels(ref channels, mImage, colorspace, visChannels, false, gradients);
            double[] scales;
            bool[] forwardORInverse;
            Utils.CreateAnyChannelCombination(ref channels, mImage, channelProj, out scales, out forwardORInverse, true);
            List<Rectangle[]> wordRects = new List<Rectangle[]>();

            try
            {
                // preparation in case the user wants combined results
                List<List<List<ERStat>>> prunedErListPerWordRect = new List<List<List<ERStat>>>();

                for (int chan = 0; chan < channels.Size; chan++) // for each available channel projection
                {
                    Mat originalImage = mImage.Clone(); // get a copy of the image because it may need to be resized further

                    if (scales[chan] != 1d) // resize to scale
                    {
                        CvInvoke.Resize(originalImage, originalImage, new Size((int)(scales[chan] * originalImage.Width), (int)(scales[chan] * originalImage.Height)));
                    }

                    using (VectorOfMat currentChannel = new VectorOfMat())
                    {
                        currentChannel.Push(channels[chan]);
                        //extraction
                        Rectangle[] singleRects = new Rectangle[0];
                        List<ERStat>[] firstRegions, feedbackRegions;
                        ERExtractor.ExecuteDefaultExtractorPipelineNoGrouping(originalImage, currentChannel, f1, f2, out firstRegions, out feedbackRegions, props, cl1, cl2, StrictNMS, forwardORInverse[chan]);

                        //grouping 
                        List<Tuple<List<ERStat>, int>> erListPerWordRect;
                        List<ChineseGrouping.Line> allLines = new List<ChineseGrouping.Line>();
                        switch (grouping)
                        {
                            case Constants.GroupingMethod.ExhaustiveSearch:
                                {
                                    List<Rectangle> boxes; List<List<Tuple<int, int>>> groups;
                                    singleRects = ERGroupingNM.DoERGroupingNM(originalImage.GetInputArray(), currentChannel, firstRegions, out groups, out boxes, true);
                                    singleRects = GroupingUtils.SouloupwseRects(singleRects, originalImage.Rows, originalImage.Cols);

                                    erListPerWordRect = WordStat.CreateErListPerWordRect(firstRegions, groups);

                                    break;
                                }
                            case Constants.GroupingMethod.chinese:
                                {
                                    List<ChineseGrouping.Line[]> linesPerChannel = GroupingUtils.ChineseGroupingPerChannel(originalImage, currentChannel, firstRegions, feedbackRegions);
                                    foreach (ChineseGrouping.Line[] lines in linesPerChannel)
                                        foreach (ChineseGrouping.Line l in lines)
                                            l.wordRect = GroupingUtils.SouloupwseRects(l.wordRect, originalImage.Rows, originalImage.Cols);

                                    // get the ers per line
                                    List<Rectangle> wRects;
                                    erListPerWordRect = LineWordStat.CreateErListPerWordRect(linesPerChannel, out wRects, out allLines);
                                    singleRects = wRects.ToArray();
                                    break;
                                }
                            default:
                                {
                                    throw new Exception("Unsupported grouping type: " + grouping);
                                }
                        }

                        // pruning
                        Rectangle[] prunedRects = new Rectangle[0];
                        if (wordFeature != 0) // word classification
                        {
                            BaseFeature[] wordStats;
                            IStatModel model = SvmUtils.DefineTypeOfClassifier(cl3);
                            if (wordFeature.HasFlag(Constants.WordFeatureType.WordStat))
                            {
                                wordStats = WordStat.CreateMultiple(singleRects, erListPerWordRect, originalImage, currentChannel, "");
                                prunedRects = ERUtils.WordPruning<SVMWordStatExample>(wordStats, model, singleRects, true, wordFeature.HasFlag(Constants.WordFeatureType.Standardized) ? Constants.FeatureType.StandarizedPerFeature : 0);
                            }
                            else if (wordFeature.HasFlag(Constants.WordFeatureType.SimpleWordStat))
                            {
                                wordStats = SimpleWordStat.CreateMultiple(singleRects, erListPerWordRect, originalImage, currentChannel, "");
                                prunedRects = ERUtils.WordPruning<SVMSimpleWordStatExample>(wordStats, model, singleRects, true, wordFeature.HasFlag(Constants.WordFeatureType.Standardized) ? Constants.FeatureType.StandarizedPerFeature : 0);
                            }
                            else if (wordFeature.HasFlag(Constants.WordFeatureType.LineWordStat))
                                if (grouping.HasFlag(Constants.GroupingMethod.chinese))
                                {
                                    wordStats = LineWordStat.CreateMultiple(singleRects, allLines.ToArray(), erListPerWordRect, originalImage, currentChannel, "");
                                    prunedRects = ERUtils.WordPruning<SVMLineWordStatSample>(wordStats, model, singleRects, true, wordFeature.HasFlag(Constants.WordFeatureType.Standardized) ? Constants.FeatureType.StandarizedPerFeature : 0);
                                }
                                else
                                    throw new Exception("Unsupported combination: " + grouping + "  " + wordFeature);
                        }
                        else // no pruning
                        {
                            prunedRects = singleRects;
                        }

                        // keep the er rect list per word rect found in channel for combination
                        prunedErListPerWordRect.Add(new List<List<ERStat>>());
                        for (int i = 0; i < prunedRects.Length; i++)
                        {
                            for (int j = 0; j < singleRects.Length; j++)
                            {
                                if (prunedRects[i] == singleRects[j])
                                {
                                    prunedErListPerWordRect[chan].Add(erListPerWordRect[j].Item1);
                                    break;
                                }
                            }
                        }
                        // upscale word rects and er rects per word rect
                        if (scales[chan] != 1d)
                        {
                            prunedRects = GroupingUtils.UpscaleRectangles(prunedRects, 1 / scales[chan], 1, mImage.Size);
                            foreach (List<ERStat> p in prunedErListPerWordRect[chan])
                            {
                                foreach (ERStat er in p)
                                {
                                    er.rect = GroupingUtils.UpscaleRectangle(er.rect, 1 / scales[chan]);
                                }
                            }
                        }

                        wordRects.Add(prunedRects);

                        originalImage.Dispose();
                    }

                }
                Rectangle[] finalRects;
                if (CombineResults)
                {
                    finalRects = GroupingUtils.CombineChannelRectangles(wordRects, prunedErListPerWordRect).ToArray();
                }
                else
                {
                    List<Rectangle> temp = new List<Rectangle>();
                    foreach (Rectangle[] sr in wordRects)
                        temp.AddRange(sr);
                    finalRects = temp.ToArray();
                }
                if (downscaleSteps > 0)
                {
                    finalRects = GroupingUtils.UpscaleRectangles(finalRects, 2, downscaleSteps, new Size(initialWidth, initialHeight));
                }

                IOUtils.SaveResultsFile(filename, finalRects, outputFolder);
            }
            catch (Exception cve)
            {
                IOUtils.SaveExceptionFile(filename, cve.ToString(), outputFolder);
                throw new GenericExtractorWorkerException(filename, cve);
            }
            finally
            {
                Disposer.DisposeMatVectorOfMatRegions(ref mImage, ref channels);
            }
            
        }

        private void worker_NMDoWork(object sender, DoWorkEventArgs e)
        {
            BackgroundWorker slave = sender as BackgroundWorker;

            WorkerObject wObj = (WorkerObject)e.Argument;

            String filename = wObj.filename;
            ERFilterProps props = wObj.props;
            List<ColorChannel> channelProj = wObj.channelProjections;
            Constants.GroupingMethod groupMethod = wObj.grouping;
            bool AllowSmoothing = wObj.smoothing;
            bool AllowGammaCorrection = wObj.gammaCorrection;

            Mat mImage = new Mat(filename, LoadImageType.Unchanged);

            ERFilterNM1 er1 = new ERFilterNM1(cl1, props.thresholdDelta, props.minArea, props.maxArea, props.minProbability, props.nonMaxSuppression, props.minProbabilityDifference);
            ERFilterNM2 er2 = new ERFilterNM2(cl2, 0.5f);

            VectorOfMat channels = new VectorOfMat();
            try
            {
                if (AllowGammaCorrection)
                    ImageProcessingUtils.CorrectionOneLine(ref mImage, wObj.gamma);
                if (AllowSmoothing)
                    ImageProcessingUtils.FilterOneLine(ref mImage, wObj.smoothX, wObj.smoothY, wObj.fun);

                //Utils.CreateXXChannels(ref channels, mImage, colorspace, channelsFlag, false, flagGradient);
                double[] scales;
                bool[] forwardORInverse;
                Utils.CreateAnyChannelCombination(ref channels, mImage, channelProj, out scales, out forwardORInverse, false);
            
                VectorOfERStat[] regions = new VectorOfERStat[channels.Size];
                for (int i = 0; i < channels.Size; i++)
                {
                    regions[i] = new VectorOfERStat();
                    er1.Run(channels[i], regions[i]);
                    er2.Run(channels[i], regions[i]);
                }
                Rectangle[] rects = new Rectangle[0];

                if (groupMethod.Equals(Constants.GroupingMethod.ExhaustiveSearch))
                    rects = ERFilter.ERGrouping(mImage, channels, regions, ERFilter.GroupingMethod.OrientationHoriz, null, 0.7f);
                else if (groupMethod.Equals(Constants.GroupingMethod.Karatzas))
                    rects = ERFilter.ERGrouping(mImage, channels, regions, ERFilter.GroupingMethod.OrientationAny, Constants.CLASSIFIERGROUPINGPATH, 0.7f);
                rects = GroupingUtils.SouloupwseRects(rects, mImage.Rows, mImage.Cols);

                IOUtils.SaveResultsFile(filename, rects, outputFolder);
            }
            catch (Exception cve)
            {
                IOUtils.SaveExceptionFile(filename, cve.ToString(), outputFolder);
                throw new GenericExtractorWorkerException(filename, cve);
            }
            finally
            {
                Disposer.DisposeMatVectorOfMatRegions(ref mImage, ref channels);
            }   
        }

        private void worker_CERDoWork(object sender, DoWorkEventArgs e)
        {
            BackgroundWorker slave = sender as BackgroundWorker;

            WorkerObject wObj = (WorkerObject)e.Argument;

            String filename = wObj.filename;
            ERFilterProps props = wObj.props;
            List<ColorChannel> channelProj = wObj.channelProjections;
            Constants.GroupingMethod flagGrouping = wObj.grouping;
            bool AllowSmoothing = wObj.smoothing;
            bool AllowGammaCorrection = wObj.gammaCorrection;
            bool CombineResults = wObj.combineChannels;

            Mat mImage = new Mat(filename, LoadImageType.Unchanged);
            int downscaleSteps = 0, initialWidth = mImage.Width, initialHeight = mImage.Height;
            while (mImage.Width * mImage.Height > 2600000)
            {
                CvInvoke.Resize(mImage, mImage, new Size((int)(0.5 * mImage.Width), (int)(0.5 * mImage.Height)));
                downscaleSteps++;
            }

            CERFilter filter1 = CERFilter.createERFilterNM1(CERFilter.loadClassifierNM1(cl1),
                                                                 props.thresholdDelta,
                                                                 props.minArea,
                                                                 props.maxArea,
                                                                 props.minProbability,
                                                                 props.nonMaxSuppression,
                                                                 props.minProbabilityDifference);
            CERFilter filter2 = CERFilter.createERFilterNM2(CERFilter.loadClassifierNM2(cl2), 0.5f);

            VectorOfMat channels = new VectorOfMat();

            try
            {
                if (AllowGammaCorrection)
                    ImageProcessingUtils.CorrectionOneLine(ref mImage, wObj.gamma);
                if (AllowSmoothing)
                    ImageProcessingUtils.FilterOneLine(ref mImage, wObj.smoothX, wObj.smoothY, wObj.fun);
                //Utils.CreateXXChannels(ref channels, mImage, colorspace, channelsFlag, false, flagGradient);
                double[] scales;
                bool[] forwardORInverse;
                Utils.CreateAnyChannelCombination(ref channels, mImage, channelProj, out scales, out forwardORInverse, false);

                List<ERStat>[] regions = new List<ERStat>[channels.Size];
                Rectangle[] rects = new Rectangle[0], rects1 = new Rectangle[0];

                for (int i = 0; i < channels.Size; i++)
                {
                    regions[i] = new List<ERStat>();
                    filter1.Run(channels[i].GetInputArray(), ref regions[i]);
                    filter2.Run(channels[i].GetInputArray(), ref regions[i]);
                }
              
                if (flagGrouping.HasFlag(Constants.GroupingMethod.Karatzas))
                {
                    VectorOfERStat[] stats = new VectorOfERStat[regions.Length];
                    GroupingUtils.ArrayListErstatsToArrVector(ref stats, regions);

                    rects = ERFilter.ERGrouping(mImage, channels, stats, ERFilter.GroupingMethod.OrientationAny, Constants.CLASSIFIERGROUPINGPATH, 0.5f);
                    GroupingUtils.SouloupwseRects(rects, mImage.Rows, mImage.Cols);
                }
                else
                {
                    List<List<Tuple<int, int>>> groups;
                    List<Rectangle> boxes;

                    rects = ERGroupingNM.DoERGroupingNM(mImage.GetInputArray(), channels, regions, out groups, out boxes, true);
                    GroupingUtils.SouloupwseRects(rects, mImage.Rows, mImage.Cols);

                    // for combination
                    List<Tuple<List<ERStat>, int>> erListPerWordRect = WordStat.CreateErListPerWordRect(regions, groups);
                    List<List<List<ERStat>>> prunedErListPerWordRect = new List<List<List<ERStat>>>();
                    prunedErListPerWordRect.Add(new List<List<ERStat>>());

                    for (int i = 0; i < rects.Length; i++)
                    {
                        prunedErListPerWordRect[0].Add(erListPerWordRect[i].Item1);
                    }

                    if (CombineResults)
                    {
                        List<Rectangle[]> temp = new List<Rectangle[]>(); temp.Add(rects);
                        rects = GroupingUtils.CombineChannelRectangles(temp, prunedErListPerWordRect).ToArray();
                    }
                }
                
                if (downscaleSteps > 0)
                {
                    rects = GroupingUtils.UpscaleRectangles(rects, 2, downscaleSteps, new Size(initialWidth, initialHeight));
                }

                //****2nd stage grouping****//
                //List<Rectangle> ungroupedRectsList = new List<Rectangle>();
                //for (int i = 0; i < channels.Size; i++)
                //{
                //    for (int j = 0; j < regions[i].Count; j++)
                //    {
                //        ungroupedRectsList.Add(regions[i][j].rect);
                //    }
                //}
                //Rectangle[] ungroupedRects = GroupingUtils.SouloupwseRects(ungroupedRectsList.ToArray(), mImage.Rows, mImage.Cols);
                //byte[,] ungroupedHeatMap = GroupingUtils.CreateHeatMap(ungroupedRects.ToArray(), mImage.Rows, mImage.Cols);
                //rects1 = GroupingUtils.UniteRects(rects, ungroupedHeatMap, mImage.Rows, mImage.Cols);
                
                IOUtils.SaveResultsFile(filename, rects, outputFolder);
            }
            catch (Exception cve)
            {
                IOUtils.SaveExceptionFile(filename, cve.ToString(), outputFolder);
                throw new GenericExtractorWorkerException(filename, cve);
            }
            finally
            {
                Disposer.DisposeMatVectorOfMatRegions(ref mImage, ref channels);
            }   
        }

        private void worker_TranslatedDoWork(object sender, DoWorkEventArgs e)
        {
            BackgroundWorker slave = sender as BackgroundWorker;

            WorkerObject wObj = (WorkerObject)e.Argument;

            String filename = wObj.filename;
            ERFilterProps props = wObj.props;
            List<ColorChannel> channelProj = wObj.channelProjections;
            Constants.GroupingMethod flagGrouping = wObj.grouping;
            bool AllowSmoothing = wObj.smoothing;
            bool AllowGammaCorrection = wObj.gammaCorrection;
            bool CombineResults = wObj.combineChannels;


            Mat mImage = new Mat(filename, LoadImageType.Unchanged);
            int downscaleSteps = 0, initialWidth = mImage.Width, initialHeight = mImage.Height;
            while (mImage.Width * mImage.Height > 2600000)
            {
                CvInvoke.Resize(mImage, mImage, new Size((int)(0.5 * mImage.Width), (int)(0.5 * mImage.Height)));
                downscaleSteps++;
            }

            MyERFilter filter1 = MyERFilterNM.createERFilterNM1(MyERFilterNM.loadClassifierNM1(cl1),
                                                    props.thresholdDelta,
                                                    props.minArea,
                                                    props.maxArea,
                                                    props.minProbability,
                                                    props.nonMaxSuppression,
                                                    props.minProbabilityDifference);
            MyERFilter filter2 = MyERFilterNM.createERFilterNM2(MyERFilterNM.loadClassifierNM2(cl2), 0.3f);

            VectorOfMat channels = new VectorOfMat();

            try
            {
                if (AllowGammaCorrection)
                    ImageProcessingUtils.CorrectionOneLine(ref mImage, wObj.gamma);
                if (AllowSmoothing)
                    ImageProcessingUtils.FilterOneLine(ref mImage, wObj.smoothX, wObj.smoothY, wObj.fun);
                //Utils.CreateXXChannels(ref channels, mImage, colorspace, channelsFlag, false, flagGradient);
                double[] scales;
                bool[] forwardORInverse;
                Utils.CreateAnyChannelCombination(ref channels, mImage, channelProj, out scales, out forwardORInverse, false);

                List<ERStat>[] regions = new List<ERStat>[channels.Size];
                Rectangle[] rects = new Rectangle[0], rects1 = new Rectangle[0];

                for (int i = 0; i < channels.Size; i++)
                {
                    regions[i] = new List<ERStat>();
                    filter1.Run(channels[i].GetInputArray(), ref regions[i]);
                    filter2.Run(channels[i].GetInputArray(), ref regions[i]);
                }

                List<List<Tuple<int, int>>> groups;
                List<Rectangle> boxes;

                rects = ERGroupingNM.DoERGroupingNM(mImage.GetInputArray(), channels, regions, out groups, out boxes, true);
                GroupingUtils.SouloupwseRects(rects, mImage.Rows, mImage.Cols);

                List<Tuple<List<ERStat>, int>> erListPerWordRect = WordStat.CreateErListPerWordRect(regions, groups);
                List<List<List<ERStat>>> prunedErListPerWordRect = new List<List<List<ERStat>>>();
                prunedErListPerWordRect.Add(new List<List<ERStat>>());
                
                for (int i = 0; i < rects.Length; i++)
                {                    
                    prunedErListPerWordRect[0].Add(erListPerWordRect[i].Item1);
                }

                if (CombineResults)
                {
                    List<Rectangle[]> temp = new List<Rectangle[]>(); temp.Add(rects);
                    rects = GroupingUtils.CombineChannelRectangles(temp, prunedErListPerWordRect).ToArray();
                }
                if (downscaleSteps > 0)
                {
                    rects = GroupingUtils.UpscaleRectangles(rects, 2, downscaleSteps, new Size(initialWidth, initialHeight));
                }

                IOUtils.SaveResultsFile(filename, rects, outputFolder);
            }
            catch (Exception cve)
            {
                IOUtils.SaveExceptionFile(filename, cve.ToString(), outputFolder);
                throw new GenericExtractorWorkerException(filename, cve);
            }
            finally
            {
                Disposer.DisposeMatVectorOfMatRegions(ref mImage, ref channels);
            }  
        }
    }
}
