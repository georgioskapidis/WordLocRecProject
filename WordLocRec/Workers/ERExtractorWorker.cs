﻿using Emgu.CV;
using Emgu.CV.CvEnum;
using Emgu.CV.ML;
using Emgu.CV.Util;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WordLocRec.DataStructs;
using WordLocRec.Grouping;
using WordLocRec.SVMStuff;
using WordLocRec.Tools;

namespace WordLocRec.Workers
{
    class ERExtractorWorker : BackgroundWorker
    {
        public string classifier1Path, classifier2Path, classifier3Path;
        public string tryName;
        private AssignmentWorker aw;

        public BackgroundWorker InitializeSlave(BackgroundWorker worker, String cl1, String cl2, String cl3, String tryName, AssignmentWorker aw)
        {
            this.aw = aw;
            classifier1Path = cl1;
            classifier2Path = cl2;
            classifier3Path = cl3;
            this.tryName = tryName;
            worker = new BackgroundWorker();
            worker.WorkerSupportsCancellation = false;
            worker.WorkerReportsProgress = false;
            worker.DoWork += EreWorkerDoWorkCombined;
            worker.RunWorkerCompleted += EreWorkerCompleted;
            return worker;
        }

        private void EreWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            this.Dispose(true);
            //nm.AnotherOneBitesTheDust(); //worker is done
            aw.dustBiting(this);
        }

        private void EreWorkerDoWorkCombined(object sender, DoWorkEventArgs e)
        {
            BackgroundWorker slave = sender as BackgroundWorker;

            WorkerObject wObj = (WorkerObject)e.Argument;

            String filename = wObj.filename;
            ERFilterProps props = wObj.props;
            Constants.ColorSpace colorspace = wObj.colorspace;
            Constants.VisChannels channelsFlag = wObj.channels;
            Constants.Gradients flagGradient = wObj.gradients;
            bool AllowSmoothing = wObj.smoothing;
            bool ApplyGamma = wObj.gammaCorrection;
            String outputFolder = "C:\\Users\\George\\Desktop\\datasets\\X2013TestSet";
            Mat mImage = new Mat(filename, LoadImageType.Unchanged);

            if (ApplyGamma)
            {
                ImageProcessingUtils.CorrectionOneLine(ref mImage, 2.2d);
            }
            if (AllowSmoothing)
            {
                ImageProcessingUtils.FilterOneLine(ref mImage, 5, 5, ImageProcessingUtils.FilterWienerC);
            }

            VectorOfMat channels = new VectorOfMat();
            Utils.CreateXXChannels(ref channels, mImage, colorspace, channelsFlag, false, flagGradient);

            Constants.FeatureType f1 = Constants.FeatureType.ColorDistLab | Constants.FeatureType.ColorDistPii | Constants.FeatureType.StandarizedPerFeature;
            Constants.FeatureType f2 = Constants.FeatureType.NM;
            Constants.FeatureType f3 = 0;

            try
            {
                // create lines
                Rectangle[] wordRects;
                List<ChineseGrouping.Line[]> linesPerChannel;
                ERExtractor.ExecuteDefaultExtractorPipelineLineGrouping(mImage, channels, f1, f2, out linesPerChannel, props);

                // get the ers per line
                List<Rectangle> wRects;
                List<ChineseGrouping.Line> allLines;
                List<Tuple<List<ERStat>, int>> erListPerWordRect = LineWordStat.CreateErListPerWordRect(linesPerChannel, out wRects, out allLines);

                if (wRects.Count > 0)
                {
                    //extract the features
                    WordStat[] wordStats = WordStat.CreateMultiple(wRects.ToArray(), erListPerWordRect, mImage, channels, filename);
                    SimpleWordStat[] simpleWordStats = SimpleWordStat.CreateMultiple(wRects.ToArray(), erListPerWordRect, mImage, channels, filename);
                    LineWordStat[] lineWordStats = LineWordStat.CreateMultiple(wRects.ToArray(), allLines.ToArray(), erListPerWordRect, mImage, channels, filename);

                    if (wordStats.Length != simpleWordStats.Length || wordStats.Length != lineWordStats.Length || simpleWordStats.Length != lineWordStats.Length) throw new Exception("tha eprepe na exoyn to idio plithos");

                    for (int i = 0; i < wordStats.Length; i++)
                    {
                        String outputPathWS = Path.Combine(outputFolder, "ChineseGroupingResults", "WordStats", Path.GetFileNameWithoutExtension(filename), "rect" + i.ToString() + ".xml");
                        String outputPathSWS = Path.Combine(outputFolder, "ChineseGroupingResults", "SimpleWordStats", Path.GetFileNameWithoutExtension(filename), "rect" + i.ToString() + ".xml");
                        String outputPathLWS = Path.Combine(outputFolder, "ChineseGroupingResults", "LineWordStats", Path.GetFileNameWithoutExtension(filename), "rect" + i.ToString() + ".xml");
                        wordStats[i].ToXML<WordStat>(outputPathWS);
                        simpleWordStats[i].ToXML<SimpleWordStat>(outputPathSWS);
                        lineWordStats[i].ToXML<LineWordStat>(outputPathLWS);
                    }
                    // load the classifier
                    FileStorage fs;
                    using (RTrees model = SvmUtils.LoadRTreesFromFile(classifier3Path, out fs))
                    {
                        SvmUtils.UnloadModel(fs);
                        // create the result rects
                        wordRects = ERUtils.WordPruning<SVMWordStatExample>(wordStats, model, wRects.ToArray(), true, f3);
                    }
                    // create the results file
                }
                else wordRects = new Rectangle[0];

                IOUtils.CreateResultsFile(filename, wordRects, tryName);
            }
            catch (Exception cve)
            {
                Debug.WriteLine(cve.ToString());
                Debug.WriteLine("@ image: " + filename);
                Debug.WriteLine("");
                return;
            }
            finally
            {
                Disposer.DisposeMatVectorOfMatRegions(ref mImage, ref channels);
            }
        }

        private void EreWorkerDoWork2(object sender, DoWorkEventArgs e)
        {
            BackgroundWorker slave = sender as BackgroundWorker;

            WorkerObject wObj = (WorkerObject)e.Argument;

            String imageFile = wObj.filename;
            ERFilterProps props = wObj.props;
            Constants.ColorSpace colorspace = wObj.colorspace;
            Constants.VisChannels channelsFlag = wObj.channels;
            Constants.Gradients flagGradient = wObj.gradients;
            bool AllowSmoothing = wObj.smoothing;
            bool ApplyGamma = wObj.gammaCorrection;
            String outputFolder = "C:\\Users\\George\\Desktop\\datasets\\X2013TestSet";
            //StringBuilder sb = new StringBuilder();
            Mat mImage = new Mat(imageFile, LoadImageType.Unchanged);
            //sb.AppendLine("threshold: " + props.thresholdDelta + " min " + props.minArea + " max " + props.maxArea + " minprob " + props.minProbability);

            if (ApplyGamma)
            {
                ImageProcessingUtils.CorrectionOneLine(ref mImage, 2.2d);
            }
            if (AllowSmoothing)
            {
                ImageProcessingUtils.FilterOneLine(ref mImage, 5, 5, ImageProcessingUtils.FilterWienerC);
            }

            VectorOfMat channels = new VectorOfMat();
            Utils.CreateXXChannels(ref channels, mImage, colorspace, channelsFlag, false, flagGradient);

            Constants.FeatureType f1 = Constants.FeatureType.ColorDistLab | Constants.FeatureType.ColorDistPii | Constants.FeatureType.StandarizedPerFeature;
            Constants.FeatureType f2 = Constants.FeatureType.NM;

            try
            {
                // create lines
                List<ChineseGrouping.Line[]> linesPerChannel;
                //sb.AppendLine(ERExtractor.ExecuteDefaultExtractorPipelineLineGrouping(mImage, channels, f1, f2, out linesPerChannel, props));
                ERExtractor.ExecuteDefaultExtractorPipelineLineGrouping(mImage, channels, f1, f2, out linesPerChannel, props);

                List<Rectangle> wRects = new List<Rectangle>();
                List<ChineseGrouping.Line> allLines = new List<ChineseGrouping.Line>();
                List<Tuple<List<ERStat>, int>> erListPerWordRect = LineWordStat.CreateErListPerWordRect(linesPerChannel, out wRects, out allLines);

                if (wRects.Count > 0)
                {
                    //extract the features
                    WordStat[] wordStats = WordStat.CreateMultiple(wRects.ToArray(), erListPerWordRect, mImage, channels, imageFile);
                    SimpleWordStat[] simpleWordStats = SimpleWordStat.CreateMultiple(wRects.ToArray(), erListPerWordRect, mImage, channels, imageFile);
                    LineWordStat[] lineWordStats = LineWordStat.CreateMultiple(wRects.ToArray(), allLines.ToArray(), erListPerWordRect, mImage, channels, imageFile);

                    if (wordStats.Length != simpleWordStats.Length || wordStats.Length != lineWordStats.Length || simpleWordStats.Length != lineWordStats.Length) throw new Exception("tha eprepe na exoyn to idio plithos");

                    for (int i = 0; i < wordStats.Length; i++)
                    {
                        String outputPathWS = Path.Combine(outputFolder, "ChineseGroupingResults", "WordStats", Path.GetFileNameWithoutExtension(imageFile), "rect" + i.ToString() + ".xml");
                        String outputPathSWS = Path.Combine(outputFolder, "ChineseGroupingResults", "SimpleWordStats", Path.GetFileNameWithoutExtension(imageFile), "rect" + i.ToString() + ".xml");
                        String outputPathLWS = Path.Combine(outputFolder, "ChineseGroupingResults", "LineWordStats", Path.GetFileNameWithoutExtension(imageFile), "rect" + i.ToString() + ".xml");
                        wordStats[i].ToXML<WordStat>(outputPathWS);
                        simpleWordStats[i].ToXML<SimpleWordStat>(outputPathSWS);
                        lineWordStats[i].ToXML<LineWordStat>(outputPathLWS);
                    }
                }
            }
            catch (Exception cve)
            {
                Debug.WriteLine(cve.ToString());
                Debug.WriteLine("@ image: " + imageFile);
                Debug.WriteLine("");
                return;
            }
            finally
            {
                Disposer.DisposeMatVectorOfMatRegions(ref mImage, ref channels);
                //File.AppendAllText(Path.Combine(outputFolder, "sanityCheck.xml"), sb.ToString());
            }
        }

        private void EreWorkerDoWork(object sender, DoWorkEventArgs e)
        {
            BackgroundWorker slave = sender as BackgroundWorker;

            WorkerObject wObj = (WorkerObject)e.Argument;

            String filename = wObj.filename;
            ERFilterProps props = wObj.props;
            Constants.ColorSpace colorspace = wObj.colorspace;
            Constants.VisChannels channelsFlag = wObj.channels;
            Constants.Gradients flagGradient = wObj.gradients;
            bool AllowSmoothing = wObj.smoothing;
            bool ApplyGamma = wObj.gammaCorrection;

            Mat mImage = new Mat(filename, LoadImageType.Unchanged);

            if (ApplyGamma)
            {
                ImageProcessingUtils.CorrectionOneLine(ref mImage, 2.2d);
            }
            if (AllowSmoothing)
            {
                ImageProcessingUtils.FilterOneLine(ref mImage, 5, 5, ImageProcessingUtils.FilterWienerC);
            }

            VectorOfMat channels = new VectorOfMat();
            Utils.CreateXXChannels(ref channels, mImage, colorspace, channelsFlag, false, flagGradient);

            Constants.FeatureType f1 = Constants.FeatureType.ColorDistLab | Constants.FeatureType.ColorDistPii | Constants.FeatureType.StandarizedPerFeature;
            Constants.FeatureType f2 = Constants.FeatureType.NM;
            Constants.FeatureType f3 = 0;

            try
            {
                // create lines
                Rectangle[] wordRects;
                List<ChineseGrouping.Line[]> linesPerChannel;
                ERExtractor.ExecuteDefaultExtractorPipelineLineGrouping(mImage, channels, f1, f2, out linesPerChannel, props);

                // get the ers per line
                List<Rectangle> wRects;
                List<ChineseGrouping.Line> allLines;
                List<Tuple<List<ERStat>, int>> erListPerWordRect = LineWordStat.CreateErListPerWordRect(linesPerChannel, out wRects, out allLines);

                // load the classifier
                FileStorage fs;
                using (RTrees model = SvmUtils.LoadRTreesFromFile(classifier3Path, out fs))
                {
                    SvmUtils.UnloadModel(fs);
                    // extract the features

                    //BaseFeature[] wordStats = LineWordStat.CreateMultiple(wRects.ToArray(), allLines.ToArray(), erListPerWordRect, mImage, channels, "");
                    BaseFeature[] wordStats = WordStat.CreateMultiple(wRects.ToArray(), erListPerWordRect, mImage, channels, "");
                    //BaseFeature[] simpleWordStats = SimpleWordStat.CreateMultiple(wRects.ToArray(), erListPerWordRect, mImage, channels, "");
                    //LineWordStat[] lineWordStats = LineWordStat.CreateMultiple(wRects.ToArray(), allLines.ToArray(), erListPerWordRect, mImage, channels, "");

                    // create the result rects
                    wordRects = ERUtils.WordPruning<SVMWordStatExample>(wordStats, model, wRects.ToArray(), true, f3);
                }
                // create the results file
                IOUtils.CreateResultsFile(filename, wordRects, tryName);
            }
            catch (Exception cve)
            {
                Debug.WriteLine(cve.ToString());
                Debug.WriteLine("@ image: " + filename);
                Debug.WriteLine("");
                return;
            }
            finally
            {
                Disposer.DisposeMatVectorOfMatRegions(ref mImage, ref channels);
            }
        }
    }
}
