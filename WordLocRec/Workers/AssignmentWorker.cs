﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using WordLocRec.DataStructs;

namespace WordLocRec.Workers
{
    class AssignmentWorker
    {
        String classifier1Path, classifier2Path, classifier3Path;
        String tryName;
        ERFilterProps props;
        Constants.ColorSpace colorspace;
        Constants.VisChannels channels;
        Constants.Gradients gradients;
        bool smoothing, gammaCorrection;
        Constants.GroupingMethod groupingMethod;

        private int totalFilesCount = 0;
        private MainForm nm;
        public BackgroundWorker CreateAssignmentWorker(ERFilterProps props, String cl1, String cl2, String tryName, MainForm nm, bool gammaCorrection, bool smoothing, Constants.ColorSpace colorspace = Constants.ColorSpace.HSV, Constants.VisChannels channels = Constants.VisChannels.AllChannels, Constants.Gradients gradients = Constants.Gradients.Single, Constants.GroupingMethod gMethod = Constants.GroupingMethod.ExhaustiveSearch, String cl3 = null)
        {
            this.nm = nm;
            this.props = props;
            this.colorspace = colorspace;
            this.channels = channels;
            this.gradients = gradients;
            this.smoothing = smoothing;
            this.gammaCorrection = gammaCorrection;
            this.groupingMethod = gMethod;
            classifier1Path = cl1; classifier2Path = cl2; classifier3Path = cl3;
            this.tryName = tryName;
            BackgroundWorker assignmentWorker = new BackgroundWorker();
            assignmentWorker.DoWork += AssignmentWorkerDoWork;
            assignmentWorker.WorkerReportsProgress = false;

            return assignmentWorker;
        }

        BackgroundWorker[] slaves;
        public void dustBiting(BackgroundWorker slave)
        {
            //nm.AnotherOneBitesTheDust();
        }
        private void AssignmentWorkerDoWork(object sender, DoWorkEventArgs e)
        {
            int threadsAllowed = Constants.MAX_EMGU_THREADS;
            BackgroundWorker master = sender as BackgroundWorker;
            slaves = new BackgroundWorker[threadsAllowed];
            for (int i = 0; i < threadsAllowed; i++)
            {
                //slaves[i] = new NMWorker().InitializeSlave(slaves[i], classifier1Path, classifier2Path, tryName, this);
                //slaves[i] = new NMCannnyWorker().InitializeSlave(slaves[i], classifier1Path, classifier2Path, tryNum);
                //slaves[i] = new NMTranslatedWorker().InitializeSlave(slaves[i], classifier1Path, classifier2Path, tryName, nm);
                //slaves[i] = new NMHybridWorker().InitializeSlave(slaves[i], classifier1Path, classifier2Path, tryName, this);
                //slaves[i] = new NMCERWorker().InitializeSlave(slaves[i], classifier1Path, classifier2Path, tryName, this);
                //slaves[i] = new ERWorker().InitializeSlave(slaves[i], classifier1Path, classifier2Path, tryName, this);
                slaves[i] = new ERExtractorWorker().InitializeSlave(slaves[i], classifier1Path, classifier2Path, classifier3Path, tryName, this);
            }
            String[] filenames = (String[])e.Argument;
            totalFilesCount = filenames.Length;
            int filesProcessed = 0;

            while (filesProcessed < filenames.Length)
            {
                var fileProcessed = false;
                while (!fileProcessed)
                {
                    for (int i = 0; i < threadsAllowed; i++)
                    {
                        if (slaves[i].IsBusy != true)
                        {
                            slaves[i].RunWorkerAsync(new WorkerObject(props, filenames[filesProcessed], colorspace, channels, gradients, gammaCorrection, smoothing, groupingMethod));
                            filesProcessed++;
                            fileProcessed = true;
                            break;
                        }
                    }
                    if (!fileProcessed)
                    {
                        Thread.Sleep(100);
                        break;
                    }
                }
            }
        }
    }
}
