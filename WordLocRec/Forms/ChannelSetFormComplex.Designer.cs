﻿namespace WordLocRec.Forms
{
    partial class ChannelSetFormComplex
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.radioButtonChineseGrouping = new System.Windows.Forms.RadioButton();
            this.radioButtonGKGrouping = new System.Windows.Forms.RadioButton();
            this.radioButtonExhastiveSearch = new System.Windows.Forms.RadioButton();
            this.buttonExit = new System.Windows.Forms.Button();
            this.listBoxOptions = new System.Windows.Forms.ListBox();
            this.label1 = new System.Windows.Forms.Label();
            this.listBoxSelected = new System.Windows.Forms.ListBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // radioButtonChineseGrouping
            // 
            this.radioButtonChineseGrouping.AutoSize = true;
            this.radioButtonChineseGrouping.Checked = true;
            this.radioButtonChineseGrouping.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.radioButtonChineseGrouping.Location = new System.Drawing.Point(6, 67);
            this.radioButtonChineseGrouping.Name = "radioButtonChineseGrouping";
            this.radioButtonChineseGrouping.Size = new System.Drawing.Size(87, 17);
            this.radioButtonChineseGrouping.TabIndex = 2;
            this.radioButtonChineseGrouping.TabStop = true;
            this.radioButtonChineseGrouping.Text = "LineGrouping";
            this.radioButtonChineseGrouping.UseVisualStyleBackColor = true;
            // 
            // radioButtonGKGrouping
            // 
            this.radioButtonGKGrouping.AutoSize = true;
            this.radioButtonGKGrouping.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.radioButtonGKGrouping.Location = new System.Drawing.Point(6, 44);
            this.radioButtonGKGrouping.Name = "radioButtonGKGrouping";
            this.radioButtonGKGrouping.Size = new System.Drawing.Size(82, 17);
            this.radioButtonGKGrouping.TabIndex = 1;
            this.radioButtonGKGrouping.Text = "GKGrouping";
            this.radioButtonGKGrouping.UseVisualStyleBackColor = true;
            // 
            // radioButtonExhastiveSearch
            // 
            this.radioButtonExhastiveSearch.AutoSize = true;
            this.radioButtonExhastiveSearch.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.radioButtonExhastiveSearch.Location = new System.Drawing.Point(6, 21);
            this.radioButtonExhastiveSearch.Name = "radioButtonExhastiveSearch";
            this.radioButtonExhastiveSearch.Size = new System.Drawing.Size(104, 17);
            this.radioButtonExhastiveSearch.TabIndex = 0;
            this.radioButtonExhastiveSearch.Text = "ExhastiveSearch";
            this.radioButtonExhastiveSearch.UseVisualStyleBackColor = true;
            // 
            // buttonExit
            // 
            this.buttonExit.BackColor = System.Drawing.Color.LightSteelBlue;
            this.buttonExit.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonExit.Location = new System.Drawing.Point(346, 277);
            this.buttonExit.Name = "buttonExit";
            this.buttonExit.Size = new System.Drawing.Size(119, 29);
            this.buttonExit.TabIndex = 21;
            this.buttonExit.Text = "Confirm Changes";
            this.buttonExit.UseVisualStyleBackColor = false;
            this.buttonExit.Click += new System.EventHandler(this.buttonExit_Click);
            // 
            // listBoxOptions
            // 
            this.listBoxOptions.FormattingEnabled = true;
            this.listBoxOptions.Items.AddRange(new object[] {
            "Hue",
            "Saturation",
            "Value",
            "Blue",
            "Green",
            "Red",
            "Lightness",
            "a",
            "b",
            "X",
            "Y",
            "Ζ",
            "Pii1",
            "Pii2",
            "Pii3",
            "Grayscale",
            "Distances",
            "Single Gradient Magnitude",
            "Multi Channel Gradient",
            "Multi Channel Sobel Gradient"});
            this.listBoxOptions.Location = new System.Drawing.Point(12, 29);
            this.listBoxOptions.Name = "listBoxOptions";
            this.listBoxOptions.Size = new System.Drawing.Size(178, 277);
            this.listBoxOptions.TabIndex = 40;
            this.listBoxOptions.DoubleClick += new System.EventHandler(this.listBoxOptions_DoubleClick);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 10);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(90, 13);
            this.label1.TabIndex = 41;
            this.label1.Text = "Available choices";
            // 
            // listBoxSelected
            // 
            this.listBoxSelected.FormattingEnabled = true;
            this.listBoxSelected.Location = new System.Drawing.Point(196, 29);
            this.listBoxSelected.Name = "listBoxSelected";
            this.listBoxSelected.Size = new System.Drawing.Size(269, 173);
            this.listBoxSelected.TabIndex = 42;
            this.listBoxSelected.KeyDown += new System.Windows.Forms.KeyEventHandler(this.listBoxSelected_KeyDown);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.radioButtonChineseGrouping);
            this.groupBox1.Controls.Add(this.radioButtonGKGrouping);
            this.groupBox1.Controls.Add(this.radioButtonExhastiveSearch);
            this.groupBox1.Location = new System.Drawing.Point(196, 207);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(134, 100);
            this.groupBox1.TabIndex = 43;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Grouping Options";
            // 
            // ChannelSetFormComplex
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.GhostWhite;
            this.ClientSize = new System.Drawing.Size(483, 319);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.listBoxSelected);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.listBoxOptions);
            this.Controls.Add(this.buttonExit);
            this.MaximumSize = new System.Drawing.Size(499, 358);
            this.MinimumSize = new System.Drawing.Size(499, 358);
            this.Name = "ChannelSetFormComplex";
            this.Text = "ChannelSetFormComplex";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.RadioButton radioButtonChineseGrouping;
        private System.Windows.Forms.RadioButton radioButtonGKGrouping;
        private System.Windows.Forms.RadioButton radioButtonExhastiveSearch;
        private System.Windows.Forms.Button buttonExit;
        private System.Windows.Forms.ListBox listBoxOptions;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ListBox listBoxSelected;
        private System.Windows.Forms.GroupBox groupBox1;

    }
}