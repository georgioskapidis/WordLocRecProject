﻿using Emgu.CV.Util;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WordLocRec.Forms
{
    public partial class MultipleWordRectangleForm : Form
    {
        VectorOfMat[] sequences;
        int viewIndex = -1;
        String[] texts;
        PictureBox[] pbs;

        public MultipleWordRectangleForm(VectorOfMat[] imageSequences, String title, String[] texts)
        {
            InitializeComponent();
            this.Text = title;

            sequences = new VectorOfMat[imageSequences.Length];
            for (int i = 0; i < imageSequences.Length; i++)
            {
                this.sequences[i] = new VectorOfMat();
                for (int j = 0; j < imageSequences[i].Size; j++)
                    this.sequences[i].Push(imageSequences[i][j].Clone());
            }
            this.texts = texts;

            DefineGrid();

            viewIndex = 0;
            setInfoToView();
        }
        private void DefineGrid()
        {
            tableLayoutPanel1.ColumnCount = sequences.Length;
            tableLayoutPanel1.RowCount = 1;
            tableLayoutPanel1.GrowStyle = TableLayoutPanelGrowStyle.FixedSize;
            CreateColumns();
        }
        private void CreateColumns()
        {
            pbs = new PictureBox[sequences.Length];
            for (int i = 0; i < sequences.Length; i++)
            {
                ColumnStyle cs = new ColumnStyle(SizeType.AutoSize);
                tableLayoutPanel1.ColumnStyles.Add(cs);
                pbs[i] = new PictureBox();
                pbs[i].Dock = DockStyle.Fill;
                pbs[i].SizeMode = PictureBoxSizeMode.Zoom;

                tableLayoutPanel1.Controls.Add(pbs[i], i, 0);
            }
        }
        
        private void setInfoToView()
        {
            for (int i = 0; i < sequences.Length; i++)
            {
                pbs[i].Image = sequences[i][viewIndex].Bitmap;
            }
            textBoxInfo.Text = texts[viewIndex];
            textBoxNumber.Text = viewIndex.ToString();
        }

        private void buttonNext_Click(object sender, EventArgs e)
        {
            viewIndex = viewIndex + 1 >= sequences[0].Size ? 0 : viewIndex + 1;

            setInfoToView();
        }

        private void buttonPrev_Click(object sender, EventArgs e)
        {
            viewIndex = viewIndex - 1 < 0 ? viewIndex = sequences[0].Size - 1 : viewIndex - 1;

            setInfoToView();
        }

        private void textBoxNumber_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode.Equals(Keys.Enter))
            {
                TextBox tb = sender as TextBox;
                if (tb.Focused)
                {
                    int number = -1;
                    if (int.TryParse(tb.Text, out number))
                    {
                        if (number > 0 && number < sequences[0].Size)
                        {
                            viewIndex = number;
                            setInfoToView();
                        }
                    }
                }
            }
        }

    }
}
