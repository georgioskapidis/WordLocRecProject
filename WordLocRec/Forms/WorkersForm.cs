﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WordLocRec.DataStructs;
using WordLocRec.Tools;
using WordLocRec.Workers;

namespace WordLocRec.Forms
{
    public partial class WorkersForm : Form
    {
        String[] imagePaths;
        String outputPath, classifier1, classifier2, classifier3;
        //Constants.ColorSpace colorspace;
        //Constants.VisChannels channels;
        //Constants.Gradients gradients;
        Constants.GroupingMethod grouping;
        Constants.FeatureType flagFeatureType1, flagFeatureType2;
        Constants.WordFeatureType flagWordType;
        ERFilterProps erprops;
        List<ColorChannel> channelsProj;

        BackgroundWorker assignmentWorker;
        bool isRunning;

        Stopwatch w;
        int selectedWorker;
        public WorkersForm()
        {
            InitializeComponent();
            InitializeVariables();
        }
        private void InitializeVariables()
        {
            imagePaths = null;
            outputPath = null;
            //colorspace = Constants.ColorSpace.LAB;
            //channels = Constants.VisChannels.AllChannels;
            //gradients = Constants.Gradients.Single;
            channelsProj = new List<ColorChannel>();
            grouping = Constants.GroupingMethod.chinese;
            selectedWorker = -1;
            w = new Stopwatch();
            isRunning = false;
        }

        #region LoadSave
        private void buttonLoad_Click(object sender, EventArgs e)
        {
            String[] fileNames = FormUtils.ParametrizedOpenFileDialog(Constants.IMAGE_FILE_FILTER, true, "Select the images");
            if (fileNames == null) return;
            imagePaths = fileNames;
            textBoxLoad.Text = Path.GetDirectoryName(imagePaths[0]);
        }

        private void buttonOutput_Click(object sender, EventArgs e)
        {
            String outputFolder = FormUtils.ParametrizedFolderBrowserDialog("Select the output folder");
            if (outputFolder == null) return;
            outputPath = outputFolder;
            textBoxOutput.Text = outputFolder;
        }

        private void buttonSetCl1_Click(object sender, EventArgs e)
        {
            String cl1 = FormUtils.ParametrizedOpenFileDialog(Constants.XML_YML_FILE_FILTER, false, "Select an appropriate classifier", true)[0];
            if (cl1 != null) classifier1 = cl1;
        }

        private void buttonSetCl2_Click(object sender, EventArgs e)
        {
            String cl2 = FormUtils.ParametrizedOpenFileDialog(Constants.XML_YML_FILE_FILTER, false, "Select an appropriate classifier", true)[0];
            if (cl2 != null) classifier2 = cl2;
        }

        private void buttonSetCl3_Click(object sender, EventArgs e)
        {
            String cl3 = FormUtils.ParametrizedOpenFileDialog(Constants.XML_YML_FILE_FILTER, false, "Select an appropriate classifier", true)[0];
            if (cl3 != null) classifier3 = cl3;
        }
        #endregion

        private void checkBoxAuto_CheckedChanged(object sender, EventArgs e)
        {
            CheckBox cb = (CheckBox)sender as CheckBox;
            panelProps.Enabled = !cb.Checked;
        }

        private void checkBoxSmooth_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBoxSmooth.Checked)
            {
                checkBoxGS.Enabled = true;
                checkBoxWiener.Enabled = true;
                numericUpDown1.Enabled = true;
                numericUpDown2.Enabled = true;
            }
            else
            {
                checkBoxGS.Enabled = false;
                checkBoxWiener.Enabled = false;
                numericUpDown1.Enabled = false;
                numericUpDown2.Enabled = false;
            }
        }

        private void checkBoxGS_CheckedChanged(object sender, EventArgs e)
        {
            FormUtils.CheckBoxesMutuallyExclusive((CheckBox)sender, checkBoxWiener);
        }

        private void checkBoxWiener_CheckedChanged(object sender, EventArgs e)
        {
            FormUtils.CheckBoxesMutuallyExclusive((CheckBox)sender, checkBoxGS);
        }

        private void checkBoxGammaCorrect_CheckedChanged(object sender, EventArgs e)
        {
            numericUpDownGamma.Enabled = checkBoxGammaCorrect.Checked;
        }

        private void buttonChannels_Click(object sender, EventArgs e)
        {
            //ChannelSetForm csf = new ChannelSetForm(colorspace, channels, gradients, grouping);
            //if (csf.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            //{
            //    colorspace = csf.flagColorSpace;
            //    channels = csf.flagChannels;
            //    gradients = csf.flagGradients;
            //    grouping = csf.flagGrouping;
            //}
            ChannelSetFormComplex csfx = new ChannelSetFormComplex(channelsProj, grouping);
            if (csfx.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                channelsProj = csfx.channels;
                grouping = csfx.grouping;
            }
        }

        private void checkBoxDefaultClassifiers_CheckedChanged(object sender, EventArgs e)
        {
            CheckBox cb = (CheckBox)sender as CheckBox;
            panelClassifiers.Enabled = !cb.Checked;
        }

        private void buttonSetF1_Click(object sender, EventArgs e)
        {
            using (ChooseFeaturesForm cff = new ChooseFeaturesForm(flagFeatureType1))
                if (cff.ShowDialog() == DialogResult.OK)
                    flagFeatureType1 = cff.featureType;
        }

        private void buttonSetF2_Click(object sender, EventArgs e)
        {
            using (ChooseFeaturesForm cff = new ChooseFeaturesForm(flagFeatureType2))
                if (cff.ShowDialog() == DialogResult.OK)
                    flagFeatureType2 = cff.featureType;
        }

        private void buttonSetF3_Click(object sender, EventArgs e)
        {
            using (ChooseWordFeaturesForm cwff = new ChooseWordFeaturesForm(flagWordType))
                if (cwff.ShowDialog() == DialogResult.OK)
                    flagWordType = cwff.wordFeatureType;
        }

        public bool ParsePropsInput()
        {
            int thresholdDelta;
            float minArea, maxArea, minProb, minProbDiff;

            if (!Int32.TryParse(textBoxThreshD.Text, out thresholdDelta) || (!float.TryParse(textBoxMinAr.Text, out minArea)) || (!float.TryParse(textBoxMaxAr.Text, out maxArea)) || (!float.TryParse(textBoxMinProb.Text, out minProb)) || (!float.TryParse(textBoxMinProbDiff.Text, out minProbDiff))) 
            {
                MessageBox.Show("Couldn't parse ER extraction properties");
                return false;
            }
            else
            {
                if (thresholdDelta > 0 && thresholdDelta < 128 && minArea > 0 && minArea < maxArea && maxArea > minArea && maxArea < 1 && minProb >= 0 && minProb <= 1 && minProbDiff >= 0 && minProbDiff < 1)
                {
                    erprops = new ERFilterProps(thresholdDelta, minArea, maxArea, minProb, minProbDiff);
                    return true;
                }
                else
                {
                    MessageBox.Show("Wrong values in ER extraction properties");
                    return false;
                }
            }
        }

        private void buttonExtract_Click(object sender, EventArgs e)
        {
            if (!isRunning)
            {
                if (channelsProj.Count == 0)
                {
                    MessageBox.Show("Please set some channels first!");
                    return;
                }
                if (imagePaths == null || outputPath == null) // set paths
                {
                    MessageBox.Show("Load images, set output path properly and try again.");
                    return;
                }
                if (checkBoxAuto.Checked == false) // parse auto or manual props
                {
                    if (!ParsePropsInput()) return;
                }
                else erprops = new ERFilterProps(2, 0.00005f, 0.1f, 0.2f, 0.1f);
                if (selectedWorker == -1)
                {
                    MessageBox.Show("Select a worker");
                    return;
                }
                // no need to parse smoothing and gamma
                if (checkBoxDefaultClassifiers.Checked)
                {
                    if (selectedWorker == 0) // default for my method
                    {
                        flagFeatureType1 = Constants.FeatureType.ColorDistLab | Constants.FeatureType.ColorDistPii | Constants.FeatureType.StandarizedPerFeature;
                        flagFeatureType2 = Constants.FeatureType.NM | Constants.FeatureType.SignificantPaths | Constants.FeatureType.ConvexityDefects | Constants.FeatureType.StandarizedPerFeature;
                        flagWordType = Constants.WordFeatureType.WordStat;
                        classifier1 = Constants.COLOR_DISTANCE_CLASSIFIER;
                        classifier2 = Constants.SVM_sNMPathsConv;
                        classifier3 = Constants.RF_WORDSTAT;
                    }
                    else //default for opencv
                    {
                        classifier1 = Constants.CLASSIFIER1PATH;
                        classifier2 = Constants.CLASSIFIER2PATH;
                        classifier3 = Constants.CLASSIFIERGROUPINGPATH;
                        grouping = Constants.GroupingMethod.ExhaustiveSearch;
                    }
                }
                if (flagWordType == Constants.WordFeatureType.LineWordStat && grouping != Constants.GroupingMethod.chinese)
                {
                    MessageBox.Show("LineWordStat is supported only by Line Grouping for now.", "Change settings please!");
                    return;
                }
                WorkerObject workerObject = new WorkerObject(erprops, null,
                    channelsProj,
                    checkBoxSmooth.Checked ? checkBoxGS.Checked ? (ImageProcessingUtils.FilteringFunction)ImageProcessingUtils.FilterGaussianC : (ImageProcessingUtils.FilteringFunction)ImageProcessingUtils.FilterWienerC : (ImageProcessingUtils.FilteringFunction)ImageProcessingUtils.FilterWienerC,
                    flagFeatureType1,
                    flagFeatureType2,
                    flagWordType,
                    radioButtonNMSstrict.Checked,
                    checkBoxCombine.Checked,
                    checkBoxGammaCorrect.Checked,
                    checkBoxSmooth.Checked,
                    grouping,
                    (float)numericUpDownGamma.Value,
                    (int)numericUpDown1.Value,
                    (int)numericUpDown2.Value);

                if (assignmentWorker == null)
                {
                    assignmentWorker = new AssignmentWorka().CreateAssignmentWorker(classifier1, classifier2, classifier3, outputPath, workerObject, selectedWorker);
                    assignmentWorker.ProgressChanged += assignmentWorker_ProgressChanged;
                    assignmentWorker.RunWorkerCompleted += assignmentWorker_RunWorkerCompleted;
                }

                if (!assignmentWorker.IsBusy)
                {
                    w.Restart();
                    assignmentWorker.RunWorkerAsync(imagePaths);
                    isRunning = true;
                    buttonExtract.Text = "Cancel";
                }
            }
            else
            {
                assignmentWorker.CancelAsync();
                this.labelStatus.Text = "Waiting for remaining asynchronous operations. Cancelling...";
            }
            

        }

        private void assignmentWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            w.Stop();
            if (e.Cancelled)
            {
                this.labelStatus.Text = "Cancelled, try again?";
            }
            else
            {
                TimeSpan ts = w.Elapsed;
                String elapsedTime = String.Format("{0::00}:{1:00}:{2:00}.{3:00}", ts.Hours, ts.Minutes, ts.Seconds, ts.Milliseconds / 10);
                String toString = elapsedTime;
                this.labelStatus.Text = "Done " + imagePaths.Length + " images in: " + toString;
            }

            isRunning = false;
            this.buttonExtract.Text = "GO!";
            this.progressBar1.Value = 0;
            assignmentWorker = null;
        }

        private void assignmentWorker_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            this.progressBar1.Value = e.ProgressPercentage;
            this.labelStatus.Text = (string)e.UserState;
        }

        private void listViewWorkers_ItemActivate(object sender, EventArgs e)
        {
            selectedWorker = listViewWorkers.SelectedIndices[0];
        }

    }
}
