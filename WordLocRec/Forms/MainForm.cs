﻿using Emgu.CV;
using Emgu.CV.Text;
using Emgu.CV.Structure;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Emgu.CV.CvEnum;
using Emgu.CV.Util;
using WordLocRec.DataStructs;
using WordLocRec.Tools;
using WordLocRec.Forms;
using WordLocRec.Grouping;
using WordLocRec.SVMStuff;
using Emgu.CV.ML;

namespace WordLocRec
{
    public partial class MainForm : Form
    {
        public MainForm()
        {           
            InitializeComponent();
            this.panelPicBox.MouseWheel += new MouseEventHandler(OnMouseWheelZoom);
            globalWatch = new Stopwatch();
        }
        
        #region Global variables
        Image<Bgr, Byte> _originalImage;
        Mat viewMat;
        Bitmap _viewedImage;
        Enum lastConversion = ColorConversionWrapper.ManualConversions.Bgr2Bgr;
        Stopwatch globalWatch;
        ERFilterProps erprops;

        Constants.ColorSpace flagColorSpace = Constants.ColorSpace.HSV;
        Constants.VisChannels flagChannels = Constants.VisChannels.AllChannels;
        Constants.Gradients flagGradients = Constants.Gradients.Single;
        Constants.GroupingMethod flagGrouping = Constants.GroupingMethod.ExhaustiveSearch;
        Constants.FeatureType flagFeatureType = 0;
        bool hasOpposites = false;
        bool channelsSet = false;

        Constants.VisChannels flagsVis = Constants.VisChannels.AllChannels;
        Boolean globShowCounter = false;
        List<Rectangle[]> rects;
        List<Tuple<List<ERStat>, int>> erListPerWordRect;
        List<ChineseGrouping.Line[]> linesPerChannel;

        double zoomScale = 1.0;
        #endregion

        private void buttonSaveViewedImage_Click(object sender, EventArgs e)
        {
            if (pictureBoxMain.Image == null)
            {
                MessageBox.Show("No Image in picture box to save!", "Opoios viazetai skontaftei");
                return;
            }
            SaveFileDialog sfd = new SaveFileDialog();
            sfd.Filter = Constants.IMAGE_FILE_FILTER;
            sfd.Title = " Save what you see!";
            sfd.ShowDialog();

            if (sfd.FileName != "")
            {
                System.IO.FileStream fs = (System.IO.FileStream)sfd.OpenFile();
                using (Image image = (Image) pictureBoxMain.Image.Clone())
                {
                    image.Save(fs, System.Drawing.Imaging.ImageFormat.Jpeg);
                }
                fs.Close();
            }
        }

        #region Zoom Buttons

        protected void OnMouseWheelZoom(object sender, MouseEventArgs e)
        {
            pictureBoxMain.Focus();
            if (ModifierKeys.HasFlag(Keys.Control))
            {
                if (pictureBoxMain.Focused)
                {
                    applyZoom(e.Delta);
                }
            }
        }
        private void ZoomIn_Click(object sender, EventArgs e)
        {
            applyZoom(120);
        }
        private void ZoomOut_Click(object sender, EventArgs e)
        {
            applyZoom(-120);
        }
        private void applyZoom(int delta)
        {
            if (viewMat != null && viewMat.Width > 0 && delta != 0)
            {
                double zoomChange = (delta / 120) * 1.1;
                if (zoomChange > 0) //zoom in
                {
                    if (zoomScale * zoomChange < Constants.MAX_ZOOM)
                    {
                        zoomScale *= zoomChange;
                    }
                }
                else if (zoomChange < 0)
                {
                    if (zoomScale / -zoomChange > Constants.MIN_ZOOM)
                    {
                        zoomScale /= -zoomChange;
                    }
                }
                Bitmap _viewedImagetmp;
                Utils.Resize3(viewMat.Bitmap, zoomScale, out _viewedImagetmp);
                setBitmapInPictureBox(_viewedImagetmp);
            }
        }

        #endregion

        #region View Buttons

        private void buttonViewLab_Click(object sender, EventArgs e)
        {
            if (_originalImage == null)
            {
                MessageBox.Show("Please load an image first!", "error");
                return;
            }
            createViewMat(ColorConversion.Bgr2Lab);

            if (viewMat != null)
            {
                setBitmapInPictureBox(viewMat.Bitmap);
            }
        }
        private void buttonViewHSV_Click(object sender, EventArgs e)
        {
            if (_originalImage == null)
            {
                MessageBox.Show("Please load an image first!", "error");
                return;
            }
            createViewMat(ColorConversion.Bgr2Hsv);

            if (viewMat != null)
            {
                setBitmapInPictureBox(viewMat.Bitmap);
            }
        }
        private void buttonViewHSV2_Click(object sender, EventArgs e)
        {
            if (_originalImage == null)
            {
                MessageBox.Show("Please load an image first!", "error");
                return;
            }
            createViewMat(ColorConversion.Rgb2Hsv);

            if (viewMat != null)
            {
                setBitmapInPictureBox(viewMat.Bitmap);
            }
        }
        private void buttonViewGrayscale_Click(object sender, EventArgs e)
        {
            if (_originalImage == null)
            {
                MessageBox.Show("Please load an image first!", "error");
                return;
            }
            createViewMat(ColorConversion.Bgr2Gray);

            if (viewMat != null)
            {
                setBitmapInPictureBox(viewMat.Bitmap);
            }
        }
        private void buttonViewXYZ_Click(object sender, EventArgs e)
        {
            if (_originalImage == null)
            {
                MessageBox.Show("Please load an image first!", "error");
                return;
            }
            createViewMat(ColorConversion.Bgr2Xyz);

            if (viewMat != null)
            {
                setBitmapInPictureBox(viewMat.Bitmap);
            }
        }
        private void buttonViewBgr_Click(object sender, EventArgs e)
        {
            if (_originalImage == null)
            {
                MessageBox.Show("Please load an image first!", "error");
                return;
            }
            viewMat = new Mat();

            createViewMat(WordLocRec.Tools.ColorConversionWrapper.ManualConversions.Bgr2Bgr);

            if (viewMat != null)
            {
                setBitmapInPictureBox(viewMat.Bitmap);
            }
        }
        private void buttonViewGradient_Click(object sender, EventArgs e)
        {
            if (_originalImage == null)
            {
                MessageBox.Show("Please load an image first!", "error");
                return;
            }
            viewMat = new Mat();

            //Mat hsv = ColorConversionWrapper.ColorConversions(_originalImage, ColorConversion.Bgr2Lab);
            //VectorOfMat hsvChannels = new VectorOfMat();
            //CvInvoke.Split(hsv, hsvChannels);
            //Mat result = ColorConversionWrapper.GradientMagnitude(hsvChannels[0]);

            createViewMat(ColorConversionWrapper.ManualConversions.Gradient);

            if (viewMat != null)
            {
                setBitmapInPictureBox(viewMat.Bitmap);
            }
        }
        private void buttonMultiGrad_Click(object sender, EventArgs e)
        {
            if (_originalImage == null)
            {
                MessageBox.Show("Please load an image first!", "error");
                return;
            }
            viewMat = new Mat();

            createViewMat(ColorConversionWrapper.ManualConversions.MGradient);

            if (viewMat != null)
            {
                setBitmapInPictureBox(viewMat.Bitmap);
            }
        }

        private void buttonSobelMultiGrad_Click(object sender, EventArgs e)
        {
            if (_originalImage == null)
            {
                MessageBox.Show("Please load an image first!", "error");
                return;
            }
            viewMat = new Mat();

            createViewMat(ColorConversionWrapper.ManualConversions.MSobelGradient);

            if (viewMat != null)
            {
                //ImageProcessingUtils.FilterOneLine(ref viewMat, 1.414, 1.414, ImageProcessingUtils.FilterGaussianC);
                setBitmapInPictureBox(viewMat.Bitmap);
            }
        }
        private void buttonViewPill_Click(object sender, EventArgs e)
        {
            if (_originalImage == null)
            {
                MessageBox.Show("Please load an image first!", "error");
                return;
            }
            viewMat = new Mat();

            createViewMat(ColorConversionWrapper.ManualConversions.Pii);

            if (viewMat != null)
            {
                setBitmapInPictureBox(viewMat.Bitmap);
            }
            
        }
        private void buttonRevert_Click(object sender, EventArgs e)
        {
            viewMat = null;
            if (_originalImage != null)
                setBitmapInPictureBox(_originalImage.Bitmap);
        }
        #endregion

        #region Data Loading
        private void loadSingleImageToolStripMenuItem_Click(object sender, EventArgs e) // load the original image
        {
            String fileName = FormUtils.ParametrizedOpenFileDialog(Constants.IMAGE_FILE_FILTER, false, "Load the original image")[0];
            if (fileName == null) return;

            if (_originalImage != null)
            {
                _originalImage.Dispose();
                if (viewMat != null) viewMat.Dispose();
                GC.Collect();
            }
            _originalImage = new Image<Bgr, Byte>(fileName);

            viewMat = _originalImage.GetInputArray().GetMat().Clone();
            _viewedImage = viewMat.Bitmap;
            pictureBoxMain.Image = _viewedImage;
            zoomScale = 1.0d;
            rects = null;
            
        }
        private void loadGroundTruthFilesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            String fileName = FormUtils.ParametrizedOpenFileDialog(Constants.TEXT_FILE_FILTER, false, "Load the ground truth file")[0];
            if (fileName == null) return;
        
            if (_originalImage != null)
            {
                Image<Bgr, byte> tmpImg = getImageToDraw(false);
                List<Rectangle> gtRects = IOUtils.LoadFileToRectList(fileName);

                for (int i = 0; i < gtRects.Count; i++)
                {
                    tmpImg.Draw(gtRects[i], new Bgr(Color.FromArgb(40, Color.Aqua)), 1);
                    tmpImg.Draw((i).ToString(), new Point(gtRects[i].X, gtRects[i].Bottom), FontFace.HersheyPlain, 1d, new Bgr(Color.Black));
                }
                viewMat = tmpImg.GetInputArray().GetMat().Clone();
                setBitmapInPictureBox(viewMat.Bitmap);
                tmpImg.Dispose();
            }
        }
        private void buttonChannels_Click(object sender, EventArgs e)
        {
            using (ChannelSetForm csf = new ChannelSetForm(flagColorSpace, flagChannels, flagGradients, flagGrouping))
            {
                if (csf.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    flagColorSpace = csf.flagColorSpace;
                    flagChannels = csf.flagChannels;
                    flagGradients = csf.flagGradients;
                    flagGrouping = csf.flagGrouping;
                    channelsSet = true;
                    if (flagChannels.HasFlag(Constants.VisChannels.Channel1Opp) || flagChannels.HasFlag(Constants.VisChannels.Channel2Opp) || flagChannels.HasFlag(Constants.VisChannels.Channel3Opp) || flagChannels.HasFlag(Constants.VisChannels.Channel4Opp))
                        hasOpposites = true;
                }
            }
        }
        private void rectanglesFromChannelsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            using (RectangleChooseForm rcf = new RectangleChooseForm(flagChannels, flagsVis))
            {
                if (rcf.ShowDialog() == DialogResult.OK)
                {
                    flagsVis = rcf.visFlags;
                }
                if (rects != null)
                {
                    VisualizeXXChannel(rects, flagChannels, flagsVis, globShowCounter);
                }
            }
        }
        private void chooseFeatureTypeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            using (ChooseFeaturesForm cff = new ChooseFeaturesForm(flagFeatureType))
                if (cff.ShowDialog() == DialogResult.OK)
                {
                    flagFeatureType = cff.featureType;
                }
        }
        private void createDatasetsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CropperForm cf = new CropperForm();
            cf.Show();
        }
        private void calculateDatasetStatisticsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            StatisticsForm sf = new StatisticsForm();
            sf.Show();
        }
        private void createModelsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ModelCreatorForm mcf = new ModelCreatorForm();
            mcf.Show();
        }
        private void workerFormToolStripMenuItem_Click(object sender, EventArgs e)
        {
            WorkersForm wf = new WorkersForm();
            wf.Show();         
        }
        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            DialogResult res = MessageBox.Show("Are you sure you want to exit?", "Is this goodbye?", MessageBoxButtons.YesNo);
            if (res == System.Windows.Forms.DialogResult.Yes)
            {
                this.Close();
            }
        }
        private void aboutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendLine("WordLocRec")
              .AppendLine()
              .AppendLine("Part of the Master Thesis of Georgios Kapidis.")
              .AppendLine(".Net Framework.")
              .AppendLine("Using Emgu CV v 3.0 for image manipulations.")
              .AppendLine("2016 All Rights Reserved")
              .AppendLine("DB had fun making this!");
            MessageBox.Show(sb.ToString(), "About us");
        }
        public bool ParsePropsInput()
        {
            int thresholdDelta;
            float minArea, maxArea, minProb, minProbDiff;

            if (!Int32.TryParse(textBoxThreshD.Text, out thresholdDelta) || (!float.TryParse(textBoxMinAr.Text, out minArea)) || (!float.TryParse(textBoxMaxAr.Text, out maxArea)) || (!float.TryParse(textBoxMinProb.Text, out minProb)) || (!float.TryParse(textBoxMinProbDiff.Text, out minProbDiff)))
            {
                MessageBox.Show("Couldn't parse ER extraction properties");
                return false;
            }
            else
            {
                if (thresholdDelta > 0 && thresholdDelta < 128 && minArea > 0 && minArea < maxArea && maxArea > minArea && maxArea < 1 && minProb >= 0 && minProb <= 1 && minProbDiff >= 0 && minProbDiff < 1)
                {
                    erprops = new ERFilterProps(thresholdDelta, minArea, maxArea, minProb, minProbDiff);
                    return true;
                }
                else
                {
                    MessageBox.Show("Wrong values in ER extraction properties");
                    return false;
                }
            }
        }
        #endregion

        #region CheckboxLogic

        private void checkBoxCH1_CheckedChanged(object sender, EventArgs e)
        {
            FormUtils.CheckBoxesMutuallyExclusive(checkBoxCH1, checkBoxCH2, checkBoxCH3, checkBoxCHAll);
        }

        private void checkBoxCH2_CheckedChanged(object sender, EventArgs e)
        {
            FormUtils.CheckBoxesMutuallyExclusive(checkBoxCH2, checkBoxCH1, checkBoxCH3, checkBoxCHAll);
        }

        private void checkBoxCH3_CheckedChanged(object sender, EventArgs e)
        {
            FormUtils.CheckBoxesMutuallyExclusive(checkBoxCH3, checkBoxCH1, checkBoxCH2, checkBoxCHAll);
        }

        private void checkBoxCHAll_CheckedChanged(object sender, EventArgs e)
        {
            FormUtils.CheckBoxesMutuallyExclusive(checkBoxCHAll, checkBoxCH1, checkBoxCH2, checkBoxCH3);
        }

        private void checkBoxSmooth_CheckedChanged(object sender, EventArgs e)
        {
            checkBoxGS.Enabled = checkBoxSmooth.Checked;
            checkBoxWiener.Enabled = checkBoxSmooth.Checked;
            numericUpDown1.Enabled = checkBoxSmooth.Checked;
            numericUpDown2.Enabled = checkBoxSmooth.Checked;
        }

        private void checkBoxGS_CheckedChanged(object sender, EventArgs e)
        {
            FormUtils.CheckBoxesMutuallyExclusive(checkBoxGS, checkBoxWiener);
        }

        private void checkBoxWiener_CheckedChanged(object sender, EventArgs e)
        {
            FormUtils.CheckBoxesMutuallyExclusive(checkBoxWiener, checkBoxGS);
        }

        private void checkBoxGammaCorrect_CheckedChanged(object sender, EventArgs e)
        {
            numericUpDownGamma.Enabled = checkBoxGammaCorrect.Checked;
        }

        private void checkBoxNMCanny_CheckedChanged(object sender, EventArgs e)
        {
            checkBoxCannyGrouping.Enabled = checkBoxCannyGrouping.Checked;
        }

        private void checkBoxNMS_CheckedChanged(object sender, EventArgs e)
        {
            radioButtonStrictNMS.Enabled = checkBoxNMS.Checked;
            radioButtonNormal.Enabled = checkBoxNMS.Checked;
        }
        #endregion

        # region Form Methods
        private Image<Bgr, byte> getImageToDraw(bool removeRects) // use only when the image is to draw because this method causes memory leaks
        {
            if (removeRects)
            {
                createViewMat(lastConversion);
            }
            Image<Bgr, byte> tmpImg;

            if (viewMat != null && viewMat.Width > 0)
            {
                if (viewMat.NumberOfChannels == 1)
                {
                    Mat tempColor = new Mat();
                    ColorConversionWrapper.ColorConversions(viewMat, ColorConversion.Gray2Bgr, ref tempColor);
                    viewMat = tempColor.Clone();
                    tempColor.Dispose();
                }
            }
            else // viewMat is empty or null or disposed
            {
                viewMat = _originalImage.GetInputArray().GetMat();
            }
            tmpImg = new Image<Bgr, byte>(viewMat.Bitmap);
            return tmpImg;
        }
        private void getChannels(ref VectorOfMat channels, IInputArray inputColorImage)
        {
            Mat mImage = inputColorImage.GetInputArray().GetMat();
            if (!channelsSet)
            {
                flagColorSpace = Constants.ColorSpace.HSV;
                flagChannels = Constants.VisChannels.AllChannels;
                flagGradients = Constants.Gradients.Single;
            }
            Utils.CreateXXChannels(ref channels, mImage, flagColorSpace, flagChannels, false, flagGradients);
            mImage.Dispose();
        }

        private void VisualizeXXChannel(List<Rectangle[]> rects, Constants.VisChannels existFlags, Constants.VisChannels visFlags, Boolean showCounter)
        {
            globShowCounter = showCounter; // assign the value in case I want to change shown channels
            Image<Bgr, byte> tmpImg = getImageToDraw(true);

            int rectThichness = 2;
            Color mColor;
            if (rects != null) // the rects to be drawn on the image
            { // tempRects' order is the same as they are created so e.g. HSV has always H first, H' 2nd, S 3rd, S' 4th etc. unless any channel is missing, in which case the list of arrays is compacted
                List<Rectangle[]> tempRects = new List<Rectangle[]>(rects);
                if (existFlags.HasFlag(Constants.VisChannels.Channel1)) // if the channel was used for er extraction
                {
                    if (visFlags.HasFlag(Constants.VisChannels.Channel1)) // if the user wants to see the rects from this channel
                    {
                        mColor = Utils.PickColor(0);
                        int counter = -1;
                        foreach (Rectangle rect in tempRects[0])
                        {
                            tmpImg.Draw(rect, new Bgr(Color.FromArgb(40, mColor)), rectThichness);
                            if (showCounter)
                                tmpImg.Draw((++counter).ToString(), new Point(rect.X, rect.Bottom), FontFace.HersheyPlain, 1d, new Bgr(Color.Black));
                        }

                    }
                    tempRects.RemoveAt(0); 
                    // remove the first array of rects because it is either drawn at the image or the user doesnt want it
                    // the user is not allowed by the ui to try to view a channel that wasnt extracted
                    // so this fifo drawing method works
                }
                if (existFlags.HasFlag(Constants.VisChannels.Channel1Opp))
                {
                    if (visFlags.HasFlag(Constants.VisChannels.Channel1Opp))
                    {
                        mColor = Utils.PickColor(4);
                        int counter = -1;
                        foreach (Rectangle rect in tempRects[0])
                        {
                            tmpImg.Draw(rect, new Bgr(Color.FromArgb(40, mColor)), rectThichness);
                            if (showCounter)
                                tmpImg.Draw((++counter).ToString(), new Point(rect.X, rect.Bottom), FontFace.HersheyPlain, 1d, new Bgr(Color.Black));
                        }
                    }
                    tempRects.RemoveAt(0);
                }
                if (existFlags.HasFlag(Constants.VisChannels.Channel2))
                {
                    if (visFlags.HasFlag(Constants.VisChannels.Channel2))
                    {
                        mColor = Utils.PickColor(1);
                        int counter = -1;
                        foreach (Rectangle rect in tempRects[0])
                        {
                            tmpImg.Draw(rect, new Bgr(Color.FromArgb(40, mColor)), rectThichness);
                            if (showCounter)
                                tmpImg.Draw((++counter).ToString(), new Point(rect.X, rect.Bottom), FontFace.HersheyPlain, 1d, new Bgr(Color.Black));
                        }
                    }
                    tempRects.RemoveAt(0);
                }
                if (existFlags.HasFlag(Constants.VisChannels.Channel2Opp))
                {
                    if (visFlags.HasFlag(Constants.VisChannels.Channel2Opp))
                    {
                        mColor = Utils.PickColor(5);
                        int counter = -1;
                        foreach (Rectangle rect in tempRects[0])
                        {
                            tmpImg.Draw(rect, new Bgr(Color.FromArgb(40, mColor)), rectThichness);
                            if (showCounter)
                                tmpImg.Draw((++counter).ToString(), new Point(rect.X, rect.Bottom), FontFace.HersheyPlain, 1d, new Bgr(Color.Black));
                        }
                    }
                    tempRects.RemoveAt(0);
                }
                if (existFlags.HasFlag(Constants.VisChannels.Channel3))
                {
                    if (visFlags.HasFlag(Constants.VisChannels.Channel3))
                    {
                        mColor = Utils.PickColor(2);
                        int counter = -1, side = 0;
                        foreach (Rectangle rect in tempRects[0])
                        {
                            side = counter % 2 == 0 ? 0 : 25;
                            tmpImg.Draw(rect, new Bgr(Color.FromArgb(40, mColor)), rectThichness);
                            if (showCounter)
                                tmpImg.Draw((++counter).ToString(), new Point(rect.X + side, rect.Bottom), FontFace.HersheyPlain, 1d, new Bgr(Color.Black));
                        }
                    }
                    tempRects.RemoveAt(0);
                }
                if (existFlags.HasFlag(Constants.VisChannels.Channel3Opp))
                {
                    if (visFlags.HasFlag(Constants.VisChannels.Channel3Opp))
                    {
                        mColor = Utils.PickColor(6);
                        int counter = -1;
                        foreach (Rectangle rect in tempRects[0])
                        {
                            tmpImg.Draw(rect, new Bgr(Color.FromArgb(40, mColor)), rectThichness);
                            if (showCounter)
                            {
                                ++counter;
                                tmpImg.Draw((counter).ToString(), new Point(rect.X, rect.Bottom), FontFace.HersheyPlain, 1d, new Bgr(Color.Black));
                            }

                        }
                    }
                    tempRects.RemoveAt(0);
                }
                if (existFlags.HasFlag(Constants.VisChannels.Channel4))
                {
                    if (visFlags.HasFlag(Constants.VisChannels.Channel4))
                    {
                        mColor = Utils.PickColor(3);
                        int counter = -1;
                        foreach (Rectangle rect in tempRects[0])
                        {
                            tmpImg.Draw(rect, new Bgr(Color.FromArgb(40, mColor)), rectThichness);
                            if (showCounter)
                                tmpImg.Draw((++counter).ToString(), new Point(rect.X, rect.Bottom), FontFace.HersheyPlain, 1d, new Bgr(Color.Black));
                        }
                    }
                    tempRects.RemoveAt(0);
                }
                if (existFlags.HasFlag(Constants.VisChannels.Channel4Opp))
                {
                    if (visFlags.HasFlag(Constants.VisChannels.Channel4Opp))
                    {
                        mColor = Utils.PickColor(7);
                        int counter = -1, side = 0;
                        foreach (Rectangle rect in tempRects[0])
                        {
                            side = counter % 2 == 0 ? 0 : 25;
                            tmpImg.Draw(rect, new Bgr(Color.FromArgb(40, mColor)), rectThichness);
                            if (showCounter)
                                tmpImg.Draw((++counter).ToString(), new Point(rect.X + side, rect.Bottom), FontFace.HersheyPlain, 1d, new Bgr(Color.Black));
                        }
                    }
                    tempRects.RemoveAt(0);
                } 
            }
            this.Text = (existFlags & visFlags).ToString();

            viewMat = tmpImg.GetInputArray().GetMat().Clone();
            setBitmapInPictureBox(viewMat.Bitmap);
            tmpImg.Dispose();
        }       
        private void VisualizeAllChannelsTogether(IEnumerable<IEnumerable<Rectangle>> recties, bool showCounter, int rectThickness)
        {
            Image<Bgr, byte> tmpImg = getImageToDraw(true);
            int counter = -1;
            Color c = Color.FromArgb(40, Color.Red);
            Bgr color = new Bgr(c);
            foreach (IEnumerable<Rectangle> rects in recties)
            foreach (Rectangle rect in rects)
            {
                tmpImg.Draw(rect, color, rectThickness);
                if (showCounter)
                    tmpImg.Draw((++counter).ToString(), new Point(rect.X, rect.Bottom), FontFace.HersheyPlain, 1, new Bgr(Color.Black));
            }           
            this.Text = "All available channels together";

            viewMat = tmpImg.GetInputArray().GetMat().Clone();
            setBitmapInPictureBox(viewMat.Bitmap);
            tmpImg.Dispose();
        }

        private void setBitmapInPictureBox(Bitmap bitmap)
        {
            if (_viewedImage != null)
            {
                _viewedImage.Dispose();
            }
            _viewedImage = null;
            _viewedImage = bitmap;
            pictureBoxMain.Image = _viewedImage;
            GC.Collect();
        }

        private void JobsDone()
        {
            if (globalWatch.IsRunning)
            {
                globalWatch.Stop();
            }
            TimeSpan ts = globalWatch.Elapsed;
            String elapsedTime = String.Format("{0::00}:{1:00}:{2:00}.{3:00}", ts.Hours, ts.Minutes, ts.Seconds, ts.Milliseconds / 10);
            String toString = "Job's done!" + " Elapsed time: " + elapsedTime;
            setLabelStatusText(toString);
        }
        private void setLabelStatusText(String text)
        {
            labelStatus.Text = text;
            //labelStatus.Refresh();
        }

        private void ImagePreprocessing(ref Mat mImage)
        {
            if (checkBoxGammaCorrect.Checked)
            {
                ImageProcessingUtils.CorrectionOneLine(ref mImage, (double)numericUpDownGamma.Value);
            }
            if (checkBoxSmooth.Checked)
            {
                ImageProcessingUtils.FilterOneLine(ref mImage, (int)numericUpDown1.Value, (int)numericUpDown2.Value, checkBoxGS.Checked ? (ImageProcessingUtils.FilteringFunction)ImageProcessingUtils.FilterGaussianC : (ImageProcessingUtils.FilteringFunction)ImageProcessingUtils.FilterWienerC);
            }
        }
        private bool LoadWordClassifierAndFeatures(out String classifierPath,  out Constants.WordFeatureType wordFeature, String title)
        {
            bool outcome = false;
            using (ChooseWordFeaturesForm cwff = new ChooseWordFeaturesForm(0))
            {
                if (cwff.ShowDialog() == DialogResult.OK)
                {
                    wordFeature = cwff.wordFeatureType;
                    if (LoadErClassifierPath(out classifierPath, title)) outcome = true;
                }
                else
                {
                    wordFeature = 0;
                    classifierPath = "";
                }
            }
            return outcome;
        }
        private bool LoadClassifierAndFeatures(out String classifierPath, out Constants.FeatureType features, String title)
        {
            bool outcome = false;
            using (ChooseFeaturesForm cff = new ChooseFeaturesForm(0))
            {
                if (cff.ShowDialog() == DialogResult.OK)
                {
                    features = cff.featureType;
                    if (LoadErClassifierPath(out classifierPath, title)) outcome = true;
                }
                else
                {
                    features = 0;
                    classifierPath = "";
                }
            }
            return outcome;

        }
        private bool LoadErClassifierPath(out String classifierPath, String title)
        {
            bool outcome = false;
            classifierPath = FormUtils.ParametrizedOpenFileDialog(Constants.XML_FILE_FILTER, false, title, true)[0];
            if (classifierPath != null)
            {
                outcome = true;
            }
            else
            {
                classifierPath = "";
            }
            return outcome;
        }
        private bool LoadBothClassifierPaths(out String classifier1Path, out String classifier2Path)
        {
            bool outcome;
            if (checkBoxClassifiers.Checked)
            {
                classifier1Path = Constants.CLASSIFIER1PATH;
                classifier2Path = Constants.CLASSIFIER2PATH;
                outcome = true;
            }
            else
            {
                outcome = LoadErClassifierPath(out classifier1Path, "Please select the first classifier.") & LoadErClassifierPath(out classifier2Path, "Please select the second classifier");
            }
            return outcome;
        }
        private bool InitializeErFilter(out ERFilterNM1 er1, out ERFilterNM2 er2)
        {
            String classifier1Path, classifier2Path;
            if (!LoadBothClassifierPaths(out classifier1Path, out classifier2Path) || !ParsePropsInput())
            {
                er1 = null; er2 = null; return false;
            }

            InitializeWatchAndLabel();

            er1 = new ERFilterNM1(classifier1Path, erprops.thresholdDelta, erprops.minArea, erprops.maxArea, erprops.minProbability, erprops.nonMaxSuppression, erprops.minProbabilityDifference);
            er2 = new ERFilterNM2(classifier2Path, 0.3f);

            return true;
        }
        private void InitializeWatchAndLabel()
        {
            setLabelStatusText("Serious computations");
            if (!globalWatch.IsRunning)
            {
                globalWatch.Reset();
                globalWatch.Start();
            }
        }
        #endregion
       
        #region ER-MSER Buttons
        private void buttonMSER_Click(object sender, EventArgs e) //mser-native no grouping // no leaks
        {
            if (_originalImage == null)
            {
                MessageBox.Show("Please load an image first!", "error");
                return;
            }
            Mat grayscale = new Mat();
            ColorConversionWrapper.ColorConversions(_originalImage, ColorConversion.Bgr2Gray, ref grayscale);
            MKeyPoint[] mserKP = MSERWrapper.DetectMSERs(grayscale);

            List<Rectangle> rectangles = MSERWrapper.ExtractRectsFromKeyPoints(mserKP);

            Image<Bgr, byte> tempImg = _originalImage.Clone();

            foreach (Rectangle rect in rectangles)
            {
                tempImg.Draw(rect, new Bgr(Color.FromArgb(40, Color.Blue)), 1);
            }
            setBitmapInPictureBox(tempImg.Bitmap);
        }      
        private void buttonERNative_Click(object sender, EventArgs e) //er-native w grouping
        {
            if (!AssertOriginalImage()) return;

            ERFilterNM1 er1;
            ERFilterNM2 er2;
            if (!InitializeErFilter(out er1, out er2)) return;

            Mat mImage = _originalImage.GetInputArray().GetMat().Clone();
            ImagePreprocessing(ref mImage);

            //VectorOfMat pyrs = new VectorOfMat();
            //Utils.ApplyPyramidDown(mImage, ref pyrs, 2);
            
            VectorOfMat channels = new VectorOfMat();          
            getChannels(ref channels, mImage);

            StringBuilder sb = new StringBuilder();
            sb.AppendLine("Counting ERs").AppendLine("NonMaxSuppression: " + checkBoxNMS);

            VectorOfERStat[] regions = new VectorOfERStat[channels.Size];
            for (int i = 0; i < channels.Size; i++)
            {
                sb.AppendLine("ERs for channel: " + i);
                regions[i] = new VectorOfERStat();
                er1.Run(channels[i], regions[i]);
                sb.AppendLine("After filter 1: " + regions[i].Size);
                if (checkBox2ndClassifier.Checked)
                {
                    er2.Run(channels[i], regions[i]);
                    sb.AppendLine("After filter 2: " + regions[i].Size); 
                }
            }

            if (checkBoxGrouping.Checked)
            {
                rects = GroupingUtils.RectangleGroupingPerChannel(mImage, ref channels, regions, hasOpposites, flagGrouping);
                sb.AppendLine();
                for (int i = 0; i < channels.Size; i++)
                    sb.AppendLine("Found " + rects[i].Length + " word rectangles in channel " + i);
            }   
            else
            {
                if (checkBoxWordPruning.Checked) // hidden functionality
                {
                    rects = new List<Rectangle[]>();
                    rects.Add(GroupingUtils.ERStatsToRecties(regions).ToArray());
                }
                rects = GroupingUtils.ERStatsToRects(regions); // rects = ungrouped Rects Per Channel
            }

            if (checkBox2ndStage.Checked)
            {
                // TODO: add 2nd stage
            }

            globalWatch.Stop();

            if (checkBoxWordPruning.Checked)
            {
                VisualizeAllChannelsTogether(rects, false, 1);
            }
            else // normal visualization
            {
                VisualizeXXChannel(rects, flagChannels, flagsVis, false);
            }

            er1.Dispose();
            er2.Dispose();
            Disposer.DisposeMatVectorOfMatRegions(ref mImage, ref channels);
            
            textBoxERData.Text = sb.ToString();
            JobsDone();
        }

        private void TestMyNM_Click(object sender, EventArgs e) //er-translated grouping option in code
        {
            if (!AssertOriginalImage()) return;

            Mat mImage = _originalImage.GetInputArray().GetMat().Clone();
            ImagePreprocessing(ref mImage);

            String classifier1Path, classifier2Path;
            if (!LoadBothClassifierPaths(out classifier1Path, out classifier2Path)) return;
            if (!ParsePropsInput()) return;

            InitializeWatchAndLabel();            

            //Stopwatch f2Watch = new Stopwatch(), f1Watch = new Stopwatch(), groupWatch = new Stopwatch();

            bool nonMaxSuppression = checkBoxNMS.Checked;
            // Create a filter for each of the classification stages
            MyERFilter filter1 = MyERFilterNM.createERFilterNM1(MyERFilterNM.loadClassifierNM1(classifier1Path),
                                                                erprops.thresholdDelta,
                                                                erprops.minArea,
                                                                erprops.maxArea,
                                                                erprops.minProbability,
                                                                nonMaxSuppression,
                                                                erprops.minProbabilityDifference);
            MyERFilter filter2 = MyERFilterNM.createERFilterNM2(MyERFilterNM.loadClassifierNM2(classifier2Path), 0.3f);

            StringBuilder sb = new StringBuilder();
            sb.AppendLine("Counting ERs").AppendLine("NonMaxSuppression: " + nonMaxSuppression);

            VectorOfMat channels = new VectorOfMat();
            getChannels(ref channels, mImage);

            List<ERStat>[] regions = new List<ERStat>[channels.Size];

            for (int i = 0; i < channels.Size; i++)
            {
                sb.AppendLine("ERs for channel: " + i);
                regions[i] = new List<ERStat>();

                //f1Watch.Start();
                filter1.Run(channels[i].GetInputArray(), ref regions[i]);
                //f1Watch.Stop();
                sb.AppendLine("After filter 1: " + regions[i].Count);

                if (checkBox2ndClassifier.Checked)
                {
                    //f2Watch.Start();
                    filter2.Run(channels[i].GetInputArray(), ref regions[i]);
                    //f2Watch.Stop();
                    sb.AppendLine("After filter 2: " + regions[i].Count);
                }
                FormUtils.VisualizeErPixels(channels[i], regions[i]);
            }
            
            if (checkBoxGrouping.Checked)
            {
                rects = GroupingUtils.RectangleGroupingPerChannel(mImage, ref channels, regions, false, hasOpposites, checkBoxFeedbackGrouping.Checked, out erListPerWordRect, out linesPerChannel, flagGrouping); // dont include strict grouping
                sb.AppendLine();
                for (int i = 0; i < channels.Size; i++)
                    sb.AppendLine("Found " + rects[i].Length + " word rectangles in channel " + i);
            }
            else
            {
                if (checkBoxWordPruning.Checked) // hidden functionality
                {
                    rects = new List<Rectangle[]>();
                    rects.Add(GroupingUtils.ERStatsToRecties(regions).ToArray());
                }
                rects = GroupingUtils.ERStatsToRects(regions); // rects = ungrouped Rects Per Channel
            }

            if (checkBox2ndStage.Checked)
            {
                // TODO: add 2nd stage
            }

            globalWatch.Stop();

            if (checkBoxWordPruning.Checked)
            {
                VisualizeAllChannelsTogether(rects, false, 1);
            }
            else // normal visualization
            {
                VisualizeXXChannel(rects, flagChannels, flagsVis, false);
            }

            //Debug.WriteLine("*****Global Watches*****");
            //Debug.WriteLine("Filter1 Watch: " + f1Watch.Elapsed);
            //Debug.WriteLine("Filter2 Watch: " + f2Watch.Elapsed);
            //Debug.WriteLine("Grouping Watch:" + groupWatch.Elapsed);
            //Debug.WriteLine("Global watch: " + globalWatch.Elapsed);
            //Debug.WriteLine("~~~~~Execution finished~~~~~");

            textBoxERData.Text = sb.ToString();
            JobsDone();
        }
        #region Hidden from UI
        private void buttonHybrid1_Click(object sender, EventArgs e) // native er extract - translated grouping
        {
            if (!AssertOriginalImage()) return;

            ERFilterNM1 er1;
            ERFilterNM2 er2;
            if (!InitializeErFilter(out er1, out er2)) return;

            Mat mImage = _originalImage.GetInputArray().GetMat().Clone();
            ImagePreprocessing(ref mImage);

            VectorOfMat channels = new VectorOfMat();
            getChannels(ref channels, mImage);

            VectorOfERStat[] regions = new VectorOfERStat[channels.Size];
            for (int i = 0; i < channels.Size; i++)
            {
                regions[i] = new VectorOfERStat();
                er1.Run(channels[i], regions[i]);
                er2.Run(channels[i], regions[i]);
            }

            //transform VectorOfERStat[] regions to List<ERStat>()
            List<ERStat>[] mRegions = new List<ERStat>[channels.Size];
            List<Rectangle> ungroupedRectsList = new List<Rectangle>();
            for (int i = 0; i < channels.Size; i++)
            {
                mRegions[i] = new List<ERStat>();
                for (int j = 0; j < regions[i].Size; j++)
                {
                    mRegions[i].Add(ERStat.TransformMCvERStat(regions[i][j]));
                    ungroupedRectsList.Add(regions[i][j].Rect);
                }
            }

            //***apply grouping below***
            bool secondStage = checkBox2ndStage.Checked == true? true: false;

            if (secondStage)
            {
                List<List<Tuple<int, int>>> groups;
                List<Rectangle> rectangles;
                Rectangle[] rects;
                if (checkBoxNMS.Checked)
                {
                    rects = ERGroupingNM.DoERGroupingNMModified(mImage.GetInputArray(), channels, mRegions, out groups, out rectangles, true);
                }
                else
                {
                    rects = ERGroupingNM.DoERGroupingNM(mImage.GetInputArray(), channels, mRegions, out groups, out rectangles, true);
                }             
                Rectangle[] rects1, ungroupedRects;
                rects = GroupingUtils.SouloupwseRects(rects, _originalImage.Rows, _originalImage.Cols);
                ungroupedRects = GroupingUtils.SouloupwseRects(ungroupedRectsList.ToArray(), _originalImage.Rows, _originalImage.Cols);

                byte[,] ungroupedHeatMap = GroupingUtils.CreateHeatMap(ungroupedRects, _originalImage.Rows, _originalImage.Cols);
                rects1 = GroupingUtils.UniteRects(rects, ungroupedHeatMap, _originalImage.Rows, _originalImage.Cols);


                if (checkBox2ndClassifier.Checked && ungroupedHeatMap != null)
                {
                    Matrix<byte> uhm = new Matrix<byte>(ungroupedHeatMap);
                    VectorOfMat uhmVec = new VectorOfMat();
                    uhmVec.Push(uhm.GetInputArray().GetMat());
                    new WordRectangleForm(uhmVec, "UngroupedHeatMap").Show();
                }
                Image<Bgr, byte> tmpImg = getImageToDraw(true);
                int counter = 0;
                foreach (Rectangle rect in rects1)
                {
                    tmpImg.Draw(rect, new Bgr(Color.FromArgb(40, Color.Blue)), 1);
                    tmpImg.Draw(counter.ToString(), new Point(rect.X, rect.Bottom), FontFace.HersheyPlain, 1d, new Bgr(Color.Black));
                    counter++;
                }

                viewMat = tmpImg.GetInputArray().GetMat().Clone();
                setBitmapInPictureBox(viewMat.Bitmap);
                tmpImg.Dispose();
            }
            else // normal grouping and viewing
            {
                rects = GroupingUtils.RectangleGroupingPerChannel(_originalImage, ref channels, mRegions, checkBoxNMS.Checked, hasOpposites, checkBoxFeedbackGrouping.Checked, out erListPerWordRect, out linesPerChannel, flagGrouping);

                VisualizeXXChannel(rects, flagChannels, flagsVis, true);
            }
            
            //**grouping from all the channel rectangles together**//
            //List<List<Tuple<int, int>>> groups;
            //List<Rectangle> rectangles;
            //Rectangle[] rects = ERGroupingNM.DoMultiChannelERGNM(_originalImage.GetInputArray(), channels, mRegions, out groups, out rectangles, true);
            //rectsOfH = rects;
            //rectsOfH = new List<Rectangle[]>();
            //for (int i = 0; i < mRegions.Length; i++)
            //{
            //    //rects.Add(new Rectangle[mRegions[i].Count]);
            //    rectsOfH.Add(new Rectangle[mRegions[i].Count]);
            //    for (int j = 0; j < mRegions[i].Count; j++)
            //    {
            //        //rects[i][j] = mRegions[i][j].rect;
            //        rectsOfH[i][j] = mRegions[i][j].rect;
            //    }
            //}
            //CompareRectangles(rectsOfT, rectsOfH);
            // ***END GROUPING***

            JobsDone();
        }
        
        private void buttonCanny_Click(object sender, EventArgs e) // add canny before native er
        {
            if (_originalImage == null)
            {
                MessageBox.Show("Please load an image first!", "error");
                return;
            }
            Mat grayscale = new Mat();
            ColorConversionWrapper.ColorConversions(_originalImage, ColorConversion.Bgr2Gray, ref grayscale);

            Mat edges = new Mat();
            CannyWrapper.CannyAutoThreshMean(grayscale, ref edges);

            //*****//
            if (checkBoxNMCanny.Checked)
            {
                VectorOfMat channels = new VectorOfMat();
                channels.Push(edges);
                int size = channels.Size;
                for (int i = 0; i < size; i++)
                {
                    Mat mat = new Mat();
                    CvInvoke.BitwiseNot(channels[i], mat);
                    channels.Push(mat);
                }

                if (!ParsePropsInput()) return;

                String classifier1Path = Constants.CLASSIFIER1PATH;
                String classifier2Path = Constants.CLASSIFIER2PATH;

                ERFilterNM1 er1 = new ERFilterNM1(classifier1Path, erprops.thresholdDelta, erprops.minArea, erprops.maxArea, erprops.minProbability, erprops.nonMaxSuppression, erprops.minProbabilityDifference);
                ERFilterNM2 er2 = new ERFilterNM2(classifier2Path);
                VectorOfERStat[] regions = new VectorOfERStat[channels.Size];
                for (int i = 0; i < channels.Size; i++)
                {
                    regions[i] = new VectorOfERStat();
                    er1.Run(channels[i], regions[i]);
                    er2.Run(channels[i], regions[i]);
                }

                Rectangle[] rects;
                if (checkBoxCannyGrouping.Checked)
                {
                    rects = ERFilter.ERGrouping(_originalImage, channels, regions, ERFilter.GroupingMethod.OrientationHoriz, null, 0.7f);
                }
                else
                {
                    List<Rectangle> rects1 = new List<Rectangle>();
                    for (int i = 0; i < regions.Length; i++)
                        for (int j = 0; j < regions[i].Size; j++)
                        {
                            MCvERStat item = regions[i][j];
                            rects1.Add(item.Rect);
                        }
                    rects = rects1.ToArray();
                }

                Image<Bgr, Byte> imgWithRects = _originalImage.Clone();
                foreach (Rectangle rect in rects)
                {
                    imgWithRects.Draw(rect, new Bgr(Color.FromArgb(40, Color.Blue)), 1);
                }
                setBitmapInPictureBox(imgWithRects.Bitmap);
                channels.Dispose();
            }
            //*****//
            else
            {
                setBitmapInPictureBox(edges.Bitmap);
            }
            grayscale.Dispose();
            edges.Dispose();
        }

        #endregion
        private void buttonCER_Click(object sender, EventArgs e) //cers own button !!
        {
            if (!AssertOriginalImage()) return;

            Mat mImage = _originalImage.GetInputArray().GetMat().Clone();
            ImagePreprocessing(ref mImage);

            InitializeWatchAndLabel();

            StringBuilder sb = new StringBuilder();
            if (!ParsePropsInput()) return;

            bool nonMaxSuppression = checkBoxNMS.Checked;
            bool filter2allowed = checkBox2ndClassifier.Checked;
            // Create a filter for each of the classification stages
            CERFilter filter1 = CERFilter.createERFilterNM1(CERFilter.loadClassifierNM1(Constants.CLASSIFIER1PATH),
                                                                erprops.thresholdDelta,
                                                                erprops.minArea,
                                                                erprops.maxArea,
                                                                erprops.minProbability,
                                                                nonMaxSuppression,
                                                                erprops.minProbabilityDifference);
            CERFilter filter2 = CERFilter.createERFilterNM2(CERFilter.loadClassifierNM2(Constants.CLASSIFIER2PATH), 0.3f);
            sb.AppendLine("Counting CERs").AppendLine("NonMaxSuppression: " + nonMaxSuppression);

            VectorOfMat channels = new VectorOfMat();
            getChannels(ref channels, mImage);

            List<ERStat>[] regions = new List<ERStat>[channels.Size];

            for (int i = 0; i < channels.Size; i++)
            {
                sb.AppendLine("ERs for channel: " + i);
                regions[i] = new List<ERStat>();

                filter1.Run(channels[i].GetInputArray(), ref regions[i]);
                sb.AppendLine("After filter 1: " + regions[i].Count);

                if (filter2allowed)
                {
                    filter2.Run(channels[i].GetInputArray(), ref regions[i]);
                    sb.AppendLine("After filter 2: " + regions[i].Count);
                }
            }
            if (checkBoxGrouping.Checked)
            {
                if (flagGrouping.HasFlag(Constants.GroupingMethod.Karatzas))
                {
                    VectorOfERStat[] stats = new VectorOfERStat[regions.Length];
                    GroupingUtils.ArrayListErstatsToArrVector(ref stats, regions);
                    rects = GroupingUtils.RectangleGroupingPerChannel(mImage, ref channels, stats, hasOpposites, flagGrouping);
                }
                else
                {
                    rects = GroupingUtils.RectangleGroupingPerChannel(mImage, ref channels, regions, false, hasOpposites, checkBoxFeedbackGrouping.Checked, out erListPerWordRect, out linesPerChannel, flagGrouping);
                }
            }
            else
            {
                rects = GroupingUtils.ERStatsToRects(regions);
            }

            if (checkBox2ndStage.Checked)
            {
                // TODO: add 2nd stage
            }

            globalWatch.Stop();
            VisualizeXXChannel(rects, flagChannels, flagsVis, false);

            textBoxERData.Text = sb.ToString();
            JobsDone();
        }
        private void buttonERExtractor_Click(object sender, EventArgs e)
        {
            if (!AssertOriginalImage()) return;
           
            String classifierPath, classifier2Path = "", classifier3Path = "";
            Constants.FeatureType f1, f2 = 0;
            Constants.WordFeatureType wordFeature = 0;
            if (checkBoxClassifiers.Checked)
            {
                classifierPath = Constants.COLOR_DISTANCE_CLASSIFIER;
                classifier2Path = Constants.SVM_NM;
                classifier3Path = Constants.RF_WORDSTAT;
                f1 = Constants.FeatureType.ColorDistLab | Constants.FeatureType.ColorDistPii | Constants.FeatureType.StandarizedPerFeature;
                f2 = Constants.FeatureType.NM;
                wordFeature = Constants.WordFeatureType.WordStat;
            }
            else
            {
                if (!LoadClassifierAndFeatures(out classifierPath, out f1, "Load an appropriate classifier")) return;
                if (checkBox2ndClassifier.Checked)
                {
                    if (!LoadClassifierAndFeatures(out classifier2Path, out f2, "Load an appropriate classifier")) return;
                }
                if (checkBoxWordPruning.Checked)
                if (!LoadWordClassifierAndFeatures(out classifier3Path, out wordFeature, "Load an appropriate word classifier")) return;
            }
            if (!ParsePropsInput()) return;

            StringBuilder sb = new StringBuilder();

            InitializeWatchAndLabel();
            Mat mImage = _originalImage.GetInputArray().GetMat().Clone();

            //CvInvoke.Resize(mImage, mImage, new Size((int)(0.5 * mImage.Width), (int)(0.5 * mImage.Height)));

            ImagePreprocessing(ref mImage);
            //VectorOfMat pyrs = new VectorOfMat();
            //Utils.ApplyPyramidDown(mImage, ref pyrs, 2);

            VectorOfMat channels = new VectorOfMat();
            getChannels(ref channels, mImage);
          
            ERExtractor.ExtractorParameters ep = new ERExtractor.ExtractorParameters(erprops.thresholdDelta, erprops.minArea, erprops.maxArea, 0.5, 0, mImage.Cols, mImage.Rows);
            ERExtractor ere = new ERExtractor(ep, ERUtils.SizeRestrictionsArea);
            List<ERStat>[] regions = new List<ERStat>[channels.Size];
            List<ERStat>[] removedRegions = new List<ERStat>[channels.Size];

            bool oppositeChannel = false; float probability = erprops.minProbability; bool feedbackGrouping = checkBoxFeedbackGrouping.Checked; bool keepERPixels = f1.HasFlag(Constants.FeatureType.ColorConsistencyLab) || f2.HasFlag(Constants.FeatureType.ColorConsistencyLab);
            for (int i = 0; i < channels.Size; i++)
            {
                sb.AppendLine("Channel: " + i);
                regions[i] = ere.extractERs(channels[i], keepERPixels, false);
                sb.AppendLine("Extracted " + regions[i].Count + " ERs");
                regions[i] = RecursiveFunctions.NonCerSuppression(regions[i]);
                sb.AppendLine("After cer " + regions[i].Count);
                regions[i] = RecursiveFunctions.NearDuplicateSuppression(regions[i], true);
                sb.AppendLine("After nd suppression: " + regions[i].Count);
                
                if (flagChannels.HasFlag(Constants.VisChannels.Channel1Opp) || flagChannels.HasFlag(Constants.VisChannels.Channel2Opp) || flagChannels.HasFlag(Constants.VisChannels.Channel3Opp) || flagChannels.HasFlag(Constants.VisChannels.Channel4Opp))
                {
                    oppositeChannel = i % 2 == 1 ? true : false;// the odd numbered channels are the opposites
                }
                regions[i] = ERUtils.TwoClassSvmPruning(channels[i], mImage, regions[i], classifierPath, oppositeChannel, f1);

                regions[i] = RecursiveFunctions.SuppressWrong(regions[i], RecursiveFunctions.erProbabilityPruning, probability);
                sb.AppendLine("After svm with prob " + probability + " regs: " + regions[i].Count);

                removedRegions[i] = new List<ERStat>(regions[i]);
                if (checkBox2ndClassifier.Checked)
                {
                    regions[i] = ERUtils.TwoClassSvmPruning(channels[i], mImage, regions[i], classifier2Path, oppositeChannel, f2);
                    removedRegions[i] = RecursiveFunctions.SuppressCorrect(removedRegions[i], RecursiveFunctions.erResponsePruningOpposite);
                    regions[i] = RecursiveFunctions.SuppressWrong(regions[i], RecursiveFunctions.erResponsePruning);
                    
                    sb.AppendLine("After 2nd response " + regions[i].Count);
                }
                if (checkBoxNMS.Checked)
                {
                    RecursiveFunctions.PruneDesicitonFunction pdf;
                    if (radioButtonStrictNMS.Checked) pdf = RecursiveFunctions.erSaveNMSOnlyStrict;
                    else pdf = RecursiveFunctions.erSaveNMSOnly;
                    //FormUtils.VisualizeErPixels(channels[i], regions[i]);
                    regions[i] = RecursiveFunctions.FillRateSuppression(regions[i]);
                    //FormUtils.VisualizeErPixels(channels[i], regions[i]);
                    regions[i] = RecursiveFunctions.SuppressNonMaximal(regions[i], pdf);
                    sb.AppendLine("After nms " + regions[i].Count);
                    removedRegions[i] = RecursiveFunctions.FillRateSuppression(removedRegions[i]);
                    removedRegions[i] = RecursiveFunctions.SuppressNonMaximal(removedRegions[i], pdf);
                }
                //Mat lab = new Mat(); //CvInvoke.CvtColor(mImage, lab, ColorConversion.Bgr2Lab);
                //ColorConversionWrapper.ColorConversions(mImage, ColorConversionWrapper.ManualConversions.Pii, ref lab);
                //CvInvoke.BitwiseNot(lab, lab);
                //VectorOfMat vLab = new VectorOfMat(); CvInvoke.Split(lab, vLab);
                //for (int j = 0; j < vLab.Size; j++)
                //    FormUtils.VisualizeErRects(vLab[j], regions[i], j);
                //FormUtils.VisualizeErPixels(channels[i], regions[i]);
            }
            if (checkBoxGrouping.Checked && checkBoxGrouping.CheckState == CheckState.Checked)
            {
                rects = GroupingUtils.RectangleGroupingPerChannel(mImage, ref channels, regions, false, hasOpposites, checkBoxFeedbackGrouping.Checked, out erListPerWordRect, out linesPerChannel, flagGrouping, removedRegions);
                sb.AppendLine();
                if (checkBox2ndStage.Checked)
                    sb.AppendLine("Found in total " + rects[0].Length + " word rectangles.");
                else
                    for (int i = 0; i < channels.Size; i++)
                        sb.AppendLine("Found " + rects[i].Length + " word rectangles in channel " + i);
            }
            else if (checkBoxGrouping.Checked && checkBoxGrouping.CheckState == CheckState.Indeterminate)
            {
                rects = GroupingUtils.ERRectanglesPerChannel(mImage, channels, regions, removedRegions);
                sb.AppendLine();
                for (int i = 0; i < channels.Size; i++)
                    sb.AppendLine("Found " + rects[i].Length + " ers as word parts in channel " + i);
            }
            else
            {
                rects = GroupingUtils.ERStatsToRects(regions); // rects = ungrouped Rects Per Channel
            }
            List<List<List<ERStat>>> prunedErListPerWordRect = new List<List<List<ERStat>>>();
            if (checkBoxGrouping.Checked && checkBoxGrouping.CheckState == CheckState.Checked && checkBoxWordPruning.Checked)
            {
                globalWatch.Stop();
                IStatModel model = SvmUtils.DefineTypeOfClassifier(classifier3Path);
                globalWatch.Start();
                BaseFeature[] wordStats;
                for (int i = 0; i < channels.Size; i++)
                {
                    prunedErListPerWordRect.Add(new List<List<ERStat>>());
                    List<Tuple<List<ERStat>, int>> erListPerWordRectPerChannel = new List<Tuple<List<ERStat>, int>>();
                    for (int j = 0; j < erListPerWordRect.Count; j++)
                        if (erListPerWordRect[j].Item2 == i) erListPerWordRectPerChannel.Add(erListPerWordRect[j]);

                    Rectangle[] prunedRects = new Rectangle[0];
                    if (wordFeature.HasFlag(Constants.WordFeatureType.WordStat))
                    {
                        wordStats = WordStat.CreateMultiple(rects[i], erListPerWordRectPerChannel, mImage, channels, "");
                        prunedRects = ERUtils.WordPruning<SVMWordStatExample>(wordStats, model, rects[i], true, wordFeature.HasFlag(Constants.WordFeatureType.Standardized) ? Constants.FeatureType.StandarizedPerFeature : 0);
                    }
                    else if (wordFeature.HasFlag(Constants.WordFeatureType.SimpleWordStat))
                    {
                        wordStats = SimpleWordStat.CreateMultiple(rects[i], erListPerWordRectPerChannel, mImage, channels, "");
                        prunedRects = ERUtils.WordPruning<SVMSimpleWordStatExample>(wordStats, model, rects[i], true, wordFeature.HasFlag(Constants.WordFeatureType.Standardized) ? Constants.FeatureType.StandarizedPerFeature : 0);
                    }
                    else
                        if (flagGrouping.HasFlag(Constants.GroupingMethod.chinese))
                        {
                            wordStats = LineWordStat.CreateMultiple(rects[i], linesPerChannel[i], erListPerWordRectPerChannel, mImage, channels, "");
                            prunedRects = ERUtils.WordPruning<SVMLineWordStatSample>(wordStats, model, rects[i], true, wordFeature.HasFlag(Constants.WordFeatureType.Standardized) ? Constants.FeatureType.StandarizedPerFeature : 0);
                        }
                        else
                            throw new Exception("Unsupported combination: " + flagGrouping + "  " + wordFeature);
                    
                    for (int j = 0; j < prunedRects.Length; j++) // psaxnei ta enapomeinanta
                    {
                        for (int k = 0; k < rects[i].Length; k++) // gia na ta vrei sta rects
                        {
                            if (prunedRects[j] == rects[i][k]) // an ta vrei pairnei th lista me ta er kai th vazei sto pruned gia na ta xrhsimopoihsei sto combined meta
                            {
                                prunedErListPerWordRect[i].Add(erListPerWordRectPerChannel[k].Item1);
                            }
                        }
                    }
                    rects[i] = prunedRects;
                }
            }
            else if (checkBoxGrouping.Checked && checkBoxGrouping.CheckState == CheckState.Checked && !checkBoxWordPruning.Checked)
            {
                prunedErListPerWordRect.Add(new List<List<ERStat>>());
                if (erListPerWordRect.Count > 0)
                    prunedErListPerWordRect[prunedErListPerWordRect.Count - 1].Add(erListPerWordRect[0].Item1);
                for (int i = 1; i < erListPerWordRect.Count; i++)
                {
                    if (erListPerWordRect[i - 1].Item2 != erListPerWordRect[i].Item2) prunedErListPerWordRect.Add(new List<List<ERStat>>());
                    prunedErListPerWordRect[prunedErListPerWordRect.Count - 1].Add(erListPerWordRect[i].Item1);            
                }
            }

            if (checkBoxGrouping.Checked && checkBoxGrouping.CheckState == CheckState.Checked && checkBox2ndStage.Checked)
            {
                List<Rectangle[]> rectsComb = new List<Rectangle[]>();
                rectsComb.Add(GroupingUtils.CombineChannelRectangles(rects, prunedErListPerWordRect).ToArray());
                VisualizeAllChannelsTogether(rectsComb, false, 2);
            }
            else
            {
                VisualizeXXChannel(rects, flagChannels, flagsVis, false);
            }

            globalWatch.Stop();

            textBoxERData.Text = sb.ToString();
            JobsDone();

            Disposer.DisposeMatVectorOfMatRegions(ref mImage, ref channels, ref regions);
        }
        #endregion

        private void button1_Click(object sender, EventArgs e) // temp button
        {
            if (!AssertOriginalImage()) return;
            
            StringBuilder sb = new StringBuilder();
            String classifierPath, classifier2Path = "";
            Constants.FeatureType f1, f2 = 0;
            if (checkBoxClassifiers.Checked)
            {
                classifierPath = Constants.COLOR_DISTANCE_CLASSIFIER;
                classifier2Path = Constants.SVM_NM;
                //classifier2Path = Constants.BEST_NM_STANDARDISED;
                //classifier2Path = Constants.BEST_NM_CONV_STROKE_CLASSIFIER_PATH;
                f1 = Constants.FeatureType.ColorDistLab | Constants.FeatureType.ColorDistPii | Constants.FeatureType.StandarizedPerFeature;
                f2 = Constants.FeatureType.NM;
                //f2 = Constants.FeatureType.NM | Constants.FeatureType.StandarizedPerFeature;
                //f2 = Constants.FeatureType.NM | Constants.FeatureType.ConvexityDefects | Constants.FeatureType.StrokeRatio;
            }
            else
            {
                if (!LoadClassifierAndFeatures(out classifierPath, out f1, "Load an appropriate classifier")) return;
                if (!checkBox2ndClassifier.Checked)
                {
                    if (!LoadClassifierAndFeatures(out classifier2Path, out f2, "Load an appropriate classifier")) return;
                }
            }
            if (!ParsePropsInput()) return;

            InitializeWatchAndLabel();
            Mat mImage = _originalImage.GetInputArray().GetMat().Clone();
            ImagePreprocessing(ref mImage);
            //VectorOfMat pyrs = new VectorOfMat();
            //Utils.ApplyPyramidDown(mImage, ref pyrs, 2);

            VectorOfMat channels = new VectorOfMat();
            getChannels(ref channels, mImage);

            rects = Experiments.ViewLineERs(mImage, channels, f1, f2, erprops, flagGrouping);
            foreach (IEnumerable<Rectangle> r in rects)
            {
                foreach (Rectangle rect in r)
                    sb.AppendLine("Rectangle " + rect.ToString());
                sb.AppendLine("end channel");
            }

            VisualizeXXChannel(rects, flagChannels, flagsVis, false);
            
            textBoxERData.Text = sb.ToString();
            JobsDone();
        }                

        private void buttonLayers_Click(object sender, EventArgs e)
        {
            //Experiments.CheckFFO(FormUtils.ParametrizedOpenFileDialog(Constants.XML_FILE_FILTER, true, "load the ffo"));
            //erRetrievePositive();
            //erRetrieveDemo();

            //Experiments.ThinningDemo();
            //Experiments.ThinningAndPathsDemo();
            //Experiments.FindContours();
            //Experiments.RetrieveERsAndPathsVisual(ParametrizedOpenFileDialog(Constants.XML_FILE_FILTER, true, "Select the xmls"));
            
            //using (Mat temp = new Mat(ParametrizedOpenFileDialog(Constants.IMAGE_FILE_FILTER, false, "")[0], LoadImageType.Grayscale))
                //Experiments.CompareBinaryMats(temp);
            //Experiments.makeTree();
            
            //String classifierPath = "";
            //Constants.FeatureType f = 0;
            //if (!LoadClassifierAndFeatures(out classifierPath, out f, "Load an appropriate classifier")) return;
            //Experiments.EvaluateClassifier(FormUtils.ParametrizedOpenFileDialog(Constants.XML_FILE_FILTER, true, "Positive dataset"), FormUtils.ParametrizedOpenFileDialog(Constants.XML_FILE_FILTER, true, "Negative dataset"), f, classifierPath);

            //Experiments.WordsExtractorMultiThread(FormUtils.ParametrizedOpenFileDialog(Constants.IMAGE_FILE_FILTER, true, "Select the images"), FormUtils.ParametrizedFolderBrowserDialog("Select the output folder"), true);

            //Experiments.WordStatAllFeaturesExtractorMT(FormUtils.ParametrizedOpenFileDialog(Constants.IMAGE_FILE_FILTER, true, "Select the images"), FormUtils.ParametrizedFolderBrowserDialog("Select the output folder"));

            //Experiments.CheckWordStats(ParametrizedOpenFileDialog(Constants.XML_FILE_FILTER, true, "Select the word stats"));

            //Experiments.FixStatsFromLineGroupingMultiThread(FormUtils.ParametrizedOpenFileDialog(Constants.XML_FILE_FILTER, true, "Select the word stats"), FormUtils.ParametrizedOpenFileDialog(Constants.XML_FILE_FILTER, true, "Select the simple word stats"), FormUtils.ParametrizedOpenFileDialog(Constants.XML_FILE_FILTER, true, "Select the line word stats")); // - problhma me th mnhmh klasika
            //Experiments.FixStatsFromLineGrouping(FormUtils.ParametrizedOpenFileDialog(Constants.XML_FILE_FILTER, true, "Select the word stats"), FormUtils.ParametrizedOpenFileDialog(Constants.XML_FILE_FILTER, true, "Select the simple word stats"), FormUtils.ParametrizedOpenFileDialog(Constants.XML_FILE_FILTER, true, "Select the line word stats"));

            //Mat image = new Mat(FormUtils.ParametrizedOpenFileDialog(Constants.IMAGE_FILE_FILTER, false, "Select an image")[0], LoadImageType.Unchanged);
            //if (image.Rows == 0 || image.Cols == 0) return;
            //Utils.MatToBgrImageC(image, out _originalImage);
            //ImagePreprocessing(ref image);
            //Rectangle[] wordRects = Experiments.ExecuteOrder66<SVMLineWordStatSample, LineWordStat>(image, FormUtils.ParametrizedOpenFileDialog(Constants.XML_FILE_FILTER, false, "Select a karatza classifier")[0], 0 | Constants.FeatureType.StandarizedPerFeature);
            //VisualizeAllChannelsTogether(wordRects, true, 2);
        }      
    
        #region Data input assertions
        private bool AssertOriginalImage()
        {
            if (_originalImage == null)
            {
                MessageBox.Show("Please load an image first!", "Come on lad");
                return false;
            }
            return true;
        }
        private bool AssertFeatureTypeInitialized()
        {
            if (flagFeatureType == 0)
            {
                MessageBox.Show("Please define the features for the classifier", "Forgetfullness...");
                return false;
            }
            return true;
        }
        #endregion

        #region Methods that manipulate global variables AND aren't buttons TREAT WITH CARE

        /// <summary>
        /// A form method that accepts an image at a colorspace and fills the global parameter "viewMat" with the channels specified by the user.
        /// After this method viewMat may still be null if the gui checkboxes are all unchecked
        /// </summary>
        /// <param name="resultColorSpace"></param>
        private void CreateImageWithRequestedChannels(Mat result)
        {            
            if (checkBoxCHNot.Checked)
            {
                CvInvoke.BitwiseNot(result, result);
            }
            if (checkBoxCHAll.Checked)
            {
                viewMat = result.Clone();
            }
            else
            {
                if (result.NumberOfChannels > 1)
                {
                    VectorOfMat resultChannels = new VectorOfMat();
                    CvInvoke.Split(result, resultChannels);
                    if (checkBoxCH1.Checked)
                    {
                        viewMat = resultChannels[0].Clone();
                    }
                    else if (checkBoxCH2.Checked)
                    {
                        viewMat = resultChannels[1].Clone();
                    }
                    else if (checkBoxCH3.Checked)
                    {
                        viewMat = resultChannels[2].Clone();
                    }
                    else
                        viewMat = null;
                    resultChannels.Dispose();
                }
                else
                {
                    viewMat = null;
                } 
            }
            result.Dispose();
            return;
        }

        private void createViewMat(Enum code)
        {
            lastConversion = code;
            viewMat = new Mat();
            Mat mImage = _originalImage.GetInputOutputArray().GetMat().Clone();
            
            ImagePreprocessing(ref mImage);

            Mat result = new Mat();
            ColorConversionWrapper.ColorConversions(mImage, code, ref result);
            CreateImageWithRequestedChannels(result);
            mImage.Dispose();
            result.Dispose();
        }
        #endregion

        private void buttonThinningDemo_Click(object sender, EventArgs e)
        {
            if (!AssertOriginalImage()) return;

            Mat binary = new Mat();
            Utils.TurnToBinary(_originalImage, ref binary);

            PathStats pathStats = ERUtils.SignificantPaths(binary);

            setBitmapInPictureBox(binary.Bitmap);
            textBoxERData.Text = pathStats.ToString();
        }


    }
}
