﻿namespace WordLocRec.Forms
{
    partial class StatisticsForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.buttonLoad = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.checkBoxSaveMinMax = new System.Windows.Forms.CheckBox();
            this.checkBoxSaveCombOnly = new System.Windows.Forms.CheckBox();
            this.groupBoxFeatureType = new System.Windows.Forms.GroupBox();
            this.checkBoxFFO = new System.Windows.Forms.CheckBox();
            this.checkBoxLWS = new System.Windows.Forms.CheckBox();
            this.checkBoxSWS = new System.Windows.Forms.CheckBox();
            this.checkBoxWS = new System.Windows.Forms.CheckBox();
            this.buttonClear = new System.Windows.Forms.Button();
            this.buttonCalculate = new System.Windows.Forms.Button();
            this.buttonSaveStats = new System.Windows.Forms.Button();
            this.tabControl1.SuspendLayout();
            this.panel1.SuspendLayout();
            this.groupBoxFeatureType.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Location = new System.Drawing.Point(158, 0);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(820, 393);
            this.tabControl1.SizeMode = System.Windows.Forms.TabSizeMode.FillToRight;
            this.tabControl1.TabIndex = 12;
            // 
            // tabPage1
            // 
            this.tabPage1.AutoScroll = true;
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(812, 367);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Combined Results";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // buttonLoad
            // 
            this.buttonLoad.BackColor = System.Drawing.Color.LightSteelBlue;
            this.buttonLoad.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonLoad.Location = new System.Drawing.Point(9, 139);
            this.buttonLoad.Name = "buttonLoad";
            this.buttonLoad.Size = new System.Drawing.Size(112, 28);
            this.buttonLoad.TabIndex = 16;
            this.buttonLoad.Text = "Load a dataset";
            this.toolTip1.SetToolTip(this.buttonLoad, "Load dataset (Note that they are added on top of any previous datasets so take ca" +
        "re to use the same feature or press CLEAR)");
            this.buttonLoad.UseVisualStyleBackColor = false;
            this.buttonLoad.Click += new System.EventHandler(this.buttonLoad_Click);
            // 
            // panel1
            // 
            this.panel1.AutoScroll = true;
            this.panel1.Controls.Add(this.checkBoxSaveMinMax);
            this.panel1.Controls.Add(this.checkBoxSaveCombOnly);
            this.panel1.Controls.Add(this.groupBoxFeatureType);
            this.panel1.Controls.Add(this.buttonLoad);
            this.panel1.Controls.Add(this.buttonClear);
            this.panel1.Controls.Add(this.buttonCalculate);
            this.panel1.Controls.Add(this.buttonSaveStats);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(152, 394);
            this.panel1.TabIndex = 13;
            // 
            // checkBoxSaveMinMax
            // 
            this.checkBoxSaveMinMax.AutoSize = true;
            this.checkBoxSaveMinMax.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.checkBoxSaveMinMax.Location = new System.Drawing.Point(8, 367);
            this.checkBoxSaveMinMax.Name = "checkBoxSaveMinMax";
            this.checkBoxSaveMinMax.Size = new System.Drawing.Size(93, 17);
            this.checkBoxSaveMinMax.TabIndex = 22;
            this.checkBoxSaveMinMax.Text = "Save Min/Max";
            this.checkBoxSaveMinMax.UseVisualStyleBackColor = true;
            // 
            // checkBoxSaveCombOnly
            // 
            this.checkBoxSaveCombOnly.AutoSize = true;
            this.checkBoxSaveCombOnly.Checked = true;
            this.checkBoxSaveCombOnly.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxSaveCombOnly.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.checkBoxSaveCombOnly.Location = new System.Drawing.Point(8, 344);
            this.checkBoxSaveCombOnly.Name = "checkBoxSaveCombOnly";
            this.checkBoxSaveCombOnly.Size = new System.Drawing.Size(94, 17);
            this.checkBoxSaveCombOnly.TabIndex = 21;
            this.checkBoxSaveCombOnly.Text = "Combined Only";
            this.checkBoxSaveCombOnly.UseVisualStyleBackColor = true;
            // 
            // groupBoxFeatureType
            // 
            this.groupBoxFeatureType.Controls.Add(this.checkBoxFFO);
            this.groupBoxFeatureType.Controls.Add(this.checkBoxLWS);
            this.groupBoxFeatureType.Controls.Add(this.checkBoxSWS);
            this.groupBoxFeatureType.Controls.Add(this.checkBoxWS);
            this.groupBoxFeatureType.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.groupBoxFeatureType.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBoxFeatureType.Location = new System.Drawing.Point(3, 12);
            this.groupBoxFeatureType.Name = "groupBoxFeatureType";
            this.groupBoxFeatureType.Size = new System.Drawing.Size(128, 117);
            this.groupBoxFeatureType.TabIndex = 20;
            this.groupBoxFeatureType.TabStop = false;
            this.groupBoxFeatureType.Text = "FeatureType";
            // 
            // checkBoxFFO
            // 
            this.checkBoxFFO.AutoSize = true;
            this.checkBoxFFO.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.checkBoxFFO.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBoxFFO.Location = new System.Drawing.Point(6, 90);
            this.checkBoxFFO.Name = "checkBoxFFO";
            this.checkBoxFFO.Size = new System.Drawing.Size(112, 17);
            this.checkBoxFFO.TabIndex = 16;
            this.checkBoxFFO.Text = "Full Feature Object";
            this.checkBoxFFO.UseVisualStyleBackColor = true;
            this.checkBoxFFO.CheckedChanged += new System.EventHandler(this.checkBoxFFO_CheckedChanged);
            // 
            // checkBoxLWS
            // 
            this.checkBoxLWS.AutoSize = true;
            this.checkBoxLWS.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.checkBoxLWS.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBoxLWS.Location = new System.Drawing.Point(6, 67);
            this.checkBoxLWS.Name = "checkBoxLWS";
            this.checkBoxLWS.Size = new System.Drawing.Size(88, 17);
            this.checkBoxLWS.TabIndex = 15;
            this.checkBoxLWS.Text = "LineWordStat";
            this.checkBoxLWS.UseVisualStyleBackColor = true;
            this.checkBoxLWS.CheckedChanged += new System.EventHandler(this.checkBoxLWS_CheckedChanged);
            // 
            // checkBoxSWS
            // 
            this.checkBoxSWS.AutoSize = true;
            this.checkBoxSWS.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.checkBoxSWS.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBoxSWS.Location = new System.Drawing.Point(6, 44);
            this.checkBoxSWS.Name = "checkBoxSWS";
            this.checkBoxSWS.Size = new System.Drawing.Size(99, 17);
            this.checkBoxSWS.TabIndex = 14;
            this.checkBoxSWS.Text = "SimpleWordStat";
            this.checkBoxSWS.UseVisualStyleBackColor = true;
            this.checkBoxSWS.CheckedChanged += new System.EventHandler(this.checkBoxSWS_CheckedChanged);
            // 
            // checkBoxWS
            // 
            this.checkBoxWS.AutoSize = true;
            this.checkBoxWS.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.checkBoxWS.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBoxWS.Location = new System.Drawing.Point(6, 21);
            this.checkBoxWS.Name = "checkBoxWS";
            this.checkBoxWS.Size = new System.Drawing.Size(68, 17);
            this.checkBoxWS.TabIndex = 12;
            this.checkBoxWS.Text = "WordStat";
            this.checkBoxWS.UseVisualStyleBackColor = true;
            this.checkBoxWS.CheckedChanged += new System.EventHandler(this.checkBoxWS_CheckedChanged);
            // 
            // buttonClear
            // 
            this.buttonClear.BackColor = System.Drawing.Color.LightSteelBlue;
            this.buttonClear.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonClear.Location = new System.Drawing.Point(9, 173);
            this.buttonClear.Name = "buttonClear";
            this.buttonClear.Size = new System.Drawing.Size(112, 23);
            this.buttonClear.TabIndex = 17;
            this.buttonClear.Text = "Clear";
            this.buttonClear.UseVisualStyleBackColor = false;
            this.buttonClear.Click += new System.EventHandler(this.buttonClear_Click);
            // 
            // buttonCalculate
            // 
            this.buttonCalculate.BackColor = System.Drawing.Color.LightSteelBlue;
            this.buttonCalculate.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonCalculate.Location = new System.Drawing.Point(9, 224);
            this.buttonCalculate.Name = "buttonCalculate";
            this.buttonCalculate.Size = new System.Drawing.Size(112, 35);
            this.buttonCalculate.TabIndex = 18;
            this.buttonCalculate.Text = "Calculate statistics";
            this.buttonCalculate.UseVisualStyleBackColor = false;
            this.buttonCalculate.Click += new System.EventHandler(this.buttonCalculate_Click);
            // 
            // buttonSaveStats
            // 
            this.buttonSaveStats.BackColor = System.Drawing.Color.LightSteelBlue;
            this.buttonSaveStats.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonSaveStats.Location = new System.Drawing.Point(9, 315);
            this.buttonSaveStats.Name = "buttonSaveStats";
            this.buttonSaveStats.Size = new System.Drawing.Size(112, 23);
            this.buttonSaveStats.TabIndex = 19;
            this.buttonSaveStats.Text = "Save to files ...";
            this.buttonSaveStats.UseVisualStyleBackColor = false;
            this.buttonSaveStats.Click += new System.EventHandler(this.buttonSaveStats_Click);
            // 
            // StatisticsForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.GhostWhite;
            this.ClientSize = new System.Drawing.Size(990, 394);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.tabControl1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.SizableToolWindow;
            this.Name = "StatisticsForm";
            this.Text = "StatisticsForm";
            this.tabControl1.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.groupBoxFeatureType.ResumeLayout(false);
            this.groupBoxFeatureType.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.CheckBox checkBoxSaveMinMax;
        private System.Windows.Forms.CheckBox checkBoxSaveCombOnly;
        private System.Windows.Forms.GroupBox groupBoxFeatureType;
        private System.Windows.Forms.CheckBox checkBoxFFO;
        private System.Windows.Forms.CheckBox checkBoxLWS;
        private System.Windows.Forms.CheckBox checkBoxSWS;
        private System.Windows.Forms.CheckBox checkBoxWS;
        private System.Windows.Forms.Button buttonLoad;
        private System.Windows.Forms.Button buttonClear;
        private System.Windows.Forms.Button buttonCalculate;
        private System.Windows.Forms.Button buttonSaveStats;
    }
}