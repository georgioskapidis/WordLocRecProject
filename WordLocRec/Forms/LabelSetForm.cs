﻿using Emgu.CV;
using Emgu.CV.Structure;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WordLocRec
{
    public partial class LabelSetForm : Form
    {
        public int Label; // 1,2,3,4
        public LabelSetForm(Image<Bgr, Byte> image)
        {
            InitializeComponent();
            pictureBoxLabel.Image = image.Bitmap;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Label = 1;
            this.DialogResult = DialogResult.OK;
            this.Close();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Label = 2;
            this.DialogResult = DialogResult.OK;
            this.Close();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            Label = 3;
            this.DialogResult = DialogResult.OK;
            this.Close();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            Label = 4;
            this.DialogResult = DialogResult.OK;
            this.Close();
        }
    }
}
