﻿namespace WordLocRec
{
    partial class ChannelSetForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.checkBoxHSV = new System.Windows.Forms.CheckBox();
            this.checkBoxBGR = new System.Windows.Forms.CheckBox();
            this.checkBoxGM = new System.Windows.Forms.CheckBox();
            this.checkBoxOp = new System.Windows.Forms.CheckBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.checkBoxVal = new System.Windows.Forms.CheckBox();
            this.checkBoxSat = new System.Windows.Forms.CheckBox();
            this.checkBoxHue = new System.Windows.Forms.CheckBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.checkBoxRed = new System.Windows.Forms.CheckBox();
            this.checkBoxGreen = new System.Windows.Forms.CheckBox();
            this.checkBoxBlue = new System.Windows.Forms.CheckBox();
            this.buttonExit = new System.Windows.Forms.Button();
            this.checkBoxLab = new System.Windows.Forms.CheckBox();
            this.panel3 = new System.Windows.Forms.Panel();
            this.checkBoxB = new System.Windows.Forms.CheckBox();
            this.checkBoxA = new System.Windows.Forms.CheckBox();
            this.checkBoxLightness = new System.Windows.Forms.CheckBox();
            this.panel4 = new System.Windows.Forms.Panel();
            this.checkBoxZ = new System.Windows.Forms.CheckBox();
            this.checkBoxY = new System.Windows.Forms.CheckBox();
            this.checkBoxX = new System.Windows.Forms.CheckBox();
            this.checkBoxXYZ = new System.Windows.Forms.CheckBox();
            this.checkBoxGrayscale = new System.Windows.Forms.CheckBox();
            this.panel5 = new System.Windows.Forms.Panel();
            this.checkBoxPii3 = new System.Windows.Forms.CheckBox();
            this.checkBoxPii2 = new System.Windows.Forms.CheckBox();
            this.checkBoxPii1 = new System.Windows.Forms.CheckBox();
            this.checkBoxPii = new System.Windows.Forms.CheckBox();
            this.panelGradient = new System.Windows.Forms.Panel();
            this.radioButtonMultiSobel = new System.Windows.Forms.RadioButton();
            this.radioButtonMulti = new System.Windows.Forms.RadioButton();
            this.radioButtonSingle = new System.Windows.Forms.RadioButton();
            this.panelGrouping = new System.Windows.Forms.Panel();
            this.radioButtonChineseGrouping = new System.Windows.Forms.RadioButton();
            this.radioButtonGKGrouping = new System.Windows.Forms.RadioButton();
            this.radioButtonExhastiveSearch = new System.Windows.Forms.RadioButton();
            this.checkBoxDistances = new System.Windows.Forms.CheckBox();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel4.SuspendLayout();
            this.panel5.SuspendLayout();
            this.panelGradient.SuspendLayout();
            this.panelGrouping.SuspendLayout();
            this.SuspendLayout();
            // 
            // checkBoxHSV
            // 
            this.checkBoxHSV.AutoSize = true;
            this.checkBoxHSV.Checked = true;
            this.checkBoxHSV.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxHSV.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.checkBoxHSV.Location = new System.Drawing.Point(15, 12);
            this.checkBoxHSV.Name = "checkBoxHSV";
            this.checkBoxHSV.Size = new System.Drawing.Size(45, 17);
            this.checkBoxHSV.TabIndex = 0;
            this.checkBoxHSV.Text = "HSV";
            this.checkBoxHSV.UseVisualStyleBackColor = true;
            this.checkBoxHSV.CheckedChanged += new System.EventHandler(this.checkBoxHSV_CheckedChanged);
            // 
            // checkBoxBGR
            // 
            this.checkBoxBGR.AutoSize = true;
            this.checkBoxBGR.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.checkBoxBGR.Location = new System.Drawing.Point(111, 13);
            this.checkBoxBGR.Name = "checkBoxBGR";
            this.checkBoxBGR.Size = new System.Drawing.Size(46, 17);
            this.checkBoxBGR.TabIndex = 1;
            this.checkBoxBGR.Text = "BGR";
            this.checkBoxBGR.UseVisualStyleBackColor = true;
            this.checkBoxBGR.CheckedChanged += new System.EventHandler(this.checkBoxBGR_CheckedChanged);
            // 
            // checkBoxGM
            // 
            this.checkBoxGM.AutoSize = true;
            this.checkBoxGM.Checked = true;
            this.checkBoxGM.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxGM.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.checkBoxGM.Location = new System.Drawing.Point(15, 132);
            this.checkBoxGM.Name = "checkBoxGM";
            this.checkBoxGM.Size = new System.Drawing.Size(116, 17);
            this.checkBoxGM.TabIndex = 2;
            this.checkBoxGM.Text = "Gradient Magnitude";
            this.checkBoxGM.UseVisualStyleBackColor = true;
            this.checkBoxGM.CheckedChanged += new System.EventHandler(this.checkBoxGM_CheckedChanged);
            // 
            // checkBoxOp
            // 
            this.checkBoxOp.AutoSize = true;
            this.checkBoxOp.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.checkBoxOp.Location = new System.Drawing.Point(15, 286);
            this.checkBoxOp.Name = "checkBoxOp";
            this.checkBoxOp.Size = new System.Drawing.Size(76, 17);
            this.checkBoxOp.TabIndex = 3;
            this.checkBoxOp.Text = "Bitwise Not";
            this.checkBoxOp.UseVisualStyleBackColor = true;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.checkBoxVal);
            this.panel1.Controls.Add(this.checkBoxSat);
            this.panel1.Controls.Add(this.checkBoxHue);
            this.panel1.Location = new System.Drawing.Point(11, 36);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(90, 67);
            this.panel1.TabIndex = 4;
            // 
            // checkBoxVal
            // 
            this.checkBoxVal.AutoSize = true;
            this.checkBoxVal.Checked = true;
            this.checkBoxVal.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxVal.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.checkBoxVal.Location = new System.Drawing.Point(4, 50);
            this.checkBoxVal.Name = "checkBoxVal";
            this.checkBoxVal.Size = new System.Drawing.Size(50, 17);
            this.checkBoxVal.TabIndex = 2;
            this.checkBoxVal.Text = "Value";
            this.checkBoxVal.UseVisualStyleBackColor = true;
            // 
            // checkBoxSat
            // 
            this.checkBoxSat.AutoSize = true;
            this.checkBoxSat.Checked = true;
            this.checkBoxSat.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxSat.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.checkBoxSat.Location = new System.Drawing.Point(4, 27);
            this.checkBoxSat.Name = "checkBoxSat";
            this.checkBoxSat.Size = new System.Drawing.Size(71, 17);
            this.checkBoxSat.TabIndex = 1;
            this.checkBoxSat.Text = "Saturation";
            this.checkBoxSat.UseVisualStyleBackColor = true;
            // 
            // checkBoxHue
            // 
            this.checkBoxHue.AutoSize = true;
            this.checkBoxHue.Checked = true;
            this.checkBoxHue.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxHue.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.checkBoxHue.Location = new System.Drawing.Point(4, 4);
            this.checkBoxHue.Name = "checkBoxHue";
            this.checkBoxHue.Size = new System.Drawing.Size(43, 17);
            this.checkBoxHue.TabIndex = 0;
            this.checkBoxHue.Text = "Hue";
            this.checkBoxHue.UseVisualStyleBackColor = true;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.checkBoxRed);
            this.panel2.Controls.Add(this.checkBoxGreen);
            this.panel2.Controls.Add(this.checkBoxBlue);
            this.panel2.Location = new System.Drawing.Point(107, 36);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(81, 67);
            this.panel2.TabIndex = 5;
            // 
            // checkBoxRed
            // 
            this.checkBoxRed.AutoSize = true;
            this.checkBoxRed.Checked = true;
            this.checkBoxRed.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxRed.Enabled = false;
            this.checkBoxRed.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.checkBoxRed.Location = new System.Drawing.Point(4, 50);
            this.checkBoxRed.Name = "checkBoxRed";
            this.checkBoxRed.Size = new System.Drawing.Size(43, 17);
            this.checkBoxRed.TabIndex = 2;
            this.checkBoxRed.Text = "Red";
            this.checkBoxRed.UseVisualStyleBackColor = true;
            // 
            // checkBoxGreen
            // 
            this.checkBoxGreen.AutoSize = true;
            this.checkBoxGreen.Checked = true;
            this.checkBoxGreen.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxGreen.Enabled = false;
            this.checkBoxGreen.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.checkBoxGreen.Location = new System.Drawing.Point(4, 27);
            this.checkBoxGreen.Name = "checkBoxGreen";
            this.checkBoxGreen.Size = new System.Drawing.Size(52, 17);
            this.checkBoxGreen.TabIndex = 1;
            this.checkBoxGreen.Text = "Green";
            this.checkBoxGreen.UseVisualStyleBackColor = true;
            // 
            // checkBoxBlue
            // 
            this.checkBoxBlue.AutoSize = true;
            this.checkBoxBlue.Checked = true;
            this.checkBoxBlue.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxBlue.Enabled = false;
            this.checkBoxBlue.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.checkBoxBlue.Location = new System.Drawing.Point(4, 4);
            this.checkBoxBlue.Name = "checkBoxBlue";
            this.checkBoxBlue.Size = new System.Drawing.Size(44, 17);
            this.checkBoxBlue.TabIndex = 0;
            this.checkBoxBlue.Text = "Blue";
            this.checkBoxBlue.UseVisualStyleBackColor = true;
            // 
            // buttonExit
            // 
            this.buttonExit.BackColor = System.Drawing.Color.LightSteelBlue;
            this.buttonExit.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonExit.Location = new System.Drawing.Point(341, 280);
            this.buttonExit.Name = "buttonExit";
            this.buttonExit.Size = new System.Drawing.Size(119, 29);
            this.buttonExit.TabIndex = 6;
            this.buttonExit.Text = "Confirm Changes";
            this.buttonExit.UseVisualStyleBackColor = false;
            this.buttonExit.Click += new System.EventHandler(this.buttonExit_Click);
            // 
            // checkBoxLab
            // 
            this.checkBoxLab.AutoSize = true;
            this.checkBoxLab.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.checkBoxLab.Location = new System.Drawing.Point(198, 12);
            this.checkBoxLab.Name = "checkBoxLab";
            this.checkBoxLab.Size = new System.Drawing.Size(41, 17);
            this.checkBoxLab.TabIndex = 7;
            this.checkBoxLab.Text = "Lab";
            this.checkBoxLab.UseVisualStyleBackColor = true;
            this.checkBoxLab.CheckedChanged += new System.EventHandler(this.checkBoxLab_CheckedChanged);
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.checkBoxB);
            this.panel3.Controls.Add(this.checkBoxA);
            this.panel3.Controls.Add(this.checkBoxLightness);
            this.panel3.Location = new System.Drawing.Point(194, 36);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(77, 67);
            this.panel3.TabIndex = 6;
            // 
            // checkBoxB
            // 
            this.checkBoxB.AutoSize = true;
            this.checkBoxB.Checked = true;
            this.checkBoxB.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxB.Enabled = false;
            this.checkBoxB.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.checkBoxB.Location = new System.Drawing.Point(4, 50);
            this.checkBoxB.Name = "checkBoxB";
            this.checkBoxB.Size = new System.Drawing.Size(29, 17);
            this.checkBoxB.TabIndex = 2;
            this.checkBoxB.Text = "b";
            this.checkBoxB.UseVisualStyleBackColor = true;
            // 
            // checkBoxA
            // 
            this.checkBoxA.AutoSize = true;
            this.checkBoxA.Checked = true;
            this.checkBoxA.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxA.Enabled = false;
            this.checkBoxA.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.checkBoxA.Location = new System.Drawing.Point(4, 27);
            this.checkBoxA.Name = "checkBoxA";
            this.checkBoxA.Size = new System.Drawing.Size(29, 17);
            this.checkBoxA.TabIndex = 1;
            this.checkBoxA.Text = "a";
            this.checkBoxA.UseVisualStyleBackColor = true;
            // 
            // checkBoxLightness
            // 
            this.checkBoxLightness.AutoSize = true;
            this.checkBoxLightness.Checked = true;
            this.checkBoxLightness.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxLightness.Enabled = false;
            this.checkBoxLightness.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.checkBoxLightness.Location = new System.Drawing.Point(4, 4);
            this.checkBoxLightness.Name = "checkBoxLightness";
            this.checkBoxLightness.Size = new System.Drawing.Size(68, 17);
            this.checkBoxLightness.TabIndex = 0;
            this.checkBoxLightness.Text = "Lightness";
            this.checkBoxLightness.UseVisualStyleBackColor = true;
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.checkBoxZ);
            this.panel4.Controls.Add(this.checkBoxY);
            this.panel4.Controls.Add(this.checkBoxX);
            this.panel4.Location = new System.Drawing.Point(277, 36);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(72, 67);
            this.panel4.TabIndex = 8;
            // 
            // checkBoxZ
            // 
            this.checkBoxZ.AutoSize = true;
            this.checkBoxZ.Checked = true;
            this.checkBoxZ.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxZ.Enabled = false;
            this.checkBoxZ.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.checkBoxZ.Location = new System.Drawing.Point(4, 50);
            this.checkBoxZ.Name = "checkBoxZ";
            this.checkBoxZ.Size = new System.Drawing.Size(30, 17);
            this.checkBoxZ.TabIndex = 2;
            this.checkBoxZ.Text = "Z";
            this.checkBoxZ.UseVisualStyleBackColor = true;
            // 
            // checkBoxY
            // 
            this.checkBoxY.AutoSize = true;
            this.checkBoxY.Checked = true;
            this.checkBoxY.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxY.Enabled = false;
            this.checkBoxY.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.checkBoxY.Location = new System.Drawing.Point(4, 27);
            this.checkBoxY.Name = "checkBoxY";
            this.checkBoxY.Size = new System.Drawing.Size(30, 17);
            this.checkBoxY.TabIndex = 1;
            this.checkBoxY.Text = "Y";
            this.checkBoxY.UseVisualStyleBackColor = true;
            // 
            // checkBoxX
            // 
            this.checkBoxX.AutoSize = true;
            this.checkBoxX.Checked = true;
            this.checkBoxX.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxX.Enabled = false;
            this.checkBoxX.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.checkBoxX.Location = new System.Drawing.Point(4, 4);
            this.checkBoxX.Name = "checkBoxX";
            this.checkBoxX.Size = new System.Drawing.Size(30, 17);
            this.checkBoxX.TabIndex = 0;
            this.checkBoxX.Text = "X";
            this.checkBoxX.UseVisualStyleBackColor = true;
            // 
            // checkBoxXYZ
            // 
            this.checkBoxXYZ.AutoSize = true;
            this.checkBoxXYZ.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.checkBoxXYZ.Location = new System.Drawing.Point(281, 12);
            this.checkBoxXYZ.Name = "checkBoxXYZ";
            this.checkBoxXYZ.Size = new System.Drawing.Size(44, 17);
            this.checkBoxXYZ.TabIndex = 9;
            this.checkBoxXYZ.Text = "XYZ";
            this.checkBoxXYZ.UseVisualStyleBackColor = true;
            this.checkBoxXYZ.CheckedChanged += new System.EventHandler(this.checkBoxXYZ_CheckedChanged);
            // 
            // checkBoxGrayscale
            // 
            this.checkBoxGrayscale.AutoSize = true;
            this.checkBoxGrayscale.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.checkBoxGrayscale.Location = new System.Drawing.Point(15, 109);
            this.checkBoxGrayscale.Name = "checkBoxGrayscale";
            this.checkBoxGrayscale.Size = new System.Drawing.Size(70, 17);
            this.checkBoxGrayscale.TabIndex = 10;
            this.checkBoxGrayscale.Text = "Grayscale";
            this.checkBoxGrayscale.UseVisualStyleBackColor = true;
            this.checkBoxGrayscale.CheckedChanged += new System.EventHandler(this.checkBoxGrayscale_CheckedChanged);
            // 
            // panel5
            // 
            this.panel5.Controls.Add(this.checkBoxPii3);
            this.panel5.Controls.Add(this.checkBoxPii2);
            this.panel5.Controls.Add(this.checkBoxPii1);
            this.panel5.Location = new System.Drawing.Point(355, 36);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(72, 67);
            this.panel5.TabIndex = 11;
            // 
            // checkBoxPii3
            // 
            this.checkBoxPii3.AutoSize = true;
            this.checkBoxPii3.Checked = true;
            this.checkBoxPii3.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxPii3.Enabled = false;
            this.checkBoxPii3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.checkBoxPii3.Location = new System.Drawing.Point(4, 50);
            this.checkBoxPii3.Name = "checkBoxPii3";
            this.checkBoxPii3.Size = new System.Drawing.Size(40, 17);
            this.checkBoxPii3.TabIndex = 2;
            this.checkBoxPii3.Text = "Pii3";
            this.checkBoxPii3.UseVisualStyleBackColor = true;
            // 
            // checkBoxPii2
            // 
            this.checkBoxPii2.AutoSize = true;
            this.checkBoxPii2.Checked = true;
            this.checkBoxPii2.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxPii2.Enabled = false;
            this.checkBoxPii2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.checkBoxPii2.Location = new System.Drawing.Point(4, 27);
            this.checkBoxPii2.Name = "checkBoxPii2";
            this.checkBoxPii2.Size = new System.Drawing.Size(40, 17);
            this.checkBoxPii2.TabIndex = 1;
            this.checkBoxPii2.Text = "Pii2";
            this.checkBoxPii2.UseVisualStyleBackColor = true;
            // 
            // checkBoxPii1
            // 
            this.checkBoxPii1.AutoSize = true;
            this.checkBoxPii1.Checked = true;
            this.checkBoxPii1.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxPii1.Enabled = false;
            this.checkBoxPii1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.checkBoxPii1.Location = new System.Drawing.Point(4, 4);
            this.checkBoxPii1.Name = "checkBoxPii1";
            this.checkBoxPii1.Size = new System.Drawing.Size(40, 17);
            this.checkBoxPii1.TabIndex = 0;
            this.checkBoxPii1.Text = "Pii3";
            this.checkBoxPii1.UseVisualStyleBackColor = true;
            // 
            // checkBoxPii
            // 
            this.checkBoxPii.AutoSize = true;
            this.checkBoxPii.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.checkBoxPii.Location = new System.Drawing.Point(359, 12);
            this.checkBoxPii.Name = "checkBoxPii";
            this.checkBoxPii.Size = new System.Drawing.Size(34, 17);
            this.checkBoxPii.TabIndex = 12;
            this.checkBoxPii.Text = "Pii";
            this.checkBoxPii.UseVisualStyleBackColor = true;
            this.checkBoxPii.CheckedChanged += new System.EventHandler(this.checkBoxPii_CheckedChanged);
            // 
            // panelGradient
            // 
            this.panelGradient.Controls.Add(this.radioButtonMultiSobel);
            this.panelGradient.Controls.Add(this.radioButtonMulti);
            this.panelGradient.Controls.Add(this.radioButtonSingle);
            this.panelGradient.Location = new System.Drawing.Point(15, 155);
            this.panelGradient.Name = "panelGradient";
            this.panelGradient.Size = new System.Drawing.Size(119, 73);
            this.panelGradient.TabIndex = 12;
            // 
            // radioButtonMultiSobel
            // 
            this.radioButtonMultiSobel.AutoSize = true;
            this.radioButtonMultiSobel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.radioButtonMultiSobel.Location = new System.Drawing.Point(5, 50);
            this.radioButtonMultiSobel.Name = "radioButtonMultiSobel";
            this.radioButtonMultiSobel.Size = new System.Drawing.Size(112, 17);
            this.radioButtonMultiSobel.TabIndex = 2;
            this.radioButtonMultiSobel.Text = "MultiChannelSobel";
            this.radioButtonMultiSobel.UseVisualStyleBackColor = true;
            // 
            // radioButtonMulti
            // 
            this.radioButtonMulti.AutoSize = true;
            this.radioButtonMulti.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.radioButtonMulti.Location = new System.Drawing.Point(5, 27);
            this.radioButtonMulti.Name = "radioButtonMulti";
            this.radioButtonMulti.Size = new System.Drawing.Size(85, 17);
            this.radioButtonMulti.TabIndex = 1;
            this.radioButtonMulti.Text = "MultiChannel";
            this.radioButtonMulti.UseVisualStyleBackColor = true;
            // 
            // radioButtonSingle
            // 
            this.radioButtonSingle.AutoSize = true;
            this.radioButtonSingle.Checked = true;
            this.radioButtonSingle.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.radioButtonSingle.Location = new System.Drawing.Point(5, 4);
            this.radioButtonSingle.Name = "radioButtonSingle";
            this.radioButtonSingle.Size = new System.Drawing.Size(95, 17);
            this.radioButtonSingle.TabIndex = 0;
            this.radioButtonSingle.TabStop = true;
            this.radioButtonSingle.Text = "Single Channel";
            this.radioButtonSingle.UseVisualStyleBackColor = true;
            // 
            // panelGrouping
            // 
            this.panelGrouping.Controls.Add(this.radioButtonChineseGrouping);
            this.panelGrouping.Controls.Add(this.radioButtonGKGrouping);
            this.panelGrouping.Controls.Add(this.radioButtonExhastiveSearch);
            this.panelGrouping.Location = new System.Drawing.Point(341, 155);
            this.panelGrouping.Name = "panelGrouping";
            this.panelGrouping.Size = new System.Drawing.Size(119, 73);
            this.panelGrouping.TabIndex = 13;
            // 
            // radioButtonChineseGrouping
            // 
            this.radioButtonChineseGrouping.AutoSize = true;
            this.radioButtonChineseGrouping.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.radioButtonChineseGrouping.Location = new System.Drawing.Point(6, 50);
            this.radioButtonChineseGrouping.Name = "radioButtonChineseGrouping";
            this.radioButtonChineseGrouping.Size = new System.Drawing.Size(87, 17);
            this.radioButtonChineseGrouping.TabIndex = 2;
            this.radioButtonChineseGrouping.Text = "LineGrouping";
            this.radioButtonChineseGrouping.UseVisualStyleBackColor = true;
            // 
            // radioButtonGKGrouping
            // 
            this.radioButtonGKGrouping.AutoSize = true;
            this.radioButtonGKGrouping.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.radioButtonGKGrouping.Location = new System.Drawing.Point(6, 27);
            this.radioButtonGKGrouping.Name = "radioButtonGKGrouping";
            this.radioButtonGKGrouping.Size = new System.Drawing.Size(82, 17);
            this.radioButtonGKGrouping.TabIndex = 1;
            this.radioButtonGKGrouping.Text = "GKGrouping";
            this.radioButtonGKGrouping.UseVisualStyleBackColor = true;
            // 
            // radioButtonExhastiveSearch
            // 
            this.radioButtonExhastiveSearch.AutoSize = true;
            this.radioButtonExhastiveSearch.Checked = true;
            this.radioButtonExhastiveSearch.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.radioButtonExhastiveSearch.Location = new System.Drawing.Point(6, 4);
            this.radioButtonExhastiveSearch.Name = "radioButtonExhastiveSearch";
            this.radioButtonExhastiveSearch.Size = new System.Drawing.Size(104, 17);
            this.radioButtonExhastiveSearch.TabIndex = 0;
            this.radioButtonExhastiveSearch.TabStop = true;
            this.radioButtonExhastiveSearch.Text = "ExhastiveSearch";
            this.radioButtonExhastiveSearch.UseVisualStyleBackColor = true;
            // 
            // checkBoxDistances
            // 
            this.checkBoxDistances.AutoSize = true;
            this.checkBoxDistances.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.checkBoxDistances.Location = new System.Drawing.Point(111, 109);
            this.checkBoxDistances.Name = "checkBoxDistances";
            this.checkBoxDistances.Size = new System.Drawing.Size(70, 17);
            this.checkBoxDistances.TabIndex = 14;
            this.checkBoxDistances.Text = "Distances";
            this.checkBoxDistances.UseVisualStyleBackColor = true;
            this.checkBoxDistances.CheckedChanged += new System.EventHandler(this.checkBoxDistances_CheckedChanged);
            // 
            // ChannelSetForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(472, 321);
            this.Controls.Add(this.checkBoxDistances);
            this.Controls.Add(this.panelGrouping);
            this.Controls.Add(this.panelGradient);
            this.Controls.Add(this.panel5);
            this.Controls.Add(this.checkBoxPii);
            this.Controls.Add(this.checkBoxGrayscale);
            this.Controls.Add(this.panel4);
            this.Controls.Add(this.checkBoxXYZ);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.checkBoxLab);
            this.Controls.Add(this.buttonExit);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.checkBoxOp);
            this.Controls.Add(this.checkBoxGM);
            this.Controls.Add(this.checkBoxBGR);
            this.Controls.Add(this.checkBoxHSV);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.SizableToolWindow;
            this.Name = "ChannelSetForm";
            this.Text = "ChannelSetForm";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            this.panelGradient.ResumeLayout(false);
            this.panelGradient.PerformLayout();
            this.panelGrouping.ResumeLayout(false);
            this.panelGrouping.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.CheckBox checkBoxHSV;
        private System.Windows.Forms.CheckBox checkBoxBGR;
        private System.Windows.Forms.CheckBox checkBoxGM;
        private System.Windows.Forms.CheckBox checkBoxOp;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.CheckBox checkBoxVal;
        private System.Windows.Forms.CheckBox checkBoxSat;
        private System.Windows.Forms.CheckBox checkBoxHue;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.CheckBox checkBoxRed;
        private System.Windows.Forms.CheckBox checkBoxGreen;
        private System.Windows.Forms.CheckBox checkBoxBlue;
        private System.Windows.Forms.Button buttonExit;
        private System.Windows.Forms.CheckBox checkBoxLab;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.CheckBox checkBoxB;
        private System.Windows.Forms.CheckBox checkBoxA;
        private System.Windows.Forms.CheckBox checkBoxLightness;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.CheckBox checkBoxZ;
        private System.Windows.Forms.CheckBox checkBoxY;
        private System.Windows.Forms.CheckBox checkBoxX;
        private System.Windows.Forms.CheckBox checkBoxXYZ;
        private System.Windows.Forms.CheckBox checkBoxGrayscale;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.CheckBox checkBoxPii3;
        private System.Windows.Forms.CheckBox checkBoxPii2;
        private System.Windows.Forms.CheckBox checkBoxPii1;
        private System.Windows.Forms.CheckBox checkBoxPii;
        private System.Windows.Forms.Panel panelGradient;
        private System.Windows.Forms.RadioButton radioButtonMultiSobel;
        private System.Windows.Forms.RadioButton radioButtonMulti;
        private System.Windows.Forms.RadioButton radioButtonSingle;
        private System.Windows.Forms.Panel panelGrouping;
        private System.Windows.Forms.RadioButton radioButtonGKGrouping;
        private System.Windows.Forms.RadioButton radioButtonExhastiveSearch;
        private System.Windows.Forms.CheckBox checkBoxDistances;
        private System.Windows.Forms.RadioButton radioButtonChineseGrouping;

    }
}