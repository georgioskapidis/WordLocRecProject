﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WordLocRec.Forms;

namespace WordLocRec
{
    public partial class ChannelSetForm : Form
    {
        public Constants.ColorSpace flagColorSpace = Constants.ColorSpace.None;
        public Constants.VisChannels flagChannels = Constants.VisChannels.None;
        public Constants.Gradients flagGradients = Constants.Gradients.None;
        public Constants.GroupingMethod flagGrouping = Constants.GroupingMethod.ExhaustiveSearch;
        public ChannelSetForm()
        {
            InitializeComponent();
        }
        public ChannelSetForm(Constants.ColorSpace oldColorSpace, Constants.VisChannels oldChannels, Constants.Gradients oldGradients, Constants.GroupingMethod oldGrouping) : this()
        {
            switch (oldColorSpace)
            {
                case Constants.ColorSpace.BGR:
                    {
                        checkBoxBGR.Checked = true;
                        checkBoxBlue.Checked = oldChannels.HasFlag(Constants.VisChannels.Channel1);
                        checkBoxGreen.Checked = oldChannels.HasFlag(Constants.VisChannels.Channel2);
                        checkBoxRed.Checked = oldChannels.HasFlag(Constants.VisChannels.Channel3);
                        break;
                    }
                case Constants.ColorSpace.HSV:
                    {
                        checkBoxHSV.Checked = true;
                        checkBoxHue.Checked = oldChannels.HasFlag(Constants.VisChannels.Channel1);
                        checkBoxSat.Checked = oldChannels.HasFlag(Constants.VisChannels.Channel2);
                        checkBoxVal.Checked = oldChannels.HasFlag(Constants.VisChannels.Channel3);
                        break;
                    }
                case Constants.ColorSpace.LAB:
                    {
                        checkBoxLab.Checked = true;
                        checkBoxLightness.Checked = oldChannels.HasFlag(Constants.VisChannels.Channel1);
                        checkBoxA.Checked = oldChannels.HasFlag(Constants.VisChannels.Channel2);
                        checkBoxB.Checked = oldChannels.HasFlag(Constants.VisChannels.Channel3);
                        break;
                    }
                case Constants.ColorSpace.Pii:
                    {
                        checkBoxPii.Checked = true;
                        checkBoxPii1.Checked = oldChannels.HasFlag(Constants.VisChannels.Channel1);
                        checkBoxPii2.Checked = oldChannels.HasFlag(Constants.VisChannels.Channel2);
                        checkBoxPii3.Checked = oldChannels.HasFlag(Constants.VisChannels.Channel3);
                        break;
                    }
                case Constants.ColorSpace.XYZ:
                    {
                        checkBoxXYZ.Checked = true;
                        checkBoxX.Checked = oldChannels.HasFlag(Constants.VisChannels.Channel1);
                        checkBoxY.Checked = oldChannels.HasFlag(Constants.VisChannels.Channel2);
                        checkBoxZ.Checked = oldChannels.HasFlag(Constants.VisChannels.Channel3);
                        break;
                    }
                case Constants.ColorSpace.Grayscale:
                    checkBoxGrayscale.Checked = true;
                    break;
                case Constants.ColorSpace.Distances:
                    checkBoxDistances.Checked = true;
                    break;
                default:
                    checkBoxHSV.Checked = false;
                    checkBoxBGR.Checked = false;
                    checkBoxLab.Checked = false;
                    checkBoxPii.Checked = false;
                    checkBoxXYZ.Checked = false;
                    checkBoxDistances.Checked = false;
                    checkBoxGrayscale.Checked = false;                    
                    break;
            }
            if (oldChannels.HasFlag(Constants.VisChannels.Channel4))
            {
                checkBoxGM.Checked = true;
                radioButtonSingle.Checked = oldGradients.HasFlag(Constants.Gradients.Single);
                radioButtonMulti.Checked = oldGradients.HasFlag(Constants.Gradients.Multi);
                radioButtonMultiSobel.Checked = oldGradients.HasFlag(Constants.Gradients.MultiSobel);
            }
            else checkBoxGM.Checked = false;
            checkBoxOp.Checked = oldChannels.HasFlag(Constants.VisChannels.Channel1Opp) || oldChannels.HasFlag(Constants.VisChannels.Channel2Opp) || oldChannels.HasFlag(Constants.VisChannels.Channel3Opp) || oldChannels.HasFlag(Constants.VisChannels.Channel4Opp);

            radioButtonExhastiveSearch.Checked = oldGrouping.HasFlag(Constants.GroupingMethod.ExhaustiveSearch);
            radioButtonGKGrouping.Checked = oldGrouping.HasFlag(Constants.GroupingMethod.Karatzas);
            radioButtonChineseGrouping.Checked = oldGrouping.HasFlag(Constants.GroupingMethod.chinese);
        }

        private void buttonExit_Click(object sender, EventArgs e)
        {
            flagColorSpace = Constants.ColorSpace.None;
            flagChannels = Constants.VisChannels.None;
            flagGradients = Constants.Gradients.None;
            flagGrouping = Constants.GroupingMethod.None;
            if (checkBoxHSV.Enabled && checkBoxHSV.Checked)
            {
                flagColorSpace = Constants.ColorSpace.HSV;
                if (checkBoxHue.Checked) 
                { 
                    flagChannels = flagChannels | Constants.VisChannels.Channel1;
                    if (checkBoxOp.Checked) flagChannels = flagChannels | Constants.VisChannels.Channel1Opp; 
                }
                if (checkBoxSat.Checked)
                {
                    flagChannels = flagChannels | Constants.VisChannels.Channel2;
                    if (checkBoxOp.Checked) flagChannels = flagChannels | Constants.VisChannels.Channel2Opp; 
                }
                if (checkBoxVal.Checked)
                {
                    flagChannels = flagChannels | Constants.VisChannels.Channel3;
                    if (checkBoxOp.Checked) flagChannels = flagChannels | Constants.VisChannels.Channel3Opp; 
                }
            }
            else if(checkBoxBGR.Enabled && checkBoxBGR.Checked)
            {
                flagColorSpace = Constants.ColorSpace.BGR;
                if (checkBoxBlue.Checked)
                {
                    flagChannels = flagChannels | Constants.VisChannels.Channel1;
                    if (checkBoxOp.Checked) flagChannels = flagChannels | Constants.VisChannels.Channel1Opp;
                }
                if (checkBoxGreen.Checked)
                {
                    flagChannels = flagChannels | Constants.VisChannels.Channel2;
                    if (checkBoxOp.Checked) flagChannels = flagChannels | Constants.VisChannels.Channel2Opp;
                }
                if (checkBoxRed.Checked)
                {
                    flagChannels = flagChannels | Constants.VisChannels.Channel3;
                    if (checkBoxOp.Checked) flagChannels = flagChannels | Constants.VisChannels.Channel3Opp;
                }
            }
            else if (checkBoxLab.Enabled && checkBoxLab.Checked)
            {
                flagColorSpace = Constants.ColorSpace.LAB;
                if (checkBoxLightness.Checked)
                {
                    flagChannels = flagChannels | Constants.VisChannels.Channel1;
                    if (checkBoxOp.Checked) flagChannels = flagChannels | Constants.VisChannels.Channel1Opp;
                }
                if (checkBoxA.Checked)
                {
                    flagChannels = flagChannels | Constants.VisChannels.Channel2;
                    if (checkBoxOp.Checked) flagChannels = flagChannels | Constants.VisChannels.Channel2Opp;
                }
                if (checkBoxB.Checked)
                {
                    flagChannels = flagChannels | Constants.VisChannels.Channel3;
                    if (checkBoxOp.Checked) flagChannels = flagChannels | Constants.VisChannels.Channel3Opp;
                }
            }
            else if (checkBoxXYZ.Enabled && checkBoxXYZ.Checked)
            {
                flagColorSpace = Constants.ColorSpace.XYZ;
                if (checkBoxX.Checked)
                {
                    flagChannels = flagChannels | Constants.VisChannels.Channel1;
                    if (checkBoxOp.Checked) flagChannels = flagChannels | Constants.VisChannels.Channel1Opp;
                }
                if (checkBoxY.Checked)
                {
                    flagChannels = flagChannels | Constants.VisChannels.Channel2;
                    if (checkBoxOp.Checked) flagChannels = flagChannels | Constants.VisChannels.Channel2Opp;
                }
                if (checkBoxZ.Checked)
                {
                    flagChannels = flagChannels | Constants.VisChannels.Channel3;
                    if (checkBoxOp.Checked) flagChannels = flagChannels | Constants.VisChannels.Channel3Opp;
                }
            }
            else if (checkBoxPii.Enabled && checkBoxPii.Checked)
            {
                flagColorSpace = Constants.ColorSpace.Pii;
                if (checkBoxPii1.Checked)
                {
                    flagChannels = flagChannels | Constants.VisChannels.Channel1;
                    if (checkBoxOp.Checked) flagChannels = flagChannels | Constants.VisChannels.Channel1Opp;
                }
                if (checkBoxPii2.Checked)
                {
                    flagChannels = flagChannels | Constants.VisChannels.Channel2;
                    if (checkBoxOp.Checked) flagChannels = flagChannels | Constants.VisChannels.Channel2Opp;
                }
                if (checkBoxPii3.Checked)
                {
                    flagChannels = flagChannels | Constants.VisChannels.Channel3;
                    if (checkBoxOp.Checked) flagChannels = flagChannels | Constants.VisChannels.Channel3Opp;
                }
            }
            else if (checkBoxGrayscale.Enabled && checkBoxGrayscale.Checked)
            {
                flagColorSpace = Constants.ColorSpace.Grayscale;
                flagChannels = flagChannels | Constants.VisChannels.Channel1;
                if (checkBoxOp.Checked) flagChannels = flagChannels | Constants.VisChannels.Channel1Opp;
            }
            else if (checkBoxDistances.Enabled && checkBoxDistances.Checked)
            {
                flagColorSpace = Constants.ColorSpace.Distances;
                flagChannels = flagChannels | Constants.VisChannels.Channel1;
                if (checkBoxOp.Checked) flagChannels = flagChannels | Constants.VisChannels.Channel1Opp;
            }
            if (checkBoxGM.Checked)
            {
                flagChannels = flagChannels | Constants.VisChannels.Channel4;
                if (checkBoxOp.Checked) flagChannels = flagChannels | Constants.VisChannels.Channel4Opp;
                if (radioButtonSingle.Checked) flagGradients = Constants.Gradients.Single;
                else if (radioButtonMulti.Checked) flagGradients = Constants.Gradients.Multi;
                else if (radioButtonMultiSobel.Checked) flagGradients = Constants.Gradients.MultiSobel;
            }
            {
                if (radioButtonExhastiveSearch.Checked)
                    flagGrouping = Constants.GroupingMethod.ExhaustiveSearch;
                else if (radioButtonGKGrouping.Checked)
                    flagGrouping = Constants.GroupingMethod.Karatzas;
                else if (radioButtonChineseGrouping.Checked)
                    flagGrouping = Constants.GroupingMethod.chinese;
            }

            this.DialogResult = DialogResult.OK;
            this.Close();
        }

        private void checkBoxHSV_CheckedChanged(object sender, EventArgs e)
        {
            FormUtils.CheckBoxesMutuallyExclusive(checkBoxHSV, checkBoxBGR, checkBoxLab, checkBoxXYZ, checkBoxPii, checkBoxGrayscale, checkBoxDistances);

            checkBoxHue.Enabled = checkBoxHSV.Checked;
            checkBoxSat.Enabled = checkBoxHSV.Checked;
            checkBoxVal.Enabled = checkBoxHSV.Checked;
        }

        private void checkBoxBGR_CheckedChanged(object sender, EventArgs e)
        {
            FormUtils.CheckBoxesMutuallyExclusive(checkBoxBGR, checkBoxHSV, checkBoxLab, checkBoxXYZ, checkBoxPii, checkBoxGrayscale, checkBoxDistances);

            checkBoxBlue.Enabled = checkBoxBGR.Checked;
            checkBoxGreen.Enabled = checkBoxBGR.Checked;
            checkBoxRed.Enabled = checkBoxBGR.Checked;
        }

        private void checkBoxLab_CheckedChanged(object sender, EventArgs e)
        {
            FormUtils.CheckBoxesMutuallyExclusive(checkBoxLab, checkBoxBGR, checkBoxHSV, checkBoxXYZ, checkBoxPii, checkBoxGrayscale, checkBoxDistances);

            checkBoxLightness.Enabled = checkBoxLab.Checked;
            checkBoxA.Enabled = checkBoxLab.Checked;
            checkBoxB.Enabled = checkBoxLab.Checked;
        }

        private void checkBoxXYZ_CheckedChanged(object sender, EventArgs e)
        {
            FormUtils.CheckBoxesMutuallyExclusive(checkBoxXYZ, checkBoxBGR, checkBoxHSV, checkBoxLab, checkBoxPii, checkBoxGrayscale, checkBoxDistances);

            checkBoxX.Enabled = checkBoxXYZ.Checked;
            checkBoxY.Enabled = checkBoxXYZ.Checked;
            checkBoxZ.Enabled = checkBoxXYZ.Checked;
        }

        private void checkBoxPii_CheckedChanged(object sender, EventArgs e)
        {
            FormUtils.CheckBoxesMutuallyExclusive(checkBoxPii, checkBoxBGR, checkBoxHSV, checkBoxLab, checkBoxXYZ, checkBoxGrayscale, checkBoxDistances);

            checkBoxPii1.Enabled = checkBoxPii.Checked;
            checkBoxPii2.Enabled = checkBoxPii.Checked;
            checkBoxPii3.Enabled = checkBoxPii.Checked;
        }

        private void checkBoxGrayscale_CheckedChanged(object sender, EventArgs e)
        {
            FormUtils.CheckBoxesMutuallyExclusive(checkBoxGrayscale, checkBoxBGR, checkBoxHSV, checkBoxLab, checkBoxXYZ, checkBoxPii, checkBoxDistances);
        }

        private void checkBoxDistances_CheckedChanged(object sender, EventArgs e)
        {
            FormUtils.CheckBoxesMutuallyExclusive(checkBoxDistances, checkBoxBGR, checkBoxHSV, checkBoxLab, checkBoxXYZ, checkBoxPii, checkBoxGrayscale);
        }

        private void checkBoxGM_CheckedChanged(object sender, EventArgs e)
        {
            radioButtonSingle.Enabled = checkBoxGM.Checked;
            radioButtonMulti.Enabled = checkBoxGM.Checked;
            radioButtonMultiSobel.Enabled = checkBoxGM.Checked;
        }
    }
}
