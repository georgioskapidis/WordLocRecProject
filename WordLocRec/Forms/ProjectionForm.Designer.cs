﻿namespace WordLocRec.Forms
{
    partial class ProjectionForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.checkboxForward = new System.Windows.Forms.CheckBox();
            this.checkBoxInverse = new System.Windows.Forms.CheckBox();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.radioButton025 = new System.Windows.Forms.RadioButton();
            this.radioButton05 = new System.Windows.Forms.RadioButton();
            this.radioButton1 = new System.Windows.Forms.RadioButton();
            this.buttonOk = new System.Windows.Forms.Button();
            this.buttonCancel = new System.Windows.Forms.Button();
            this.checkBoxAllChannels = new System.Windows.Forms.CheckBox();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // checkboxForward
            // 
            this.checkboxForward.AutoSize = true;
            this.checkboxForward.Checked = true;
            this.checkboxForward.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkboxForward.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.checkboxForward.Location = new System.Drawing.Point(12, 19);
            this.checkboxForward.Name = "checkboxForward";
            this.checkboxForward.Size = new System.Drawing.Size(110, 17);
            this.checkboxForward.TabIndex = 0;
            this.checkboxForward.Text = "Forward projection";
            this.checkboxForward.UseVisualStyleBackColor = true;
            // 
            // checkBoxInverse
            // 
            this.checkBoxInverse.AutoSize = true;
            this.checkBoxInverse.Checked = true;
            this.checkBoxInverse.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxInverse.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.checkBoxInverse.Location = new System.Drawing.Point(12, 42);
            this.checkBoxInverse.Name = "checkBoxInverse";
            this.checkBoxInverse.Size = new System.Drawing.Size(107, 17);
            this.checkBoxInverse.TabIndex = 1;
            this.checkBoxInverse.Text = "Inverse projection";
            this.checkBoxInverse.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.Dock = System.Windows.Forms.DockStyle.Top;
            this.label1.Location = new System.Drawing.Point(0, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(233, 21);
            this.label1.TabIndex = 2;
            this.label1.Text = "Choose Forward/ Inverse projection and scale";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.checkboxForward);
            this.groupBox1.Controls.Add(this.checkBoxInverse);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.groupBox1.Location = new System.Drawing.Point(0, 21);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(233, 69);
            this.groupBox1.TabIndex = 3;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Projections";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.radioButton025);
            this.groupBox2.Controls.Add(this.radioButton05);
            this.groupBox2.Controls.Add(this.radioButton1);
            this.groupBox2.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.groupBox2.Location = new System.Drawing.Point(0, 90);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(233, 95);
            this.groupBox2.TabIndex = 4;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Select one scale";
            // 
            // radioButton025
            // 
            this.radioButton025.AutoSize = true;
            this.radioButton025.Location = new System.Drawing.Point(13, 66);
            this.radioButton025.Name = "radioButton025";
            this.radioButton025.Size = new System.Drawing.Size(46, 17);
            this.radioButton025.TabIndex = 2;
            this.radioButton025.Text = "0.25";
            this.radioButton025.UseVisualStyleBackColor = true;
            // 
            // radioButton05
            // 
            this.radioButton05.AutoSize = true;
            this.radioButton05.Location = new System.Drawing.Point(13, 43);
            this.radioButton05.Name = "radioButton05";
            this.radioButton05.Size = new System.Drawing.Size(40, 17);
            this.radioButton05.TabIndex = 1;
            this.radioButton05.Text = "0.5";
            this.radioButton05.UseVisualStyleBackColor = true;
            // 
            // radioButton1
            // 
            this.radioButton1.AutoSize = true;
            this.radioButton1.Checked = true;
            this.radioButton1.Location = new System.Drawing.Point(13, 20);
            this.radioButton1.Name = "radioButton1";
            this.radioButton1.Size = new System.Drawing.Size(31, 17);
            this.radioButton1.TabIndex = 0;
            this.radioButton1.TabStop = true;
            this.radioButton1.Text = "1";
            this.radioButton1.UseVisualStyleBackColor = true;
            // 
            // buttonOk
            // 
            this.buttonOk.BackColor = System.Drawing.Color.LightSteelBlue;
            this.buttonOk.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonOk.Location = new System.Drawing.Point(146, 211);
            this.buttonOk.Name = "buttonOk";
            this.buttonOk.Size = new System.Drawing.Size(75, 23);
            this.buttonOk.TabIndex = 5;
            this.buttonOk.Text = "OK";
            this.buttonOk.UseVisualStyleBackColor = false;
            this.buttonOk.Click += new System.EventHandler(this.buttonOk_Click);
            // 
            // buttonCancel
            // 
            this.buttonCancel.BackColor = System.Drawing.Color.LightSteelBlue;
            this.buttonCancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonCancel.Location = new System.Drawing.Point(12, 211);
            this.buttonCancel.MaximumSize = new System.Drawing.Size(75, 23);
            this.buttonCancel.MinimumSize = new System.Drawing.Size(75, 23);
            this.buttonCancel.Name = "buttonCancel";
            this.buttonCancel.Size = new System.Drawing.Size(75, 23);
            this.buttonCancel.TabIndex = 6;
            this.buttonCancel.Text = "Cancel";
            this.buttonCancel.UseVisualStyleBackColor = false;
            this.buttonCancel.Click += new System.EventHandler(this.buttonCancel_Click);
            // 
            // checkBoxAllChannels
            // 
            this.checkBoxAllChannels.AutoSize = true;
            this.checkBoxAllChannels.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.checkBoxAllChannels.Location = new System.Drawing.Point(12, 191);
            this.checkBoxAllChannels.Name = "checkBoxAllChannels";
            this.checkBoxAllChannels.Size = new System.Drawing.Size(138, 17);
            this.checkBoxAllChannels.TabIndex = 2;
            this.checkBoxAllChannels.Text = "All 3 channels if possible";
            this.checkBoxAllChannels.UseVisualStyleBackColor = true;
            // 
            // ProjectionForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.GhostWhite;
            this.ClientSize = new System.Drawing.Size(233, 246);
            this.Controls.Add(this.checkBoxAllChannels);
            this.Controls.Add(this.buttonCancel);
            this.Controls.Add(this.buttonOk);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.SizableToolWindow;
            this.Name = "ProjectionForm";
            this.Text = "ProjectionForm";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.CheckBox checkboxForward;
        private System.Windows.Forms.CheckBox checkBoxInverse;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.RadioButton radioButton025;
        private System.Windows.Forms.RadioButton radioButton05;
        private System.Windows.Forms.RadioButton radioButton1;
        private System.Windows.Forms.Button buttonOk;
        private System.Windows.Forms.Button buttonCancel;
        private System.Windows.Forms.CheckBox checkBoxAllChannels;
    }
}