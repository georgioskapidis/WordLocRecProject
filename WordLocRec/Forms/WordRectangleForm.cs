﻿using Emgu.CV;
using Emgu.CV.Util;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WordLocRec
{
    public partial class WordRectangleForm : Form
    {
        VectorOfMat wordMats;
        List<ERStat> ers;
        List<string> info;
        bool moreInfoAvaible = false;
        bool erInfoAvailable = false;
        int viewIndex = -1;
        public WordRectangleForm(VectorOfMat wordMats, String title, List<ERStat> ERs, List<string> infos)
            : this(wordMats, title, ERs)
        {
            if (infos.Count == ERs.Count)
            {
                this.info = new List<string>(infos);
                moreInfoAvaible = true;
            }
        }
        public WordRectangleForm(VectorOfMat wordMats, String title, List<ERStat> ERs):this(wordMats, title)
        {
            if (ERs.Count == this.wordMats.Size)
            {
                ers = new List<ERStat>(ERs);
                erInfoAvailable = true;
            }
        }
        public WordRectangleForm(VectorOfMat wordMats, String title)
        {
            InitializeComponent();
            this.Text = title;
            this.wordMats = new VectorOfMat();
            for (int i = 0; i < wordMats.Size; i++)
                this.wordMats.Push(wordMats[i].Clone());
//            this.wordMats = wordMats;

            viewIndex = 0;
            setInfoToView();
        }
        private void setBitmapInPictureBox(Bitmap bitmap)
        {
            pictureBox1.Image = bitmap;
            textBoxNumber.Text = viewIndex.ToString();
        }
        private void setErInfoToTooltip(String text)
        {
            toolTipERInfo.SetToolTip(pictureBox1, text);
            toolTipERInfo.AutoPopDelay = 30000;
        }

        private void buttonNext_Click(object sender, EventArgs e)
        {
            viewIndex = viewIndex + 1 >= wordMats.Size ? 0 : viewIndex + 1;

            setInfoToView();
        }

        private void buttonPrev_Click(object sender, EventArgs e)
        {
            viewIndex = viewIndex - 1 < 0 ? viewIndex = wordMats.Size - 1 : viewIndex - 1;

            setInfoToView();
        }

        private void setInfoToView()
        {
            setBitmapInPictureBox(wordMats[viewIndex].Bitmap);
            if (erInfoAvailable)
            {
                String text = moreInfoAvaible ? info[viewIndex] : createERInfo(ers[viewIndex]);
                setErInfoToTooltip(text);
            }
        }

        private String createERInfo(ERStat er)
        {
            StringBuilder sb = new StringBuilder();
            
            sb.AppendLine("Level -> " + er.level);
            sb.AppendLine("Rectangle -> { x: " + er.rect.X + ", y: " + er.rect.Y + ", width: " + er.rect.Width + ", height: " + er.rect.Height);
            sb.AppendLine("Area of pixels -> " + er.area);
            sb.AppendLine("Area of bb -> " + (er.rect.Width * er.rect.Height));
            sb.AppendLine("Perimeter -> " + er.perimeter);
            sb.AppendLine("Area Variation -> " + er.areaVariation);
            if (er.ffo != null)
            {
                sb.AppendLine(er.ffo.ToString());
            }
            return sb.ToString();
        }

        private void textBoxNumber_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode.Equals(Keys.Enter))
            {
                TextBox tb = sender as TextBox;
                if (tb.Focused)
                {
                    int number = -1;
                    if (int.TryParse(tb.Text, out number))
                    {
                        if (number > 0 && number < wordMats.Size)
                        {
                            viewIndex = number;
                            setInfoToView();
                        }
                    }
                }
            }
            
        }
    }
}
