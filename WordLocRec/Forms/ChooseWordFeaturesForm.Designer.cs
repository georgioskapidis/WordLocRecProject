﻿namespace WordLocRec.Forms
{
    partial class ChooseWordFeaturesForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.checkBoxWS = new System.Windows.Forms.CheckBox();
            this.checkBoxSWS = new System.Windows.Forms.CheckBox();
            this.checkBoxLWS = new System.Windows.Forms.CheckBox();
            this.buttonOK = new System.Windows.Forms.Button();
            this.checkBoxStandardize = new System.Windows.Forms.CheckBox();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.Dock = System.Windows.Forms.DockStyle.Top;
            this.label1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(0, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(220, 29);
            this.label1.TabIndex = 0;
            this.label1.Text = "Choose word features";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // checkBoxWS
            // 
            this.checkBoxWS.AutoSize = true;
            this.checkBoxWS.Checked = true;
            this.checkBoxWS.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxWS.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.checkBoxWS.Location = new System.Drawing.Point(12, 34);
            this.checkBoxWS.Name = "checkBoxWS";
            this.checkBoxWS.Size = new System.Drawing.Size(68, 17);
            this.checkBoxWS.TabIndex = 1;
            this.checkBoxWS.Text = "WordStat";
            this.checkBoxWS.UseVisualStyleBackColor = true;
            this.checkBoxWS.CheckedChanged += new System.EventHandler(this.checkBoxWS_CheckedChanged);
            // 
            // checkBoxSWS
            // 
            this.checkBoxSWS.AutoSize = true;
            this.checkBoxSWS.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.checkBoxSWS.Location = new System.Drawing.Point(12, 57);
            this.checkBoxSWS.Name = "checkBoxSWS";
            this.checkBoxSWS.Size = new System.Drawing.Size(99, 17);
            this.checkBoxSWS.TabIndex = 2;
            this.checkBoxSWS.Text = "SimpleWordStat";
            this.checkBoxSWS.UseVisualStyleBackColor = true;
            this.checkBoxSWS.CheckedChanged += new System.EventHandler(this.checkBoxSWS_CheckedChanged);
            // 
            // checkBoxLWS
            // 
            this.checkBoxLWS.AutoSize = true;
            this.checkBoxLWS.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.checkBoxLWS.Location = new System.Drawing.Point(12, 80);
            this.checkBoxLWS.Name = "checkBoxLWS";
            this.checkBoxLWS.Size = new System.Drawing.Size(88, 17);
            this.checkBoxLWS.TabIndex = 3;
            this.checkBoxLWS.Text = "LineWordStat";
            this.checkBoxLWS.UseVisualStyleBackColor = true;
            this.checkBoxLWS.CheckedChanged += new System.EventHandler(this.checkBoxLWS_CheckedChanged);
            // 
            // buttonOK
            // 
            this.buttonOK.BackColor = System.Drawing.Color.LightSteelBlue;
            this.buttonOK.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonOK.Location = new System.Drawing.Point(133, 114);
            this.buttonOK.Name = "buttonOK";
            this.buttonOK.Size = new System.Drawing.Size(75, 23);
            this.buttonOK.TabIndex = 4;
            this.buttonOK.Text = "OK";
            this.buttonOK.UseVisualStyleBackColor = false;
            this.buttonOK.Click += new System.EventHandler(this.buttonOK_Click);
            // 
            // checkBoxStandardize
            // 
            this.checkBoxStandardize.AutoSize = true;
            this.checkBoxStandardize.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.checkBoxStandardize.Location = new System.Drawing.Point(12, 117);
            this.checkBoxStandardize.Name = "checkBoxStandardize";
            this.checkBoxStandardize.Size = new System.Drawing.Size(79, 17);
            this.checkBoxStandardize.TabIndex = 5;
            this.checkBoxStandardize.Text = "Standardize";
            this.checkBoxStandardize.UseVisualStyleBackColor = true;
            // 
            // ChooseWordFeaturesForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.GhostWhite;
            this.ClientSize = new System.Drawing.Size(220, 152);
            this.Controls.Add(this.checkBoxStandardize);
            this.Controls.Add(this.buttonOK);
            this.Controls.Add(this.checkBoxLWS);
            this.Controls.Add(this.checkBoxSWS);
            this.Controls.Add(this.checkBoxWS);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.SizableToolWindow;
            this.Name = "ChooseWordFeaturesForm";
            this.Text = "Choose Word Features";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.CheckBox checkBoxWS;
        private System.Windows.Forms.CheckBox checkBoxSWS;
        private System.Windows.Forms.CheckBox checkBoxLWS;
        private System.Windows.Forms.Button buttonOK;
        private System.Windows.Forms.CheckBox checkBoxStandardize;

    }
}