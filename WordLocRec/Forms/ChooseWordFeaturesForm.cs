﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WordLocRec.Forms
{
    public partial class ChooseWordFeaturesForm : Form
    {
        public Constants.WordFeatureType wordFeatureType;
        public ChooseWordFeaturesForm(Constants.WordFeatureType wft)
        {
            InitializeComponent();
            wordFeatureType = wft;
            checkBoxWS.Checked = wft.HasFlag(Constants.WordFeatureType.WordStat);
            checkBoxSWS.Checked = wft.HasFlag(Constants.WordFeatureType.SimpleWordStat);
            checkBoxLWS.Checked = wft.HasFlag(Constants.WordFeatureType.LineWordStat);
            checkBoxStandardize.Checked = wft.HasFlag(Constants.WordFeatureType.Standardized);
        }

        private void buttonOK_Click(object sender, EventArgs e)
        {
            if (checkBoxStandardize.Checked) wordFeatureType = wordFeatureType | Constants.WordFeatureType.Standardized;
            this.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.Close();
        }

        private void checkBoxWS_CheckedChanged(object sender, EventArgs e)
        {
            FormUtils.CheckBoxesMutuallyExclusive(checkBoxWS, checkBoxSWS, checkBoxLWS);
            wordFeatureType = checkBoxWS.Checked? Constants.WordFeatureType.WordStat : 0;
        }

        private void checkBoxSWS_CheckedChanged(object sender, EventArgs e)
        {
            FormUtils.CheckBoxesMutuallyExclusive(checkBoxSWS, checkBoxWS, checkBoxLWS);
            wordFeatureType = checkBoxSWS.Checked? Constants.WordFeatureType.SimpleWordStat : 0;
        }

        private void checkBoxLWS_CheckedChanged(object sender, EventArgs e)
        {
            FormUtils.CheckBoxesMutuallyExclusive(checkBoxLWS, checkBoxSWS, checkBoxWS);
            wordFeatureType = checkBoxLWS.Checked? Constants.WordFeatureType.LineWordStat : 0;
        }
    }
}
