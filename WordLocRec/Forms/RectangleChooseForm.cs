﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WordLocRec
{
    public partial class RectangleChooseForm : Form
    {
        public Constants.VisChannels visFlags = Constants.VisChannels.None;
        public RectangleChooseForm(Constants.VisChannels flag, Constants.VisChannels previousFlag)
        {
            InitializeComponent();
            if (flag.HasFlag(Constants.VisChannels.Channel1))
            {
                checkBox1.Enabled = true;
                checkBox1.Checked = previousFlag.HasFlag(Constants.VisChannels.Channel1) ? true : false;
                if (flag.HasFlag(Constants.VisChannels.Channel1Opp))
                {
                    checkBox5.Enabled = true;
                    checkBox5.Checked = previousFlag.HasFlag(Constants.VisChannels.Channel1Opp) ? true : false;
                }
            }
            if (flag.HasFlag(Constants.VisChannels.Channel2))
            {
                checkBox2.Enabled = true;
                checkBox2.Checked = previousFlag.HasFlag(Constants.VisChannels.Channel2) ? true : false;
                if (flag.HasFlag(Constants.VisChannels.Channel2Opp))
                {
                    checkBox6.Enabled = true;
                    checkBox6.Checked = previousFlag.HasFlag(Constants.VisChannels.Channel2Opp) ? true : false;
                }
            }
            if (flag.HasFlag(Constants.VisChannels.Channel3))
            {
                checkBox3.Enabled = true;
                checkBox3.Checked = previousFlag.HasFlag(Constants.VisChannels.Channel3) ? true : false;
                if (flag.HasFlag(Constants.VisChannels.Channel3Opp))
                {
                    checkBox7.Enabled = true;
                    checkBox7.Checked = previousFlag.HasFlag(Constants.VisChannels.Channel3Opp) ? true : false;
                }
            }
            if (flag.HasFlag(Constants.VisChannels.Channel4))
            {
                checkBox4.Enabled = true;
                checkBox4.Checked = previousFlag.HasFlag(Constants.VisChannels.Channel4) ? true : false;
                if (flag.HasFlag(Constants.VisChannels.Channel4Opp))
                {
                    checkBox8.Enabled = true;
                    checkBox8.Checked = previousFlag.HasFlag(Constants.VisChannels.Channel4Opp) ? true : false;
                }
            }
        }

        private void buttonConfirm_Click(object sender, EventArgs e)
        {
            visFlags = Constants.VisChannels.None;
            if (checkBox1.Enabled && checkBox1.Checked)
                visFlags = visFlags | Constants.VisChannels.Channel1;
            if (checkBox2.Enabled && checkBox2.Checked)
                visFlags = visFlags | Constants.VisChannels.Channel2;
            if (checkBox3.Enabled && checkBox3.Checked)
                visFlags = visFlags | Constants.VisChannels.Channel3;
            if (checkBox4.Enabled && checkBox4.Checked)
                visFlags = visFlags | Constants.VisChannels.Channel4;
            if (checkBox5.Enabled && checkBox5.Checked)
                visFlags = visFlags | Constants.VisChannels.Channel1Opp;
            if (checkBox6.Enabled && checkBox6.Checked)
                visFlags = visFlags | Constants.VisChannels.Channel2Opp;
            if (checkBox7.Enabled && checkBox7.Checked)
                visFlags = visFlags | Constants.VisChannels.Channel3Opp;
            if (checkBox8.Enabled && checkBox8.Checked)
                visFlags = visFlags | Constants.VisChannels.Channel4Opp;
            this.DialogResult = DialogResult.OK;
            this.Close();
        }
    }
}
