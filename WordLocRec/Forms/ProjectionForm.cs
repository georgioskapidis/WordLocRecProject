﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WordLocRec.DataStructs;

namespace WordLocRec.Forms
{
    public partial class ProjectionForm : Form
    {
        public ColorChannel colorChannel;
        public ProjectionForm(ColorChannel cc)
        {
            this.colorChannel = cc;
            InitializeComponent();
            if (colorChannel.ForwardProjection) this.checkboxForward.Checked = true;
            if (colorChannel.InverseProjection) this.checkBoxInverse.Checked = true;

            if (colorChannel.Scale == 1d)
                radioButton1.Checked = true;
            else if (colorChannel.Scale == 0.5d)
                radioButton05.Checked = true;
            else if (colorChannel.Scale == 0.25d)
                radioButton025.Checked = true;
        }

        private void buttonOk_Click(object sender, EventArgs e)
        {
            colorChannel.ForwardProjection = checkboxForward.Checked;
            colorChannel.InverseProjection = checkBoxInverse.Checked;
            if (radioButton1.Checked)
                colorChannel.Scale = 1d;
            else if (radioButton05.Checked)
                colorChannel.Scale = 0.5d;
            else
                colorChannel.Scale = 0.25;
            if (checkBoxAllChannels.Checked)
            {
                if (colorChannel.Space.Equals(ColorChannel.AvailableColorSpaces.HSV) || colorChannel.Space.Equals(ColorChannel.AvailableColorSpaces.LAB) || colorChannel.Space.Equals(ColorChannel.AvailableColorSpaces.XYZ) || colorChannel.Space.Equals(ColorChannel.AvailableColorSpaces.Pii) || colorChannel.Space.Equals(ColorChannel.AvailableColorSpaces.BGR))
                    colorChannel.Kanali = ColorChannel.Channel.AllChannels;
                else colorChannel.Kanali = ColorChannel.Channel.Channel1;
            }

            colorChannel.ValidCombination = colorChannel.IsValidCombination();
            this.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.Close();
        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            colorChannel.ForwardProjection = false;
            colorChannel.InverseProjection = false;
            this.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.Close();
        }
    }
}
