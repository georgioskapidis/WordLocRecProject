﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Remoting;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WordLocRec.DataStructs;
using WordLocRec.Grouping;
using WordLocRec.SVMStuff;

namespace WordLocRec.Forms
{
    public partial class StatisticsForm : Form
    {
        List<BaseFeature> saveOutput; // the final four are the statistics of the full dataset, the rest, split in fours are the statistics of each individual dataset that has been loaded
        List<String[]> datasets;
        String defaultOutputFolder;
        List<String> datasetNames;
        Type t; // type of the loaded datasets

        public StatisticsForm()
        {
            InitializeComponent();
            InitializeData();
        }

        private void InitializeData()
        {
            datasets = new List<string[]>();
            datasetNames = new List<string>();
            saveOutput = new List<BaseFeature>();
            t = null;
            defaultOutputFolder = "";
            //textBoxText.Text = "";

            checkBoxFFO.Enabled = true; checkBoxLWS.Enabled = true; checkBoxSWS.Enabled = true; checkBoxWS.Enabled = true;
            checkBoxFFO.Checked = false; checkBoxLWS.Checked = false; checkBoxSWS.Checked = false; checkBoxWS.Checked = false;

            tabControl1.TabPages.Clear();
        }

        #region FormMethods

        private List<BaseFeature> CalcMeanStdMinMaxToBaseFeatures(Type t, String[] dataset)
        {
            // well done Jon Skeet
            MethodInfo UtilsCalcStatistics = typeof(Utils).GetMethod("CalcStatistics");
            MethodInfo generic = UtilsCalcStatistics.MakeGenericMethod(t);
            Tuple<List<float>, List<float>, List<float>, List<float>> meanStdMinMax = (Tuple<List<float>, List<float>, List<float>, List<float>>)generic.Invoke(null, new object[] { dataset });

            MethodInfo FromFloatList = t.GetMethod("FromFloatList");

            List<BaseFeature> bf = new List<BaseFeature>();
            bf.Add((BaseFeature)FromFloatList.Invoke(this, new object[] { meanStdMinMax.Item1 }));
            bf.Add((BaseFeature)FromFloatList.Invoke(this, new object[] { meanStdMinMax.Item2 }));
            bf.Add((BaseFeature)FromFloatList.Invoke(this, new object[] { meanStdMinMax.Item3 }));
            bf.Add((BaseFeature)FromFloatList.Invoke(this, new object[] { meanStdMinMax.Item4 }));

            return bf;
        }

        private void ViewResults2()
        {//1 tab per dataset and 1 for the combined results if necessary
            int tabsCount = datasets.Count;
            tabControl1.TabPages.Clear();
            for (int i = 0; i < tabsCount; i++) 
            {
                TabPage tb = new TabPage(" Tab " + datasetNames[i]);
                tb.AutoScroll = true;
                Panel p = new Panel();
                p.Dock = DockStyle.Fill;
                p.AutoScroll = true;
                p.Controls.Add(CreateTable(new ColumnStyle(SizeType.AutoSize), i));
                tb.Controls.Add(p);
                tabControl1.TabPages.Add(tb);
            }
            if (saveOutput.Count > 4)
            {
                TabPage tb = new TabPage(" Tab Combined ");
                tb.AutoScroll = true;
                Panel p = new Panel();
                p.Dock = DockStyle.Fill;
                p.AutoScroll = true;
                p.Controls.Add(CreateTable(new ColumnStyle(SizeType.AutoSize), tabsCount));
                tb.Controls.Add(p);
                tabControl1.TabPages.Add(tb);
            }
        }

        private TableLayoutPanel CreateTable(ColumnStyle cs, int datasetIndex)
        {
            TableLayoutPanel tlp = new TableLayoutPanel();
            tlp.CellBorderStyle = TableLayoutPanelCellBorderStyle.Inset;
            tlp.AutoSize = true;
            tlp.BackColor = Color.GhostWhite;
            tlp.RowCount = 5;
            int featureLength = t.Equals(typeof(WordStat)) ? 14 : t.Equals(typeof(SimpleWordStat)) ? 10 : t.Equals(typeof(LineWordStat)) ? 8 : 17;
            tlp.ColumnCount = 1 + featureLength;
            tlp.AutoScroll = true;

            tlp.ColumnStyles.Add(cs);

            CreateTableLegend(ref tlp);

            int totalFiles = 0;
            if (datasetNames.Count != datasetIndex)
                totalFiles = datasets[datasetIndex].Length;
            else
            {
                foreach (var dataset in datasets)
                    totalFiles += dataset.Length;
            }
            Label num = CreateLabel(totalFiles.ToString());
            tlp.Controls.Add(num, 0, 0);
            for (int i = 1; i < tlp.RowCount; i++)
            {
                List<float> data = saveOutput[datasetIndex * 4 + (i - 1)].ToFloatList();
                for (int j = 1; j < tlp.ColumnCount; j++)
                {
                    Label l = new Label(); 
                    l.Text = data[j - 1].ToString();
                    tlp.Controls.Add(l, j, i);
                }
            }
            return tlp;
        }
        private void CreateTableLegend(ref TableLayoutPanel tlp)
        {
            if (t == null)
            {
                MessageBox.Show("Pick a type of feature"); return;
            }
            if (t.Equals(typeof(WordStat)))
                TableLegendWordStat(ref tlp);
            else if (t.Equals(typeof(SimpleWordStat)))
                TableLegendSimpleWordStat(ref tlp);
            else if (t.Equals(typeof(LineWordStat)))
                TableLegendLineWordStat(ref tlp);
            else if (t.Equals(typeof(FullFeatureObject)))
                TableLegendFullFeatureObject(ref tlp);
        }
        private void TableLegendWordStat(ref TableLayoutPanel tlp)
        {
            List<Label> labels = new List<Label>();
            labels.Add(CreateLabel("FG intensities STD"));
            labels.Add(CreateLabel("BG intensities STD"));
            labels.Add(CreateLabel("Major Axis CoV"));
            labels.Add(CreateLabel("Stroke Widths CoV"));
            labels.Add(CreateLabel("Mean Gradient STD"));
            labels.Add(CreateLabel("Aspect Ratio CoV"));
            labels.Add(CreateLabel("Hu Moments Avg EuclideanDistance"));
            labels.Add(CreateLabel("ConvexHullCompactnessMean"));
            labels.Add(CreateLabel("ConvexHullCompactnessStd"));
            labels.Add(CreateLabel("ConvexityDefects CoV"));
            labels.Add(CreateLabel("Mst angles mean"));
            labels.Add(CreateLabel("Mst angles std"));
            labels.Add(CreateLabel("Mst EdgeWidths CoV"));
            labels.Add(CreateLabel("Mst EdgeDistances"));

            AdditionalTableLabels(ref tlp, labels);
        }
        private void TableLegendSimpleWordStat(ref TableLayoutPanel tlp)
        {
            List<Label> labels = new List<Label>();
            labels.Add(CreateLabel("Mean FG intensities CoV"));
            labels.Add(CreateLabel("Mean Outer boundary intensities CoV")); 
            labels.Add(CreateLabel("Foreground Background Lab Distance CoV")); 
            labels.Add(CreateLabel("Perimetric Gradient Magnitude Mean CoV"));
            labels.Add(CreateLabel("Mean Stroke Width CoV"));
            labels.Add(CreateLabel("Convex Hull Compactness Mean"));
            labels.Add(CreateLabel("Convex Hull Compactness STD"));
            labels.Add(CreateLabel("Convexity Defects CoV"));
            labels.Add(CreateLabel("Vertical Center Distance CoV"));
            labels.Add(CreateLabel("Horizontal Center Distances CoV"));

            AdditionalTableLabels(ref tlp, labels);
        }       
        private void TableLegendLineWordStat(ref TableLayoutPanel tlp)
        {
            List<Label> labels = new List<Label>();
            labels.Add(CreateLabel("Mean Stroke Width CoV"));
            labels.Add(CreateLabel("Mean Lab FGBG Distance CoV"));
            labels.Add(CreateLabel("Lab Constistency Among ERs"));
            labels.Add(CreateLabel("Max Vertical Error"));
            labels.Add(CreateLabel("Width CoV"));
            labels.Add(CreateLabel("Percentage Horizontal Overlap"));
            labels.Add(CreateLabel("Percentage Uncovered"));
            labels.Add(CreateLabel("Avg Euclidean Hu Distance"));

            AdditionalTableLabels(ref tlp, labels);
        }
        private void TableLegendFullFeatureObject(ref TableLayoutPanel tlp)
        {
            List<Label> labels = new List<Label>();

            labels.Add(CreateLabel("Aspect ratio"));
            labels.Add(CreateLabel("Compactness"));
            labels.Add(CreateLabel("Holes"));
            labels.Add(CreateLabel("MedianOfCrossings"));
            labels.Add(CreateLabel("HoleAreaRatio"));
            labels.Add(CreateLabel("ConvexHullRatio"));
            labels.Add(CreateLabel("NumInflexionPoints"));

            labels.Add(CreateLabel("ERBB Pii Distance"));
            labels.Add(CreateLabel("ERBB Lab Distance"));
            labels.Add(CreateLabel("ERBB BGR Distance"));
            labels.Add(CreateLabel("Lab ColorConsistency"));
            labels.Add(CreateLabel("SignificantPaths"));
            labels.Add(CreateLabel("SkelContourRatio"));
            labels.Add(CreateLabel("SkelAreaRatio"));
            labels.Add(CreateLabel("AreaBBRatio"));
            labels.Add(CreateLabel("ConvexityDefects"));
            labels.Add(CreateLabel("StrokeW ERwidthRatio"));

            AdditionalTableLabels(ref tlp, labels);
        }
        private Label CreateLabel(String text)
        {
            Label l = new Label();
            l.FlatStyle = FlatStyle.Flat;
            l.Dock = DockStyle.Fill;
            l.Text = text;
            l.AutoSize = true;
            return l;
        }

        private static void AdditionalTableLabels(ref TableLayoutPanel tlp, List<Label> labels)
        {
            for (int i = 0; i < labels.Count; i++)
            {
                tlp.Controls.Add(labels[i], i + 1, 0);
            }

            Label mean = new Label(); mean.Text = "Mean values";
            Label std = new Label(); std.Text = "Standard deviations";
            Label min = new Label(); min.Text = "Minimums";
            Label max = new Label(); max.Text = "Maximums";

            tlp.Controls.Add(mean, 0, 1); tlp.Controls.Add(std, 0, 2); tlp.Controls.Add(min, 0, 3); tlp.Controls.Add(max, 0, 4);
        }

        #endregion

        #region oldstuff

        //private void ViewResults(List<BaseFeature> saveOutput)
        //{
        //    StringBuilder sb = new StringBuilder();
        //    sb.AppendLine(GetStringLegend());
        //    sb.AppendLine();

        //    if (!checkBoxShowComb.Checked)
        //    for (int i = 0; i < datasetNames.Count; i++)
        //    {
        //        sb.AppendLine(datasetNames[i] + " Mean of " + datasets[i].Length + " data files");
        //        sb.AppendLine(saveOutput[4 * i].ToFlatString());
        //        sb.AppendLine();

        //        sb.AppendLine(datasetNames[i] + " Std of " + datasets[i].Length + " data files");
        //        sb.AppendLine(saveOutput[4 * i + 1].ToFlatString());
        //        sb.AppendLine();

        //        if (checkBoxShowMinMax.Checked)
        //        {
        //            sb.AppendLine(datasetNames[i] + " Min of " + datasets[i].Length + " data files");
        //            sb.AppendLine(saveOutput[4 * i + 2].ToFlatString());
        //            sb.AppendLine();

        //            sb.AppendLine(datasetNames[i] + " Max of " + datasets[i].Length + " data files");
        //            sb.AppendLine(saveOutput[4 * i + 3].ToFlatString());
        //            sb.AppendLine();
        //        }
        //    }

        //    int count = saveOutput.Count;
        //    if (count > 4)
        //    {
        //        int totalFiles = 0;
        //        foreach (var dataset in datasets)
        //            totalFiles += dataset.Length;

        //        sb.AppendLine(" Combined Mean of " + totalFiles + " data files");
        //        sb.AppendLine(saveOutput[count - 4].ToFlatString());
        //        sb.AppendLine();

        //        sb.AppendLine(" Combined Std of " + totalFiles + " data files");
        //        sb.AppendLine(saveOutput[count - 3].ToFlatString());
        //        sb.AppendLine();

        //        if (checkBoxShowMinMax.Checked)
        //        {
        //            sb.AppendLine(" Combined Min of " + totalFiles + " data files");
        //            sb.AppendLine(saveOutput[count - 2].ToFlatString());
        //            sb.AppendLine();

        //            sb.AppendLine(" Combined Max of " + totalFiles + " data files");
        //            sb.AppendLine(saveOutput[count - 1].ToFlatString());
        //            sb.AppendLine();
        //        }
        //    }

        //    textBoxText.Text = sb.ToString();
        //} // old method

        //private String GetStringLegend()
        //{
        //    String legend;
        //    if (t.Equals(typeof(WordStat)))
        //    {
        //        legend = IOUtils.WordStatFileFormat();
        //    }
        //    else if (t.Equals(typeof(SimpleWordStat)))
        //    {
        //        legend = IOUtils.SimpleWordStatFileFormat();
        //    }
        //    else if (t.Equals(typeof(LineWordStat)))
        //    {
        //        legend = IOUtils.LineWordStatFileFormat();
        //    }
        //    else legend = "Please select a feature";
        //    return legend;
        //} // old method

        #endregion

        #region Checkbox logic
        private void checkBoxWS_CheckedChanged(object sender, EventArgs e)
        {
            FormUtils.CheckBoxesMutuallyExclusive(checkBoxWS, checkBoxSWS, checkBoxLWS, checkBoxFFO);
        }

        private void checkBoxSWS_CheckedChanged(object sender, EventArgs e)
        {
            FormUtils.CheckBoxesMutuallyExclusive(checkBoxSWS, checkBoxWS, checkBoxLWS, checkBoxFFO);
        }

        private void checkBoxLWS_CheckedChanged(object sender, EventArgs e)
        {
            FormUtils.CheckBoxesMutuallyExclusive(checkBoxLWS, checkBoxWS, checkBoxSWS, checkBoxFFO);
        }

        private void checkBoxFFO_CheckedChanged(object sender, EventArgs e)
        {
            FormUtils.CheckBoxesMutuallyExclusive(checkBoxFFO, checkBoxWS, checkBoxSWS, checkBoxLWS);
        }

        #endregion

        private void buttonLoad_Click(object sender, EventArgs e)
        {
            if (checkBoxWS.Checked) t = typeof(WordStat);
            else if (checkBoxSWS.Checked) t = typeof(SimpleWordStat);
            else if (checkBoxLWS.Checked) t = typeof(LineWordStat);
            else if (checkBoxFFO.Checked) t = typeof(FullFeatureObject);
            else
            {
                MessageBox.Show("Define the feature type first!");
                return;
            }
            checkBoxWS.Enabled = false; checkBoxSWS.Enabled = false; checkBoxLWS.Enabled = false; checkBoxFFO.Enabled = false;

            String[] filenames = FormUtils.ParametrizedOpenFileDialog(Constants.XML_FILE_FILTER, true, "Select the dataset files");
            if (filenames == null) return;
            datasets.Add(filenames);
            if (defaultOutputFolder == "")
            {
                defaultOutputFolder = Directory.GetParent(Path.GetDirectoryName(filenames[0])).ToString();
            }
            datasetNames.Add(Path.GetFileName(Path.GetDirectoryName(filenames[0])));
        }

        private void buttonClear_Click(object sender, EventArgs e)
        {
            DialogResult res = MessageBox.Show("Everything will be cleared", " Continue or Exit", MessageBoxButtons.OKCancel);
            if (res != System.Windows.Forms.DialogResult.OK)
                return;

            InitializeData();
            
        }

        private void buttonCalculate_Click(object sender, EventArgs e)
        {
            bool hasFeature = checkBoxFFO.Checked || checkBoxWS.Checked || checkBoxSWS.Checked || checkBoxLWS.Checked;
            if (!hasFeature)
            {
                MessageBox.Show("Please select a feature that describes the datasets", "Error");
                return;
            }

            if (datasets.Count > 0) // calculations can be done
            {
                if (checkBoxWS.Checked)
                    t = typeof(WordStat);
                else if (checkBoxSWS.Checked)
                    t = typeof(SimpleWordStat);
                else if (checkBoxLWS.Checked)
                    t = typeof(LineWordStat);
                else // if (checkBoxFFO.Checked)
                    t = typeof(FullFeatureObject);

                foreach (var dataset in datasets)
                    saveOutput.AddRange(CalcMeanStdMinMaxToBaseFeatures(t, dataset));

                if (datasets.Count > 1) // if more than one datasets are selected put the resulting combination statistics in the final four places
                {
                    List<String> allTheData = new List<string>();
                    for (int i = 0; i < datasets.Count; i++)
                        allTheData.AddRange(datasets[i]);
                    saveOutput.AddRange(CalcMeanStdMinMaxToBaseFeatures(t, allTheData.ToArray()));
                }

                //ViewResults(saveOutput);
                ViewResults2();
            }
        }

        private void buttonSaveStats_Click(object sender, EventArgs e)
        {
            if (saveOutput.Count == 0)
            {
                MessageBox.Show("Make some data first.");
                return;
            }
                MethodInfo TOXML = typeof(IOUtils).GetMethod("ToXML");
                MethodInfo genericToXml = TOXML.MakeGenericMethod(t);

            if (!checkBoxSaveCombOnly.Checked)
            for (int i = 0; i < datasetNames.Count; i++)
            {
                genericToXml.Invoke(null, new object[] { saveOutput[4 * i], defaultOutputFolder + Path.DirectorySeparatorChar + datasetNames[i] + "Mean.xml" });
                genericToXml.Invoke(null, new object[] { saveOutput[4 * i + 1], defaultOutputFolder + Path.DirectorySeparatorChar + datasetNames[i] + "Std.xml" });
                
                if (checkBoxSaveMinMax.Checked)
                {
                    genericToXml.Invoke(null, new object[] { saveOutput[4 * i + 2], defaultOutputFolder + Path.DirectorySeparatorChar + datasetNames[i] + "Min.xml" });
                    genericToXml.Invoke(null, new object[] { saveOutput[4 * i + 3], defaultOutputFolder + Path.DirectorySeparatorChar + datasetNames[i] + "Max.xml" });
                }
                
            }
            int count = saveOutput.Count;
            if (count > 4)
            {
                genericToXml.Invoke(null, new object[] { saveOutput[count - 4], defaultOutputFolder + Path.DirectorySeparatorChar + "CombinedMean.xml" });
                genericToXml.Invoke(null, new object[] { saveOutput[count - 3], defaultOutputFolder + Path.DirectorySeparatorChar + "CombinedStd.xml" });

                if (checkBoxSaveMinMax.Checked)
                {
                    genericToXml.Invoke(null, new object[] { saveOutput[count - 2], defaultOutputFolder + Path.DirectorySeparatorChar + "CombinedMin.xml" });
                    genericToXml.Invoke(null, new object[] { saveOutput[count - 1], defaultOutputFolder + Path.DirectorySeparatorChar + "CombinedMax.xml" });
                }
            }
            
        }


    }
}
