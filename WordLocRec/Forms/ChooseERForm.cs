﻿using Emgu.CV;
using Emgu.CV.Util;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WordLocRec.Forms
{
    public partial class ChooseERForm : Form
    {
        public List<ERStat> toReturn;
        public List<int> positiveIndices;
        public List<int> negativeIndices;

        List<ERStat> ERs;
        List<Mat> mats;
        bool typeOfExample;
        int acceptedERsCount;
        int dismissedERsCount;

        private bool[] indices;
        private int viewIndex = 0;
        private List<int> actionHistory;
        public ChooseERForm(List<ERStat> extremalRegions, VectorOfMat erMats, bool typeOfExample)
        {
            InitializeComponent();
            this.typeOfExample = typeOfExample;
            this.mats = new List<Mat>();
            indices = new bool[erMats.Size];
            actionHistory = new List<int>();
            positiveIndices = new List<int>();
            negativeIndices = new List<int>();

            for (int i = 0; i < erMats.Size; i++)
            {
                mats.Add(erMats[i].Clone());
                indices[i] = true;
            }
                
            ERs = new List<ERStat>(extremalRegions);
            toReturn = new List<ERStat>();

            if (extremalRegions.Count == 0)
            {
                MessageBox.Show("No ERs available");
                this.Close();
            }
            else
            {
                //initialize text boxes
                StringBuilder titleText = new StringBuilder();
                titleText.Append("Working with: ");
                titleText.Append(typeOfExample ? " Positive examples" : " Negative examples");
                textBoxTitle.Text = titleText.ToString();

                textBoxAvailableERs.Text = extremalRegions.Count.ToString();
                acceptedERsCount = 0;
                dismissedERsCount = 0;
                textBoxAcceptedERs.Text = acceptedERsCount.ToString();
                textBoxDismissedERs.Text = dismissedERsCount.ToString();

                setInfoToView();
            }
        }
        private String createERInfo(ERStat er)
        {
            StringBuilder sb = new StringBuilder();

            sb.AppendLine("Level -> " + er.level);
            sb.AppendLine("Rectangle -> { x: " + er.rect.X + ", y: " + er.rect.Y + ", width: " + er.rect.Width + ", height: " + er.rect.Height);
            sb.AppendLine("Area of pixels -> " + er.area);
            sb.AppendLine("Aspect ratio -> " + ((float)er.rect.Width / er.rect.Height));
            sb.AppendLine("Area of bb -> " + (er.rect.Width * er.rect.Height));
            sb.AppendLine("Perimeter -> " + er.perimeter);
            sb.AppendLine("Compactness -> " + ((float)Math.Sqrt((float)(er.area)) / er.perimeter));
            sb.AppendLine("Median Crossings -> " + er.medCrossings);
            sb.AppendLine("Euler num -> " + er.euler + " , Num Of holes -> " + (1 - er.euler));
            sb.AppendLine("Hole-Area ratio -> " + er.holeAreaRatio);
            sb.AppendLine("ConvexHull ratio -> " + er.convexHullRatio);
            sb.AppendLine("Inflextion Points -> " + er.numInflexionPoints);
            sb.AppendLine("Area Variation -> " + er.areaVariation);

            return sb.ToString();
        }
        private void setInfoToView()
        {
            textBoxAcceptedERs.Text = positiveIndices.Count.ToString();
            textBoxDismissedERs.Text = negativeIndices.Count.ToString();

            if (viewIndex == -1)
            {
                DialogResult res = MessageBox.Show("All have been decided, exit?", " Continue or Exit", MessageBoxButtons.OKCancel);
                if (res == System.Windows.Forms.DialogResult.OK)
                {
                    if (typeOfExample)
                        foreach (int i in positiveIndices) toReturn.Add(ERs[i]);
                    else
                        foreach (int i in negativeIndices) toReturn.Add(ERs[i]);

                    this.Close();
                }
                else { buttonUndo_Click(null, null); }
            }
            else
            {
                setBitmapToPictureBox(mats[viewIndex].Bitmap);
                textBoxERInfo.Text = createERInfo(ERs[viewIndex]);
                textBoxViewIndex.Text = viewIndex.ToString();
            }           
        }
        private void setBitmapToPictureBox(Bitmap bmp)
        {
            this.pictureBoxER.Image = bmp;
        }
        private int FindNextApplicable()
        {
            for (int i = 0; i < indices.Length; i++)
            {
                if (indices[i]) return i;
            }
            return -1;
        }

        private void buttonUndo_Click(object sender, EventArgs e)
        {
            if (actionHistory.Count == 0) return;
            if (positiveIndices.Contains(actionHistory[actionHistory.Count - 1]))
                positiveIndices.Remove(actionHistory[actionHistory.Count - 1]);
            else if (negativeIndices.Contains(actionHistory[actionHistory.Count - 1]))
                negativeIndices.Remove(actionHistory[actionHistory.Count - 1]);

            indices[actionHistory[actionHistory.Count - 1]] = true;
            viewIndex = actionHistory[actionHistory.Count - 1];

            actionHistory.RemoveAt(actionHistory.Count - 1);

            setInfoToView();
        }
        private void buttonAcceptER_Click(object sender, EventArgs e)
        {
            if (indices[viewIndex])
            {
                positiveIndices.Add(viewIndex);
                indices[viewIndex] = false;
                actionHistory.Add(viewIndex);
            }
            viewIndex = FindNextApplicable();
            setInfoToView();        
        }
        private void buttonDismissER_Click(object sender, EventArgs e)
        {
            if (indices[viewIndex])
            {
                negativeIndices.Add(viewIndex);
                indices[viewIndex] = false;
                actionHistory.Add(viewIndex);
            }
            viewIndex = FindNextApplicable();
            setInfoToView();
        }
        private void buttonNext_Click(object sender, EventArgs e)
        {
            viewIndex = viewIndex + 1 > mats.Count - 1 ? 0 : viewIndex + 1;
            setInfoToView();
        }
        private void buttonPrev_Click(object sender, EventArgs e)
        {
            viewIndex = viewIndex - 1 < 0 ? viewIndex = mats.Count - 1 : viewIndex - 1;

            setInfoToView();
        }
    }
}
