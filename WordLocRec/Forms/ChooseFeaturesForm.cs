﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WordLocRec.Forms
{
    public partial class ChooseFeaturesForm : Form
    {
        public Constants.FeatureType featureType { get; private set; }
        public ChooseFeaturesForm(Constants.FeatureType previousValues)
        {
            InitializeComponent();
            if (previousValues.HasFlag(Constants.FeatureType.NM))
                checkBoxNM.Checked = true;
            if (previousValues.HasFlag(Constants.FeatureType.Histogram))
                checkBoxHistograms.Checked = true;
            if (previousValues.HasFlag(Constants.FeatureType.ColorDistPii))
                checkBoxPii.Checked = true;
            if (previousValues.HasFlag(Constants.FeatureType.ColorDistLab))
                checkBoxLab.Checked = true;
            if (previousValues.HasFlag(Constants.FeatureType.SignificantPaths))
                checkBoxPaths.Checked = true;
            if (previousValues.HasFlag(Constants.FeatureType.StandarizedPerFeature))
                checkBoxStand.Checked = true;
            if (previousValues.HasFlag(Constants.FeatureType.NumberOfHoles))
                checkBoxHoles.Checked = true;
            if (previousValues.HasFlag(Constants.FeatureType.ColorDistBgr))
                checkBoxBgr.Checked = true;
            if (previousValues.HasFlag(Constants.FeatureType.ColorConsistencyLab))
                checkBoxLabCon.Checked = true;
            if (previousValues.HasFlag(Constants.FeatureType.ConvexityDefects))
                checkBoxConvDefects.Checked = true;
            if (previousValues.HasFlag(Constants.FeatureType.StrokeRatio))
                checkBoxStrokeWidthRatio.Checked = true;
        }
        private void buttonOk_Click(object sender, EventArgs e)
        {
            if (checkBoxNM.Checked)
                featureType = featureType | Constants.FeatureType.NM;
            if (checkBoxHistograms.Checked)
                featureType = featureType | Constants.FeatureType.Histogram;
            if (checkBoxPii.Checked)
                featureType = featureType | Constants.FeatureType.ColorDistPii;
            if (checkBoxLab.Checked)
                featureType = featureType | Constants.FeatureType.ColorDistLab;
            if (checkBoxPaths.Checked)
                featureType = featureType | Constants.FeatureType.SignificantPaths;
            if (checkBoxStand.Checked)
                featureType = featureType | Constants.FeatureType.StandarizedPerFeature;
            if (checkBoxHoles.Checked)
                featureType = featureType | Constants.FeatureType.NumberOfHoles;
            if (checkBoxBgr.Checked)
                featureType = featureType | Constants.FeatureType.ColorDistBgr;
            if (checkBoxLabCon.Checked)
                featureType = featureType | Constants.FeatureType.ColorConsistencyLab;
            if (checkBoxConvDefects.Checked)
                featureType = featureType | Constants.FeatureType.ConvexityDefects;
            if (checkBoxStrokeWidthRatio.Checked)
                featureType = featureType | Constants.FeatureType.StrokeRatio;

            this.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.Close();
        }
        

    }
}
