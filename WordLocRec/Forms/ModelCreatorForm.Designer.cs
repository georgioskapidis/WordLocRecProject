﻿namespace WordLocRec.Forms
{
    partial class ModelCreatorForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.buttonLoadP = new System.Windows.Forms.Button();
            this.buttonLoadN = new System.Windows.Forms.Button();
            this.buttonTrain = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.checkBoxFFO = new System.Windows.Forms.CheckBox();
            this.buttonEval = new System.Windows.Forms.Button();
            this.buttonTestSet = new System.Windows.Forms.Button();
            this.checkBoxLWS = new System.Windows.Forms.CheckBox();
            this.checkBoxSWS = new System.Windows.Forms.CheckBox();
            this.checkBoxWS = new System.Windows.Forms.CheckBox();
            this.label6 = new System.Windows.Forms.Label();
            this.tbSplit = new System.Windows.Forms.TextBox();
            this.listView1 = new System.Windows.Forms.ListView();
            this.label2 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel4 = new System.Windows.Forms.Panel();
            this.buttonTrainAll = new System.Windows.Forms.Button();
            this.buttonTrainRemaining = new System.Windows.Forms.Button();
            this.panel3 = new System.Windows.Forms.Panel();
            this.tabControlClassifiers = new System.Windows.Forms.TabControl();
            this.tabPageSVM = new System.Windows.Forms.TabPage();
            this.tbDegree = new System.Windows.Forms.TextBox();
            this.tbCoef0 = new System.Windows.Forms.TextBox();
            this.tbNu = new System.Windows.Forms.TextBox();
            this.tbIterations = new System.Windows.Forms.TextBox();
            this.tbGamma = new System.Windows.Forms.TextBox();
            this.tbC = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.cbTwoClassSVM = new System.Windows.Forms.CheckBox();
            this.cbOneClassSVM = new System.Windows.Forms.CheckBox();
            this.comboBoxKernels = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.cbTrainAuto = new System.Windows.Forms.CheckBox();
            this.tabPageDTrees = new System.Windows.Forms.TabPage();
            this.cbUseSurrogates = new System.Windows.Forms.CheckBox();
            this.cbUseSE1Rule = new System.Windows.Forms.CheckBox();
            this.cbTruncatePrunedTree = new System.Windows.Forms.CheckBox();
            this.tbCVFolds = new System.Windows.Forms.TextBox();
            this.tbMinSampleCount = new System.Windows.Forms.TextBox();
            this.tbMaxDepth = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.tabPageRF = new System.Windows.Forms.TabPage();
            this.tbFolds = new System.Windows.Forms.TextBox();
            this.tbIters = new System.Windows.Forms.TextBox();
            this.tbMaxDepthR = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.cbUseSurrogatesR = new System.Windows.Forms.CheckBox();
            this.cbUseSE1RuleR = new System.Windows.Forms.CheckBox();
            this.cbTruncatePrunedTreeR = new System.Windows.Forms.CheckBox();
            this.cbVarImportance = new System.Windows.Forms.CheckBox();
            this.cbActiveVarCountAll = new System.Windows.Forms.CheckBox();
            this.cbActiveVarCountRoot = new System.Windows.Forms.CheckBox();
            this.buttonAddModel = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.buttonSave = new System.Windows.Forms.Button();
            this.panel5 = new System.Windows.Forms.Panel();
            this.cbStandardized = new System.Windows.Forms.CheckBox();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel4.SuspendLayout();
            this.panel3.SuspendLayout();
            this.tabControlClassifiers.SuspendLayout();
            this.tabPageSVM.SuspendLayout();
            this.tabPageDTrees.SuspendLayout();
            this.tabPageRF.SuspendLayout();
            this.panel5.SuspendLayout();
            this.SuspendLayout();
            // 
            // buttonLoadP
            // 
            this.buttonLoadP.BackColor = System.Drawing.Color.LightSteelBlue;
            this.buttonLoadP.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonLoadP.Location = new System.Drawing.Point(3, 68);
            this.buttonLoadP.Name = "buttonLoadP";
            this.buttonLoadP.Size = new System.Drawing.Size(94, 23);
            this.buttonLoadP.TabIndex = 0;
            this.buttonLoadP.Text = "Positive";
            this.buttonLoadP.UseVisualStyleBackColor = false;
            this.buttonLoadP.Click += new System.EventHandler(this.buttonLoadP_Click);
            // 
            // buttonLoadN
            // 
            this.buttonLoadN.BackColor = System.Drawing.Color.LightSteelBlue;
            this.buttonLoadN.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonLoadN.Location = new System.Drawing.Point(121, 68);
            this.buttonLoadN.Name = "buttonLoadN";
            this.buttonLoadN.Size = new System.Drawing.Size(94, 23);
            this.buttonLoadN.TabIndex = 1;
            this.buttonLoadN.Text = "Negative";
            this.buttonLoadN.UseVisualStyleBackColor = false;
            this.buttonLoadN.Click += new System.EventHandler(this.buttonLoadN_Click);
            // 
            // buttonTrain
            // 
            this.buttonTrain.BackColor = System.Drawing.Color.LightSteelBlue;
            this.buttonTrain.Dock = System.Windows.Forms.DockStyle.Top;
            this.buttonTrain.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonTrain.Location = new System.Drawing.Point(0, 25);
            this.buttonTrain.Name = "buttonTrain";
            this.buttonTrain.Size = new System.Drawing.Size(219, 25);
            this.buttonTrain.TabIndex = 2;
            this.buttonTrain.Text = "Train selected";
            this.buttonTrain.UseVisualStyleBackColor = false;
            this.buttonTrain.Click += new System.EventHandler(this.buttonTrain_Click);
            // 
            // label1
            // 
            this.label1.Dock = System.Windows.Forms.DockStyle.Top;
            this.label1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(0, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(221, 16);
            this.label1.TabIndex = 3;
            this.label1.Text = "Load training data";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel1.Controls.Add(this.checkBoxFFO);
            this.panel1.Controls.Add(this.buttonEval);
            this.panel1.Controls.Add(this.buttonTestSet);
            this.panel1.Controls.Add(this.checkBoxLWS);
            this.panel1.Controls.Add(this.checkBoxSWS);
            this.panel1.Controls.Add(this.checkBoxWS);
            this.panel1.Controls.Add(this.label6);
            this.panel1.Controls.Add(this.tbSplit);
            this.panel1.Controls.Add(this.buttonLoadN);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.buttonLoadP);
            this.panel1.Location = new System.Drawing.Point(12, 12);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(225, 159);
            this.panel1.TabIndex = 4;
            // 
            // checkBoxFFO
            // 
            this.checkBoxFFO.AutoSize = true;
            this.checkBoxFFO.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.checkBoxFFO.Location = new System.Drawing.Point(100, 45);
            this.checkBoxFFO.Name = "checkBoxFFO";
            this.checkBoxFFO.Size = new System.Drawing.Size(112, 17);
            this.checkBoxFFO.TabIndex = 12;
            this.checkBoxFFO.Text = "Full Feature Object";
            this.checkBoxFFO.UseVisualStyleBackColor = true;
            this.checkBoxFFO.CheckedChanged += new System.EventHandler(this.checkBoxFFO_CheckedChanged);
            // 
            // buttonEval
            // 
            this.buttonEval.BackColor = System.Drawing.Color.LightSteelBlue;
            this.buttonEval.Enabled = false;
            this.buttonEval.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonEval.Location = new System.Drawing.Point(121, 125);
            this.buttonEval.Name = "buttonEval";
            this.buttonEval.Size = new System.Drawing.Size(94, 23);
            this.buttonEval.TabIndex = 11;
            this.buttonEval.Text = "eval";
            this.buttonEval.UseVisualStyleBackColor = false;
            this.buttonEval.Click += new System.EventHandler(this.buttonEval_Click);
            // 
            // buttonTestSet
            // 
            this.buttonTestSet.BackColor = System.Drawing.Color.LightSteelBlue;
            this.buttonTestSet.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonTestSet.Location = new System.Drawing.Point(3, 125);
            this.buttonTestSet.Name = "buttonTestSet";
            this.buttonTestSet.Size = new System.Drawing.Size(94, 23);
            this.buttonTestSet.TabIndex = 10;
            this.buttonTestSet.Text = "Load Test Set";
            this.buttonTestSet.UseVisualStyleBackColor = false;
            this.buttonTestSet.Click += new System.EventHandler(this.buttonTestSet_Click);
            // 
            // checkBoxLWS
            // 
            this.checkBoxLWS.AutoSize = true;
            this.checkBoxLWS.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.checkBoxLWS.Location = new System.Drawing.Point(3, 45);
            this.checkBoxLWS.Name = "checkBoxLWS";
            this.checkBoxLWS.Size = new System.Drawing.Size(88, 17);
            this.checkBoxLWS.TabIndex = 8;
            this.checkBoxLWS.Text = "LineWordStat";
            this.checkBoxLWS.UseVisualStyleBackColor = true;
            this.checkBoxLWS.CheckedChanged += new System.EventHandler(this.checkBoxLWS_CheckedChanged);
            // 
            // checkBoxSWS
            // 
            this.checkBoxSWS.AutoSize = true;
            this.checkBoxSWS.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.checkBoxSWS.Location = new System.Drawing.Point(100, 22);
            this.checkBoxSWS.Name = "checkBoxSWS";
            this.checkBoxSWS.Size = new System.Drawing.Size(99, 17);
            this.checkBoxSWS.TabIndex = 7;
            this.checkBoxSWS.Text = "SimpleWordStat";
            this.checkBoxSWS.UseVisualStyleBackColor = true;
            this.checkBoxSWS.CheckedChanged += new System.EventHandler(this.checkBoxSWS_CheckedChanged);
            // 
            // checkBoxWS
            // 
            this.checkBoxWS.AutoSize = true;
            this.checkBoxWS.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.checkBoxWS.Location = new System.Drawing.Point(3, 22);
            this.checkBoxWS.Name = "checkBoxWS";
            this.checkBoxWS.Size = new System.Drawing.Size(68, 17);
            this.checkBoxWS.TabIndex = 6;
            this.checkBoxWS.Text = "WordStat";
            this.checkBoxWS.UseVisualStyleBackColor = true;
            this.checkBoxWS.CheckedChanged += new System.EventHandler(this.checkBoxWS_CheckedChanged);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label6.Location = new System.Drawing.Point(3, 100);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(90, 13);
            this.label6.TabIndex = 5;
            this.label6.Text = "Split % for testing:";
            // 
            // tbSplit
            // 
            this.tbSplit.Location = new System.Drawing.Point(121, 97);
            this.tbSplit.Name = "tbSplit";
            this.tbSplit.Size = new System.Drawing.Size(94, 20);
            this.tbSplit.TabIndex = 4;
            this.tbSplit.Text = "20";
            this.tbSplit.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // listView1
            // 
            this.listView1.BackColor = System.Drawing.Color.GhostWhite;
            this.listView1.Dock = System.Windows.Forms.DockStyle.Top;
            this.listView1.Location = new System.Drawing.Point(0, 16);
            this.listView1.Name = "listView1";
            this.listView1.Size = new System.Drawing.Size(221, 210);
            this.listView1.TabIndex = 5;
            this.listView1.UseCompatibleStateImageBehavior = false;
            this.listView1.View = System.Windows.Forms.View.List;
            this.listView1.ItemActivate += new System.EventHandler(this.listView1_ItemActivate);
            this.listView1.KeyDown += new System.Windows.Forms.KeyEventHandler(this.listView1_KeyDown);
            // 
            // label2
            // 
            this.label2.Dock = System.Windows.Forms.DockStyle.Top;
            this.label2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(0, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(221, 16);
            this.label2.TabIndex = 6;
            this.label2.Text = "Available models";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel2
            // 
            this.panel2.AutoScroll = true;
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel2.Controls.Add(this.panel4);
            this.panel2.Controls.Add(this.listView1);
            this.panel2.Controls.Add(this.label2);
            this.panel2.Location = new System.Drawing.Point(12, 177);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(225, 313);
            this.panel2.TabIndex = 7;
            // 
            // panel4
            // 
            this.panel4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel4.Controls.Add(this.buttonTrainAll);
            this.panel4.Controls.Add(this.buttonTrain);
            this.panel4.Controls.Add(this.buttonTrainRemaining);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel4.Location = new System.Drawing.Point(0, 231);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(221, 78);
            this.panel4.TabIndex = 7;
            // 
            // buttonTrainAll
            // 
            this.buttonTrainAll.BackColor = System.Drawing.Color.LightSteelBlue;
            this.buttonTrainAll.Dock = System.Windows.Forms.DockStyle.Top;
            this.buttonTrainAll.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonTrainAll.Location = new System.Drawing.Point(0, 50);
            this.buttonTrainAll.Name = "buttonTrainAll";
            this.buttonTrainAll.Size = new System.Drawing.Size(219, 27);
            this.buttonTrainAll.TabIndex = 8;
            this.buttonTrainAll.Text = "Train all";
            this.buttonTrainAll.UseVisualStyleBackColor = false;
            this.buttonTrainAll.Click += new System.EventHandler(this.buttonTrainAll_Click);
            // 
            // buttonTrainRemaining
            // 
            this.buttonTrainRemaining.BackColor = System.Drawing.Color.LightSteelBlue;
            this.buttonTrainRemaining.Dock = System.Windows.Forms.DockStyle.Top;
            this.buttonTrainRemaining.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonTrainRemaining.Location = new System.Drawing.Point(0, 0);
            this.buttonTrainRemaining.Name = "buttonTrainRemaining";
            this.buttonTrainRemaining.Size = new System.Drawing.Size(219, 25);
            this.buttonTrainRemaining.TabIndex = 7;
            this.buttonTrainRemaining.Text = "Train remaining ";
            this.buttonTrainRemaining.UseVisualStyleBackColor = false;
            this.buttonTrainRemaining.Click += new System.EventHandler(this.buttonTrainRemaining_Click);
            // 
            // panel3
            // 
            this.panel3.AutoScroll = true;
            this.panel3.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel3.Controls.Add(this.cbStandardized);
            this.panel3.Controls.Add(this.tabControlClassifiers);
            this.panel3.Controls.Add(this.buttonAddModel);
            this.panel3.Controls.Add(this.label3);
            this.panel3.Location = new System.Drawing.Point(269, 10);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(270, 482);
            this.panel3.TabIndex = 8;
            // 
            // tabControlClassifiers
            // 
            this.tabControlClassifiers.Controls.Add(this.tabPageSVM);
            this.tabControlClassifiers.Controls.Add(this.tabPageDTrees);
            this.tabControlClassifiers.Controls.Add(this.tabPageRF);
            this.tabControlClassifiers.Location = new System.Drawing.Point(3, 22);
            this.tabControlClassifiers.Name = "tabControlClassifiers";
            this.tabControlClassifiers.SelectedIndex = 0;
            this.tabControlClassifiers.Size = new System.Drawing.Size(262, 402);
            this.tabControlClassifiers.TabIndex = 6;
            // 
            // tabPageSVM
            // 
            this.tabPageSVM.BackColor = System.Drawing.Color.GhostWhite;
            this.tabPageSVM.Controls.Add(this.tbDegree);
            this.tabPageSVM.Controls.Add(this.tbCoef0);
            this.tabPageSVM.Controls.Add(this.tbNu);
            this.tabPageSVM.Controls.Add(this.tbIterations);
            this.tabPageSVM.Controls.Add(this.tbGamma);
            this.tabPageSVM.Controls.Add(this.tbC);
            this.tabPageSVM.Controls.Add(this.label12);
            this.tabPageSVM.Controls.Add(this.label11);
            this.tabPageSVM.Controls.Add(this.label10);
            this.tabPageSVM.Controls.Add(this.label9);
            this.tabPageSVM.Controls.Add(this.label8);
            this.tabPageSVM.Controls.Add(this.label7);
            this.tabPageSVM.Controls.Add(this.cbTwoClassSVM);
            this.tabPageSVM.Controls.Add(this.cbOneClassSVM);
            this.tabPageSVM.Controls.Add(this.comboBoxKernels);
            this.tabPageSVM.Controls.Add(this.label4);
            this.tabPageSVM.Controls.Add(this.cbTrainAuto);
            this.tabPageSVM.Location = new System.Drawing.Point(4, 22);
            this.tabPageSVM.Name = "tabPageSVM";
            this.tabPageSVM.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageSVM.Size = new System.Drawing.Size(254, 386);
            this.tabPageSVM.TabIndex = 0;
            this.tabPageSVM.Text = "SVM";
            // 
            // tbDegree
            // 
            this.tbDegree.Location = new System.Drawing.Point(130, 256);
            this.tbDegree.Name = "tbDegree";
            this.tbDegree.Size = new System.Drawing.Size(100, 20);
            this.tbDegree.TabIndex = 17;
            this.tbDegree.Text = "10";
            this.tbDegree.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // tbCoef0
            // 
            this.tbCoef0.Location = new System.Drawing.Point(130, 230);
            this.tbCoef0.Name = "tbCoef0";
            this.tbCoef0.Size = new System.Drawing.Size(100, 20);
            this.tbCoef0.TabIndex = 16;
            this.tbCoef0.Text = "0";
            this.tbCoef0.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // tbNu
            // 
            this.tbNu.Location = new System.Drawing.Point(130, 204);
            this.tbNu.Name = "tbNu";
            this.tbNu.Size = new System.Drawing.Size(100, 20);
            this.tbNu.TabIndex = 15;
            this.tbNu.Text = "0,1";
            this.tbNu.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // tbIterations
            // 
            this.tbIterations.Location = new System.Drawing.Point(130, 178);
            this.tbIterations.Name = "tbIterations";
            this.tbIterations.Size = new System.Drawing.Size(100, 20);
            this.tbIterations.TabIndex = 14;
            this.tbIterations.Text = "20";
            this.tbIterations.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // tbGamma
            // 
            this.tbGamma.Location = new System.Drawing.Point(130, 152);
            this.tbGamma.Name = "tbGamma";
            this.tbGamma.Size = new System.Drawing.Size(100, 20);
            this.tbGamma.TabIndex = 13;
            this.tbGamma.Text = "0,01";
            this.tbGamma.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // tbC
            // 
            this.tbC.Location = new System.Drawing.Point(130, 126);
            this.tbC.Name = "tbC";
            this.tbC.Size = new System.Drawing.Size(100, 20);
            this.tbC.TabIndex = 12;
            this.tbC.Text = "2";
            this.tbC.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label12.Location = new System.Drawing.Point(20, 259);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(42, 13);
            this.label12.TabIndex = 11;
            this.label12.Text = "Degree";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label11.Location = new System.Drawing.Point(20, 233);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(35, 13);
            this.label11.TabIndex = 10;
            this.label11.Text = "Coef0";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label10.Location = new System.Drawing.Point(20, 207);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(21, 13);
            this.label10.TabIndex = 9;
            this.label10.Text = "Nu";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label9.Location = new System.Drawing.Point(20, 181);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(102, 13);
            this.label9.TabIndex = 8;
            this.label9.Text = "Number of Iterations";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label8.Location = new System.Drawing.Point(20, 155);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(43, 13);
            this.label8.TabIndex = 7;
            this.label8.Text = "Gamma";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label7.Location = new System.Drawing.Point(20, 129);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(14, 13);
            this.label7.TabIndex = 6;
            this.label7.Text = "C";
            // 
            // cbTwoClassSVM
            // 
            this.cbTwoClassSVM.AutoSize = true;
            this.cbTwoClassSVM.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cbTwoClassSVM.Location = new System.Drawing.Point(130, 98);
            this.cbTwoClassSVM.Name = "cbTwoClassSVM";
            this.cbTwoClassSVM.Size = new System.Drawing.Size(98, 17);
            this.cbTwoClassSVM.TabIndex = 5;
            this.cbTwoClassSVM.Text = "Two Class SVM";
            this.cbTwoClassSVM.UseVisualStyleBackColor = true;
            this.cbTwoClassSVM.CheckedChanged += new System.EventHandler(this.cbTwoClassSVM_CheckedChanged);
            // 
            // cbOneClassSVM
            // 
            this.cbOneClassSVM.AutoSize = true;
            this.cbOneClassSVM.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cbOneClassSVM.Location = new System.Drawing.Point(16, 98);
            this.cbOneClassSVM.Name = "cbOneClassSVM";
            this.cbOneClassSVM.Size = new System.Drawing.Size(97, 17);
            this.cbOneClassSVM.TabIndex = 4;
            this.cbOneClassSVM.Text = "One Class SVM";
            this.cbOneClassSVM.UseVisualStyleBackColor = true;
            this.cbOneClassSVM.CheckedChanged += new System.EventHandler(this.cbOneClassSVM_CheckedChanged);
            // 
            // comboBoxKernels
            // 
            this.comboBoxKernels.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.comboBoxKernels.FormattingEnabled = true;
            this.comboBoxKernels.Items.AddRange(new object[] {
            "Chi2",
            "Inter",
            "Linear",
            "Poly",
            "Rbf",
            "Sigmoid"});
            this.comboBoxKernels.Location = new System.Drawing.Point(121, 53);
            this.comboBoxKernels.Name = "comboBoxKernels";
            this.comboBoxKernels.Size = new System.Drawing.Size(121, 21);
            this.comboBoxKernels.TabIndex = 3;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label4.Location = new System.Drawing.Point(6, 56);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(107, 13);
            this.label4.TabIndex = 2;
            this.label4.Text = "Choose a kernel type";
            // 
            // cbTrainAuto
            // 
            this.cbTrainAuto.AutoSize = true;
            this.cbTrainAuto.Checked = true;
            this.cbTrainAuto.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbTrainAuto.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cbTrainAuto.Location = new System.Drawing.Point(6, 6);
            this.cbTrainAuto.Name = "cbTrainAuto";
            this.cbTrainAuto.Size = new System.Drawing.Size(69, 17);
            this.cbTrainAuto.TabIndex = 0;
            this.cbTrainAuto.Text = "TrainAuto";
            this.cbTrainAuto.UseVisualStyleBackColor = true;
            // 
            // tabPageDTrees
            // 
            this.tabPageDTrees.BackColor = System.Drawing.Color.GhostWhite;
            this.tabPageDTrees.Controls.Add(this.cbUseSurrogates);
            this.tabPageDTrees.Controls.Add(this.cbUseSE1Rule);
            this.tabPageDTrees.Controls.Add(this.cbTruncatePrunedTree);
            this.tabPageDTrees.Controls.Add(this.tbCVFolds);
            this.tabPageDTrees.Controls.Add(this.tbMinSampleCount);
            this.tabPageDTrees.Controls.Add(this.tbMaxDepth);
            this.tabPageDTrees.Controls.Add(this.label15);
            this.tabPageDTrees.Controls.Add(this.label14);
            this.tabPageDTrees.Controls.Add(this.label13);
            this.tabPageDTrees.Location = new System.Drawing.Point(4, 22);
            this.tabPageDTrees.Name = "tabPageDTrees";
            this.tabPageDTrees.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageDTrees.Size = new System.Drawing.Size(254, 386);
            this.tabPageDTrees.TabIndex = 1;
            this.tabPageDTrees.Text = "Decision trees";
            // 
            // cbUseSurrogates
            // 
            this.cbUseSurrogates.AutoSize = true;
            this.cbUseSurrogates.Enabled = false;
            this.cbUseSurrogates.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cbUseSurrogates.Location = new System.Drawing.Point(9, 52);
            this.cbUseSurrogates.Name = "cbUseSurrogates";
            this.cbUseSurrogates.Size = new System.Drawing.Size(96, 17);
            this.cbUseSurrogates.TabIndex = 8;
            this.cbUseSurrogates.Text = "Use Surrogates";
            this.cbUseSurrogates.UseVisualStyleBackColor = true;
            // 
            // cbUseSE1Rule
            // 
            this.cbUseSE1Rule.AutoSize = true;
            this.cbUseSE1Rule.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cbUseSE1Rule.Location = new System.Drawing.Point(9, 29);
            this.cbUseSE1Rule.Name = "cbUseSE1Rule";
            this.cbUseSE1Rule.Size = new System.Drawing.Size(90, 17);
            this.cbUseSE1Rule.TabIndex = 7;
            this.cbUseSE1Rule.Text = "Use SE1 Rule";
            this.cbUseSE1Rule.UseVisualStyleBackColor = true;
            // 
            // cbTruncatePrunedTree
            // 
            this.cbTruncatePrunedTree.AutoSize = true;
            this.cbTruncatePrunedTree.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cbTruncatePrunedTree.Location = new System.Drawing.Point(9, 6);
            this.cbTruncatePrunedTree.Name = "cbTruncatePrunedTree";
            this.cbTruncatePrunedTree.Size = new System.Drawing.Size(123, 17);
            this.cbTruncatePrunedTree.TabIndex = 6;
            this.cbTruncatePrunedTree.Text = "Truncate pruned tree";
            this.cbTruncatePrunedTree.UseVisualStyleBackColor = true;
            // 
            // tbCVFolds
            // 
            this.tbCVFolds.Location = new System.Drawing.Point(141, 129);
            this.tbCVFolds.Name = "tbCVFolds";
            this.tbCVFolds.Size = new System.Drawing.Size(100, 20);
            this.tbCVFolds.TabIndex = 5;
            this.tbCVFolds.Text = "0";
            this.tbCVFolds.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // tbMinSampleCount
            // 
            this.tbMinSampleCount.Location = new System.Drawing.Point(141, 103);
            this.tbMinSampleCount.Name = "tbMinSampleCount";
            this.tbMinSampleCount.Size = new System.Drawing.Size(100, 20);
            this.tbMinSampleCount.TabIndex = 4;
            this.tbMinSampleCount.Text = "2";
            this.tbMinSampleCount.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // tbMaxDepth
            // 
            this.tbMaxDepth.Location = new System.Drawing.Point(141, 77);
            this.tbMaxDepth.Name = "tbMaxDepth";
            this.tbMaxDepth.Size = new System.Drawing.Size(100, 20);
            this.tbMaxDepth.TabIndex = 3;
            this.tbMaxDepth.Text = "8";
            this.tbMaxDepth.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label15.Location = new System.Drawing.Point(7, 132);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(110, 13);
            this.label15.TabIndex = 2;
            this.label15.Text = "Cross Validation Folds";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label14.Location = new System.Drawing.Point(7, 106);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(89, 13);
            this.label14.TabIndex = 1;
            this.label14.Text = "Min # of Samples";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label13.Location = new System.Drawing.Point(7, 80);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(92, 13);
            this.label13.TabIndex = 0;
            this.label13.Text = "Max Depth of tree";
            // 
            // tabPageRF
            // 
            this.tabPageRF.BackColor = System.Drawing.Color.GhostWhite;
            this.tabPageRF.Controls.Add(this.tbFolds);
            this.tabPageRF.Controls.Add(this.tbIters);
            this.tabPageRF.Controls.Add(this.tbMaxDepthR);
            this.tabPageRF.Controls.Add(this.label16);
            this.tabPageRF.Controls.Add(this.label17);
            this.tabPageRF.Controls.Add(this.label18);
            this.tabPageRF.Controls.Add(this.cbUseSurrogatesR);
            this.tabPageRF.Controls.Add(this.cbUseSE1RuleR);
            this.tabPageRF.Controls.Add(this.cbTruncatePrunedTreeR);
            this.tabPageRF.Controls.Add(this.cbVarImportance);
            this.tabPageRF.Controls.Add(this.cbActiveVarCountAll);
            this.tabPageRF.Controls.Add(this.cbActiveVarCountRoot);
            this.tabPageRF.Location = new System.Drawing.Point(4, 22);
            this.tabPageRF.Name = "tabPageRF";
            this.tabPageRF.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageRF.Size = new System.Drawing.Size(254, 376);
            this.tabPageRF.TabIndex = 2;
            this.tabPageRF.Text = "Random Forests";
            // 
            // tbFolds
            // 
            this.tbFolds.Location = new System.Drawing.Point(140, 171);
            this.tbFolds.Name = "tbFolds";
            this.tbFolds.Size = new System.Drawing.Size(100, 20);
            this.tbFolds.TabIndex = 17;
            this.tbFolds.Text = "0";
            this.tbFolds.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // tbIters
            // 
            this.tbIters.Location = new System.Drawing.Point(140, 197);
            this.tbIters.Name = "tbIters";
            this.tbIters.Size = new System.Drawing.Size(100, 20);
            this.tbIters.TabIndex = 16;
            this.tbIters.Text = "2";
            this.tbIters.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // tbMaxDepthR
            // 
            this.tbMaxDepthR.Location = new System.Drawing.Point(140, 145);
            this.tbMaxDepthR.Name = "tbMaxDepthR";
            this.tbMaxDepthR.Size = new System.Drawing.Size(100, 20);
            this.tbMaxDepthR.TabIndex = 15;
            this.tbMaxDepthR.Text = "8";
            this.tbMaxDepthR.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label16.Location = new System.Drawing.Point(6, 174);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(110, 13);
            this.label16.TabIndex = 14;
            this.label16.Text = "Cross Validation Folds";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label17.Location = new System.Drawing.Point(6, 200);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(73, 13);
            this.label17.TabIndex = 13;
            this.label17.Text = "Max Iterations";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label18.Location = new System.Drawing.Point(6, 148);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(92, 13);
            this.label18.TabIndex = 12;
            this.label18.Text = "Max Depth of tree";
            // 
            // cbUseSurrogatesR
            // 
            this.cbUseSurrogatesR.AutoSize = true;
            this.cbUseSurrogatesR.Enabled = false;
            this.cbUseSurrogatesR.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cbUseSurrogatesR.Location = new System.Drawing.Point(6, 121);
            this.cbUseSurrogatesR.Name = "cbUseSurrogatesR";
            this.cbUseSurrogatesR.Size = new System.Drawing.Size(96, 17);
            this.cbUseSurrogatesR.TabIndex = 11;
            this.cbUseSurrogatesR.Text = "Use Surrogates";
            this.cbUseSurrogatesR.UseVisualStyleBackColor = true;
            // 
            // cbUseSE1RuleR
            // 
            this.cbUseSE1RuleR.AutoSize = true;
            this.cbUseSE1RuleR.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cbUseSE1RuleR.Location = new System.Drawing.Point(6, 98);
            this.cbUseSE1RuleR.Name = "cbUseSE1RuleR";
            this.cbUseSE1RuleR.Size = new System.Drawing.Size(90, 17);
            this.cbUseSE1RuleR.TabIndex = 10;
            this.cbUseSE1RuleR.Text = "Use SE1 Rule";
            this.cbUseSE1RuleR.UseVisualStyleBackColor = true;
            // 
            // cbTruncatePrunedTreeR
            // 
            this.cbTruncatePrunedTreeR.AutoSize = true;
            this.cbTruncatePrunedTreeR.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cbTruncatePrunedTreeR.Location = new System.Drawing.Point(6, 75);
            this.cbTruncatePrunedTreeR.Name = "cbTruncatePrunedTreeR";
            this.cbTruncatePrunedTreeR.Size = new System.Drawing.Size(123, 17);
            this.cbTruncatePrunedTreeR.TabIndex = 9;
            this.cbTruncatePrunedTreeR.Text = "Truncate pruned tree";
            this.cbTruncatePrunedTreeR.UseVisualStyleBackColor = true;
            // 
            // cbVarImportance
            // 
            this.cbVarImportance.AutoSize = true;
            this.cbVarImportance.Checked = true;
            this.cbVarImportance.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbVarImportance.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cbVarImportance.Location = new System.Drawing.Point(6, 52);
            this.cbVarImportance.Name = "cbVarImportance";
            this.cbVarImportance.Size = new System.Drawing.Size(162, 17);
            this.cbVarImportance.TabIndex = 2;
            this.cbVarImportance.Text = "Calculate variable importance";
            this.cbVarImportance.UseVisualStyleBackColor = true;
            // 
            // cbActiveVarCountAll
            // 
            this.cbActiveVarCountAll.AutoSize = true;
            this.cbActiveVarCountAll.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cbActiveVarCountAll.Location = new System.Drawing.Point(6, 29);
            this.cbActiveVarCountAll.Name = "cbActiveVarCountAll";
            this.cbActiveVarCountAll.Size = new System.Drawing.Size(79, 17);
            this.cbActiveVarCountAll.TabIndex = 1;
            this.cbActiveVarCountAll.Text = "All variables";
            this.cbActiveVarCountAll.UseVisualStyleBackColor = true;
            this.cbActiveVarCountAll.CheckedChanged += new System.EventHandler(this.cbActiveVarCountAll_CheckedChanged);
            // 
            // cbActiveVarCountRoot
            // 
            this.cbActiveVarCountRoot.AutoSize = true;
            this.cbActiveVarCountRoot.Checked = true;
            this.cbActiveVarCountRoot.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbActiveVarCountRoot.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cbActiveVarCountRoot.Location = new System.Drawing.Point(6, 6);
            this.cbActiveVarCountRoot.Name = "cbActiveVarCountRoot";
            this.cbActiveVarCountRoot.Size = new System.Drawing.Size(81, 17);
            this.cbActiveVarCountRoot.TabIndex = 0;
            this.cbActiveVarCountRoot.Text = "Sqrt (# vars)";
            this.cbActiveVarCountRoot.UseVisualStyleBackColor = true;
            this.cbActiveVarCountRoot.CheckedChanged += new System.EventHandler(this.cbActiveVarCountRoot_CheckedChanged);
            // 
            // buttonAddModel
            // 
            this.buttonAddModel.BackColor = System.Drawing.Color.LightSteelBlue;
            this.buttonAddModel.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.buttonAddModel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonAddModel.Location = new System.Drawing.Point(0, 456);
            this.buttonAddModel.Name = "buttonAddModel";
            this.buttonAddModel.Size = new System.Drawing.Size(266, 22);
            this.buttonAddModel.TabIndex = 1;
            this.buttonAddModel.Text = "Add model";
            this.buttonAddModel.UseVisualStyleBackColor = false;
            this.buttonAddModel.Click += new System.EventHandler(this.buttonAddModel_Click);
            // 
            // label3
            // 
            this.label3.Dock = System.Windows.Forms.DockStyle.Top;
            this.label3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(0, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(266, 16);
            this.label3.TabIndex = 0;
            this.label3.Text = "Choose model parameters";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // textBox1
            // 
            this.textBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.textBox1.Location = new System.Drawing.Point(0, 0);
            this.textBox1.Multiline = true;
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(221, 453);
            this.textBox1.TabIndex = 9;
            // 
            // buttonSave
            // 
            this.buttonSave.BackColor = System.Drawing.Color.LightSteelBlue;
            this.buttonSave.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.buttonSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonSave.Location = new System.Drawing.Point(0, 458);
            this.buttonSave.Name = "buttonSave";
            this.buttonSave.Size = new System.Drawing.Size(221, 22);
            this.buttonSave.TabIndex = 10;
            this.buttonSave.Text = "Save current model";
            this.buttonSave.UseVisualStyleBackColor = false;
            this.buttonSave.Click += new System.EventHandler(this.buttonSave_Click);
            // 
            // panel5
            // 
            this.panel5.Controls.Add(this.buttonSave);
            this.panel5.Controls.Add(this.textBox1);
            this.panel5.Location = new System.Drawing.Point(580, 10);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(221, 480);
            this.panel5.TabIndex = 9;
            // 
            // cbStandardized
            // 
            this.cbStandardized.AutoSize = true;
            this.cbStandardized.BackColor = System.Drawing.Color.LightSteelBlue;
            this.cbStandardized.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cbStandardized.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbStandardized.Location = new System.Drawing.Point(13, 430);
            this.cbStandardized.Name = "cbStandardized";
            this.cbStandardized.Size = new System.Drawing.Size(134, 22);
            this.cbStandardized.TabIndex = 7;
            this.cbStandardized.Text = "Standardize data";
            this.cbStandardized.UseVisualStyleBackColor = false;
            // 
            // ModelCreatorForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.BackColor = System.Drawing.Color.GhostWhite;
            this.ClientSize = new System.Drawing.Size(813, 505);
            this.Controls.Add(this.panel5);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.SizableToolWindow;
            this.Name = "ModelCreatorForm";
            this.Text = "ModelCreatorForm";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.tabControlClassifiers.ResumeLayout(false);
            this.tabPageSVM.ResumeLayout(false);
            this.tabPageSVM.PerformLayout();
            this.tabPageDTrees.ResumeLayout(false);
            this.tabPageDTrees.PerformLayout();
            this.tabPageRF.ResumeLayout(false);
            this.tabPageRF.PerformLayout();
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button buttonLoadP;
        private System.Windows.Forms.Button buttonLoadN;
        private System.Windows.Forms.Button buttonTrain;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.ListView listView1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Button buttonAddModel;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Button buttonSave;
        private System.Windows.Forms.Button buttonTrainRemaining;
        private System.Windows.Forms.Button buttonTrainAll;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox tbSplit;
        private System.Windows.Forms.CheckBox checkBoxLWS;
        private System.Windows.Forms.CheckBox checkBoxSWS;
        private System.Windows.Forms.CheckBox checkBoxWS;
        private System.Windows.Forms.Button buttonTestSet;
        private System.Windows.Forms.Button buttonEval;
        private System.Windows.Forms.CheckBox checkBoxFFO;
        private System.Windows.Forms.TabControl tabControlClassifiers;
        private System.Windows.Forms.TabPage tabPageSVM;
        private System.Windows.Forms.TabPage tabPageDTrees;
        private System.Windows.Forms.TabPage tabPageRF;
        private System.Windows.Forms.CheckBox cbTrainAuto;
        private System.Windows.Forms.ComboBox comboBoxKernels;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.CheckBox cbTwoClassSVM;
        private System.Windows.Forms.CheckBox cbOneClassSVM;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox tbDegree;
        private System.Windows.Forms.TextBox tbCoef0;
        private System.Windows.Forms.TextBox tbNu;
        private System.Windows.Forms.TextBox tbIterations;
        private System.Windows.Forms.TextBox tbGamma;
        private System.Windows.Forms.TextBox tbC;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox tbCVFolds;
        private System.Windows.Forms.TextBox tbMinSampleCount;
        private System.Windows.Forms.TextBox tbMaxDepth;
        private System.Windows.Forms.CheckBox cbUseSurrogates;
        private System.Windows.Forms.CheckBox cbUseSE1Rule;
        private System.Windows.Forms.CheckBox cbTruncatePrunedTree;
        private System.Windows.Forms.CheckBox cbVarImportance;
        private System.Windows.Forms.CheckBox cbActiveVarCountAll;
        private System.Windows.Forms.CheckBox cbActiveVarCountRoot;
        private System.Windows.Forms.CheckBox cbUseSurrogatesR;
        private System.Windows.Forms.CheckBox cbUseSE1RuleR;
        private System.Windows.Forms.CheckBox cbTruncatePrunedTreeR;
        private System.Windows.Forms.TextBox tbFolds;
        private System.Windows.Forms.TextBox tbMaxDepthR;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.TextBox tbIters;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.CheckBox cbStandardized;
    }
}