﻿namespace WordLocRec.Forms
{
    partial class TrainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.AddToNoiseButton = new System.Windows.Forms.Button();
            this.AddToLettersButton = new System.Windows.Forms.Button();
            this.textBoxNumber = new System.Windows.Forms.TextBox();
            this.DontAddAnywhereButton = new System.Windows.Forms.Button();
            this.pictureBoxSample = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxSample)).BeginInit();
            this.SuspendLayout();
            // 
            // pictureBox1
            // 
            this.pictureBox1.Location = new System.Drawing.Point(12, 2);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(288, 251);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 1;
            this.pictureBox1.TabStop = false;
            // 
            // AddToNoiseButton
            // 
            this.AddToNoiseButton.Location = new System.Drawing.Point(12, 269);
            this.AddToNoiseButton.Name = "AddToNoiseButton";
            this.AddToNoiseButton.Size = new System.Drawing.Size(116, 77);
            this.AddToNoiseButton.TabIndex = 2;
            this.AddToNoiseButton.Text = "Class1 - NOISE";
            this.AddToNoiseButton.UseVisualStyleBackColor = true;
            this.AddToNoiseButton.Click += new System.EventHandler(this.AddToNoiseButton_Click);
            // 
            // AddToLettersButton
            // 
            this.AddToLettersButton.Location = new System.Drawing.Point(210, 269);
            this.AddToLettersButton.Name = "AddToLettersButton";
            this.AddToLettersButton.Size = new System.Drawing.Size(117, 77);
            this.AddToLettersButton.TabIndex = 3;
            this.AddToLettersButton.Text = "class2 - Clear Letter";
            this.AddToLettersButton.UseVisualStyleBackColor = true;
            this.AddToLettersButton.Click += new System.EventHandler(this.AddToLettersButton_Click);
            // 
            // textBoxNumber
            // 
            this.textBoxNumber.Location = new System.Drawing.Point(533, 269);
            this.textBoxNumber.Name = "textBoxNumber";
            this.textBoxNumber.Size = new System.Drawing.Size(70, 20);
            this.textBoxNumber.TabIndex = 4;
            // 
            // DontAddAnywhereButton
            // 
            this.DontAddAnywhereButton.Location = new System.Drawing.Point(457, 282);
            this.DontAddAnywhereButton.Name = "DontAddAnywhereButton";
            this.DontAddAnywhereButton.Size = new System.Drawing.Size(70, 50);
            this.DontAddAnywhereButton.TabIndex = 5;
            this.DontAddAnywhereButton.Text = "Ambiguous - Dont add";
            this.DontAddAnywhereButton.UseVisualStyleBackColor = true;
            this.DontAddAnywhereButton.Click += new System.EventHandler(this.DontAddAnywhereButton_Click);
            // 
            // pictureBoxSample
            // 
            this.pictureBoxSample.Location = new System.Drawing.Point(315, 2);
            this.pictureBoxSample.Name = "pictureBoxSample";
            this.pictureBoxSample.Size = new System.Drawing.Size(288, 251);
            this.pictureBoxSample.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBoxSample.TabIndex = 6;
            this.pictureBoxSample.TabStop = false;
            // 
            // TrainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(615, 358);
            this.Controls.Add(this.pictureBoxSample);
            this.Controls.Add(this.DontAddAnywhereButton);
            this.Controls.Add(this.textBoxNumber);
            this.Controls.Add(this.AddToLettersButton);
            this.Controls.Add(this.AddToNoiseButton);
            this.Controls.Add(this.pictureBox1);
            this.Name = "TrainForm";
            this.Text = "TrainForm";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxSample)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Button AddToNoiseButton;
        private System.Windows.Forms.Button AddToLettersButton;
        private System.Windows.Forms.TextBox textBoxNumber;
        private System.Windows.Forms.Button DontAddAnywhereButton;
        private System.Windows.Forms.PictureBox pictureBoxSample;
    }
}