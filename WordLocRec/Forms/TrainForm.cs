﻿using Emgu.CV;
using Emgu.CV.Util;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WordLocRec.Forms
{
    public partial class TrainForm : Form
    {
        private static Size s = new Size(30, 30);
        public VectorOfMat erMats, noiseMats, letterMats;
        int viewIndex = 0;
        Mat current;
        Mat currentSample;

        public TrainForm(VectorOfMat erMats)
        {
            InitializeComponent();
            this.erMats = new VectorOfMat();

            for (int i = 0; i < erMats.Size; i++)
                this.erMats.Push(erMats[i].Clone());

            viewIndex = 0;
            noiseMats = new VectorOfMat();
            letterMats = new VectorOfMat();

            createNextSampleAndShow();
        }

        private void createNextSampleAndShow()
        {
            current = erMats[viewIndex];
            TransformMatToTrainSample(current, out currentSample);
            setBitmapInPictureBox(current.Bitmap, currentSample.Bitmap);
        }
        private void TransformMatToTrainSample(Mat mat, out Mat result)
        {
            result = new Mat(s, Emgu.CV.CvEnum.DepthType.Cv8U, 1);
            CvInvoke.Resize(mat, result, s);
            CvInvoke.Threshold(result, result, 127, 255, Emgu.CV.CvEnum.ThresholdType.Binary);
        }
        private void setBitmapInPictureBox(Bitmap bitmap, Bitmap sample)
        {
            pictureBox1.Image = bitmap;
            pictureBoxSample.Image = sample;
            textBoxNumber.Text = viewIndex.ToString();
        }

        private void AddToNoiseButton_Click(object sender, EventArgs e)
        {
            noiseMats.Push(currentSample);
            increaseIndex();
            if (viewIndex == 0) // shmainei oti teleiwsan ola ta mats kai pame gia na ksekinhsoume apo thn arxh
            {
                MessageBox.Show("All train data have been labeled.\nClose this form to continue.");
                AddToNoiseButton.Enabled = false;
                AddToLettersButton.Enabled = false;
                DontAddAnywhereButton.Enabled = false;
                this.DialogResult = System.Windows.Forms.DialogResult.OK;
            }
            else
            {
                createNextSampleAndShow();
            }
        }

        private void AddToLettersButton_Click(object sender, EventArgs e)
        {
            letterMats.Push(currentSample);
            increaseIndex();
            if (viewIndex == 0) // shmainei oti teleiwsan ola ta mats kai pame gia na ksekinhsoume apo thn arxh
            {
                MessageBox.Show("All train data have been labeled.\nClose this form to continue.");
                AddToNoiseButton.Enabled = false;
                AddToLettersButton.Enabled = false;
                DontAddAnywhereButton.Enabled = false;
                this.DialogResult = System.Windows.Forms.DialogResult.OK;
            }
            else
            {
                createNextSampleAndShow();
            }
        }

        private void increaseIndex()
        {
            viewIndex = viewIndex + 1 >= erMats.Size ? 0 : viewIndex + 1;
        }

        private void DontAddAnywhereButton_Click(object sender, EventArgs e)
        {
            increaseIndex();
            createNextSampleAndShow();
        }
    }
}
