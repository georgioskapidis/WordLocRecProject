﻿using Emgu.CV;
using Emgu.CV.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WordLocRec.Forms
{
    public static class FormUtils
    {
        public static void VisualizeErRects(Mat m, List<ERStat> regions, int channelid)
        {
            VectorOfMat erMats = new VectorOfMat();
            foreach (ERStat r in regions)
                erMats.Push(new Mat(m, r.rect));
            WordRectangleForm wr = new WordRectangleForm(erMats, "er rect mats for channel: " + channelid, regions);
            wr.Show();
        }
        public static void VisualizeErPixels(Mat m, List<ERStat> regions)
        {
            VectorOfMat erMats = new VectorOfMat();
            Utils.MultipleMatsFromERPixelsFloodFill(m, regions, ref erMats, true);
            WordRectangleForm wr = new WordRectangleForm(erMats, "er mats", regions);
            wr.Show();
        }
        public static void VisualizeErPixels(Mat m, List<ERStat> regions, List<String> texts)
        {
            VectorOfMat erMats = new VectorOfMat();
            Utils.MultipleMatsFromERPixelsFloodFill(m, regions, ref erMats, true);
            WordRectangleForm wr = new WordRectangleForm(erMats, "er mats", regions, texts);
            wr.Show();
        }

        public static String[] ParametrizedOpenFileDialog(String filter, bool multiselect, String title = "", bool initDirectoryResources = false )
        {
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.InitialDirectory = initDirectoryResources ? Constants.PathResources : Constants.PathDesktop;
            ofd.Multiselect = multiselect;
            ofd.Title = title;
            ofd.Filter = filter;
            ofd.RestoreDirectory = true;
            if (ofd.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                return ofd.FileNames;
            }
            if (!multiselect) return new String[1];
            return null;
        }

        public static String ParametrizedFolderBrowserDialog(String title = "")
        {
            FolderBrowserDialog fbd = new FolderBrowserDialog();
            fbd.Description = title == "" ? "Select a folder" : title;
            fbd.RootFolder = Environment.SpecialFolder.Desktop;
            if (fbd.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                return fbd.SelectedPath;
            }
            else return null;
        }

        public static void CheckBoxesMutuallyExclusive(CheckBox cb, params CheckBox[] cbs)
        {
            if (cb.Checked == true)
            {
                for (int i = 0; i < cbs.Length; i++)
                    cbs[i].Checked = false;
            }
        }
    }
}
