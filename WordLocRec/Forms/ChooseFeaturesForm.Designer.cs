﻿namespace WordLocRec.Forms
{
    partial class ChooseFeaturesForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panelBoxes = new System.Windows.Forms.Panel();
            this.checkBoxStrokeWidthRatio = new System.Windows.Forms.CheckBox();
            this.checkBoxPaths = new System.Windows.Forms.CheckBox();
            this.checkBoxConvDefects = new System.Windows.Forms.CheckBox();
            this.checkBoxLab = new System.Windows.Forms.CheckBox();
            this.checkBoxPii = new System.Windows.Forms.CheckBox();
            this.checkBoxHistograms = new System.Windows.Forms.CheckBox();
            this.checkBoxNM = new System.Windows.Forms.CheckBox();
            this.checkBoxStand = new System.Windows.Forms.CheckBox();
            this.buttonOk = new System.Windows.Forms.Button();
            this.checkBoxHoles = new System.Windows.Forms.CheckBox();
            this.checkBoxBgr = new System.Windows.Forms.CheckBox();
            this.checkBoxLabCon = new System.Windows.Forms.CheckBox();
            this.panelBoxes.SuspendLayout();
            this.SuspendLayout();
            // 
            // panelBoxes
            // 
            this.panelBoxes.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.panelBoxes.Controls.Add(this.checkBoxStrokeWidthRatio);
            this.panelBoxes.Controls.Add(this.checkBoxPaths);
            this.panelBoxes.Controls.Add(this.checkBoxConvDefects);
            this.panelBoxes.Controls.Add(this.checkBoxLab);
            this.panelBoxes.Controls.Add(this.checkBoxPii);
            this.panelBoxes.Controls.Add(this.checkBoxHistograms);
            this.panelBoxes.Controls.Add(this.checkBoxNM);
            this.panelBoxes.Location = new System.Drawing.Point(12, 12);
            this.panelBoxes.Name = "panelBoxes";
            this.panelBoxes.Size = new System.Drawing.Size(150, 174);
            this.panelBoxes.TabIndex = 0;
            // 
            // checkBoxStrokeWidthRatio
            // 
            this.checkBoxStrokeWidthRatio.AutoSize = true;
            this.checkBoxStrokeWidthRatio.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.checkBoxStrokeWidthRatio.Location = new System.Drawing.Point(4, 142);
            this.checkBoxStrokeWidthRatio.Name = "checkBoxStrokeWidthRatio";
            this.checkBoxStrokeWidthRatio.Size = new System.Drawing.Size(139, 17);
            this.checkBoxStrokeWidthRatio.TabIndex = 7;
            this.checkBoxStrokeWidthRatio.Text = "Stroke Width / Width (1)";
            this.checkBoxStrokeWidthRatio.UseVisualStyleBackColor = true;
            // 
            // checkBoxPaths
            // 
            this.checkBoxPaths.AutoSize = true;
            this.checkBoxPaths.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.checkBoxPaths.Location = new System.Drawing.Point(4, 96);
            this.checkBoxPaths.Name = "checkBoxPaths";
            this.checkBoxPaths.Size = new System.Drawing.Size(117, 17);
            this.checkBoxPaths.TabIndex = 4;
            this.checkBoxPaths.Text = "Significant Paths (4)";
            this.checkBoxPaths.UseVisualStyleBackColor = true;
            // 
            // checkBoxConvDefects
            // 
            this.checkBoxConvDefects.AutoSize = true;
            this.checkBoxConvDefects.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.checkBoxConvDefects.Location = new System.Drawing.Point(4, 119);
            this.checkBoxConvDefects.Name = "checkBoxConvDefects";
            this.checkBoxConvDefects.Size = new System.Drawing.Size(124, 17);
            this.checkBoxConvDefects.TabIndex = 6;
            this.checkBoxConvDefects.Text = "Convexity Defects (1)";
            this.checkBoxConvDefects.UseVisualStyleBackColor = true;
            // 
            // checkBoxLab
            // 
            this.checkBoxLab.AutoSize = true;
            this.checkBoxLab.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.checkBoxLab.Location = new System.Drawing.Point(4, 73);
            this.checkBoxLab.Name = "checkBoxLab";
            this.checkBoxLab.Size = new System.Drawing.Size(128, 17);
            this.checkBoxLab.TabIndex = 3;
            this.checkBoxLab.Text = "Lab Color Distance (1)";
            this.checkBoxLab.UseVisualStyleBackColor = true;
            // 
            // checkBoxPii
            // 
            this.checkBoxPii.AutoSize = true;
            this.checkBoxPii.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.checkBoxPii.Location = new System.Drawing.Point(4, 50);
            this.checkBoxPii.Name = "checkBoxPii";
            this.checkBoxPii.Size = new System.Drawing.Size(121, 17);
            this.checkBoxPii.TabIndex = 2;
            this.checkBoxPii.Text = "Pii Color Distance (1)";
            this.checkBoxPii.UseVisualStyleBackColor = true;
            // 
            // checkBoxHistograms
            // 
            this.checkBoxHistograms.AutoSize = true;
            this.checkBoxHistograms.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.checkBoxHistograms.Location = new System.Drawing.Point(4, 27);
            this.checkBoxHistograms.Name = "checkBoxHistograms";
            this.checkBoxHistograms.Size = new System.Drawing.Size(90, 17);
            this.checkBoxHistograms.TabIndex = 1;
            this.checkBoxHistograms.Text = "Histograms (4)";
            this.checkBoxHistograms.UseVisualStyleBackColor = true;
            // 
            // checkBoxNM
            // 
            this.checkBoxNM.AutoSize = true;
            this.checkBoxNM.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.checkBoxNM.Location = new System.Drawing.Point(4, 4);
            this.checkBoxNM.Name = "checkBoxNM";
            this.checkBoxNM.Size = new System.Drawing.Size(110, 17);
            this.checkBoxNM.TabIndex = 0;
            this.checkBoxNM.Text = "Neuman Matas (7)";
            this.checkBoxNM.UseVisualStyleBackColor = true;
            // 
            // checkBoxStand
            // 
            this.checkBoxStand.AutoSize = true;
            this.checkBoxStand.Checked = true;
            this.checkBoxStand.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxStand.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.checkBoxStand.Location = new System.Drawing.Point(16, 232);
            this.checkBoxStand.Name = "checkBoxStand";
            this.checkBoxStand.Size = new System.Drawing.Size(143, 17);
            this.checkBoxStand.TabIndex = 1;
            this.checkBoxStand.Text = "Standardized Per Feature";
            this.checkBoxStand.UseVisualStyleBackColor = true;
            // 
            // buttonOk
            // 
            this.buttonOk.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonOk.BackColor = System.Drawing.Color.LightSteelBlue;
            this.buttonOk.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonOk.Location = new System.Drawing.Point(242, 226);
            this.buttonOk.Name = "buttonOk";
            this.buttonOk.Size = new System.Drawing.Size(75, 23);
            this.buttonOk.TabIndex = 2;
            this.buttonOk.Text = "OK";
            this.buttonOk.UseVisualStyleBackColor = false;
            this.buttonOk.Click += new System.EventHandler(this.buttonOk_Click);
            // 
            // checkBoxHoles
            // 
            this.checkBoxHoles.AutoSize = true;
            this.checkBoxHoles.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.checkBoxHoles.Location = new System.Drawing.Point(171, 16);
            this.checkBoxHoles.Name = "checkBoxHoles";
            this.checkBoxHoles.Size = new System.Drawing.Size(115, 17);
            this.checkBoxHoles.TabIndex = 3;
            this.checkBoxHoles.Text = "Number of holes (1)";
            this.checkBoxHoles.UseVisualStyleBackColor = true;
            // 
            // checkBoxBgr
            // 
            this.checkBoxBgr.AutoSize = true;
            this.checkBoxBgr.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.checkBoxBgr.Location = new System.Drawing.Point(171, 39);
            this.checkBoxBgr.Name = "checkBoxBgr";
            this.checkBoxBgr.Size = new System.Drawing.Size(126, 17);
            this.checkBoxBgr.TabIndex = 4;
            this.checkBoxBgr.Text = "Bgr Color Distance (1)";
            this.checkBoxBgr.UseVisualStyleBackColor = true;
            // 
            // checkBoxLabCon
            // 
            this.checkBoxLabCon.AutoSize = true;
            this.checkBoxLabCon.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.checkBoxLabCon.Location = new System.Drawing.Point(171, 62);
            this.checkBoxLabCon.Name = "checkBoxLabCon";
            this.checkBoxLabCon.Size = new System.Drawing.Size(143, 17);
            this.checkBoxLabCon.TabIndex = 5;
            this.checkBoxLabCon.Text = "Lab Color Consistency (1)";
            this.checkBoxLabCon.UseVisualStyleBackColor = true;
            // 
            // ChooseFeaturesForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.GhostWhite;
            this.ClientSize = new System.Drawing.Size(335, 261);
            this.Controls.Add(this.checkBoxLabCon);
            this.Controls.Add(this.checkBoxBgr);
            this.Controls.Add(this.checkBoxHoles);
            this.Controls.Add(this.buttonOk);
            this.Controls.Add(this.checkBoxStand);
            this.Controls.Add(this.panelBoxes);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.SizableToolWindow;
            this.Name = "ChooseFeaturesForm";
            this.Text = "Select Features";
            this.panelBoxes.ResumeLayout(false);
            this.panelBoxes.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panelBoxes;
        private System.Windows.Forms.CheckBox checkBoxPaths;
        private System.Windows.Forms.CheckBox checkBoxLab;
        private System.Windows.Forms.CheckBox checkBoxPii;
        private System.Windows.Forms.CheckBox checkBoxHistograms;
        private System.Windows.Forms.CheckBox checkBoxNM;
        private System.Windows.Forms.CheckBox checkBoxStand;
        private System.Windows.Forms.Button buttonOk;
        private System.Windows.Forms.CheckBox checkBoxHoles;
        private System.Windows.Forms.CheckBox checkBoxBgr;
        private System.Windows.Forms.CheckBox checkBoxLabCon;
        private System.Windows.Forms.CheckBox checkBoxStrokeWidthRatio;
        private System.Windows.Forms.CheckBox checkBoxConvDefects;
    }
}