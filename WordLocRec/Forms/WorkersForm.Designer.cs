﻿namespace WordLocRec.Forms
{
    partial class WorkersForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.ListViewItem listViewItem5 = new System.Windows.Forms.ListViewItem("ER extractor worker");
            System.Windows.Forms.ListViewItem listViewItem6 = new System.Windows.Forms.ListViewItem("NM worker");
            System.Windows.Forms.ListViewItem listViewItem7 = new System.Windows.Forms.ListViewItem("NM worker + CER");
            System.Windows.Forms.ListViewItem listViewItem8 = new System.Windows.Forms.ListViewItem("NM Translated worker");
            this.label1 = new System.Windows.Forms.Label();
            this.textBoxLoad = new System.Windows.Forms.TextBox();
            this.buttonLoad = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.textBoxOutput = new System.Windows.Forms.TextBox();
            this.buttonOutput = new System.Windows.Forms.Button();
            this.listViewWorkers = new System.Windows.Forms.ListView();
            this.label3 = new System.Windows.Forms.Label();
            this.panelProps = new System.Windows.Forms.Panel();
            this.label9 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.textBoxMinProbDiff = new System.Windows.Forms.TextBox();
            this.textBoxMinProb = new System.Windows.Forms.TextBox();
            this.textBoxMaxAr = new System.Windows.Forms.TextBox();
            this.textBoxMinAr = new System.Windows.Forms.TextBox();
            this.textBoxThreshD = new System.Windows.Forms.TextBox();
            this.checkBoxAuto = new System.Windows.Forms.CheckBox();
            this.buttonChannels = new System.Windows.Forms.Button();
            this.checkBoxGammaCorrect = new System.Windows.Forms.CheckBox();
            this.checkBoxSmooth = new System.Windows.Forms.CheckBox();
            this.label10 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.numericUpDownGamma = new System.Windows.Forms.NumericUpDown();
            this.label11 = new System.Windows.Forms.Label();
            this.numericUpDown2 = new System.Windows.Forms.NumericUpDown();
            this.numericUpDown1 = new System.Windows.Forms.NumericUpDown();
            this.checkBoxWiener = new System.Windows.Forms.CheckBox();
            this.checkBoxGS = new System.Windows.Forms.CheckBox();
            this.panelClassifiers = new System.Windows.Forms.Panel();
            this.buttonSetCl3 = new System.Windows.Forms.Button();
            this.buttonSetCl2 = new System.Windows.Forms.Button();
            this.buttonSetCl1 = new System.Windows.Forms.Button();
            this.label18 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.buttonSetF3 = new System.Windows.Forms.Button();
            this.label15 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.buttonSetF2 = new System.Windows.Forms.Button();
            this.buttonSetF1 = new System.Windows.Forms.Button();
            this.label12 = new System.Windows.Forms.Label();
            this.checkBoxDefaultClassifiers = new System.Windows.Forms.CheckBox();
            this.buttonExtract = new System.Windows.Forms.Button();
            this.progressBar1 = new System.Windows.Forms.ProgressBar();
            this.labelStatus = new System.Windows.Forms.Label();
            this.checkBoxCombine = new System.Windows.Forms.CheckBox();
            this.radioButtonNMSstrict = new System.Windows.Forms.RadioButton();
            this.radioButtonNMSorig = new System.Windows.Forms.RadioButton();
            this.panelProps.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownGamma)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).BeginInit();
            this.panelClassifiers.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label1.Location = new System.Drawing.Point(13, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(73, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Select images";
            // 
            // textBoxLoad
            // 
            this.textBoxLoad.Location = new System.Drawing.Point(104, 32);
            this.textBoxLoad.Name = "textBoxLoad";
            this.textBoxLoad.Size = new System.Drawing.Size(326, 20);
            this.textBoxLoad.TabIndex = 1;
            this.textBoxLoad.Text = "...";
            this.textBoxLoad.Click += new System.EventHandler(this.buttonLoad_Click);
            // 
            // buttonLoad
            // 
            this.buttonLoad.BackColor = System.Drawing.Color.LightSteelBlue;
            this.buttonLoad.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonLoad.Location = new System.Drawing.Point(16, 29);
            this.buttonLoad.Name = "buttonLoad";
            this.buttonLoad.Size = new System.Drawing.Size(82, 24);
            this.buttonLoad.TabIndex = 2;
            this.buttonLoad.Text = "Load";
            this.buttonLoad.UseVisualStyleBackColor = false;
            this.buttonLoad.Click += new System.EventHandler(this.buttonLoad_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label2.Location = new System.Drawing.Point(13, 56);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(63, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Output path";
            // 
            // textBoxOutput
            // 
            this.textBoxOutput.Location = new System.Drawing.Point(104, 74);
            this.textBoxOutput.Name = "textBoxOutput";
            this.textBoxOutput.Size = new System.Drawing.Size(326, 20);
            this.textBoxOutput.TabIndex = 4;
            this.textBoxOutput.Text = "...";
            this.textBoxOutput.Click += new System.EventHandler(this.buttonOutput_Click);
            // 
            // buttonOutput
            // 
            this.buttonOutput.BackColor = System.Drawing.Color.LightSteelBlue;
            this.buttonOutput.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonOutput.Location = new System.Drawing.Point(16, 72);
            this.buttonOutput.Name = "buttonOutput";
            this.buttonOutput.Size = new System.Drawing.Size(82, 23);
            this.buttonOutput.TabIndex = 5;
            this.buttonOutput.Text = "Set output";
            this.buttonOutput.UseVisualStyleBackColor = false;
            this.buttonOutput.Click += new System.EventHandler(this.buttonOutput_Click);
            // 
            // listViewWorkers
            // 
            this.listViewWorkers.Items.AddRange(new System.Windows.Forms.ListViewItem[] {
            listViewItem5,
            listViewItem6,
            listViewItem7,
            listViewItem8});
            this.listViewWorkers.Location = new System.Drawing.Point(16, 143);
            this.listViewWorkers.MultiSelect = false;
            this.listViewWorkers.Name = "listViewWorkers";
            this.listViewWorkers.Size = new System.Drawing.Size(156, 222);
            this.listViewWorkers.TabIndex = 6;
            this.listViewWorkers.UseCompatibleStateImageBehavior = false;
            this.listViewWorkers.View = System.Windows.Forms.View.List;
            this.listViewWorkers.ItemActivate += new System.EventHandler(this.listViewWorkers_ItemActivate);
            // 
            // label3
            // 
            this.label3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(13, 106);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(159, 34);
            this.label3.TabIndex = 7;
            this.label3.Text = "Choose an extraction method";
            this.label3.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // panelProps
            // 
            this.panelProps.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelProps.Controls.Add(this.label9);
            this.panelProps.Controls.Add(this.label5);
            this.panelProps.Controls.Add(this.label4);
            this.panelProps.Controls.Add(this.label6);
            this.panelProps.Controls.Add(this.label7);
            this.panelProps.Controls.Add(this.label8);
            this.panelProps.Controls.Add(this.textBoxMinProbDiff);
            this.panelProps.Controls.Add(this.textBoxMinProb);
            this.panelProps.Controls.Add(this.textBoxMaxAr);
            this.panelProps.Controls.Add(this.textBoxMinAr);
            this.panelProps.Controls.Add(this.textBoxThreshD);
            this.panelProps.Enabled = false;
            this.panelProps.Location = new System.Drawing.Point(188, 143);
            this.panelProps.Name = "panelProps";
            this.panelProps.Size = new System.Drawing.Size(163, 222);
            this.panelProps.TabIndex = 8;
            // 
            // label9
            // 
            this.label9.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label9.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(1, 2);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(157, 16);
            this.label9.TabIndex = 44;
            this.label9.Text = "ER Parameters";
            this.label9.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // label5
            // 
            this.label5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label5.AutoSize = true;
            this.label5.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label5.Location = new System.Drawing.Point(23, 179);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(133, 13);
            this.label5.TabIndex = 43;
            this.label5.Text = "Min Prob. Difference (float)";
            // 
            // label4
            // 
            this.label4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label4.AutoSize = true;
            this.label4.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label4.Location = new System.Drawing.Point(24, 61);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(78, 13);
            this.label4.TabIndex = 40;
            this.label4.Text = "Min Area (float)";
            // 
            // label6
            // 
            this.label6.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label6.AutoSize = true;
            this.label6.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label6.Location = new System.Drawing.Point(23, 140);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(104, 13);
            this.label6.TabIndex = 42;
            this.label6.Text = "Min Probability (float)";
            // 
            // label7
            // 
            this.label7.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label7.AutoSize = true;
            this.label7.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(24, 22);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(102, 13);
            this.label7.TabIndex = 39;
            this.label7.Text = "Threshold Delta (int)";
            // 
            // label8
            // 
            this.label8.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label8.AutoSize = true;
            this.label8.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label8.Location = new System.Drawing.Point(23, 101);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(81, 13);
            this.label8.TabIndex = 41;
            this.label8.Text = "Max Area (float)";
            // 
            // textBoxMinProbDiff
            // 
            this.textBoxMinProbDiff.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxMinProbDiff.Location = new System.Drawing.Point(23, 195);
            this.textBoxMinProbDiff.Name = "textBoxMinProbDiff";
            this.textBoxMinProbDiff.Size = new System.Drawing.Size(109, 20);
            this.textBoxMinProbDiff.TabIndex = 38;
            this.textBoxMinProbDiff.Text = "0,1";
            // 
            // textBoxMinProb
            // 
            this.textBoxMinProb.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxMinProb.Location = new System.Drawing.Point(23, 156);
            this.textBoxMinProb.Name = "textBoxMinProb";
            this.textBoxMinProb.Size = new System.Drawing.Size(109, 20);
            this.textBoxMinProb.TabIndex = 37;
            this.textBoxMinProb.Text = "0,2";
            // 
            // textBoxMaxAr
            // 
            this.textBoxMaxAr.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxMaxAr.Location = new System.Drawing.Point(23, 117);
            this.textBoxMaxAr.Name = "textBoxMaxAr";
            this.textBoxMaxAr.Size = new System.Drawing.Size(109, 20);
            this.textBoxMaxAr.TabIndex = 36;
            this.textBoxMaxAr.Text = "0,1";
            // 
            // textBoxMinAr
            // 
            this.textBoxMinAr.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxMinAr.Location = new System.Drawing.Point(23, 77);
            this.textBoxMinAr.Name = "textBoxMinAr";
            this.textBoxMinAr.Size = new System.Drawing.Size(109, 20);
            this.textBoxMinAr.TabIndex = 35;
            this.textBoxMinAr.Text = "0,00005";
            // 
            // textBoxThreshD
            // 
            this.textBoxThreshD.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxThreshD.Location = new System.Drawing.Point(23, 38);
            this.textBoxThreshD.Name = "textBoxThreshD";
            this.textBoxThreshD.Size = new System.Drawing.Size(109, 20);
            this.textBoxThreshD.TabIndex = 34;
            this.textBoxThreshD.Text = "2";
            // 
            // checkBoxAuto
            // 
            this.checkBoxAuto.AutoSize = true;
            this.checkBoxAuto.Checked = true;
            this.checkBoxAuto.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxAuto.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.checkBoxAuto.Location = new System.Drawing.Point(191, 107);
            this.checkBoxAuto.Name = "checkBoxAuto";
            this.checkBoxAuto.Size = new System.Drawing.Size(157, 17);
            this.checkBoxAuto.TabIndex = 9;
            this.checkBoxAuto.Text = "Default Extraction Properties";
            this.checkBoxAuto.UseVisualStyleBackColor = true;
            this.checkBoxAuto.CheckedChanged += new System.EventHandler(this.checkBoxAuto_CheckedChanged);
            // 
            // buttonChannels
            // 
            this.buttonChannels.BackColor = System.Drawing.Color.LightSteelBlue;
            this.buttonChannels.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonChannels.Location = new System.Drawing.Point(537, 142);
            this.buttonChannels.Name = "buttonChannels";
            this.buttonChannels.Size = new System.Drawing.Size(86, 23);
            this.buttonChannels.TabIndex = 10;
            this.buttonChannels.Text = "Set Channels";
            this.buttonChannels.UseVisualStyleBackColor = false;
            this.buttonChannels.Click += new System.EventHandler(this.buttonChannels_Click);
            // 
            // checkBoxGammaCorrect
            // 
            this.checkBoxGammaCorrect.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.checkBoxGammaCorrect.AutoSize = true;
            this.checkBoxGammaCorrect.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.checkBoxGammaCorrect.Location = new System.Drawing.Point(6, 175);
            this.checkBoxGammaCorrect.Name = "checkBoxGammaCorrect";
            this.checkBoxGammaCorrect.Size = new System.Drawing.Size(152, 17);
            this.checkBoxGammaCorrect.TabIndex = 60;
            this.checkBoxGammaCorrect.Text = "Gamma Correction Enabled";
            this.checkBoxGammaCorrect.UseVisualStyleBackColor = true;
            this.checkBoxGammaCorrect.CheckedChanged += new System.EventHandler(this.checkBoxGammaCorrect_CheckedChanged);
            // 
            // checkBoxSmooth
            // 
            this.checkBoxSmooth.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.checkBoxSmooth.AutoSize = true;
            this.checkBoxSmooth.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.checkBoxSmooth.Location = new System.Drawing.Point(6, 37);
            this.checkBoxSmooth.Name = "checkBoxSmooth";
            this.checkBoxSmooth.Size = new System.Drawing.Size(118, 17);
            this.checkBoxSmooth.TabIndex = 59;
            this.checkBoxSmooth.Text = "Smoothing  Enabled";
            this.checkBoxSmooth.UseVisualStyleBackColor = true;
            this.checkBoxSmooth.CheckedChanged += new System.EventHandler(this.checkBoxSmooth_CheckedChanged);
            // 
            // label10
            // 
            this.label10.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(3, 3);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(154, 23);
            this.label10.TabIndex = 61;
            this.label10.Text = "Preprocess images";
            this.label10.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.numericUpDownGamma);
            this.panel1.Controls.Add(this.label11);
            this.panel1.Controls.Add(this.numericUpDown2);
            this.panel1.Controls.Add(this.numericUpDown1);
            this.panel1.Controls.Add(this.checkBoxWiener);
            this.panel1.Controls.Add(this.checkBoxGS);
            this.panel1.Controls.Add(this.label10);
            this.panel1.Controls.Add(this.checkBoxGammaCorrect);
            this.panel1.Controls.Add(this.checkBoxSmooth);
            this.panel1.Location = new System.Drawing.Point(369, 143);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(162, 222);
            this.panel1.TabIndex = 62;
            // 
            // numericUpDownGamma
            // 
            this.numericUpDownGamma.DecimalPlaces = 1;
            this.numericUpDownGamma.Enabled = false;
            this.numericUpDownGamma.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.numericUpDownGamma.Location = new System.Drawing.Point(12, 196);
            this.numericUpDownGamma.Maximum = new decimal(new int[] {
            30,
            0,
            0,
            0});
            this.numericUpDownGamma.Minimum = new decimal(new int[] {
            30,
            0,
            0,
            -2147483648});
            this.numericUpDownGamma.Name = "numericUpDownGamma";
            this.numericUpDownGamma.Size = new System.Drawing.Size(90, 20);
            this.numericUpDownGamma.TabIndex = 67;
            this.numericUpDownGamma.Value = new decimal(new int[] {
            22,
            0,
            0,
            65536});
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label11.Location = new System.Drawing.Point(3, 97);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(83, 13);
            this.label11.TabIndex = 66;
            this.label11.Text = "Kernel size (x,y):";
            // 
            // numericUpDown2
            // 
            this.numericUpDown2.Enabled = false;
            this.numericUpDown2.Location = new System.Drawing.Point(52, 118);
            this.numericUpDown2.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDown2.Name = "numericUpDown2";
            this.numericUpDown2.Size = new System.Drawing.Size(30, 20);
            this.numericUpDown2.TabIndex = 65;
            this.numericUpDown2.Value = new decimal(new int[] {
            5,
            0,
            0,
            0});
            // 
            // numericUpDown1
            // 
            this.numericUpDown1.Enabled = false;
            this.numericUpDown1.Location = new System.Drawing.Point(12, 118);
            this.numericUpDown1.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDown1.Name = "numericUpDown1";
            this.numericUpDown1.Size = new System.Drawing.Size(30, 20);
            this.numericUpDown1.TabIndex = 64;
            this.numericUpDown1.Value = new decimal(new int[] {
            5,
            0,
            0,
            0});
            // 
            // checkBoxWiener
            // 
            this.checkBoxWiener.AutoSize = true;
            this.checkBoxWiener.Enabled = false;
            this.checkBoxWiener.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.checkBoxWiener.Location = new System.Drawing.Point(12, 77);
            this.checkBoxWiener.Name = "checkBoxWiener";
            this.checkBoxWiener.Size = new System.Drawing.Size(57, 17);
            this.checkBoxWiener.TabIndex = 63;
            this.checkBoxWiener.Text = "Wiener";
            this.checkBoxWiener.UseVisualStyleBackColor = true;
            this.checkBoxWiener.CheckedChanged += new System.EventHandler(this.checkBoxWiener_CheckedChanged);
            // 
            // checkBoxGS
            // 
            this.checkBoxGS.AutoSize = true;
            this.checkBoxGS.Checked = true;
            this.checkBoxGS.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxGS.Enabled = false;
            this.checkBoxGS.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.checkBoxGS.Location = new System.Drawing.Point(12, 60);
            this.checkBoxGS.Name = "checkBoxGS";
            this.checkBoxGS.Size = new System.Drawing.Size(67, 17);
            this.checkBoxGS.TabIndex = 62;
            this.checkBoxGS.Text = "Gaussian";
            this.checkBoxGS.UseVisualStyleBackColor = true;
            this.checkBoxGS.CheckedChanged += new System.EventHandler(this.checkBoxGS_CheckedChanged);
            // 
            // panelClassifiers
            // 
            this.panelClassifiers.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelClassifiers.Controls.Add(this.buttonSetCl3);
            this.panelClassifiers.Controls.Add(this.buttonSetCl2);
            this.panelClassifiers.Controls.Add(this.buttonSetCl1);
            this.panelClassifiers.Controls.Add(this.label18);
            this.panelClassifiers.Controls.Add(this.label17);
            this.panelClassifiers.Controls.Add(this.label16);
            this.panelClassifiers.Controls.Add(this.buttonSetF3);
            this.panelClassifiers.Controls.Add(this.label15);
            this.panelClassifiers.Controls.Add(this.label14);
            this.panelClassifiers.Controls.Add(this.label13);
            this.panelClassifiers.Controls.Add(this.buttonSetF2);
            this.panelClassifiers.Controls.Add(this.buttonSetF1);
            this.panelClassifiers.Controls.Add(this.label12);
            this.panelClassifiers.Enabled = false;
            this.panelClassifiers.Location = new System.Drawing.Point(629, 143);
            this.panelClassifiers.Name = "panelClassifiers";
            this.panelClassifiers.Size = new System.Drawing.Size(186, 223);
            this.panelClassifiers.TabIndex = 63;
            // 
            // buttonSetCl3
            // 
            this.buttonSetCl3.BackColor = System.Drawing.Color.LightSteelBlue;
            this.buttonSetCl3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonSetCl3.Location = new System.Drawing.Point(89, 192);
            this.buttonSetCl3.Name = "buttonSetCl3";
            this.buttonSetCl3.Size = new System.Drawing.Size(87, 23);
            this.buttonSetCl3.TabIndex = 77;
            this.buttonSetCl3.Text = "Set ...";
            this.buttonSetCl3.UseVisualStyleBackColor = false;
            this.buttonSetCl3.Click += new System.EventHandler(this.buttonSetCl3_Click);
            // 
            // buttonSetCl2
            // 
            this.buttonSetCl2.BackColor = System.Drawing.Color.LightSteelBlue;
            this.buttonSetCl2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonSetCl2.Location = new System.Drawing.Point(89, 164);
            this.buttonSetCl2.Name = "buttonSetCl2";
            this.buttonSetCl2.Size = new System.Drawing.Size(87, 23);
            this.buttonSetCl2.TabIndex = 76;
            this.buttonSetCl2.Text = "Set ...";
            this.buttonSetCl2.UseVisualStyleBackColor = false;
            this.buttonSetCl2.Click += new System.EventHandler(this.buttonSetCl2_Click);
            // 
            // buttonSetCl1
            // 
            this.buttonSetCl1.BackColor = System.Drawing.Color.LightSteelBlue;
            this.buttonSetCl1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonSetCl1.Location = new System.Drawing.Point(89, 137);
            this.buttonSetCl1.Name = "buttonSetCl1";
            this.buttonSetCl1.Size = new System.Drawing.Size(87, 23);
            this.buttonSetCl1.TabIndex = 75;
            this.buttonSetCl1.Text = "Set ...";
            this.buttonSetCl1.UseVisualStyleBackColor = false;
            this.buttonSetCl1.Click += new System.EventHandler(this.buttonSetCl1_Click);
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label18.Location = new System.Drawing.Point(3, 197);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(83, 13);
            this.label18.TabIndex = 74;
            this.label18.Text = "Load classifier 3";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label17.Location = new System.Drawing.Point(3, 169);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(83, 13);
            this.label17.TabIndex = 73;
            this.label17.Text = "Load classifier 2";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label16.Location = new System.Drawing.Point(3, 142);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(83, 13);
            this.label16.TabIndex = 72;
            this.label16.Text = "Load classifier 1";
            // 
            // buttonSetF3
            // 
            this.buttonSetF3.BackColor = System.Drawing.Color.LightSteelBlue;
            this.buttonSetF3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonSetF3.Location = new System.Drawing.Point(89, 98);
            this.buttonSetF3.Name = "buttonSetF3";
            this.buttonSetF3.Size = new System.Drawing.Size(87, 23);
            this.buttonSetF3.TabIndex = 68;
            this.buttonSetF3.Text = "Set ...";
            this.buttonSetF3.UseVisualStyleBackColor = false;
            this.buttonSetF3.Click += new System.EventHandler(this.buttonSetF3_Click);
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label15.Location = new System.Drawing.Point(3, 103);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(74, 13);
            this.label15.TabIndex = 67;
            this.label15.Text = "Word features";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label14.Location = new System.Drawing.Point(3, 74);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(69, 13);
            this.label14.TabIndex = 66;
            this.label14.Text = "Feature set 2";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label13.Location = new System.Drawing.Point(3, 45);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(69, 13);
            this.label13.TabIndex = 65;
            this.label13.Text = "Feature set 1";
            // 
            // buttonSetF2
            // 
            this.buttonSetF2.BackColor = System.Drawing.Color.LightSteelBlue;
            this.buttonSetF2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonSetF2.Location = new System.Drawing.Point(89, 69);
            this.buttonSetF2.Name = "buttonSetF2";
            this.buttonSetF2.Size = new System.Drawing.Size(87, 23);
            this.buttonSetF2.TabIndex = 64;
            this.buttonSetF2.Text = "Set ...";
            this.buttonSetF2.UseVisualStyleBackColor = false;
            this.buttonSetF2.Click += new System.EventHandler(this.buttonSetF2_Click);
            // 
            // buttonSetF1
            // 
            this.buttonSetF1.BackColor = System.Drawing.Color.LightSteelBlue;
            this.buttonSetF1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonSetF1.Location = new System.Drawing.Point(89, 40);
            this.buttonSetF1.Name = "buttonSetF1";
            this.buttonSetF1.Size = new System.Drawing.Size(87, 23);
            this.buttonSetF1.TabIndex = 63;
            this.buttonSetF1.Text = "Set ...";
            this.buttonSetF1.UseVisualStyleBackColor = false;
            this.buttonSetF1.Click += new System.EventHandler(this.buttonSetF1_Click);
            // 
            // label12
            // 
            this.label12.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(3, 4);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(173, 33);
            this.label12.TabIndex = 62;
            this.label12.Text = "Set features and classifiers";
            this.label12.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // checkBoxDefaultClassifiers
            // 
            this.checkBoxDefaultClassifiers.AutoSize = true;
            this.checkBoxDefaultClassifiers.Checked = true;
            this.checkBoxDefaultClassifiers.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxDefaultClassifiers.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.checkBoxDefaultClassifiers.Location = new System.Drawing.Point(629, 107);
            this.checkBoxDefaultClassifiers.Name = "checkBoxDefaultClassifiers";
            this.checkBoxDefaultClassifiers.Size = new System.Drawing.Size(167, 17);
            this.checkBoxDefaultClassifiers.TabIndex = 64;
            this.checkBoxDefaultClassifiers.Text = "Default classifiers and features";
            this.checkBoxDefaultClassifiers.UseVisualStyleBackColor = true;
            this.checkBoxDefaultClassifiers.CheckedChanged += new System.EventHandler(this.checkBoxDefaultClassifiers_CheckedChanged);
            // 
            // buttonExtract
            // 
            this.buttonExtract.BackColor = System.Drawing.Color.LightSteelBlue;
            this.buttonExtract.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonExtract.Location = new System.Drawing.Point(442, 25);
            this.buttonExtract.Name = "buttonExtract";
            this.buttonExtract.Size = new System.Drawing.Size(89, 27);
            this.buttonExtract.TabIndex = 65;
            this.buttonExtract.Text = "GO!";
            this.buttonExtract.UseVisualStyleBackColor = false;
            this.buttonExtract.Click += new System.EventHandler(this.buttonExtract_Click);
            // 
            // progressBar1
            // 
            this.progressBar1.Location = new System.Drawing.Point(537, 25);
            this.progressBar1.Name = "progressBar1";
            this.progressBar1.Size = new System.Drawing.Size(278, 27);
            this.progressBar1.TabIndex = 66;
            // 
            // labelStatus
            // 
            this.labelStatus.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.labelStatus.Location = new System.Drawing.Point(537, 56);
            this.labelStatus.Name = "labelStatus";
            this.labelStatus.Size = new System.Drawing.Size(278, 23);
            this.labelStatus.TabIndex = 67;
            this.labelStatus.Text = "Press \"GO!\" to begin extraction";
            this.labelStatus.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // checkBoxCombine
            // 
            this.checkBoxCombine.AutoSize = true;
            this.checkBoxCombine.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.checkBoxCombine.Location = new System.Drawing.Point(540, 181);
            this.checkBoxCombine.Name = "checkBoxCombine";
            this.checkBoxCombine.Size = new System.Drawing.Size(64, 17);
            this.checkBoxCombine.TabIndex = 68;
            this.checkBoxCombine.Text = "Combine";
            this.checkBoxCombine.UseVisualStyleBackColor = true;
            // 
            // radioButtonNMSstrict
            // 
            this.radioButtonNMSstrict.AutoSize = true;
            this.radioButtonNMSstrict.Checked = true;
            this.radioButtonNMSstrict.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.radioButtonNMSstrict.Location = new System.Drawing.Point(537, 204);
            this.radioButtonNMSstrict.Name = "radioButtonNMSstrict";
            this.radioButtonNMSstrict.Size = new System.Drawing.Size(75, 17);
            this.radioButtonNMSstrict.TabIndex = 69;
            this.radioButtonNMSstrict.TabStop = true;
            this.radioButtonNMSstrict.Text = "Strict NMS";
            this.radioButtonNMSstrict.UseVisualStyleBackColor = true;
            // 
            // radioButtonNMSorig
            // 
            this.radioButtonNMSorig.AutoSize = true;
            this.radioButtonNMSorig.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.radioButtonNMSorig.Location = new System.Drawing.Point(537, 227);
            this.radioButtonNMSorig.Name = "radioButtonNMSorig";
            this.radioButtonNMSorig.Size = new System.Drawing.Size(86, 17);
            this.radioButtonNMSorig.TabIndex = 70;
            this.radioButtonNMSorig.TabStop = true;
            this.radioButtonNMSorig.Text = "Original NMS";
            this.radioButtonNMSorig.UseVisualStyleBackColor = true;
            // 
            // WorkersForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.GhostWhite;
            this.ClientSize = new System.Drawing.Size(827, 379);
            this.Controls.Add(this.radioButtonNMSorig);
            this.Controls.Add(this.radioButtonNMSstrict);
            this.Controls.Add(this.checkBoxCombine);
            this.Controls.Add(this.labelStatus);
            this.Controls.Add(this.progressBar1);
            this.Controls.Add(this.buttonExtract);
            this.Controls.Add(this.checkBoxDefaultClassifiers);
            this.Controls.Add(this.panelClassifiers);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.buttonChannels);
            this.Controls.Add(this.checkBoxAuto);
            this.Controls.Add(this.panelProps);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.listViewWorkers);
            this.Controls.Add(this.buttonOutput);
            this.Controls.Add(this.textBoxOutput);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.buttonLoad);
            this.Controls.Add(this.textBoxLoad);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.SizableToolWindow;
            this.MinimumSize = new System.Drawing.Size(843, 418);
            this.Name = "WorkersForm";
            this.Text = "Multiple images";
            this.panelProps.ResumeLayout(false);
            this.panelProps.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownGamma)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).EndInit();
            this.panelClassifiers.ResumeLayout(false);
            this.panelClassifiers.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBoxLoad;
        private System.Windows.Forms.Button buttonLoad;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox textBoxOutput;
        private System.Windows.Forms.Button buttonOutput;
        private System.Windows.Forms.ListView listViewWorkers;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Panel panelProps;
        private System.Windows.Forms.CheckBox checkBoxAuto;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox textBoxMinProbDiff;
        private System.Windows.Forms.TextBox textBoxMinProb;
        private System.Windows.Forms.TextBox textBoxMaxAr;
        private System.Windows.Forms.TextBox textBoxMinAr;
        private System.Windows.Forms.TextBox textBoxThreshD;
        private System.Windows.Forms.Button buttonChannels;
        private System.Windows.Forms.CheckBox checkBoxGammaCorrect;
        private System.Windows.Forms.CheckBox checkBoxSmooth;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.CheckBox checkBoxWiener;
        private System.Windows.Forms.CheckBox checkBoxGS;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.NumericUpDown numericUpDown2;
        private System.Windows.Forms.NumericUpDown numericUpDown1;
        private System.Windows.Forms.NumericUpDown numericUpDownGamma;
        private System.Windows.Forms.Panel panelClassifiers;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Button buttonSetF2;
        private System.Windows.Forms.Button buttonSetF1;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Button buttonSetCl3;
        private System.Windows.Forms.Button buttonSetCl2;
        private System.Windows.Forms.Button buttonSetCl1;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Button buttonSetF3;
        private System.Windows.Forms.CheckBox checkBoxDefaultClassifiers;
        private System.Windows.Forms.Button buttonExtract;
        private System.Windows.Forms.ProgressBar progressBar1;
        private System.Windows.Forms.Label labelStatus;
        private System.Windows.Forms.CheckBox checkBoxCombine;
        private System.Windows.Forms.RadioButton radioButtonNMSstrict;
        private System.Windows.Forms.RadioButton radioButtonNMSorig;
    }
}