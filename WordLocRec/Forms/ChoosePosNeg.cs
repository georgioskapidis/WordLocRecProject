﻿using Emgu.CV;
using Emgu.CV.Util;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WordLocRec.Forms
{
    public partial class ChoosePosNeg : Form
    {
        List<Mat> mats;
        List<String> texts;
        private bool textsAvailable = false;
        public List<int> positiveIndices;
        public List<int> negativeIndices;
        public List<int> ambiguousIndices;

        private bool[] indices;
        private int viewIndex = 0;
        private List<int> actionHistory;

        private ChoosePosNeg(VectorOfMat erMats, List<String> texts, bool arbitrary)
        {
            InitializeComponent();

            positiveIndices = new List<int>();
            negativeIndices = new List<int>();
            ambiguousIndices = new List<int>();

            if (erMats.Size == 0) return;
            this.mats = new List<Mat>();
            indices = new bool[erMats.Size];
            actionHistory = new List<int>();
            for (int i = 0; i < erMats.Size; i++)
            {
                mats.Add(erMats[i].Clone());
                indices[i] = true;
            }

            if (texts != null)
            {
                this.texts = new List<string>(texts);
                textsAvailable = true;
            }

            textBoxTitle.Text = "Be careful of your choices!";
            textBoxAvailable.Text = erMats.Size.ToString();

            textBoxPositive.Text = "0";
            textBoxNegative.Text = "0";
            textBoxAmbiguous.Text = "0";
            viewIndex = 0;
            setInfoToView();
        }
        public ChoosePosNeg(VectorOfMat erMats, List<String> texts) : this(erMats, texts, false) { }
        public ChoosePosNeg(VectorOfMat erMats) : this (erMats, null, false) { }
        private void setBitmapToPictureBox(Bitmap bmp)
        {
            this.pictureBoxER.Image = bmp;
        }
        private String createExampleInfo(int viewIndex)
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendLine(indices[viewIndex] ? "Available" : "Defined");
            if (textsAvailable)
            {
                sb.AppendLine(texts[viewIndex]);
            }
            return sb.ToString();
        }
        private void setInfoToView()
        {
            textBoxPositive.Text = positiveIndices.Count.ToString();
            textBoxNegative.Text = negativeIndices.Count.ToString();
            textBoxAmbiguous.Text = ambiguousIndices.Count.ToString();
            if (viewIndex == -1)
            {
                DialogResult res = MessageBox.Show("All have been decided, exit?"," Continue or Exit", MessageBoxButtons.OKCancel);
                if (res == System.Windows.Forms.DialogResult.OK) this.Close();
                else { buttonUndo_Click(null, null); }
            }
            else
            {
                setBitmapToPictureBox(mats[viewIndex].Bitmap);
                textBoxERInfo.Text = createExampleInfo(viewIndex);
                textBoxViewIndex.Text = viewIndex.ToString();
            }           
        }
        private int FindNextApplicable()
        {
            for (int i = 0; i < indices.Length; i++)
            {
                if (indices[i]) return i;
            }
            return -1;
        }

        private void buttonPositive_Click(object sender, EventArgs e)
        {
            if (indices[viewIndex])
            {
                positiveIndices.Add(viewIndex);
                indices[viewIndex] = false;
                actionHistory.Add(viewIndex);
            }
            viewIndex = FindNextApplicable();
            setInfoToView();        
        }
        private void buttonNegative_Click(object sender, EventArgs e)
        {
            if (indices[viewIndex])
            {
                negativeIndices.Add(viewIndex);
                indices[viewIndex] = false;
                actionHistory.Add(viewIndex);
            }
            viewIndex = FindNextApplicable();
            setInfoToView();
        }
        private void buttonAmbiguous_Click(object sender, EventArgs e)
        {
            if (indices[viewIndex])
            {
                ambiguousIndices.Add(viewIndex);
                indices[viewIndex] = false;
                actionHistory.Add(viewIndex);
            }
            viewIndex = FindNextApplicable();
            setInfoToView();

        }

        private void buttonUndo_Click(object sender, EventArgs e)
        {
            if (actionHistory.Count == 0) return;
            if (positiveIndices.Contains(actionHistory[actionHistory.Count - 1]))
                positiveIndices.Remove(actionHistory[actionHistory.Count - 1]);
            else if (negativeIndices.Contains(actionHistory[actionHistory.Count - 1]))
                negativeIndices.Remove(actionHistory[actionHistory.Count - 1]);
            else
                ambiguousIndices.Remove(actionHistory[actionHistory.Count - 1]);


            indices[actionHistory[actionHistory.Count - 1]] = true;
            viewIndex = actionHistory[actionHistory.Count - 1];

            actionHistory.RemoveAt(actionHistory.Count - 1);

            setInfoToView();
        }

        private void buttonPrev_Click(object sender, EventArgs e)
        {
            viewIndex = viewIndex - 1 < 0 ? viewIndex = mats.Count - 1 : viewIndex - 1;
            setInfoToView();
        }

        private void buttonNext_Click(object sender, EventArgs e)
        {
            viewIndex = viewIndex + 1 > mats.Count - 1 ? 0 : viewIndex + 1;
            setInfoToView();
        }
    }
}
