﻿using Emgu.CV;
using Emgu.CV.ML;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WordLocRec.DataStructs;
using WordLocRec.Grouping;
using WordLocRec.SVMStuff;
using WordLocRec.Tools;

namespace WordLocRec.Forms
{
    public partial class ModelCreatorForm : Form
    {
        List<String> positiveNames, negativeNames;

        List<BaseModelCreator> modelContainer;

        Type t;
        Constants.FeatureType f, wordFeature;
        // evaluation
        List<String>[] rectFilesPerImage;
        List<String> gtFiles;
        public void InitializeStuff()
        {
            positiveNames = new List<string>();
            negativeNames = new List<string>();
            listView1.MultiSelect = false;

            modelContainer = new List<BaseModelCreator>();
        }

        public ModelCreatorForm()
        {
            InitializeComponent();
            InitializeStuff();
        }

        private void TrainModel(int i, bool askToRetrain)
        {
            if (modelContainer[i].IsTrained)
            {
                if (askToRetrain) // already trained, ask if user wants to retrain
                {
                    var result = MessageBox.Show("This model is already trained! Retrain?", "Warning", MessageBoxButtons.YesNo);
                    if (result == System.Windows.Forms.DialogResult.No)
                        return;
                }
                else // already trained and user doesn't want to change it
                    return;
            } // either untrained, or the user wants to retrain
            ExecuteGenericMethod(modelContainer[i], "CreateTrainAndTestSetClasses");
            modelContainer[i].TrainClassifier();
            modelContainer[i].Evaluate();
            textBox1.Text = modelContainer[i].ToString();
        }

        private void ExecuteGenericMethod(BaseModelCreator baseModelCreator, String methodName)
        {
            MethodInfo createTrainAndTestSetClasses = baseModelCreator.GetType().GetMethod(methodName);
            MethodInfo generic = createTrainAndTestSetClasses.MakeGenericMethod(t);
            generic.Invoke(baseModelCreator, null);
        }

        private void TrainAll(bool retrain)
        {
            if (retrain)
            {
                var result = MessageBox.Show("If any models are already trained, they will be retrained. Continue?", "Warning", MessageBoxButtons.OKCancel);
                if (result == System.Windows.Forms.DialogResult.OK)
                {
                    for (int i = 0; i < modelContainer.Count; i++)
                    {
                        modelContainer[i].IsTrained = false;
                    }
                }
            }
            for (int i = 0; i < modelContainer.Count; i++)
                TrainModel(i, false);

        }

        #region Buttons

        #region Checkbox logic

        void cbOneClassSVM_CheckedChanged(object sender, EventArgs e)
        {
            CheckBox cb = (CheckBox)sender as CheckBox;
            FormUtils.CheckBoxesMutuallyExclusive(cb, cbTwoClassSVM);
        }
        void cbTwoClassSVM_CheckedChanged(object sender, EventArgs e)
        {
            CheckBox cb = (CheckBox)sender as CheckBox;
            FormUtils.CheckBoxesMutuallyExclusive(cb, cbOneClassSVM);
        }

        void cbActiveVarCountRoot_CheckedChanged(object sender, EventArgs e)
        {
            CheckBox cb = (CheckBox)sender as CheckBox;
            FormUtils.CheckBoxesMutuallyExclusive(cb, cbActiveVarCountAll);
        }
        void cbActiveVarCountAll_CheckedChanged(object sender, EventArgs e)
        {
            CheckBox cb = (CheckBox)sender as CheckBox;
            FormUtils.CheckBoxesMutuallyExclusive(cb, cbActiveVarCountRoot);
        }

        private void checkBoxWS_CheckedChanged(object sender, EventArgs e)
        {
            CheckBox cb = (CheckBox)sender as CheckBox;
            FormUtils.CheckBoxesMutuallyExclusive(cb, checkBoxSWS, checkBoxLWS, checkBoxFFO);
        }

        private void checkBoxSWS_CheckedChanged(object sender, EventArgs e)
        {
            CheckBox cb = (CheckBox)sender as CheckBox;
            FormUtils.CheckBoxesMutuallyExclusive(cb, checkBoxWS, checkBoxLWS, checkBoxFFO);
        }

        private void checkBoxLWS_CheckedChanged(object sender, EventArgs e)
        {
            CheckBox cb = (CheckBox)sender as CheckBox;
            FormUtils.CheckBoxesMutuallyExclusive(cb, checkBoxWS, checkBoxSWS, checkBoxFFO);
        }

        private void checkBoxFFO_CheckedChanged(object sender, EventArgs e)
        {
            FormUtils.CheckBoxesMutuallyExclusive(checkBoxFFO, checkBoxWS, checkBoxSWS, checkBoxLWS);
        }
        #endregion

        private void buttonLoadP_Click(object sender, EventArgs e)
        {
            if (checkBoxWS.Checked) t = typeof(WordStat);
            else if (checkBoxSWS.Checked) t = typeof(SimpleWordStat);
            else if (checkBoxLWS.Checked) t = typeof(LineWordStat);
            else if (checkBoxFFO.Checked)
            {
                t = typeof(FullFeatureObject);       
                if (wordFeature == 0)
                    using (ChooseFeaturesForm cff = new ChooseFeaturesForm(0))
                        if (cff.ShowDialog() == DialogResult.OK)
                        {
                            wordFeature = cff.featureType;
                            buttonTestSet.Enabled = false;
                        }
                           
                        else return;
            }
            else
            {
                MessageBox.Show("Define the feature type first!");
                return;
            }
            checkBoxWS.Enabled = false; checkBoxSWS.Enabled = false; checkBoxLWS.Enabled = false; checkBoxFFO.Enabled = false;

            String[] names = FormUtils.ParametrizedOpenFileDialog(Constants.XML_FILE_FILTER, true, "Select the dataset files");
            if (names == null) return;
            positiveNames.AddRange(names.ToList());  
        }

        private void buttonLoadN_Click(object sender, EventArgs e)
        {
            if (checkBoxWS.Checked) t = typeof(WordStat);
            else if (checkBoxSWS.Checked) t = typeof(SimpleWordStat);
            else if (checkBoxLWS.Checked) t = typeof(LineWordStat);
            else if (checkBoxFFO.Checked)
            {
                t = typeof(FullFeatureObject);
                if (wordFeature == 0)
                    using (ChooseFeaturesForm cff = new ChooseFeaturesForm(0))
                        if (cff.ShowDialog() == DialogResult.OK)
                        {
                            wordFeature = cff.featureType;
                            buttonTestSet.Enabled = false;
                        }
                        else return;
            }
            else
            {
                MessageBox.Show("Define the feature type first!");
                return;
            }
            checkBoxWS.Enabled = false; checkBoxSWS.Enabled = false; checkBoxLWS.Enabled = false; checkBoxFFO.Enabled = false;

            String[] names = FormUtils.ParametrizedOpenFileDialog(Constants.XML_FILE_FILTER, true, "Select the dataset files");
            if (names == null) return;
            negativeNames.AddRange(names.ToList());
        }

        /// <summary>
        /// Load the folder that contains the folders with the extracted datasets (WordStat etc.).
        /// Then put the files of each folder in a list and all the lists in an array.
        /// Then for the image file that corresponds to a list load the appropriate gt file.
        /// P.S. Each file in a list contains details about a detected rectangle that was extracted.
        /// Example: Loading the word stats of the image files 1 and 5, results in loading the gt files of img 1 and 5. IF the whole dataset of word stats is loaded then the whole dataset of gts will be loaded as well.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonTestSet_Click(object sender, EventArgs e)
        {
            String selectedFolder = FormUtils.ParametrizedFolderBrowserDialog("Select the folder of the according test set folders");
            if (selectedFolder == null) return;
            String[] imageFolders = Directory.GetDirectories(selectedFolder);
            rectFilesPerImage = new List<string>[imageFolders.Length];

            String[] gts = Directory.GetFiles(Constants.ICDAR2013GTPath);
            gtFiles = new List<string>();
            for (int i = 0; i < imageFolders.Length; i++)
            {
                rectFilesPerImage[i] = Directory.GetFiles(imageFolders[i]).ToList();
                var temp = from gt in gts
                           where Path.GetFileNameWithoutExtension(gt).EndsWith(Path.GetFileName(imageFolders[i]))
                           select gt;
                gtFiles.AddRange(temp.ToList());
            }
            if (rectFilesPerImage.Length > 0) 
                buttonEval.Enabled = true;

            //// debug gia sugkrish me ta unpruned //
            //String[] unpruned = Directory.GetFiles("C:\\resultsNM\\UnprunedLines");
            //List<RectangleF>[] rectsPerImageUnpruned = new List<RectangleF>[unpruned.Length];
            //for (int i = 0; i < unpruned.Length; i++)
            //{
            //    rectsPerImageUnpruned[i] = IOUtils.LoadFileToRectListF(unpruned[i], ',');
            //}
            //List<int> wrongs = new List<int>();
            //for (int i = 0; i < rectsPerImageUnpruned.Length; i++)
            //{
            //    if (rectsPerImageUnpruned[i].Count != rectFilesPerImage[i].Count)
            //    {
            //        wrongs.Add(i);
            //    }
            //} katelhksa sto oti exei diafora to release akoma kai me ton eauto tou (!)
        }

        private void buttonAddModel_Click(object sender, EventArgs e) // TODO: na enswmatwsw ta montela tou full feature object sta eidi classifiers!
        {
            if (positiveNames.Count == 0 || negativeNames.Count == 0)
            {
                MessageBox.Show("Please load the datasets properly");
                return;
            }
            int percSplit;
            if (!int.TryParse(tbSplit.Text, out percSplit) && (percSplit < 0 && percSplit > 100))
            {
                MessageBox.Show("Use a value between 0 - 100 (integer) for the split percentage");
                return;
            }
            double val;

            if (tabControlClassifiers.SelectedIndex == 0)
            {
                SVM svm = new SVM();
                if (comboBoxKernels.SelectedItem == null) return;
                string kernelType = comboBoxKernels.SelectedItem.ToString();
                switch (kernelType)
                {
                    case "Chi2": svm.SetKernel(SVM.SvmKernelType.Chi2); break;
                    case "Inter": svm.SetKernel(SVM.SvmKernelType.Inter); break;
                    case "Linear": svm.SetKernel(SVM.SvmKernelType.Linear); break;
                    case "Poly": svm.SetKernel(SVM.SvmKernelType.Poly); break;
                    case "Rbf": svm.SetKernel(SVM.SvmKernelType.Rbf); break;
                    case "Sigmoid": svm.SetKernel(SVM.SvmKernelType.Sigmoid); break;
                    default: return;
                }
                svm.Type = cbOneClassSVM.Checked ? SVM.SvmType.OneClass : SVM.SvmType.CSvc;

                svm.C = double.TryParse(tbC.Text, out val) ? val : 0;
                svm.Gamma = double.TryParse(tbGamma.Text, out val) ? val : 0;
                svm.Nu = double.TryParse(tbNu.Text, out val) ? val > 0 && val < 1 ? val : 0.1 : 0.1;
                svm.Coef0 = double.TryParse(tbCoef0.Text, out val) ? val : 0;
                svm.Degree = double.TryParse(tbDegree.Text, out val) ? val : 10;

                int termIter;
                if (int.TryParse(tbIterations.Text, out termIter))
                    svm.TermCriteria = new Emgu.CV.Structure.MCvTermCriteria(termIter, float.Epsilon);
                else return;

                String modelName = "SVM: " + svm.Type + ", " + svm.KernelType + ", C: " + svm.C + ", γ: " + svm.Gamma + ", iters: " + termIter + (cbStandardized.Checked?" Standardized":"");

                f = cbStandardized.Checked ? wordFeature | Constants.FeatureType.StandarizedPerFeature : wordFeature;

                BaseModelCreator model = new SVMModelCreator(f, positiveNames.ToArray(), negativeNames.ToArray(), percSplit, cbTrainAuto.Checked);
                model.model = svm;
                model.ModelName = modelName;
                modelContainer.Add(model);

                ListViewItem lvi = new ListViewItem("SVM");
                listView1.Items.Add(lvi);
            }
            else if (tabControlClassifiers.SelectedIndex == 1)
            {
                DTrees dTrees = new DTrees();
                int val1;
                dTrees.CVFolds = int.TryParse(tbCVFolds.Text, out val1) ? val1 : 0;
                dTrees.MaxDepth = int.TryParse(tbMaxDepth.Text, out val1) ? val1 : 0;
                dTrees.MinSampleCount = int.TryParse(tbMinSampleCount.Text, out val1) ? val1 : 0;
                dTrees.TruncatePrunedTree = cbTruncatePrunedTree.Checked;
                dTrees.Use1SERule = cbUseSE1Rule.Checked;
                dTrees.UseSurrogates = cbUseSurrogates.Checked;
                
                String modelName = "DTree folds: " + dTrees.CVFolds.ToString() + ", depth: " + dTrees.MaxDepth.ToString() + ", minSample# " + dTrees.MinSampleCount.ToString();

                f = cbStandardized.Checked ? wordFeature | Constants.FeatureType.StandarizedPerFeature : wordFeature;

                BaseModelCreator model = new DTreeModelCreator(f, positiveNames.ToArray(), negativeNames.ToArray(), percSplit);

                model.model = dTrees;
                model.ModelName = modelName;
                modelContainer.Add(model);

                ListViewItem lvi = new ListViewItem("Decision Tree");
                listView1.Items.Add(lvi);
            }
            else if (tabControlClassifiers.SelectedIndex == 2)
            {
                RTrees rTrees = new RTrees();
                int featureLength = t.Equals(typeof(WordStat)) ? 14 : t.Equals(typeof(SimpleWordStat)) ? 10 : t.Equals(typeof(LineWordStat)) ? 8 : SVMFullFeatureExample.DecideFeatureLengthStatic(wordFeature);
                rTrees.ActiveVarCount = cbActiveVarCountAll.Checked ? featureLength : (int)Math.Truncate(Math.Sqrt(featureLength)); 
                rTrees.CalculateVarImportance = cbVarImportance.Checked;
                int val1;
                rTrees.CVFolds = int.TryParse(tbFolds.Text, out val1) ? val1 : 0;
                rTrees.MaxDepth = int.TryParse(tbMaxDepthR.Text, out val1) ? val1 : 0;
                if (int.TryParse(tbIters.Text, out val1))
                    rTrees.TermCriteria = new Emgu.CV.Structure.MCvTermCriteria(val1, float.Epsilon);
                else return;
                rTrees.TruncatePrunedTree = cbTruncatePrunedTreeR.Checked;
                rTrees.Use1SERule = cbUseSE1RuleR.Checked;
                rTrees.UseSurrogates = cbUseSurrogatesR.Checked;

                String modelName = "RTree: folds: " + rTrees.CVFolds.ToString() + ", depth: " + rTrees.MaxDepth.ToString() + ", iterations: " + rTrees.TermCriteria.MaxIter.ToString();

                f = cbStandardized.Checked ? wordFeature | Constants.FeatureType.StandarizedPerFeature : wordFeature;

                BaseModelCreator model = new RTreeModelCreator(f, positiveNames.ToArray(), negativeNames.ToArray(), percSplit);

                model.model = rTrees;
                model.ModelName = modelName;
                modelContainer.Add(model);

                ListViewItem lvi = new ListViewItem("Random Forest");
                listView1.Items.Add(lvi);
            }
            else
            {
                MessageBox.Show("Define a model first!");
            }
        }

        private void buttonTrain_Click(object sender, EventArgs e)
        {
            if (listView1.Items.Count == 0)
            {
                MessageBox.Show("Nothing to train");
                return;
            }
            else if (listView1.SelectedItems.Count == 0 || listView1.SelectedItems.Count > 1)
            {
                MessageBox.Show("Pick one model from the list to train");
                return;
            }
            TrainModel(listView1.SelectedIndices[0], true);
            listView1.SelectedIndices.Clear();
        }

        private void buttonTrainRemaining_Click(object sender, EventArgs e)
        {
            if (listView1.Items.Count == 0)
            {
                MessageBox.Show("Nothing to train");
                return;
            }
            TrainAll(false);
        }

        private void buttonTrainAll_Click(object sender, EventArgs e)
        {
            if (listView1.Items.Count == 0)
            {
                MessageBox.Show("Nothing to train");
                return;
            }
            TrainAll(true);
        }

        private void listView1_ItemActivate(object sender, EventArgs e)
        {
            if (listView1.SelectedIndices.Count > 0)
            {
                int i = listView1.SelectedIndices[0];
                textBox1.Text = modelContainer[i].ToString();
                listView1.SelectedIndices.Clear();
            }
            
        }

        private void buttonSave_Click(object sender, EventArgs e)
        {
            if (listView1.Items.Count == 0)
            {
                MessageBox.Show("Nothing to save");
                return;
            }
            else if (listView1.SelectedItems.Count == 0 || listView1.SelectedItems.Count > 1)
            {
                MessageBox.Show("Pick one model from the list to save");
                return;
            }
            int i = listView1.SelectedIndices[0];

            SaveFileDialog sfd = new SaveFileDialog();
            sfd.Filter = Constants.XML_FILE_FILTER;
            sfd.InitialDirectory = Constants.PathDesktop;
            sfd.Title = "Save the file";
            sfd.OverwritePrompt = true;
            sfd.FileName = listView1.Items[i].ToString();
            if (sfd.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                modelContainer[i].SaveClassifier(sfd.FileName);
            }

            listView1.SelectedIndices.Clear();
        }

        private void listView1_KeyDown(object sender, KeyEventArgs e)
        {
            if (t == null)
            {
                MessageBox.Show("Set the Feature first.");
                return;
            }
            if (e.KeyCode.Equals(Keys.Delete))
            {
                if (listView1.SelectedIndices.Count > 0)
                {
                    modelContainer.RemoveAt(listView1.SelectedIndices[0]);
                    listView1.Items.RemoveAt(listView1.SelectedIndices[0]);
                    listView1.SelectedIndices.Clear();
                }
            }// cheat codes!!
            else if (e.KeyCode.Equals(Keys.R))
            {
                String r = FormUtils.ParametrizedOpenFileDialog(Constants.XML_FILE_FILTER, false, "Load the rTree")[0];
                if (r == null) return;
                f = cbStandardized.Checked ? wordFeature | Constants.FeatureType.StandarizedPerFeature : wordFeature;
                RTreeModelCreator rtreemodel = new RTreeModelCreator(f, positiveNames.ToArray(), negativeNames.ToArray());
                rtreemodel.LoadClassifer(r);
                rtreemodel.IsTrained = true;
                ExecuteGenericMethod(rtreemodel, "CreateTrainAndTestSetClasses");
                rtreemodel.Evaluate();
                modelContainer.Add(rtreemodel);
                ListViewItem lvi = new ListViewItem("RTree from file");
                listView1.Items.Add(lvi);
            }
            else if (e.KeyCode.Equals(Keys.D))
            {
                String d = FormUtils.ParametrizedOpenFileDialog(Constants.XML_FILE_FILTER, false, "Load the dTree")[0];
                if (d == null) return;
                f = cbStandardized.Checked ? wordFeature | Constants.FeatureType.StandarizedPerFeature : wordFeature;
                DTreeModelCreator dtreemodel = new DTreeModelCreator(f, positiveNames.ToArray(), negativeNames.ToArray());
                dtreemodel.LoadClassifer(d);
                dtreemodel.IsTrained = true;
                ExecuteGenericMethod(dtreemodel, "CreateTrainAndTestSetClasses");
                dtreemodel.Evaluate();
                modelContainer.Add(dtreemodel);
                ListViewItem lvi = new ListViewItem("Dtree from file");
                listView1.Items.Add(lvi);
            }
            else if (e.KeyCode.Equals(Keys.S))
            {
                String s = FormUtils.ParametrizedOpenFileDialog(Constants.XML_FILE_FILTER, false, "Load the SVM")[0];
                if (s == null) return;
                f = cbStandardized.Checked ? wordFeature | Constants.FeatureType.StandarizedPerFeature : wordFeature;
                SVMModelCreator svmmodel = new SVMModelCreator(f, positiveNames.ToArray(), negativeNames.ToArray());
                svmmodel.LoadClassifer(s);
                svmmodel.IsTrained = true;
                ExecuteGenericMethod(svmmodel, "CreateTrainAndTestSetClasses");
                svmmodel.Evaluate();
                modelContainer.Add(svmmodel);
                ListViewItem lvi = new ListViewItem("svm from file");
                listView1.Items.Add(lvi);
            }
        }

        private void buttonEval_Click(object sender, EventArgs e)
        {
            if (modelContainer.Count > 0)
            {
                for (int i = 0; i < modelContainer.Count; i++) // eval for each model
                {
                    if (!modelContainer[i].IsTrained) continue;
                    // start my eval here
                    float totalRecallOfModel = 0f, totalPrecisionOfModel = 0f, totalHMeanOfModel = 0f;
                    int sumMatchG = 0, sumMatchD = 0;
                    int zeroCounter = 0;
                    int totalRectanglesDetected = 0;
                    float[] recallsOfModel = new float[rectFilesPerImage.Length]; // number of images
                    float[] precisionsOfModel = new float[rectFilesPerImage.Length];
                    for (int j = 0; j < rectFilesPerImage.Length; j++) // eval for each file
                    {
                        MethodInfo ResponsesToDataset = modelContainer[i].GetType().GetMethod("ResponsesToDataset");
                        MethodInfo ResponsesToDatasetT = ResponsesToDataset.MakeGenericMethod(t);
                        List<RectangleF> detectedRects = (List<RectangleF>)ResponsesToDatasetT.Invoke(modelContainer[i], new object[] { rectFilesPerImage[j].ToArray() });
                        List<RectangleF> gtRects = IOUtils.LoadFileToRectListF(gtFiles[j], ',');
                        totalRectanglesDetected += detectedRects.Count;

                        double[,] sigma = new double[gtRects.Count, detectedRects.Count];
                        double[,] taf = new double[gtRects.Count, detectedRects.Count];
                        double[,] ypsilon = new double[gtRects.Count, gtRects.Count];
                        for (int k = 0; k < gtRects.Count; k++)
                        {
                            for (int l = 0; l < detectedRects.Count; l++)
                            {
                                sigma[k, l] = EvalUtils.CalcRecallAreaRect(gtRects[k], detectedRects[l]);
                                taf[k, l] = EvalUtils.CalcPrecAreaRect(gtRects[k], detectedRects[l]);
                            }
                        }
                        for (int k = 0; k < gtRects.Count; k++)
                        {
                            for (int m = 0; m < gtRects.Count; m++)
                                ypsilon[k, m] = EvalUtils.CalcRecallAreaRect(gtRects[k], gtRects[m]);
                        }
                        float[] matchD;
                        float[] matchG = EvalUtils.CalcMatches(sigma, taf, ypsilon, 0.8, 0.4, gtRects, detectedRects, out matchD);

                        recallsOfModel[j] = 0f;
                        for (int k = 0; k < matchG.Length; k++)
                            recallsOfModel[j] += matchG[k];

                        totalRecallOfModel += recallsOfModel[j];
                        sumMatchG += matchG.Length;

                        precisionsOfModel[j] = 0f;
                        for (int k = 0; k < matchD.Length; k++)
                            precisionsOfModel[j] += matchD[k];

                        totalPrecisionOfModel += precisionsOfModel[j];
                        sumMatchD += matchD.Length;
                        double hMean = Utils.CalcHMean(recallsOfModel[j] / matchG.Length, precisionsOfModel[j] / matchD.Length);

                        if (double.IsNaN(hMean) || hMean == 0) zeroCounter++;
                    }
                    totalHMeanOfModel = Utils.CalcHMean(totalRecallOfModel / sumMatchG, totalPrecisionOfModel / sumMatchD);

                    modelContainer[i].Recall = totalRecallOfModel / sumMatchG;
                    modelContainer[i].Precision = totalPrecisionOfModel / sumMatchD;
                    modelContainer[i].HMean = totalHMeanOfModel;
                    /// kai an aksizei ton kopo na to souloupwsw ligo
                    /// na ftiaksw to test dataset
                    /// na ftiaksw kamia 1000 montela kai na ta kanw evaluate ... 

                }
            }
        }
        #endregion



    }
}
