﻿namespace WordLocRec.Forms
{
    partial class ChoosePosNeg
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.buttonUndo = new System.Windows.Forms.Button();
            this.textBoxViewIndex = new System.Windows.Forms.TextBox();
            this.textBoxNegative = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.textBoxERInfo = new System.Windows.Forms.TextBox();
            this.buttonNegative = new System.Windows.Forms.Button();
            this.buttonPositive = new System.Windows.Forms.Button();
            this.buttonNext = new System.Windows.Forms.Button();
            this.buttonPrev = new System.Windows.Forms.Button();
            this.textBoxPositive = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.textBoxTitle = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.textBoxAvailable = new System.Windows.Forms.TextBox();
            this.pictureBoxER = new System.Windows.Forms.PictureBox();
            this.buttonAmbiguous = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.textBoxAmbiguous = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxER)).BeginInit();
            this.SuspendLayout();
            // 
            // buttonUndo
            // 
            this.buttonUndo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonUndo.BackColor = System.Drawing.Color.LightSteelBlue;
            this.buttonUndo.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonUndo.Location = new System.Drawing.Point(279, 292);
            this.buttonUndo.Name = "buttonUndo";
            this.buttonUndo.Size = new System.Drawing.Size(75, 40);
            this.buttonUndo.TabIndex = 48;
            this.buttonUndo.Text = "Undo";
            this.buttonUndo.UseVisualStyleBackColor = false;
            this.buttonUndo.Click += new System.EventHandler(this.buttonUndo_Click);
            // 
            // textBoxViewIndex
            // 
            this.textBoxViewIndex.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.textBoxViewIndex.Location = new System.Drawing.Point(360, 340);
            this.textBoxViewIndex.Name = "textBoxViewIndex";
            this.textBoxViewIndex.Size = new System.Drawing.Size(28, 20);
            this.textBoxViewIndex.TabIndex = 47;
            // 
            // textBoxNegative
            // 
            this.textBoxNegative.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxNegative.Location = new System.Drawing.Point(467, 95);
            this.textBoxNegative.Name = "textBoxNegative";
            this.textBoxNegative.ReadOnly = true;
            this.textBoxNegative.Size = new System.Drawing.Size(86, 20);
            this.textBoxNegative.TabIndex = 46;
            // 
            // label3
            // 
            this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label3.AutoSize = true;
            this.label3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label3.Location = new System.Drawing.Point(360, 98);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(53, 13);
            this.label3.TabIndex = 45;
            this.label3.Text = "Negative:";
            // 
            // label4
            // 
            this.label4.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label4.AutoSize = true;
            this.label4.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label4.Location = new System.Drawing.Point(565, 7);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(92, 13);
            this.label4.TabIndex = 44;
            this.label4.Text = "Example Statistics";
            // 
            // textBoxERInfo
            // 
            this.textBoxERInfo.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxERInfo.Location = new System.Drawing.Point(568, 23);
            this.textBoxERInfo.Multiline = true;
            this.textBoxERInfo.Name = "textBoxERInfo";
            this.textBoxERInfo.ReadOnly = true;
            this.textBoxERInfo.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.textBoxERInfo.Size = new System.Drawing.Size(179, 338);
            this.textBoxERInfo.TabIndex = 43;
            this.textBoxERInfo.Text = "Nothing available atm";
            // 
            // buttonNegative
            // 
            this.buttonNegative.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonNegative.BackColor = System.Drawing.Color.LightSteelBlue;
            this.buttonNegative.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonNegative.Location = new System.Drawing.Point(110, 292);
            this.buttonNegative.Name = "buttonNegative";
            this.buttonNegative.Size = new System.Drawing.Size(77, 69);
            this.buttonNegative.TabIndex = 42;
            this.buttonNegative.Text = "Negative Example";
            this.buttonNegative.UseVisualStyleBackColor = false;
            this.buttonNegative.Click += new System.EventHandler(this.buttonNegative_Click);
            // 
            // buttonPositive
            // 
            this.buttonPositive.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonPositive.BackColor = System.Drawing.Color.LightSteelBlue;
            this.buttonPositive.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonPositive.Location = new System.Drawing.Point(12, 292);
            this.buttonPositive.Name = "buttonPositive";
            this.buttonPositive.Size = new System.Drawing.Size(92, 69);
            this.buttonPositive.TabIndex = 41;
            this.buttonPositive.Text = "Positive Example";
            this.buttonPositive.UseVisualStyleBackColor = false;
            this.buttonPositive.Click += new System.EventHandler(this.buttonPositive_Click);
            // 
            // buttonNext
            // 
            this.buttonNext.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonNext.BackColor = System.Drawing.Color.LightSteelBlue;
            this.buttonNext.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonNext.Location = new System.Drawing.Point(394, 338);
            this.buttonNext.Name = "buttonNext";
            this.buttonNext.Size = new System.Drawing.Size(75, 23);
            this.buttonNext.TabIndex = 40;
            this.buttonNext.Text = "Next";
            this.buttonNext.UseVisualStyleBackColor = false;
            this.buttonNext.Click += new System.EventHandler(this.buttonNext_Click);
            // 
            // buttonPrev
            // 
            this.buttonPrev.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonPrev.BackColor = System.Drawing.Color.LightSteelBlue;
            this.buttonPrev.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonPrev.Location = new System.Drawing.Point(279, 338);
            this.buttonPrev.Name = "buttonPrev";
            this.buttonPrev.Size = new System.Drawing.Size(75, 23);
            this.buttonPrev.TabIndex = 39;
            this.buttonPrev.Text = "Previous";
            this.buttonPrev.UseVisualStyleBackColor = false;
            this.buttonPrev.Click += new System.EventHandler(this.buttonPrev_Click);
            // 
            // textBoxPositive
            // 
            this.textBoxPositive.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxPositive.Location = new System.Drawing.Point(467, 69);
            this.textBoxPositive.Name = "textBoxPositive";
            this.textBoxPositive.ReadOnly = true;
            this.textBoxPositive.Size = new System.Drawing.Size(86, 20);
            this.textBoxPositive.TabIndex = 38;
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label2.AutoSize = true;
            this.label2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label2.Location = new System.Drawing.Point(360, 72);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(47, 13);
            this.label2.TabIndex = 37;
            this.label2.Text = "Positive:";
            // 
            // textBoxTitle
            // 
            this.textBoxTitle.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxTitle.Location = new System.Drawing.Point(363, 12);
            this.textBoxTitle.Name = "textBoxTitle";
            this.textBoxTitle.Size = new System.Drawing.Size(190, 20);
            this.textBoxTitle.TabIndex = 36;
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label1.AutoSize = true;
            this.label1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label1.Location = new System.Drawing.Point(360, 41);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(103, 13);
            this.label1.TabIndex = 35;
            this.label1.Text = "Available examples :";
            // 
            // textBoxAvailable
            // 
            this.textBoxAvailable.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxAvailable.Location = new System.Drawing.Point(467, 38);
            this.textBoxAvailable.Name = "textBoxAvailable";
            this.textBoxAvailable.ReadOnly = true;
            this.textBoxAvailable.Size = new System.Drawing.Size(86, 20);
            this.textBoxAvailable.TabIndex = 34;
            // 
            // pictureBoxER
            // 
            this.pictureBoxER.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureBoxER.Location = new System.Drawing.Point(12, 12);
            this.pictureBoxER.Name = "pictureBoxER";
            this.pictureBoxER.Size = new System.Drawing.Size(342, 272);
            this.pictureBoxER.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBoxER.TabIndex = 33;
            this.pictureBoxER.TabStop = false;
            // 
            // buttonAmbiguous
            // 
            this.buttonAmbiguous.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonAmbiguous.BackColor = System.Drawing.Color.LightSteelBlue;
            this.buttonAmbiguous.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonAmbiguous.Location = new System.Drawing.Point(193, 292);
            this.buttonAmbiguous.Name = "buttonAmbiguous";
            this.buttonAmbiguous.Size = new System.Drawing.Size(77, 69);
            this.buttonAmbiguous.TabIndex = 49;
            this.buttonAmbiguous.Text = "Ambiguous Example";
            this.buttonAmbiguous.UseVisualStyleBackColor = false;
            this.buttonAmbiguous.Click += new System.EventHandler(this.buttonAmbiguous_Click);
            // 
            // label5
            // 
            this.label5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label5.AutoSize = true;
            this.label5.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label5.Location = new System.Drawing.Point(360, 124);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(62, 13);
            this.label5.TabIndex = 50;
            this.label5.Text = "Ambiguous:";
            // 
            // textBoxAmbiguous
            // 
            this.textBoxAmbiguous.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxAmbiguous.Location = new System.Drawing.Point(467, 121);
            this.textBoxAmbiguous.Name = "textBoxAmbiguous";
            this.textBoxAmbiguous.ReadOnly = true;
            this.textBoxAmbiguous.Size = new System.Drawing.Size(86, 20);
            this.textBoxAmbiguous.TabIndex = 51;
            // 
            // ChoosePosNeg
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.GhostWhite;
            this.ClientSize = new System.Drawing.Size(757, 373);
            this.Controls.Add(this.textBoxAmbiguous);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.buttonAmbiguous);
            this.Controls.Add(this.buttonUndo);
            this.Controls.Add(this.textBoxViewIndex);
            this.Controls.Add(this.textBoxNegative);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.textBoxERInfo);
            this.Controls.Add(this.buttonNegative);
            this.Controls.Add(this.buttonPositive);
            this.Controls.Add(this.buttonNext);
            this.Controls.Add(this.buttonPrev);
            this.Controls.Add(this.textBoxPositive);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.textBoxTitle);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.textBoxAvailable);
            this.Controls.Add(this.pictureBoxER);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.SizableToolWindow;
            this.MinimumSize = new System.Drawing.Size(600, 300);
            this.Name = "ChoosePosNeg";
            this.Text = "ChoosePosNeg";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxER)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button buttonUndo;
        private System.Windows.Forms.TextBox textBoxViewIndex;
        private System.Windows.Forms.TextBox textBoxNegative;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox textBoxERInfo;
        private System.Windows.Forms.Button buttonNegative;
        private System.Windows.Forms.Button buttonPositive;
        private System.Windows.Forms.Button buttonNext;
        private System.Windows.Forms.Button buttonPrev;
        private System.Windows.Forms.TextBox textBoxPositive;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox textBoxTitle;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBoxAvailable;
        private System.Windows.Forms.PictureBox pictureBoxER;
        private System.Windows.Forms.Button buttonAmbiguous;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox textBoxAmbiguous;
    }
}