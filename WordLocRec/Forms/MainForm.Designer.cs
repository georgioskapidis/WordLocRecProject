﻿namespace WordLocRec
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.menuBar = new System.Windows.Forms.MenuStrip();
            this.testToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.loadImagesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.loadGroundTruthFilesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.optionsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.rectanglesFromChannelsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.chooseFeatureTypeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.moreToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.createDatasetsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.calculateDatasetStatisticsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.createModelsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.multipleImagesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.aboutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pictureBoxMain = new System.Windows.Forms.PictureBox();
            this.panelPicBox = new System.Windows.Forms.Panel();
            this.ZoomOut = new System.Windows.Forms.Button();
            this.ZoomIn = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.buttonMSER = new System.Windows.Forms.Button();
            this.buttonERNative = new System.Windows.Forms.Button();
            this.TestMyNM = new System.Windows.Forms.Button();
            this.textBoxThreshD = new System.Windows.Forms.TextBox();
            this.textBoxMinAr = new System.Windows.Forms.TextBox();
            this.textBoxMaxAr = new System.Windows.Forms.TextBox();
            this.textBoxMinProb = new System.Windows.Forms.TextBox();
            this.textBoxMinProbDiff = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.checkBoxClassifiers = new System.Windows.Forms.CheckBox();
            this.buttonCanny = new System.Windows.Forms.Button();
            this.textBoxThresh1 = new System.Windows.Forms.TextBox();
            this.textBoxThresh2 = new System.Windows.Forms.TextBox();
            this.checkBoxNMCanny = new System.Windows.Forms.CheckBox();
            this.checkBoxCannyGrouping = new System.Windows.Forms.CheckBox();
            this.buttonHybrid1 = new System.Windows.Forms.Button();
            this.buttonLayers = new System.Windows.Forms.Button();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.checkBoxCHAll = new System.Windows.Forms.CheckBox();
            this.checkBoxCH3 = new System.Windows.Forms.CheckBox();
            this.checkBoxCH2 = new System.Windows.Forms.CheckBox();
            this.checkBoxCH1 = new System.Windows.Forms.CheckBox();
            this.checkBoxCHNot = new System.Windows.Forms.CheckBox();
            this.buttonRevert = new System.Windows.Forms.Button();
            this.checkBox2ndStage = new System.Windows.Forms.CheckBox();
            this.checkBoxNMS = new System.Windows.Forms.CheckBox();
            this.checkBox2ndClassifier = new System.Windows.Forms.CheckBox();
            this.panelERData = new System.Windows.Forms.Panel();
            this.textBoxERData = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.buttonCER = new System.Windows.Forms.Button();
            this.buttonSaveViewedImage = new System.Windows.Forms.Button();
            this.buttonERExtractor = new System.Windows.Forms.Button();
            this.tabControlMainForm = new System.Windows.Forms.TabControl();
            this.tabPageViewOptions = new System.Windows.Forms.TabPage();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.labelSeparator = new System.Windows.Forms.Label();
            this.numericUpDownGamma = new System.Windows.Forms.NumericUpDown();
            this.label6 = new System.Windows.Forms.Label();
            this.numericUpDown2 = new System.Windows.Forms.NumericUpDown();
            this.numericUpDown1 = new System.Windows.Forms.NumericUpDown();
            this.checkBoxWiener = new System.Windows.Forms.CheckBox();
            this.checkBoxGS = new System.Windows.Forms.CheckBox();
            this.checkBoxGammaCorrect = new System.Windows.Forms.CheckBox();
            this.checkBoxSmooth = new System.Windows.Forms.CheckBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.buttonViewBgr = new System.Windows.Forms.Button();
            this.buttonViewLab = new System.Windows.Forms.Button();
            this.buttonViewHSV = new System.Windows.Forms.Button();
            this.buttonSobelMultiGrad = new System.Windows.Forms.Button();
            this.buttonViewHSV2 = new System.Windows.Forms.Button();
            this.buttonMultiGrad = new System.Windows.Forms.Button();
            this.buttonViewXYZ = new System.Windows.Forms.Button();
            this.buttonViewPill = new System.Windows.Forms.Button();
            this.buttonViewGrayscale = new System.Windows.Forms.Button();
            this.buttonViewGradient = new System.Windows.Forms.Button();
            this.tabPageExtraction = new System.Windows.Forms.TabPage();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.radioButtonNormal = new System.Windows.Forms.RadioButton();
            this.radioButtonStrictNMS = new System.Windows.Forms.RadioButton();
            this.checkBoxFeedbackGrouping = new System.Windows.Forms.CheckBox();
            this.checkBoxWordPruning = new System.Windows.Forms.CheckBox();
            this.checkBoxGrouping = new System.Windows.Forms.CheckBox();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.buttonChannels = new System.Windows.Forms.Button();
            this.tabMore = new System.Windows.Forms.TabPage();
            this.groupBox8 = new System.Windows.Forms.GroupBox();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.buttonThinningDemo = new System.Windows.Forms.Button();
            this.toolTipMainForm = new System.Windows.Forms.ToolTip(this.components);
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.NMWorkerProgress = new System.Windows.Forms.ToolStripStatusLabel();
            this.labelStatus = new System.Windows.Forms.ToolStripStatusLabel();
            this.groupBox9 = new System.Windows.Forms.GroupBox();
            this.groupBox10 = new System.Windows.Forms.GroupBox();
            this.menuBar.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxMain)).BeginInit();
            this.panelPicBox.SuspendLayout();
            this.panelERData.SuspendLayout();
            this.tabControlMainForm.SuspendLayout();
            this.tabPageViewOptions.SuspendLayout();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownGamma)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).BeginInit();
            this.groupBox2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.tabPageExtraction.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.tabMore.SuspendLayout();
            this.groupBox8.SuspendLayout();
            this.groupBox7.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            this.groupBox9.SuspendLayout();
            this.groupBox10.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuBar
            // 
            this.menuBar.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.testToolStripMenuItem,
            this.optionsToolStripMenuItem,
            this.moreToolStripMenuItem,
            this.aboutToolStripMenuItem});
            this.menuBar.Location = new System.Drawing.Point(0, 0);
            this.menuBar.Name = "menuBar";
            this.menuBar.Size = new System.Drawing.Size(1072, 24);
            this.menuBar.TabIndex = 0;
            this.menuBar.Text = "menuStrip1";
            // 
            // testToolStripMenuItem
            // 
            this.testToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.loadImagesToolStripMenuItem,
            this.loadGroundTruthFilesToolStripMenuItem,
            this.exitToolStripMenuItem});
            this.testToolStripMenuItem.Name = "testToolStripMenuItem";
            this.testToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.testToolStripMenuItem.Text = "File";
            // 
            // loadImagesToolStripMenuItem
            // 
            this.loadImagesToolStripMenuItem.Name = "loadImagesToolStripMenuItem";
            this.loadImagesToolStripMenuItem.Size = new System.Drawing.Size(207, 22);
            this.loadImagesToolStripMenuItem.Text = "Load Test Image ...";
            this.loadImagesToolStripMenuItem.Click += new System.EventHandler(this.loadSingleImageToolStripMenuItem_Click);
            // 
            // loadGroundTruthFilesToolStripMenuItem
            // 
            this.loadGroundTruthFilesToolStripMenuItem.Name = "loadGroundTruthFilesToolStripMenuItem";
            this.loadGroundTruthFilesToolStripMenuItem.Size = new System.Drawing.Size(207, 22);
            this.loadGroundTruthFilesToolStripMenuItem.Text = "Load Ground Truth File ...";
            this.loadGroundTruthFilesToolStripMenuItem.Click += new System.EventHandler(this.loadGroundTruthFilesToolStripMenuItem_Click);
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(207, 22);
            this.exitToolStripMenuItem.Text = "Exit";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
            // 
            // optionsToolStripMenuItem
            // 
            this.optionsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.rectanglesFromChannelsToolStripMenuItem,
            this.chooseFeatureTypeToolStripMenuItem});
            this.optionsToolStripMenuItem.Name = "optionsToolStripMenuItem";
            this.optionsToolStripMenuItem.Size = new System.Drawing.Size(61, 20);
            this.optionsToolStripMenuItem.Text = "Options";
            // 
            // rectanglesFromChannelsToolStripMenuItem
            // 
            this.rectanglesFromChannelsToolStripMenuItem.Name = "rectanglesFromChannelsToolStripMenuItem";
            this.rectanglesFromChannelsToolStripMenuItem.Size = new System.Drawing.Size(224, 22);
            this.rectanglesFromChannelsToolStripMenuItem.Text = "Rectangles from Channels ...";
            this.rectanglesFromChannelsToolStripMenuItem.Click += new System.EventHandler(this.rectanglesFromChannelsToolStripMenuItem_Click);
            // 
            // chooseFeatureTypeToolStripMenuItem
            // 
            this.chooseFeatureTypeToolStripMenuItem.Name = "chooseFeatureTypeToolStripMenuItem";
            this.chooseFeatureTypeToolStripMenuItem.Size = new System.Drawing.Size(224, 22);
            this.chooseFeatureTypeToolStripMenuItem.Text = "Choose Feature Type...";
            this.chooseFeatureTypeToolStripMenuItem.Click += new System.EventHandler(this.chooseFeatureTypeToolStripMenuItem_Click);
            // 
            // moreToolStripMenuItem
            // 
            this.moreToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.createDatasetsToolStripMenuItem,
            this.calculateDatasetStatisticsToolStripMenuItem,
            this.createModelsToolStripMenuItem,
            this.multipleImagesToolStripMenuItem});
            this.moreToolStripMenuItem.Name = "moreToolStripMenuItem";
            this.moreToolStripMenuItem.Size = new System.Drawing.Size(47, 20);
            this.moreToolStripMenuItem.Text = "More";
            // 
            // createDatasetsToolStripMenuItem
            // 
            this.createDatasetsToolStripMenuItem.Name = "createDatasetsToolStripMenuItem";
            this.createDatasetsToolStripMenuItem.Size = new System.Drawing.Size(231, 22);
            this.createDatasetsToolStripMenuItem.Text = "Create Datasets...";
            this.createDatasetsToolStripMenuItem.Click += new System.EventHandler(this.createDatasetsToolStripMenuItem_Click);
            // 
            // calculateDatasetStatisticsToolStripMenuItem
            // 
            this.calculateDatasetStatisticsToolStripMenuItem.Name = "calculateDatasetStatisticsToolStripMenuItem";
            this.calculateDatasetStatisticsToolStripMenuItem.Size = new System.Drawing.Size(231, 22);
            this.calculateDatasetStatisticsToolStripMenuItem.Text = "Calculate Dataset Statistics...";
            this.calculateDatasetStatisticsToolStripMenuItem.Click += new System.EventHandler(this.calculateDatasetStatisticsToolStripMenuItem_Click);
            // 
            // createModelsToolStripMenuItem
            // 
            this.createModelsToolStripMenuItem.Name = "createModelsToolStripMenuItem";
            this.createModelsToolStripMenuItem.Size = new System.Drawing.Size(231, 22);
            this.createModelsToolStripMenuItem.Text = "Create Models...";
            this.createModelsToolStripMenuItem.Click += new System.EventHandler(this.createModelsToolStripMenuItem_Click);
            // 
            // multipleImagesToolStripMenuItem
            // 
            this.multipleImagesToolStripMenuItem.Name = "multipleImagesToolStripMenuItem";
            this.multipleImagesToolStripMenuItem.Size = new System.Drawing.Size(231, 22);
            this.multipleImagesToolStripMenuItem.Text = "Multiple Image Calculations...";
            this.multipleImagesToolStripMenuItem.Click += new System.EventHandler(this.workerFormToolStripMenuItem_Click);
            // 
            // aboutToolStripMenuItem
            // 
            this.aboutToolStripMenuItem.Name = "aboutToolStripMenuItem";
            this.aboutToolStripMenuItem.Size = new System.Drawing.Size(52, 20);
            this.aboutToolStripMenuItem.Text = "About";
            this.aboutToolStripMenuItem.Click += new System.EventHandler(this.aboutToolStripMenuItem_Click);
            // 
            // pictureBoxMain
            // 
            this.pictureBoxMain.Cursor = System.Windows.Forms.Cursors.Cross;
            this.pictureBoxMain.Location = new System.Drawing.Point(3, 3);
            this.pictureBoxMain.Name = "pictureBoxMain";
            this.pictureBoxMain.Size = new System.Drawing.Size(377, 220);
            this.pictureBoxMain.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBoxMain.TabIndex = 1;
            this.pictureBoxMain.TabStop = false;
            // 
            // panelPicBox
            // 
            this.panelPicBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panelPicBox.AutoScroll = true;
            this.panelPicBox.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panelPicBox.Controls.Add(this.pictureBoxMain);
            this.panelPicBox.Location = new System.Drawing.Point(12, 27);
            this.panelPicBox.Name = "panelPicBox";
            this.panelPicBox.Size = new System.Drawing.Size(829, 556);
            this.panelPicBox.TabIndex = 2;
            // 
            // ZoomOut
            // 
            this.ZoomOut.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ZoomOut.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ZoomOut.Location = new System.Drawing.Point(68, 12);
            this.ZoomOut.Name = "ZoomOut";
            this.ZoomOut.Size = new System.Drawing.Size(35, 35);
            this.ZoomOut.TabIndex = 2;
            this.ZoomOut.Text = "-";
            this.ZoomOut.UseVisualStyleBackColor = true;
            this.ZoomOut.Click += new System.EventHandler(this.ZoomOut_Click);
            // 
            // ZoomIn
            // 
            this.ZoomIn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ZoomIn.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ZoomIn.Location = new System.Drawing.Point(109, 12);
            this.ZoomIn.Name = "ZoomIn";
            this.ZoomIn.Size = new System.Drawing.Size(35, 35);
            this.ZoomIn.TabIndex = 3;
            this.ZoomIn.Text = "+";
            this.ZoomIn.UseVisualStyleBackColor = true;
            this.ZoomIn.Click += new System.EventHandler(this.ZoomIn_Click);
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.LightSteelBlue;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.Location = new System.Drawing.Point(10, 19);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(94, 23);
            this.button1.TabIndex = 3;
            this.button1.Text = "TestButton1";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // buttonMSER
            // 
            this.buttonMSER.BackColor = System.Drawing.Color.LightSteelBlue;
            this.buttonMSER.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonMSER.Location = new System.Drawing.Point(77, 19);
            this.buttonMSER.Name = "buttonMSER";
            this.buttonMSER.Size = new System.Drawing.Size(75, 23);
            this.buttonMSER.TabIndex = 4;
            this.buttonMSER.Text = "MSER";
            this.buttonMSER.UseVisualStyleBackColor = false;
            this.buttonMSER.Click += new System.EventHandler(this.buttonMSER_Click);
            // 
            // buttonERNative
            // 
            this.buttonERNative.BackColor = System.Drawing.Color.LightSteelBlue;
            this.buttonERNative.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonERNative.Location = new System.Drawing.Point(31, 48);
            this.buttonERNative.Name = "buttonERNative";
            this.buttonERNative.Size = new System.Drawing.Size(75, 23);
            this.buttonERNative.TabIndex = 5;
            this.buttonERNative.Text = "Native ER";
            this.buttonERNative.UseVisualStyleBackColor = false;
            this.buttonERNative.Click += new System.EventHandler(this.buttonERNative_Click);
            // 
            // TestMyNM
            // 
            this.TestMyNM.BackColor = System.Drawing.Color.LightSteelBlue;
            this.TestMyNM.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.TestMyNM.Location = new System.Drawing.Point(112, 48);
            this.TestMyNM.Name = "TestMyNM";
            this.TestMyNM.Size = new System.Drawing.Size(75, 23);
            this.TestMyNM.TabIndex = 7;
            this.TestMyNM.Text = "Transl. ER";
            this.TestMyNM.UseVisualStyleBackColor = false;
            this.TestMyNM.Click += new System.EventHandler(this.TestMyNM_Click);
            // 
            // textBoxThreshD
            // 
            this.textBoxThreshD.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxThreshD.Location = new System.Drawing.Point(134, 29);
            this.textBoxThreshD.Name = "textBoxThreshD";
            this.textBoxThreshD.Size = new System.Drawing.Size(60, 20);
            this.textBoxThreshD.TabIndex = 9;
            this.textBoxThreshD.Text = "1";
            // 
            // textBoxMinAr
            // 
            this.textBoxMinAr.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxMinAr.Location = new System.Drawing.Point(134, 55);
            this.textBoxMinAr.Name = "textBoxMinAr";
            this.textBoxMinAr.Size = new System.Drawing.Size(60, 20);
            this.textBoxMinAr.TabIndex = 10;
            this.textBoxMinAr.Text = "0,00005";
            // 
            // textBoxMaxAr
            // 
            this.textBoxMaxAr.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxMaxAr.Location = new System.Drawing.Point(134, 81);
            this.textBoxMaxAr.Name = "textBoxMaxAr";
            this.textBoxMaxAr.Size = new System.Drawing.Size(60, 20);
            this.textBoxMaxAr.TabIndex = 11;
            this.textBoxMaxAr.Text = "0,13";
            // 
            // textBoxMinProb
            // 
            this.textBoxMinProb.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxMinProb.Location = new System.Drawing.Point(134, 107);
            this.textBoxMinProb.Name = "textBoxMinProb";
            this.textBoxMinProb.Size = new System.Drawing.Size(60, 20);
            this.textBoxMinProb.TabIndex = 12;
            this.textBoxMinProb.Text = "0,2";
            // 
            // textBoxMinProbDiff
            // 
            this.textBoxMinProbDiff.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxMinProbDiff.Location = new System.Drawing.Point(134, 134);
            this.textBoxMinProbDiff.Name = "textBoxMinProbDiff";
            this.textBoxMinProbDiff.Size = new System.Drawing.Size(60, 20);
            this.textBoxMinProbDiff.TabIndex = 13;
            this.textBoxMinProbDiff.Text = "0,1";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(4, 32);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(102, 13);
            this.label1.TabIndex = 14;
            this.label1.Text = "Threshold Delta (int)";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(4, 58);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(78, 13);
            this.label2.TabIndex = 15;
            this.label2.Text = "Min Area (float)";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(4, 84);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(81, 13);
            this.label3.TabIndex = 15;
            this.label3.Text = "Max Area (float)";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(4, 110);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(104, 13);
            this.label4.TabIndex = 16;
            this.label4.Text = "Min Probability (float)";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(4, 137);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(133, 13);
            this.label5.TabIndex = 17;
            this.label5.Text = "Min Prob. Difference (float)";
            // 
            // checkBoxClassifiers
            // 
            this.checkBoxClassifiers.AutoSize = true;
            this.checkBoxClassifiers.Checked = true;
            this.checkBoxClassifiers.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxClassifiers.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.checkBoxClassifiers.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBoxClassifiers.Location = new System.Drawing.Point(6, 20);
            this.checkBoxClassifiers.Name = "checkBoxClassifiers";
            this.checkBoxClassifiers.Size = new System.Drawing.Size(138, 17);
            this.checkBoxClassifiers.TabIndex = 19;
            this.checkBoxClassifiers.Text = "Default Classifier Models";
            this.checkBoxClassifiers.UseVisualStyleBackColor = true;
            // 
            // buttonCanny
            // 
            this.buttonCanny.BackColor = System.Drawing.Color.LightSteelBlue;
            this.buttonCanny.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonCanny.Location = new System.Drawing.Point(4, 19);
            this.buttonCanny.Name = "buttonCanny";
            this.buttonCanny.Size = new System.Drawing.Size(75, 23);
            this.buttonCanny.TabIndex = 23;
            this.buttonCanny.Text = "CannyTest";
            this.buttonCanny.UseVisualStyleBackColor = false;
            this.buttonCanny.Click += new System.EventHandler(this.buttonCanny_Click);
            // 
            // textBoxThresh1
            // 
            this.textBoxThresh1.Location = new System.Drawing.Point(7, 61);
            this.textBoxThresh1.Name = "textBoxThresh1";
            this.textBoxThresh1.Size = new System.Drawing.Size(52, 20);
            this.textBoxThresh1.TabIndex = 24;
            this.textBoxThresh1.Text = "30";
            // 
            // textBoxThresh2
            // 
            this.textBoxThresh2.Location = new System.Drawing.Point(65, 61);
            this.textBoxThresh2.Name = "textBoxThresh2";
            this.textBoxThresh2.Size = new System.Drawing.Size(52, 20);
            this.textBoxThresh2.TabIndex = 25;
            this.textBoxThresh2.Text = "100";
            // 
            // checkBoxNMCanny
            // 
            this.checkBoxNMCanny.AutoSize = true;
            this.checkBoxNMCanny.Checked = true;
            this.checkBoxNMCanny.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxNMCanny.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.checkBoxNMCanny.Location = new System.Drawing.Point(10, 87);
            this.checkBoxNMCanny.Name = "checkBoxNMCanny";
            this.checkBoxNMCanny.Size = new System.Drawing.Size(92, 17);
            this.checkBoxNMCanny.TabIndex = 27;
            this.checkBoxNMCanny.Text = "NMAfterCanny";
            this.checkBoxNMCanny.UseVisualStyleBackColor = true;
            this.checkBoxNMCanny.CheckedChanged += new System.EventHandler(this.checkBoxNMCanny_CheckedChanged);
            // 
            // checkBoxCannyGrouping
            // 
            this.checkBoxCannyGrouping.AutoSize = true;
            this.checkBoxCannyGrouping.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.checkBoxCannyGrouping.Location = new System.Drawing.Point(10, 110);
            this.checkBoxCannyGrouping.Name = "checkBoxCannyGrouping";
            this.checkBoxCannyGrouping.Size = new System.Drawing.Size(123, 17);
            this.checkBoxCannyGrouping.TabIndex = 28;
            this.checkBoxCannyGrouping.Text = "NMCanny + grouping";
            this.checkBoxCannyGrouping.UseVisualStyleBackColor = true;
            // 
            // buttonHybrid1
            // 
            this.buttonHybrid1.BackColor = System.Drawing.Color.LightSteelBlue;
            this.buttonHybrid1.Enabled = false;
            this.buttonHybrid1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonHybrid1.Location = new System.Drawing.Point(7, 126);
            this.buttonHybrid1.Name = "buttonHybrid1";
            this.buttonHybrid1.Size = new System.Drawing.Size(68, 23);
            this.buttonHybrid1.TabIndex = 29;
            this.buttonHybrid1.Text = "Hybrid ER";
            this.buttonHybrid1.UseVisualStyleBackColor = false;
            this.buttonHybrid1.Visible = false;
            this.buttonHybrid1.Click += new System.EventHandler(this.buttonHybrid1_Click);
            // 
            // buttonLayers
            // 
            this.buttonLayers.BackColor = System.Drawing.Color.LightSteelBlue;
            this.buttonLayers.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonLayers.Location = new System.Drawing.Point(110, 19);
            this.buttonLayers.Name = "buttonLayers";
            this.buttonLayers.Size = new System.Drawing.Size(94, 23);
            this.buttonLayers.TabIndex = 30;
            this.buttonLayers.Text = "Data Handling";
            this.buttonLayers.UseVisualStyleBackColor = false;
            this.buttonLayers.Click += new System.EventHandler(this.buttonLayers_Click);
            // 
            // label7
            // 
            this.label7.Dock = System.Windows.Forms.DockStyle.Top;
            this.label7.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(3, 3);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(211, 28);
            this.label7.TabIndex = 31;
            this.label7.Text = "ER/MSER Buttons";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label8.Location = new System.Drawing.Point(4, 45);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(151, 13);
            this.label8.TabIndex = 32;
            this.label8.Text = "Canny Thres. 1 and 2 (0 - 255)";
            // 
            // label11
            // 
            this.label11.Dock = System.Windows.Forms.DockStyle.Top;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(3, 3);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(211, 28);
            this.label11.TabIndex = 35;
            this.label11.Text = "Viewing Options";
            this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label12
            // 
            this.label12.Dock = System.Windows.Forms.DockStyle.Top;
            this.label12.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(3, 3);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(211, 28);
            this.label12.TabIndex = 36;
            this.label12.Text = "Test Buttons";
            this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // checkBoxCHAll
            // 
            this.checkBoxCHAll.AutoSize = true;
            this.checkBoxCHAll.Checked = true;
            this.checkBoxCHAll.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxCHAll.Location = new System.Drawing.Point(6, 42);
            this.checkBoxCHAll.Name = "checkBoxCHAll";
            this.checkBoxCHAll.Size = new System.Drawing.Size(39, 19);
            this.checkBoxCHAll.TabIndex = 3;
            this.checkBoxCHAll.Text = "All";
            this.checkBoxCHAll.UseVisualStyleBackColor = true;
            this.checkBoxCHAll.CheckedChanged += new System.EventHandler(this.checkBoxCHAll_CheckedChanged);
            // 
            // checkBoxCH3
            // 
            this.checkBoxCH3.AutoSize = true;
            this.checkBoxCH3.Location = new System.Drawing.Point(126, 17);
            this.checkBoxCH3.Name = "checkBoxCH3";
            this.checkBoxCH3.Size = new System.Drawing.Size(54, 19);
            this.checkBoxCH3.TabIndex = 2;
            this.checkBoxCH3.Text = "Ch. 3";
            this.checkBoxCH3.UseVisualStyleBackColor = true;
            this.checkBoxCH3.CheckedChanged += new System.EventHandler(this.checkBoxCH3_CheckedChanged);
            // 
            // checkBoxCH2
            // 
            this.checkBoxCH2.AutoSize = true;
            this.checkBoxCH2.Location = new System.Drawing.Point(66, 17);
            this.checkBoxCH2.Name = "checkBoxCH2";
            this.checkBoxCH2.Size = new System.Drawing.Size(54, 19);
            this.checkBoxCH2.TabIndex = 1;
            this.checkBoxCH2.Text = "Ch. 2";
            this.checkBoxCH2.UseVisualStyleBackColor = true;
            this.checkBoxCH2.CheckedChanged += new System.EventHandler(this.checkBoxCH2_CheckedChanged);
            // 
            // checkBoxCH1
            // 
            this.checkBoxCH1.AutoSize = true;
            this.checkBoxCH1.Location = new System.Drawing.Point(6, 17);
            this.checkBoxCH1.Name = "checkBoxCH1";
            this.checkBoxCH1.Size = new System.Drawing.Size(54, 19);
            this.checkBoxCH1.TabIndex = 0;
            this.checkBoxCH1.Text = "Ch. 1";
            this.checkBoxCH1.UseVisualStyleBackColor = true;
            this.checkBoxCH1.CheckedChanged += new System.EventHandler(this.checkBoxCH1_CheckedChanged);
            // 
            // checkBoxCHNot
            // 
            this.checkBoxCHNot.AutoSize = true;
            this.checkBoxCHNot.Location = new System.Drawing.Point(66, 42);
            this.checkBoxCHNot.Name = "checkBoxCHNot";
            this.checkBoxCHNot.Size = new System.Drawing.Size(87, 19);
            this.checkBoxCHNot.TabIndex = 44;
            this.checkBoxCHNot.Text = "Bitwise Not";
            this.checkBoxCHNot.UseVisualStyleBackColor = true;
            // 
            // buttonRevert
            // 
            this.buttonRevert.BackColor = System.Drawing.Color.LightSteelBlue;
            this.buttonRevert.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonRevert.Location = new System.Drawing.Point(28, 12);
            this.buttonRevert.Name = "buttonRevert";
            this.buttonRevert.Size = new System.Drawing.Size(75, 23);
            this.buttonRevert.TabIndex = 45;
            this.buttonRevert.Text = "Undo all";
            this.buttonRevert.UseVisualStyleBackColor = false;
            this.buttonRevert.Click += new System.EventHandler(this.buttonRevert_Click);
            // 
            // checkBox2ndStage
            // 
            this.checkBox2ndStage.AutoSize = true;
            this.checkBox2ndStage.Checked = true;
            this.checkBox2ndStage.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBox2ndStage.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.checkBox2ndStage.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBox2ndStage.Location = new System.Drawing.Point(6, 133);
            this.checkBox2ndStage.Name = "checkBox2ndStage";
            this.checkBox2ndStage.Size = new System.Drawing.Size(110, 17);
            this.checkBox2ndStage.TabIndex = 46;
            this.checkBox2ndStage.Text = "Combine channels";
            this.toolTipMainForm.SetToolTip(this.checkBox2ndStage, "Only for ERExtractor");
            this.checkBox2ndStage.UseVisualStyleBackColor = true;
            // 
            // checkBoxNMS
            // 
            this.checkBoxNMS.AutoSize = true;
            this.checkBoxNMS.Checked = true;
            this.checkBoxNMS.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxNMS.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.checkBoxNMS.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBoxNMS.Location = new System.Drawing.Point(136, 43);
            this.checkBoxNMS.Name = "checkBoxNMS";
            this.checkBoxNMS.Size = new System.Drawing.Size(47, 17);
            this.checkBoxNMS.TabIndex = 47;
            this.checkBoxNMS.Text = "NMS";
            this.checkBoxNMS.UseVisualStyleBackColor = true;
            this.checkBoxNMS.CheckedChanged += new System.EventHandler(this.checkBoxNMS_CheckedChanged);
            // 
            // checkBox2ndClassifier
            // 
            this.checkBox2ndClassifier.AutoSize = true;
            this.checkBox2ndClassifier.Checked = true;
            this.checkBox2ndClassifier.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBox2ndClassifier.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.checkBox2ndClassifier.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBox2ndClassifier.Location = new System.Drawing.Point(6, 43);
            this.checkBox2ndClassifier.Name = "checkBox2ndClassifier";
            this.checkBox2ndClassifier.Size = new System.Drawing.Size(84, 17);
            this.checkBox2ndClassifier.TabIndex = 48;
            this.checkBox2ndClassifier.Text = "2nd classifier";
            this.checkBox2ndClassifier.UseVisualStyleBackColor = true;
            // 
            // panelERData
            // 
            this.panelERData.Controls.Add(this.textBoxERData);
            this.panelERData.Controls.Add(this.label13);
            this.panelERData.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelERData.Location = new System.Drawing.Point(3, 131);
            this.panelERData.Name = "panelERData";
            this.panelERData.Size = new System.Drawing.Size(211, 257);
            this.panelERData.TabIndex = 49;
            // 
            // textBoxERData
            // 
            this.textBoxERData.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxERData.Location = new System.Drawing.Point(3, 24);
            this.textBoxERData.Multiline = true;
            this.textBoxERData.Name = "textBoxERData";
            this.textBoxERData.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.textBoxERData.Size = new System.Drawing.Size(204, 230);
            this.textBoxERData.TabIndex = 1;
            // 
            // label13
            // 
            this.label13.Dock = System.Windows.Forms.DockStyle.Top;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(0, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(211, 21);
            this.label13.TabIndex = 0;
            this.label13.Text = "Extremal Region Data";
            this.label13.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // buttonCER
            // 
            this.buttonCER.BackColor = System.Drawing.Color.LightSteelBlue;
            this.buttonCER.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonCER.Location = new System.Drawing.Point(31, 76);
            this.buttonCER.Name = "buttonCER";
            this.buttonCER.Size = new System.Drawing.Size(75, 23);
            this.buttonCER.TabIndex = 52;
            this.buttonCER.Text = "CER";
            this.buttonCER.UseVisualStyleBackColor = false;
            this.buttonCER.Click += new System.EventHandler(this.buttonCER_Click);
            // 
            // buttonSaveViewedImage
            // 
            this.buttonSaveViewedImage.BackColor = System.Drawing.Color.LightSteelBlue;
            this.buttonSaveViewedImage.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonSaveViewedImage.Location = new System.Drawing.Point(109, 12);
            this.buttonSaveViewedImage.Name = "buttonSaveViewedImage";
            this.buttonSaveViewedImage.Size = new System.Drawing.Size(75, 23);
            this.buttonSaveViewedImage.TabIndex = 54;
            this.buttonSaveViewedImage.Text = "Save as ...";
            this.buttonSaveViewedImage.UseVisualStyleBackColor = false;
            this.buttonSaveViewedImage.Click += new System.EventHandler(this.buttonSaveViewedImage_Click);
            // 
            // buttonERExtractor
            // 
            this.buttonERExtractor.BackColor = System.Drawing.Color.LightSteelBlue;
            this.buttonERExtractor.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonERExtractor.Location = new System.Drawing.Point(112, 76);
            this.buttonERExtractor.Name = "buttonERExtractor";
            this.buttonERExtractor.Size = new System.Drawing.Size(75, 23);
            this.buttonERExtractor.TabIndex = 57;
            this.buttonERExtractor.Text = "ERExtractor";
            this.buttonERExtractor.UseVisualStyleBackColor = false;
            this.buttonERExtractor.Click += new System.EventHandler(this.buttonERExtractor_Click);
            // 
            // tabControlMainForm
            // 
            this.tabControlMainForm.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabControlMainForm.Appearance = System.Windows.Forms.TabAppearance.FlatButtons;
            this.tabControlMainForm.Controls.Add(this.tabPageViewOptions);
            this.tabControlMainForm.Controls.Add(this.tabPageExtraction);
            this.tabControlMainForm.Controls.Add(this.tabMore);
            this.tabControlMainForm.Location = new System.Drawing.Point(847, 24);
            this.tabControlMainForm.Name = "tabControlMainForm";
            this.tabControlMainForm.SelectedIndex = 0;
            this.tabControlMainForm.Size = new System.Drawing.Size(225, 559);
            this.tabControlMainForm.TabIndex = 60;
            // 
            // tabPageViewOptions
            // 
            this.tabPageViewOptions.AutoScroll = true;
            this.tabPageViewOptions.BackColor = System.Drawing.Color.GhostWhite;
            this.tabPageViewOptions.Controls.Add(this.groupBox10);
            this.tabPageViewOptions.Controls.Add(this.groupBox9);
            this.tabPageViewOptions.Controls.Add(this.groupBox3);
            this.tabPageViewOptions.Controls.Add(this.groupBox2);
            this.tabPageViewOptions.Controls.Add(this.groupBox1);
            this.tabPageViewOptions.Controls.Add(this.label11);
            this.tabPageViewOptions.Location = new System.Drawing.Point(4, 25);
            this.tabPageViewOptions.Name = "tabPageViewOptions";
            this.tabPageViewOptions.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageViewOptions.Size = new System.Drawing.Size(217, 530);
            this.tabPageViewOptions.TabIndex = 0;
            this.tabPageViewOptions.Text = "View Options";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.labelSeparator);
            this.groupBox3.Controls.Add(this.numericUpDownGamma);
            this.groupBox3.Controls.Add(this.label6);
            this.groupBox3.Controls.Add(this.numericUpDown2);
            this.groupBox3.Controls.Add(this.numericUpDown1);
            this.groupBox3.Controls.Add(this.checkBoxWiener);
            this.groupBox3.Controls.Add(this.checkBoxGS);
            this.groupBox3.Controls.Add(this.checkBoxGammaCorrect);
            this.groupBox3.Controls.Add(this.checkBoxSmooth);
            this.groupBox3.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox3.Location = new System.Drawing.Point(3, 270);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(211, 165);
            this.groupBox3.TabIndex = 59;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Image preprocessing";
            this.toolTipMainForm.SetToolTip(this.groupBox3, "Note: The options selected at \"Image preprocessing\" apply to extraction as well.");
            // 
            // labelSeparator
            // 
            this.labelSeparator.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.labelSeparator.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.labelSeparator.Location = new System.Drawing.Point(-6, 110);
            this.labelSeparator.Name = "labelSeparator";
            this.labelSeparator.Size = new System.Drawing.Size(211, 2);
            this.labelSeparator.TabIndex = 76;
            // 
            // numericUpDownGamma
            // 
            this.numericUpDownGamma.DecimalPlaces = 1;
            this.numericUpDownGamma.Enabled = false;
            this.numericUpDownGamma.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.numericUpDownGamma.Location = new System.Drawing.Point(50, 137);
            this.numericUpDownGamma.Maximum = new decimal(new int[] {
            30,
            0,
            0,
            0});
            this.numericUpDownGamma.Minimum = new decimal(new int[] {
            30,
            0,
            0,
            -2147483648});
            this.numericUpDownGamma.Name = "numericUpDownGamma";
            this.numericUpDownGamma.Size = new System.Drawing.Size(87, 21);
            this.numericUpDownGamma.TabIndex = 75;
            this.numericUpDownGamma.Value = new decimal(new int[] {
            22,
            0,
            0,
            65536});
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label6.Location = new System.Drawing.Point(47, 67);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(96, 15);
            this.label6.TabIndex = 74;
            this.label6.Text = "Kernel size (x,y):";
            // 
            // numericUpDown2
            // 
            this.numericUpDown2.Enabled = false;
            this.numericUpDown2.Location = new System.Drawing.Point(107, 86);
            this.numericUpDown2.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDown2.Name = "numericUpDown2";
            this.numericUpDown2.Size = new System.Drawing.Size(30, 21);
            this.numericUpDown2.TabIndex = 73;
            this.numericUpDown2.Value = new decimal(new int[] {
            5,
            0,
            0,
            0});
            // 
            // numericUpDown1
            // 
            this.numericUpDown1.Enabled = false;
            this.numericUpDown1.Location = new System.Drawing.Point(50, 86);
            this.numericUpDown1.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDown1.Name = "numericUpDown1";
            this.numericUpDown1.Size = new System.Drawing.Size(30, 21);
            this.numericUpDown1.TabIndex = 72;
            this.numericUpDown1.Value = new decimal(new int[] {
            5,
            0,
            0,
            0});
            // 
            // checkBoxWiener
            // 
            this.checkBoxWiener.AutoSize = true;
            this.checkBoxWiener.Enabled = false;
            this.checkBoxWiener.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.checkBoxWiener.Location = new System.Drawing.Point(109, 45);
            this.checkBoxWiener.Name = "checkBoxWiener";
            this.checkBoxWiener.Size = new System.Drawing.Size(62, 19);
            this.checkBoxWiener.TabIndex = 71;
            this.checkBoxWiener.Text = "Wiener";
            this.checkBoxWiener.UseVisualStyleBackColor = true;
            this.checkBoxWiener.CheckedChanged += new System.EventHandler(this.checkBoxWiener_CheckedChanged);
            // 
            // checkBoxGS
            // 
            this.checkBoxGS.AutoSize = true;
            this.checkBoxGS.Checked = true;
            this.checkBoxGS.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxGS.Enabled = false;
            this.checkBoxGS.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.checkBoxGS.Location = new System.Drawing.Point(28, 45);
            this.checkBoxGS.Name = "checkBoxGS";
            this.checkBoxGS.Size = new System.Drawing.Size(75, 19);
            this.checkBoxGS.TabIndex = 70;
            this.checkBoxGS.Text = "Gaussian";
            this.checkBoxGS.UseVisualStyleBackColor = true;
            this.checkBoxGS.CheckedChanged += new System.EventHandler(this.checkBoxGS_CheckedChanged);
            // 
            // checkBoxGammaCorrect
            // 
            this.checkBoxGammaCorrect.AutoSize = true;
            this.checkBoxGammaCorrect.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.checkBoxGammaCorrect.Location = new System.Drawing.Point(6, 115);
            this.checkBoxGammaCorrect.Name = "checkBoxGammaCorrect";
            this.checkBoxGammaCorrect.Size = new System.Drawing.Size(176, 19);
            this.checkBoxGammaCorrect.TabIndex = 69;
            this.checkBoxGammaCorrect.Text = "Gamma Correction Enabled";
            this.checkBoxGammaCorrect.UseVisualStyleBackColor = true;
            this.checkBoxGammaCorrect.CheckedChanged += new System.EventHandler(this.checkBoxGammaCorrect_CheckedChanged);
            // 
            // checkBoxSmooth
            // 
            this.checkBoxSmooth.AutoSize = true;
            this.checkBoxSmooth.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.checkBoxSmooth.Location = new System.Drawing.Point(6, 20);
            this.checkBoxSmooth.Name = "checkBoxSmooth";
            this.checkBoxSmooth.Size = new System.Drawing.Size(135, 19);
            this.checkBoxSmooth.TabIndex = 68;
            this.checkBoxSmooth.Text = "Smoothing  Enabled";
            this.checkBoxSmooth.UseVisualStyleBackColor = true;
            this.checkBoxSmooth.CheckedChanged += new System.EventHandler(this.checkBoxSmooth_CheckedChanged);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.checkBoxCHAll);
            this.groupBox2.Controls.Add(this.checkBoxCH1);
            this.groupBox2.Controls.Add(this.checkBoxCH3);
            this.groupBox2.Controls.Add(this.checkBoxCH2);
            this.groupBox2.Controls.Add(this.checkBoxCHNot);
            this.groupBox2.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox2.Location = new System.Drawing.Point(3, 203);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(211, 67);
            this.groupBox2.TabIndex = 58;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Channels";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.buttonViewBgr);
            this.groupBox1.Controls.Add(this.buttonViewLab);
            this.groupBox1.Controls.Add(this.buttonViewHSV);
            this.groupBox1.Controls.Add(this.buttonSobelMultiGrad);
            this.groupBox1.Controls.Add(this.buttonViewHSV2);
            this.groupBox1.Controls.Add(this.buttonMultiGrad);
            this.groupBox1.Controls.Add(this.buttonViewXYZ);
            this.groupBox1.Controls.Add(this.buttonViewPill);
            this.groupBox1.Controls.Add(this.buttonViewGrayscale);
            this.groupBox1.Controls.Add(this.buttonViewGradient);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.Location = new System.Drawing.Point(3, 31);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(211, 172);
            this.groupBox1.TabIndex = 57;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Available color spaces";
            // 
            // buttonViewBgr
            // 
            this.buttonViewBgr.BackColor = System.Drawing.Color.LightSteelBlue;
            this.buttonViewBgr.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonViewBgr.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonViewBgr.Location = new System.Drawing.Point(28, 20);
            this.buttonViewBgr.Name = "buttonViewBgr";
            this.buttonViewBgr.Size = new System.Drawing.Size(75, 23);
            this.buttonViewBgr.TabIndex = 61;
            this.buttonViewBgr.Text = "BGR";
            this.buttonViewBgr.UseVisualStyleBackColor = false;
            this.buttonViewBgr.Click += new System.EventHandler(this.buttonViewBgr_Click);
            // 
            // buttonViewLab
            // 
            this.buttonViewLab.BackColor = System.Drawing.Color.LightSteelBlue;
            this.buttonViewLab.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonViewLab.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonViewLab.Location = new System.Drawing.Point(109, 20);
            this.buttonViewLab.Name = "buttonViewLab";
            this.buttonViewLab.Size = new System.Drawing.Size(75, 23);
            this.buttonViewLab.TabIndex = 57;
            this.buttonViewLab.Text = "Lab";
            this.buttonViewLab.UseVisualStyleBackColor = false;
            this.buttonViewLab.Click += new System.EventHandler(this.buttonViewLab_Click);
            // 
            // buttonViewHSV
            // 
            this.buttonViewHSV.BackColor = System.Drawing.Color.LightSteelBlue;
            this.buttonViewHSV.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonViewHSV.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonViewHSV.Location = new System.Drawing.Point(28, 49);
            this.buttonViewHSV.Name = "buttonViewHSV";
            this.buttonViewHSV.Size = new System.Drawing.Size(75, 23);
            this.buttonViewHSV.TabIndex = 58;
            this.buttonViewHSV.Text = "HSV";
            this.buttonViewHSV.UseVisualStyleBackColor = false;
            this.buttonViewHSV.Click += new System.EventHandler(this.buttonViewHSV_Click);
            // 
            // buttonSobelMultiGrad
            // 
            this.buttonSobelMultiGrad.BackColor = System.Drawing.Color.LightSteelBlue;
            this.buttonSobelMultiGrad.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonSobelMultiGrad.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonSobelMultiGrad.Location = new System.Drawing.Point(109, 136);
            this.buttonSobelMultiGrad.Name = "buttonSobelMultiGrad";
            this.buttonSobelMultiGrad.Size = new System.Drawing.Size(75, 23);
            this.buttonSobelMultiGrad.TabIndex = 66;
            this.buttonSobelMultiGrad.Text = "SobelMultiGrad";
            this.buttonSobelMultiGrad.UseVisualStyleBackColor = false;
            this.buttonSobelMultiGrad.Click += new System.EventHandler(this.buttonSobelMultiGrad_Click);
            // 
            // buttonViewHSV2
            // 
            this.buttonViewHSV2.BackColor = System.Drawing.Color.LightSteelBlue;
            this.buttonViewHSV2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonViewHSV2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonViewHSV2.Location = new System.Drawing.Point(109, 49);
            this.buttonViewHSV2.Name = "buttonViewHSV2";
            this.buttonViewHSV2.Size = new System.Drawing.Size(75, 23);
            this.buttonViewHSV2.TabIndex = 60;
            this.buttonViewHSV2.Text = "H\'SV";
            this.buttonViewHSV2.UseVisualStyleBackColor = false;
            this.buttonViewHSV2.Click += new System.EventHandler(this.buttonViewHSV2_Click);
            // 
            // buttonMultiGrad
            // 
            this.buttonMultiGrad.BackColor = System.Drawing.Color.LightSteelBlue;
            this.buttonMultiGrad.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonMultiGrad.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonMultiGrad.Location = new System.Drawing.Point(28, 136);
            this.buttonMultiGrad.Name = "buttonMultiGrad";
            this.buttonMultiGrad.Size = new System.Drawing.Size(75, 23);
            this.buttonMultiGrad.TabIndex = 65;
            this.buttonMultiGrad.Text = "MultiGrad";
            this.buttonMultiGrad.UseVisualStyleBackColor = false;
            this.buttonMultiGrad.Click += new System.EventHandler(this.buttonMultiGrad_Click);
            // 
            // buttonViewXYZ
            // 
            this.buttonViewXYZ.BackColor = System.Drawing.Color.LightSteelBlue;
            this.buttonViewXYZ.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonViewXYZ.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonViewXYZ.Location = new System.Drawing.Point(28, 78);
            this.buttonViewXYZ.Name = "buttonViewXYZ";
            this.buttonViewXYZ.Size = new System.Drawing.Size(75, 23);
            this.buttonViewXYZ.TabIndex = 63;
            this.buttonViewXYZ.Text = "XYZ";
            this.buttonViewXYZ.UseVisualStyleBackColor = false;
            this.buttonViewXYZ.Click += new System.EventHandler(this.buttonViewXYZ_Click);
            // 
            // buttonViewPill
            // 
            this.buttonViewPill.BackColor = System.Drawing.Color.LightSteelBlue;
            this.buttonViewPill.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonViewPill.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonViewPill.Location = new System.Drawing.Point(109, 78);
            this.buttonViewPill.Name = "buttonViewPill";
            this.buttonViewPill.Size = new System.Drawing.Size(75, 23);
            this.buttonViewPill.TabIndex = 64;
            this.buttonViewPill.Text = "P - ill";
            this.buttonViewPill.UseVisualStyleBackColor = false;
            this.buttonViewPill.Click += new System.EventHandler(this.buttonViewPill_Click);
            // 
            // buttonViewGrayscale
            // 
            this.buttonViewGrayscale.BackColor = System.Drawing.Color.LightSteelBlue;
            this.buttonViewGrayscale.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonViewGrayscale.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonViewGrayscale.Location = new System.Drawing.Point(28, 107);
            this.buttonViewGrayscale.Name = "buttonViewGrayscale";
            this.buttonViewGrayscale.Size = new System.Drawing.Size(75, 23);
            this.buttonViewGrayscale.TabIndex = 62;
            this.buttonViewGrayscale.Text = "Grayscale";
            this.buttonViewGrayscale.UseVisualStyleBackColor = false;
            this.buttonViewGrayscale.Click += new System.EventHandler(this.buttonViewGrayscale_Click);
            // 
            // buttonViewGradient
            // 
            this.buttonViewGradient.BackColor = System.Drawing.Color.LightSteelBlue;
            this.buttonViewGradient.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonViewGradient.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonViewGradient.Location = new System.Drawing.Point(109, 107);
            this.buttonViewGradient.Name = "buttonViewGradient";
            this.buttonViewGradient.Size = new System.Drawing.Size(75, 23);
            this.buttonViewGradient.TabIndex = 59;
            this.buttonViewGradient.Text = "Gradient";
            this.buttonViewGradient.UseVisualStyleBackColor = false;
            this.buttonViewGradient.Click += new System.EventHandler(this.buttonViewGradient_Click);
            // 
            // tabPageExtraction
            // 
            this.tabPageExtraction.AutoScroll = true;
            this.tabPageExtraction.BackColor = System.Drawing.Color.GhostWhite;
            this.tabPageExtraction.Controls.Add(this.groupBox6);
            this.tabPageExtraction.Controls.Add(this.groupBox5);
            this.tabPageExtraction.Controls.Add(this.groupBox4);
            this.tabPageExtraction.Controls.Add(this.label7);
            this.tabPageExtraction.Location = new System.Drawing.Point(4, 25);
            this.tabPageExtraction.Name = "tabPageExtraction";
            this.tabPageExtraction.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageExtraction.Size = new System.Drawing.Size(217, 530);
            this.tabPageExtraction.TabIndex = 1;
            this.tabPageExtraction.Text = "Extraction";
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.radioButtonNormal);
            this.groupBox6.Controls.Add(this.radioButtonStrictNMS);
            this.groupBox6.Controls.Add(this.checkBoxFeedbackGrouping);
            this.groupBox6.Controls.Add(this.checkBoxWordPruning);
            this.groupBox6.Controls.Add(this.checkBoxGrouping);
            this.groupBox6.Controls.Add(this.checkBoxClassifiers);
            this.groupBox6.Controls.Add(this.checkBoxNMS);
            this.groupBox6.Controls.Add(this.checkBox2ndStage);
            this.groupBox6.Controls.Add(this.checkBox2ndClassifier);
            this.groupBox6.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox6.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.groupBox6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox6.Location = new System.Drawing.Point(3, 350);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(211, 174);
            this.groupBox6.TabIndex = 34;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "More extraction options";
            // 
            // radioButtonNormal
            // 
            this.radioButtonNormal.AutoSize = true;
            this.radioButtonNormal.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.radioButtonNormal.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radioButtonNormal.Location = new System.Drawing.Point(136, 83);
            this.radioButtonNormal.Name = "radioButtonNormal";
            this.radioButtonNormal.Size = new System.Drawing.Size(67, 19);
            this.radioButtonNormal.TabIndex = 53;
            this.radioButtonNormal.Text = "Original";
            this.toolTipMainForm.SetToolTip(this.radioButtonNormal, "Only for ERExtractor");
            this.radioButtonNormal.UseVisualStyleBackColor = true;
            // 
            // radioButtonStrictNMS
            // 
            this.radioButtonStrictNMS.AutoSize = true;
            this.radioButtonStrictNMS.Checked = true;
            this.radioButtonStrictNMS.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.radioButtonStrictNMS.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radioButtonStrictNMS.Location = new System.Drawing.Point(136, 63);
            this.radioButtonStrictNMS.Name = "radioButtonStrictNMS";
            this.radioButtonStrictNMS.Size = new System.Drawing.Size(51, 19);
            this.radioButtonStrictNMS.TabIndex = 52;
            this.radioButtonStrictNMS.TabStop = true;
            this.radioButtonStrictNMS.Text = "Strict";
            this.toolTipMainForm.SetToolTip(this.radioButtonStrictNMS, "Only for ERExtractor");
            this.radioButtonStrictNMS.UseVisualStyleBackColor = true;
            // 
            // checkBoxFeedbackGrouping
            // 
            this.checkBoxFeedbackGrouping.AutoSize = true;
            this.checkBoxFeedbackGrouping.Checked = true;
            this.checkBoxFeedbackGrouping.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxFeedbackGrouping.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.checkBoxFeedbackGrouping.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBoxFeedbackGrouping.Location = new System.Drawing.Point(6, 88);
            this.checkBoxFeedbackGrouping.Name = "checkBoxFeedbackGrouping";
            this.checkBoxFeedbackGrouping.Size = new System.Drawing.Size(115, 17);
            this.checkBoxFeedbackGrouping.TabIndex = 51;
            this.checkBoxFeedbackGrouping.Text = "Feedback grouping";
            this.toolTipMainForm.SetToolTip(this.checkBoxFeedbackGrouping, "Works for ERExtractor and Translated Exhaustive Search");
            this.checkBoxFeedbackGrouping.UseVisualStyleBackColor = true;
            // 
            // checkBoxWordPruning
            // 
            this.checkBoxWordPruning.AutoSize = true;
            this.checkBoxWordPruning.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.checkBoxWordPruning.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBoxWordPruning.Location = new System.Drawing.Point(6, 111);
            this.checkBoxWordPruning.Name = "checkBoxWordPruning";
            this.checkBoxWordPruning.Size = new System.Drawing.Size(87, 17);
            this.checkBoxWordPruning.TabIndex = 50;
            this.checkBoxWordPruning.Text = "Word pruning";
            this.toolTipMainForm.SetToolTip(this.checkBoxWordPruning, "Only for ERExtractor");
            this.checkBoxWordPruning.UseVisualStyleBackColor = true;
            // 
            // checkBoxGrouping
            // 
            this.checkBoxGrouping.AutoSize = true;
            this.checkBoxGrouping.Checked = true;
            this.checkBoxGrouping.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxGrouping.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.checkBoxGrouping.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBoxGrouping.Location = new System.Drawing.Point(6, 65);
            this.checkBoxGrouping.Name = "checkBoxGrouping";
            this.checkBoxGrouping.Size = new System.Drawing.Size(75, 17);
            this.checkBoxGrouping.TabIndex = 49;
            this.checkBoxGrouping.Text = "Group ERs";
            this.checkBoxGrouping.ThreeState = true;
            this.checkBoxGrouping.UseVisualStyleBackColor = true;
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.textBoxThreshD);
            this.groupBox5.Controls.Add(this.textBoxMinAr);
            this.groupBox5.Controls.Add(this.textBoxMaxAr);
            this.groupBox5.Controls.Add(this.textBoxMinProb);
            this.groupBox5.Controls.Add(this.textBoxMinProbDiff);
            this.groupBox5.Controls.Add(this.label3);
            this.groupBox5.Controls.Add(this.label1);
            this.groupBox5.Controls.Add(this.label4);
            this.groupBox5.Controls.Add(this.label2);
            this.groupBox5.Controls.Add(this.label5);
            this.groupBox5.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox5.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.groupBox5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox5.Location = new System.Drawing.Point(3, 186);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(211, 164);
            this.groupBox5.TabIndex = 33;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "ER Parameters";
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.buttonChannels);
            this.groupBox4.Controls.Add(this.buttonMSER);
            this.groupBox4.Controls.Add(this.buttonERNative);
            this.groupBox4.Controls.Add(this.buttonERExtractor);
            this.groupBox4.Controls.Add(this.TestMyNM);
            this.groupBox4.Controls.Add(this.buttonCER);
            this.groupBox4.Controls.Add(this.buttonHybrid1);
            this.groupBox4.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox4.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.groupBox4.Location = new System.Drawing.Point(3, 31);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(211, 155);
            this.groupBox4.TabIndex = 32;
            this.groupBox4.TabStop = false;
            // 
            // buttonChannels
            // 
            this.buttonChannels.BackColor = System.Drawing.Color.LightSteelBlue;
            this.buttonChannels.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonChannels.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonChannels.Location = new System.Drawing.Point(77, 105);
            this.buttonChannels.Name = "buttonChannels";
            this.buttonChannels.Size = new System.Drawing.Size(75, 44);
            this.buttonChannels.TabIndex = 58;
            this.buttonChannels.Text = "Set Channels";
            this.buttonChannels.UseVisualStyleBackColor = false;
            this.buttonChannels.Click += new System.EventHandler(this.buttonChannels_Click);
            // 
            // tabMore
            // 
            this.tabMore.AutoScroll = true;
            this.tabMore.BackColor = System.Drawing.Color.GhostWhite;
            this.tabMore.Controls.Add(this.groupBox8);
            this.tabMore.Controls.Add(this.panelERData);
            this.tabMore.Controls.Add(this.groupBox7);
            this.tabMore.Controls.Add(this.label12);
            this.tabMore.Location = new System.Drawing.Point(4, 25);
            this.tabMore.Name = "tabMore";
            this.tabMore.Padding = new System.Windows.Forms.Padding(3);
            this.tabMore.Size = new System.Drawing.Size(217, 530);
            this.tabMore.TabIndex = 2;
            this.tabMore.Text = "More ...";
            // 
            // groupBox8
            // 
            this.groupBox8.Controls.Add(this.buttonCanny);
            this.groupBox8.Controls.Add(this.label8);
            this.groupBox8.Controls.Add(this.textBoxThresh1);
            this.groupBox8.Controls.Add(this.textBoxThresh2);
            this.groupBox8.Controls.Add(this.checkBoxCannyGrouping);
            this.groupBox8.Controls.Add(this.checkBoxNMCanny);
            this.groupBox8.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox8.Location = new System.Drawing.Point(3, 388);
            this.groupBox8.Name = "groupBox8";
            this.groupBox8.Size = new System.Drawing.Size(211, 137);
            this.groupBox8.TabIndex = 38;
            this.groupBox8.TabStop = false;
            this.groupBox8.Text = "Canny tests";
            this.groupBox8.Visible = false;
            // 
            // groupBox7
            // 
            this.groupBox7.Controls.Add(this.buttonThinningDemo);
            this.groupBox7.Controls.Add(this.button1);
            this.groupBox7.Controls.Add(this.buttonLayers);
            this.groupBox7.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox7.Location = new System.Drawing.Point(3, 31);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Size = new System.Drawing.Size(211, 100);
            this.groupBox7.TabIndex = 37;
            this.groupBox7.TabStop = false;
            // 
            // buttonThinningDemo
            // 
            this.buttonThinningDemo.BackColor = System.Drawing.Color.LightSteelBlue;
            this.buttonThinningDemo.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonThinningDemo.Location = new System.Drawing.Point(10, 58);
            this.buttonThinningDemo.Name = "buttonThinningDemo";
            this.buttonThinningDemo.Size = new System.Drawing.Size(94, 23);
            this.buttonThinningDemo.TabIndex = 31;
            this.buttonThinningDemo.Text = "Thinning Demo";
            this.buttonThinningDemo.UseVisualStyleBackColor = false;
            this.buttonThinningDemo.Click += new System.EventHandler(this.buttonThinningDemo_Click);
            // 
            // toolTipMainForm
            // 
            this.toolTipMainForm.IsBalloon = true;
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.NMWorkerProgress,
            this.labelStatus});
            this.statusStrip1.Location = new System.Drawing.Point(0, 601);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(1072, 22);
            this.statusStrip1.TabIndex = 61;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // NMWorkerProgress
            // 
            this.NMWorkerProgress.Name = "NMWorkerProgress";
            this.NMWorkerProgress.Size = new System.Drawing.Size(0, 17);
            // 
            // labelStatus
            // 
            this.labelStatus.Name = "labelStatus";
            this.labelStatus.Size = new System.Drawing.Size(39, 17);
            this.labelStatus.Text = "Status";
            // 
            // groupBox9
            // 
            this.groupBox9.Controls.Add(this.buttonRevert);
            this.groupBox9.Controls.Add(this.buttonSaveViewedImage);
            this.groupBox9.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox9.Location = new System.Drawing.Point(3, 435);
            this.groupBox9.Name = "groupBox9";
            this.groupBox9.Size = new System.Drawing.Size(211, 40);
            this.groupBox9.TabIndex = 60;
            this.groupBox9.TabStop = false;
            // 
            // groupBox10
            // 
            this.groupBox10.Controls.Add(this.ZoomOut);
            this.groupBox10.Controls.Add(this.ZoomIn);
            this.groupBox10.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox10.Location = new System.Drawing.Point(3, 475);
            this.groupBox10.Name = "groupBox10";
            this.groupBox10.Size = new System.Drawing.Size(211, 49);
            this.groupBox10.TabIndex = 61;
            this.groupBox10.TabStop = false;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.GhostWhite;
            this.ClientSize = new System.Drawing.Size(1072, 623);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.tabControlMainForm);
            this.Controls.Add(this.panelPicBox);
            this.Controls.Add(this.menuBar);
            this.MainMenuStrip = this.menuBar;
            this.Name = "MainForm";
            this.Text = "WordLocRec";
            this.menuBar.ResumeLayout(false);
            this.menuBar.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxMain)).EndInit();
            this.panelPicBox.ResumeLayout(false);
            this.panelPicBox.PerformLayout();
            this.panelERData.ResumeLayout(false);
            this.panelERData.PerformLayout();
            this.tabControlMainForm.ResumeLayout(false);
            this.tabPageViewOptions.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownGamma)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.tabPageExtraction.ResumeLayout(false);
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.tabMore.ResumeLayout(false);
            this.groupBox8.ResumeLayout(false);
            this.groupBox8.PerformLayout();
            this.groupBox7.ResumeLayout(false);
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.groupBox9.ResumeLayout(false);
            this.groupBox10.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuBar;
        private System.Windows.Forms.ToolStripMenuItem testToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem loadImagesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem loadGroundTruthFilesToolStripMenuItem;
        private System.Windows.Forms.PictureBox pictureBoxMain;
        private System.Windows.Forms.Panel panelPicBox;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button buttonMSER;
        private System.Windows.Forms.Button buttonERNative;
        private System.Windows.Forms.Button TestMyNM;
        private System.Windows.Forms.TextBox textBoxThreshD;
        private System.Windows.Forms.TextBox textBoxMinAr;
        private System.Windows.Forms.TextBox textBoxMaxAr;
        private System.Windows.Forms.TextBox textBoxMinProb;
        private System.Windows.Forms.TextBox textBoxMinProbDiff;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.CheckBox checkBoxClassifiers;
        private System.Windows.Forms.Button buttonCanny;
        private System.Windows.Forms.TextBox textBoxThresh1;
        private System.Windows.Forms.TextBox textBoxThresh2;
        private System.Windows.Forms.CheckBox checkBoxNMCanny;
        private System.Windows.Forms.CheckBox checkBoxCannyGrouping;
        private System.Windows.Forms.Button buttonHybrid1;
        private System.Windows.Forms.Button buttonLayers;
        private System.Windows.Forms.ToolStripMenuItem optionsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem rectanglesFromChannelsToolStripMenuItem;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.CheckBox checkBoxCHAll;
        private System.Windows.Forms.CheckBox checkBoxCH3;
        private System.Windows.Forms.CheckBox checkBoxCH2;
        private System.Windows.Forms.CheckBox checkBoxCH1;
        private System.Windows.Forms.CheckBox checkBoxCHNot;
        private System.Windows.Forms.Button buttonRevert;
        private System.Windows.Forms.CheckBox checkBox2ndStage;
        private System.Windows.Forms.CheckBox checkBoxNMS;
        private System.Windows.Forms.CheckBox checkBox2ndClassifier;
        private System.Windows.Forms.Button ZoomIn;
        private System.Windows.Forms.Button ZoomOut;
        private System.Windows.Forms.Panel panelERData;
        private System.Windows.Forms.TextBox textBoxERData;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Button buttonCER;
        private System.Windows.Forms.Button buttonSaveViewedImage;
        private System.Windows.Forms.Button buttonERExtractor;
        private System.Windows.Forms.ToolStripMenuItem chooseFeatureTypeToolStripMenuItem;
        private System.Windows.Forms.TabControl tabControlMainForm;
        private System.Windows.Forms.TabPage tabPageViewOptions;
        private System.Windows.Forms.TabPage tabPageExtraction;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button buttonViewBgr;
        private System.Windows.Forms.Button buttonViewLab;
        private System.Windows.Forms.Button buttonViewHSV;
        private System.Windows.Forms.Button buttonSobelMultiGrad;
        private System.Windows.Forms.Button buttonViewHSV2;
        private System.Windows.Forms.Button buttonMultiGrad;
        private System.Windows.Forms.Button buttonViewXYZ;
        private System.Windows.Forms.Button buttonViewPill;
        private System.Windows.Forms.Button buttonViewGrayscale;
        private System.Windows.Forms.Button buttonViewGradient;
        private System.Windows.Forms.Label labelSeparator;
        private System.Windows.Forms.NumericUpDown numericUpDownGamma;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.NumericUpDown numericUpDown2;
        private System.Windows.Forms.NumericUpDown numericUpDown1;
        private System.Windows.Forms.CheckBox checkBoxWiener;
        private System.Windows.Forms.CheckBox checkBoxGS;
        private System.Windows.Forms.CheckBox checkBoxGammaCorrect;
        private System.Windows.Forms.CheckBox checkBoxSmooth;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.TabPage tabMore;
        private System.Windows.Forms.GroupBox groupBox8;
        private System.Windows.Forms.GroupBox groupBox7;
        private System.Windows.Forms.ToolTip toolTipMainForm;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel NMWorkerProgress;
        private System.Windows.Forms.ToolStripStatusLabel labelStatus;
        private System.Windows.Forms.Button buttonChannels;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem aboutToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem moreToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem calculateDatasetStatisticsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem createModelsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem multipleImagesToolStripMenuItem;
        private System.Windows.Forms.CheckBox checkBoxGrouping;
        private System.Windows.Forms.CheckBox checkBoxWordPruning;
        private System.Windows.Forms.CheckBox checkBoxFeedbackGrouping;
        private System.Windows.Forms.ToolStripMenuItem createDatasetsToolStripMenuItem;
        private System.Windows.Forms.Button buttonThinningDemo;
        private System.Windows.Forms.RadioButton radioButtonNormal;
        private System.Windows.Forms.RadioButton radioButtonStrictNMS;
        private System.Windows.Forms.GroupBox groupBox10;
        private System.Windows.Forms.GroupBox groupBox9;
    }
}

