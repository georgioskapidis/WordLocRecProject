﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WordLocRec.DataStructs;

namespace WordLocRec.Forms
{
    public partial class ChannelSetFormComplex : Form
    {
        public List<ColorChannel> channels;
        public Constants.GroupingMethod grouping = Constants.GroupingMethod.ExhaustiveSearch;
        public ChannelSetFormComplex(List<ColorChannel> oldChannels, Constants.GroupingMethod oldGrouping)
        {
            InitializeComponent();
            grouping = oldGrouping;
            if (grouping.HasFlag(Constants.GroupingMethod.ExhaustiveSearch)) radioButtonExhastiveSearch.Checked = true;
            else if (grouping.HasFlag(Constants.GroupingMethod.Karatzas)) radioButtonGKGrouping.Checked = true;
            else radioButtonChineseGrouping.Checked = true;
            channels = new List<ColorChannel>(oldChannels);
            foreach (ColorChannel cc in channels)
                listBoxSelected.Items.Add(cc.ToString());
        }

        private void buttonExit_Click(object sender, EventArgs e)
        {
            if (channels.Count > 0)
            {
                if (radioButtonExhastiveSearch.Checked)
                    grouping = Constants.GroupingMethod.ExhaustiveSearch;
                else if (radioButtonGKGrouping.Checked)
                    grouping = Constants.GroupingMethod.Karatzas;
                else
                    grouping = Constants.GroupingMethod.chinese;
                this.DialogResult = System.Windows.Forms.DialogResult.OK;
            }
            this.Close();
        }

        private void listBoxOptions_DoubleClick(object sender, EventArgs e)
        {
            ColorChannel cc;
            switch (listBoxOptions.SelectedIndex)
            {
                case 0:
                    {
                        cc = new ColorChannel(ColorChannel.AvailableColorSpaces.HSV, ColorChannel.Channel.Channel1, true, true, 1);
                        cc = ShowProjectionDialog(cc);
                        break;
                    }
                case 1:
                    {
                        cc = new ColorChannel(ColorChannel.AvailableColorSpaces.HSV, ColorChannel.Channel.Channel2, true, true, 1);
                        cc = ShowProjectionDialog(cc);
                        break;
                    }
                case 2:
                    {
                        cc = new ColorChannel(ColorChannel.AvailableColorSpaces.HSV, ColorChannel.Channel.Channel3, true, true, 1);
                        cc = ShowProjectionDialog(cc);
                        break;
                    }
                case 3:
                    {
                        cc = new ColorChannel(ColorChannel.AvailableColorSpaces.BGR, ColorChannel.Channel.Channel1, true, true, 1);
                        cc = ShowProjectionDialog(cc);
                        break;
                    }
                case 4:
                    {
                        cc = new ColorChannel(ColorChannel.AvailableColorSpaces.BGR, ColorChannel.Channel.Channel2, true, true, 1);
                        cc = ShowProjectionDialog(cc);
                        break;
                    }
                case 5:
                    {
                        cc = new ColorChannel(ColorChannel.AvailableColorSpaces.BGR, ColorChannel.Channel.Channel3, true, true, 1);
                        cc = ShowProjectionDialog(cc);
                        break;
                    }
                case 6:
                    {
                        cc = new ColorChannel(ColorChannel.AvailableColorSpaces.LAB, ColorChannel.Channel.Channel1, true, true, 1);
                        cc = ShowProjectionDialog(cc);
                        break;
                    }
                case 7:
                    {
                        cc = new ColorChannel(ColorChannel.AvailableColorSpaces.LAB, ColorChannel.Channel.Channel2, true, true, 1);
                        cc = ShowProjectionDialog(cc);
                        break;
                    }
                case 8:
                    {
                        cc = new ColorChannel(ColorChannel.AvailableColorSpaces.LAB, ColorChannel.Channel.Channel3, true, true, 1);
                        cc = ShowProjectionDialog(cc);
                        break;
                    }
                case 9:
                    {
                        cc = new ColorChannel(ColorChannel.AvailableColorSpaces.XYZ, ColorChannel.Channel.Channel1, true, true, 1);
                        cc = ShowProjectionDialog(cc);
                        break;
                    }
                case 10:
                    {
                        cc = new ColorChannel(ColorChannel.AvailableColorSpaces.XYZ, ColorChannel.Channel.Channel2, true, true, 1);
                        cc = ShowProjectionDialog(cc);
                        break;
                    }
                case 11:
                    {
                        cc = new ColorChannel(ColorChannel.AvailableColorSpaces.XYZ, ColorChannel.Channel.Channel3, true, true, 1);
                        cc = ShowProjectionDialog(cc);
                        break;
                    }
                case 12:
                    {
                        cc = new ColorChannel(ColorChannel.AvailableColorSpaces.Pii, ColorChannel.Channel.Channel1, true, true, 1);
                        cc = ShowProjectionDialog(cc);
                        break;
                    }
                case 13:
                    {
                        cc = new ColorChannel(ColorChannel.AvailableColorSpaces.Pii, ColorChannel.Channel.Channel2, true, true, 1);
                        cc = ShowProjectionDialog(cc);
                        break;
                    }
                case 14:
                    {
                        cc = new ColorChannel(ColorChannel.AvailableColorSpaces.Pii, ColorChannel.Channel.Channel3, true, true, 1);
                        cc = ShowProjectionDialog(cc);
                        break;
                    }
                case 15:
                    {
                        cc = new ColorChannel(ColorChannel.AvailableColorSpaces.Grayscale, ColorChannel.Channel.Channel1, true, true, 1);
                        cc = ShowProjectionDialog(cc);
                        break;
                    }
                case 16:
                    {
                        cc = new ColorChannel(ColorChannel.AvailableColorSpaces.Distances, ColorChannel.Channel.Channel1, true, true, 1);
                        cc = ShowProjectionDialog(cc);
                        break;
                    }
                case 17:
                    {
                        cc = new ColorChannel(ColorChannel.AvailableColorSpaces.SingleGrad, ColorChannel.Channel.Channel1, true, true, 1);
                        cc = ShowProjectionDialog(cc);
                        break;
                    }
                case 18:
                    {
                        cc = new ColorChannel(ColorChannel.AvailableColorSpaces.MultiGrad, ColorChannel.Channel.Channel1, true, true, 1);
                        cc = ShowProjectionDialog(cc);
                        break;
                    }
                case 19:
                    {
                        cc = new ColorChannel(ColorChannel.AvailableColorSpaces.MultiSobelGrad, ColorChannel.Channel.Channel1, true, true, 1);
                        cc = ShowProjectionDialog(cc);
                        break;
                    }
            }
        }

        private ColorChannel ShowProjectionDialog(ColorChannel cc)
        {
            using (ProjectionForm pf = new ProjectionForm(cc))
                if (pf.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    cc = pf.colorChannel;
                }
            if (cc.ValidCombination)
            {
                channels.Add(cc);
                listBoxSelected.Items.Add(cc.ToString());
            }
            return cc;
        }
        private void listBoxSelected_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode.Equals(Keys.Delete))
            {
                if (listBoxSelected.SelectedIndices.Count > 0)
                {
                    channels.RemoveAt(listBoxSelected .SelectedIndices[0]);
                    listBoxSelected.Items.RemoveAt(listBoxSelected.SelectedIndices[0]);
                    listBoxSelected.SelectedIndices.Clear();
                }
            }
        }
    }
}
