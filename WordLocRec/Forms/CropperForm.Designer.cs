﻿namespace WordLocRec.Forms
{
    partial class CropperForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.buttonCrop = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.radioButtonWords = new System.Windows.Forms.RadioButton();
            this.radioButtonChar = new System.Windows.Forms.RadioButton();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.radioButtonInverse = new System.Windows.Forms.RadioButton();
            this.radioButtonForward = new System.Windows.Forms.RadioButton();
            this.checkBoxOnlyGT = new System.Windows.Forms.CheckBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.textBoxDest = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.textBoxGT = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.textBoxImages = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // buttonCrop
            // 
            this.buttonCrop.BackColor = System.Drawing.Color.LightSteelBlue;
            this.buttonCrop.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonCrop.Location = new System.Drawing.Point(583, 194);
            this.buttonCrop.Name = "buttonCrop";
            this.buttonCrop.Size = new System.Drawing.Size(103, 43);
            this.buttonCrop.TabIndex = 0;
            this.buttonCrop.Text = "Create dataset";
            this.buttonCrop.UseVisualStyleBackColor = false;
            this.buttonCrop.Click += new System.EventHandler(this.buttonCrop_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.radioButtonWords);
            this.groupBox1.Controls.Add(this.radioButtonChar);
            this.groupBox1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(182, 70);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Choose Extraction Mode";
            // 
            // radioButtonWords
            // 
            this.radioButtonWords.AutoSize = true;
            this.radioButtonWords.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.radioButtonWords.Location = new System.Drawing.Point(7, 43);
            this.radioButtonWords.Name = "radioButtonWords";
            this.radioButtonWords.Size = new System.Drawing.Size(55, 17);
            this.radioButtonWords.TabIndex = 1;
            this.radioButtonWords.TabStop = true;
            this.radioButtonWords.Text = "Words";
            this.radioButtonWords.UseVisualStyleBackColor = true;
            this.radioButtonWords.CheckedChanged += new System.EventHandler(this.radioButtonWords_CheckedChanged);
            // 
            // radioButtonChar
            // 
            this.radioButtonChar.AutoSize = true;
            this.radioButtonChar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.radioButtonChar.Location = new System.Drawing.Point(7, 20);
            this.radioButtonChar.Name = "radioButtonChar";
            this.radioButtonChar.Size = new System.Drawing.Size(75, 17);
            this.radioButtonChar.TabIndex = 0;
            this.radioButtonChar.TabStop = true;
            this.radioButtonChar.Text = "Characters";
            this.radioButtonChar.UseVisualStyleBackColor = true;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.radioButtonInverse);
            this.groupBox2.Controls.Add(this.radioButtonForward);
            this.groupBox2.Controls.Add(this.checkBoxOnlyGT);
            this.groupBox2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.groupBox2.Location = new System.Drawing.Point(12, 88);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(182, 100);
            this.groupBox2.TabIndex = 2;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Options";
            // 
            // radioButtonInverse
            // 
            this.radioButtonInverse.AutoSize = true;
            this.radioButtonInverse.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.radioButtonInverse.Location = new System.Drawing.Point(7, 66);
            this.radioButtonInverse.Name = "radioButtonInverse";
            this.radioButtonInverse.Size = new System.Drawing.Size(100, 17);
            this.radioButtonInverse.TabIndex = 2;
            this.radioButtonInverse.Text = "Inverse channel";
            this.radioButtonInverse.UseVisualStyleBackColor = true;
            // 
            // radioButtonForward
            // 
            this.radioButtonForward.AutoSize = true;
            this.radioButtonForward.Checked = true;
            this.radioButtonForward.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.radioButtonForward.Location = new System.Drawing.Point(7, 43);
            this.radioButtonForward.Name = "radioButtonForward";
            this.radioButtonForward.Size = new System.Drawing.Size(103, 17);
            this.radioButtonForward.TabIndex = 1;
            this.radioButtonForward.TabStop = true;
            this.radioButtonForward.Text = "Forward channel";
            this.radioButtonForward.UseVisualStyleBackColor = true;
            // 
            // checkBoxOnlyGT
            // 
            this.checkBoxOnlyGT.AutoSize = true;
            this.checkBoxOnlyGT.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.checkBoxOnlyGT.Location = new System.Drawing.Point(7, 20);
            this.checkBoxOnlyGT.Name = "checkBoxOnlyGT";
            this.checkBoxOnlyGT.Size = new System.Drawing.Size(123, 17);
            this.checkBoxOnlyGT.TabIndex = 0;
            this.checkBoxOnlyGT.Text = "Ground Truth Images";
            this.checkBoxOnlyGT.UseVisualStyleBackColor = true;
            this.checkBoxOnlyGT.CheckedChanged += new System.EventHandler(this.checkBoxOnlyGT_CheckedChanged);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.textBoxDest);
            this.groupBox3.Controls.Add(this.label3);
            this.groupBox3.Controls.Add(this.textBoxGT);
            this.groupBox3.Controls.Add(this.label2);
            this.groupBox3.Controls.Add(this.textBoxImages);
            this.groupBox3.Controls.Add(this.label1);
            this.groupBox3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.groupBox3.Location = new System.Drawing.Point(201, 13);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(485, 175);
            this.groupBox3.TabIndex = 3;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Destinations";
            // 
            // textBoxDest
            // 
            this.textBoxDest.Location = new System.Drawing.Point(82, 142);
            this.textBoxDest.Name = "textBoxDest";
            this.textBoxDest.Size = new System.Drawing.Size(397, 20);
            this.textBoxDest.TabIndex = 5;
            this.textBoxDest.Text = "...";
            this.textBoxDest.Click += new System.EventHandler(this.textBoxDest_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label3.Location = new System.Drawing.Point(6, 145);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(63, 13);
            this.label3.TabIndex = 4;
            this.label3.Text = "Destination:";
            // 
            // textBoxGT
            // 
            this.textBoxGT.Location = new System.Drawing.Point(82, 49);
            this.textBoxGT.Name = "textBoxGT";
            this.textBoxGT.Size = new System.Drawing.Size(397, 20);
            this.textBoxGT.TabIndex = 3;
            this.textBoxGT.Text = "...";
            this.textBoxGT.Click += new System.EventHandler(this.textBoxGT_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label2.Location = new System.Drawing.Point(6, 52);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(69, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Ground truth:";
            // 
            // textBoxImages
            // 
            this.textBoxImages.Location = new System.Drawing.Point(82, 17);
            this.textBoxImages.Name = "textBoxImages";
            this.textBoxImages.Size = new System.Drawing.Size(397, 20);
            this.textBoxImages.TabIndex = 1;
            this.textBoxImages.Text = "...";
            this.textBoxImages.Click += new System.EventHandler(this.textBoxImages_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label1.Location = new System.Drawing.Point(6, 21);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(44, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Images:";
            // 
            // CropperForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.GhostWhite;
            this.ClientSize = new System.Drawing.Size(698, 249);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.buttonCrop);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.SizableToolWindow;
            this.Name = "CropperForm";
            this.Text = "Cropper";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button buttonCrop;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.RadioButton radioButtonChar;
        private System.Windows.Forms.RadioButton radioButtonWords;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.TextBox textBoxImages;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBoxGT;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.CheckBox checkBoxOnlyGT;
        private System.Windows.Forms.TextBox textBoxDest;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.RadioButton radioButtonInverse;
        private System.Windows.Forms.RadioButton radioButtonForward;
    }
}