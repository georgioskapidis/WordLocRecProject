﻿using Emgu.CV;
using Emgu.CV.CvEnum;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WordLocRec.Tools;

namespace WordLocRec.Forms
{
    public partial class CropperForm : Form
    {
        String[] imagePaths, gtPaths;
        String destFolder;
        public CropperForm()
        {
            InitializeComponent();
        }

        private void checkBoxOnlyGT_CheckedChanged(object sender, EventArgs e)
        {
            textBoxGT.Enabled = !checkBoxOnlyGT.Checked && radioButtonChar.Checked;
            radioButtonInverse.Checked = false;
        }
        private void textBoxImages_Click(object sender, EventArgs e)
        {
            textBoxImages.Text = null;
            imagePaths = FormUtils.ParametrizedOpenFileDialog(Constants.IMAGE_FILE_FILTER, true, "Select the images");
            if (imagePaths != null) textBoxImages.Text = Path.GetDirectoryName(imagePaths[0]);
        }

        private void textBoxGT_Click(object sender, EventArgs e)
        {
            textBoxGT.Text = null;
            gtPaths = FormUtils.ParametrizedOpenFileDialog(Constants.TEXT_FILE_FILTER, true, "Select the ground truth files");
            if (gtPaths != null)
            {
                if (imagePaths != null && gtPaths.Length != imagePaths.Length)
                {
                    MessageBox.Show("Please use equal number of gt files to images.");
                    gtPaths = null;
                    return;
                }
                textBoxGT.Text = Path.GetDirectoryName(gtPaths[0]);
            }
        }

        private void textBoxDest_Click(object sender, EventArgs e)
        {
            textBoxDest.Text = null;
            destFolder = FormUtils.ParametrizedFolderBrowserDialog("Choose the output folder");
            if (destFolder != null) textBoxDest.Text = destFolder;
        }

        private void buttonCrop_Click(object sender, EventArgs e)
        {
            if (imagePaths == null)
            {
                MessageBox.Show("Images not loaded"); return;
            }
            if (gtPaths == null && radioButtonChar.Checked && !checkBoxOnlyGT.Checked)
            {
                MessageBox.Show("Ground truth not loaded"); return;
            }
            if (destFolder == null)
            {
                MessageBox.Show("Destination path not loaded"); return;
            }
            if (radioButtonChar.Checked)
            {
                if (checkBoxOnlyGT.Checked) executeExplicitCropper(imagePaths, destFolder, radioButtonForward.Checked);
                else executeCropper(imagePaths, gtPaths, destFolder, radioButtonForward.Checked);
            }
            else if (radioButtonWords.Checked)
            {
                Experiments.WordsExtractorMultiThread(imagePaths, destFolder, checkBoxOnlyGT.Checked);
            }
        }

        public void executeExplicitCropper(String[] imFileNames, String outputFolder, bool forward) 
            // auto trexei stis kommenes gt eikones sto xeri kai paragei mono thetika apotelesmata
        {
            for (int i = 0; i < imFileNames.Length; i++)
            {
                String filename = imFileNames[i];
                using (Mat m = new Mat(filename, LoadImageType.Grayscale))
                {
                    Cropper c;
                    ERExtractor.ExtractorParameters ep = new ERExtractor.ExtractorParameters(1, 0.3f, 0.96f, 0.5, 0, m.Cols, m.Rows);
                    c = new Cropper(filename, new Rectangle[0], outputFolder, false, ep); // change to new Rectangle[0] for individual character images
                 
                    if (forward) c.RunCropper(m, true, "BgrGrayscaleF", false);
                    else
                    {
                        CvInvoke.BitwiseNot(m, m);
                        c.RunCropper(m, true, "BgrGrayscaleB", true);
                    }
                }
            }
        }

        public void executeCropper(String[] imFileNames, String[] groundTruthPaths, String destination, bool forward)
        {
            // Define type of examples
            DialogResult res = MessageBox.Show("Select Yes for Positive examples and No for Negative", "Type Of Examples", MessageBoxButtons.YesNo);
            bool typeOfExamples = res == System.Windows.Forms.DialogResult.Yes ? true : false;

            // Load gt rects to know where the characters are located in order to either avoid or find them
            List<Rectangle[]> gtRects = new List<Rectangle[]>();
            
            foreach (String s in groundTruthPaths)
            {
                gtRects.Add(IOUtils.LoadFileToRectList(s, ' ').ToArray());
            }

            for (int i = 0; i < imFileNames.Length; i++)
            {
                String filename = imFileNames[i];

                using (Mat m = new Mat(filename, LoadImageType.Grayscale))
                {
                    Cropper c;
                    ERExtractor.ExtractorParameters ep = new ERExtractor.ExtractorParameters(1, 0.00005f, 0.96f, 0, 0, m.Cols, m.Rows);

                    c = new Cropper(filename, gtRects[i], destFolder, true, ep); // change to new Rectangle[0] and false for individual character images

                    if (forward)
                        c.RunCropper(m, typeOfExamples, "BgrGrayscaleF", false);
                    else
                    {
                        CvInvoke.BitwiseNot(m, m);
                        c.RunCropper(m, typeOfExamples, "BgrGrayscaleB", true);
                    }
                    res = MessageBox.Show("Do you want the other channel for more results?", "Prompt", MessageBoxButtons.YesNo);
                    if (res == System.Windows.Forms.DialogResult.Yes)
                    {
                        CvInvoke.BitwiseNot(m, m);
                        if (forward)
                            c.RunCropper(m, typeOfExamples, "BgrGrayscaleB", true);
                        else
                            c.RunCropper(m, typeOfExamples, "BgrGrayscaleF", false);
                    }

                }
            }
        }

        private void radioButtonWords_CheckedChanged(object sender, EventArgs e)
        {
            textBoxGT.Enabled = !checkBoxOnlyGT.Checked && radioButtonChar.Checked;
            radioButtonForward.Enabled = radioButtonChar.Checked;
            radioButtonInverse.Enabled = radioButtonChar.Checked;
        }
    }
}
