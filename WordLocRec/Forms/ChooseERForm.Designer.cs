﻿namespace WordLocRec.Forms
{
    partial class ChooseERForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pictureBoxER = new System.Windows.Forms.PictureBox();
            this.textBoxAvailableERs = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.textBoxTitle = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.textBoxAcceptedERs = new System.Windows.Forms.TextBox();
            this.buttonPrev = new System.Windows.Forms.Button();
            this.buttonNext = new System.Windows.Forms.Button();
            this.buttonAcceptER = new System.Windows.Forms.Button();
            this.buttonDismissER = new System.Windows.Forms.Button();
            this.textBoxERInfo = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.textBoxDismissedERs = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.textBoxViewIndex = new System.Windows.Forms.TextBox();
            this.buttonUndo = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxER)).BeginInit();
            this.SuspendLayout();
            // 
            // pictureBoxER
            // 
            this.pictureBoxER.Location = new System.Drawing.Point(13, 13);
            this.pictureBoxER.Name = "pictureBoxER";
            this.pictureBoxER.Size = new System.Drawing.Size(258, 209);
            this.pictureBoxER.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBoxER.TabIndex = 0;
            this.pictureBoxER.TabStop = false;
            // 
            // textBoxAvailableERs
            // 
            this.textBoxAvailableERs.Location = new System.Drawing.Point(384, 60);
            this.textBoxAvailableERs.Name = "textBoxAvailableERs";
            this.textBoxAvailableERs.ReadOnly = true;
            this.textBoxAvailableERs.Size = new System.Drawing.Size(86, 20);
            this.textBoxAvailableERs.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label1.Location = new System.Drawing.Point(278, 63);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(79, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Available ERs :";
            // 
            // textBoxTitle
            // 
            this.textBoxTitle.Location = new System.Drawing.Point(280, 13);
            this.textBoxTitle.Name = "textBoxTitle";
            this.textBoxTitle.ReadOnly = true;
            this.textBoxTitle.Size = new System.Drawing.Size(190, 20);
            this.textBoxTitle.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label2.Location = new System.Drawing.Point(278, 89);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(79, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "ERs Accepted:";
            // 
            // textBoxAcceptedERs
            // 
            this.textBoxAcceptedERs.Location = new System.Drawing.Point(384, 86);
            this.textBoxAcceptedERs.Name = "textBoxAcceptedERs";
            this.textBoxAcceptedERs.ReadOnly = true;
            this.textBoxAcceptedERs.Size = new System.Drawing.Size(86, 20);
            this.textBoxAcceptedERs.TabIndex = 6;
            // 
            // buttonPrev
            // 
            this.buttonPrev.BackColor = System.Drawing.Color.LightSteelBlue;
            this.buttonPrev.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonPrev.Location = new System.Drawing.Point(277, 274);
            this.buttonPrev.Name = "buttonPrev";
            this.buttonPrev.Size = new System.Drawing.Size(75, 23);
            this.buttonPrev.TabIndex = 8;
            this.buttonPrev.Text = "Previous";
            this.buttonPrev.UseVisualStyleBackColor = false;
            this.buttonPrev.Click += new System.EventHandler(this.buttonPrev_Click);
            // 
            // buttonNext
            // 
            this.buttonNext.BackColor = System.Drawing.Color.LightSteelBlue;
            this.buttonNext.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonNext.Location = new System.Drawing.Point(395, 274);
            this.buttonNext.Name = "buttonNext";
            this.buttonNext.Size = new System.Drawing.Size(75, 23);
            this.buttonNext.TabIndex = 9;
            this.buttonNext.Text = "Next";
            this.buttonNext.UseVisualStyleBackColor = false;
            this.buttonNext.Click += new System.EventHandler(this.buttonNext_Click);
            // 
            // buttonAcceptER
            // 
            this.buttonAcceptER.BackColor = System.Drawing.Color.LightSteelBlue;
            this.buttonAcceptER.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonAcceptER.Location = new System.Drawing.Point(13, 228);
            this.buttonAcceptER.Name = "buttonAcceptER";
            this.buttonAcceptER.Size = new System.Drawing.Size(118, 69);
            this.buttonAcceptER.TabIndex = 10;
            this.buttonAcceptER.Text = "Accept ER";
            this.buttonAcceptER.UseVisualStyleBackColor = false;
            this.buttonAcceptER.Click += new System.EventHandler(this.buttonAcceptER_Click);
            // 
            // buttonDismissER
            // 
            this.buttonDismissER.BackColor = System.Drawing.Color.LightSteelBlue;
            this.buttonDismissER.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonDismissER.Location = new System.Drawing.Point(153, 228);
            this.buttonDismissER.Name = "buttonDismissER";
            this.buttonDismissER.Size = new System.Drawing.Size(118, 69);
            this.buttonDismissER.TabIndex = 11;
            this.buttonDismissER.Text = "Dismiss ER";
            this.buttonDismissER.UseVisualStyleBackColor = false;
            this.buttonDismissER.Click += new System.EventHandler(this.buttonDismissER_Click);
            // 
            // textBoxERInfo
            // 
            this.textBoxERInfo.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxERInfo.Location = new System.Drawing.Point(476, 24);
            this.textBoxERInfo.Multiline = true;
            this.textBoxERInfo.Name = "textBoxERInfo";
            this.textBoxERInfo.ReadOnly = true;
            this.textBoxERInfo.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.textBoxERInfo.Size = new System.Drawing.Size(179, 273);
            this.textBoxERInfo.TabIndex = 12;
            this.textBoxERInfo.Text = "Nothing available atm";
            // 
            // label4
            // 
            this.label4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label4.AutoSize = true;
            this.label4.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label4.Location = new System.Drawing.Point(476, 8);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(67, 13);
            this.label4.TabIndex = 13;
            this.label4.Text = "ER Statistics";
            // 
            // textBoxDismissedERs
            // 
            this.textBoxDismissedERs.Location = new System.Drawing.Point(384, 112);
            this.textBoxDismissedERs.Name = "textBoxDismissedERs";
            this.textBoxDismissedERs.ReadOnly = true;
            this.textBoxDismissedERs.Size = new System.Drawing.Size(86, 20);
            this.textBoxDismissedERs.TabIndex = 15;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label3.Location = new System.Drawing.Point(277, 115);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(80, 13);
            this.label3.TabIndex = 14;
            this.label3.Text = "ERs Dismissed:";
            // 
            // textBoxViewIndex
            // 
            this.textBoxViewIndex.Location = new System.Drawing.Point(361, 276);
            this.textBoxViewIndex.Name = "textBoxViewIndex";
            this.textBoxViewIndex.Size = new System.Drawing.Size(28, 20);
            this.textBoxViewIndex.TabIndex = 16;
            // 
            // buttonUndo
            // 
            this.buttonUndo.BackColor = System.Drawing.Color.LightSteelBlue;
            this.buttonUndo.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonUndo.Location = new System.Drawing.Point(277, 228);
            this.buttonUndo.Name = "buttonUndo";
            this.buttonUndo.Size = new System.Drawing.Size(75, 40);
            this.buttonUndo.TabIndex = 17;
            this.buttonUndo.Text = "Undo";
            this.buttonUndo.UseVisualStyleBackColor = false;
            this.buttonUndo.Click += new System.EventHandler(this.buttonUndo_Click);
            // 
            // ChooseERForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.GhostWhite;
            this.ClientSize = new System.Drawing.Size(660, 309);
            this.Controls.Add(this.buttonUndo);
            this.Controls.Add(this.textBoxViewIndex);
            this.Controls.Add(this.textBoxDismissedERs);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.textBoxERInfo);
            this.Controls.Add(this.buttonDismissER);
            this.Controls.Add(this.buttonAcceptER);
            this.Controls.Add(this.buttonNext);
            this.Controls.Add(this.buttonPrev);
            this.Controls.Add(this.textBoxAcceptedERs);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.textBoxTitle);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.textBoxAvailableERs);
            this.Controls.Add(this.pictureBoxER);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.SizableToolWindow;
            this.Name = "ChooseERForm";
            this.Text = "ChooseERForm";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxER)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBoxER;
        private System.Windows.Forms.TextBox textBoxAvailableERs;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBoxTitle;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox textBoxAcceptedERs;
        private System.Windows.Forms.Button buttonPrev;
        private System.Windows.Forms.Button buttonNext;
        private System.Windows.Forms.Button buttonAcceptER;
        private System.Windows.Forms.Button buttonDismissER;
        private System.Windows.Forms.TextBox textBoxERInfo;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox textBoxDismissedERs;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox textBoxViewIndex;
        private System.Windows.Forms.Button buttonUndo;
    }
}