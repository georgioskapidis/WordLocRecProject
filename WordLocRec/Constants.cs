﻿using System;
using System.IO;

namespace WordLocRec
{
    public static class Constants
    {
        public readonly static String IMAGE_FILE_FILTER = "Image Files (*.jpg, *.png, *.bmp)|*.jpg; *.png; *.bmp|All files (*.*)|*.*";
        public readonly static String TEXT_FILE_FILTER = "Text Files (*.txt)|*.txt;";
        public readonly static String XML_FILE_FILTER = "Xml Files (*.xml)|*.xml;";
        public readonly static String XML_YML_FILE_FILTER = "Xml Files (*.xml)|*.xml| Y(a)ml Files (*.yml, *.yaml)|*.yml; *.yaml";

        public readonly static double MAX_ZOOM = 25.0;
        public readonly static double MIN_ZOOM = 0.1;
        public readonly static int MAX_EMGU_THREADS = 2;
        public readonly static double EPSILON = 0.0000001;

        public readonly static String RF_WORDSTAT = "Resources\\ThesisClassifiers\\rWSgtpos_ngtneg61.xml";
        public readonly static String RF_SIMPLEWORDSTAT = "Resources\\ThesisClassifiers\\rSWSallexamplesplusAmbUnstandard.xml";
        public readonly static String RF_LINEWORDSTAT = "Resources\\ThesisClassifiers\\rLWSallExamplesPlusAmbUnstandard.xml";

        public readonly static String COLOR_DISTANCE_CLASSIFIER = "Resources\\ThesisClassifiers\\ColorDistLab, ColorDistPii, StandarizedPerFeatureDefIter.xml";

        public readonly static String SVM_NM = "Resources\\ThesisClassifiers\\NM.xml";
        public readonly static String SVM_sNM = "Resources\\ThesisClassifiers\\sNM.xml";
        public readonly static String SVM_sNMPathsConv = "Resources\\ThesisClassifiers\\sNMPathsConv.xml";
        public readonly static String SVM_NMPathsConvStr = "Resources\\ThesisClassifiers\\NMPathsConvStr.xml";
        public readonly static String SVM_sNMPathsConvStrLinear = "Resources\\ThesisClassifiers\\linear50SNMPathsConvStr.xml";
        public readonly static String SVM_sNMPathsConvStrPoly = "Resources\\ThesisClassifiers\\poly50SNMPathsConvStr.xml";
        public readonly static String SVM_sNMPathsConvStrRbf20 = "Resources\\ThesisClassifiers\\rbf20SNMPathsConvStr.xml";
        public readonly static String SVM_sNMPathsConvStrRbf50 = "Resources\\ThesisClassifiers\\rbf50SNMPathsConvStr.xml";

        public readonly static String MeanFfoPath = "Resources\\MeanStd\\ffoMean2.xml";
        public readonly static String StdFfoPath = "Resources\\MeanStd\\ffoStd2.xml";

        public readonly static String MeanWSPath = "Resources\\MeanStd\\wordstatmeanX2GtGtNeg.xml";
        public readonly static String StdWSPath = "Resources\\MeanStd\\wordstatstdX2GtGtNeg.xml";
        public readonly static String MeanSWSPath = "Resources\\MeanStd\\simplewordstatmeanX2GtGtNeg.xml";
        public readonly static String StdSWSPath = "Resources\\MeanStd\\simplewordstatstdX2GtGtNeg.xml";
        public readonly static string MeanLWSPath = "Resources\\MeanStd\\lwsCombinedMean.xml";
        public readonly static string StdLWSPath = "Resources\\MeanStd\\lwsCombinedStd.xml";

        public readonly static String svmInfoFilePath = PathDesktop + "\\SVMInfo.txt";
        public readonly static String PathDesktop = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
        public readonly static String PathDocuments = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
        public readonly static String PathResources = Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location) + "\\Resources";

        public readonly static String ICDAR2013GTPath = "Resources\\ICDAR2013_TESTGT";


        public readonly static bool VERBOSE = true;
        [Flags]
        public enum FeatureType
        {
            NM = 1,
            Histogram = 2,
            ColorDistLab = 4,
            ColorDistPii = 8,
            SignificantPaths = 16,
            NormalizedL1 = 32,
            NormalizedL2 = 64,
            StandarizedPerFeature = 128,
            NumberOfHoles = 256,
            ColorDistBgr = 512,
            ColorConsistencyLab = 1024,
            SimpleFeature = 2048,
            ConvexityDefects = 4096,
            StrokeRatio = 8192
        };
        [Flags]
        public enum WordFeatureType
        {
            WordStat = 1,
            SimpleWordStat = 2,
            LineWordStat = 4,
            Standardized = 8
        }// todo embeed sto sunoliko project sta shmeia opoy xrhsimopoiw wordstats. pros to paron tha to xrhsimopoihsw gia ta statistics calculations

        public enum ColorSpace
        {
            None,
            HSV,
            BGR,
            LAB,
            XYZ,
            Grayscale,
            Pii,
            Distances
        };
        [Flags]
        public enum VisChannels : byte
        {
            None = 0,
            Channel1 = 1,
            Channel2 = 2,
            Channel3 = 4,
            Channel4 = 8,
            Channel1Opp = 16,
            Channel2Opp = 32,
            Channel3Opp = 64,
            Channel4Opp = 128,
            
            AllChannels = Channel1 | Channel1Opp | Channel2 | Channel2Opp | Channel3 | Channel3Opp | Channel4 | Channel4Opp
        };
        public enum Gradients
        {
            None,
            Single,
            Multi,
            MultiSobel
        };
        public enum GroupingMethod
        {
            None,
            ExhaustiveSearch,
            Karatzas,
            secStage,
            chinese
        };

        // constants for Exhaustive Search Grouping Algorithm

        public readonly static double PAIR_MIN_HEIGHT_RATIO = 0.4;
        public readonly static double PAIR_MIN_CENTROID_ANGLE = -0.85;
        public readonly static double PAIR_MAX_CENTROID_ANGLE = 0.85;
        public readonly static double PAIR_MIN_REGION_DIST = -0.4;
        public readonly static double PAIR_MAX_REGION_DIST = 2.2;
        public readonly static int PAIR_MAX_INTENSITY_DIST = 111;
        public readonly static int PAIR_MAX_AB_DIST = 54;
        public readonly static double TRIPLET_MAX_DIST = 0.9;
        public readonly static double TRIPLET_MAX_SLOPE = 0.3;
        public readonly static double SEQUENCE_MAX_TRIPLET_DIST = 0.45;
        public readonly static int SEQUENCE_MIN_LENGTH = 4;
        public readonly static String CLASSIFIER1PATH = "Resources\\OpenCVClassifiers\\trained_classifierNM1.xml";
        public readonly static String CLASSIFIER2PATH = "Resources\\OpenCVClassifiers\\trained_classifierNM2.xml";
        public readonly static String CLASSIFIERGROUPINGPATH = "Resources\\OpenCVClassifiers\\trained_classifier_erGrouping.xml";


    }
}
