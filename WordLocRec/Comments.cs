﻿using Emgu.CV;
using Emgu.CV.CvEnum;
using Emgu.CV.ML;
using Emgu.CV.Structure;
using Emgu.CV.Util;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WordLocRec.DataStructs;
using WordLocRec.Forms;
using WordLocRec.SVMStuff;
using WordLocRec.Tools;

namespace WordLocRec
{
    class Comments
    {

        #region WordStatExtractorMultiThread

        ///// <summary>
        ///// gia na eksagw ta simple word stats apo ta word stats xwris na ksanakovw
        ///// </summary>
        ///// <param name="imageFiles"></param>
        ///// <param name="positives"></param>
        ///// <param name="negatives"></param>
        ///// <param name="outputFolder"></param>
        //public static void SimpleWordStatExtractorMultiThread(String[] imageFiles, String[] positives, String[] negatives, String outputFolder)
        //{
        //    if (imageFiles == null) return;
        //    if (outputFolder == null) return;
        //    if (positives == null) return;
        //    if (negatives == null) return;
        //    Constants.FeatureType f1 = Constants.FeatureType.ColorDistLab | Constants.FeatureType.ColorDistPii | Constants.FeatureType.StandarizedPerFeature;
        //    Constants.FeatureType f2 = Constants.FeatureType.NM;

        //    Tuple<List<Rectangle>, int>[] positiveRectangles = new Tuple<List<Rectangle>, int>[imageFiles.Length];
        //    Tuple<List<Rectangle>, int>[] negativeRectangles = new Tuple<List<Rectangle>, int>[imageFiles.Length];
        //    for (int i = 0; i < imageFiles.Length; i++)
        //    {
        //        positiveRectangles[i] = Tuple.Create<List<Rectangle>, int>(new List<Rectangle>(), int.Parse(Path.GetFileNameWithoutExtension(imageFiles[i])));
        //        negativeRectangles[i] = Tuple.Create<List<Rectangle>, int>(new List<Rectangle>(), int.Parse(Path.GetFileNameWithoutExtension(imageFiles[i])));
        //    }
        //    foreach (String pos in positives)
        //    {
        //        String[] parts = Path.GetFileNameWithoutExtension(pos).Split('T');
        //        int fileId = int.Parse(parts[0]);
        //        for (int i = 0; i < positiveRectangles.Length; i++)
        //        {
        //            if (positiveRectangles[i].Item2 == fileId)
        //            {
        //                WordStat temp = IOUtils.FromXml<WordStat>(pos);
        //                positiveRectangles[i].Item1.Add(temp.WordRect);
        //                break;
        //            }
        //        }
        //    }
        //    foreach (String neg in negatives)
        //    {
        //        String[] parts = Path.GetFileNameWithoutExtension(neg).Split('F');
        //        int fileId = int.Parse(parts[0]);
        //        for (int i = 0; i < negativeRectangles.Length; i++)
        //        {
        //            if (negativeRectangles[i].Item2 == fileId)
        //            {
        //                WordStat temp = IOUtils.FromXml<WordStat>(neg);
        //                negativeRectangles[i].Item1.Add(temp.WordRect);
        //                break;
        //            }
        //        }
        //    }
        //    ThreadPool.SetMaxThreads(1, 4);
        //    WordStatExtractorMultiThread[] loops = new WordStatExtractorMultiThread[imageFiles.Length];
        //    ManualResetEvent[] events = new ManualResetEvent[imageFiles.Length];
        //    for (int oo = 0; oo < imageFiles.Length; oo++)
        //    {
        //        events[oo] = new ManualResetEvent(false);
        //        WordStatExtractorMultiThread aloop = new WordStatExtractorMultiThread(imageFiles[oo], f1, f2, positiveRectangles[oo].Item1, negativeRectangles[oo].Item1, outputFolder, events[oo]);
        //        loops[oo] = aloop;

        //        ThreadPool.QueueUserWorkItem(aloop.ThreadPoolCallback, oo);
        //    }
        //}

        #endregion

        #region CERFilter


        //public List<CECER> getCECERS(InputArray image, bool useColorDistance)
        //{
        //    List<CECER> colorEnhancedRegions = new List<CECER>();
        //    cerEnhancement(image, colorEnhancedRegions, useColorDistance);
        //    enhanced = colorEnhancedRegions;
        //    return enhanced;
        //}
            #region xalasmeno cerEnhancement

        //private int calculateChildDominantColor(ERStat child)
        //{
        //    int thislvlArea = child.area;
        //    if (child.lvlNum.Count > 0) //item1 to threshold, item2 to count twn pixel sto sygkekrimeno threshold
        //    { //shmainei oti to child exei history ERs mesa toy opote prepei na afairethoyn apo to area gia na ypologistei to Smed ksana
        //        int historyArea = 0; //area of intrinsic ERs at a lower threshold
        //        for (int i = 0; i < child.lvlNum.Count; i++)
        //        {
        //            historyArea += child.lvlNum[i].Item2;
        //        }
        //        thislvlArea = child.area - historyArea; // area of this threshold only
        //        // if more than 50% of the ER's pixels are of this threshold then this is the dominant threshold
        //        if (thislvlArea >= historyArea)
        //        {
        //            child.fDC = child.level;
        //        }
        //        else  // use the pixels of current threshold and complete with pixels from lower thresholds to get 50%
        //        {
        //            child.fDC = thislvlArea * child.level;
        //            int remainingSpots = (int)Math.Ceiling((double)(child.area / 2) - thislvlArea);
        //            for (int i = 0; i < child.lvlNum.Count; i++)
        //            {
        //                if (child.lvlNum[i].Item2 <= remainingSpots)
        //                {
        //                    child.fDC += child.lvlNum[i].Item2 * child.lvlNum[i].Item1;
        //                    remainingSpots -= child.lvlNum[i].Item2;
        //                }
        //                else
        //                {
        //                    child.fDC += child.lvlNum[i].Item1 * remainingSpots;
        //                    break;
        //                }
        //            }
        //            child.fDC /= Math.Ceiling((double)child.area / 2);
        //        }
        //    }
        //    else
        //    {
        //        child.fDC = child.level;
        //    }
        //    return thislvlArea;
        //}
        //private void CombineParentChildColorHistory(ERStat parent, ERStat child)
        //{
        //    int thislvlArea = calculateChildDominantColor(child);
        //    // molis upologistei to dominant color tou child, prepei na prostethoun ta lvl tou ston parent
        //    // malista isws metraei na sunduastoun pithana idia level gia na glutwsoume epanalhpseis

        //    // add the current child threshold
        //    bool addChildlvls = true;
        //    int ai = 0;
        //    for (int i = 0; i < parent.lvlNum.Count; i++)
        //    {
        //        ai = -1;
        //        if (parent.lvlNum[i].Item1 > child.level) // child lvl is not found at parent history
        //        {
        //            ai = i;
        //            break;
        //        }
        //        if (parent.lvlNum[i].Item1 == child.level) // child's lvl already exists at parent history
        //        {
        //            parent.lvlNum[i] = Tuple.Create<int, int>(child.level, parent.lvlNum[i].Item2 + thislvlArea);
        //            addChildlvls = false;
        //            break;
        //        }
        //        ai = i;
        //    }
        //    if (addChildlvls)
        //    {
        //        parent.lvlNum.Insert(ai, Tuple.Create<int, int>(child.level, thislvlArea));
        //    }

        //    // add the history of the child
        //    addChildlvls = true;
        //    int index;
        //    for (int j = 0; j < child.lvlNum.Count; j++)
        //    {
        //        index = -1;
        //        for (int i = parent.lvlNum.Count - 1; i >= 0; i--)
        //        {
        //            if (child.lvlNum[j].Item1 < parent.lvlNum[i].Item1)
        //            {
        //                index = i;
        //                break;
        //            }
        //            if (parent.lvlNum[i].Item1 == child.lvlNum[j].Item1)
        //            {
        //                parent.lvlNum[i] = Tuple.Create<int, int>(child.lvlNum[j].Item1, parent.lvlNum[i].Item2 + child.lvlNum[j].Item2);
        //                addChildlvls = false;
        //                break;
        //            }
        //            index = i;
        //        }
        //        if (addChildlvls)
        //        {
        //            parent.lvlNum.Insert(index + 1, Tuple.Create<int, int>(child.lvlNum[j].Item1, child.lvlNum[j].Item2));
        //        }
        //    }
        //}
        //private void cerEnhancement(InputArray image, List<CECER> enhanced, bool useColorDistance)
        //{
        //    Byte[] imageData = image.GetMat().GetData();
        //    mWidth = image.GetMat().Cols;
        //    double defColorDistance = 85;
        //    for (int i = 0; i < regions.Count; i++)
        //    {
        //        CECER c = new CECER(regions[i].level, regions[i].fDC);
        //        Matrix<byte> tempM = new Matrix<byte>(regions[i].rect.Size);
        //        tempM.SetZero();
        //        int maxY = -1; int maxX = -1;
        //        foreach (int pixel in regions[i].pixels)
        //        {
        //            if (useColorDistance)
        //            {
        //                //double colorDistance = Math.Sqrt(Math.Pow(imageData[pixel], 2) - Math.Pow(c.dc, 2));
        //                double colorDistance = Math.Abs(imageData[pixel] - c.dc);
        //                if (colorDistance < defColorDistance)
        //                {
        //                    c.area++;
        //                    int tempX = pixel % mWidth;
        //                    int tempY = pixel / mWidth;
        //                    tempM[tempY - regions[i].rect.Y, tempX - regions[i].rect.X] = 255;
        //                    if (tempX > maxX) maxX = tempX;
        //                    if (tempY > maxY) maxY = tempY;
        //                }
        //            }
        //            else
        //            {
        //                c.area++;
        //                int tempX = pixel % mWidth;
        //                int tempY = pixel / mWidth;
        //                tempM[tempY - regions[i].rect.Y, tempX - regions[i].rect.X] = 255;
        //                if (tempX > maxX) maxX = tempX;
        //                if (tempY > maxY) maxY = tempY;
        //            }
        //        }
        //        //c.boundingBox = new Rectangle(regions[i].pixel % mWidth, regions[i].pixel / mWidth, maxX, maxY);
        //        c.boundingBox = regions[i].rect; // works as above lol eimai poly spatalos
        //        c.m = tempM.Mat.Clone();
        //        enhanced.Add(c);
        //    }
        //}

        #endregion  
        //if (nmSvmFilterAllowed)
        //        {
        //            SVM svm = new SVM();
        //            FileStorage fs = new FileStorage(Constants.CLASSIFIERSVMPATHPOSITIVE, FileStorage.Mode.Read);
        //            svm.Read(fs.GetRoot());
        //            var supvec = svm.GetSupportVectors();
        //            Matrix<float> vectors = new Matrix<float>(supvec.Rows, supvec.Cols, supvec.DataPointer);
        //            Matrix<float> sample = new Matrix<float>(regions.Count(), 7);
        //            Matrix<float> response = new Matrix<float>(regions.Count(), 1);
        //            for (int i = 0; i < regions.Count; i++)
        //            {
        //                ERStat stat = regions[i];
        //                CalcNM2stats(image.GetMat(), ref stat); // calculate the nm2 features
        //                NMFeatureObject nmfeature = new NMFeatureObject((float)(stat.rect.Width) / (stat.rect.Height), (float)Math.Sqrt((float)(stat.area)) / stat.perimeter, (float)(1 - stat.euler), stat.medCrossings, stat.holeAreaRatio, stat.convexHullRatio, stat.numInflexionPoints);

        //                nmfeature.NormalizeL2();
        //                SVMFullFeatureExample.NMFeatureToMatrixSample(nmfeature, ref sample, i);
        //            }

        //            svm.Predict(sample, response);

        //            List<ERStat> tempRes = new List<ERStat>(regions);
        //            regions = new List<ERStat>(tempRes.Count);
        //            for (int i = 0; i < response.Rows; i++)
        //                if (response[i, 0] == 1)
        //                {
        //                    regions.Add(tempRes[i]);
        //                }
        //            fs.ReleaseAndGetString();
        //        }
        //        if (svmFilterAllowed)
        //        {
        //            SVM svm = new SVM();
        //            FileStorage fs = new FileStorage(Constants.SVM_positive_intensities, FileStorage.Mode.Read);
        //            svm.Read(fs.GetRoot());
        //            var supvec = svm.GetSupportVectors();
        //            Matrix<float> vectors = new Matrix<float>(supvec.Rows, supvec.Cols, supvec.DataPointer);
        //            Matrix<float> sample = new Matrix<float>(regions.Count(), 11);
        //            Matrix<float> response = new Matrix<float>(regions.Count(), 1);
        //            for (int i = 0; i < regions.Count; i++)
        //            {
        //                ERStat stat = regions[i];
        //                CalcNM2stats(image.GetMat(), ref stat); // calculate the nm2 features
        //                NMFeatureObject nmfeature = new NMFeatureObject((float)(stat.rect.Width) / (stat.rect.Height), (float)Math.Sqrt((float)(stat.area)) / stat.perimeter, (float)(1 - stat.euler), stat.medCrossings, stat.holeAreaRatio, stat.convexHullRatio, stat.numInflexionPoints);

        //                Mat roi = new Mat(image.GetMat(), stat.rect);
        //                Mat erMask = new Mat();

        //                // calculate the histogram features
        //                Utils.BinaryMatFromERPixels(stat, image.GetMat(), ref erMask, true);
        //                HistogramCalculations.HistFeatures hf = new HistogramCalculations(roi, erMask).calculateHistFeatures();

        //                FullFeatureObject ffo = new FullFeatureObject(false, nmfeature, "", stat.rect, stat.pixel, image.GetMat().Cols, image.GetMat().Rows, stat.level, "", hf.distanceIntensityBB, hf.percOfBBPixelsAt2MainIntensities, hf.erbbIntensityRatio, hf.stdDevFullRange);

        //                ffo.NormalizeL2();

        //                Utils.FFFeatureToMatrixSample(ffo, ref sample, i);
        //                //Utils.NMFeatureToMatrixSample(nmfeature, ref sample, i);
        //            }
                
        //            svm.Predict(sample, response);

        //            List<ERStat> tempRes = new List<ERStat>(regions);
        //            regions = new List<ERStat>(tempRes.Count);
        //            for (int i = 0; i < response.Rows; i++)
        //                if (response[i, 0] == 1)
        //                {
        //                    regions.Add(tempRes[i]);
        //                }
        //            fs.ReleaseAndGetString();
        //        }

        #endregion

        #region old MainForm stuff
        // create and evaluate multiple models

        //public void CrossCheckSvmModels()
        //{
        //    String[] positiveFilenames = FormUtils.ParametrizedOpenFileDialog(Constants.XML_FILE_FILTER, true, "Select positive examples ");
        //    String[] negativeFilenames = FormUtils.ParametrizedOpenFileDialog(Constants.XML_FILE_FILTER, true, "Select negative examples ");
        //    if (positiveFilenames == null || negativeFilenames == null) return;

        //    String[] trainSetPos, testSetPos, trainSetNeg, testSetNeg;
        //    Utils.PickPercAtRandom<String>(positiveFilenames, out trainSetPos, out testSetPos);
        //    Utils.PickPercAtRandom<String>(negativeFilenames, out trainSetNeg, out testSetNeg);

        //    List<Constants.FeatureType> fTypes = new List<Constants.FeatureType>();
        //    Constants.FeatureType nm = Constants.FeatureType.NM;
        //    Constants.FeatureType nmStandardized = nm | Constants.FeatureType.StandarizedPerFeature;
        //    Constants.FeatureType nmLab = nm | Constants.FeatureType.ColorDistLab;
        //    Constants.FeatureType nmLabStandardized = nmLab | Constants.FeatureType.StandarizedPerFeature;
        //    Constants.FeatureType nmPii = nm | Constants.FeatureType.ColorDistPii;
        //    Constants.FeatureType nmPiiStandardized = nmPii | Constants.FeatureType.StandarizedPerFeature;
        //    Constants.FeatureType nmPaths = nm | Constants.FeatureType.SignificantPaths;
        //    Constants.FeatureType nmPathsStandardized = nmPaths | Constants.FeatureType.StandarizedPerFeature;
        //    Constants.FeatureType nmLabPaths = nmLab | nmPaths;
        //    Constants.FeatureType nmLabPathsStandardized = nmLabPaths | Constants.FeatureType.StandarizedPerFeature;
        //    Constants.FeatureType nmPiiPaths = nmPii | nmPaths;
        //    Constants.FeatureType nmPiiPathsStandardized = nmPiiPaths | Constants.FeatureType.StandarizedPerFeature;
        //    Constants.FeatureType nmLabPiiPaths = nm | Constants.FeatureType.ColorDistLab | Constants.FeatureType.ColorDistPii | Constants.FeatureType.SignificantPaths;
        //    Constants.FeatureType nmLabPiiPathsStandardized = nmLabPiiPaths | Constants.FeatureType.StandarizedPerFeature;
        //    Constants.FeatureType labPaths = Constants.FeatureType.ColorDistLab | Constants.FeatureType.SignificantPaths;
        //    Constants.FeatureType labPathsStandardized = labPaths | Constants.FeatureType.StandarizedPerFeature;
        //    Constants.FeatureType piiPaths = Constants.FeatureType.ColorDistPii | Constants.FeatureType.SignificantPaths;
        //    Constants.FeatureType piiPathsStandardized = piiPaths | Constants.FeatureType.StandarizedPerFeature;
        //    Constants.FeatureType labPiiPaths = Constants.FeatureType.ColorDistLab | Constants.FeatureType.ColorDistPii | Constants.FeatureType.SignificantPaths;
        //    Constants.FeatureType labPiiPathsStandardized = labPiiPaths | Constants.FeatureType.StandarizedPerFeature;
        //    Constants.FeatureType labPii = Constants.FeatureType.ColorDistLab | Constants.FeatureType.ColorDistPii;
        //    Constants.FeatureType labPiiStandardized = labPii | Constants.FeatureType.StandarizedPerFeature;
        //    fTypes.Add(nm); fTypes.Add(nmStandardized); fTypes.Add(nmLab); fTypes.Add(nmLabStandardized); fTypes.Add(nmPii); fTypes.Add(nmPiiStandardized); fTypes.Add(nmPaths); fTypes.Add(nmPathsStandardized); fTypes.Add(nmLabPaths); fTypes.Add(nmLabPathsStandardized); fTypes.Add(nmPiiPaths); fTypes.Add(nmPiiPathsStandardized); fTypes.Add(nmLabPiiPaths); fTypes.Add(nmLabPiiPathsStandardized); fTypes.Add(labPaths); fTypes.Add(labPathsStandardized); fTypes.Add(piiPaths); fTypes.Add(piiPathsStandardized); fTypes.Add(labPiiPaths); fTypes.Add(labPiiPathsStandardized); fTypes.Add(labPii); fTypes.Add(labPiiStandardized);
        //    List<SVMCreator> models = new List<SVMCreator>();
        //    foreach (var ftype in fTypes)
        //    {
        //        SVMCreator svmCreator1 = new SVMCreator(trainSetPos, testSetPos, trainSetNeg, testSetNeg, ftype);
        //        SVMCreator svmCreator2 = new SVMCreator(trainSetPos, testSetPos, trainSetNeg, testSetNeg, ftype);
        //        svmCreator1.CreateTrainTestSetAndClasses();
        //        svmCreator2.CreateTrainTestSetAndClasses();
        //        svmCreator1.MakeSVMAuto(20, true);
        //        svmCreator2.MakeSVMAuto(0, false);

        //        float[] percDefIt = svmCreator1.Evaluate<SVM>();
        //        float[] percUndefIt = svmCreator2.Evaluate<SVM>();

        //        if (Constants.VERBOSE)
        //        {
        //            Debug.WriteLine(ftype + " Defined Iterations model " + svmCreator1.ToString(true));
        //            Debug.WriteLine(ftype + " Undefined Iterations model " + svmCreator2.ToString(true));
        //        }
        //        models.Add(svmCreator1);
        //        models.Add(svmCreator2);
        //    }
        //    int counter = -1;
        //    for (int i = 0; i < models.Count; i++)
        //    {
        //        String iter = "";
        //        if (i % 2 == 0)
        //        {
        //            iter = "DefIter";
        //            counter++;
        //        }
        //        else
        //        {
        //            iter = "UndefIter";
        //        }
        //        String path = "C:\\Users\\George\\Desktop\\multipleModels\\" + fTypes[counter] + iter + ".xml";
        //        SvmUtils.SaveSVMToFile(models[i].currentModel, path);
        //    }
        //}


        //calc statistics

        //public void CalcStatistics<T>() where T : BaseFeature
        //{ // for base feature files
        //    String[] filenames = FormUtils.ParametrizedOpenFileDialog(Constants.XML_FILE_FILTER, true, "Select the files");
        //    if (filenames == null) return;

        //    List<List<float>> data = new List<List<float>>();
        //    for (int i = 0; i < filenames.Length; i++)
        //    {
        //        data.Add(IOUtils.FromXml<T>(filenames[i]).ToFloatList());
        //    }

        //    List<Tuple<float, float>> minmax = Utils.MinMaxInDataset(data);
        //    List<Tuple<float, float>> meanstd = Utils.MeanStdInDataset(data);
        //    foreach (Tuple<float, float> ff in minmax)
        //        Debug.WriteLine(ff.Item1 + " : " + ff.Item2);
        //    foreach (Tuple<float, float> ff in meanstd)
        //        Debug.WriteLine("mean: " + ff.Item1 + " || std: " + ff.Item2);
        //    if (typeof(T).Equals(typeof(SimpleWordStat)))
        //    {
        //        SimpleWordStat swsMean = new SimpleWordStat();
        //        swsMean.MeanForegroundIntensitiesCoV = meanstd[0].Item1;
        //        swsMean.MeanOuterBoundaryIntensitiesCoV = meanstd[1].Item1;
        //        swsMean.ForegroundBackgroundLabDistanceCoV = meanstd[2].Item1;
        //        swsMean.PerimetricGradientMagnitudeMeanCoV = meanstd[3].Item1;
        //        swsMean.MeanStrokeWidthCoV = meanstd[4].Item1;
        //        swsMean.ConvexHullCompactnessMean = meanstd[5].Item1;
        //        swsMean.ConvexHullCompactnessStd = meanstd[6].Item1;
        //        swsMean.ConvexityDefectsCoV = meanstd[7].Item1;
        //        swsMean.VerticalCenterDistanceCoV = meanstd[8].Item1;
        //        swsMean.HorizontalCenterDistancesCoV = meanstd[9].Item1;

        //        SimpleWordStat swsStd = new SimpleWordStat();
        //        swsStd.MeanForegroundIntensitiesCoV = meanstd[0].Item2;
        //        swsStd.MeanOuterBoundaryIntensitiesCoV = meanstd[1].Item2;
        //        swsStd.ForegroundBackgroundLabDistanceCoV = meanstd[2].Item2;
        //        swsStd.PerimetricGradientMagnitudeMeanCoV = meanstd[3].Item2;
        //        swsStd.MeanStrokeWidthCoV = meanstd[4].Item2;
        //        swsStd.ConvexHullCompactnessMean = meanstd[5].Item2;
        //        swsStd.ConvexHullCompactnessStd = meanstd[6].Item2;
        //        swsStd.ConvexityDefectsCoV = meanstd[7].Item2;
        //        swsStd.VerticalCenterDistanceCoV = meanstd[8].Item2;
        //        swsStd.HorizontalCenterDistancesCoV = meanstd[9].Item2;
        //        swsMean.ToXML<SimpleWordStat>(Constants.MeanSWSPath);
        //        swsStd.ToXML<SimpleWordStat>(Constants.StdSWSPath);
        //    }
        //    else if (typeof(T).Equals(typeof(LineWordStat)))
        //    {
        //        LineWordStat lwsMean = new LineWordStat();
        //        lwsMean.MeanStrokeWidthCoV = meanstd[0].Item1;
        //        lwsMean.MeanLabFGBGDistanceCoV = meanstd[1].Item1;
        //        lwsMean.LabConsistencyAmongERs = meanstd[2].Item1;
        //        lwsMean.MaxVerticalError = meanstd[3].Item1;
        //        lwsMean.WidthCoV = meanstd[4].Item1;
        //        lwsMean.PercentageHorizontalOverlap = meanstd[5].Item1;
        //        lwsMean.PercentageUncovered = meanstd[6].Item1;
        //        lwsMean.AvgHuDistEucl = meanstd[7].Item1;

        //        LineWordStat lwsStd = new LineWordStat();
        //        lwsStd.MeanStrokeWidthCoV = meanstd[0].Item2;
        //        lwsStd.MeanLabFGBGDistanceCoV = meanstd[1].Item2;
        //        lwsStd.LabConsistencyAmongERs = meanstd[2].Item2;
        //        lwsStd.MaxVerticalError = meanstd[3].Item2;
        //        lwsStd.WidthCoV = meanstd[4].Item2;
        //        lwsStd.PercentageHorizontalOverlap = meanstd[5].Item2;
        //        lwsStd.PercentageUncovered = meanstd[6].Item2;
        //        lwsStd.AvgHuDistEucl = meanstd[7].Item2;

        //        lwsMean.ToXML<LineWordStat>(Constants.MeanLWSPath);
        //        lwsStd.ToXML<LineWordStat>(Constants.StdLWSPath);
        //    }
        //    else if (typeof(T).Equals(typeof(WordStat)))
        //    {
        //        WordStat wsMean = new WordStat();
        //        wsMean.FGIntensitiesStd = meanstd[0].Item1;
        //        wsMean.BGIntensitiesStd = meanstd[1].Item1;
        //        wsMean.MajorAxisCoeffVariation = meanstd[2].Item1;
        //        wsMean.StrokeWidthsCoeffVariation = meanstd[3].Item1;
        //        wsMean.MeanGradientStd = meanstd[4].Item1;
        //        wsMean.AspectRatioCoeffVariation = meanstd[5].Item1;
        //        wsMean.HuMomentsAvgEuclideanDistance = meanstd[6].Item1;
        //        wsMean.ConvexHullCompactnessMean = meanstd[7].Item1;
        //        wsMean.ConvexHullCompactnessStd = meanstd[8].Item1;
        //        wsMean.ConvexityDefectsCoeffVariation = meanstd[9].Item1;
        //        wsMean.mstFeatures.AnglesMean = meanstd[10].Item1;
        //        wsMean.mstFeatures.AnglesStd = meanstd[11].Item1;
        //        wsMean.mstFeatures.EdgeWidthsCoeffVariation = meanstd[12].Item1;
        //        wsMean.mstFeatures.EdgeDistancesMeanDiametersMeanRatio = meanstd[13].Item1;
        //        wsMean.ToXML<WordStat>(Constants.MeanWSPath);

        //        WordStat wsStd = new WordStat();
        //        wsStd.FGIntensitiesStd = meanstd[0].Item2;
        //        wsStd.BGIntensitiesStd = meanstd[1].Item2;
        //        wsStd.MajorAxisCoeffVariation = meanstd[2].Item2;
        //        wsStd.StrokeWidthsCoeffVariation = meanstd[3].Item2;
        //        wsStd.MeanGradientStd = meanstd[4].Item2;
        //        wsStd.AspectRatioCoeffVariation = meanstd[5].Item2;
        //        wsStd.HuMomentsAvgEuclideanDistance = meanstd[6].Item2;
        //        wsStd.ConvexHullCompactnessMean = meanstd[7].Item2;
        //        wsStd.ConvexHullCompactnessStd = meanstd[8].Item2;
        //        wsStd.ConvexityDefectsCoeffVariation = meanstd[9].Item2;
        //        wsStd.mstFeatures.AnglesMean = meanstd[10].Item2;
        //        wsStd.mstFeatures.AnglesStd = meanstd[11].Item2;
        //        wsStd.mstFeatures.EdgeWidthsCoeffVariation = meanstd[12].Item2;
        //        wsStd.mstFeatures.EdgeDistancesMeanDiametersMeanRatio = meanstd[13].Item2;
        //        wsStd.ToXML<WordStat>(Constants.StdWSPath);
        //    }
        //}

        // Test methods from main form

        //public void nmTest()
        //{
        //    if (!ParsePropsInput()) return;
        //    // Create a filter for each of the classification stages
        //    MyERFilter filter1 = MyERFilterNM.createERFilterNM1(MyERFilterNM.loadClassifierNM1(Constants.CLASSIFIER1PATH),
        //                                                        erprops.thresholdDelta,
        //                                                        erprops.minArea,
        //                                                        erprops.maxArea,
        //                                                        erprops.minProbability,
        //                                                        true,
        //                                                        erprops.minProbabilityDifference);
        //    MyERFilter filter2 = MyERFilterNM.createERFilterNM2(MyERFilterNM.loadClassifierNM2(Constants.CLASSIFIER2PATH), 0.3f);

        //    byte[,] m = new byte[10, 10];
        //    //m.SetValue(new MCvScalar(230));
        //    for (int i = 0; i <= 9; i++)
        //    {
        //        for (int j = 0; j <= 9; j++)
        //        {
        //            if (i == 0 || j == 0 || i == 9 || j == 9)
        //            {
        //                m[i, j] = 230;
        //                continue;
        //            }
        //            m[i, j] = 170;
        //        }
        //    }
        //    //m[1, 3] = 230; m[1, 8] = 230;
        //    //m[2, 2] = 230; m[2, 5] = 170; m[2, 8] = 230;
        //    //m[3, 1] = 230; m[3, 2] = 230; m[3, 4] = 80; m[3, 5] = 170; m[3, 6] = 50; m[3, 8] = 230;
        //    //m[4, 1] = 230; m[4, 4] = 80; m[4, 5] = 230; m[4, 6] = 50;
        //    //m[5, 1] = 230; m[5, 3] = 80; m[5, 4] = 80; m[5, 5] = 230; m[5, 6] = 50; m[5, 7] = 50;
        //    //m[6, 3] = 80; m[6, 4] = 80; m[6, 5] = 170; m[6, 6] = 50; m[6, 7] = 50; m[6, 8] = 50;
        //    //m[7, 2] = 80; m[7, 3] = 80; m[7, 8] = 50;
        //    //m[8, 5] = 230; m[8, 6] = 230;

        //    m[1, 3] = 230; m[1, 8] = 230;
        //    m[2, 2] = 230; m[2, 5] = 80; m[2, 8] = 230;
        //    m[3, 1] = 230; m[3, 2] = 230; m[3, 4] = 80; m[3, 5] = 80; m[3, 6] = 80; m[3, 8] = 230;
        //    m[4, 1] = 230; m[4, 4] = 80; m[4, 5] = 230; m[4, 6] = 80;
        //    m[5, 1] = 230; m[5, 3] = 80; m[5, 4] = 80; m[5, 5] = 230; m[5, 6] = 80; m[5, 7] = 80;
        //    m[6, 3] = 80; m[6, 4] = 80; m[6, 5] = 80; m[6, 6] = 80; m[6, 7] = 80; m[6, 8] = 80;
        //    m[7, 2] = 80; m[7, 3] = 80; m[7, 8] = 80;
        //    m[8, 5] = 230; m[8, 6] = 230;

        //    Matrix<byte> matrix = new Matrix<byte>(m);

        //    List<ERStat> regions = new List<ERStat>();
        //    filter1.Run(matrix.GetInputArray(), ref regions);
        //    filter2.Run(matrix.GetInputArray(), ref regions);
        //    viewMat = matrix.GetInputArray().GetMat();
        //    setBitmapInPictureBox(viewMat.Bitmap);
        //}

        //public void method()
        //{
        //    Mat temp = new Mat();
        //    ColorConversionWrapper.ColorConversions(_originalImage, ColorConversion.Bgr2Gray, ref temp);

        //    // *** Dilation for text lines *** //
        //    Mat dilated = temp.Clone();
        //    Point anchor = new Point(-1, -1);
        //    //Mat structuringElement = CvInvoke.GetStructuringElement(ElementShape.Cross, new Size(3, 3), anchor);
        //    Byte[] strEl = new Byte[18];
        //    GCHandle strElHandle = GCHandle.Alloc(strEl, GCHandleType.Pinned);
        //    strEl[0] = 1; strEl[1] = 0; strEl[2] = 1; strEl[3] = 0; strEl[4] = 1; strEl[5] = 0; strEl[6] = 1; strEl[7] = 0; strEl[8] = 1;
        //    strEl[9] = 0; strEl[10] = 1; strEl[11] = 0; strEl[12] = 1; strEl[13] = 0; strEl[14] = 1; strEl[15] = 0; strEl[16] = 1; strEl[17] = 0;
        //    Mat structuringElement = new Mat(2, 9, DepthType.Cv8U, 1, strElHandle.AddrOfPinnedObject(), 9);
        //    CvInvoke.Dilate(temp, dilated, structuringElement, new Point(-1, -1), 2, BorderType.Default, new MCvScalar(0));

        //    // use erosion with a different structuring element that only keeps the text lines

        //    strElHandle.Free();

        //    strEl = new Byte[8];
        //    strElHandle = GCHandle.Alloc(strEl, GCHandleType.Pinned);
        //    strEl[0] = 1; strEl[1] = 1; strEl[2] = 1; strEl[3] = 1; strEl[4] = 1; strEl[5] = 1; strEl[6] = 1; strEl[7] = 1;
        //    structuringElement = new Mat(2, 4, DepthType.Cv8U, 1, strElHandle.AddrOfPinnedObject(), 4);
        //    //structuringElement = CvInvoke.GetStructuringElement(ElementShape.Rectangle,new Size(5,2),anchor);
        //    CvInvoke.MorphologyEx(dilated, dilated, MorphOp.Open, structuringElement, anchor, 5, BorderType.Default, new MCvScalar(0));
        //    structuringElement = CvInvoke.GetStructuringElement(ElementShape.Rectangle, new Size(3, 6), anchor);
        //    CvInvoke.Erode(dilated, dilated, structuringElement, anchor, 2, BorderType.Default, new MCvScalar(0));

        //    setBitmapInPictureBox(dilated.Bitmap);
        //}

        //public void GaussianPyramid()
        //{
        //    Mat gray = new Mat();
        //    ColorConversionWrapper.ColorConversions(_originalImage, ColorConversion.Bgr2Gray, ref gray);
        //    Image<Gray, Byte> pyrlvl0;
        //    Utils.MatToGrayImageC(gray, out pyrlvl0);
        //    Image<Gray, Byte> pyrlvl1 = new Image<Gray, byte>(pyrlvl0.Width / 2, pyrlvl0.Height / 2);
        //    Image<Gray, Byte> pyrlvl2 = new Image<Gray, byte>(pyrlvl1.Width / 2, pyrlvl1.Height / 2);
        //    Image<Gray, Byte> pyrlvl_1 = new Image<Gray, byte>(pyrlvl0.Width * 2, pyrlvl0.Height * 2);
        //    CvInvoke.PyrUp(pyrlvl0, pyrlvl_1);
        //    CvInvoke.PyrDown(pyrlvl0, pyrlvl1);
        //    CvInvoke.PyrDown(pyrlvl1, pyrlvl2);


        //    pyrlvl0._GammaCorrect(2);
        //    //pyrlvl0._EqualizeHist();         

        //    Image<Gray, Byte> pyrver = new Image<Gray, Byte>(pyrlvl0.Size);
        //    Image<Gray, Byte> pyrhor = pyrver.Clone();
        //    //pyrlvl_1 = (Image<Gray, byte>)Utils.ApplyCannyAutoThresh(pyrlvl_1);
        //    //pyrlvl0 = (Image<Gray, Byte>)Utils.ApplyCannyAutoThreshMedian(pyrlvl0);
        //    Mat cannyResult = new Mat();
        //    CannyWrapper.CannyAutoThreshMedian(pyrlvl0, ref cannyResult);
        //    Utils.MatToGrayImageC(cannyResult, out pyrlvl0);
        //    //pyrlvl0 = Utils.MatToGrayImage(CannyWrapper.CannyAutoThreshMedian(pyrlvl0));
        //    //CvInvoke.Erode(pyrlvl0, pyrhor, CvInvoke.GetStructuringElement(ElementShape.Rectangle, new Size(3, 1), new Point(-1, -1)), new Point(-1, -1), 1, BorderType.Default, new MCvScalar(0));
        //    //CvInvoke.Erode(pyrlvl0, pyrver, CvInvoke.GetStructuringElement(ElementShape.Rectangle, new Size(1, 3), new Point(-1, -1)), new Point(-1, -1), 1, BorderType.Default, new MCvScalar(0));

        //    //pyrlvl0 = pyrver + pyrhor;
        //    //CvInvoke.Dilate(pyrlvl0, pyrlvl0, CvInvoke.GetStructuringElement(ElementShape.Rectangle, new Size(3, 3), new Point(-1, -1)), new Point(-1, -1), 2, BorderType.Default, new MCvScalar(0));
        //    //pyrlvl1 = (Image<Gray, byte>)Utils.ApplyCannyAutoThresh(pyrlvl1);
        //    //pyrlvl2 = (Image<Gray, Byte>)Utils.ApplyCannyAutoThresh(pyrlvl2);
        //    //Image<Bgr, byte> result = Utils.ResizeTo(_originalImage, 0.5);
        //    //Image<Bgr, byte> result = _originalImage.Clone();
        //    //Rectangle[] rects = Utils.FindRegions(ref pyrlvl0);
        //    //int counter = 0;
        //    //foreach (Rectangle r in rects)
        //    //{
        //    //   result.Draw(r, new Bgr(100, 100, 100), 1);
        //    //   result.Draw(counter.ToString(), new Point(r.X, r.Bottom), FontFace.HersheyPlain, 1d, new Bgr(Color.Black));
        //    //   counter++;
        //    //}

        //    setBitmapInPictureBox(pyrlvl0.Bitmap);
        //}

        //public void MorphologicalTesting()
        //{
        //    Mat temp = new Mat();
        //    ColorConversionWrapper.ColorConversions(_originalImage, ColorConversion.Bgr2Gray, ref temp);

        //    //_edges = Utils.BinarizeOtsu(_edges);
        //    //_edges = (Image<Gray, Byte>)Utils.ApplyCannyAutoThresh(_edges);            
        //    //CvInvoke.BitwiseNot(_edges, _edges);

        //    Mat cleared = temp.Clone();
        //    Point anchor = new Point(-1, -1);
        //    Mat structuringElement = CvInvoke.GetStructuringElement(ElementShape.Cross, new Size(1, 1), anchor);

        //    //CvInvoke.Dilate(_edges, cleared, structuringElement, new Point(-1, -1), 1, BorderType.Default, new MCvScalar(0));
        //    CvInvoke.Erode(cleared, cleared, structuringElement, new Point(-1, -1), 1, BorderType.Default, new MCvScalar(0));

        //    setBitmapInPictureBox(cleared.Bitmap);
        //}

        //public void JustBinarization()
        //{
        //    Mat temp = new Mat(), gray = new Mat();
        //    ColorConversionWrapper.ColorConversions(_originalImage, ColorConversion.Bgr2Gray, ref gray);
        //    ImageProcessingUtils.BinarizeOtsuC(gray, ref temp);

        //    setBitmapInPictureBox(temp.Bitmap);

        //    temp.Dispose();
        //    gray.Dispose();
        //}

        //public void ColorQuantizationTest()
        //{
        //    Mat gray = new Mat();
        //    ColorConversionWrapper.ColorConversions(_originalImage, ColorConversion.Bgr2Gray, ref gray);

        //    Mat m = new Mat();
        //    ColorQuantizationWrapper.ColorQuantizationControler(gray, 16, ref m);
        //    //Image<Gray, Byte> m2 = Utils.MatToGrayImage(m);

        //    Mat smoothed = new Mat();
        //    ImageProcessingUtils.FilterGaussianC(m, ref smoothed);
        //    //Image<Gray, byte> m2 = new Image<Gray, byte>(m.Size);
        //    //Utils.MatToGrayImageC(m, out m2);
        //    //m2 = m2.SmoothBlur(3, 3);

        //    //Mat cannyResult = CannyWrapper.CannyAutoThreshMedian(m2);
        //    Mat cannyResult = new Mat();
        //    CannyWrapper.CannyAutoThreshMedian(smoothed, ref cannyResult);

        //    setBitmapInPictureBox(cannyResult.Bitmap);

        //    m.Dispose(); smoothed.Dispose(); cannyResult.Dispose(); gray.Dispose();
        //}

        //public void svmXORTest()
        //{
        //    Matrix<float> sample = new Matrix<float>(7, 2);
        //    Matrix<float> trainData = new Matrix<float>(6, 2);
        //    trainData[0, 0] = 0; trainData[0, 1] = 0;
        //    trainData[1, 0] = 1; trainData[1, 1] = 1;
        //    trainData[2, 0] = 2; trainData[2, 1] = 2;
        //    trainData[3, 0] = 3; trainData[3, 1] = 3;
        //    trainData[4, 0] = 4; trainData[4, 1] = 4;
        //    trainData[5, 0] = 5; trainData[5, 1] = 5;
        //    Matrix<int> labels = new Matrix<int>(6, 1);
        //    labels[0, 0] = 1; labels[1, 0] = 1; labels[2, 0] = 1; labels[3, 0] = 2; labels[4, 0] = 2; labels[5, 0] = 2;

        //    using (SVM model = new SVM())
        //    {
        //        model.SetKernel(SVM.SvmKernelType.Inter);
        //        model.Type = SVM.SvmType.CSvc;
        //        model.C = 1;
        //        model.TermCriteria = new MCvTermCriteria(6, 0.00001);

        //        TrainData td = new TrainData(trainData, Emgu.CV.ML.MlEnum.DataLayoutType.RowSample, labels);
        //        bool trained = model.TrainAuto(td);
        //        var supvec = model.GetSupportVectors();
        //        var vectors = new Matrix<float>(supvec.Rows, supvec.Cols, supvec.DataPointer);

        //        sample[0, 0] = 3.5f;
        //        sample[0, 1] = 3.5f;
        //        sample[1, 0] = 3.49f;
        //        sample[1, 1] = 3.49f;
        //        sample[2, 0] = -1f;
        //        sample[2, 1] = -1f;
        //        sample[3, 0] = 0.5f;
        //        sample[3, 1] = 0.5f;
        //        sample[4, 0] = 6f;
        //        sample[4, 1] = 6f;
        //        sample[5, 0] = 1f;
        //        sample[5, 1] = 4f;
        //        sample[6, 0] = 2f;
        //        sample[6, 1] = 2f;

        //        Matrix<float> responses = new Matrix<float>(sample.Rows, 1);
        //        model.Predict(sample, responses);
        //        Matrix<float> probs = new Matrix<float>(sample.Rows, 1);
        //        model.Predict(sample, probs, 1);

        //        for (int i = 0; i < responses.Rows; i++)
        //        {
        //            float exp = (float)Math.Exp(-2 * responses[i, 0]);
        //            probs[i, 0] = (float)1 - (float)1 / (1 + exp);
        //        }

        //    }
        //}

        //public void svmColorTest()
        //{
        //    Matrix<float> trainData = new Matrix<float>(6, 2);
        //    trainData[0, 0] = 10; trainData[0, 1] = 20; //15
        //    trainData[1, 0] = 20; trainData[1, 1] = 10;
        //    trainData[2, 0] = 20; trainData[2, 1] = 40; //30
        //    trainData[3, 0] = 40; trainData[3, 1] = 20;
        //    trainData[4, 0] = 40; trainData[4, 1] = 50; //45
        //    trainData[5, 0] = 50; trainData[5, 1] = 40;
        //    Matrix<int> labels = new Matrix<int>(6, 1);
        //    labels[0, 0] = 0; labels[1, 0] = 0; labels[2, 0] = 1; labels[3, 0] = 1; labels[4, 0] = 2; labels[5, 0] = 2;

        //    SVM model = new SVM();
        //    model.SetKernel(SVM.SvmKernelType.Rbf);
        //    model.Type = SVM.SvmType.CSvc;
        //    model.C = 1;
        //    model.Degree = 2;
        //    model.Gamma = 0.5;
        //    model.TermCriteria = new MCvTermCriteria(100, 0.00001);

        //    TrainData td = new TrainData(trainData, Emgu.CV.ML.MlEnum.DataLayoutType.RowSample, labels);
        //    bool trained = model.TrainAuto(td);

        //    Image<Bgr, byte> img = new Image<Bgr, byte>(60, 60);
        //    Matrix<float> sample = new Matrix<float>(img.Rows * img.Cols, 2);
        //    Matrix<float> response = new Matrix<float>(img.Rows * img.Cols, 1);
        //    int counter = 0;
        //    for (int y = 0; y < img.Rows; y++)
        //    {
        //        for (int x = 0; x < img.Cols; x++)
        //        {

        //            sample[counter, 0] = (float)y;
        //            sample[counter, 1] = (float)x;
        //            counter++;
        //        }
        //    }
        //    model.Predict(sample, response);

        //    for (int y = 0; y < img.Rows; y++)
        //    {
        //        for (int x = 0; x < img.Cols; x++)
        //        {
        //            img[y, x] = response[y * img.Cols + x, 0] == 0 ? new Bgr(90, 0, 0) :
        //                        response[y * img.Cols + x, 0] == 1 ? new Bgr(0, 90, 0) :
        //                        new Bgr(0, 0, 90);
        //        }
        //    }


        //    var supvec = model.GetSupportVectors();
        //    var vectors = new Matrix<float>(supvec.Rows, supvec.Cols, supvec.DataPointer);
        //    for (int i = 0; i < vectors.Rows; i++)
        //    {
        //        PointF p = new PointF(vectors[i, 0], vectors[i, 1]);
        //        img.Draw(new CircleF(p, 2), new Bgr(128, 128, 128), 2);
        //    }
        //    for (int i = 0; i < trainData.Rows / 3; i++)
        //    {
        //        img.Draw(new CircleF(new PointF(trainData[i, 0], trainData[i, 1]), 1), new Bgr(50, 0, 0));
        //        img.Draw(new CircleF(new PointF(trainData[i + trainData.Rows / 3, 0], trainData[i + trainData.Rows / 3, 1]), 1), new Bgr(0, 50, 0));
        //        img.Draw(new CircleF(new PointF(trainData[i + 2 * trainData.Rows / 3, 0], trainData[i + 2 * trainData.Rows / 3, 1]), 1), new Bgr(0, 0, 50));
        //    }
        //    new ImageViewer(img).Show();
        //}

        // stuff from cer CECERS and gifs

        //VectorOfMat mats = new VectorOfMat();
        //foreach (CECER c in enhancedCERs)
        //    mats.Push(c.m);
        //Utils.CreateGifFromVecMat(mats);
        //new WordRectangleForm(mats,"CECERS").Show();


        //VectorOfMat numbers = new VectorOfMat();
        //int eHeight = enhancedCERs[1].boundingBox.Height;
        //int eWidth = enhancedCERs[1].boundingBox.Width;
        //for (int i = 1; i < enhancedCERs.Count; i++)
        //{
        //    CECER c = enhancedCERs[i];
        //    Image<Gray, byte> tmp = new Image<Gray, byte>(32, eHeight, new Gray(0));
        //    tmp.Draw(c.level.ToString(), new Point(0, 11), FontFace.HersheyPlain, 1, new Gray(255));

        //    Matrix<byte> cmfull = new Matrix<byte>(eHeight, eWidth, 1);
        //    cmfull.SetZero();
        //    //Byte[] cmdata = c.m.GetData();
        //    Image<Gray, byte> cmdata = c.m.ToImage<Gray,byte>();
        //    int colDif = eWidth - c.boundingBox.Width;
        //    int rowDif = eHeight - c.boundingBox.Height;
        //    if (colDif < 0 || rowDif < 0) continue;

        //    for (int y = 0; y < cmdata.Height; y++)
        //    {
        //        for (int x = 0; x < cmdata.Width; x++)
        //        {
        //            cmfull[y + rowDif / 2, x + colDif / 2] = (byte)((int)cmdata[y, x].Intensity);
        //        }
        //    }

        //    Mat m = new Mat();

        //    CvInvoke.HConcat(tmp.GetInputArray().GetMat(), cmfull, m);
        //    numbers.Push(m);
        //}
        //Utils.CreateGifFromVecMat(numbers);

        // stuff from cer : generalized color enhancement

        //Mat orIm = new Mat();
        //ColorConversionWrapper.ColorConversions(mImage, flagColorSpace, ref orIm);
        //Mat[] forEnhancement = new Mat[3], forEnhancementNot = new Mat[3];

        //VectorOfMat temp = new VectorOfMat();
        //CvInvoke.Split(orIm, temp);
        //forEnhancement[0] = temp[0].Clone();
        //forEnhancement[1] = temp[1].Clone();
        //forEnhancement[2] = temp[2].Clone();
        //temp = new VectorOfMat();

        //Mat orImNot = new Mat();
        //CvInvoke.BitwiseNot(orIm, orImNot);
        //CvInvoke.Split(orImNot, temp);
        //forEnhancementNot[0] = temp[0].Clone();
        //forEnhancementNot[1] = temp[1].Clone();
        //forEnhancementNot[2] = temp[2].Clone();
        //temp.Dispose();
        //orIm.Dispose();
        //orImNot.Dispose();

        //List<ERStat> enhanced, enhanced2;
        //Δομή του vector "channels" είναι στα ζυγά τα ορθά κανάλια και στα περιττά τα αντίστροφα.
        // αν δεν υπάρχουν τα περιττά από επιλογή του χρήστη τότε το vector "channels" είναι συμπιεσμένο στις πρώτες θέσεις του vector

        //GC.Collect();
        //for (int k = 0; k < regions.Length; k++)
        //{
        //    VectorOfMat erMats = new VectorOfMat();
        //    for (int i = 0; i < regions[k].Count; i++)
        //    {
        //        Mat m = new Mat();
        //        Utils.BinaryMatFromERPixels(regions[k][i], channels[k], ref m);
        //        erMats.Push(m.Clone());
        //        m.Dispose();
        //    }
        //    //NNTrain(erMats);
        //    new WordRectangleForm(erMats, "ermats of channel: " + k, regions[k]).Show();

        //    VectorOfMat mats2 = new VectorOfMat();
        //    if (hasOpposites && k % 2 == 1)
        //        enhanced2 = filter1.generalizedColorEnhancement(forEnhancementNot, regions[k], ref erMats, ref mats2);
        //    else
        //        enhanced2 = filter1.generalizedColorEnhancement(forEnhancement, regions[k], ref erMats, ref mats2);
        //    if (mats2.Size > 0) new WordRectangleForm(mats2, "enhanced2 channel: " + k, enhanced2).Show();

        //    //VectorOfMat mats = new VectorOfMat();
        //    //if (hasOpposites && k % 2 == 1)
        //    //    enhanced = filter1.colorEnhancement(forEnhancementNot, regions[k], ref mats);
        //    //else
        //    //    enhanced = filter1.colorEnhancement(forEnhancement, regions[k], ref mats);
        //    //new WordRectangleForm(mats, "enhanced channel: " + k, enhanced).Show();

        //    erMats.Dispose();
        //    //mats2.Dispose();
        //    //mats.Dispose();
        //    //mats = new VectorOfMat();
        //    //enhanced = filter1.colorEnhancement(forEnhancementNot, regions[0], ref mats);
        //    //new WordRectangleForm(mats, "enhancedNot").Show();
        //}

        // some stuff about entropy from button3

        //** roi mats **//
        //List<Rectangle> rectsForRois = new List<Rectangle>();
        //foreach (Rectangle[] ra in rects)
        //    foreach (Rectangle r in ra)
        //        rectsForRois.Add(r);

        //Rectangle[] rectsForRoisArr = Utils.SouloupwseRects(rectsForRois.ToArray(), _originalImage.Rows, _originalImage.Cols);
        //VectorOfMat wordMats = RoiMatsFromWordRects(rectsForRoisArr, _originalImageGrayscale);
        //new WordRectangleForm(wordMats, "256 colors").Show();
        //VectorOfMat quantizedWordMats = new VectorOfMat();
        //for (int i = 0; i < wordMats.Size; i++)
        //{
        //    quantizedWordMats.Push((Mat)Utils.ColorQuantizationWrapper(wordMats[i], 2));
        //}
        //new WordRectangleForm(quantizedWordMats, "2 colors").Show();

        //VectorOfMat mats;
        //double[] entropies = new double[wordMats.Size], normEntropies = new double[wordMats.Size];

        //for (int ind = 0; ind < wordMats.Size; ind++)
        //{
        //    Mat firstMat = quantizedWordMats[ind];
        //    mats = new VectorOfMat();
        //    mats.Push(firstMat);
        //    Mat hist = new Mat();
        //    int[] histChannels = { 0 };
        //    int[] histSize = { 256 };               
        //    float[] histRanges = { 0, 256 };
        //    CvInvoke.CalcHist(mats, histChannels, null, hist, histSize, histRanges, false);

        //    Matrix<float> histogram = new Matrix<float>(hist.Rows, hist.Cols, hist.DataPointer);
        //    Matrix<float> freqHistogram = histogram / (firstMat.Rows * firstMat.Cols);
        //    Matrix<byte> wordRectMatrix = new Matrix<byte>(firstMat.Rows, firstMat.Cols, firstMat.DataPointer);

        //    double entropy = 0d;
        //    for (int i = 0; i < firstMat.Rows; i++)
        //        for (int j = 0; j < firstMat.Cols; j++)
        //        {
        //            double px = freqHistogram[wordRectMatrix[i, j], 0];
        //            if (px != 0d)
        //                entropy -= px * Math.Log(px, 2);
        //        }
        //    entropies[ind] = entropy;
        //    normEntropies[ind] = entropy / (firstMat.Rows * firstMat.Cols);
        //    Debug.WriteLine("***");
        //    Debug.WriteLine("Entropy for rect " + ind + " is: " + entropy);
        //    Debug.WriteLine("Entropy / size for rect " + ind + " is: " + normEntropies[ind]);
        //}

        //Image<Bgr, byte> tempImg = viewMat != null ? Utils.MatToBgrImage(Utils.GrayToBgr(viewMat)) : _originalImage.Clone();
        //for (int i = 0; i < quantizedWordMats.Size; i++)
        //{
        //    if (normEntropies[i] >= 0.15 && normEntropies[i] <= 0.20)
        //    {
        //        tempImg.Draw(rectsForRoisArr[i], new Bgr(Color.FromArgb(40, Color.Blue)), 1);
        //    }
        //}
        //VisualizeAllChannelsTogether(rects.ToList(), false, 2);

        //private void loadTestImagesToolStripMenuItem_Click(object sender, EventArgs e) // for massive calculations
        //{
        //    String tryName = textBoxTryNum.Text;
        //    String[] fileNames = FormUtils.ParametrizedOpenFileDialog(Constants.IMAGE_FILE_FILTER, true, "Select the images");
        //    if (fileNames == null) return;

        //    String classifier1Path, classifier2Path;
        //    if (!LoadBothClassifierPaths(out classifier1Path, out classifier2Path)) return;
        //    InitializeWatchAndLabel();

        //    ERFilterProps erProps = ParsePropsInput();
        //    BackgroundWorker assignmentWorker;

        //    if (!channelsSet)
        //    {
        //        flagColorSpace = Constants.ColorSpace.HSV;
        //        flagChannels = Constants.VisChannels.AllChannels;
        //        flagGrouping = Constants.GroupingMethod.ExhaustiveSearch;
        //    }
        //    assignmentWorker = new AssignmentWorker().CreateAssignmentWorker(erProps, classifier1Path, classifier2Path, tryName, this, checkBoxGammaCorrect.Checked, checkBoxGS.Checked, flagColorSpace, flagChannels, flagGradients, flagGrouping, FormUtils.ParametrizedOpenFileDialog(Constants.XML_FILE_FILTER, false, "Select the DTrees file")[0]);

        //    if (assignmentWorker.IsBusy != true)
        //    {
        //        assignmentWorker.RunWorkerAsync(fileNames);
        //        imagesCount = fileNames.Length;
        //    }
        //}

        //public void AnotherOneBitesTheDust()
        //{
        //    imagesCount--;

        //    if (NMWorkerProgress.InvokeRequired)
        //    {
        //        NMWorkerCallback callback = new NMWorkerCallback(setNMWorkerText);
        //        this.Invoke(callback);
        //    }
        //    else
        //    {
        //        setNMWorkerText();
        //    }

        //    if (imagesCount <= 0)
        //    {
        //        if (labelStatus.InvokeRequired)
        //        {
        //            NMWorkerCallback callback = new NMWorkerCallback(JobsDone);
        //            this.Invoke(callback);
        //        }
        //        else
        //        {
        //            JobsDone();
        //        }

        //    }
        //}
        //private void setNMWorkerText()
        //{
        //    String toString = "" + imagesCount + " remain";
        //    NMWorkerProgress.Text = toString;
        //}

        //private void loadTrainImagesTSMI_Click(object sender, EventArgs e)
        //{
        //    // load the train images
        //    String[] fileNames = FormUtils.ParametrizedOpenFileDialog(Constants.IMAGE_FILE_FILTER, true, "Select the images");
        //    if (fileNames == null) return;

        //    foreach (String s in fileNames)
        //    {
        //        Mat colorIm = new Mat(s, LoadImageType.Unchanged);
        //        Mat grayMat = new Mat();

        //        ColorConversionWrapper.ColorConversions(colorIm, ColorConversion.Bgr2Gray, ref grayMat);

        //        //Image<Gray, Byte> bin = (Image<Gray, Byte>)Utils.ApplyCannyAutoThreshMean(grayIm);
        //        Mat bin = new Mat();
        //        CannyWrapper.CannyAutoThreshMean(grayMat, ref grayMat);

        //        ColorConversionWrapper.ColorConversions(bin, ColorConversion.Gray2Bgr, ref colorIm);

        //        Image<Bgr, Byte> colorBin = new Image<Bgr, byte>(colorIm.Size);
        //        Utils.MatToBgrImageC(colorIm, out colorBin);

        //        //Utils.MatToBgrImage(colorIm);

        //        List<ConnComp> connectedComponents, connectedComponentsPruned;
        //        int totalCCPixels;

        //        int[,] labels = Utils.ConnectedComponentLabeling(bin, out connectedComponents, out totalCCPixels);

        //        connectedComponentsPruned = new List<ConnComp>();

        //        foreach (ConnComp cc in connectedComponents)
        //        {
        //            List<Point> ccList = cc.GetPointList();
        //            Bgr bgr;
        //            if (ccList.Count < 30)
        //            {
        //                bgr = new Bgr(Color.Black);
        //                foreach (Point p in ccList)
        //                {
        //                    labels[p.Y, p.X] = 0;
        //                }
        //            }
        //            else
        //            {
        //                bgr = new Bgr(cc.Color);
        //                connectedComponentsPruned.Add(cc);
        //            }
        //            foreach (Point p in cc.GetPointList())
        //                colorBin.Draw(new Point[] { p }, bgr);
        //        }

        //        foreach (ConnComp cc in connectedComponentsPruned)
        //        {
        //            Rectangle boundingRect = CvInvoke.BoundingRectangle(new VectorOfPoint(cc.GetPointList().ToArray()));
        //            cc.SetRectangle(boundingRect);
        //            colorBin.ROI = boundingRect;
        //            using (var lsf = new LabelSetForm(colorBin))
        //            {
        //                var result = lsf.ShowDialog();
        //                int label = 4;
        //                if (result == DialogResult.OK)
        //                {
        //                    label = lsf.Label;
        //                }
        //            }
        //            CCStats stat = new CCStats();
        //            stat.RatioWH = (double)boundingRect.Width / (double)boundingRect.Height;
        //            stat.RatioPixel = (double)cc.GetPointList().Count / (double)(boundingRect.Width * boundingRect.Height);

        //            // gia cc count
        //            int[,] rectLabels = new int[boundingRect.Height, boundingRect.Width];
        //            for (int i = 0; i < boundingRect.Height; i++)
        //            {
        //                for (int j = 0; j < boundingRect.Width; j++)
        //                {
        //                    rectLabels[i, j] = labels[i + boundingRect.Y, j + boundingRect.X];
        //                }
        //            }
        //            bool moreThan1CC = Utils.MoreThanOneCCs(ref rectLabels, cc.GetPointList());
        //            if (moreThan1CC)
        //            {
        //                List<ConnComp> rectCCs = new List<ConnComp>();
        //                Utils.RectConnectedComponent(rectLabels, out rectCCs);
        //                for (int ind = 0; ind < rectCCs.Count; ind++)
        //                    if (rectCCs[ind].GetPointList().Count == 0)
        //                        rectCCs.RemoveAt(ind);

        //                stat.CCCount = rectCCs.Count + 1;
        //            }
        //            else
        //            {
        //                stat.CCCount = 1;
        //            }

        //            stat.InflexionPoints = Utils.CountInflexionPoints(cc.GetPointList());
        //        }
        //    }
        //}   
        #endregion

        #region ChineseGrouping old
        //private static List<ERStat> RegionsInclusionCheck(List<ERStat> regions)
        //{
        //    for (int i = 1; i < regions.Count; i++)
        //    {
        //        for (int j = i + 1; j < regions.Count; j++)
        //        {
        //            ERStat thisER = regions[j];
        //            ERStat parent = thisER.parent;
        //            if (regions[i].rect.Contains(regions[j].rect))
        //                RemoveFromTree(regions, ref j, ref thisER, parent);
        //        }
        //    }
        //    return regions;
        //}

        //public static Rectangle[] DoChineseGrouping(List<ERStat> regs, IInputArray input3Channels, IInputArray inputChannel)
        //{
        //    using (Mat channel = inputChannel.GetInputArray().GetMat())
        //    using (Mat src3Channels = input3Channels.GetInputArray().GetMat())
        //    {
        //        if (channel.NumberOfChannels > 1) throw new Exception("lathos kanalia, prepei na einai 1");
        //        if (src3Channels.NumberOfChannels != 3) throw new Exception("lathos kanalia, prepei na einai 3");

        //        List<ERStat> regions = new List<ERStat>(regs);

        //        regions.Sort(new ERComparer());

        //        List<Line> lines = new List<Line>();

        //        for (int i = 0; i < regions.Count; i++)
        //        {
        //            if (regions[i].parent == null) continue;
        //            ERStat thisEr = regions[i];
        //            bool erAddedToLine = false;
        //            foreach (Line l in lines)
        //            {
        //                if (l.CanAdd(regions[i], src3Channels, channel))
        //                {
        //                    ERStat parent = regions[i].parent;
        //                    if (parent != null && parent.parent != null)
        //                    {
        //                        if (l.Includes(parent))
        //                        {
        //                            RemoveFromTree(regions, ref i, ref thisEr, parent);
        //                        }
        //                        else if (l.CanAdd(parent, src3Channels, channel))
        //                        {
        //                            RemoveFromTree(regions, ref i, ref thisEr, parent);
        //                            thisEr = parent;
        //                            l.AddER(thisEr);
        //                        }
        //                    }
        //                    else l.AddER(thisEr);
        //                    erAddedToLine = true;
        //                    break;
        //                }
        //            }
        //            if (!erAddedToLine)
        //            {
        //                lines.Add(new Line());
        //                lines[lines.Count - 1].AddER(thisEr);
        //            }
        //        }
        //        List<Rectangle> results = new List<Rectangle>();
        //        foreach (Line l in lines)
        //        {
        //            results.Add(l.LineToRectangle());
        //        }
        //        return results.ToArray();
        //    }
        //}

        //public static float FitLineLMS(List<Point> points, out float intercept, out float slope)
        //{
        //    intercept = -1; // intercept - tomh
        //    slope = 0;  // slope - klish
        //    int nPoints = points.Count;
        //    if (nPoints == 0) return -1;

        //    float err = float.MinValue, maxErr = float.MinValue;
        //    float xMean = 0, yMean = 0, sumMulXY = 0, sumXSquares = 0;
        //    float[] distX = new float[nPoints], distY = new float[nPoints], mulDistXY = new float[nPoints], distX2 = new float[nPoints];

        //    for (int i = 0; i < nPoints; i++)
        //    {
        //        xMean += points[i].X;
        //        yMean += points[i].Y;
        //    }
        //    xMean /= nPoints;
        //    yMean /= nPoints;

        //    for (int i = 0; i < nPoints; i++)
        //    {
        //        distX[i] = points[i].X - xMean;
        //        distY[i] = points[i].Y - yMean;
        //        mulDistXY[i] = distX[i] * distY[i];
        //        distX2[i] = distX[i] * distX[i];
        //        sumMulXY += mulDistXY[i];
        //        sumXSquares += distX2[i];
        //    }
        //    sumMulXY /= nPoints;
        //    sumXSquares /= nPoints;

        //    slope = sumMulXY / sumXSquares;
        //    intercept = yMean - slope * xMean;

        //    for (int i = 0; i < nPoints; i++)
        //    {
        //        err = (points[i].Y - (intercept + slope * points[i].X));
        //        if (err > maxErr) maxErr = err;
        //    }
        //    return maxErr;
        //}

        // the first CreateLineLMS method
        //public LineEstimate CreateLineLMS()
        //{
        //    if (!IsValidLine()) return null;
        //    List<Point> pointsBR = new List<Point>(), pointsTL = new List<Point>();
        //    int hMax = -1;
        //    for (int i = 0; i < ersOfLine.Count; i++)
        //    {
        //        pointsBR.Add(new Point(ersOfLine[i].rect.Right, ersOfLine[i].rect.Bottom));
        //        pointsTL.Add(new Point(ersOfLine[i].rect.Left, ersOfLine[i].rect.Top));
        //        if (ersOfLine[i].rect.Height > hMax) hMax = ersOfLine[i].rect.Height;
        //    }
        //    float interceptB0, interceptB1, interceptT0, interceptT1, slope;
        //    float maxErrorB = FitLineLMS(pointsBR, out interceptB0, out slope);
        //    interceptB1 = interceptB0;
        //    if (Math.Abs(maxErrorB) > hMax / 6)
        //    {
        //        interceptB1 = interceptB0 + maxErrorB;
        //    }
        //    interceptT1 = interceptT0 = 0;

        //    lineEstimates = new LineEstimate(interceptB0, interceptB1, interceptT0, interceptT1, slope, maxErrorB, 0);

        //    return lineEstimates;
        //}
        #endregion

        #region MatchShapes
        //private float MatchShapes(double[][] humoments)
        //{
        //    float avgSimilarity1 = 0f, avgSimilarity2 = 0f, avgSimilarity3 = 0f;
        //    int numMatches = 0;
        //    for (int i = 0; i < humoments.Length; i++)
        //    {
        //        double[] convI = LogConvertMoments(humoments[i]);
        //        for (int j = i + 1; j < humoments.Length; j++)
        //        {
        //            if (j == 11)
        //            {

        //            }
        //            double[] convJ = LogConvertMoments(humoments[j]);
        //            float sim1 = (float)OpenCVSim1(convI, convJ);
        //            float sim2 = (float)OpenCVSim2(convI, convJ);
        //            float sim3 = (float)OpenCVSim3(convI, convJ);
        //            avgSimilarity1 += sim1;
        //            avgSimilarity2 += sim2;
        //            avgSimilarity3 += sim3;
        //            numMatches++;
        //        }
        //    }

        //    return avgSimilarity3 / numMatches;
        //}
        //private double[] LogConvertMoments(double[] moments)
        //{
        //    double[] convertedMoments = new double[moments.Length];
        //    for (int i = 0; i < moments.Length; i++)
        //        convertedMoments[i] = Math.Sign(moments[i]) * Math.Log10(Math.Abs(moments[i]));
        //    return convertedMoments;
        //}
        //private double OpenCVSim1(double[] momentsA, double[] momentsB)
        //{
        //    double avgDistance = 0d;

        //    for (int i = 0; i < momentsA.Length; i++)
        //    {
        //        avgDistance += Math.Abs(1 / momentsA[i] - 1 / momentsB[i]);
        //    }
        //    return avgDistance;
        //}
        //private double OpenCVSim2(double[] momentsA, double[] momentsB)
        //{
        //    double avgDistance = 0d;

        //    for (int i = 0; i < momentsA.Length; i++)
        //    {
        //        avgDistance += Math.Abs(momentsA[i] - momentsB[i]);
        //    }
        //    return avgDistance;
        //}
        //private double OpenCVSim3(double[] momentsA, double[] momentsB)
        //{
        //    double avgDistance = 0d;

        //    for (int i = 0; i < momentsA.Length; i++)
        //    {
        //        avgDistance += Math.Abs(momentsA[i] - momentsB[i]) / Math.Abs(momentsA[i]);
        //    }
        //    return avgDistance;
        //}
        #endregion

        //public void CreateLabDistanceProjection()
        //{
        //    if (_originalImage == null)
        //    {
        //        MessageBox.Show("Please load an image first!", "error");
        //        return;
        //    }
        //    createViewMat(ColorConversionWrapper.ManualConversions.Bgr2LabTest);

        //    if (viewMat != null)
        //    {
        //        setBitmapInPictureBox(viewMat.Bitmap);
        //    }
        //}

        //public static VectorOfMat CreateNMChannels(Image<Bgr, Byte> originalImage, Image<Gray, Byte> originalImageGrayscale)
        //{
        //    VectorOfMat channels = new VectorOfMat();
        //    Mat gradient = ColorConversionWrapper.GradientMagnitude(originalImageGrayscale.GetInputArray().GetMat());
        //    Mat[] hsv = GetChannelsHSV(originalImage);

        //    // *** palio grad ***//
        //    //Image<Gray, Byte> temp = Utils.TurnToGradientSpace2(originalImageGrayscale.Clone());
        //    //channels.Push(CvInvoke.CvArrToMat(temp,true,true,0));
        //    //channels.Push(temp.Clone().GetInputArray().GetMat());
        //    // *** telos palio grad ***//

        //    //channels.Push(hsv[0]);
        //    //channels.Push(hsv[1]);
        //    //channels.Push(hsv[2]);
        //    //Mat m = new Mat();
        //    //CvInvoke.BitwiseNot(temp, m);
        //    //channels.Push(m);

        //    // *** OLA TA KANALIA OPWS PREPEI *** //
        //    channels.Push(hsv);
        //    channels.Push(gradient);
        //    int size = channels.Size;
        //    for (int i = 0; i < size; i++)
        //    {
        //        Mat mat = new Mat();
        //        CvInvoke.BitwiseNot(channels[i], mat);
        //        channels.Push(mat);
        //    }
        //    // *** END CHANNELS *** //

        //    return channels;
        //}

        //public static void ExtractColorERs(VectorOfMat channels, List<ERStat>[] regions, int i)
        //{
        //    if (regions[i].Count > 240)
        //    {
        //        VectorOfMat vec = new VectorOfMat();

        //        //Mat erMask = Utils.ErMaskFloodFill(channels[i], regions[i][293], false);
        //        ERStat region = regions[i][241];
        //        Mat reg = new Mat(channels[i], region.rect);
        //        //Utils.GetRoiPixels(mImage, erMask, regions[i][293], ref reg);

        //        vec.Push(reg.Clone());

        //        WordRectangleForm wrf = new WordRectangleForm(vec, " 241 ");
        //        wrf.Show();

        //    }
        //}

        //public static void CompareBinaryMats(Mat channel)
        //{
        //    if (channel == null) return;

        //    ERExtractor ere;
        //    List<ERStat> regions = new List<ERStat>();
        //    ere = new ERExtractor(new ERExtractor.ExtractorParameters(1, 0.00005, 0.96, 0, 0, channel.Width, channel.Height), ERUtils.SizeRestrictionsAcceptAll);
        //    regions = ere.extractERs(channel, false, false);

        //    Mat erPixels, erPixels3;
        //    Stopwatch bin = new Stopwatch(), flood = new Stopwatch();
        //    //bin.Start();
        //    //for (int i = 0; i < regions.Count; i++)
        //    //{
        //    //    erPixels = new Mat();

        //    //    Utils.BinaryMatFromERPixels(regions[i], channel, ref erPixels, true);

        //    //    erPixels.Dispose(); 
        //    //}
        //    //bin.Stop();
        //    flood.Start();
        //    for (int i = 0; i < regions.Count; i++)
        //    {
        //        using (Mat erPixels2 = Utils.ErMaskFloodFill(channel, regions[i], true))
        //        {
        //            CvInvoke.BitwiseNot(erPixels2, erPixels2);
        //        }
        //    }
        //    flood.Stop();

        //    Debug.WriteLine("time binaryMat " + bin.Elapsed);

        //    Debug.WriteLine("time floodfill " + flood.Elapsed);
        //    Debug.WriteLine("for " + regions.Count + " regions");
        //}

        #region oldPaths
        //public static void oldPathExtraction(ERStat stat, Mat m)
        //{
        //    Mat erPixels = new Mat();
        //    Utils.BinaryMatFromERPixels(stat, m, ref erPixels);


        //    Mat skeleton = new Mat();
        //    ZhangSuenThinning tt = new ZhangSuenThinning();
        //    tt.Skeletonize(erPixels, ref skeleton);

        //    Mat neighbourMat = new Mat(skeleton.Rows, skeleton.Cols, DepthType.Cv8U, 1);
        //    StrokeUtils.CreateNeighbourMat(skeleton, ref neighbourMat);

        //    double minVal = -1, maxVal = 0;
        //    Point minLoc = new Point(), maxLoc = new Point();
        //    CvInvoke.MinMaxLoc(neighbourMat, ref minVal, ref maxVal, ref minLoc, ref maxLoc, skeleton);
        //    Mat res = new Mat(skeleton.Rows, skeleton.Cols, DepthType.Cv8U, 1);
        //    List<int> pathLengths;
        //    CreatePaths(skeleton, neighbourMat, minLoc, out pathLengths, ref res);
        //}
        //public static void CreatePaths(Mat skeleton, Mat neighbourMat, Point startPoint, out List<int> pathLengths, ref Mat res)
        //{
        //    bool pathFinished = false;
        //    List<Point> checkpoints = new List<Point>();
        //    Point checkpoint = startPoint;
        //    Point prevCheckpoint = startPoint;
        //    checkpoints.Add(checkpoint);

        //    int numPaths = 0;
        //    pathLengths = new List<int>();
        //    int curPathLength = 0;
        //    numPaths++;

        //    int rows = skeleton.Rows;
        //    int cols = skeleton.Cols;

        //    int x, y, idx;
        //    bool[,] traversed = new bool[rows, cols];

        //    byte[] skel = skeleton.GetData();
        //    byte[] neig = neighbourMat.GetData();

        //    List<Point> queue = new List<Point>();


        //    while (!pathFinished)
        //    {
        //        x = checkpoint.X;
        //        y = checkpoint.Y;
        //        idx = y * cols + x;
        //        traversed[y, x] = true;
        //        curPathLength++;

        //        if (neig[idx] > 0)
        //        {
        //            float maxDistance = 0;
        //            Point maxIndices = new Point(-1, -1);
        //            bool neverEntered = true;

        //            for (int k = -1; k <= 1; k++) // οι δύο for είναι για να ελέγξω τα στοιχεία της γειτονιάς του i,j
        //            {
        //                for (int l = -1; l <= 1; l++)
        //                {
        //                    if ((k == 0 && l == 0) || (y + k) < 0 || (x + l) < 0 || (y + k) >= rows || (x + l) >= cols)
        //                        continue;
        //                    idx = (y + k) * cols + (x + l);
        //                    if (skel[idx] == 255 && !traversed[y + k, x + l])
        //                    {
        //                        neverEntered = false;
        //                        double x2mx1 = x + l - startPoint.X;
        //                        double y2my1 = y + k - startPoint.Y;
        //                        float newDistance = (float)Math.Sqrt(Math.Pow(x2mx1, 2) + Math.Pow(y2my1, 2));
        //                        if (maxDistance < newDistance)
        //                        {
        //                            if (maxIndices.X != -1 && maxIndices.Y != -1)
        //                                queue.Add(maxIndices);
        //                            maxDistance = newDistance;
        //                            maxIndices = new Point(x + l, y + k);
        //                        }
        //                        else
        //                        {
        //                            queue.Add(new Point(x + l, y + k));
        //                        }
        //                    }
        //                }
        //            }
        //            if (!neverEntered)
        //            {
        //                prevCheckpoint = checkpoint; //debug
        //                checkpoint = maxIndices;
        //                checkpoints.Add(checkpoint); //debug                        
        //            }
        //            else
        //            {
        //                //get from the queue, same bad code as above
        //                var temp = new Point(-1, -1);
        //                if (queue.Count > 0)
        //                {
        //                    temp = queue[queue.Count - 1];
        //                    queue.RemoveAt(queue.Count - 1);

        //                    while (traversed[temp.Y, temp.X])
        //                    {
        //                        if (queue.Count > 0)
        //                        {
        //                            temp = queue[queue.Count - 1];
        //                            queue.RemoveAt(queue.Count - 1);
        //                        }
        //                        else
        //                        {
        //                            pathLengths.Add(curPathLength);
        //                            pathFinished = true;
        //                            break;
        //                        }
        //                    }
        //                }
        //                else
        //                {
        //                    pathLengths.Add(curPathLength);
        //                    pathFinished = true;
        //                }
        //                // asign the point from the queue as the next checkpoint
        //                if (!pathFinished)
        //                {
        //                    prevCheckpoint = checkpoint; //debug
        //                    checkpoint = temp;
        //                    // the path has no more points so end this one and start a new one
        //                    if (!CommonNeighborhood(checkpoint, prevCheckpoint))
        //                    {
        //                        pathLengths.Add(curPathLength);
        //                        numPaths++;
        //                        curPathLength = 0;
        //                    }
        //                    checkpoints.Add(checkpoint); //debug
        //                }
        //            }
        //        }
        //        else { pathFinished = true; }
        //    }

        //    Utils.ByteMatFromBoolValues(traversed, ref res);

        //    MCvScalar ss = new MCvScalar(), sq = new MCvScalar();
        //    ss = CvInvoke.Sum(skeleton);
        //    sq = CvInvoke.Sum(res);
        //    if (!skeleton.Equals(res))
        //    {
        //    }

        //}

        //public static bool CommonNeighborhood(Point p1, Point p2)
        //{
        //    return Math.Abs(p1.X - p2.X) < 2 && Math.Abs(p1.Y - p2.Y) < 2;
        //}
        //public static void RunCa(Mat m, ref Mat skeleton /*, ref VectorOfMat gif */)
        //{
        //    byte[] data = m.GetData();
        //    bool[,] bin = new bool[m.Rows, m.Cols];
        //    for (int i = 0; i < data.Length; i++)
        //    {
        //        bin[i / m.Cols, i % m.Cols] = data[i] == 255 ? true : false;
        //    }
        //    CA ca = new CA(bin);
        //    bin = ca.Skeletonize(/*ref gif*/);
        //    byte[,] res = new byte[m.Rows, m.Cols];
        //    for (int i = 0; i < m.Rows; i++)
        //    {
        //        for (int j = 0; j < m.Cols; j++)
        //        {
        //            res[i, j] = bin[i, j] ? (byte)255 : (byte)0;
        //        }
        //    }
        //    skeleton.SetTo(res);
        //}
        #endregion

        //public static IOutputArray ApplyCannyAutoThreshMedian<TColor, TDepth>(Image<TColor, TDepth> image)
        //    where TColor : struct, IColor
        //    where TDepth : new()
        //{
        //    int median = Utils.CalculateMedian(image);
        //    double thresh1 = Math.Max(0, (1 - 0.5) * median);
        //    double thresh2 = Math.Min(255, (1 + 0.2) * median);

        //    return image.Canny(thresh1, thresh2);
        //}

        #region old machine learning

        //public void makeSVMOneClassFFOnormed()
        //{
        //    OpenFileDialog positiveOfd = new OpenFileDialog();
        //    positiveOfd.Multiselect = true;
        //    positiveOfd.Title = " Select positive examples";
        //    if (positiveOfd.ShowDialog() == System.Windows.Forms.DialogResult.OK)
        //    {
        //        String[] trainSetPos, testSetPos;
        //        Utils.PickPercAtRandom<String>(positiveOfd.FileNames, out trainSetPos, out testSetPos);

        //        Matrix<float> trainExamples = new Matrix<float>(trainSetPos.Length, 11);
        //        for (int i = 0; i < trainSetPos.Length; i++)
        //        {
        //            FullFeatureObject ffObj = IOUtils.FromXml<FullFeatureObject>(trainSetPos[i]);
        //            ffObj.NormalizeL2();
        //            Utils.FFFeatureToMatrixSample(ffObj, ref trainExamples, i);
        //        }
        //        Matrix<int> trainClasses = new Matrix<int>(trainSetPos.Length, 1);
        //        trainClasses.SetValue(1);
        //        Matrix<float> testExamples = new Matrix<float>(testSetPos.Length, 11);
        //        for (int i = 0; i < testSetPos.Length; i++)
        //        {
        //            FullFeatureObject ffObj = IOUtils.FromXml<FullFeatureObject>(testSetPos[i]);
        //            ffObj.NormalizeL2();
        //            Utils.FFFeatureToMatrixSample(ffObj, ref testExamples, i);
        //        }
        //        Matrix<int> testClasses = new Matrix<int>(testSetPos.Length, 1);
        //        testClasses.SetValue(1);

        //        // train the model
        //        SVM model = new SVM();
        //        model.SetKernel(SVM.SvmKernelType.Rbf);
        //        model.Type = SVM.SvmType.OneClass;
        //        model.Nu = 0.2;
        //        model.TermCriteria = new MCvTermCriteria(10, 10e-8);

        //        TrainData td = new TrainData(trainExamples, Emgu.CV.ML.MlEnum.DataLayoutType.RowSample, trainClasses);
        //        model.TrainAuto(td);

        //        // save the svm model
        //        String path = Constants.SVM_positive_intensities;
        //        SvmUtils.SaveSVMToFile(model, path);

        //        // evaluate the model
        //        float percentageTrain = SvmUtils.EvaluateSVMModel(model, trainExamples, trainClasses);
        //        float percentageTest = SvmUtils.EvaluateSVMModel(model, testExamples, testClasses);

        //    }
        //}
        //public void makeSvmOneClassNMnormed()
        //{
        //    OpenFileDialog positiveOfd = new OpenFileDialog();
        //    positiveOfd.Multiselect = true;
        //    positiveOfd.Title = " Select positive examples";
        //    if (positiveOfd.ShowDialog() == System.Windows.Forms.DialogResult.OK)
        //    {
        //        String[] trainSetPos, testSetPos;
        //        Utils.PickPercAtRandom<String>(positiveOfd.FileNames, out trainSetPos, out testSetPos);

        //        Matrix<float> trainExamples = new Matrix<float>(trainSetPos.Length, 7);
        //        for (int i = 0; i < trainSetPos.Length; i++)
        //        {
        //            NMFeatureObject nmObj = IOUtils.FromXml<FullFeatureObject>(trainSetPos[i]).getNMFeatureObject();
        //            nmObj.NormalizeL2();
        //            SVMFullFeatureExample.NMFeatureToMatrixSample(nmObj, ref trainExamples, i);
        //        }
        //        Matrix<int> trainClasses = new Matrix<int>(trainSetPos.Length, 1);
        //        trainClasses.SetValue(1);
        //        Matrix<float> testExamples = new Matrix<float>(testSetPos.Length, 7);
        //        for (int i = 0; i < testSetPos.Length; i++)
        //        {
        //            NMFeatureObject nmObj = IOUtils.FromXml<FullFeatureObject>(testSetPos[i]).getNMFeatureObject();
        //            nmObj.NormalizeL2();
        //            SVMFullFeatureExample.NMFeatureToMatrixSample(nmObj, ref testExamples, i);
        //        }
        //        Matrix<int> testClasses = new Matrix<int>(testSetPos.Length, 1);
        //        testClasses.SetValue(1);

        //        // train the model
        //        SVM model = new SVM();
        //        model.SetKernel(SVM.SvmKernelType.Rbf);
        //        model.Type = SVM.SvmType.OneClass;
        //        model.Nu = 0.1;
        //        model.TermCriteria = new MCvTermCriteria(1, 10e-8);

        //        TrainData td = new TrainData(trainExamples, Emgu.CV.ML.MlEnum.DataLayoutType.RowSample, trainClasses);
        //        model.TrainAuto(td);

        //        String path = Constants.CLASSIFIERSVMPATHNEGATIVE;
        //        // save the svm model
        //        SvmUtils.SaveSVMToFile(model, path);

        //        // evaluate the model
        //        float percentageTrain = SvmUtils.EvaluateSVMModel(model, trainExamples, trainClasses);
        //        float percentageTest = SvmUtils.EvaluateSVMModel(model, testExamples, testClasses);

        //    }
        //}
        //public void make2SVMs(String[] trainSetPos, String[] testSetPos, String[] trainSetNeg, String[] testSetNeg)
        //{
        //    // train examples and classes
        //    Matrix<float> trainExamples1 = new Matrix<float>(trainSetPos.Length + trainSetNeg.Length, 5);
        //    Matrix<float> trainExamples2 = new Matrix<float>(trainSetPos.Length + trainSetNeg.Length, 3);
        //    Matrix<float> positiveExamples1 = trainExamples1.GetRows(0, trainSetPos.Length, 1);
        //    Matrix<float> positiveExamples2 = trainExamples2.GetRows(0, trainSetPos.Length, 1);
        //    for (int i = 0; i < trainSetPos.Length; i++)
        //    {
        //        FullFeatureObject ffo = IOUtils.FromXml<FullFeatureObject>(trainSetPos[i]);
        //        NMFeatureObject nmObj = ffo.getNMFeatureObject();

        //        positiveExamples1[i, 0] = nmObj.aspectRatio;
        //        positiveExamples1[i, 1] = nmObj.compactness;
        //        positiveExamples1[i, 2] = nmObj.numOfHoles;
        //        positiveExamples1[i, 3] = nmObj.medianOfCrossings;
        //        positiveExamples1[i, 4] = ffo.colorDistERBBLab;

        //        positiveExamples2[i, 0] = nmObj.holeAreaRatio;
        //        positiveExamples2[i, 1] = nmObj.convexHullRatio;
        //        positiveExamples2[i, 2] = nmObj.numInflextionPoints;
        //        //positiveExamples2[i, 3] = ffo.colorDistERBBLab;
        //    }
        //    Matrix<float> negativeExamples1 = trainExamples1.GetRows(trainSetPos.Length, trainExamples1.Rows, 1);
        //    Matrix<float> negativeExamples2 = trainExamples2.GetRows(trainSetPos.Length, trainExamples2.Rows, 1);
        //    for (int i = 0; i < trainSetNeg.Length; i++)
        //    {
        //        FullFeatureObject ffo = IOUtils.FromXml<FullFeatureObject>(trainSetNeg[i]);
        //        NMFeatureObject nmObj = ffo.getNMFeatureObject();

        //        negativeExamples1[i, 0] = nmObj.aspectRatio;
        //        negativeExamples1[i, 1] = nmObj.compactness;
        //        negativeExamples1[i, 2] = nmObj.numOfHoles;
        //        negativeExamples1[i, 3] = nmObj.medianOfCrossings;
        //        negativeExamples1[i, 4] = ffo.colorDistERBBLab;

        //        negativeExamples2[i, 0] = nmObj.holeAreaRatio;
        //        negativeExamples2[i, 1] = nmObj.convexHullRatio;
        //        negativeExamples2[i, 2] = nmObj.numInflextionPoints;
        //        //negativeExamples2[i, 3] = ffo.colorDistERBBLab;
        //    }
        //    Matrix<int> trainClasses = SVMCreator.DualTrainClasses(positiveExamples1.Rows, trainExamples1.Rows);
        //    //test examples and classes
        //    Matrix<float> testExamples1 = new Matrix<float>(testSetPos.Length + testSetNeg.Length, 5);
        //    Matrix<float> testExamples2 = new Matrix<float>(testSetPos.Length + testSetNeg.Length, 3);
        //    Matrix<float> positiveTest1 = testExamples1.GetRows(0, testSetPos.Length, 1);
        //    Matrix<float> positiveTest2 = testExamples2.GetRows(0, testSetPos.Length, 1);
        //    for (int i = 0; i < testSetPos.Length; i++)
        //    {
        //        FullFeatureObject ffo = IOUtils.FromXml<FullFeatureObject>(testSetPos[i]);
        //        NMFeatureObject nmObj = ffo.getNMFeatureObject();

        //        positiveTest1[i, 0] = nmObj.aspectRatio;
        //        positiveTest1[i, 1] = nmObj.compactness;
        //        positiveTest1[i, 2] = nmObj.numOfHoles;
        //        positiveTest1[i, 3] = nmObj.medianOfCrossings;
        //        positiveTest1[i, 4] = ffo.colorDistERBBLab;

        //        positiveTest2[i, 0] = nmObj.holeAreaRatio;
        //        positiveTest2[i, 1] = nmObj.convexHullRatio;
        //        positiveTest2[i, 2] = nmObj.numInflextionPoints;
        //        //positiveTest2[i, 3] = ffo.colorDistERBBLab;
        //    }
        //    Matrix<float> negativeTest1 = testExamples1.GetRows(testSetPos.Length, testExamples1.Rows, 1);
        //    Matrix<float> negativeTest2 = testExamples2.GetRows(testSetPos.Length, testExamples2.Rows, 1);
        //    for (int i = 0; i < testSetNeg.Length; i++)
        //    {
        //        FullFeatureObject ffo = IOUtils.FromXml<FullFeatureObject>(testSetNeg[i]);
        //        NMFeatureObject nmObj = ffo.getNMFeatureObject();

        //        negativeTest1[i, 0] = nmObj.aspectRatio;
        //        negativeTest1[i, 1] = nmObj.compactness;
        //        negativeTest1[i, 2] = nmObj.numOfHoles;
        //        negativeTest1[i, 3] = nmObj.medianOfCrossings;
        //        negativeTest1[i, 4] = ffo.colorDistERBBLab;

        //        negativeTest2[i, 0] = nmObj.holeAreaRatio;
        //        negativeTest2[i, 1] = nmObj.convexHullRatio;
        //        negativeTest2[i, 2] = nmObj.numInflextionPoints;
        //        //negativeTest2[i, 3] = ffo.colorDistERBBLab;
        //    }
        //    Matrix<int> testClasses = SVMCreator.DualTrainClasses(positiveTest1.Rows, testExamples1.Rows);

        //    // create the svm models
        //    SVM model = new SVM(), model2 = new SVM();
        //    model.SetKernel(SVM.SvmKernelType.Rbf); model2.SetKernel(SVM.SvmKernelType.Rbf);
        //    model.Type = SVM.SvmType.CSvc; model2.Type = SVM.SvmType.CSvc;
        //    model.C = 2; model2.C = 2;
        //    model.Gamma = 0.01; model2.Gamma = 0.01;
        //    model.Coef0 = 0; model2.Coef0 = 0;
        //    model.Nu = 0.1; model2.Nu = 0.1;
        //    model.Degree = 10; model2.Degree = 10;
        //    model.TermCriteria = new MCvTermCriteria(10, 10e-8); model2.TermCriteria = new MCvTermCriteria(10, 10e-8);

        //    // train the svm models
        //    TrainData td = new TrainData(trainExamples1, Emgu.CV.ML.MlEnum.DataLayoutType.RowSample, trainClasses);
        //    bool trained = model.TrainAuto(td);
        //    TrainData td2 = new TrainData(trainExamples2, Emgu.CV.ML.MlEnum.DataLayoutType.RowSample, trainClasses);
        //    bool trained2 = model2.TrainAuto(td2);

        //    // save the svm models
        //    String path = "C:\\Users\\George\\Desktop\\trainedSVM\\SVM53Features1.xml";
        //    SvmUtils.SaveSVMToFile(model, path);

        //    String path2 = "C:\\Users\\George\\Desktop\\trainedSVM\\SVM53Features2.xml";
        //    SvmUtils.SaveSVMToFile(model2, path2);

        //    FileStorage fs, fs2;
        //    model = SvmUtils.LoadSVMFromFile(path, out fs);
        //    model2 = SvmUtils.LoadSVMFromFile(path2, out fs2);

        //    float percTrain = SvmUtils.EvaluateSVMModel(model, trainExamples1, trainClasses);
        //    float percTest = SvmUtils.EvaluateSVMModel(model, testExamples1, testClasses);
        //    float percTrain2 = SvmUtils.EvaluateSVMModel(model2, trainExamples2, trainClasses);
        //    float percTest2 = SvmUtils.EvaluateSVMModel(model2, testExamples2, testClasses);

        //    var supvec = model.GetSupportVectors();
        //    Matrix<float> vectors = new Matrix<float>(supvec.Rows, supvec.Cols, supvec.DataPointer);

        //    var supvec2 = model2.GetSupportVectors();
        //    Matrix<float> vectors2 = new Matrix<float>(supvec2.Rows, supvec2.Cols, supvec2.DataPointer);
        //}
        //public void makeSVM(String[] trainSetPos, String[] testSetPos, String[] trainSetNeg, String[] testSetNeg)
        //{
        //    // train examples and classes
        //    Matrix<float> trainExamples = new Matrix<float>(trainSetPos.Length + trainSetNeg.Length, 8);
        //    Matrix<float> positiveExamples = trainExamples.GetRows(0, trainSetPos.Length, 1);
        //    for (int i = 0; i < trainSetPos.Length; i++)
        //    {
        //        FullFeatureObject ffo = IOUtils.FromXml<FullFeatureObject>(trainSetPos[i]);
        //        NMFeatureObject nmObj = ffo.getNMFeatureObject();
        //        //nmObj.NormalizeL2();
        //        SVMFullFeatureExample.NMFeatureToMatrixSample(nmObj, ref positiveExamples, i);
        //        positiveExamples[i, 7] = ffo.colorDistERBBLab;
        //    }
        //    Matrix<float> negativeExamples = trainExamples.GetRows(trainSetPos.Length, trainExamples.Rows, 1);
        //    for (int i = 0; i < trainSetNeg.Length; i++)
        //    {
        //        FullFeatureObject ffo = IOUtils.FromXml<FullFeatureObject>(trainSetNeg[i]);
        //        NMFeatureObject nmObj = ffo.getNMFeatureObject();
        //        //nmObj.NormalizeL2();
        //        SVMFullFeatureExample.NMFeatureToMatrixSample(nmObj, ref negativeExamples, i);
        //        negativeExamples[i, 7] = ffo.colorDistERBBLab;
        //    }
        //    Matrix<int> trainClasses = new Matrix<int>(trainExamples.Rows, 1);
        //    Matrix<int> trainClassesPos = trainClasses.GetRows(0, positiveExamples.Rows, 1);
        //    trainClassesPos.SetValue(1);
        //    Matrix<int> trainClassesNeg = trainClasses.GetRows(positiveExamples.Rows, trainExamples.Rows, 1);
        //    trainClassesNeg.SetValue(2);
        //    //test examples and classes
        //    Matrix<float> testExamples = new Matrix<float>(testSetPos.Length + testSetNeg.Length, 8);
        //    Matrix<float> positiveTest = testExamples.GetRows(0, testSetPos.Length, 1);
        //    for (int i = 0; i < testSetPos.Length; i++)
        //    {
        //        FullFeatureObject ffo = IOUtils.FromXml<FullFeatureObject>(testSetPos[i]);
        //        NMFeatureObject nmObj = ffo.getNMFeatureObject();
        //        //nmObj.NormalizeL2();
        //        SVMFullFeatureExample.NMFeatureToMatrixSample(nmObj, ref positiveTest, i);
        //        positiveTest[i, 7] = ffo.colorDistERBBLab;
        //    }
        //    Matrix<float> negativeTest = testExamples.GetRows(testSetPos.Length, testExamples.Rows, 1);
        //    for (int i = 0; i < testSetNeg.Length; i++)
        //    {
        //        FullFeatureObject ffo = IOUtils.FromXml<FullFeatureObject>(testSetNeg[i]);
        //        NMFeatureObject nmObj = ffo.getNMFeatureObject();
        //        //nmObj.NormalizeL2();
        //        SVMFullFeatureExample.NMFeatureToMatrixSample(nmObj, ref negativeTest, i);
        //        negativeTest[i, 7] = ffo.colorDistERBBLab;
        //    }
        //    Matrix<int> testClasses = new Matrix<int>(testExamples.Rows, 1);
        //    Matrix<int> testClassesPos = testClasses.GetRows(0, positiveTest.Rows, 1);
        //    testClassesPos.SetValue(1);
        //    Matrix<int> testClassesNeg = testClasses.GetRows(positiveTest.Rows, testExamples.Rows, 1);
        //    testClassesNeg.SetValue(2);

        //    String path = "C:\\Users\\George\\Desktop\\trainedSVM\\SVM8Features4.xml";

        //    SvmUtils.CreateSaveTestSVM(path, trainExamples, testExamples, trainClasses, testClasses);
        //}
        //public void makeNMSVM(String[] trainSetPos, String[] testSetPos, String[] trainSetNeg, String[] testSetNeg)
        //{
        //    // train examples and classes
        //    Matrix<float> trainExamples = new Matrix<float>(trainSetPos.Length + trainSetNeg.Length, 7);
        //    Matrix<float> positiveExamples = trainExamples.GetRows(0, trainSetPos.Length, 1);
        //    for (int i = 0; i < trainSetPos.Length; i++)
        //    {
        //        FullFeatureObject ffo = IOUtils.FromXml<FullFeatureObject>(trainSetPos[i]);
        //        NMFeatureObject nmObj = ffo.getNMFeatureObject();
        //        SVMFullFeatureExample.NMFeatureToMatrixSample(nmObj, ref positiveExamples, i);
        //    }
        //    Matrix<float> negativeExamples = trainExamples.GetRows(trainSetPos.Length, trainExamples.Rows, 1);
        //    for (int i = 0; i < trainSetNeg.Length; i++)
        //    {
        //        FullFeatureObject ffo = IOUtils.FromXml<FullFeatureObject>(trainSetNeg[i]);
        //        NMFeatureObject nmObj = ffo.getNMFeatureObject();
        //        SVMFullFeatureExample.NMFeatureToMatrixSample(nmObj, ref negativeExamples, i);
        //    }
        //    Matrix<int> trainClasses = SVMCreator.DualTrainClasses(positiveExamples.Rows, negativeExamples.Rows);

        //    //test examples and classes
        //    Matrix<float> testExamples = new Matrix<float>(testSetPos.Length + testSetNeg.Length, 7);
        //    Matrix<float> positiveTest = testExamples.GetRows(0, testSetPos.Length, 1);
        //    for (int i = 0; i < testSetPos.Length; i++)
        //    {
        //        FullFeatureObject ffo = IOUtils.FromXml<FullFeatureObject>(testSetPos[i]);
        //        NMFeatureObject nmObj = ffo.getNMFeatureObject();
        //        SVMFullFeatureExample.NMFeatureToMatrixSample(nmObj, ref positiveTest, i);
        //    }
        //    Matrix<float> negativeTest = testExamples.GetRows(testSetPos.Length, testExamples.Rows, 1);
        //    for (int i = 0; i < testSetNeg.Length; i++)
        //    {
        //        FullFeatureObject ffo = IOUtils.FromXml<FullFeatureObject>(testSetNeg[i]);
        //        NMFeatureObject nmObj = ffo.getNMFeatureObject();
        //        SVMFullFeatureExample.NMFeatureToMatrixSample(nmObj, ref negativeTest, i);
        //    }

        //    Matrix<int> testClasses = SVMCreator.DualTrainClasses(positiveTest.Rows, negativeTest.Rows);

        //    String path = "C:\\Users\\George\\Desktop\\trainedSVM\\SVMnmFeatures3.xml";
        //    SvmUtils.CreateSaveTestSVM(path, trainExamples, testExamples, trainClasses, testClasses);

        //}

        //public void makeNMLabSVM(String[] trainSetPos, String[] testSetPos, String[] trainSetNeg, String[] testSetNeg)
        //{
        //    // train examples and classes
        //    Matrix<float> trainExamples = new Matrix<float>(trainSetPos.Length + trainSetNeg.Length, 8);
        //    Matrix<float> positiveExamples = trainExamples.GetRows(0, trainSetPos.Length, 1);
        //    for (int i = 0; i < trainSetPos.Length; i++)
        //    {
        //        FullFeatureObject ffo = IOUtils.FromXml<FullFeatureObject>(trainSetPos[i]);
        //        NMFeatureObject nmObj = ffo.getNMFeatureObject();
        //        SVMFullFeatureExample.NMFeatureToMatrixSample(nmObj, ref positiveExamples, i);
        //        positiveExamples[i, 7] = ffo.colorDistERBBLab;
        //    }
        //    Matrix<float> negativeExamples = trainExamples.GetRows(trainSetPos.Length, trainExamples.Rows, 1);
        //    for (int i = 0; i < trainSetNeg.Length; i++)
        //    {
        //        FullFeatureObject ffo = IOUtils.FromXml<FullFeatureObject>(trainSetNeg[i]);
        //        NMFeatureObject nmObj = ffo.getNMFeatureObject();
        //        SVMFullFeatureExample.NMFeatureToMatrixSample(nmObj, ref negativeExamples, i);
        //        negativeExamples[i, 7] = ffo.colorDistERBBLab;
        //    }
        //    Matrix<int> trainClasses = SVMCreator.DualTrainClasses(positiveExamples.Rows, trainExamples.Rows);

        //    //test examples and classes
        //    Matrix<float> testExamples = new Matrix<float>(testSetPos.Length + testSetNeg.Length, 8);
        //    Matrix<float> positiveTest = testExamples.GetRows(0, testSetPos.Length, 1);
        //    for (int i = 0; i < testSetPos.Length; i++)
        //    {
        //        FullFeatureObject ffo = IOUtils.FromXml<FullFeatureObject>(testSetPos[i]);
        //        NMFeatureObject nmObj = ffo.getNMFeatureObject();
        //        SVMFullFeatureExample.NMFeatureToMatrixSample(nmObj, ref positiveTest, i);
        //        positiveTest[i, 7] = ffo.colorDistERBBLab;
        //    }
        //    Matrix<float> negativeTest = testExamples.GetRows(testSetPos.Length, testExamples.Rows, 1);
        //    for (int i = 0; i < testSetNeg.Length; i++)
        //    {
        //        FullFeatureObject ffo = IOUtils.FromXml<FullFeatureObject>(testSetNeg[i]);
        //        NMFeatureObject nmObj = ffo.getNMFeatureObject();
        //        SVMFullFeatureExample.NMFeatureToMatrixSample(nmObj, ref negativeTest, i);
        //        negativeTest[i, 7] = ffo.colorDistERBBLab;
        //    }

        //    Matrix<int> testClasses = SVMCreator.DualTrainClasses(positiveTest.Rows, testExamples.Rows);

        //    String path = "C:\\Users\\George\\Desktop\\trainedSVM\\SVMnmLabFeatures.xml";
        //    SvmUtils.CreateSaveTestSVM(path, trainExamples, testExamples, trainClasses, testClasses);
        //}

        //public void makeNMStrokeSVM(String[] trainSetPos, String[] testSetPos, String[] trainSetNeg, String[] testSetNeg)
        //{
        //    // train examples and classes
        //    Matrix<float> trainExamples = new Matrix<float>(trainSetPos.Length + trainSetNeg.Length, 8);
        //    Matrix<float> positiveExamples = trainExamples.GetRows(0, trainSetPos.Length, 1);
        //    for (int i = 0; i < trainSetPos.Length; i++)
        //    {
        //        FullFeatureObject ffo = IOUtils.FromXml<FullFeatureObject>(trainSetPos[i]);
        //        NMFeatureObject nmObj = ffo.getNMFeatureObject();
        //        SVMFullFeatureExample.NMFeatureToMatrixSample(nmObj, ref positiveExamples, i);
        //        positiveExamples[i, 7] = ffo.significantPaths;
        //    }
        //    Matrix<float> negativeExamples = trainExamples.GetRows(trainSetPos.Length, trainExamples.Rows, 1);
        //    for (int i = 0; i < trainSetNeg.Length; i++)
        //    {
        //        FullFeatureObject ffo = IOUtils.FromXml<FullFeatureObject>(trainSetNeg[i]);
        //        NMFeatureObject nmObj = ffo.getNMFeatureObject();
        //        SVMFullFeatureExample.NMFeatureToMatrixSample(nmObj, ref negativeExamples, i);
        //        negativeExamples[i, 7] = ffo.significantPaths;
        //    }
        //    Matrix<int> trainClasses = SVMCreator.DualTrainClasses(positiveExamples.Rows, trainExamples.Rows);

        //    //test examples and classes
        //    Matrix<float> testExamples = new Matrix<float>(testSetPos.Length + testSetNeg.Length, 8);
        //    Matrix<float> positiveTest = testExamples.GetRows(0, testSetPos.Length, 1);
        //    for (int i = 0; i < testSetPos.Length; i++)
        //    {
        //        FullFeatureObject ffo = IOUtils.FromXml<FullFeatureObject>(testSetPos[i]);
        //        NMFeatureObject nmObj = ffo.getNMFeatureObject();
        //        SVMFullFeatureExample.NMFeatureToMatrixSample(nmObj, ref positiveTest, i);
        //        positiveTest[i, 7] = ffo.significantPaths;
        //    }
        //    Matrix<float> negativeTest = testExamples.GetRows(testSetPos.Length, testExamples.Rows, 1);
        //    for (int i = 0; i < testSetNeg.Length; i++)
        //    {
        //        FullFeatureObject ffo = IOUtils.FromXml<FullFeatureObject>(testSetNeg[i]);
        //        NMFeatureObject nmObj = ffo.getNMFeatureObject();
        //        SVMFullFeatureExample.NMFeatureToMatrixSample(nmObj, ref negativeTest, i);
        //        negativeTest[i, 7] = ffo.significantPaths;
        //    }

        //    Matrix<int> testClasses = SVMCreator.DualTrainClasses(positiveTest.Rows, testExamples.Rows);

        //    String path = "C:\\Users\\George\\Desktop\\trainedSVM\\SVMnmStrokeFeatures.xml";
        //    SvmUtils.CreateSaveTestSVM(path, trainExamples, testExamples, trainClasses, testClasses);
        //}

        //private static void NNTrain(VectorOfMat erMats)
        //{
        //    TrainForm tf = new TrainForm(erMats);
        //    var dialogResult = tf.ShowDialog();
        //    VectorOfMat noiseMats = new VectorOfMat(), letterMats = new VectorOfMat();
        //    if (dialogResult == System.Windows.Forms.DialogResult.OK)
        //    {
        //        for (int i = 0; i < tf.noiseMats.Size; i++)
        //            noiseMats.Push(tf.noiseMats[i].Clone());
        //        for (int i = 0; i < tf.letterMats.Size; i++)
        //            letterMats.Push(tf.letterMats[i].Clone());
        //        tf.Dispose();
        //    }
        //    IOUtils.AppendVectorOfByteMatToTrainFile(noiseMats, Constants.TRAINFILE_NOISE);
        //    IOUtils.AppendVectorOfByteMatToTrainFile(letterMats, Constants.TRAINFILE_LETTERS);
        //}
        #endregion
    }

    //private void ERPtrTest(MCvERStat stat)
    //{
    //    //VectorOfInt pixels = new VectorOfInt();


    //    unsafe
    //    {
    //        MCvERStat* parent = (MCvERStat*)stat.ParentPtr;
    //        //Mat m = new Mat(1000, 1, DepthType.Cv32S, 1, stat.Pixels, 4);
    //        //void* vectorPtr = stat.Pixels.ToPointer(); //dinei th dieythinsh ekkinhshs toy vector
    //        //void* vectorPtr2 = (void*) stat.Pixels; // to idio me panw
    //        //void* vectorPtr3 = &stat.Pixels; // th dieuthinsi toy antikeimenoy intptr stat.pixels (den kanei mallon)
    //        //int* vectorPtr4 = (int*)&stat.Pixels; // idio me to vectorptr3
    //        //int* ptr = (int*)stat.Pixels; //idio me to void mono poy thewrhtika mporei na kanei dereference argotera
    //        //Debug.WriteLine("new crossings");
    //        //int* identifier = (int*)stat.Crossings.ToPointer();              
    //        ////byte* ptr2 = (ptr + 8);
    //        ////int* ptr3 = (int*)ptr;            
    //        //Debug.WriteLine(" ");
    //        //for (int i = 0; i < stat.Rect.Height; i++ )
    //        //{
    //        //    Debug.WriteLine(" : " + *identifier);
    //        //    identifier++;
    //        //}
    //        //Debug.WriteLine(" ");             
    //        //Debug.WriteLine(" ");
    //        //identifier += sizeof(byte);
    //        //Debug.WriteLine(" ");
    //    }         
    //}
}
