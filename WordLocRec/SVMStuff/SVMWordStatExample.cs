﻿using Emgu.CV;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WordLocRec.DataStructs;
using WordLocRec.Grouping;

namespace WordLocRec.SVMStuff
{
    public class SVMWordStatExample: SVMExample
    {
        private Constants.FeatureType featureType;
        private WordStat standardMean, standardStd;
        private int columnOfSample = 0;

        public SVMWordStatExample() { }
        public SVMWordStatExample(Constants.FeatureType featureType)
        {
            this.featureType = featureType;
            this.featureLength = DecideFeatureLength(featureType);

            if (featureType.HasFlag(Constants.FeatureType.StandarizedPerFeature))
            {
                LoadMeanStdInDataset();
            }  
        }
        public override void LoadMeanStdInDataset()
        {
            standardMean = IOUtils.FromXml<WordStat>(Constants.MeanWSPath);
            standardStd = IOUtils.FromXml<WordStat>(Constants.StdWSPath);
        }
        public override void SetFeatureType(Constants.FeatureType feature)
        {
            base.SetFeatureType(feature);
            if (feature.HasFlag(Constants.FeatureType.StandarizedPerFeature))
            {
                LoadMeanStdInDataset();
            }
        }
        public override void SetRowSample(BaseFeature wordStat, ref Matrix<float> sample, int rowOfSample)
        {
            SetRowSample((WordStat)wordStat, ref sample, rowOfSample);
        }

        public void SetRowSample(WordStat wordStat, ref Matrix<float> sample, int rowOfSample)
        {
            if (featureType.HasFlag(Constants.FeatureType.StandarizedPerFeature))
                wordStat.Standardize(standardMean, standardStd);
            
            FloatFeatureToSample(wordStat.FGIntensitiesStd, ref sample, rowOfSample, columnOfSample++);
            FloatFeatureToSample(wordStat.BGIntensitiesStd, ref sample, rowOfSample, columnOfSample++);
            FloatFeatureToSample(wordStat.MajorAxisCoeffVariation, ref sample, rowOfSample, columnOfSample++);
            FloatFeatureToSample(wordStat.StrokeWidthsCoeffVariation, ref sample, rowOfSample, columnOfSample++);
            FloatFeatureToSample(wordStat.MeanGradientStd, ref sample, rowOfSample, columnOfSample++);
            FloatFeatureToSample(wordStat.AspectRatioCoeffVariation, ref sample, rowOfSample, columnOfSample++);
            FloatFeatureToSample(wordStat.HuMomentsAvgEuclideanDistance, ref sample, rowOfSample, columnOfSample++);
            FloatFeatureToSample(wordStat.ConvexHullCompactnessMean, ref sample, rowOfSample, columnOfSample++);
            FloatFeatureToSample(wordStat.ConvexHullCompactnessStd, ref sample, rowOfSample, columnOfSample++);
            FloatFeatureToSample(wordStat.ConvexityDefectsCoeffVariation, ref sample, rowOfSample, columnOfSample++);
            FloatFeatureToSample(wordStat.mstFeatures.AnglesMean, ref sample, rowOfSample, columnOfSample++);
            FloatFeatureToSample(wordStat.mstFeatures.AnglesStd, ref sample, rowOfSample, columnOfSample++);
            FloatFeatureToSample(wordStat.mstFeatures.EdgeWidthsCoeffVariation, ref sample, rowOfSample, columnOfSample++);
            FloatFeatureToSample(wordStat.mstFeatures.EdgeDistancesMeanDiametersMeanRatio, ref sample, rowOfSample, columnOfSample);

            columnOfSample = 0;
        }

        public override int DecideFeatureLength(Constants.FeatureType featureType)
        {
            return 14;
        }

        public static int DecideFeatureLengthStatic(Constants.FeatureType featureType)
        {
            return 14;
        }
    }
}
