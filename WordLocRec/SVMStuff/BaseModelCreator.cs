﻿using Emgu.CV;
using Emgu.CV.ML;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WordLocRec.DataStructs;
using WordLocRec.Grouping;

namespace WordLocRec.SVMStuff
{
    public abstract class BaseModelCreator
    {
        protected virtual Type typeOfModel { get; set; }
        public string ModelName { get; set; }

        public bool IsTrained { get; set; }
        public bool Auto = false;

        private String[] positiveTestSet, positiveTrainSet, negativeTestSet, negativeTrainSet;
        public Matrix<float> trainSet, testSet;
        public Matrix<int> trainClasses, testClasses;

        public IStatModel model;

        public float PercTrain { get; protected set; }
        public float PercTest { get; protected set; }

        public float Recall { get; set; }
        public float Precision { get; set; }
        public float HMean { get; set; }

        public Constants.FeatureType featureType;

        public bool OnlyForEvaluation { get; private set; }

        #region Constructors

        private BaseModelCreator(Constants.FeatureType f, bool onlyforeval)
        {
            this.featureType = f;
            OnlyForEvaluation = onlyforeval;
        }

        /// <summary>
        /// Used when the train and set datasets for the positive and the negative cases are already split up
        /// </summary>
        /// <param name="f"></param>
        /// <param name="positiveTestSet"></param>
        /// <param name="positiveTrainSet"></param>
        /// <param name="negativeTestSet"></param>
        /// <param name="negativeTrainSet"></param>
        public BaseModelCreator(Constants.FeatureType f, String[] positiveTestSet, String[] positiveTrainSet, String[] negativeTestSet, String[] negativeTrainSet) 
            : this(f, false)
        {
            this.positiveTestSet = positiveTestSet;
            this.positiveTrainSet = positiveTrainSet;
            this.negativeTestSet = negativeTestSet;
            this.negativeTrainSet = negativeTrainSet;
            
        }

        /// <summary>
        /// This constructor helps define the ratio of the train to test dataset. A usual value for the <paramref name="splitPercentage"/> is 0.2. If 0 is given then all the available data are used for training.
        /// </summary>
        /// <param name="f"></param>
        /// <param name="positiveNames"></param>
        /// <param name="negativeNames"></param>
        /// <param name="splitPercentage"></param>
        public BaseModelCreator(Constants.FeatureType f, String[] positiveNames, String[] negativeNames, int splitPercentage) 
            : this (f, false)
        {
            SetDatasetsExplicitly(positiveNames, negativeNames, splitPercentage);
        }

        /// <summary>
        /// This constructor is used explicitly when only some testing dataset is given to evaluate a classifier.
        /// </summary>
        /// <param name="f"></param>
        /// <param name="testNamesPositive"></param>
        /// <param name="testNamesNegative"></param>
        public BaseModelCreator(Constants.FeatureType f, String[] testNamesPositive, String[] testNamesNegative)
            : this(f, true)
        {
            positiveTestSet = testNamesPositive;
            positiveTrainSet = null;
            negativeTestSet = testNamesNegative;
            negativeTrainSet = null;
        }

        public void SetDatasetsExplicitly(String[] positiveNames, String[] negativeNames, int splitPercentage)
        {
            Utils.PickPercAtRandom<String>(positiveNames, out positiveTrainSet, out positiveTestSet, splitPercentage / 100f);
            Utils.PickPercAtRandom<String>(negativeNames, out negativeTrainSet, out negativeTestSet, splitPercentage / 100f);
        }
        #endregion

        #region Set up datasets and classes for test and/or train data

        public List<RectangleF> ResponsesToDataset<T>(String[] names) where T : BaseFeature
        {
            Matrix<float> dataset;
            MakeExamples<T>(names, new String[0], out dataset);

            Matrix<float> responses = new Matrix<float>(dataset.Rows, 1);
            model.Predict(dataset, responses); // gamhse to AutoTrain pros to paron

            List<T> l = IOUtils.LoadMultipleObjectsFromXML<T>(names);
            List<RectangleF> correctRects = new List<RectangleF>();
            for (int i = 0; i < responses.Rows; i++)
            {
                if (responses[i, 0] == 1 - (Auto ? 1 : 0))
                {
                    if (typeof(T).Equals(typeof(LineWordStat)))
                        correctRects.Add(l[i].wordRect);
                    else correctRects.Add(l[i].WordRect); // apisteuth sapila exw kanei edw. alla thelei na allaksw ola ta LineWordStat sthn eggrafh wordStat -> WordStat . . .
                }
            }
            return correctRects;
        }

        public void TrainClassifier()
        {
            TrainClassifier(model);
        }

        public void CreateTrainAndTestSetClasses<T>() where T : BaseFeature
        {
            Matrix<float> testSet;
            MakeExamples<T>(positiveTestSet, negativeTestSet, out testSet);
            this.testSet = testSet;
            testClasses = DualTrainClasses(positiveTestSet.Length, negativeTestSet.Length);

            if (OnlyForEvaluation)
            {
                this.trainSet = null; this.trainClasses = null;
            }
            else
            {
                Matrix<float> trainSet;
                MakeExamples<T>(positiveTrainSet, negativeTrainSet, out trainSet);
                this.trainSet = trainSet;
                trainClasses = DualTrainClasses(positiveTrainSet.Length, negativeTrainSet.Length);
            }

        }

        private void MakeExamples<T>(String[] positiveSet, String[] negativeSet, out Matrix<float> set) where T: BaseFeature
        {
            List<T> listPositive = IOUtils.LoadMultipleObjectsFromXML<T>(positiveSet);
            List<T> listNegative = IOUtils.LoadMultipleObjectsFromXML<T>(negativeSet);

            SVMExample svmExample;
            if (typeof(T).Equals(typeof(WordStat)))
            {
                svmExample = new SVMWordStatExample(featureType);
            }
            else if (typeof(T).Equals(typeof(SimpleWordStat)))
            {
                svmExample = new SVMSimpleWordStatExample(featureType);
            }
            else if (typeof(T).Equals(typeof(LineWordStat)))
            {
                svmExample = new SVMLineWordStatSample(featureType);
            }
            else
            {
                svmExample = new SVMFullFeatureExample(featureType);
            }

            int featureLength = svmExample.featureLength;

            set = new Matrix<float>(listPositive.Count + listNegative.Count, featureLength);
            for (int i = 0; i < listPositive.Count; i++)
            {
                svmExample.SetRowSample(listPositive[i], ref set, i);
            }
            for (int i = 0; i < listNegative.Count; i++)
            {
                svmExample.SetRowSample(listNegative[i], ref set, i + listPositive.Count);
            }
        }

        /// <summary>
        /// Assign a class value for the two possible classes, i.e. positive and negative. Works even if there are no positive or negative examples (used in OnlyForEvaluation cases only)
        /// </summary>
        /// <param name="positiveExamplesCount"></param>
        /// <param name="negativeExamplesCount"></param>
        /// <returns></returns>
        public Matrix<int> DualTrainClasses(int positiveExamplesCount, int negativeExamplesCount)
        {
            Matrix<int> classes = new Matrix<int>(positiveExamplesCount + negativeExamplesCount, 1);
            if (classes.Rows > 0)
            {
                if (positiveExamplesCount > 0)
                {
                    Matrix<int> classesPositive = classes.GetRows(0, positiveExamplesCount, 1);
                    classesPositive.SetValue(1);
                }
                if (negativeExamplesCount > 0)
                {
                    Matrix<int> trainClassesNeg = classes.GetRows(positiveExamplesCount, classes.Rows, 1);
                    trainClassesNeg.SetValue(2);
                }
                
            }
            return classes;
        }

        #endregion

        public virtual String ToString()
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendLine("PercRecall = " + Recall)
              .AppendLine("PercPrecision = " + Precision)
              .AppendLine("PercHMean = " + HMean);
            return sb.ToString();
        }

        #region abstract methods

        public abstract void Evaluate();
        public abstract void TrainClassifier(IStatModel model);
        public abstract void SaveClassifier(String path);
        public abstract void LoadClassifer(String path);

        #endregion
    }
}
