﻿using Emgu.CV;
using Emgu.CV.ML;
using Emgu.CV.Structure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WordLocRec.DataStructs;
using WordLocRec.Grouping;

namespace WordLocRec.SVMStuff
{
    public class SVMCreator
    {
        public String name = "";
        String[] positiveSet, negativeSet, trainSetPositive, testSetPositive, trainSetNegative, testSetNegative;
        public Matrix<float> trainSet { get; private set; }
        public Matrix<float> testSet { get; private set; }
        public Matrix<int> trainClasses { get; private set; }
        public Matrix<int> testClasses { get; private set; }

        public RTrees currentRTree { get; private set; }
        public DTrees currentMod { get; private set; }
        public SVM currentModel { get; private set; }
        public float percTrain { get; set; }
        public float percTest { get; set; }

        Constants.FeatureType featureType;

        public SVMCreator(){}
        /// <summary>
        /// Get the filenames of the positive and the negative set and the type of the features to use for the classifier
        /// </summary>
        /// <param name="positiveSet"></param>
        /// <param name="negativeSet"></param>
        /// <param name="featureType"></param>
        public SVMCreator(String[] positiveSet, String[] negativeSet, Constants.FeatureType featureType)
        {
            this.positiveSet = positiveSet;
            this.negativeSet = negativeSet;
            this.featureType = featureType;
            percTrain = 0;
            percTest = 0;

            Utils.PickPercAtRandom<String>(positiveSet, out trainSetPositive, out testSetPositive);
            Utils.PickPercAtRandom<String>(negativeSet, out trainSetNegative, out testSetNegative);
        }
        public SVMCreator(String[] trainSetPositive, String[] testSetPositive, String[] trainSetNegative, String[] testSetNegative, Constants.FeatureType featureType)
        {
            this.featureType = featureType;
            this.trainSetPositive = trainSetPositive;
            this.testSetPositive = testSetPositive;
            this.trainSetNegative = trainSetNegative;
            this.testSetNegative = testSetNegative;
        }

        public SVM MakeSVMAuto(int termIterations, bool useIterations = true)
        {
            SVM model = new SVM();
            model.SetKernel(SVM.SvmKernelType.Rbf);
            model.Type = SVM.SvmType.CSvc;
            model.C = 2;
            model.Gamma = 0.01;
            model.Coef0 = 0;
            model.Nu = 0.1;
            model.Degree = 10;
            if (useIterations)
                model.TermCriteria = new MCvTermCriteria(termIterations, 10e-8);
            else
                model.TermCriteria = new MCvTermCriteria(100, 10e-8);

            // train the svm model
            TrainData td = new TrainData(trainSet, Emgu.CV.ML.MlEnum.DataLayoutType.RowSample, trainClasses);
            bool trained = model.TrainAuto(td);

            this.currentModel = model;
            return model;
        }
        public DTrees MakeDTreesAuto()
        {
            DTrees model = new DTrees();
            model.MaxDepth = 8;
            model.MinSampleCount = 2;
            model.UseSurrogates = false;
            model.CVFolds = 0;
            model.Use1SERule = false;
            model.TruncatePrunedTree = false;
            TrainData td = new TrainData(trainSet, Emgu.CV.ML.MlEnum.DataLayoutType.RowSample, trainClasses);

            model.Train(td);

            this.currentMod = model;
            return model;
        }
        public RTrees MakeRtreesAuto()
        {
            RTrees model = new RTrees();
            model.ActiveVarCount = (int)Math.Truncate(Math.Sqrt(trainSet.Cols));
            model.CalculateVarImportance = true;
            model.CVFolds = 0;
            model.MaxDepth = 8;
            model.TermCriteria = new MCvTermCriteria(20, float.Epsilon);
            model.TruncatePrunedTree = false;
            model.Use1SERule = false;
            model.UseSurrogates = false;

            TrainData td = new TrainData(trainSet, Emgu.CV.ML.MlEnum.DataLayoutType.RowSample, trainClasses);

            model.Train(td);

            this.currentRTree = model;
            return model;
        }

        public float[] Evaluate<T>() where T : IStatModel
        {
            if (typeof(T).Equals(typeof(SVM)))
                return Evaluate(currentModel);
            else if (typeof(T).Equals(typeof(RTrees)))
                return Evaluate(currentRTree);
            else return Evaluate(currentMod);
        }
        public float[] Evaluate(RTrees model)
        {
            float[] percentages = new float[2];
            percentages[0] = SvmUtils.EvaluateSVMModel(model, trainSet, trainClasses);
            percentages[1] = SvmUtils.EvaluateSVMModel(model, testSet, testClasses);
            percTrain = percentages[0];
            percTest = percentages[1];

            return percentages;
        }
        public float[] Evaluate(DTrees model)
        {
            float[] percentages = new float[2];

            percentages[0] = SvmUtils.EvaluateSVMModel(model, trainSet, trainClasses);
            percentages[1] = SvmUtils.EvaluateSVMModel(model, testSet, testClasses);
            percTrain = percentages[0];
            percTest = percentages[1];

            return percentages;
        }
        public float[] Evaluate(SVM model)
        {
            float[] percentages = new float[2];

            percentages[0] = SvmUtils.EvaluateSVMModel(model, trainSet, trainClasses);
            percentages[1] = SvmUtils.EvaluateSVMModel(model, testSet, testClasses);
            percTrain = percentages[0];
            percTest = percentages[1];

            return percentages;
        }
        public float EvaluateSimple(SVM model)
        {
            float perc = SvmUtils.EvaluateSVMModel(model, testSet, testClasses);
            percTest = perc;
            return perc;
        }

        public Matrix<float> GetSupportVectors()
        {
            Matrix<float> vectors = new Matrix<float>(new float[,]{{0}});
            if (currentModel != null) 
            {
                var supvec = currentModel.GetSupportVectors();
                vectors = new Matrix<float>(supvec.Rows, supvec.Cols, supvec.DataPointer);
            }
            return vectors;
        }
        public int GetSupportVectorsCount()
        {
            return GetSupportVectors().Rows;
        }

        public void CreateTrainTestSetAndClasses<T>() where T: BaseFeature
        {
            Matrix<float> trainSet;
            MakeExamples<T>(trainSetPositive, trainSetNegative, featureType, out trainSet);
            this.trainSet = trainSet;
            trainClasses = DualTrainClasses(trainSetPositive.Length, trainSetNegative.Length);

            Matrix<float> testSet;
            MakeExamples<T>(testSetPositive, testSetNegative, featureType, out testSet);
            this.testSet = testSet;
            testClasses = DualTrainClasses(testSetPositive.Length, testSetNegative.Length);
        }

        public void CreateTrainTestSetAndClasses()
        {
            Matrix<float> trainSet;
            MakeExamples(trainSetPositive, trainSetNegative, featureType, out trainSet);
            this.trainSet = trainSet;
            trainClasses = DualTrainClasses(trainSetPositive.Length, trainSetNegative.Length);

            Matrix<float> testSet;
            MakeExamples(testSetPositive, testSetNegative, featureType, out testSet);
            this.testSet = testSet;
            testClasses = DualTrainClasses(testSetPositive.Length, testSetNegative.Length);
        }
        public void CreateTestSetAndClasses()
        {
            Matrix<float> dataset;
            MakeExamplesSimple(positiveSet, negativeSet, featureType, out dataset);
            this.testSet = dataset;
            testClasses = DualTrainClasses(positiveSet.Length, negativeSet.Length);
        }

        private void MakeExamples<T>(String[] positiveSet, String[] negativeSet, Constants.FeatureType featureType, out Matrix<float> set) where T : BaseFeature
        {
            List<T> listPositive = IOUtils.LoadMultipleObjectsFromXML<T>(positiveSet);
            List<T> listNegative = IOUtils.LoadMultipleObjectsFromXML<T>(negativeSet);

            SVMExample svmExample;
            if (typeof(T).Equals(typeof(WordStat)))
            {
                svmExample = new SVMWordStatExample(featureType);

            }
            else if (typeof(T).Equals(typeof(SimpleWordStat)))
            {
                svmExample = new SVMSimpleWordStatExample(featureType);
            }
            else if (typeof(T).Equals(typeof(LineWordStat)))
            {
                svmExample = new SVMLineWordStatSample(featureType);
            }
            else
            {
                svmExample = new SVMFullFeatureExample(featureType);
            }

            int featureLength = svmExample.featureLength;

            set = new Matrix<float>(listPositive.Count + listNegative.Count, featureLength);
            for (int i = 0; i < listPositive.Count; i++)
            {
                svmExample.SetRowSample(listPositive[i], ref set, i);
            }
            for (int i = 0; i < listNegative.Count; i++)
            {
                svmExample.SetRowSample(listNegative[i], ref set, i + listPositive.Count);
            }
        }

        /// <summary>
        /// Loads the data from files and uses the feature type to create the appropriate example type
        /// </summary>
        /// <param name="positiveSet"></param>
        /// <param name="negativeSet"></param>
        /// <param name="featureType"></param>
        /// <param name="set"></param>
        private void MakeExamples(String[] positiveSet, String[] negativeSet, Constants.FeatureType featureType, out Matrix<float> set)
        {
            List<FullFeatureObject> listPositive = FullFeatureObject.LoadFullFeatureObjects(positiveSet);
            List<FullFeatureObject> listNegative = FullFeatureObject.LoadFullFeatureObjects(negativeSet);

            int featureLength = SVMFullFeatureExample.DecideFeatureLengthStatic(featureType);

            SVMFullFeatureExample svmExample = new SVMFullFeatureExample(featureType);

            set = new Matrix<float>(listPositive.Count + listNegative.Count, featureLength);
            for (int i = 0; i < listPositive.Count; i++)
            {
                svmExample.SetRowSample(listPositive[i], ref set, i);
            }
            for (int i = 0; i < listNegative.Count; i++)
            {
                svmExample.SetRowSample(listNegative[i], ref set, i + listPositive.Count);
            }
        }
        private void MakeExamplesSimple(String[] positiveSet, String[] negativeSet, Constants.FeatureType featureType, out Matrix<float> set)
        {
            String[] fullDataset = new String[positiveSet.Length + negativeSet.Length];
            for (int i = 0; i < positiveSet.Length; i++)
                fullDataset[i] = positiveSet[i];
            for (int i = 0; i < negativeSet.Length; i++)
                fullDataset[positiveSet.Length + i] = negativeSet[i];

            List<FullFeatureObject> listDataset = FullFeatureObject.LoadFullFeatureObjects(fullDataset);

            int featureLength = SVMFullFeatureExample.DecideFeatureLengthStatic(featureType);

            SVMFullFeatureExample svmExample = new SVMFullFeatureExample(featureType);

            set = new Matrix<float>(fullDataset.Length, featureLength);

            for (int i = 0; i < listDataset.Count; i++)
            {
                svmExample.SetRowSample(listDataset[i], ref set, i);
            }
        }

        /// <summary>
        /// Creates a <c>Matrix"int"</c> object that defines class "1" for the first <paramref name="positiveExamplesCount"/> and class "2" for the rest examples (equal to <paramref name="negativeExamplesCount"/>).
        /// </summary>
        /// <param name="positiveExamplesCount">Number of positive examples</param>
        /// <param name="negativeExamplesCount">Total number of examples</param>
        /// <returns>A Matrix"int" object</returns>
        public static Matrix<int> DualTrainClasses(int positiveExamplesCount, int negativeExamplesCount)
        {
            Matrix<int> trainClasses = new Matrix<int>(positiveExamplesCount + negativeExamplesCount, 1);
            if (trainClasses.Rows > 0)
            {
                Matrix<int> trainClassesPos = trainClasses.GetRows(0, positiveExamplesCount, 1);
                trainClassesPos.SetValue(1);
                Matrix<int> trainClassesNeg = trainClasses.GetRows(positiveExamplesCount, trainClasses.Rows, 1);
                trainClassesNeg.SetValue(2); 
            }
            return trainClasses;
        }

        public string ToString(bool addSVMDetails)
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendLine();
            sb.AppendLine(" Training % = " + (percTrain * 100))
              .AppendLine(" Test % = " + (percTest * 100))
              .AppendLine(" Support Vectors = " + GetSupportVectorsCount());
            sb.AppendLine();
            if (addSVMDetails)
            {
                sb.AppendLine("C = " + currentModel.C)
                  .AppendLine("Gamma = " + currentModel.Gamma)
                  .AppendLine("Termination Criteria: Iters = " + currentModel.TermCriteria.MaxIter);
            }
            sb.AppendLine("****End model****");

            return sb.ToString();
        }
        public string ToString(string name, string exampleType)
        {
            StringBuilder sb = new StringBuilder();
            int columnLength = 20;

            sb.Append(IOUtils.CreateColumn(name, 2 *columnLength)); // mono gia to name valto 40 na xwraei

            sb.Append(IOUtils.CreateColumn(featureType.ToString(), columnLength));

            sb.Append(IOUtils.CreateColumn(exampleType, columnLength));

            sb.Append(IOUtils.CreateColumn((percTrain * 100).ToString(), columnLength));
            sb.Append(IOUtils.CreateColumn((percTest * 100).ToString(), columnLength));
            sb.Append(IOUtils.CreateColumn(GetSupportVectorsCount().ToString(), columnLength));
            sb.Append(IOUtils.CreateColumn(currentModel.C.ToString(), columnLength));
            sb.Append(IOUtils.CreateColumn(currentModel.Gamma.ToString(), columnLength));
            sb.Append(IOUtils.CreateColumn(currentModel.TermCriteria.MaxIter.ToString(), columnLength));
            sb.AppendLine();

            return sb.ToString();
        }

        public void SetCurrentDTree(DTrees ext)
        {
            currentMod = ext;
        }

        public void SetCurrentSVM(SVM externalModel)
        {
            //currentMod = externalModel;
            currentModel = externalModel;
        }
        public void SetCurrentRTree(RTrees rTree)
        {
            currentRTree = rTree;
        }
    }
}
