﻿using Emgu.CV;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WordLocRec.DataStructs;
using WordLocRec.Grouping;

namespace WordLocRec.SVMStuff
{
    class SVMLineWordStatSample : SVMExample
    {
        private LineWordStat standardMean, standardStd;
        private int columnOfSample = 0;

        public SVMLineWordStatSample() { }
        public SVMLineWordStatSample(Constants.FeatureType featureType)
        {
            this.feature = featureType;
            this.featureLength = DecideFeatureLength(feature);

            if (featureType.HasFlag(Constants.FeatureType.StandarizedPerFeature))
            {
                LoadMeanStdInDataset();
            }
        }
        public override void LoadMeanStdInDataset()
        {
            standardMean = IOUtils.FromXml<LineWordStat>(Constants.MeanLWSPath);
            standardStd = IOUtils.FromXml<LineWordStat>(Constants.StdLWSPath);
        }
        public override void SetFeatureType(Constants.FeatureType feature)
        {
            base.SetFeatureType(feature);
            if (feature.HasFlag(Constants.FeatureType.StandarizedPerFeature))
            {
                LoadMeanStdInDataset();
            }
        }

        public override void SetRowSample(BaseFeature wordStat, ref Matrix<float> sample, int rowOfSample)
        {
            SetRowSample((LineWordStat)wordStat, ref sample, rowOfSample);
        }

        public void SetRowSample(LineWordStat wordStat, ref Matrix<float> sample, int rowOfSample)
        {
            if (feature.HasFlag(Constants.FeatureType.StandarizedPerFeature))
                wordStat.Standardize(standardMean, standardStd);

			FloatFeatureToSample(wordStat.MeanStrokeWidthCoV, ref sample, rowOfSample, columnOfSample++);
			FloatFeatureToSample(wordStat.MeanLabFGBGDistanceCoV, ref sample, rowOfSample, columnOfSample++);
			FloatFeatureToSample(wordStat.LabConsistencyAmongERs, ref sample, rowOfSample, columnOfSample++);
			FloatFeatureToSample(wordStat.MaxVerticalError, ref sample, rowOfSample, columnOfSample++);
			FloatFeatureToSample(wordStat.WidthCoV, ref sample, rowOfSample, columnOfSample++);
			FloatFeatureToSample(wordStat.PercentageHorizontalOverlap, ref sample, rowOfSample, columnOfSample++);
			FloatFeatureToSample(wordStat.PercentageUncovered, ref sample, rowOfSample, columnOfSample++);
			FloatFeatureToSample(wordStat.AvgHuDistEucl, ref sample, rowOfSample, columnOfSample++);
		
            columnOfSample = 0;
        }

        public override int DecideFeatureLength(Constants.FeatureType featureType)
        {
            return DecideFeatureLengthStatic(featureType);
        }

        public static int DecideFeatureLengthStatic(Constants.FeatureType featureType)
        {
            return 8;
        }
    }
}
