﻿using Emgu.CV;
using Emgu.CV.ML;
using Emgu.CV.Structure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WordLocRec.DataStructs;
using WordLocRec.Grouping;

namespace WordLocRec.SVMStuff
{
    public class SVMCreatorOneClass
    {
        String[] dataset, trainDataset, testDataset;

        public Matrix<float> trainSet { get; private set; }
        public Matrix<float> testSet { get; private set; }
        public Matrix<int> trainClasses { get; private set; }
        public Matrix<int> testClasses { get; private set; }
        public SVM currentModel { get; private set; }
        public float percTrain { get; private set; }
        public float percTest { get; private set; }
       
        Constants.FeatureType featureType;

        public SVMCreatorOneClass(String[] dataset, Constants.FeatureType featureType)
        {
            this.dataset = dataset;
            this.featureType = featureType;
            percTrain = 0;
            percTest = 0;

            Utils.PickPercAtRandom<String>(this.dataset, out trainDataset, out testDataset);
        }
        private void MakeExamples<T>(String[] data, Constants.FeatureType featureType, out Matrix<float> set) where T : BaseFeature
        {
            List<T> listData = IOUtils.LoadMultipleObjectsFromXML<T>(data);

            SVMExample svmExample;
            if (typeof(T).Equals(typeof(WordStat)))
                svmExample = new SVMWordStatExample(featureType);
            else if (typeof(T).Equals(typeof(SimpleWordStat)))
                svmExample = new SVMSimpleWordStatExample(featureType);           
            else
                svmExample = new SVMFullFeatureExample(featureType);

            int featureLength = svmExample.featureLength;

            set = new Matrix<float>(listData.Count, featureLength);
            for (int i = 0; i < listData.Count; i++)
            {
                svmExample.SetRowSample(listData[i], ref set, i);
            }
        }
        public void CreateTrainTestSetAndClasses<T>() where T : BaseFeature
        {
            Matrix<float> trainSet;
            MakeExamples<T>(trainDataset, featureType, out trainSet);
            this.trainSet = trainSet;
            trainClasses = new Matrix<int>(new int[trainDataset.Length]);

            Matrix<float> testSet;
            MakeExamples<T>(testDataset, featureType, out testSet);
            this.testSet = testSet;
            testClasses = new Matrix<int>(new int[testDataset.Length]);
        }

        public SVM MakeSvmAuto(int termIterations, bool useIterations = true)
        {
            SVM model = new SVM();
            model.SetKernel(SVM.SvmKernelType.Rbf);
            model.Type = SVM.SvmType.OneClass;
            model.Nu = 0.1;
            if (useIterations)
                model.TermCriteria = new MCvTermCriteria(termIterations, 10e-8);
            else
                model.TermCriteria = new MCvTermCriteria(100000, 10e-8);

            // train the svm model
            TrainData td = new TrainData(trainSet, Emgu.CV.ML.MlEnum.DataLayoutType.RowSample, trainClasses);
            bool trained = model.TrainAuto(td);

            this.currentModel = model;
            return model;
        }
        public string ToString(bool addSVMDetails)
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendLine();
            sb.AppendLine(" Training % = " + (percTrain * 100))
              .AppendLine(" Test % = " + (percTest * 100))
              .AppendLine(" Support Vectors = " + GetSupportVectorsCount());
            sb.AppendLine();
            if (addSVMDetails)
            {
                sb.AppendLine("C = " + currentModel.C)
                  .AppendLine("Gamma = " + currentModel.Gamma)
                  .AppendLine("Termination Criteria: Iters = " + currentModel.TermCriteria.MaxIter);
            }
            sb.AppendLine("****End model****");

            return sb.ToString();
        }

        public void SetCurrentModel(SVM externalModel)
        {
            currentModel = externalModel;
        }

        public Matrix<float> GetSupportVectors()
        {
            var supvec = currentModel.GetSupportVectors();
            Matrix<float> vectors = new Matrix<float>(supvec.Rows, supvec.Cols, supvec.DataPointer);

            return vectors;
        }
        public int GetSupportVectorsCount()
        {
            return GetSupportVectors().Rows;
        }

        public float[] Evaluate()
        {
            return Evaluate(currentModel);
        }
        public float[] Evaluate(SVM model)
        {
            float[] percentages = new float[2];

            percentages[0] = SvmUtils.EvaluateSVMModel(model, trainSet, trainClasses);
            percentages[1] = SvmUtils.EvaluateSVMModel(model, testSet, testClasses);
            percTrain = percentages[0];
            percTest = percentages[1];

            return percentages;
        }
    }
}
