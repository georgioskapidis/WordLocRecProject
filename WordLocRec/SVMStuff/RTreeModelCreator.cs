﻿using Emgu.CV;
using Emgu.CV.ML;
using Emgu.CV.Structure;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WordLocRec.DataStructs;

namespace WordLocRec.SVMStuff
{
    public class RTreeModelCreator : BaseModelCreator
    {
         #region Constructors
        public RTreeModelCreator(Constants.FeatureType f, String[] positiveTestSet, String[] positiveTrainSet, String[] negativeTestSet, String[] negativeTrainSet) 
            : base(f, positiveTestSet, positiveTrainSet, negativeTestSet, negativeTrainSet)
        { typeOfModel = typeof(RTrees); }

        public RTreeModelCreator(Constants.FeatureType f, String[] positiveNames, String[] negativeNames, int splitPercentage) 
            : base(f, positiveNames, negativeNames, splitPercentage)
        { typeOfModel = typeof(RTrees); }

        public RTreeModelCreator(Constants.FeatureType f, String[] testNamesPositive, String[] testNamesNegative)
            : base(f, testNamesPositive, testNamesNegative)
        { typeOfModel = typeof(RTrees); }

        #endregion

        #region Overrides
        public override void Evaluate()
        {
            if (OnlyForEvaluation)
            {
                PercTrain = 0; // unneeded
            }
            else
                PercTrain = SvmUtils.EvaluateSVMModel((RTrees)this.model, trainSet, trainClasses);

            PercTest = SvmUtils.EvaluateSVMModel((RTrees)this.model, testSet, testClasses);
        }
        public override void TrainClassifier(IStatModel model)
        {
            if (model.GetType() != typeOfModel) throw new Exception("Can't train RTree");

            TrainData td = new TrainData(trainSet, Emgu.CV.ML.MlEnum.DataLayoutType.RowSample, trainClasses);
            IsTrained = ((RTrees)model).Train(td);

            this.model = model;
        }
        public override void SaveClassifier(String path)
        {
            if (OnlyForEvaluation) return;
            if (model == null) return;
            if (File.Exists(path)) File.Delete(path);
            FileStorage fs = new FileStorage(path, FileStorage.Mode.Write);
            ((RTrees)model).Write(fs);
            fs.ReleaseAndGetString();
            SvmUtils.FixXML(path);
        }
        public override void LoadClassifer(string path)
        {
            RTrees rTrees = new RTrees();
            FileStorage fs = new FileStorage(path, FileStorage.Mode.Read);
            rTrees.Read(fs.GetRoot());
            fs.ReleaseAndGetString();

            this.model = rTrees;
        }
        public override string ToString()
        {
            if (!IsTrained) return "Random forest not trained";

            StringBuilder sb = new StringBuilder();
            if (Recall != 0 && Precision != 0 && HMean != 0) sb.AppendLine(base.ToString());
            sb.AppendLine()
              .AppendLine("Random forest ")
              .AppendLine(ModelName)
              .AppendLine("Active var count: " + ((RTrees)model).ActiveVarCount)
              .AppendLine("Number of cv folds: " + ((RTrees)model).CVFolds)
              .AppendLine("Max depth of tree: " + ((RTrees)model).MaxDepth)
              .AppendLine("Number of max iterations during training: " + ((RTrees)model).TermCriteria.MaxIter)
              .AppendLine("Truncate pruned tree: " + ((RTrees)model).TruncatePrunedTree)
              .AppendLine("Use 1SE Rule: " + ((RTrees)model).Use1SERule)
              .AppendLine(" Training % = " + (PercTrain * 100))
              .AppendLine(" Test % = " + (PercTest * 100))
              .AppendLine()
              .AppendLine("****End model****");

            return sb.ToString();
        }
        #endregion

        #region Remaining methods
        public string ToString(string name, string exampleType, int columnLength)
        {
            StringBuilder sb = new StringBuilder();

            sb.Append(IOUtils.CreateColumn(name, 2 * columnLength)); // mono gia to name valto 40 na xwraei

            sb.Append(IOUtils.CreateColumn(featureType.ToString(), columnLength));

            sb.Append(IOUtils.CreateColumn(exampleType, columnLength));

            sb.Append(IOUtils.CreateColumn((PercTrain * 100).ToString(), columnLength));
            sb.Append(IOUtils.CreateColumn((PercTest * 100).ToString(), columnLength));

            sb.AppendLine();

            return sb.ToString();
        }

        public RTrees CreateModel(int maxDepth, int minSampleCount, int CVFolds, int activeVarCount, bool calcVarImportance, bool use1SERule, bool truncatePrunedTree, MCvTermCriteria termCriteria, int maxCategories = 0)
        {
            RTrees model = new RTrees();

            model.ActiveVarCount = activeVarCount;
            model.CalculateVarImportance = calcVarImportance;
            model.CVFolds = CVFolds;
            model.MaxDepth = maxDepth;
            model.MaxCategories = maxCategories;
            model.MinSampleCount = minSampleCount;
            model.TermCriteria = termCriteria;
            model.TruncatePrunedTree = truncatePrunedTree;
            model.Use1SERule = use1SERule;
            model.UseSurrogates = false;

            return model;
        }

        #endregion
    }
}
