﻿using Emgu.CV;
using Emgu.CV.ML;
using Emgu.CV.Structure;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WordLocRec.DataStructs;

namespace WordLocRec.SVMStuff
{
    public class SVMModelCreator : BaseModelCreator
    {
        #region Constructors

        public SVMModelCreator(Constants.FeatureType f, String[] positiveTestSet, String[] positiveTrainSet, String[] negativeTestSet, String[] negativeTrainSet, bool TrainAuto) 
            : base(f, positiveTestSet, positiveTrainSet, negativeTestSet, negativeTrainSet)
        { 
            typeOfModel = typeof(SVM); 
            Auto = TrainAuto;
        }

        public SVMModelCreator(Constants.FeatureType f, String[] positiveNames, String[] negativeNames, int splitPercentage, bool TrainAuto) 
            : base(f, positiveNames, negativeNames, splitPercentage)
        { 
            typeOfModel = typeof(SVM); 
            Auto = TrainAuto;
        }

        public SVMModelCreator(Constants.FeatureType f, String[] testNamesPositive, String[] testNamesNegative)
            : base(f, testNamesPositive, testNamesNegative)
        { 
            typeOfModel = typeof(SVM);
        }

        #endregion

        #region Overrides

        public override void Evaluate()
        {
            if (OnlyForEvaluation)
            {
                PercTrain = 0; // unneeded
            }
            else
                PercTrain = SvmUtils.EvaluateSVMModel((SVM)this.model, trainSet, trainClasses);

            PercTest = SvmUtils.EvaluateSVMModel((SVM)this.model, testSet, testClasses);
        }

        public override void TrainClassifier(IStatModel model)
        {
            if (OnlyForEvaluation) return;
            if (model.GetType() != typeOfModel) throw new Exception("Can't train SVM");
            
            TrainData td = new TrainData(trainSet, Emgu.CV.ML.MlEnum.DataLayoutType.RowSample, trainClasses);
            if (Auto) IsTrained = ((SVM)model).TrainAuto(td);
            else IsTrained = ((SVM)model).Train(td);

            this.model = model;
        }

        public override void SaveClassifier(String path)
        {
            if (OnlyForEvaluation) return;
            if (model == null) return;
            if (File.Exists(path)) File.Delete(path);
            FileStorage fs = new FileStorage(path, FileStorage.Mode.Write);
            ((SVM)model).Write(fs);
            fs.ReleaseAndGetString();
        }

        public override void LoadClassifer(string path)
        {
            SVM svm = new SVM();
            FileStorage fs = new FileStorage(path, FileStorage.Mode.Read);
            svm.Read(fs.GetRoot());
            fs.ReleaseAndGetString();

            this.model = svm;
        }

        public override string ToString()
        {
            if (!IsTrained) return "SVM not trained";
            StringBuilder sb = new StringBuilder();
            if (Recall != 0 && Precision != 0 && HMean != 0) sb.AppendLine(base.ToString());
            sb.AppendLine()
              .AppendLine(" SVM ")
              .AppendLine(" Training % = " + (PercTrain * 100))
              .AppendLine(" Test % = " + (PercTest * 100))
              .AppendLine()
              .AppendLine(" Support Vectors = " + GetSupportVectors().Rows)
              .AppendLine("C = " + ((SVM)model).C)
              .AppendLine("Gamma = " + ((SVM)model).Gamma)
              .AppendLine("Nu = " + ((SVM)model).Nu)
              .AppendLine("Coef = " + ((SVM)model).Coef0)
              .AppendLine("Degree = " + ((SVM)model).Degree)
              .AppendLine("SVM type = " + ((SVM)model).Type)
              .AppendLine("Kernel type = " + ((SVM)model).KernelType)
              .AppendLine("Termination Criteria: Iters = " + ((SVM)model).TermCriteria.MaxIter)
              .AppendLine("****End model****");

            return sb.ToString();
        }

        #endregion

        #region Remaining methods

        public string ToString(string name, string exampleType, int columnLength)
        {
            StringBuilder sb = new StringBuilder();

            sb.Append(IOUtils.CreateColumn(name, 2 * columnLength)); // mono gia to name valto 40 na xwraei

            sb.Append(IOUtils.CreateColumn(featureType.ToString(), columnLength));

            sb.Append(IOUtils.CreateColumn(exampleType, columnLength));

            sb.Append(IOUtils.CreateColumn((PercTrain * 100).ToString(), columnLength));
            sb.Append(IOUtils.CreateColumn((PercTest * 100).ToString(), columnLength));
            sb.Append(IOUtils.CreateColumn(GetSupportVectors().Rows.ToString(), columnLength));
            sb.Append(IOUtils.CreateColumn(((SVM)model).C.ToString(), columnLength));
            sb.Append(IOUtils.CreateColumn(((SVM)model).Gamma.ToString(), columnLength));
            sb.Append(IOUtils.CreateColumn(((SVM)model).TermCriteria.MaxIter.ToString(), columnLength));
            sb.AppendLine();

            return sb.ToString();
        }

        public Matrix<float> GetSupportVectors()
        {
            Matrix<float> vectors = new Matrix<float>(new float[,] { { 0 } });
            if (model != null)
            {
                var supvec = ((SVM)model).GetSupportVectors();
                vectors = new Matrix<float>(supvec.Rows, supvec.Cols, supvec.DataPointer);
            }
            return vectors;
        }

        public SVM CreateModel(SVM.SvmType svmType, SVM.SvmKernelType kernelType, double C, double Gamma, MCvTermCriteria termCriteria, double Coeff = 0, double nu = 0.1, double degree = 10)
        {
            SVM model = new SVM();
            model.Type = svmType;
            model.SetKernel(kernelType);
            model.C = C;
            model.Gamma = Gamma;
            model.Coef0 = Coeff;
            model.Nu = nu;
            model.Degree = degree;
            model.TermCriteria = termCriteria;

            return model;
        }

        #endregion
    }
}
