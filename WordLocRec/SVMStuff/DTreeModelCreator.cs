﻿using Emgu.CV;
using Emgu.CV.ML;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WordLocRec.DataStructs;

namespace WordLocRec.SVMStuff
{
    public class DTreeModelCreator : BaseModelCreator
    {
        #region Constructors
        public DTreeModelCreator(Constants.FeatureType f, String[] positiveTestSet, String[] positiveTrainSet, String[] negativeTestSet, String[] negativeTrainSet) 
            : base(f, positiveTestSet, positiveTrainSet, negativeTestSet, negativeTrainSet)
        { typeOfModel = typeof(DTrees); }

        public DTreeModelCreator(Constants.FeatureType f, String[] positiveNames, String[] negativeNames, int splitPercentage) 
            : base(f, positiveNames, negativeNames, splitPercentage)
        { typeOfModel = typeof(DTrees); }

        public DTreeModelCreator(Constants.FeatureType f, String[] testNamesPositive, String[] testNamesNegative)
            : base(f, testNamesPositive, testNamesNegative)
        { typeOfModel = typeof(DTrees); }

        #endregion

        #region Overrides
        public override void Evaluate()
        {
            if (OnlyForEvaluation)
            {
                PercTrain = 0; // unneeded
            }
            else
                PercTrain = SvmUtils.EvaluateSVMModel((DTrees)this.model, trainSet, trainClasses);

            PercTest = SvmUtils.EvaluateSVMModel((DTrees)this.model, testSet, testClasses);
        }
        public override void TrainClassifier(IStatModel model)
        {
            if (model.GetType() != typeOfModel) throw new Exception("Can't train DTree");

            TrainData td = new TrainData(trainSet, Emgu.CV.ML.MlEnum.DataLayoutType.RowSample, trainClasses);
            IsTrained = ((DTrees)model).Train(td);

            this.model = model;
        }
        public override void SaveClassifier(String path)
        {
            if (OnlyForEvaluation) return;
            if (model == null) return;
            if (File.Exists(path)) File.Delete(path);
            FileStorage fs = new FileStorage(path, FileStorage.Mode.Write);
            ((DTrees)model).Write(fs);
            fs.ReleaseAndGetString();
            SvmUtils.FixXML(path);
        }
        public override void LoadClassifer(string path)
        {
            DTrees dTrees = new DTrees();
            FileStorage fs = new FileStorage(path, FileStorage.Mode.Read);
            dTrees.Read(fs.GetRoot());
            fs.ReleaseAndGetString();

            this.model = dTrees;
        }
        public override string ToString()
        {
            if (!IsTrained) return "Decision tree not trained";

            StringBuilder sb = new StringBuilder();
            if (Recall != 0 && Precision != 0 && HMean != 0) sb.AppendLine(base.ToString());
            sb.AppendLine()
              .AppendLine(" Decision Tree ")
              .AppendLine(ModelName)
              .AppendLine("Number of cv folds: " + ((DTrees)model).CVFolds)
              .AppendLine("Max depth of tree: " + ((DTrees)model).MaxDepth)
              .AppendLine("Min sample count: " + ((DTrees)model).MinSampleCount)
              .AppendLine("Truncate pruned tree: " + ((DTrees)model).TruncatePrunedTree)
              .AppendLine("Use 1SE Rule: " + ((DTrees)model).Use1SERule)
              .AppendLine(" Training % = " + (PercTrain * 100))
              .AppendLine(" Test % = " + (PercTest * 100))
              .AppendLine()
              .AppendLine()
              .AppendLine("****End model****");

            return sb.ToString();
        }
        #endregion

        #region Remaining methods
        public string ToString(string name, string exampleType, int columnLength)
        {
            StringBuilder sb = new StringBuilder();

            sb.Append(IOUtils.CreateColumn(name, 2 * columnLength)); // mono gia to name valto 40 na xwraei

            sb.Append(IOUtils.CreateColumn(featureType.ToString(), columnLength));

            sb.Append(IOUtils.CreateColumn(exampleType, columnLength));

            sb.Append(IOUtils.CreateColumn((PercTrain * 100).ToString(), columnLength));
            sb.Append(IOUtils.CreateColumn((PercTest * 100).ToString(), columnLength));

            sb.AppendLine();

            return sb.ToString();
        }

        public DTrees CreateModel(int maxDepth, int minSampleCount, int CVFolds, bool use1SERule, bool truncatePrunedTree, int maxCategories = 0)
        {
            DTrees model = new DTrees();

            model.MaxDepth = maxDepth;
            model.MinSampleCount = minSampleCount;
            model.UseSurrogates = false;
            model.CVFolds = CVFolds;
            model.Use1SERule = use1SERule;
            model.TruncatePrunedTree = truncatePrunedTree;
            model.MaxCategories = maxCategories;

            return model;
        }

        #endregion
    }
}
