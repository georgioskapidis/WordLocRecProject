﻿using Emgu.CV;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WordLocRec.DataStructs;
using WordLocRec.Tools;

namespace WordLocRec.SVMStuff
{
    public class SVMFullFeatureExample : SVMExample
    {
        private FullFeatureObject standardMean, standardStd;
        private int columnOfSample = 0;

        public SVMFullFeatureExample() { }
        public SVMFullFeatureExample(Constants.FeatureType featureType)
        {
            this.feature = featureType;
            this.featureLength = DecideFeatureLength(feature);
            if (featureType.HasFlag(Constants.FeatureType.StandarizedPerFeature))
            {
                LoadMeanStdInDataset();
            }
        }

        public override void LoadMeanStdInDataset()
        {
            standardMean = IOUtils.FromXml<FullFeatureObject>(Constants.MeanFfoPath);
            standardStd = IOUtils.FromXml<FullFeatureObject>(Constants.StdFfoPath);
        }

        public override void SetFeatureType(Constants.FeatureType feature)
        {
            base.SetFeatureType(feature);
            if (feature.HasFlag(Constants.FeatureType.StandarizedPerFeature))
            {
                LoadMeanStdInDataset();
            }
        }
        public override void SetRowSample(BaseFeature ffo, ref Matrix<float> sample, int rowOfSample)
        {
            SetRowSample((FullFeatureObject)ffo, ref sample, rowOfSample);
        }

        public void SetRowSample(FullFeatureObject ffo, ref Matrix<float> sample, int rowOfSample)
        {
            if (feature.HasFlag(Constants.FeatureType.StandarizedPerFeature))
            {
                ffo.Standardize(standardMean, standardStd, feature);
            }
            if (feature.HasFlag(Constants.FeatureType.NM))
            {
                NMFeatureObject nmfeature = ffo.getNMFeatureObject();
                NMFeatureToMatrixSample(nmfeature, ref sample, rowOfSample, columnOfSample);
                columnOfSample += 7;
            }
            if (feature.HasFlag(Constants.FeatureType.Histogram))
            {
                HistogramCalculations.HistFeatures hf = ffo.getHistFeaturesObject();
                HistFeatureToSample(hf, ref sample, rowOfSample, columnOfSample);
                columnOfSample += 4;
            }
            if (feature.HasFlag(Constants.FeatureType.ColorDistPii))
            {
                float colorDistanceERBB = ffo.colorDistERBB;
                FloatFeatureToSample(colorDistanceERBB, ref sample, rowOfSample, columnOfSample);
                columnOfSample++;
            }
            if (feature.HasFlag(Constants.FeatureType.ColorDistLab))
            {
                float colorDistanceERBB = ffo.colorDistERBBLab;
                FloatFeatureToSample(colorDistanceERBB, ref sample, rowOfSample, columnOfSample);
                columnOfSample++;
            }
            if (feature.HasFlag(Constants.FeatureType.ColorDistBgr))
            {
                float colorDistanceERBB = ffo.colorDistERBBBgr;
                FloatFeatureToSample(colorDistanceERBB, ref sample, rowOfSample, columnOfSample);
                columnOfSample++;
            }
            if (feature.HasFlag(Constants.FeatureType.SignificantPaths))
            {
                float significantPaths = ffo.significantPaths;
                FloatFeatureToSample(significantPaths, ref sample, rowOfSample, columnOfSample);
                columnOfSample++;
                //float skelContourRatio = ffo.skelContourRatio;
                //FloatFeatureToSample(skelContourRatio, ref sample, rowOfSample, columnOfSample);
                //columnOfSample++;
                //float skelAreaRatio = ffo.skelAreaRatio;
                //FloatFeatureToSample(skelAreaRatio, ref sample, rowOfSample, columnOfSample);
                //columnOfSample++;
                //float areaBBRatio = ffo.areaBbRatio;
                //FloatFeatureToSample(areaBBRatio, ref sample, rowOfSample, columnOfSample);
                //columnOfSample++;
            }
            if (feature.HasFlag(Constants.FeatureType.NumberOfHoles))
            {
                float numOfHoles = ffo.nmFeature.numOfHoles;
                FloatFeatureToSample(numOfHoles, ref sample, rowOfSample, columnOfSample);
                columnOfSample++;
            }
            if (feature.HasFlag(Constants.FeatureType.ColorConsistencyLab))
            {
                float colorConsistencyLab = ffo.colorConsistencyLab;
                FloatFeatureToSample(colorConsistencyLab, ref sample, rowOfSample, columnOfSample);
                columnOfSample++;
            }
            if (feature.HasFlag(Constants.FeatureType.ConvexityDefects))
            {
                float convexityDefects = ffo.ConvexityDefects;
                FloatFeatureToSample(convexityDefects, ref sample, rowOfSample, columnOfSample);
                columnOfSample++;
            }
            if (feature.HasFlag(Constants.FeatureType.StrokeRatio))
            {
                float strokeRatio = ffo.StrokeWidthERWidthRatio;
                FloatFeatureToSample(strokeRatio, ref sample, rowOfSample, columnOfSample);
                columnOfSample++;
            }
            columnOfSample = 0;
        }
        public static void NMFeatureToMatrixSample(NMFeatureObject nmObj, ref Matrix<float> sample, int row = 0, int startCol = 0)
        {
            sample[row, startCol] = nmObj.aspectRatio;
            sample[row, startCol + 1] = nmObj.compactness;
            sample[row, startCol + 2] = nmObj.numOfHoles;
            sample[row, startCol + 3] = nmObj.medianOfCrossings;
            sample[row, startCol + 4] = nmObj.holeAreaRatio;
            sample[row, startCol + 5] = nmObj.convexHullRatio;
            sample[row, startCol + 6] = nmObj.numInflextionPoints;
        }
        public static void HistFeatureToSample(HistogramCalculations.HistFeatures hf, ref Matrix<float> sample, int row = 0, int startCol = 0)
        {
            sample[row, startCol] = hf.distanceIntensityBB;
            sample[row, startCol + 1] = hf.percOfBBPixelsAt2MainIntensities;
            sample[row, startCol + 2] = hf.erbbIntensityRatio;
            sample[row, startCol + 3] = hf.stdDevFullRange;
        }

        public static int DecideFeatureLengthStatic(Constants.FeatureType featureType)
        {
            int size = 0;
            if (featureType.HasFlag(Constants.FeatureType.NM))
            {
                size += 7;
            }
            if (featureType.HasFlag(Constants.FeatureType.Histogram))
            {
                size += 4;
            }
            if (featureType.HasFlag(Constants.FeatureType.ColorDistPii))
            {
                size++;
            }
            if (featureType.HasFlag(Constants.FeatureType.ColorDistLab))
            {
                size++;
            }
            if (featureType.HasFlag(Constants.FeatureType.ColorDistBgr))
            {
                size++;
            }
            if (featureType.HasFlag(Constants.FeatureType.NumberOfHoles))
            {
                size++;
            }
            if (featureType.HasFlag(Constants.FeatureType.SignificantPaths))
            {
                //size++;
                size += 4;
            }
            if (featureType.HasFlag(Constants.FeatureType.ColorConsistencyLab))
            {
                size++;
            }
            if (featureType.HasFlag(Constants.FeatureType.ConvexityDefects))
            {
                size++;
            }
            if (featureType.HasFlag(Constants.FeatureType.StrokeRatio))
            {
                size++;
            }
            return size;
        }
        public override int DecideFeatureLength(Constants.FeatureType featureType)
        {
            return DecideFeatureLengthStatic(featureType);
        }

    }
}
