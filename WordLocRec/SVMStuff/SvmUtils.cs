﻿using Emgu.CV;
using Emgu.CV.ML;
using Emgu.CV.Structure;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace WordLocRec.SVMStuff
{
    public static class SvmUtils
    {
        public static void SaveSVMInfo(String fullpath, String dataLine)
        {
            if (IOUtils.CreateSVMInfoFile(fullpath))
            {
                File.AppendAllText(fullpath, dataLine);
            }
        }
        public static SVM MakeSVMAuto(Matrix<float> trainExamples, Matrix<int> trainClasses, int termIterations)
        {
            SVM model = new SVM();
            model.SetKernel(SVM.SvmKernelType.Rbf);
            model.Type = SVM.SvmType.CSvc;
            model.C = 2;
            model.Gamma = 0.01;
            model.Coef0 = 0;
            model.Nu = 0.1;
            model.Degree = 10;
            model.TermCriteria = new MCvTermCriteria(termIterations, 10e-8);

            // train the svm model
            TrainData td = new TrainData(trainExamples, Emgu.CV.ML.MlEnum.DataLayoutType.RowSample, trainClasses);
            bool trained = model.TrainAuto(td);

            return model;
        }
        public static void SaveSVMToFile(SVM model, String path)
        {
            if (File.Exists(path)) File.Delete(path);
            FileStorage fs = new FileStorage(path, FileStorage.Mode.Write);
            model.Write(fs);
            fs.ReleaseAndGetString();
        }
        public static SVM LoadSVMFromFile(String svmPath, out FileStorage fs)
        {
            SVM svm = new SVM();
            fs = new FileStorage(svmPath, FileStorage.Mode.Read);
            svm.Read(fs.GetRoot());
            return svm;
        }
        public static void SaveDTreesToFile(DTrees model, String path)
        {
            if (File.Exists(path)) File.Delete(path);
            FileStorage fs = new FileStorage(path, FileStorage.Mode.Write);
            model.Write(fs);
            fs.ReleaseAndGetString();
        }
        public static void FixXML(String fpath)
        {
            var textLines = File.ReadAllLines(fpath).ToList();
            textLines.Insert(2, "<format>3</format>");
            File.WriteAllLines(fpath, textLines);
        }
        public static DTrees LoadDTreesFromFile(String dTreesPath, out FileStorage fs)
        {
            DTrees dtrees = new DTrees();
            fs = new FileStorage(dTreesPath, FileStorage.Mode.Read);
            dtrees.Read(fs.GetRoot());
            return dtrees;
        }
        public static void SaveRTreesToFile(RTrees model, String path)
        {
            if (File.Exists(path)) File.Delete(path);
            FileStorage fs = new FileStorage(path, FileStorage.Mode.Write);
            model.Write(fs);
            fs.ReleaseAndGetString();
        }
        public static RTrees LoadRTreesFromFile(String rTreesPath, out FileStorage fs)
        {
            RTrees rtrees = new RTrees();
            fs = new FileStorage(rTreesPath, FileStorage.Mode.Read);
            rtrees.Read(fs.GetRoot());
            return rtrees;
        }
        public static void UnloadModel(FileStorage fs)
        {
            fs.ReleaseAndGetString();
        }
       
        public static void CreateSaveTestSVM(String path, Matrix<float> trainExamples, Matrix<float> testExamples, Matrix<int> trainClasses, Matrix<int> testClasses)
        {
            // create the svm model
            SVM model = MakeSVMAuto(trainExamples, trainClasses, 10);

            // save the svm model
            SaveSVMToFile(model, path);

            // load the svm model
            FileStorage fs;
            model = LoadSVMFromFile(path, out fs);

            // test the svm model
            float percTrain = EvaluateSVMModel(model, trainExamples, trainClasses);
            float percTest = EvaluateSVMModel(model, testExamples, testClasses);

            var supvec = model.GetSupportVectors();
            Matrix<float> vectors = new Matrix<float>(supvec.Rows, supvec.Cols, supvec.DataPointer);

            if (Constants.VERBOSE)
            {
                Debug.WriteLine(" Training % = " + (percTrain * 100));
                Debug.WriteLine(" Test % = " + (percTest * 100));
                Debug.WriteLine(" Support Vectors = " + vectors.Rows);
            }
        }

        public static IStatModel DefineTypeOfClassifier(String path)
        {
            FileStorage fs;
            XmlDocument doc = new XmlDocument();
            using (XmlReader xmlReader = XmlReader.Create(path))
            {
                doc.Load(xmlReader);
                XmlNode root = doc.DocumentElement;
                foreach (XmlNode node in root)
                {
                    if (node.Name.Equals("svmType"))
                    {
                        SVM model = LoadSVMFromFile(path, out fs);
                        UnloadModel(fs);
                        return model;
                    }
                    else if (node.Name.Equals("ntrees"))
                    {
                        RTrees model = LoadRTreesFromFile(path, out fs);
                        UnloadModel(fs);
                        return model;
                    }
                    else if (node.Name.Equals("nodes"))
                    {
                        DTrees model = LoadDTreesFromFile(path, out fs);
                        UnloadModel(fs);
                        return model;
                    }
                }
                throw new Exception("Trying to read unknown Classifier type");
            }
        }


        /// <summary>
        /// an zhthsw raw output tote epistrefei enan arithmo poy mporw na metatrepsw se pithanothta, p.x. gia xrhsh se nms
        /// </summary>
        /// <param name="model"></param>
        /// <param name="examples"></param>
        /// <param name="expectedResults"></param>
        /// <returns></returns>
        public static float EvaluateSVMModel(SVM model, Matrix<float> examples, Matrix<int> expectedResults, int temp = 0)
        {
            float perc = 0f;
            if (examples.Rows > 0)
            {
                Matrix<float> responses = new Matrix<float>(examples.Rows, 1);
                model.Predict(examples, responses);
                Matrix<float> probs = new Matrix<float>(examples.Rows, 1);
                model.Predict(examples, probs, 1);
                for (int i = 0; i < responses.Rows; i++)
                {
                    float exp = (float)Math.Exp(-2 * probs[i, 0]);
                    probs[i, 0] = (float)1 - (float)1 / (1 + exp);
                    if (responses[i, 0] == 1 && expectedResults[i, 0] == 1) perc++;
                    else if (responses[i, 0] == 2 && expectedResults[i, 0] == 2) perc++;
                }
                perc /= responses.Rows;
            }
            return perc;
        }
        /// <summary>
        /// Same name is kept only for polymorphism and it will be fixed later if dtrees are deemed worthy!
        /// </summary>
        /// <param name="model"></param>
        /// <param name="examples"></param>
        /// <param name="expectedResults"></param>
        /// <returns></returns>
        public static float EvaluateSVMModel(DTrees model, Matrix<float> examples, Matrix<int> expectedResults)
        {
            float perc = 0f;
            if (examples.Rows > 0)
            {
                Matrix<float> responses = new Matrix<float>(examples.Rows, 1);
                model.Predict(examples, responses);
                for (int i = 0; i < responses.Rows; i++)
                {
                    if (responses[i, 0] == 1 && expectedResults[i, 0] == 1) perc++;
                    else if (responses[i, 0] == 2 && expectedResults[i, 0] == 2) perc++;
                }
                perc /= responses.Rows;
            }
            return perc;
        }
        public static float EvaluateSVMModel(RTrees model, Matrix<float> examples, Matrix<int> expectedResults)
        {
            float perc = 0f;
            if (examples.Rows > 0)
            {
                Matrix<float> responses = new Matrix<float>(examples.Rows, 1);
                model.Predict(examples, responses);
                Matrix<float> probs = new Matrix<float>(examples.Rows, 1);
                model.Predict(examples, probs, 1);
                double min, max;
                Point p1, p2; 
                probs.MinMax(out min, out max, out p1, out p2, null);
                for (int i = 0; i < responses.Rows; i++)
                {
                    if (responses[i, 0] == 1 && expectedResults[i, 0] == 1) perc++;
                    else if (responses[i, 0] == 2 && expectedResults[i, 0] == 2) perc++;
                }
                perc /= responses.Rows;
            }
            return perc;
        }
    }
}
