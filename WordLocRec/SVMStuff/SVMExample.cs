﻿using Emgu.CV;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WordLocRec.DataStructs;

namespace WordLocRec.SVMStuff
{
    public abstract class SVMExample
    {
        public Constants.FeatureType feature;
        public SVMExample(){}
        public abstract void SetRowSample(BaseFeature obj, ref Matrix<float> sample, int rowOfSample);
        public int featureLength;
        public abstract int DecideFeatureLength(Constants.FeatureType featureType);
        public abstract void LoadMeanStdInDataset();

        public static void FloatFeatureToSample(float f, ref Matrix<float> sample, int row = 0, int startCol = 0)
        {
            sample[row, startCol] = f;
        }
        public virtual void SetFeatureType(Constants.FeatureType feature)
        {
            this.feature = feature;
        }
    }
}
