﻿using Emgu.CV;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WordLocRec.DataStructs;
using WordLocRec.Grouping;

namespace WordLocRec.SVMStuff
{
    public class SVMSimpleWordStatExample : SVMExample
    {
        private SimpleWordStat standardMean, standardStd;
        private int columnOfSample = 0;

        public SVMSimpleWordStatExample() { }
        public SVMSimpleWordStatExample(Constants.FeatureType featureType)
        {
            this.feature = featureType;
            this.featureLength = DecideFeatureLength(feature);

            if (featureType.HasFlag(Constants.FeatureType.StandarizedPerFeature))
            {
                LoadMeanStdInDataset();
            }
        }
        public override void LoadMeanStdInDataset()
        {
            standardMean = IOUtils.FromXml<SimpleWordStat>(Constants.MeanSWSPath);
            standardStd = IOUtils.FromXml<SimpleWordStat>(Constants.StdSWSPath);
        }
        public override void SetFeatureType(Constants.FeatureType feature)
        {
            base.SetFeatureType(feature);
            if (feature.HasFlag(Constants.FeatureType.StandarizedPerFeature))
            {
                LoadMeanStdInDataset();
            }
        }

        public override void SetRowSample(BaseFeature wordStat, ref Matrix<float> sample, int rowOfSample)
        {
            SetRowSample((SimpleWordStat)wordStat, ref sample, rowOfSample);
        }

        public void SetRowSample(SimpleWordStat wordStat, ref Matrix<float> sample, int rowOfSample)
        {
            if (feature.HasFlag(Constants.FeatureType.StandarizedPerFeature))
                wordStat.Standardize(standardMean, standardStd);

            if (feature.HasFlag(Constants.FeatureType.SimpleFeature))
            {
                FloatFeatureToSample(wordStat.ForegroundBackgroundLabDistanceCoV, ref sample, rowOfSample, columnOfSample++);
                FloatFeatureToSample(wordStat.MeanStrokeWidthCoV, ref sample, rowOfSample, columnOfSample++);
                FloatFeatureToSample(wordStat.ConvexHullCompactnessMean, ref sample, rowOfSample, columnOfSample++);
                FloatFeatureToSample(wordStat.ConvexHullCompactnessStd, ref sample, rowOfSample, columnOfSample++);
                FloatFeatureToSample(wordStat.ConvexityDefectsCoV, ref sample, rowOfSample, columnOfSample++);
                FloatFeatureToSample(wordStat.VerticalCenterDistanceCoV, ref sample, rowOfSample, columnOfSample++);
                FloatFeatureToSample(wordStat.HorizontalCenterDistancesCoV, ref sample, rowOfSample, columnOfSample++);
            }
            else
            {
                FloatFeatureToSample(wordStat.MeanForegroundIntensitiesCoV, ref sample, rowOfSample, columnOfSample++);
                FloatFeatureToSample(wordStat.MeanOuterBoundaryIntensitiesCoV, ref sample, rowOfSample, columnOfSample++);
                FloatFeatureToSample(wordStat.ForegroundBackgroundLabDistanceCoV, ref sample, rowOfSample, columnOfSample++);
                FloatFeatureToSample(wordStat.PerimetricGradientMagnitudeMeanCoV, ref sample, rowOfSample, columnOfSample++);
                FloatFeatureToSample(wordStat.MeanStrokeWidthCoV, ref sample, rowOfSample, columnOfSample++);
                FloatFeatureToSample(wordStat.ConvexHullCompactnessMean, ref sample, rowOfSample, columnOfSample++);
                FloatFeatureToSample(wordStat.ConvexHullCompactnessStd, ref sample, rowOfSample, columnOfSample++);
                FloatFeatureToSample(wordStat.ConvexityDefectsCoV, ref sample, rowOfSample, columnOfSample++);
                FloatFeatureToSample(wordStat.VerticalCenterDistanceCoV, ref sample, rowOfSample, columnOfSample++);
                FloatFeatureToSample(wordStat.HorizontalCenterDistancesCoV, ref sample, rowOfSample, columnOfSample++);
            }
            columnOfSample = 0;
        }

        public override int DecideFeatureLength(Constants.FeatureType featureType)
        {
            return DecideFeatureLengthStatic(featureType);
        }

        public static int DecideFeatureLengthStatic(Constants.FeatureType featureType)
        {
            return featureType.HasFlag(Constants.FeatureType.SimpleFeature) ? 7 : 10;
        }
    }
}
