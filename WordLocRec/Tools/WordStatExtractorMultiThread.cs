﻿using Emgu.CV;
using Emgu.CV.CvEnum;
using Emgu.CV.Structure;
using Emgu.CV.Util;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using WordLocRec.Forms;
using WordLocRec.Grouping;

namespace WordLocRec.Tools
{
    public class WordStatExtractorMultiThread
    {
        private ManualResetEvent mre;
        String imageFile, outputFolder;
        Constants.FeatureType f1, f2;
        bool onlyGT;

        public WordStatExtractorMultiThread(String imageFile, Constants.FeatureType f1, Constants.FeatureType f2, String outputFolder, bool onlyGT, ManualResetEvent mre)
        {
            this.imageFile = imageFile;
            this.f1 = f1;
            this.f2 = f2;
            this.outputFolder = outputFolder;
            this.onlyGT = onlyGT;
            this.mre = mre;
        }
        public WordStatExtractorMultiThread(String imageFile, Constants.FeatureType f1, Constants.FeatureType f2, String outputFolder)
        {
            this.imageFile = imageFile;
            this.f1 = f1;
            this.f2 = f2;
            this.outputFolder = outputFolder;
        }

        public void ThreadPoolCallback(Object threadContext)
        {
            int threadIndex = (int)threadContext;
            ExtractAndSaveAllDataTypes();
            //ExtractAndSaveAllDataTypesWithoutAsking();
            mre.Set();
        }

        public void ExtractAndSaveAllDataTypesWithoutAsking()
        {
            Mat mImage = new Mat(imageFile, LoadImageType.Unchanged);
            if (mImage.Rows == 0 || mImage.Cols == 0) return;
            VectorOfMat channels = new VectorOfMat();
            Utils.CreateXXChannels(ref channels, mImage, Constants.ColorSpace.Grayscale, Constants.VisChannels.Channel1 | Constants.VisChannels.Channel1Opp, false, Constants.Gradients.None);
            try
            {
                List<ERStat>[] firstRegions, feedbackRegions;
                ERExtractor.ExecuteDefaultExtractorPipelineNoGrouping(mImage, channels, f1, f2, out firstRegions, out feedbackRegions);

                GroupingChinese_WordStats_SimpleWordStats_LineWordStatsExtractionDontAsk(mImage, channels, firstRegions, feedbackRegions);
            }
            catch (Exception cve)
            {
                Debug.WriteLine(cve.ToString());
                Debug.WriteLine("@ image: " + imageFile);
                Debug.WriteLine("");
                return;
            }
            finally
            {
                Disposer.DisposeMatVectorOfMatRegions(ref mImage, ref channels);
            }      
        }

        public void ExtractAndSaveAllDataTypes()
        {
            using (Mat mImage = new Mat(imageFile, LoadImageType.Unchanged))
            {
                if (mImage.Rows == 0 || mImage.Cols == 0) return;
                VectorOfMat channels = new VectorOfMat();
                Utils.CreateXXChannels(ref channels, mImage, Constants.ColorSpace.Grayscale, Constants.VisChannels.Channel1 | Constants.VisChannels.Channel1Opp, false, Constants.Gradients.None);

                List<ERStat>[] firstRegions, feedbackRegions;
                ERExtractor.ExecuteDefaultExtractorPipelineNoGrouping(mImage, channels, f1, f2, out firstRegions, out feedbackRegions);

                GroupingNeumMatas_WordStats_SimpleWordStatsExtraction(mImage, channels, firstRegions);

                GroupingChinese_WordStats_SimpleWordStats_LineWordStatsExtraction(mImage, channels, firstRegions, feedbackRegions);
            }
        }

        /// <summary>
        /// Method to create all the 3 types of word features for the results of the *original* files of the 2013 ICDAR dataset (or any other dataset for that matter)
        /// </summary>
        /// <param name="mImage"></param>
        /// <param name="channels"></param>
        /// <param name="firstRegions"></param>
        /// <param name="feedbackRegions"></param>
        private void GroupingChinese_WordStats_SimpleWordStats_LineWordStatsExtractionDontAsk(Mat mImage, VectorOfMat channels, List<ERStat>[] firstRegions, List<ERStat>[] feedbackRegions)
        {
            List<ChineseGrouping.Line[]> linesPerChannel = GroupingUtils.ChineseGroupingPerChannel(mImage, channels, firstRegions, feedbackRegions);
            
            int totalWordRects = 0;
            foreach (ChineseGrouping.Line[] lines in linesPerChannel)
            {
                totalWordRects += lines.Length;
                foreach (ChineseGrouping.Line l in lines)
                    l.wordRect = GroupingUtils.SouloupwseRects(l.wordRect, mImage.Rows, mImage.Cols);
            }

            List<Rectangle> wRects = new List<Rectangle>();
            List<ChineseGrouping.Line> allLines = new List<ChineseGrouping.Line>();
            if (totalWordRects > 0)
            {
                List<Tuple<List<ERStat>, int>> erListPerWordRect = LineWordStat.CreateErListPerWordRect(linesPerChannel, out wRects, out allLines);
              
                //extract the features
                WordStat[] wordStats = WordStat.CreateMultiple(wRects.ToArray(), erListPerWordRect, mImage, channels, imageFile);
                SimpleWordStat[] simpleWordStats = SimpleWordStat.CreateMultiple(wRects.ToArray(), erListPerWordRect, mImage, channels, imageFile);
                LineWordStat[] lineWordStats = LineWordStat.CreateMultiple(wRects.ToArray(), allLines.ToArray(), erListPerWordRect, mImage, channels, imageFile);
                
                if (wordStats.Length != simpleWordStats.Length || wordStats.Length != lineWordStats.Length || simpleWordStats.Length != lineWordStats.Length) throw new Exception("tha eprepe na exoyn to idio plithos");

                for (int i = 0; i < wordStats.Length; i++)
                {
                    String outputPathWS = Path.Combine(outputFolder, "ChineseGroupingResults", "WordStats", Path.GetFileNameWithoutExtension(imageFile), "rect" + i.ToString() + ".xml");
                    String outputPathSWS = Path.Combine(outputFolder, "ChineseGroupingResults", "SimpleWordStats", Path.GetFileNameWithoutExtension(imageFile), "rect" + i.ToString() + ".xml");
                    String outputPathLWS = Path.Combine(outputFolder, "ChineseGroupingResults", "LineWordStats", Path.GetFileNameWithoutExtension(imageFile), "rect" + i.ToString() + ".xml");
                    wordStats[i].ToXML<WordStat>(outputPathWS);
                    simpleWordStats[i].ToXML<SimpleWordStat>(outputPathSWS);
                    lineWordStats[i].ToXML<LineWordStat>(outputPathLWS);
                }
            }
        }

        private void GroupingChinese_WordStats_SimpleWordStats_LineWordStatsExtraction(Mat mImage, VectorOfMat channels, List<ERStat>[] firstRegions, List<ERStat>[] feedbackRegions)
        {
            List<ChineseGrouping.Line[]> linesPerChannel = GroupingUtils.ChineseGroupingPerChannel(mImage, channels, firstRegions, feedbackRegions);
            int totalWordRects = 0;
            foreach (ChineseGrouping.Line[] lines in linesPerChannel)
            {
                totalWordRects += lines.Length;
                foreach (ChineseGrouping.Line l in lines)
                    l.wordRect = GroupingUtils.SouloupwseRects(l.wordRect, mImage.Rows, mImage.Cols);
            }

            List<Rectangle> wRects = new List<Rectangle>();
            List<ChineseGrouping.Line> allLines = new List<ChineseGrouping.Line>();
            if (totalWordRects > 0)
            {
                VectorOfMat wordMats = new VectorOfMat();
                List<Tuple<List<ERStat>, int>> erListPerWordRect = new List<Tuple<List<ERStat>, int>>();
                
                for (int i = 0; i < linesPerChannel.Count; i++)
                {
                    for (int j = 0; j < linesPerChannel[i].Length; j++)
                    {
                        erListPerWordRect.Add(Tuple.Create<List<ERStat>, int>(new List<ERStat>(linesPerChannel[i][j].ersOfLine), i));
                        wRects.Add(linesPerChannel[i][j].wordRect);
                        allLines.Add(linesPerChannel[i][j]);

                        // create images for viewing by user
                        using (Mat imageDrawERs = channels[i].Clone())
                        {
                            foreach (ERStat er in linesPerChannel[i][j].ersOfLine)
                                CvInvoke.Rectangle(imageDrawERs, er.rect, new Bgr(255, 0, 0).MCvScalar, 1);
                            using (Mat drawIm = new Mat(imageDrawERs, linesPerChannel[i][j].wordRect))
                                wordMats.Push(drawIm.Clone());
                        }
                    }
                }
                //extract the features
                WordStat[] wordStats = WordStat.CreateMultiple(wRects.ToArray(), erListPerWordRect, mImage, channels, imageFile);
                SimpleWordStat[] simpleWordStats = SimpleWordStat.CreateMultiple(wRects.ToArray(), erListPerWordRect, mImage, channels, imageFile);
                LineWordStat[] lineWordStats = LineWordStat.CreateMultiple(wRects.ToArray(), allLines.ToArray(), erListPerWordRect, mImage, channels, imageFile);
                
                // sanity check
                if (wordStats.Length != simpleWordStats.Length || lineWordStats.Length != simpleWordStats.Length || lineWordStats.Length != wordStats.Length) throw new Exception("Olo malakies, check the sizes of the 'stats' arrays");

                // extract the texts for viewing purposes
                List<String> texts = new List<string>();
                for (int i = 0; i < wordStats.Length; i++)
                {
                    StringBuilder sb = new StringBuilder();
                    sb.AppendLine("Line Grouping: " + lineWordStats[i].numberOfERs + " ERs used.");
                    sb.AppendLine(wordStats[i].ToString() + simpleWordStats[i].ToString() + lineWordStats[i].ToString());
                    texts.Add(sb.ToString());
                }
                //FormUtils.VisualizeErPixels(channels[i], l.ersOfLine);
                // choose positive-negative examples
                ChoosePosNeg cpn = new ChoosePosNeg(wordMats, texts);
                cpn.ShowDialog();
                wordMats.Dispose();

                for (int i = 0; i < wordStats.Length; i++)
                {
                    String folder, type;
                    if (cpn.positiveIndices.Contains(i)) // processing a positive example
                    {
                        if (onlyGT)
                            folder = "GT"; // positive example from ground truth
                        else 
                            folder = "Positive"; // positive example found from complete image
                        type = "True";
                    }
                    else if (cpn.negativeIndices.Contains(i)) // processing a negative example
                    {
                        if (onlyGT)
                            folder = "GTNeg"; // negative example from ground truth
                        else
                            folder = "Negative"; // negative example found from complete image
                        type = "False";
                    }
                    else if (cpn.ambiguousIndices.Contains(i)) // processing an ambiguous example
                    {
                        if (onlyGT)
                            folder = "PositiveGTAmbiguous"; // ambiguous example from ground truth
                        else 
                            folder = "PositiveAmbiguous"; // ambiguous example from complete image
                        type = "TrueDat";
                    }
                    else
                    {
                        folder = "wtf";
                        type = "wrong";
                        // never get in here!
                    }
                    // save the wordstats
                    String outputPathWS = Path.Combine(outputFolder, "ChineseGroupingResults", "WordStats", folder, Path.GetFileNameWithoutExtension(imageFile) + type + i.ToString() + ".xml");
                    String outputPathSWS = Path.Combine(outputFolder, "ChineseGroupingResults", "SimpleWordStats", folder, Path.GetFileNameWithoutExtension(imageFile) + type + i.ToString() + ".xml");
                    String outputPathLWS = Path.Combine(outputFolder, "ChineseGroupingResults", "LineWordStats", folder, Path.GetFileNameWithoutExtension(imageFile) + type + i.ToString() + ".xml");
                    wordStats[i].ToXML<WordStat>(outputPathWS);
                    simpleWordStats[i].ToXML<SimpleWordStat>(outputPathSWS);
                    lineWordStats[i].ToXML<LineWordStat>(outputPathLWS);
                }
            }          
        }
       
        private void GroupingNeumMatas_WordStats_SimpleWordStatsExtraction(Mat mImage, VectorOfMat channels, List<ERStat>[] firstRegions)
        {
            // copy the regions because they are changed by the feedback loop at nm method
            List<ERStat>[] nmRegions = new List<ERStat>[firstRegions.Length];
            for (int i = 0; i < nmRegions.Length; i++)
                nmRegions[i] = new List<ERStat>(firstRegions[i]);
            List<Rectangle> boxes; List<List<Tuple<int, int>>> groups;
            Rectangle[] wordRects = ERGroupingNM.DoERGroupingNM(mImage.GetInputArray(), channels, nmRegions, out groups, out boxes, true);
            wordRects = GroupingUtils.SouloupwseRects(wordRects, mImage.Rows, mImage.Cols);
            //List<int> trimmedIndices = new List<int>();
            //wordRects = GroupingUtils.TrimRects(wordRects, mImage.Rows, mImage.Cols, 0.7f, ref trimmedIndices); // trim sto timio pososto
            //for (int i = trimmedIndices.Count - 1; i >= 0; i--)
            //    groups.RemoveAt(trimmedIndices[i]);

            if (wordRects.Length > 0)
            {
                VectorOfMat wordMats = new VectorOfMat();
                // create a list of ers for every word rect, and the according channel it is extracted from
                List<Tuple<List<ERStat>, int>> erListPerWordRect = new List<Tuple<List<ERStat>, int>>();
                int counter = -1;
                foreach (List<Tuple<int, int>> wordERs in groups)
                {
                    counter++;
                    List<ERStat> tmp = new List<ERStat>();
                    for (int i = 0; i < wordERs.Count; i++)
                    {
                        tmp.Add(nmRegions[wordERs[i].Item1][wordERs[i].Item2]);
                    }
                    erListPerWordRect.Add(Tuple.Create<List<ERStat>, int>(new List<ERStat>(tmp), wordERs[0].Item1));

                    using (Mat imageDrawERs = channels[wordERs[0].Item1].Clone())
                    {
                        foreach (ERStat er in tmp)
                            CvInvoke.Rectangle(imageDrawERs, er.rect, new Bgr(255, 0, 0).MCvScalar, 1);
                        using(Mat drawIm = new Mat(imageDrawERs,wordRects[counter]))
                            wordMats.Push(drawIm.Clone());
                    }
                }
                WordStat[] wordStats = WordStat.CreateMultiple(wordRects, erListPerWordRect, mImage, channels, imageFile);
                SimpleWordStat[] simpleWordStats = SimpleWordStat.CreateMultiple(wordRects, erListPerWordRect, mImage, channels, imageFile);

                if (wordStats.Length != simpleWordStats.Length) throw new Exception("Olo malakies, check the sizes of the 'stats' arrays");

                List<String> texts = new List<string>();
                for (int i = 0; i < wordStats.Length; i++)
                {
                    StringBuilder sb = new StringBuilder();
                    sb.AppendLine("Exhaustive Search Grouping: " + wordStats[i].numberOfERs + " ERs used.");
                    sb.AppendLine(wordStats[i].ToString() + simpleWordStats[i].ToString());
                    texts.Add(sb.ToString());
                }

                // choose positive-negative examples
                ChoosePosNeg cpn = new ChoosePosNeg(wordMats, texts);
                cpn.ShowDialog();
                wordMats.Dispose();

                for (int i = 0; i < wordStats.Length; i++)
                {
                    String folder, type;
                    if (cpn.positiveIndices.Contains(i)) // processing a positive example
                    {
                        if (onlyGT)
                            folder = "GT"; // positive example from ground truth
                        else
                            folder = "Positive"; // positive example found from complete image
                        type = "True";
                    }
                    else if (cpn.negativeIndices.Contains(i)) // processing a negative example
                    {
                        if (onlyGT)
                            folder = "GTNeg"; // negative example from ground truth
                        else
                            folder = "Negative"; // negative example found from complete image
                        type = "False";
                    }
                    else if (cpn.ambiguousIndices.Contains(i)) // processing an ambiguous example
                    {
                        if (onlyGT)
                            folder = "PositiveGTAmbiguous"; // ambiguous example from ground truth
                        else
                            folder = "PositiveAmbiguous"; // ambiguous example from complete image
                        type = "TrueDat";
                    }
                    else
                    {
                        folder = "wtf";
                        type = "wrong";
                        // never get in here!
                    }
                    // save the wordstats
                    String outputPathWS = Path.Combine(outputFolder, "NMResults", "WordStats", folder, Path.GetFileNameWithoutExtension(imageFile) + type + i.ToString() + ".xml");
                    String outputPathSWS = Path.Combine(outputFolder, "NMResults", "SimpleWordStats", folder, Path.GetFileNameWithoutExtension(imageFile) + type + i.ToString() + ".xml");
                    wordStats[i].ToXML<WordStat>(outputPathWS);
                    simpleWordStats[i].ToXML<SimpleWordStat>(outputPathSWS);
                }
            }
        }


    }
}
