﻿using Emgu.CV;
using Emgu.CV.CvEnum;
using Emgu.CV.Util;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using WordLocRec.DataStructs;
using WordLocRec.Grouping;

namespace WordLocRec.Tools
{
    class WordStatFixerMultiThread
    {
        private ManualResetEvent mre;
        List<Rectangle> positiveRects, negativeRects;
        Constants.FeatureType f1, f2;
        List<Tuple<WordStat, string>> perImageSplitWS;
        List<Tuple<SimpleWordStat, string>> perImageSplitSWS;
        List<Tuple<LineWordStat, string>> perImageSplitLWS;

        public WordStatFixerMultiThread(Constants.FeatureType f1, Constants.FeatureType f2, List<Rectangle> positiveRects, List<Rectangle> negativeRects, ManualResetEvent mre, List<Tuple<WordStat, string>> perImageSplitWS, List<Tuple<SimpleWordStat, string>> perImageSplitSWS, List<Tuple<LineWordStat, string>> perImageSplitLWS): this(f1, f2, mre, perImageSplitWS, perImageSplitSWS, perImageSplitLWS)
        {
            if (positiveRects != null)
                this.positiveRects = positiveRects;
            else this.positiveRects = new List<Rectangle>();
            if (negativeRects != null)
                this.negativeRects = negativeRects;
            else this.negativeRects = new List<Rectangle>();
        }

        public WordStatFixerMultiThread(Constants.FeatureType f1, Constants.FeatureType f2, ManualResetEvent mre, List<Tuple<WordStat, string>> perImageSplitWS, List<Tuple<SimpleWordStat, string>> perImageSplitSWS, List<Tuple<LineWordStat, string>> perImageSplitLWS)
        {
            this.f1 = f1;
            this.f2 = f2;
            this.mre = mre;
            this.perImageSplitWS = perImageSplitWS;
            this.perImageSplitSWS = perImageSplitSWS;
            this.perImageSplitLWS = perImageSplitLWS;
        }
        public void ThreadPoolCallback(Object threadContext)
        {
            int threadIndex = (int)threadContext;
            FixLineGroupingResultsPerImage(perImageSplitWS, perImageSplitSWS, perImageSplitLWS);

            mre.Set();
        }
        private void FixLineGroupingResultsPerImage(List<Tuple<WordStat, string>> perImageSplitWS, List<Tuple<SimpleWordStat, string>> perImageSplitSWS, List<Tuple<LineWordStat, string>> perImageSplitLWS)
        {
            Mat mImage = new Mat(perImageSplitWS[0].Item1.imageName, LoadImageType.Unchanged);
            if (mImage.Rows == 0 || mImage.Cols == 0) return;

            VectorOfMat channels = new VectorOfMat();
            Utils.CreateXXChannels(ref channels, mImage, Constants.ColorSpace.Grayscale, Constants.VisChannels.Channel1 | Constants.VisChannels.Channel1Opp, false, Constants.Gradients.None);

            List<ChineseGrouping.Line[]> linesPerChannel;
            ERExtractor.ExecuteDefaultExtractorPipelineLineGrouping(mImage, channels, f1, f2, out linesPerChannel);

            List<Rectangle> wRects;
            List<ChineseGrouping.Line> allLines;
            List<Tuple<List<ERStat>, int>> erListPerWordRect = LineWordStat.CreateErListPerWordRect(linesPerChannel, out wRects, out allLines);

            for (int j = 0; j < perImageSplitWS.Count; j++)
            {
                WordStat wordstat = perImageSplitWS[j].Item1;
                SimpleWordStat simpleWordStat = perImageSplitSWS[j].Item1;
                LineWordStat lineWordStat = perImageSplitLWS[j].Item1;

                int indexCorrect;
                if (!((wordstat.WordRect.Equals(simpleWordStat.WordRect)) | (wordstat.WordRect.Equals(lineWordStat.wordRect)) | (simpleWordStat.WordRect.Equals(lineWordStat.wordRect))))
                    throw new Exception("different word rects at the same case");

                Rectangle wordRect = GroupingUtils.TrimRectsToCorrect(wRects.ToArray(), wordstat.WordRect, out indexCorrect);
                if (indexCorrect == -1)
                {
                    Debug.WriteLine("Couldn't reach: " + wordstat.WordRect + " with name: " + wordstat.imageName);
                    continue;
                }
                //List<Tuple<List<ERStat>, int>> erListPerWordRect = WordStat.CreateErListPerWordRect(regions, groups2);

                WordStat wordStatWanted = new WordStat(wordRect, erListPerWordRect[indexCorrect].Item1, mImage.Size);
                WordERFeatures[] wordERFeatures = new WordERFeatures[erListPerWordRect[indexCorrect].Item1.Count];
                SimpleWordStat simpleWordStatWanted = new SimpleWordStat(wordRect, erListPerWordRect[indexCorrect].Item1, mImage.Size);
                SimpleWordErFeatures[] simpleWordERFeatures = new SimpleWordErFeatures[erListPerWordRect[indexCorrect].Item1.Count];
                LineWordStat lineWordStatWanted = new LineWordStat(wordRect, erListPerWordRect[indexCorrect].Item1, mImage.Size);
                LineWordFeatures[] lineWordFeatures = new LineWordFeatures[erListPerWordRect[indexCorrect].Item1.Count];

                for (int k = 0; k < erListPerWordRect[indexCorrect].Item1.Count; k++)
                {
                    wordERFeatures[k] = new WordERFeatures(erListPerWordRect[indexCorrect].Item1[k], channels[erListPerWordRect[indexCorrect].Item2], mImage);
                    simpleWordERFeatures[k] = new SimpleWordErFeatures(erListPerWordRect[indexCorrect].Item1[k], channels[erListPerWordRect[indexCorrect].Item2], mImage);
                    lineWordFeatures[k] = new LineWordFeatures(erListPerWordRect[indexCorrect].Item1[k], channels[erListPerWordRect[indexCorrect].Item2], mImage);
                }
                wordStatWanted.ComputeWordFeatures(wordERFeatures.ToList());
                wordStatWanted.imageName = wordstat.imageName;
                wordStatWanted.channel = erListPerWordRect[indexCorrect].Item2 == 0 ? "BgrGrayscaleF" : "BgrGrayscaleB";

                simpleWordStatWanted.Compute(simpleWordERFeatures.ToList());
                simpleWordStatWanted.imageName = simpleWordStat.imageName;
                simpleWordStatWanted.channel = erListPerWordRect[indexCorrect].Item2 == 0 ? "BgrGrayscaleF" : "BgrGrayscaleB";

                lineWordStatWanted.ComputeFeatures(allLines[indexCorrect], lineWordFeatures.ToList());
                lineWordStatWanted.imageName = lineWordStat.imageName;
                lineWordStatWanted.channel = erListPerWordRect[indexCorrect].Item2 == 0 ? "BgrGrayscaleF" : "BgrGrayscaleB";

                wordStatWanted.ToXML<WordStat>(perImageSplitWS[j].Item2);
                simpleWordStatWanted.ToXML<SimpleWordStat>(perImageSplitSWS[j].Item2);
                lineWordStatWanted.ToXML<LineWordStat>(perImageSplitLWS[j].Item2);
            }
            Disposer.DisposeMatVectorOfMatRegions(ref mImage, ref channels);
            GC.Collect();
        }

    }
}
