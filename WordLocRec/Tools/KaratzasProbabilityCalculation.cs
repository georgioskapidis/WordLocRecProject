﻿using Emgu.CV;
using Emgu.CV.ML;
using Emgu.CV.Structure;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WordLocRec.Grouping;

namespace WordLocRec.Tools
{
    class KaratzasProbabilityCalculation
    {
        private Boost boost;

        public KaratzasProbabilityCalculation()
        {
            boost = new Boost();
            FileStorage fs = new FileStorage(Constants.CLASSIFIERGROUPINGPATH, FileStorage.Mode.Read);
            FileNode fn = fs.GetFirstTopLevelNode();
            boost.Read(fn);
            if (boost == null)
            {
                throw new Exception("Exception at reading the classifier (1) at file: " + Constants.CLASSIFIERGROUPINGPATH);
            }
        }

        public double KaratzasProbability(List<ERStat> wordERs, List<WordERFeatures> wordERFeatures, Size imSize)
        {
            if (wordERs.Count != wordERFeatures.Count) throw new Exception("Gamw to spiti sou ^ 2");
            if (wordERs.Count > 50)
            {
                return 0f;
            }
            List<float> sample = new List<float>();
            sample.Add(wordERs.Count);

            Matrix<float> diameters = new Matrix<float>(wordERs.Count, 1, 1);
            Matrix<float> strokes = new Matrix<float>(wordERs.Count, 1, 1);
            Matrix<float> gradients = new Matrix<float>(wordERs.Count, 1, 1);
            Matrix<float> fg_intensities = new Matrix<float>(wordERs.Count, 1, 1);
            Matrix<float> bg_intensities = new Matrix<float>(wordERs.Count, 1, 1);
            Matrix<float> axial_ratios = new Matrix<float>(wordERs.Count, 1, 1);
            Matrix<float> chull_ratios = new Matrix<float>(wordERs.Count, 1, 1);
            Matrix<float> convexities = new Matrix<float>(wordERs.Count, 1, 1);

            float maxAvgOverlap = 0;

            for (int i = wordERs.Count - 1; i >= 0; i--) // gia ola ta ERs sto wordERs
            {
                diameters[i, 0] = (float)Math.Max(wordERs[i].rect.Width, wordERs[i].rect.Height);
                strokes[i, 0] = (float)wordERFeatures[i].StrokeWidth;
                gradients[i, 0] = (float)wordERFeatures[i].GradientMagnitude;
                fg_intensities[i, 0] = (float)wordERFeatures[i].ForegroundIntensity;
                bg_intensities[i, 0] = (float)wordERFeatures[i].BackgroundIntensity;
                axial_ratios[i, 0] = (float)Math.Max(wordERs[i].rect.Size.Width, wordERs[i].rect.Size.Height) / Math.Min(wordERs[i].rect.Size.Width, wordERs[i].rect.Size.Height);
                chull_ratios[i, 0] = (float)wordERFeatures[i].ConvexHullCompactness;
                convexities[i, 0] = (float)wordERFeatures[i].ConvexityDefects;

                float avgOverlap = 0;

                for (int j = 0; j < wordERs.Count; j++)
                {
                    if (j != i)
                    {
                        Rectangle intersection = Rectangle.Intersect(wordERs[i].rect, wordERs[j].rect);
                        int areaInter = intersection.Width * intersection.Height;
                        int areaI = wordERs[i].rect.Width * wordERs[i].rect.Height;
                        int areaJ = wordERs[j].rect.Width * wordERs[j].rect.Height;

                        if (areaInter > 0)
                        {
                            float overlap = (float)(areaInter / Math.Min(areaI, areaJ));
                            avgOverlap += overlap;
                        }
                    }
                }
                avgOverlap /= (wordERs.Count - 1);
                if (avgOverlap > maxAvgOverlap)
                    maxAvgOverlap = avgOverlap;
            }

            MCvScalar mean = new MCvScalar(), std = new MCvScalar();
            CvInvoke.MeanStdDev(diameters, ref mean, ref std); // pairnei to mean kai to std olwn twn major axis
            sample.Add((float)(std.V0 / mean.V0)); // major axis coefficient of variation
            float diameter_mean = (float)mean.V0;
            CvInvoke.MeanStdDev(strokes, ref mean, ref std); // mean kai std toy mean stroke toy kathe ER toy wordERs
            sample.Add((float)(std.V0 / mean.V0)); // stroke widths coefficient of variation
            CvInvoke.MeanStdDev(gradients, ref mean, ref std); // mean kai std toy mean gradient toy kathe ER toy wordERs
            sample.Add((float)std.V0); // standard deviation of gradients
            CvInvoke.MeanStdDev(fg_intensities, ref mean, ref std); // mean kai std toy mean intensity toy kathe ER toy wordERs
            sample.Add((float)std.V0); // standard deviation of foreground pixel intensities
            CvInvoke.MeanStdDev(bg_intensities, ref mean, ref std); // mean kai std toy mean intensity toy background toy kathe ER toy wordERs
            sample.Add((float)std.V0);

            WordStat.MSTFeatures mstFeatures = new WordStat.MSTFeatures(wordERs, imSize);

            sample.Add((float)mstFeatures.AnglesStd);
            sample.Add((float)mstFeatures.AnglesMean);

            sample.Add((float)(mstFeatures.EdgeWidthsCoeffVariation));
            sample.Add((float)(mstFeatures.EdgeDistancesMeanDiametersMeanRatio));

            CvInvoke.MeanStdDev(axial_ratios, ref mean, ref std);
            sample.Add((float)mean.V0);
            sample.Add((float)std.V0);

            /// Calculate average shape self-similarity
            double avg_shape_match = 0;
            double eps = 1e-5;
            int num_matches = 0, sma, smb;
            for (int i = 0; i < wordERFeatures.Count; i++)
            {
                for (int j = i + 1; j < wordERFeatures.Count; j++)
                {
                    for (int h = 0; h < 7; h++)
                    {
                        double ama = Math.Abs(wordERFeatures[i].HuMoments[h]);
                        double amb = Math.Abs(wordERFeatures[j].HuMoments[h]);

                        if (wordERFeatures[i].HuMoments[h] > 0)
                            sma = 1;
                        else if (wordERFeatures[i].HuMoments[h] < 0)
                            sma = -1;
                        else
                            sma = 0;
                        if (wordERFeatures[j].HuMoments[h] > 0)
                            smb = 1;
                        else if (wordERFeatures[j].HuMoments[h] < 0)
                            smb = -1;
                        else
                            smb = 0;

                        if (ama > eps && amb > eps)
                        {
                            ama = 1.0 / (sma * Math.Log10(ama));
                            amb = 1.0 / (smb * Math.Log10(amb));
                            avg_shape_match += Math.Abs(-ama + amb);
                        }
                    }
                    num_matches++;
                }
            }

            sample.Add((float)(avg_shape_match / num_matches));

            sample.Add(maxAvgOverlap);

            CvInvoke.MeanStdDev(chull_ratios, ref mean, ref std);
            sample.Add((float)mean.V0);
            sample.Add((float)std.V0);

            CvInvoke.MeanStdDev(convexities, ref mean, ref std);
            sample.Add((float)mean.V0);
            sample.Add((float)std.V0);

            Matrix<float> sampl = new Matrix<float>(sample.ToArray());

            float votes_group = boost.Predict(sampl, null, 256 | 1);

            return (double)1 - (double)1 / (1 + Math.Exp(-2 * votes_group));
        }
    }    
}
