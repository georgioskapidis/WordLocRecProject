﻿using Emgu.CV;
using Emgu.CV.CvEnum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WordLocRec.Tools
{
    public static class TestUtils
    {
        public static Mat GetTestChannel()
        {
            byte[,] m = new byte[10, 10];
            for (int i = 0; i <= 9; i++)
            {
                for (int j = 0; j <= 9; j++)
                {
                    if (i == 0 || j == 0 || i == 9 || j == 9)
                    {
                        m[i, j] = 230;
                        continue;
                    }
                    m[i, j] = 170;
                }
            }
            m[1, 3] = 230; m[1, 8] = 230;
            m[2, 2] = 230; m[2, 5] = 80; m[2, 8] = 230;
            m[3, 1] = 230; m[3, 2] = 230; m[3, 4] = 80; m[3, 5] = 80; m[3, 6] = 80; m[3, 8] = 230;
            m[4, 1] = 230; m[4, 4] = 80; m[4, 5] = 230; m[4, 6] = 80;
            m[5, 1] = 230; m[5, 3] = 80; m[5, 4] = 80; m[5, 5] = 230; m[5, 6] = 80; m[5, 7] = 80;
            m[6, 3] = 80; m[6, 4] = 80; m[6, 5] = 80; m[6, 6] = 80; m[6, 7] = 80; m[6, 8] = 80;
            m[7, 2] = 80; m[7, 3] = 80; m[7, 8] = 80;
            m[8, 5] = 230; m[8, 6] = 230;

            Matrix<byte> matrix = new Matrix<byte>(m);

            return matrix.Mat;
        }
        public static Mat GetTestChannel2()
        {
            byte[,] m = new byte[5, 3];
            m[0, 0] = 110; m[0, 1] = 90; m[0, 2] = 100;
            m[1, 0] = 50; m[1, 1] = 50; m[1, 2] = 50;
            m[2, 0] = 40; m[2, 1] = 20; m[2, 2] = 50;
            m[3, 0] = 50; m[3, 1] = 50; m[3, 2] = 50;
            m[4, 0] = 120; m[4, 1] = 70; m[4, 2] = 80;

            Matrix<byte> temp = new Matrix<byte>(m);
            Mat mat = new Mat(5, 3, DepthType.Cv8U, 1);
            
            mat.SetTo(temp);
            //CvInvoke.BitwiseNot(mat, mat);
            return mat;
        }
        public static Mat GetTestChannel3()
        {
            byte[,] m = new byte[10, 10];
            for (int i = 0; i <= 9; i++)
            {
                for (int j = 0; j <= 9; j++)
                {
                    if (i == 0 || j == 0 || i == 9 || j == 9)
                    {
                        m[i, j] = 230;
                        continue;
                    }
                    m[i, j] = 80;
                }
            }
            m[1, 3] = 230; m[1, 8] = 230;
            m[2, 2] = 230; m[2, 5] = 170; m[2, 8] = 230;
            m[3, 1] = 230; m[3, 2] = 230; m[3, 4] = 170; m[3, 5] = 170; m[3, 6] = 170; m[3, 8] = 230;
            m[4, 1] = 230; m[4, 4] = 170; m[4, 5] = 230; m[4, 6] = 170;
            m[5, 1] = 230; m[5, 3] = 170; m[5, 4] = 170; m[5, 5] = 230; m[5, 6] = 170; m[5, 7] = 170;
            m[6, 3] = 170; m[6, 4] = 170; m[6, 5] = 170; m[6, 6] = 170; m[6, 7] = 170; m[6, 8] = 170;
            m[7, 2] = 170; m[7, 3] = 170; m[7, 8] = 170;
            m[8, 5] = 230; m[8, 6] = 230;

            Matrix<byte> matrix = new Matrix<byte>(m);

            return matrix.Mat;
        }
    }
}
