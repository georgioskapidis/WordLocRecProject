﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WordLocRec.Tools
{
    public static class EvalUtils
    {
        public static double CalcRecallAreaRect(RectangleF Gi, RectangleF Di)
        {
            RectangleF intersection = RectangleF.Intersect(Gi, Di);
            double areaInter = intersection.Width * intersection.Height;
            double areaG = Gi.Width * Gi.Height;
            return (double)areaInter / areaG;
        }
        public static double CalcPrecAreaRect(RectangleF Gi, RectangleF Di)
        {
            RectangleF intersection = RectangleF.Intersect(Gi, Di);
            double areaInter = intersection.Width * intersection.Height;
            double areaD = Di.Width * Di.Height;
            return (double)areaInter / areaD;
        }

        public static bool GroundTruthInclusionRelation(double[,] ypsilon, int gtI, List<int> indiceList)
        {
            foreach (int indJ in indiceList)
            {
                if (gtI != indJ)
                {
                    if (ypsilon[gtI, indJ] == 1)
                    {
                        return true;
                    }
                }
            }
            return false;
        }

        public static float[] CalcMatches(double[,] sigma, double[,] taf, double[,] ypsilon, double tr, double tp, List<RectangleF> gtRects, List<RectangleF> detRects, out float[] matchD)
        {
            int numOfGT = sigma.GetLength(0);
            int numOfDet = sigma.GetLength(1);
            float[] matchValues = new float[numOfGT];
            //forVis = new int[numOfDet];
            List<int>[] gtMatches11 = new List<int>[numOfGT];
            List<int>[] gtMatches1N = new List<int>[numOfGT];
            double[] totalSigma1N = new double[numOfGT];
            List<int>[] gtMatchesN1 = new List<int>[numOfGT];
            for (int i = 0; i < numOfGT; i++)
            {
                matchValues[i] = 0f;
                gtMatches11[i] = new List<int>(); // mia lista gia kathe gt poy na periexei ola ta pithana 1-1
                gtMatches1N[i] = new List<int>(); // mia lista gia kathe gt poy tha periexei ena 1-N tairiasma. Ena 1-N tairiasma tha einai mia List<int> poy tha perilamvanei ena apo ta N stoixeia tou tairiasmatos. Tha eixa List<List<int>> se periptwsh poy yphrxe h dunatothta perissoterwn toy enos 1-N gia ena gt
                gtMatchesN1[i] = new List<int>(); // mia lista gia kathe gt poy tha perilamvanei to det apo to opoio kaluptetai tmhmatika
                totalSigma1N[i] = 0d;
            }
            matchD = new float[numOfDet];
            for (int i = 0; i < numOfDet; i++)
            {
                matchD[i] = 0f;
                //forVis[i] = 0;
            }

            #region 1-1
            for (int i = 0; i < numOfGT; i++)
            {
                for (int j = 0; j < numOfDet; j++)
                {
                    if (sigma[i, j] > tr && taf[i, j] > tp)
                    {
                        gtMatches11[i].Add(j);
                    }
                }
            }
            #endregion

            #region 1-N
            double sumsSigma;
            int setSize = 0;
            List<int> tempUsedIndices; // ta rects poy xrhsimopoiountai gia ton entopismo kapoiou gt
            for (int i = 0; i < numOfGT; i++)
            {
                sumsSigma = 0f;
                setSize = 0;
                tempUsedIndices = new List<int>();
                for (int j = 0; j < numOfDet; j++)
                {
                    if (taf[i, j] >= tp)
                    {
                        sumsSigma += sigma[i, j];
                        tempUsedIndices.Add(j);
                        setSize++;
                    }
                }
                #region overlap test

                if (!(sumsSigma >= tr && setSize > 1)) continue;
                sumsSigma = 0;
                setSize = 0;
                Rectangle groundTruth = Rectangle.Ceiling(gtRects[i]);
                List<Tuple<Rectangle, double, int>> detectedRecalls = new List<Tuple<Rectangle, double, int>>();
                List<double> exclusiveRecalls = new List<double>();
                for (int m = 0; m < tempUsedIndices.Count; m++)
                {
                    Rectangle det = Rectangle.Ceiling(detRects[tempUsedIndices[m]]);
                    detectedRecalls.Add(Tuple.Create(det, EvalUtils.CalcRecallAreaRect(groundTruth, det), tempUsedIndices[m])); // krataw to rectangle, to recall toy kai to index apo to tempusedindices
                }
                tempUsedIndices = new List<int>();
                detectedRecalls.Sort((x, y) => x.Item2.CompareTo(y.Item2)); //sort kata to recall. to prwto einai to mikrotero

                for (int iLocal = 0; iLocal < detectedRecalls.Count - 1; iLocal++)
                {
                    double maxCoverArea = 0d;
                    int maxCoverAreaIndex = -1;
                    for (int jLocal = detectedRecalls.Count - 1; jLocal > iLocal; jLocal--)
                    {
                        double tempCoverArea = EvalUtils.CalcRecallAreaRect(detectedRecalls[iLocal].Item1, detectedRecalls[jLocal].Item1);
                        if (tempCoverArea > maxCoverArea)
                        {
                            maxCoverArea = tempCoverArea;
                            maxCoverAreaIndex = jLocal;
                        }
                    }
                    if (maxCoverAreaIndex != -1) //an kaluptetai merikws h plhrws apo kapoio allo detected
                    {
                        //to exclusive recall einai auto poy prosferei sthn anakthsh to sygkekrimeno det apokleistika
                        Rectangle intersectionIJ = Rectangle.Intersect(detectedRecalls[iLocal].Item1, detectedRecalls[maxCoverAreaIndex].Item1);
                        exclusiveRecalls.Add(detectedRecalls[iLocal].Item2 - EvalUtils.CalcRecallAreaRect(groundTruth, intersectionIJ));
                    }
                    else
                    {
                        exclusiveRecalls.Add(detectedRecalls[iLocal].Item2);
                    }
                    if (exclusiveRecalls[iLocal] > 0)
                    {
                        sumsSigma += exclusiveRecalls[iLocal];
                        setSize++;
                        tempUsedIndices.Add(detectedRecalls[iLocal].Item3); // krataw to index toy det
                    }

                }
                // gia to teleutaio kai megalutero (se recall) detected den kanw epanalhpsh giati exei to megalutero exclusive recall
                exclusiveRecalls.Add(detectedRecalls[detectedRecalls.Count - 1].Item2);
                sumsSigma += exclusiveRecalls[exclusiveRecalls.Count - 1];
                setSize++;
                tempUsedIndices.Add(detectedRecalls[detectedRecalls.Count - 1].Item3);

                #endregion
                if (sumsSigma >= tr && setSize > 1)
                {
                    gtMatches1N[i].AddRange(tempUsedIndices);
                    totalSigma1N[i] = sumsSigma;
                }
                else if (sumsSigma >= tr && setSize == 1)
                {
                    gtMatches11[i].Add(tempUsedIndices[0]);
                }
                //else just discard the match
            }
            #endregion

            #region N-1
            setSize = 0;
            double sumsTaf = 0f;
            List<int>[] indiceList = new List<int>[numOfDet]; // xrhsimopoieitai gia na diathrountai ta gt poy kaluptei kathe det
            for (int j = 0; j < numOfDet; j++)
            {
                indiceList[j] = new List<int>();
                sumsTaf = 0f;
                setSize = 0;
                for (int i = 0; i < numOfGT; i++)
                {
                    if (sigma[i, j] >= tr)
                    {
                        sumsTaf += taf[i, j];
                        setSize++;
                        indiceList[j].Add(i);
                    }
                }
                if (sumsTaf >= tp && setSize > 1)
                {
                    for (int l = 0; l < indiceList[j].Count; l++)
                    {
                        gtMatchesN1[indiceList[j][l]].Add(j);
                    }
                }
                else
                {
                    indiceList[j] = new List<int>();
                }
            }
            #endregion

            #region match selection
            for (int i = 0; i < numOfGT; i++)
            {
                bool exist11 = gtMatches11[i].Count > 0 ? true : false;
                bool exist1N = gtMatches1N[i].Count > 0 ? true : false;
                bool existN1 = gtMatchesN1[i].Count > 0 ? true : false;
                List<List<int>> matches = new List<List<int>>();

                if (!exist11 && !exist1N && !existN1) continue;

                if (exist11)
                {
                    if (existN1) //elegxos pleonasmou twn 1-1
                    {
                        bool gtExistsInGT = true;
                        foreach (int det in gtMatchesN1[i])
                        {
                            gtExistsInGT = EvalUtils.GroundTruthInclusionRelation(ypsilon, i, indiceList[det]);
                            if (gtExistsInGT) break;
                        }
                        if (!gtExistsInGT) //ta 1-1 einai pleonazonta opote diagrafontai
                        {
                            gtMatches11[i] = new List<int>();
                        }
                    }
                    foreach (var match in gtMatches11[i])
                    {
                        List<int> tmpList = new List<int>();
                        tmpList.Add(match); // monadiaia lista
                        matches.Add(tmpList); // ta 1-1 tha summetexoun ston teliko elegxo gia ta fmeasure
                    }
                }
                if (exist1N)
                {
                    if (existN1) //elegxos pleonasmou twn 1-1
                    {
                        bool gtExistsInGT = true;
                        foreach (int det in gtMatchesN1[i])
                        {
                            gtExistsInGT = EvalUtils.GroundTruthInclusionRelation(ypsilon, i, indiceList[det]);
                            if (gtExistsInGT) break;
                        }
                        if (!gtExistsInGT) //ta 1-1 einai pleonazonta opote diagrafontai
                        {
                            gtMatches1N[i] = new List<int>();
                        }
                    }
                    if (gtMatches1N[i].Count > 0)
                    {
                        matches.Add(gtMatches1N[i]); // ta 1-N tha summetexoun ston teliko elegxo gia ta fmeasure
                    }
                }
                if (existN1) //more than 1 N-1 matches, ara prepei na krathsw mono to megalutero
                {
                    // find the indices of the det that find this gt and extract their gt lists.
                    List<Tuple<List<int>, int>> indiceListIndex = new List<Tuple<List<int>, int>>();
                    for (int k = 0; k < gtMatchesN1[i].Count; k++)
                    {
                        int detId = gtMatchesN1[i][k];
                        indiceListIndex.Add(Tuple.Create<List<int>, int>(indiceList[detId], detId));
                    }
                    // sort the gt list of each det that finds this gt
                    indiceListIndex.Sort((x, y) => x.Item1.Count.CompareTo(y.Item1.Count));

                    // search the gt list of each det to find any unique gts i.e. gt rects that are not found by other det rects.
                    for (int ii = 0; ii < indiceListIndex.Count; ii++) // each det
                    {
                        // aIndList = the gt list of this det
                        List<int> aIndList = indiceListIndex[ii].Item1; // each gt list
                        int[] foundIndices = new int[aIndList.Count];
                        bool allIndicesFound = true;
                        double maxSumSigma = 0d;
                        for (int k = 0; k < aIndList.Count; k++) // each gt list
                        {
                            int aIndice = aIndList[k];
                            foundIndices[k] = -1;
                            // search all the other gt lists
                            for (int jj = indiceListIndex.Count - 1; jj > ii; jj--)
                            {
                                //if (ii == jj) continue;
                                List<int> bIndList = indiceListIndex[jj].Item1; // another gt list
                                foreach (var bIndice in bIndList)
                                {
                                    if (aIndice == bIndice) //indice from List A found
                                    {
                                        foundIndices[k] = jj;
                                        break;
                                    }
                                }
                                if (foundIndices[k] != -1)
                                {
                                    break;
                                }
                            }
                            if (foundIndices[k] == -1) //index not found in any of the indice lists, shmainei monadikh euresh
                            {
                                List<int> tmpList = new List<int>();
                                tmpList.Add(indiceListIndex[ii].Item2);
                                matches.Add(tmpList); //TODO: den prepei na mpainei edw gia to teleutaio N-1 nte kai kala
                                allIndicesFound = false;
                                break;
                            }
                        }
                        if (allIndicesFound)
                        {
                            bool abort = false;
                            for (int k = 0; k < foundIndices.Length - 1; k++)
                            {
                                if (foundIndices[k] != foundIndices[k + 1])
                                {
                                    abort = true;
                                    break;
                                }
                            }
                            if (abort) continue;
                            // an ftasei edw tote shmainei oti vrhke ola ta stoixeia ths listas alla mono se mia
                            // opote elegxw megethos
                            int aListSize = aIndList.Count;
                            int bListSize = indiceListIndex[foundIndices[0]].Item1.Count;
                            if (aListSize < bListSize) abort = true;
                            if (abort) continue;
                            // an ftasei edw tote exoun idio megethos opote elegxw ta recall gia na dw an tha thn krathsw H' oxi
                            double aSumsSigma = 0d, bSumsSigma = 0d;
                            foreach (var indice in aIndList)
                            {
                                aSumsSigma += sigma[indice, indiceListIndex[ii].Item2];
                                bSumsSigma += sigma[indice, indiceListIndex[foundIndices[0]].Item2];
                            }
                            // aSumsSigma has the best recall, so save it
                            if (aSumsSigma >= bSumsSigma && aSumsSigma > maxSumSigma)
                            {
                                maxSumSigma = aSumsSigma;
                                List<int> tmpList = new List<int>();
                                tmpList.Add(indiceListIndex[ii].Item2);

                                if (matches.Count > 0)
                                {
                                    matches[matches.Count - 1] = tmpList; // replace maxSumSigma parolo poy theoritika einai i idia lista
                                }
                                else
                                {
                                    matches.Add(tmpList);
                                }

                                indiceListIndex.RemoveAt(foundIndices[0]);
                                if (foundIndices[0] < ii)
                                    ii--;
                            }
                            else if (aSumsSigma < bSumsSigma) // svhse to aSumSigma gt einai xeirotero apo to bSumsSigma
                            {
                                indiceListIndex.RemoveAt(ii);
                                ii--;
                            }
                            //remove kai ta duo
                            else if (maxSumSigma >= aSumsSigma && maxSumSigma >= bSumsSigma)
                            {
                                indiceListIndex.RemoveAt(ii);
                                foundIndices[0]--;
                                indiceListIndex.RemoveAt(foundIndices[0]);
                            }
                        }
                    }
                }
                #region fmeasure calculation
                int numOfMatches = matches.Count;
                float[] fMeasures = new float[numOfMatches];
                for (int l = 0; l < numOfMatches; l++)
                {
                    if (matches[l].Count > 1) // 1 - N match
                    {
                        double totalPrecisions = 0d;
                        for (int ll = 0; ll < matches[l].Count; ll++)
                        {
                            totalPrecisions += taf[i, matches[l][ll]];
                        }
                        totalPrecisions /= matches[l].Count;
                        fMeasures[l] = (float)(2 * (totalSigma1N[i] * totalPrecisions) / (totalSigma1N[i] + totalPrecisions));
                    }
                    else // N-1 or 1-1
                    {
                        fMeasures[l] = (float)(2 * (sigma[i, matches[l][0]] * taf[i, matches[l][0]]) / (sigma[i, matches[l][0]] + taf[i, matches[l][0]]));
                    }
                }
                #endregion

                #region choose match
                float maxFmeasure = -1f; int maxFmeasureInd = -1;
                for (int l = 0; l < fMeasures.Length; l++)
                {
                    if (fMeasures[l] > maxFmeasure)
                    {
                        maxFmeasure = fMeasures[l];
                        maxFmeasureInd = l;
                    }
                }
                if (maxFmeasure > 0)
                {
                    if (matches[maxFmeasureInd].Count > 1)
                    {
                        matchValues[i] = 0.8f;
                        Debug.Write("Found gt#" + (i + 1) + " by det#");
                        foreach (var detectedRect in matches[maxFmeasureInd])
                        {
                            matchD[detectedRect] = 0.8f;
                            //forVis[detectedRect] = -2;
                            Debug.Write("" + (detectedRect + 1) + ",");
                        }
                        Debug.WriteLine("");
                    }
                    else
                    {
                        matchValues[i] = 1f;
                        matchD[matches[maxFmeasureInd][0]] = 1f;
                        //if (forVis[matches[maxFmeasureInd][0]] == 0)
                        //{
                        //    forVis[matches[maxFmeasureInd][0]] = -1;
                        //}
                        //else if (forVis[matches[maxFmeasureInd][0]] == -1)
                        //{
                        //    forVis[matches[maxFmeasureInd][0]] = -3;
                        //}
                        Debug.WriteLine("Found gt#" + (i + 1) + " by det#" + (matches[maxFmeasureInd][0] + 1));
                    }
                }
                else
                {
                    matchValues[i] = 0f;
                }
                #endregion
            }
            #endregion

            return matchValues;
        }
    }
}
