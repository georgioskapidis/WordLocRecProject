﻿using Emgu.CV;
using Emgu.CV.Features2D;
using Emgu.CV.Structure;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WordLocRec.Tools
{
    static class MSERWrapper
    {
        public static MKeyPoint[] DetectMSERs(Mat mGrayscaleImage)
        {
            // create Mser detector with default values
            // for more info about parameters check here: http://goo.gl/4D21vG
            MSERDetector mserDetector = new MSERDetector();

            // detect the keypoints
            MKeyPoint[] mKeyPoints = mserDetector.Detect(mGrayscaleImage);

            return mKeyPoints;
        }
        public static List<Rectangle> ExtractRectsFromKeyPoints(MKeyPoint[] keypoints)
        {
            List<Rectangle> rectangles = new List<Rectangle>();
            foreach (MKeyPoint key in keypoints)
            {
                // get the region around the keypoint as save it as a rectangle
                rectangles.Add(RectangleFromKeyPoint(key));
            }
            return rectangles;
        }
        public static Rectangle RectangleFromKeyPoint(MKeyPoint key)
        {
            float a = (float)key.Size / 2;
            PointF mPointF = new PointF(key.Point.X - a, key.Point.Y - a - 5);
            float sizeX = 2 * a + 3;
            float sizeY = 2 * a + 7;

            if (mPointF.X < 0)
            {
                float xDiff = -mPointF.X;
                sizeX = sizeX - xDiff;
                mPointF.X = 0;
            }
            if (mPointF.Y < 0)
            {
                float yDiff = -mPointF.Y;
                sizeY = sizeY - yDiff;
                mPointF.Y = 0;
            }
            SizeF mSizeF = new SizeF(sizeX, sizeY);

            return Rectangle.Ceiling(new RectangleF(mPointF, mSizeF));
        }
    }
}
