﻿using Emgu.CV;
using Emgu.CV.CvEnum;
using Emgu.CV.ML;
using Emgu.CV.Structure;
using Emgu.CV.UI;
using Emgu.CV.Util;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Threading;
using System.Windows.Forms;
using WordLocRec.DataStructs;
using WordLocRec.Forms;
using WordLocRec.Grouping;
using WordLocRec.SVMStuff;

namespace WordLocRec.Tools
{
    class Experiments
    {
        //Mat image = _originalImage == null? new Mat(FormUtils.ParametrizedOpenFileDialog(Constants.IMAGE_FILE_FILTER, false, "Select an image")[0], LoadImageType.Unchanged): _originalImage.Mat.Clone();
        //if (image.Rows == 0 || image.Cols == 0) return;
        //Utils.MatToBgrImageC(image, out _originalImage);
        //ImagePreprocessing(ref image);
        //Rectangle[] wordRects = Experiments.TrainDTree<LineWordStat, RTrees>(image, FormUtils.ParametrizedOpenFileDialog(Constants.XML_FILE_FILTER, true, "Select the positive"), FormUtils.ParametrizedOpenFileDialog(Constants.XML_FILE_FILTER, true, "Select the negative"), true, 0);
        ////Rectangle[] wordRects = Experiments.EvalDTreeOnImage(FormUtils.ParametrizedOpenFileDialog(Constants.XML_YML_FILE_FILTER, false, "Select the dtree")[0], image, Constants.FeatureType.StandarizedPerFeature);
        //VisualizeAllChannelsTogether(wordRects, true, 2);

        private static IStatModel omg;
        // train model of type R with data of feature T and evaluate on loaded image. code to call from main form above
        public static Rectangle[] TrainDTree<T, R>(Mat mImage, String[] positiveFilenames, String[] negativeFilenames, bool splitDatasets = true, Constants.FeatureType f3 = Constants.FeatureType.StandarizedPerFeature) where T: BaseFeature where R:IStatModel
        {
            if (positiveFilenames == null || negativeFilenames == null) return new Rectangle[0];
            String[] trainSetPos, testSetPos, trainSetNeg, testSetNeg;
            if (splitDatasets)
            {
                Utils.PickPercAtRandom<String>(positiveFilenames, out trainSetPos, out testSetPos);
                Utils.PickPercAtRandom<String>(negativeFilenames, out trainSetNeg, out testSetNeg);
            }
            else
            {
                trainSetPos = positiveFilenames;
                trainSetNeg = negativeFilenames;
                testSetPos = new String[0];
                testSetNeg = new String[0];
            }

            SVMCreator svmCreator1 = new SVMCreator(trainSetPos, testSetPos, trainSetNeg, testSetNeg, f3);
            svmCreator1.CreateTrainTestSetAndClasses<T>();

            if (typeof(R).Equals(typeof(SVM)))
            {
                svmCreator1.MakeSVMAuto(20);
                omg = svmCreator1.currentModel;

                svmCreator1.Evaluate<R>();
                String p = "C:\\Users\\George\\Desktop\\SVMWordExample.xml";
                SvmUtils.SaveSVMToFile((SVM)omg, p);
            }
            else if (typeof(R).Equals(typeof(RTrees)))
            {
                svmCreator1.MakeRtreesAuto();
                omg = svmCreator1.currentRTree;

                svmCreator1.Evaluate<R>();
                String p = "C:\\Users\\George\\Desktop\\RtreeExample.xml";
                SvmUtils.SaveRTreesToFile((RTrees)omg, p);
                SvmUtils.FixXML(p);
            }
            else
            {
                svmCreator1.MakeDTreesAuto();
                omg = svmCreator1.currentMod;

                svmCreator1.Evaluate<R>();
                String p = "C:\\Users\\George\\Desktop\\DtreeExample.xml";
                SvmUtils.SaveDTreesToFile((DTrees)omg, p);
                SvmUtils.FixXML(p);
            }

            // eval on image

            return EvalWordModelOnImage(omg, mImage, f3);
        }
        public static Rectangle[] EvalWordModelOnImage(IStatModel model, Mat mImage, Constants.FeatureType f3)
        {
            Constants.FeatureType f1 = Constants.FeatureType.ColorDistLab | Constants.FeatureType.ColorDistPii | Constants.FeatureType.StandarizedPerFeature;
            Constants.FeatureType f2 = Constants.FeatureType.NM;

            VectorOfMat channels = new VectorOfMat();
            Utils.CreateXXChannels(ref channels, mImage, Constants.ColorSpace.Grayscale, Constants.VisChannels.Channel1 | Constants.VisChannels.Channel1Opp, false, Constants.Gradients.None);

            Rectangle[] wordRects;

            List<ChineseGrouping.Line[]> linesPerChannel;
            ERExtractor.ExecuteDefaultExtractorPipelineLineGrouping(mImage, channels, f1, f2, out linesPerChannel);

            List<Rectangle> wRects;
            List<ChineseGrouping.Line> allLines;
            List<Tuple<List<ERStat>, int>> erListPerWordRect = LineWordStat.CreateErListPerWordRect(linesPerChannel, out wRects, out allLines);

            BaseFeature[] wordStats;
            //extract the features
            wordStats = LineWordStat.CreateMultiple(wRects.ToArray(), allLines.ToArray(), erListPerWordRect, mImage, channels, "");

            wordRects = ERUtils.WordPruning<SVMLineWordStatSample>(wordStats, model, wRects.ToArray(), true, f3);

            return wordRects;
        }

        #region oldExperiments
        public static void ThinningAndPathsDemo()
        {
            Mat m = new Mat("C:\\Users\\George\\Desktop\\img_1croppedE.jpg", LoadImageType.Grayscale);
            //Mat m = new Mat("C:\\Users\\George\\Desktop\\cross.jpg", LoadImageType.Grayscale);
            CvInvoke.Threshold(m, m, 127, 255, ThresholdType.BinaryInv);

            Mat skeleton = new Mat();

            ZhangSuenThinning tt = new ZhangSuenThinning();
            int steps = tt.Skeletonize(m, ref skeleton /*, ref gif2 */); // result is binary 0 - 255
            //skeleton.Save("C:\\Users\\George\\Desktop\\crossSkel.jpg");
            Mat neighbourMat = new Mat(skeleton.Rows, skeleton.Cols, DepthType.Cv8U, 1);
            StrokeUtils.CreateNeighbourMat(skeleton, ref neighbourMat);

            double minVal = -1, maxVal = 0;
            Point minLoc = new Point(), maxLoc = new Point();
            CvInvoke.MinMaxLoc(neighbourMat, ref minVal, ref maxVal, ref minLoc, ref maxLoc, skeleton);

            List<int>[] neighboursArr = StrokeUtils.GetNeighbourIndices(skeleton);
            StrokeUtils.SortNeighboursToDistance(ref neighboursArr, skeleton.Cols);

            PathStats pathStats = StrokeUtils.FindPaths(skeleton, neighboursArr, minLoc);

            VectorOfMat vom = new VectorOfMat();
            vom.Push(skeleton);
            WordRectangleForm wrf = new WordRectangleForm(vom, "");
            wrf.Show();
        }
        public static void FindContours()
        {
            Mat m = new Mat("C:\\Users\\George\\Desktop\\img_1croppedE.jpg", LoadImageType.Grayscale);
            //Mat m = new Mat("C:\\Users\\George\\Desktop\\cross.jpg", LoadImageType.Grayscale);
            CvInvoke.Threshold(m, m, 127, 255, ThresholdType.Binary);
            new ImageViewer(m.Clone(), "original").Show();
            Mat res = new Mat(m.Size, DepthType.Cv8U, 1);
            res.SetTo(new MCvScalar(0));
            VectorOfVectorOfPoint contours = new VectorOfVectorOfPoint();

            Mat hierarchy = new Mat();

            //CvInvoke.FindContours(m, contours, hierarchy, RetrType.Tree, ChainApproxMethod.ChainApproxNone, new Point(0, 0));

            //VectorOfPoint contourPoly = new VectorOfPoint();
            //CvInvoke.ApproxPolyDP(contours[0], contourPoly, float.Epsilon, true);
            int[,] hierar = CvInvoke.FindContourTree(m, contours, ChainApproxMethod.ChainApproxNone);
            int sumContourPoints = 0;

            for (int i = 0; i < hierar.GetLength(0); i++)
            {
                sumContourPoints += contours[i].Size;
                CvInvoke.DrawContours(res, contours, i, new MCvScalar(100, 100, 100, 0));
                int num = CvInvoke.CountNonZero(res);
            }
                

            new ImageViewer(res).Show();
        }
        public static void ThinningDemo()
        {
            Mat m = new Mat("C:\\Users\\George\\Desktop\\img_1croppedE.jpg", LoadImageType.Grayscale);

            CvInvoke.Threshold(m, m, 127, 255, ThresholdType.BinaryInv);

            VectorOfMat gif2 = new VectorOfMat();//,gif2 = new VectorOfMat();

            Mat skeleton = new Mat();

            //StrokeUtils.RunCa(m, ref skeleton, ref gif);

            ZhangSuenThinning tt = new ZhangSuenThinning();
            int steps = tt.Skeletonize(m, ref skeleton /*, ref gif2 */); // result is binary 0 - 255

            //Utils.CreateGifFromVecMat(gif, "skelSer");
            Utils.CreateGifFromVecMat(gif2, "skelPar");

            //Mat neighbourMat = new Mat(skeleton.Rows, skeleton.Cols, DepthType.Cv8U, 1);
            //StrokeUtils.CreateNeighbourMat(skeleton, ref neighbourMat);

            //double minVal = -1, maxVal = 0;
            //Point minLoc = new Point(), maxLoc = new Point();
            //CvInvoke.MinMaxLoc(neighbourMat, ref minVal, ref maxVal, ref minLoc, ref maxLoc, skeleton);

            //Mat result = new Mat();
            //List<int> pathLengths;
            //StrokeUtils.CreatePaths(skeleton, neighbourMat, minLoc, out pathLengths, ref result);

            //int trimmed = pathLengths.Count(n => n > 3);

        }

        public static void RetrieveERsAndPathsVisual(String[] erFiles)
        {
            String imageName = "";
            String channel = "";
            //Mat mBgr;
            Mat m = new Mat(), piiImage = new Mat();
            ERExtractor ere;
            List<ERStat> regions = new List<ERStat>();

            VectorOfMat erMats = new VectorOfMat();
            VectorOfMat skeletons = new VectorOfMat();
            String[] texts = new String[erFiles.Length];

            for (int i = 0; i < erFiles.Length; i++)
            {
                FullFeatureObject ffo = IOUtils.FromXml<FullFeatureObject>(erFiles[i]);

                if ((imageName != ffo.imageName || channel != ffo.channel))
                {
                    imageName = ffo.imageName;
                    channel = ffo.channel;
                    if (File.Exists(imageName))
                    {
                        m = new Mat(imageName, LoadImageType.Grayscale);
                        if (channel.EndsWith("B"))
                        {
                            CvInvoke.BitwiseNot(m, m);
                        }
                        regions.Clear();
                        ere = new ERExtractor(new ERExtractor.ExtractorParameters(1, 0.00005, 0.96, 0, 0, m.Width, m.Height), ERUtils.SizeRestrictionsArea);
                        regions = ere.extractERs(m, true, false);
                        regions = RecursiveFunctions.NonCerSuppression(regions);
                        //regions = RecursiveFunctions.NearDuplicateSuppression(regions, true);
                    }
                    else // copy the images in the correct path
                    {
                    }
                }
                ERStat stat = ERUtils.SearchRegions(regions, ffo);
                if (stat != null) // found the er so we can do all kinds of computations with it now
                {
                    Mat erPixels = Utils.ErMaskFloodFill(m, stat);
                    erMats.Push(erPixels.Clone());
                        
                    Mat skeleton = new Mat();
                    ZhangSuenThinning tt = new ZhangSuenThinning();
                    tt.Skeletonize(erPixels, ref skeleton);
                    skeletons.Push(skeleton.Clone());

                    Mat neighbourMat = new Mat(skeleton.Rows, skeleton.Cols, DepthType.Cv8U, 1);
                    StrokeUtils.CreateNeighbourMat(skeleton, ref neighbourMat);

                    double minVal = -1, maxVal = 0;
                    Point minLoc = new Point(), maxLoc = new Point();
                    CvInvoke.MinMaxLoc(neighbourMat, ref minVal, ref maxVal, ref minLoc, ref maxLoc, skeleton);

                    List<int>[] neighboursArr = StrokeUtils.GetNeighbourIndices(skeleton);
                    StrokeUtils.SortNeighboursToDistance(ref neighboursArr, skeleton.Cols);

                    PathStats pathStats = StrokeUtils.FindPaths(skeleton, neighboursArr, minLoc);                        
                    texts[i] = pathStats.ToString();
                    //ffo.significantPaths = pathStats.PathLengths.Count(n => n > 3);
                    //ffo.ToXML(erFiles.FileNames[i]);

                    //piiImage.Dispose();
                }
                else // not found :(
                {
                    continue;                           
                }                  
            }

            VectorOfMat[] mmats = new VectorOfMat[2];
            mmats[0] = erMats;
            mmats[1] = skeletons;
            MultipleWordRectangleForm mwrf = new MultipleWordRectangleForm(mmats, "Multiple", texts);
            mwrf.Show();

            m.Dispose();
            regions.Clear();
            GC.Collect();        

        }

        /// <summary>
        /// Er extraction at an image. All the ERs are skeletonized. ER pixels and skeletons are viewed side by side for qualitative evaluation.
        /// </summary>
        /// <param name="channel"></param>
        public static void ViewERBinaryMats(Mat channel)
        {
            ERExtractor ere;
            List<ERStat> regions = new List<ERStat>();
            ere = new ERExtractor(new ERExtractor.ExtractorParameters(1, 0.00005, 0.96, 0, 0, channel.Width, channel.Height), ERUtils.SizeRestrictionsAcceptAll);
            regions = ere.extractERs(channel, true, false);
            
            VectorOfMat erMats = new VectorOfMat();
            VectorOfMat skeletons = new VectorOfMat();
            String[] texts = new String[regions.Count];
            
            for (int i = 0; i < regions.Count; i++)
            {
                Mat erPixels = new Mat(), erPixels2 = new Mat();
                //Utils.BinaryMat2(regions[i], ref erPixels2, false);
                Utils.BinaryMatFromERPixels(regions[i], channel, ref erPixels);
                erMats.Push(erPixels.Clone());
                skeletons.Push(erPixels2.Clone());
                texts[i] = "";
            }

            VectorOfMat[] mmats = new VectorOfMat[2];
            mmats[0] = erMats;
            mmats[1] = skeletons;
            MultipleWordRectangleForm mwrf = new MultipleWordRectangleForm(mmats, "Multiple", texts);
            mwrf.Show();
        }
        #endregion

        #region Utils
        public static void makeTree() // given test image to make specific tree for visualization and save as gif
        {
            Mat m = new Mat("C:\\Users\\George\\Desktop\\presentation\\106Trains.jpg", LoadImageType.Unchanged);
            VectorOfMat vec = new VectorOfMat(); CvInvoke.Split(m, vec);
            Mat m2 = new Mat(vec[0], Rectangle.FromLTRB(62, 25, 73, 33));


            ERExtractor.ExtractorParameters ep = new ERExtractor.ExtractorParameters(1, 0, 1, 0, 0, m2.Width, m2.Height);

            ERExtractor ere = new ERExtractor(ep, ERUtils.SizeRestrictionAccept109);

            List<ERStat> regions = ere.extractERs(m2, false, false);
            regions = RecursiveFunctions.NonCerSuppression(regions);


            //regions = RecursiveFunctions.NearDuplicateSuppression(regions, true);

            VectorOfMat erMats = new VectorOfMat();
            Utils.MultipleMatsFromERPixelsFloodFill(m2, regions, ref erMats, true);

            Utils.CreateGifFromVecMat(erMats, "cersNew");
        }
        public static void CreateGif(String[] filenames)
        {
            if (filenames == null) return;

            VectorOfMat vec = new VectorOfMat();
            for (int i = 0; i < filenames.Length; i++)
            {
                vec.Push(new Mat(filenames[i], LoadImageType.Unchanged));
            }

            Utils.CreateGifFromVecMat(vec, "ERBuildup", 500);


        }

        #endregion

        #region Classifier - Create and Evaluate

        public static void CreateClassifierOneClass<T>(String[] filenames, String outputFolder, String outputName) where T : BaseFeature
        {
            if (filenames == null || outputFolder == null || outputName == null) return;

            Constants.FeatureType f = 0;
            Constants.FeatureType fStand = Constants.FeatureType.StandarizedPerFeature;
            List<Constants.FeatureType> fTypes = new List<Constants.FeatureType>();
            fTypes.Add(f); fTypes.Add(fStand);

            List<SVMCreatorOneClass> models = new List<SVMCreatorOneClass>();
            foreach (var ftype in fTypes)
            {
                SVMCreatorOneClass svmDef = new SVMCreatorOneClass(filenames, ftype);
                SVMCreatorOneClass svmUndef = new SVMCreatorOneClass(filenames, ftype);
                svmDef.CreateTrainTestSetAndClasses<T>();
                svmUndef.CreateTrainTestSetAndClasses<T>();
                svmDef.MakeSvmAuto(15, true);
                svmUndef.MakeSvmAuto(0, false);

                float[] percDefIt = svmDef.Evaluate();
                float[] percUndefIt = svmUndef.Evaluate();

                if (Constants.VERBOSE)
                {
                    Debug.WriteLine(ftype + " Defined Iterations model " + svmDef.ToString(true));
                    Debug.WriteLine(ftype + " Undefined Iterations model " + svmUndef.ToString(true));
                }

                models.Add(svmDef);
                models.Add(svmUndef);
            }
            int counter = -1;
            for (int i = 0; i < models.Count; i++)
            {
                String iter = "";
                if (i > 1) iter = "Standardized_";
                if (i % 2 == 0)
                {
                    iter += " DefIter";
                    counter++;
                }
                else
                {
                    iter += " UndefIter";
                }
                String path = Path.Combine(outputFolder, outputName + iter + ".xml");
                SvmUtils.SaveSVMToFile(models[i].currentModel, path);
            }
        }

        public static void CreateClassifier<T>(String[] positiveFilenames, String[] negativeFilenames, String outputFolder, String outputName, bool splitDatasets = true) where T: BaseFeature
        {
            if (positiveFilenames == null || negativeFilenames == null) return;
            if (outputFolder == null) return;

            String[] trainSetPos, testSetPos, trainSetNeg, testSetNeg;
            if (splitDatasets)
            {
                Utils.PickPercAtRandom<String>(positiveFilenames, out trainSetPos, out testSetPos);
                Utils.PickPercAtRandom<String>(negativeFilenames, out trainSetNeg, out testSetNeg);
            }
            else
            {
                trainSetPos = positiveFilenames;
                trainSetNeg = negativeFilenames;
                testSetPos = new String[0];
                testSetNeg = new String[0];
            }

            Constants.FeatureType featurePaths = 0;
            Constants.FeatureType featurePathsStandardized = featurePaths | Constants.FeatureType.StandarizedPerFeature;
            List<Constants.FeatureType> fTypes = new List<Constants.FeatureType>();
            fTypes.Add(featurePaths); 
            fTypes.Add(featurePathsStandardized);

            List<SVMCreator> models = new List<SVMCreator>();
            foreach (var ftype in fTypes)
            {
                SVMCreator svmCreator1 = new SVMCreator(trainSetPos, testSetPos, trainSetNeg, testSetNeg, ftype);
                SVMCreator svmCreator2 = new SVMCreator(trainSetPos, testSetPos, trainSetNeg, testSetNeg, ftype);
                svmCreator1.CreateTrainTestSetAndClasses<T>();
                svmCreator2.CreateTrainTestSetAndClasses<T>();
                svmCreator1.MakeSVMAuto(20, true);
                svmCreator2.MakeSVMAuto(0, false);

                float[] percDefIt = svmCreator1.Evaluate<SVM>();
                float[] percUndefIt = svmCreator2.Evaluate<SVM>();

                if (Constants.VERBOSE)
                {
                    Debug.WriteLine(ftype + " Defined Iterations model " + svmCreator1.ToString(true));
                    Debug.WriteLine(ftype + " Undefined Iterations model " + svmCreator2.ToString(true));
                }
                models.Add(svmCreator1);
                models.Add(svmCreator2);
            }
            int counter = -1;
            for (int i = 0; i < models.Count; i++)
            {
                String iter = "";
                if (i > 1) iter = "Standardized_"; 
                if (i % 2 == 0)
                {
                    iter += "DefIter";
                    counter++;
                }
                else
                {
                    iter += "UndefIter";
                }
                if (!splitDatasets) iter += "FullD";
                String path = Path.Combine(outputFolder, outputName + iter + ".xml");
                SvmUtils.SaveSVMToFile(models[i].currentModel, path);
                SvmUtils.SaveSVMInfo(Constants.svmInfoFilePath, models[i].ToString(outputName + iter, typeof(T).Name.ToString()));
            }
        }

        public static void EvaluateClassifier(String[] positiveFilenames, String[] negativeFilenames, Constants.FeatureType fType, String svmPath)
        {
            if (positiveFilenames == null || negativeFilenames == null) return;

            SVMCreator creator = new SVMCreator(positiveFilenames, negativeFilenames, fType);

            creator.CreateTestSetAndClasses();

            FileStorage fs;
            SVM model = SvmUtils.LoadSVMFromFile(svmPath, out fs);

            creator.SetCurrentSVM(model);

            creator.EvaluateSimple(model);
            
            if (Constants.VERBOSE)
                Debug.WriteLine(fType + " classifier" + creator.ToString(false));

            SvmUtils.UnloadModel(fs);
        }

        #endregion
        
        #region Methods to Create datasets
        /// <summary>
        /// 
        /// </summary>
        /// <param name="imageFiles"></param>
        /// <param name="outputFolder"></param>
        public static void WordsAllFeaturesExtractorMainThread(String[] imageFiles, String outputFolder, bool onlyGT)
        {
            if (imageFiles == null) return;
            if (outputFolder == null) return;
            Constants.FeatureType f1 = Constants.FeatureType.ColorDistLab | Constants.FeatureType.ColorDistPii | Constants.FeatureType.StandarizedPerFeature;
            Constants.FeatureType f2 = Constants.FeatureType.NM;

            WordStatExtractorMultiThread[] loops = new WordStatExtractorMultiThread[imageFiles.Length];
            ManualResetEvent[] events = new ManualResetEvent[imageFiles.Length];
            for (int oo = 0; oo < imageFiles.Length; oo++)
            {
                events[oo] = new ManualResetEvent(false);
                WordStatExtractorMultiThread aloop = new WordStatExtractorMultiThread(imageFiles[oo], f1, f2, outputFolder, onlyGT, events[oo]);
                loops[oo] = aloop;

                //aloop.ExtractAndSaveAllDataTypes();
                aloop.ExtractAndSaveAllDataTypesWithoutAsking();
            }
        }

        /// <summary>
        /// gia na ftiaxnw ta 5 datasets (line, wordstat, simplewordstat gia line grouping kai exhaustive grouping) gia ta groundtruth
        /// </summary>
        /// <param name="imageFiles"></param>
        /// <param name="outputFolder"></param>
        public static void WordsExtractorMultiThread(String[] imageFiles, String outputFolder, bool onlyGT)
        {
            if (imageFiles == null) return;
            if (outputFolder == null) return;
            Constants.FeatureType f1 = Constants.FeatureType.ColorDistLab | Constants.FeatureType.ColorDistPii | Constants.FeatureType.StandarizedPerFeature;
            Constants.FeatureType f2 = Constants.FeatureType.NM;

            ThreadPool.SetMaxThreads(1, 2); //(1,4) gia positive-negative, (2,1) gia gt-gtneg
            WordStatExtractorMultiThread[] loops = new WordStatExtractorMultiThread[imageFiles.Length];
            ManualResetEvent[] events = new ManualResetEvent[imageFiles.Length];

            for (int oo = 0; oo < imageFiles.Length; oo++)
            {
                events[oo] = new ManualResetEvent(false);
                WordStatExtractorMultiThread aloop = new WordStatExtractorMultiThread(imageFiles[oo], f1, f2, outputFolder, onlyGT, events[oo]);
                loops[oo] = aloop;
                ThreadPool.QueueUserWorkItem(aloop.ThreadPoolCallback, oo);
            }
        }
        #endregion

        #region retrieve and recalculate datasets

        public static void FixStatsFromLineGrouping(String[] wordStatFiles, String[] simpleWordStatFiles, String[] lineWordStatFiles)
        {
            if (wordStatFiles == null || simpleWordStatFiles == null || lineWordStatFiles == null) return;
            if (wordStatFiles.Length != simpleWordStatFiles.Length || wordStatFiles.Length != lineWordStatFiles.Length || simpleWordStatFiles.Length != lineWordStatFiles.Length) throw new Exception("lathos plhthos anamesa sta diaforetika hdh word stats");
            List<Tuple<WordStat, string>> allWordStats = new List<Tuple<WordStat, string>>();
            List<Tuple<SimpleWordStat, string>> allSimpleWordStats = new List<Tuple<SimpleWordStat, string>>();
            List<Tuple<LineWordStat, string>> allLineWordStats = new List<Tuple<LineWordStat, string>>();

            for (int oo = 0; oo < wordStatFiles.Length; oo++)
            {
                WordStat ws = IOUtils.FromXml<WordStat>(wordStatFiles[oo]);
                allWordStats.Add(Tuple.Create<WordStat, string>(ws, wordStatFiles[oo]));
                SimpleWordStat sws = IOUtils.FromXml<SimpleWordStat>(simpleWordStatFiles[oo]);
                allSimpleWordStats.Add(Tuple.Create<SimpleWordStat, string>(sws, simpleWordStatFiles[oo]));
                LineWordStat lws = IOUtils.FromXml<LineWordStat>(lineWordStatFiles[oo]);
                allLineWordStats.Add(Tuple.Create<LineWordStat, string>(lws, lineWordStatFiles[oo]));
            }
            // spase ta arxeia se mia lista ana eikona, wste stis periptwseis poy uparxoyn perissotera apo ena arxeia apo mia eikona na mhn ginetai to er extraction polles fores
            List<List<Tuple<WordStat, string>>> perImageSplitWS = new List<List<Tuple<WordStat, string>>>();
            List<List<Tuple<SimpleWordStat, string>>> perImageSplitSWS = new List<List<Tuple<SimpleWordStat, string>>>();
            List<List<Tuple<LineWordStat, string>>> perImageSplitLWS = new List<List<Tuple<LineWordStat, string>>>();
            for (int i = 0; i < allWordStats.Count; i++)
            {
                String currentImage = allWordStats[i].Item1.imageName;
                List<Tuple<WordStat, string>> perImageWordStats = new List<Tuple<WordStat, string>>();
                List<Tuple<SimpleWordStat, string>> perImageSimple = new List<Tuple<SimpleWordStat, string>>();
                List<Tuple<LineWordStat, string>> perImageLine = new List<Tuple<LineWordStat, string>>();
                perImageWordStats.Add(allWordStats[i]);
                perImageSimple.Add(allSimpleWordStats[i]);
                perImageLine.Add(allLineWordStats[i]);
                int j;
                for (j = i + 1; j < allWordStats.Count; j++)// psakse sta epomena sthn ekastote lista gia na deis an exoun to idio onoma eikonas (upothetw oti ta idia dedomena poy isxuoun sta wordstats isxioun kai sta simple kai sta line
                {
                    if (currentImage.Equals(allWordStats[j].Item1.imageName))
                    {
                        perImageWordStats.Add(allWordStats[j]);
                        perImageSimple.Add(allSimpleWordStats[j]);
                        perImageLine.Add(allLineWordStats[j]);
                    }
                    else
                    {
                        perImageSplitWS.Add(perImageWordStats);
                        perImageSplitSWS.Add(perImageSimple);
                        perImageSplitLWS.Add(perImageLine);
                        i = j - 1;
                        break;
                    }
                }
                if (j == allWordStats.Count) // gia na douleuei swsta otan adeiasei h lista
                {
                    perImageSplitWS.Add(perImageWordStats);
                    perImageSplitSWS.Add(perImageSimple);
                    perImageSplitLWS.Add(perImageLine);
                    i = j - 1;
                }
            }
            if (perImageSplitWS.Count != perImageSplitSWS.Count || perImageSplitWS.Count != perImageSplitLWS.Count || perImageSplitSWS.Count != perImageSplitLWS.Count)
                throw new Exception("Different number of per image splits");
            for (int i = 0; i < perImageSplitWS.Count; i++)
            {
                Constants.FeatureType f1 = Constants.FeatureType.ColorDistLab | Constants.FeatureType.ColorDistPii | Constants.FeatureType.StandarizedPerFeature;
                Constants.FeatureType f2 = Constants.FeatureType.NM;
                Mat mImage = new Mat(perImageSplitWS[i][0].Item1.imageName, LoadImageType.Unchanged);
                if (mImage.Rows == 0 || mImage.Cols == 0) return;

                VectorOfMat channels = new VectorOfMat();
                Utils.CreateXXChannels(ref channels, mImage, Constants.ColorSpace.Grayscale, Constants.VisChannels.Channel1 | Constants.VisChannels.Channel1Opp, false, Constants.Gradients.None);

                List<ChineseGrouping.Line[]> linesPerChannel;
                ERExtractor.ExecuteDefaultExtractorPipelineLineGrouping(mImage, channels, f1, f2, out linesPerChannel);

                List<Rectangle> wRects;
                List<ChineseGrouping.Line> allLines;
                List<Tuple<List<ERStat>, int>> erListPerWordRect = LineWordStat.CreateErListPerWordRect(linesPerChannel, out wRects, out allLines);

                for (int j = 0; j < perImageSplitWS[i].Count; j++)
                {
                    WordStat wordstat = perImageSplitWS[i][j].Item1;
                    SimpleWordStat simpleWordStat = perImageSplitSWS[i][j].Item1;
                    LineWordStat lineWordStat = perImageSplitLWS[i][j].Item1;

                    int indexCorrect;
                    if (!((wordstat.WordRect.Equals(simpleWordStat.WordRect))|(wordstat.WordRect.Equals(lineWordStat.wordRect))|(simpleWordStat.WordRect.Equals(lineWordStat.wordRect))))
                        throw new Exception("different word rects at the same case");
                    
                    Rectangle wordRect = GroupingUtils.TrimRectsToCorrect(wRects.ToArray(), wordstat.WordRect, out indexCorrect);
                    if (indexCorrect == -1)
                    {
                        Debug.WriteLine("Couldn't reach: " + wordstat.WordRect + " with name: " + wordstat.imageName);
                        continue;
                    }
                    //List<Tuple<List<ERStat>, int>> erListPerWordRect = WordStat.CreateErListPerWordRect(regions, groups2);

                    WordStat wordStatWanted = new WordStat(wordRect, erListPerWordRect[indexCorrect].Item1, mImage.Size);
                    WordERFeatures[] wordERFeatures = new WordERFeatures[erListPerWordRect[indexCorrect].Item1.Count];
                    SimpleWordStat simpleWordStatWanted = new SimpleWordStat(wordRect, erListPerWordRect[indexCorrect].Item1, mImage.Size);
                    SimpleWordErFeatures[] simpleWordERFeatures = new SimpleWordErFeatures[erListPerWordRect[indexCorrect].Item1.Count];
                    LineWordStat lineWordStatWanted = new LineWordStat(wordRect, erListPerWordRect[indexCorrect].Item1, mImage.Size);
                    LineWordFeatures[] lineWordFeatures = new LineWordFeatures[erListPerWordRect[indexCorrect].Item1.Count];

                    for (int k = 0; k < erListPerWordRect[indexCorrect].Item1.Count; k++)
                    {
                        wordERFeatures[k] = new WordERFeatures(erListPerWordRect[indexCorrect].Item1[k], channels[erListPerWordRect[indexCorrect].Item2], mImage);
                        simpleWordERFeatures[k] = new SimpleWordErFeatures(erListPerWordRect[indexCorrect].Item1[k], channels[erListPerWordRect[indexCorrect].Item2], mImage);
                        lineWordFeatures[k] = new LineWordFeatures(erListPerWordRect[indexCorrect].Item1[k], channels[erListPerWordRect[indexCorrect].Item2], mImage);
                    }
                    wordStatWanted.ComputeWordFeatures(wordERFeatures.ToList());
                    wordStatWanted.imageName = wordstat.imageName;
                    wordStatWanted.channel = erListPerWordRect[indexCorrect].Item2 == 0 ? "BgrGrayscaleF" : "BgrGrayscaleB";

                    simpleWordStatWanted.Compute(simpleWordERFeatures.ToList());
                    simpleWordStatWanted.imageName = simpleWordStat.imageName;
                    simpleWordStatWanted.channel = erListPerWordRect[indexCorrect].Item2 == 0 ? "BgrGrayscaleF" : "BgrGrayscaleB";

                    lineWordStatWanted.ComputeFeatures(allLines[indexCorrect], lineWordFeatures.ToList());
                    lineWordStatWanted.imageName = lineWordStat.imageName;
                    lineWordStatWanted.channel = erListPerWordRect[indexCorrect].Item2 == 0 ? "BgrGrayscaleF" : "BgrGrayscaleB";

                    wordStatWanted.ToXML<WordStat>(perImageSplitWS[i][j].Item2);
                    simpleWordStatWanted.ToXML<SimpleWordStat>(perImageSplitSWS[i][j].Item2);
                    lineWordStatWanted.ToXML<LineWordStat>(perImageSplitLWS[i][j].Item2);
                }
                Disposer.DisposeMatVectorOfMatRegions(ref mImage, ref channels);
                GC.Collect();
            }
        }

        public static void FixStatsFromLineGroupingMultiThread(String[] wordStatFiles, String[] simpleWordStatFiles, String[] lineWordStatFiles)
        {
            if (wordStatFiles == null || simpleWordStatFiles == null || lineWordStatFiles == null) return;
            if (wordStatFiles.Length != simpleWordStatFiles.Length || wordStatFiles.Length != lineWordStatFiles.Length || simpleWordStatFiles.Length != lineWordStatFiles.Length) throw new Exception("lathos plhthos anamesa sta diaforetika hdh word stats");
            List<Tuple<WordStat, string>> allWordStats = new List<Tuple<WordStat, string>>();
            List<Tuple<SimpleWordStat, string>> allSimpleWordStats = new List<Tuple<SimpleWordStat, string>>();
            List<Tuple<LineWordStat, string>> allLineWordStats = new List<Tuple<LineWordStat, string>>();

            for (int oo = 0; oo < wordStatFiles.Length; oo++)
            {
                WordStat ws = IOUtils.FromXml<WordStat>(wordStatFiles[oo]);
                allWordStats.Add(Tuple.Create<WordStat, string>(ws, wordStatFiles[oo]));
                SimpleWordStat sws = IOUtils.FromXml<SimpleWordStat>(simpleWordStatFiles[oo]);
                allSimpleWordStats.Add(Tuple.Create<SimpleWordStat, string>(sws, simpleWordStatFiles[oo]));
                LineWordStat lws = IOUtils.FromXml<LineWordStat>(lineWordStatFiles[oo]);
                allLineWordStats.Add(Tuple.Create<LineWordStat, string>(lws, lineWordStatFiles[oo]));
            }
            // spase ta arxeia se mia lista ana eikona, wste stis periptwseis poy uparxoyn perissotera apo ena arxeia apo mia eikona na mhn ginetai to er extraction polles fores
            List<List<Tuple<WordStat, string>>> perImageSplitWS = new List<List<Tuple<WordStat, string>>>();
            List<List<Tuple<SimpleWordStat, string>>> perImageSplitSWS = new List<List<Tuple<SimpleWordStat, string>>>();
            List<List<Tuple<LineWordStat, string>>> perImageSplitLWS = new List<List<Tuple<LineWordStat, string>>>();
            for (int i = 0; i < allWordStats.Count; i++)
            {
                String currentImage = allWordStats[i].Item1.imageName;
                List<Tuple<WordStat, string>> perImageWordStats = new List<Tuple<WordStat, string>>();
                List<Tuple<SimpleWordStat, string>> perImageSimple = new List<Tuple<SimpleWordStat, string>>();
                List<Tuple<LineWordStat, string>> perImageLine = new List<Tuple<LineWordStat, string>>();
                perImageWordStats.Add(allWordStats[i]);
                perImageSimple.Add(allSimpleWordStats[i]);
                perImageLine.Add(allLineWordStats[i]);
                int j;
                for (j = i + 1; j < allWordStats.Count; j++)// psakse sta epomena sthn ekastote lista gia na deis an exoun to idio onoma eikonas (upothetw oti ta idia dedomena poy isxuoun sta wordstats isxioun kai sta simple kai sta line
                {
                    if (currentImage.Equals(allWordStats[j].Item1.imageName))
                    {
                        perImageWordStats.Add(allWordStats[j]);
                        perImageSimple.Add(allSimpleWordStats[j]);
                        perImageLine.Add(allLineWordStats[j]);
                    }
                    else
                    {
                        perImageSplitWS.Add(perImageWordStats);
                        perImageSplitSWS.Add(perImageSimple);
                        perImageSplitLWS.Add(perImageLine);
                        i = j - 1;
                        break;
                    }
                }
                if (j == allWordStats.Count) // gia na douleuei swsta otan adeiasei h lista
                {
                    perImageSplitWS.Add(perImageWordStats);
                    perImageSplitSWS.Add(perImageSimple);
                    perImageSplitLWS.Add(perImageLine);
                    i = j - 1;
                }
            }
            if (perImageSplitWS.Count != perImageSplitSWS.Count || perImageSplitWS.Count != perImageSplitLWS.Count || perImageSplitSWS.Count != perImageSplitLWS.Count)
                throw new Exception("Different number of per image splits");

            Constants.FeatureType f1 = Constants.FeatureType.ColorDistLab | Constants.FeatureType.ColorDistPii | Constants.FeatureType.StandarizedPerFeature;
            Constants.FeatureType f2 = Constants.FeatureType.NM;
            // multithreaded below
            ThreadPool.SetMaxThreads(3, 3);
            WordStatFixerMultiThread[] loops = new WordStatFixerMultiThread[perImageSplitWS.Count];
            ManualResetEvent[] events = new ManualResetEvent[perImageSplitWS.Count];
            for (int oo = 0; oo < perImageSplitWS.Count; oo++)
            {
                events[oo] = new ManualResetEvent(false);
                WordStatFixerMultiThread aloop = new WordStatFixerMultiThread(f1, f2, events[oo], perImageSplitWS[oo], perImageSplitSWS[oo], perImageSplitLWS[oo]);
                loops[oo] = aloop;
                ThreadPool.QueueUserWorkItem(aloop.ThreadPoolCallback, oo);
            }
        }

        public static void CheckFFO(String[] ffoFiles)
        {
            if (ffoFiles == null) return;
            List<Tuple<FullFeatureObject, string>> broken = new List<Tuple<FullFeatureObject, string>>();
            for (int oo = 0; oo < ffoFiles.Length; oo++)
            {
                FullFeatureObject ffo = IOUtils.FromXml<FullFeatureObject>(ffoFiles[oo]);
                List<float> featureList = ffo.ToFloatList();
                if (featureList[featureList.Count - 1] > 1)
                {
                    broken.Add(Tuple.Create<FullFeatureObject, string>(ffo, ffoFiles[oo]));
                }
            }
        }

        /// <summary>
        /// infinity checks (kai NaN) kai epanypologismoi xarakthristikwn
        /// </summary>
        /// <param name="wordStatFiles"></param>
        public static void CheckWordStats(String[] wordStatFiles)
        {
            if (wordStatFiles == null) return;
            List<Tuple<WordStat, string>> broken = new List<Tuple<WordStat, string>>();
            for (int oo = 0; oo < wordStatFiles.Length; oo++)
            {
                WordStat wordstat = IOUtils.FromXml<WordStat>(wordStatFiles[oo]);
                List<float> featureList = wordstat.ToFloatList();
                foreach (float f in featureList)
                {
                    if (float.IsInfinity(f))
                    {           
                        broken.Add(Tuple.Create<WordStat, string>(wordstat, wordStatFiles[oo]));
                        Debug.WriteLine("Fixed: " + wordStatFiles[oo]);
                        break;
                    }
                }
            }
            // per image split
            List<List<Tuple<WordStat, string>>> perImageSplit = new List<List<Tuple<WordStat,string>>>();

            for (int i = 0; i < broken.Count; i++)
            {
                String currentImage = broken[i].Item1.imageName;
                List<Tuple<WordStat, string>> perImageWordStats = new List<Tuple<WordStat,string>>();
                perImageWordStats.Add(broken[i]);
                int j;
                for (j = i + 1; j < broken.Count; j++)
                {
                    if (currentImage.Equals(broken[j].Item1.imageName))
                    {
                        perImageWordStats.Add(broken[j]);
                    }
                    else
                    {
                        perImageSplit.Add(perImageWordStats);
                        i = j - 1;
                        break;
                    }
                }
                if (j == broken.Count)
                {
                    perImageSplit.Add(perImageWordStats);
                    i = j - 1;
                }
            }
            for (int i = 0; i < perImageSplit.Count; i++)
            {            
                Constants.FeatureType f1 = Constants.FeatureType.ColorDistLab | Constants.FeatureType.ColorDistPii | Constants.FeatureType.StandarizedPerFeature;
                Constants.FeatureType f2 = Constants.FeatureType.NM;
                Mat mImage = new Mat(perImageSplit[i][0].Item1.imageName, LoadImageType.Unchanged);
                if (mImage.Rows == 0 || mImage.Cols == 0) return;

                VectorOfMat channels = new VectorOfMat();
                Utils.CreateXXChannels(ref channels, mImage, Constants.ColorSpace.Grayscale, Constants.VisChannels.Channel1 | Constants.VisChannels.Channel1Opp, false, Constants.Gradients.None);

                List<ERStat>[] regions; List<List<Tuple<int, int>>> groups; Rectangle[] wordRects;
                ERExtractor.ExecuteDefaultExtractorPipelineNMGrouping(mImage, channels, f1, f2, out regions, out groups, out wordRects);

                for (int j = 0; j < perImageSplit[i].Count; j++)
                {
                    WordStat wordstat = perImageSplit[i][j].Item1;

                    int indexCorrect;
                    Rectangle wordRect = GroupingUtils.TrimRectsToCorrect(wordRects, wordstat.WordRect, out indexCorrect);

                    List<List<Tuple<int, int>>> groups2 = new List<List<Tuple<int, int>>>();
                    groups2.Add(groups[indexCorrect]);

                    List<Tuple<List<ERStat>, int>> erListPerWordRect = WordStat.CreateErListPerWordRect(regions, groups2);

                    WordStat wordStatWanted = new WordStat(wordRect, erListPerWordRect[0].Item1, mImage.Size);
                    WordERFeatures[] wordERFeatures = new WordERFeatures[erListPerWordRect[0].Item1.Count];
                    for (int k = 0; k < erListPerWordRect[0].Item1.Count; k++)
                    {
                        wordERFeatures[k] = new WordERFeatures(erListPerWordRect[0].Item1[k], channels[erListPerWordRect[0].Item2], mImage);
                    }
                    wordStatWanted.ComputeWordFeatures(wordERFeatures.ToList());
                    wordStatWanted.imageName = wordstat.imageName;
                    wordStatWanted.channel = erListPerWordRect[0].Item2 == 0 ? "BgrGrayscaleF" : "BgrGrayscaleB";

                    wordStatWanted.ToXML<WordStat>(perImageSplit[i][j].Item2);
                }
                
                Disposer.DisposeMatVectorOfMatRegions(ref mImage, ref channels, ref regions);
                GC.Collect();
            }
        }

        public void erRetrievePositive()
        {
            String[] erFileNames = FormUtils.ParametrizedOpenFileDialog(Constants.XML_FILE_FILTER, true, "Select the er files");
            if (erFileNames == null) return;
            // no need to organise anything. each ffo xml file contains the image it originates from, so the ractangle coordinates included are correlated with this (cropped) image
            String imageName = "";
            String channel = "";
            Mat mBgr = new Mat(); Mat mLab = new Mat();
            Mat m = new Mat();
            ERExtractor ere;
            List<ERStat> regions = new List<ERStat>();
            for (int i = 0; i < erFileNames.Length; i++)
            {
                FullFeatureObject ffo = IOUtils.FromXml<FullFeatureObject>(erFileNames[i]);

                // retrieve all the regions from input image
                if ((imageName != ffo.imageName || channel != ffo.channel))
                {
                    imageName = ffo.imageName;
                    channel = ffo.channel;
                    if (File.Exists(imageName))
                    {
                        m = new Mat(imageName, LoadImageType.Grayscale);
                        mBgr = new Mat(imageName, LoadImageType.Unchanged);
                        if (channel.EndsWith("B"))
                        {
                            CvInvoke.BitwiseNot(m, m);
                        }
                        ColorConversionWrapper.ColorConversions(mBgr, ColorConversion.Bgr2Lab, ref mLab);
                        regions.Clear();
                        ere = new ERExtractor(new ERExtractor.ExtractorParameters(1, 0.00005, 0.96, 0, 0, m.Width, m.Height), ERUtils.SizeRestrictionsArea);
                        regions = ere.extractERs(m, true, false);
                        regions = RecursiveFunctions.NonCerSuppression(regions);
                        //regions = RecursiveFunctions.NearDuplicateSuppression(regions, true);
                    }
                    else // copy the images in the correct path
                    { }
                }
                ERStat stat = ERUtils.SearchRegions(regions, ffo);
                if (stat != null) // found the er so we can do all kinds of computations with it now
                {
                    using (Mat erMask = Utils.ErMaskFloodFill(m, stat, false))
                    {
                        float StrokeWidth = StrokeUtils.MeanStrokeWidth(erMask);
                        if (float.IsNaN(StrokeWidth)) StrokeWidth = 0;
                        ffo.StrokeWidthERWidthRatio = StrokeWidth / stat.rect.Width;

                        VectorOfVectorOfPoint contours = Utils.CalcContours(erMask);
                        if (contours.Size > 0)
                        {
                            float ConvexityDefects = Utils.CalcConvexityDefects(contours, Utils.CalcConvexHullPoints(contours));
                            if (float.IsNaN(ConvexityDefects)) ConvexityDefects = 0;
                            ffo.ConvexityDefects = ConvexityDefects;
                        }
                    }



                    //float consistency = ERUtils.ColorConsistencyER(m, mLab, stat, ERUtils.LabColorDistance);
                    //ffo.colorConsistencyLab = consistency;
                    //Mat skeleton = new Mat();
                    //ZhangSuenThinning tt = new ZhangSuenThinning();
                    //tt.Skeletonize(erPixels, ref skeleton);

                    //Mat neighbourMat = new Mat(skeleton.Rows, skeleton.Cols, DepthType.Cv8U, 1);
                    //StrokeUtils.CreateNeighbourMat(skeleton, ref neighbourMat);

                    //double minVal = -1, maxVal = 0;
                    //Point minLoc = new Point(), maxLoc = new Point();
                    //CvInvoke.MinMaxLoc(neighbourMat, ref minVal, ref maxVal, ref minLoc, ref maxLoc, skeleton);

                    ////new ImageViewer(erPixels, "er").Show();
                    ////new ImageViewer(skeleton, "skel").Show();

                    //List<int>[] neighboursArr = StrokeUtils.GetNeighbourIndices(skeleton);
                    //StrokeUtils.SortNeighboursToDistance(ref neighboursArr, skeleton.Cols);

                    //StrokeUtils.PathStats pathStats = StrokeUtils.FindPaths(skeleton, neighboursArr, minLoc);

                    //int skelPixels = pathStats.GetTotalPixelsCount();        
                    //int contourPixels = Utils.GetContourPointsSum(erPixels); // prosoxh, to erpixels allazei meta apo dw!!!!!             
                    //int areaPixels = stat.area;
                    //int bbPixels = stat.rect.Width * stat.rect.Height;

                    ////new ImageViewer(res, " result").Show();

                    ////ffo.significantPaths = pathStats.PathLengths.Count(n => n > 3);
                    //ffo.significantPaths = pathStats.NumberOfPaths;
                    //ffo.skelContourRatio = skelPixels / (float)contourPixels;
                    //ffo.skelAreaRatio = skelPixels / (float)areaPixels;
                    //ffo.areaBbRatio = areaPixels / (float)bbPixels;

                    ffo.ToXML(erFileNames[i]);

                    //piiImage.Dispose();
                }
                else // not found :(
                {
                    Debug.WriteLine("");
                    Debug.WriteLine("*****");
                    Debug.WriteLine("Not found: " + erFileNames[i]);
                    Debug.WriteLine("*****");
                    Debug.WriteLine("");
                    continue;
                }

            }
            m.Dispose();
            regions.Clear();
            GC.Collect();
        }

        /// <summary>
        /// This method is used to retrieve an <c> ER </c> saved in xml format from the image it originates from in order to calculate extra characteristics, or whoknowswhatelse.
        /// </summary>
        public void erRetrieveDemo()
        {
            String[] imageFileNames = FormUtils.ParametrizedOpenFileDialog(Constants.IMAGE_FILE_FILTER, true, "Select the images");
            String[] erFilesNames = FormUtils.ParametrizedOpenFileDialog(Constants.XML_FILE_FILTER, true, "Select the er files");
            if (imageFileNames == null || erFilesNames == null) return;

            // organise the files a bit
            List<String> imageNames = imageFileNames.ToList();
            List<String> erFileNames = erFilesNames.ToList();
            List<List<String>> erPaths = new List<List<string>>();

            for (int i = 0; i < imageNames.Count; i++)
            {
                String name = Path.GetFileNameWithoutExtension(imageNames[i]);
                erPaths.Add(new List<string>());

                String erName = Path.GetFileNameWithoutExtension(erFileNames[0]);
                while (erName.StartsWith(name))
                {
                    erPaths[i].Add(erFileNames[0]);
                    erFileNames.RemoveAt(0);
                    if (erFileNames.Count > 0) erName = Path.GetFileNameWithoutExtension(erFileNames[0]);
                    else erName = "";
                }
                if (erPaths[i].Count == 0)
                {
                    erPaths.RemoveAt(i);
                    imageNames.RemoveAt(i);
                    i--;
                }
            }
            // now we have a list with the image names and a corresponding list containing a list with all the cropped examples per image
            for (int i = 0; i < imageNames.Count; i++)
            {
                Mat m = new Mat(imageNames[i], LoadImageType.Grayscale);
                List<FullFeatureObject> ffos = new List<FullFeatureObject>();
                foreach (String name in erPaths[i])
                {
                    ffos.Add(IOUtils.FromXml<FullFeatureObject>(name));
                }
                ERExtractor ere = new ERExtractor(new ERExtractor.ExtractorParameters(1, 0.00005, 0.96, 0, 0, m.Cols, m.Rows), ERUtils.SizeRestrictionsArea);
                List<ERStat> regions = ere.extractERs(m, true, false);
                regions = RecursiveFunctions.NonCerSuppression(regions);
                regions = RecursiveFunctions.NearDuplicateSuppression(regions, true);

                foreach (FullFeatureObject ffo in ffos)
                {
                    ERStat found = ERUtils.SearchRegions(regions, ffo);
                    if (found == null)
                    {

                    }
                    else
                    {

                    }
                }

            }


        }

        #endregion

        #region Methods to view the Word Rects of whatever type - pruned or not
        public static List<Rectangle[]> CalculateWordStats(Mat mImage)
        {
            Constants.FeatureType f1 = Constants.FeatureType.ColorDistLab | Constants.FeatureType.ColorDistPii | Constants.FeatureType.StandarizedPerFeature;
            Constants.FeatureType f2 = Constants.FeatureType.NM;

            VectorOfMat channels = new VectorOfMat();
            Utils.CreateXXChannels(ref channels, mImage, Constants.ColorSpace.Grayscale, Constants.VisChannels.Channel1 | Constants.VisChannels.Channel1Opp, false, Constants.Gradients.None);

            List<ERStat>[] regions;
            List<List<Tuple<int, int>>> groups;
            Rectangle[] wordRects;
            ERExtractor.ExecuteDefaultExtractorPipelineNMGrouping(mImage, channels, f1, f2, out regions, out groups, out wordRects);

            // visualize the word rectangles from each channel            
            VectorOfMat wordMats = new VectorOfMat();
            Utils.RoiMatsFromWordRects(wordRects, mImage, ref wordMats);
            if (wordMats.Size > 0)
            {
                WordRectangleForm wrf = new WordRectangleForm(wordMats, "wordMats");
                wrf.Show();
            }

            List<Rectangle>[] rectsPerChannel = new List<Rectangle>[channels.Size];
            for (int i = 0; i < channels.Size; i++)
            {
                rectsPerChannel[i] = new List<Rectangle>();
            }

            List<Tuple<List<ERStat>, int>> erListPerWordRect = new List<Tuple<List<ERStat>, int>>();

            foreach (List<Tuple<int, int>> wordERs in groups)
            {
                List<ERStat> tmp = new List<ERStat>();
                for (int i = 0; i < wordERs.Count; i++)
                {
                    tmp.Add(regions[wordERs[i].Item1][wordERs[i].Item2]);
                    rectsPerChannel[wordERs[i].Item1].Add(regions[wordERs[i].Item1][wordERs[i].Item2].rect); //put here the rects per channel for output
                }
                erListPerWordRect.Add(Tuple.Create<List<ERStat>, int>(new List<ERStat>(tmp), wordERs[0].Item1));
            }

            WordStat[] wordStats = WordStat.CreateMultiple(wordRects, erListPerWordRect, mImage, channels, "");
            for (int i = 0; i < wordStats.Length; i++)
            {
                List<string> texts = new List<string>();
                foreach (WordERFeatures f in wordStats[i].features)
                    texts.Add(f.ToString());
                FormUtils.VisualizeErPixels(channels[wordStats[i].channel.EndsWith("F") ? 0 : 1], wordStats[i].ERs, texts);
            }

            Disposer.DisposeMatVectorOfMatRegions(ref mImage, ref channels, ref regions);

            return Utils.TransformArrayOfListToListArray(rectsPerChannel);
        }
        public static List<Rectangle[]> CalculateSimpleWordStats(Mat mImage)
        {
            Constants.FeatureType f1 = Constants.FeatureType.ColorDistLab | Constants.FeatureType.ColorDistPii | Constants.FeatureType.StandarizedPerFeature;
            Constants.FeatureType f2 = Constants.FeatureType.NM;

            VectorOfMat channels = new VectorOfMat();
            Utils.CreateXXChannels(ref channels, mImage, Constants.ColorSpace.Grayscale, Constants.VisChannels.Channel1 | Constants.VisChannels.Channel1Opp, false, Constants.Gradients.None);

            List<ERStat>[] regions;
            List<List<Tuple<int, int>>> groups;
            Rectangle[] wordRects;
            ERExtractor.ExecuteDefaultExtractorPipelineNMGrouping(mImage, channels, f1, f2, out regions, out groups, out wordRects);

            // visualize the word rectangles from each channel            
            VectorOfMat wordMats = new VectorOfMat();
            Utils.RoiMatsFromWordRects(wordRects, mImage, ref wordMats);
            if (wordMats.Size > 0)
            {
                WordRectangleForm wrf = new WordRectangleForm(wordMats, "wordMats");
                wrf.Show();
            }

            List<Rectangle>[] rectsPerChannel = new List<Rectangle>[channels.Size];
            for (int i = 0; i < channels.Size; i++)
            {
                rectsPerChannel[i] = new List<Rectangle>();
            }

            List<Tuple<List<ERStat>, int>> erListPerWordRect = new List<Tuple<List<ERStat>, int>>();

            foreach (List<Tuple<int, int>> wordERs in groups)
            {
                List<ERStat> tmp = new List<ERStat>();
                for (int i = 0; i < wordERs.Count; i++)
                {
                    tmp.Add(regions[wordERs[i].Item1][wordERs[i].Item2]);
                    rectsPerChannel[wordERs[i].Item1].Add(regions[wordERs[i].Item1][wordERs[i].Item2].rect); //put here the rects per channel for output
                }
                erListPerWordRect.Add(Tuple.Create<List<ERStat>, int>(new List<ERStat>(tmp), wordERs[0].Item1));
            }

            SimpleWordStat[] wordStats = SimpleWordStat.CreateMultiple(wordRects, erListPerWordRect, mImage, channels, "");
            for (int i = 0; i < wordStats.Length; i++)
            {
                List<string> texts = new List<string>();
                foreach (SimpleWordErFeatures f in wordStats[i].features)
                    texts.Add(f.ToString());
                FormUtils.VisualizeErPixels(channels[wordStats[i].channel.EndsWith("F") ? 0 : 1], wordStats[i].ERs, texts);
            }

            Disposer.DisposeMatVectorOfMatRegions(ref mImage, ref channels, ref regions);

            return Utils.TransformArrayOfListToListArray(rectsPerChannel);
        }
        public static Rectangle[] ExecuteOrder66<T, R>(Mat mImage, String karatzaPath, Constants.FeatureType f3)
            where T : SVMExample, new()
            where R : BaseFeature
        {
            if (karatzaPath == null) return new Rectangle[0];

            Constants.FeatureType f1 = Constants.FeatureType.ColorDistLab | Constants.FeatureType.ColorDistPii | Constants.FeatureType.StandarizedPerFeature;
            Constants.FeatureType f2 = Constants.FeatureType.NM;

            VectorOfMat channels = new VectorOfMat();
            Utils.CreateXXChannels(ref channels, mImage, Constants.ColorSpace.Grayscale, Constants.VisChannels.Channel1 | Constants.VisChannels.Channel1Opp, false, Constants.Gradients.None);

            //List<ERStat>[] regions;
            //List<List<Tuple<int, int>>> groups;
            Rectangle[] wordRects;
            ERExtractor.ExecuteDefaultExtractorLineGroupingAndWordTrimming<T, R>(mImage, channels, f1, f2, f3, karatzaPath, out wordRects);

            return wordRects;
        }

        public static List<Rectangle[]> ViewLineERs(Mat mImage, VectorOfMat channels, Constants.FeatureType f1, Constants.FeatureType f2, ERFilterProps props, Constants.GroupingMethod flagGrouping)
        {
            List<Rectangle[]> erRectsPerChannel = new List<Rectangle[]>();

            if (flagGrouping.Equals(Constants.GroupingMethod.chinese))
            {
                List<ChineseGrouping.Line[]> linesPerChannel;
                ERExtractor.ExecuteDefaultExtractorPipelineLineGrouping(mImage, channels, f1, f2, out linesPerChannel, props);

                for (int i = 0; i < linesPerChannel.Count; i++)
                {
                    List<Rectangle> erRects = new List<Rectangle>();
                    for (int j = 0; j < linesPerChannel[i].Length; j++)
                    {
                        for (int k = 0; k < linesPerChannel[i][j].ersOfLine.Count; k++)
                            erRects.Add(linesPerChannel[i][j].ersOfLine[k].rect);
                    }
                    erRectsPerChannel.Add(erRects.ToArray());
                    
                }
            }
            else if (flagGrouping.Equals(Constants.GroupingMethod.ExhaustiveSearch))
            {
                List<ERStat>[] regions;
                List<List<Tuple<int, int>>> groups;
                Rectangle[] wordRects;
                ERExtractor.ExecuteDefaultExtractorPipelineNMGrouping(mImage, channels, f1, f2, out regions, out groups, out wordRects);

                List<Tuple<List<ERStat>, int>> erListPerWordRect = WordStat.CreateErListPerWordRect(regions, groups);
                List<List<Rectangle>> tempListOfLists = new List<List<Rectangle>>();
                for (int i = 0; i < channels.Size; i++)
                    tempListOfLists.Add(new List<Rectangle>());

                for (int i = 0; i < erListPerWordRect.Count; i++)
                {
                    for (int j = 0; j < erListPerWordRect[i].Item1.Count; j++)
                        tempListOfLists[erListPerWordRect[i].Item2].Add(erListPerWordRect[i].Item1[j].rect);
                }
                for (int i = 0; i < tempListOfLists.Count; i++)
                    erRectsPerChannel.Add(tempListOfLists[i].ToArray());
            }
            return erRectsPerChannel;
        }
        #endregion


        
    }
}
