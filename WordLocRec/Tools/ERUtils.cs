﻿using Emgu.CV;
using Emgu.CV.CvEnum;
using Emgu.CV.ML;
using Emgu.CV.Structure;
using Emgu.CV.Util;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using WordLocRec.DataStructs;
using WordLocRec.Tools;
using WordLocRec.SVMStuff;
using WordLocRec.Grouping;

namespace WordLocRec.Tools
{
    public static class ERUtils
    {
        #region AddPixel Utils
        public static void erAddPixel(ref ERStat parent, int x, int y, IncrementalyComputableDescriptors icd, int currentPixel, bool savePixels = false)
        {
            ERUtils.AddAreaPerimeterEuler(ref parent, icd);
            //currentBinaryMask[currentPixel] = curValue;
            if (savePixels) parent.pixels.Add(currentPixel);

            ERUtils.PixelCrossings(ref parent, y, icd);

            ERUtils.ReshapeBoundingBox(ref parent, x, y, x, y);

            ERUtils.ConsumeMoments(ref parent, x, y, x * x, x * y, y * y);
        }
        public static void PixelCrossings(ref ERStat parent, int y, IncrementalyComputableDescriptors icd)
        {
            List<int> parentCrossings = parent.crossings;
            if (parentCrossings.Count > 0)
            {
                int TopHeightDifference = y - parent.rect.Y;
                if (TopHeightDifference < 0) //to neo pixel vrisketai panw apo to rectangle
                    parentCrossings.Insert(0, 2);
                else if (y > parent.rect.Bottom - 1) //to neo pixel vrisketai katw apo to rectangle
                    parentCrossings.Add(2);
                else //to neo pixel vrisketai entos toy ypsoys toy rectangle
                    parentCrossings[TopHeightDifference] += 2 - 2 * icd.NonBoundaryNeighboursHor;
            }
            else
                parentCrossings.Add(2);

            parent.crossings = parentCrossings;
        }
        public static void AddAreaPerimeterEuler(ref ERStat parent, IncrementalyComputableDescriptors icd)
        {
            parent.area++;
            parent.perimeter += 4 - 2 * icd.NonBoundaryNeighbours;
            parent.euler += (icd.dC1 - icd.dC2 + 2 * icd.dC3) / 4;
        }
        #endregion

        #region Merge Utils
        /// <summary>
        /// Connect an accepted child with its parent
        /// </summary>
        /// <param name="parent"></param>
        /// <param name="child"></param>
        public static void AcceptChild(ref ERStat parent, ref ERStat child)
        {
            child.next = parent.child;
            if (parent.child != null)
            {
                parent.child.prev = child;
            }
            parent.child = child;
            child.parent = parent;
        }
        /// <summary>
        /// Dismiss a child and connect its children (if any) to its parent
        /// </summary>
        /// <param name="parent"></param>
        /// <param name="child"></param>
        public static void RejectChild(ref ERStat parent, ref ERStat child)
        {
            if (child.prev != null)
            {
                child.prev.next = child.next;
            }

            ERStat newChild = child.child;

            if (newChild != null)
            {
                while (newChild.next != null)
                    newChild = newChild.next;
                newChild.next = parent.child;
                if (parent.child != null)
                    parent.child.prev = newChild;
                parent.child = child.child;
                child.child.parent = parent;
            }
            child = null;
        }
        public static void AddAreaPerimeterEuler(ref ERStat parent, ref ERStat child)
        {
            parent.area += child.area;
            parent.perimeter += child.perimeter;
            parent.euler += child.euler;
        }
        public static void MergeCrossings(ref ERStat parent, ref ERStat child)
        {
            for (int i = parent.rect.Y; i <= Math.Min(parent.rect.Bottom - 1, child.rect.Bottom - 1); i++)
                if (i - child.rect.Y >= 0)
                    parent.crossings[i - parent.rect.Y] += child.crossings[i - child.rect.Y];

            for (int i = parent.rect.Y - 1; i >= child.rect.Y; i--)
                if (i - child.rect.Y < child.crossings.Count)
                    parent.crossings.Insert(0, child.crossings[i - child.rect.Y]);
                else
                    parent.crossings.Insert(0, 0);

            for (int i = parent.rect.Bottom; i < child.rect.Y; i++)
                parent.crossings.Add(0);

            for (int i = Math.Max(parent.rect.Bottom, child.rect.Y); i <= child.rect.Bottom - 1; i++)
                parent.crossings.Add(child.crossings[i - child.rect.Y]);
        }
        public static void CalcMedCrossings(ref ERStat child)
        {
            List<int> mCrossings = new List<int>();
            mCrossings.Add(child.crossings[(int)(child.rect.Height) / 6]);
            mCrossings.Add(child.crossings[(int)3 * (child.rect.Height) / 6]);
            mCrossings.Add(child.crossings[(int)5 * (child.rect.Height) / 6]);
            mCrossings.Sort();

            child.medCrossings = (float)mCrossings[1];

            child.crossings.Clear();
            child.crossings = null;
        }
        public static bool SizeRestrictionsAcceptAll(ERStat child, ERExtractor.ExtractorParameters ep)
        {
            return true;
        }
        public static bool SizeRestrictionAccept109(ERStat child, ERExtractor.ExtractorParameters ep)
        {
            return child.level <= 109;
        }
        /// <summary>
        /// if true the child fits the size restrictions
        /// </summary>
        /// <param name="child"></param>
        /// <param name="ep"></param>
        /// <returns></returns>
        public static bool SizeRestrictionsArea(ERStat child, ERExtractor.ExtractorParameters ep)
        {
            return ((child.rect.Width > 2 && child.rect.Height > 2) &&
                    (child.area > ep.MinArea * ep.ImageHeight * ep.ImageWidth) &&
                    (child.area < ep.MaxArea * ep.ImageHeight * ep.ImageWidth));
        }
        /// <summary>
        /// if true the child fits the size restrictions
        /// </summary>
        /// <param name="child"></param>
        /// <param name="ep"></param>
        /// <returns></returns>
        public static bool SizeRestrictionsBoundingBox(ERStat child, ERExtractor.ExtractorParameters ep)
        {
            int rectAr = child.rect.Width * child.rect.Height;
            return ((child.rect.Width > 4 && child.rect.Height > 4) && 
                    (rectAr > ep.MinArea * ep.ImageHeight * ep.ImageWidth) &&
                    (rectAr < ep.MaxArea * ep.MaxArea * ep.ImageHeight * ep.ImageWidth));
        }

        #endregion

        #region GlobalUtils

        public static void ConsumeMoments(ref ERStat parent, int raw0, int raw1, int cen0, int cen1, int cen2)
        {
            parent.rawMoments[0] += raw0;
            parent.rawMoments[1] += raw1;

            parent.centralMoments[0] += cen0;
            parent.centralMoments[1] += cen1;
            parent.centralMoments[2] += cen2;
        }
        public static void ReshapeBoundingBox(ref ERStat parent, int x1, int y1, int x2, int y2)
        {
            int newX1 = Math.Min(parent.rect.X, x1);
            int newY1 = Math.Min(parent.rect.Y, y1);
            int newX2 = Math.Max(parent.rect.Right - 1, x2);
            int newY2 = Math.Max(parent.rect.Bottom - 1, y2);

            parent.rect.X = newX1;
            parent.rect.Y = newY1;
            parent.rect.Width = newX2 - newX1 + 1;
            parent.rect.Height = newY2 - newY1 + 1;
        }
        #endregion

        #region Cer Utils

        public static ERStat FindAGoodParent(ref ERStat thisER)
        {
            int lvl = thisER.level;
            ERStat tmpParent = thisER.parent;
            while (tmpParent != null && tmpParent.level < thisER.level + 3)
            {
                if (tmpParent.parent != null) tmpParent = tmpParent.parent; // an den exei parent me diafora megaluterh apo 3 tote xrisimopoiei auto poy vriskei
                else break;
            }

            return tmpParent;
        }
        public static void CombineParentChildPixels(ref ERStat parent, ref ERStat child)
        {
            IncludeNewChildPixels(ref child);
            if (parent != null) mergeParentChildPixelLists(ref parent, ref child);
            child.setSizeType();
        }
        /// <summary>
        /// include the new pixel list from the current pixel level to the child's history
        /// </summary>
        /// <param name="child"></param>
        public static void IncludeNewChildPixels(ref ERStat child)
        {
            child.pixelsInEachThreshold[0].AddRange(new List<int>(child.pixels));
            child.pixels = new List<int>();
        }
        private static void mergeParentChildPixelLists(ref ERStat parent, ref ERStat child)
        {
            for (int i = 0; i < child.thresholdsInER.Count; i++)
            {
                mergePixels(ref parent, child.thresholdsInER[i], child.pixelsInEachThreshold[i]);
            }
        }
        private static void mergePixels(ref ERStat parent, int childLevel, List<int> pixels)
        {
            int childInsertionLevel = parent.thresholdsInER.Count;
            bool newListRequired = true;

            for (int i = parent.thresholdsInER.Count - 1; i >= 0; i--)
            {
                if (parent.thresholdsInER[i] < childLevel)
                    continue;
                else if (parent.thresholdsInER[i] > childLevel)
                {
                    childInsertionLevel = i + 1;
                    break;
                }
                else // if (parent.thresholdsInER[i] == childLevel)
                {
                    newListRequired = false;
                    childInsertionLevel = i;
                }
            }

            if (newListRequired)
            {
                if (childInsertionLevel == parent.thresholdsInER.Count)
                {
                    parent.thresholdsInER.Add(childLevel);
                    parent.pixelsInEachThreshold.Add(new List<int>(pixels));
                }
                else
                {
                    parent.thresholdsInER.Insert(childInsertionLevel, childLevel);
                    parent.pixelsInEachThreshold.Insert(childInsertionLevel, new List<int>(pixels));
                }
            }
            else
            {
                parent.pixelsInEachThreshold[childInsertionLevel].AddRange(new List<int>(pixels));
            }

        }
        #endregion

        #region HelperFunctions
        public static void ConvertToDelta(ref Mat src, int thresholdDelta)
        {
            if (thresholdDelta > 1)
            {
                using (Mat temp = new Mat(src.Rows, src.Cols, Emgu.CV.CvEnum.DepthType.Cv8U, 1))
                {
                    temp.SetTo(new MCvScalar(thresholdDelta));
                    CvInvoke.Divide(src, temp, src);
                    temp.SetTo(new MCvScalar(1));
                    CvInvoke.Subtract(src, temp, src);
                }
            }
        }
        public static byte[,] InitializeQuads()
        {
            byte[,] quads = new byte[3, 4];

            quads[0, 0] = 1 << 3;
            quads[0, 1] = 1 << 2;
            quads[0, 2] = 1 << 1;
            quads[0, 3] = 1;
            quads[1, 0] = 1 << 2 | 1 << 1 | 1;
            quads[1, 1] = 1 << 3 | 1 << 1 | 1;
            quads[1, 2] = 1 << 3 | 1 << 2 | 1;
            quads[1, 3] = 1 << 3 | 1 << 2 | 1 << 1;
            quads[2, 0] = 1 << 2 | 1 << 1;
            quads[2, 1] = 1 << 3 | 1;

            return quads;
        }
        public static bool SearchNeighbours(int x, int y, ref int currentPixel, ref int currentEdge, ref int currentLevel, ref int thresholdLevel, int width, int height, ref byte[] imageData, ref bool[] accessiblePixelMask, ref List<int>[] boundaryPixels, ref List<int>[] boundaryEdges)
        {
            for (; currentEdge < 4; currentEdge++) // den exei arxh to current edge edw giati pairnei apo to boundary edges
            {
                int neighbourPixel = currentPixel;

                switch (currentEdge)
                {
                    case 0: if (x - width + 1 < 0) neighbourPixel = currentPixel + 1; break; // deksi pixel
                    case 1: if (y - height + 1 < 0) neighbourPixel = currentPixel + width; break; // katw pixel
                    case 2: if (x > 0) neighbourPixel = currentPixel - 1; break; // aristero pixel
                    default: if (y > 0) neighbourPixel = currentPixel - width; break; // panw pixel
                }
                if (neighbourPixel != currentPixel && !accessiblePixelMask[neighbourPixel])
                {
                    int neighbourLevel = imageData[neighbourPixel];
                    accessiblePixelMask[neighbourPixel] = true; // to neighbour pixel thewreitai elegmeno

                    if (neighbourLevel >= currentLevel) // to geitoniko pixel apothikeuetai gia metagenestero elegxo
                    {
                        boundaryPixels[neighbourLevel].Add(neighbourPixel); // vrethike to tade pixel sto sygkekrimeno level
                        boundaryEdges[neighbourLevel].Add(0); // to epomeno edge toy tade pixel poy tha elegxthei einai to 0, i.e. den exei elegxthei.

                        // diorthwsh toy threshold level sthn periptwsh poy vrethike geitoniko pixel se level mikrotero toy threshold gia thn epomenh epanalhpsh alla >= apo to current level
                        // p.x. estw current level 50 alla einai to teleutaio pixel me auth thn timh opote exei oristei sthn prohgoumenh epanalhpsh oti to threshold einai 60. an to neighbour poy vrethike einai metaksu 50 kai 60 tote allazei to threshold level wste to neo pixel na einai auto poy tha upologistei sthn epomenh epanalhpsh.
                        if (neighbourLevel < thresholdLevel)
                            thresholdLevel = neighbourLevel;
                    }
                    else // to geitoniko pixel tha dhmiourghsei neo er sto level toy.
                    {
                        boundaryPixels[currentLevel].Add(currentPixel); // to pixel poy elegxotan apothikeuetai gia sunexeia sta ypoloipa edges
                        boundaryEdges[currentLevel].Add(currentEdge + 1); // to index toy epomenoy edge poy tha elegxthei

                        if (currentLevel < thresholdLevel)
                            thresholdLevel = currentLevel;

                        currentPixel = neighbourPixel;
                        currentEdge = 0;
                        currentLevel = neighbourLevel;

                        return true;
                    }
                }
            }
            return false;
        }
        public static IncrementalyComputableDescriptors ComputeDescriptors(int x, int y, int width, int height, int currentPixel, ref byte[] imageData, bool[] accumulatedPixelMask, byte[,] quads)
        {
            IncrementalyComputableDescriptors icd = new IncrementalyComputableDescriptors();

            int nonBoundaryNeighbours = 0;
            int nonBoundaryNeighboursHoriz = 0;

            byte[] quadBefore = new Byte[] { 0, 0, 0, 0 };
            byte[] quadAfter = new Byte[] { 0, 0, 0, 0 };
            quadAfter[0] = 1 << 1;
            quadAfter[1] = 1 << 3;
            quadAfter[2] = 1 << 2;
            quadAfter[3] = 1;

            for (int edge = 0; edge < 8; edge++)
            {
                int neighbour4 = -1; // von Neumann neighborhood
                int neighbour8 = -1; // Moore neighborhood
                int cell = 0;
                switch (edge)
                {   //all the ifs check if the pixel belongs in the matrix boundary
                    case 0: if (x < width - 1) { neighbour4 = neighbour8 = currentPixel + 1; } cell = 5; break;
                    case 1: if ((x < width - 1) && (y < height - 1)) { neighbour8 = currentPixel + 1 + width; } cell = 8; break;
                    case 2: if (y < height - 1) { neighbour4 = neighbour8 = currentPixel + width; } cell = 7; break;
                    case 3: if ((x > 0) && (y < height - 1)) { neighbour8 = currentPixel - 1 + width; } cell = 6; break;
                    case 4: if (x > 0) { neighbour4 = neighbour8 = currentPixel - 1; } cell = 3; break;
                    case 5: if ((x > 0) && (y > 0)) { neighbour8 = currentPixel - 1 - width; } cell = 0; break;
                    case 6: if (y > 0) { neighbour4 = neighbour8 = currentPixel - width; } cell = 1; break;
                    default: if ((y > 0) && (x < width - 1)) { neighbour8 = currentPixel + 1 - width; } cell = 2; break;
                }

                BoundaryNeighboursIter(currentPixel, imageData, accumulatedPixelMask, ref nonBoundaryNeighbours, ref nonBoundaryNeighboursHoriz, edge, neighbour4);

                int pixValue = imageData[currentPixel] + 1;
                if (neighbour8 != -1)
                    if (accumulatedPixelMask[neighbour8])
                        pixValue = imageData[neighbour8];

                if (pixValue <= imageData[currentPixel])
                {
                    QuadDifferences(ref quadBefore, ref quadAfter, cell);
                }

            } // end for edge

            int[] cBefore;
            int[] cAfter;
            EulerDifferencesIter(quads, quadBefore, quadAfter, out cBefore, out cAfter);

            int dC1 = cAfter[0] - cBefore[0];
            int dC2 = cAfter[1] - cBefore[1];
            int dC3 = cAfter[2] - cBefore[2];

            icd.NonBoundaryNeighbours = nonBoundaryNeighbours;
            icd.NonBoundaryNeighboursHor = nonBoundaryNeighboursHoriz;
            icd.dC1 = dC1;
            icd.dC2 = dC2;
            icd.dC3 = dC3;

            return icd;
        }        
        /// <summary>
        /// Calculates the Euler differences for a region after a new pixel is added.
        /// </summary>
        /// <param name="quads">The 3*4 array of the quad patterns we search for. Use "InitializeQuads" to get the correct patterns.</param>
        /// <param name="quadBefore">The previous sum of the quad patterns around the pixel.</param>
        /// <param name="quadAfter">The sum of the quad patterns after the pixel is added.</param>
        /// <param name="cBefore">Counter for the times a quad pattern is found before the current pixel.</param>
        /// <param name="cAfter">Counter for the times a quad pattern is found after the current pixel.</param>
        public static void EulerDifferencesIter(byte[,] quads, byte[] quadBefore, byte[] quadAfter, out int[] cBefore, out int[] cAfter)
        {
            cBefore = new int[] { 0, 0, 0 };
            cAfter = new int[] { 0, 0, 0 };

            for (int p = 0; p < 3; p++)
            {
                for (int q = 0; q < 4; q++)
                {
                    if ((quadBefore[0] == quads[p, q]) && ((p < 2) || (q < 2)))
                        cBefore[p]++;
                    if ((quadBefore[1] == quads[p, q]) && ((p < 2) || (q < 2)))
                        cBefore[p]++;
                    if ((quadBefore[2] == quads[p, q]) && ((p < 2) || (q < 2)))
                        cBefore[p]++;
                    if ((quadBefore[3] == quads[p, q]) && ((p < 2) || (q < 2)))
                        cBefore[p]++;

                    if ((quadAfter[0] == quads[p, q]) && ((p < 2) || (q < 2)))
                        cAfter[p]++;
                    if ((quadAfter[1] == quads[p, q]) && ((p < 2) || (q < 2)))
                        cAfter[p]++;
                    if ((quadAfter[2] == quads[p, q]) && ((p < 2) || (q < 2)))
                        cAfter[p]++;
                    if ((quadAfter[3] == quads[p, q]) && ((p < 2) || (q < 2)))
                        cAfter[p]++;
                }
            }
        }
        /// <summary>
        /// Decides which quad pattern to upgrade according to the value of the neighbour.
        /// </summary>
        /// <param name="quadBefore">The counter of each quad pattern around the area before the new pixel.</param>
        /// <param name="quadAfter">The counter of each quad pattern around the area after the new pixel.</param>
        /// <param name="cell">The neighbour (8-N) of the pixel. If the neighbour exists (a.k.a. its value is smaller than that of the current pixel) then the appropriate quad counter is modified accordingly.</param>
        public static void QuadDifferences(ref byte[] quadBefore, ref byte[] quadAfter, int cell)
        {
            switch (cell)
            {
                case 0:
                    quadBefore[3] = (byte)(quadBefore[3] | 1 << 3);
                    quadAfter[3] = (byte)(quadAfter[3] | 1 << 3);
                    break;
                case 1:
                    quadBefore[3] = (byte)(quadBefore[3] | 1 << 2);
                    quadAfter[3] = (byte)(quadAfter[3] | 1 << 2);
                    quadBefore[0] = (byte)(quadBefore[0] | 1 << 3);
                    quadAfter[0] = (byte)(quadAfter[0] | 1 << 3);
                    break;
                case 2:
                    quadBefore[0] = (byte)(quadBefore[0] | 1 << 2);
                    quadAfter[0] = (byte)(quadAfter[0] | 1 << 2);
                    break;
                case 3:
                    quadBefore[3] = (byte)(quadBefore[3] | 1 << 1);
                    quadAfter[3] = (byte)(quadAfter[3] | 1 << 1);
                    quadBefore[2] = (byte)(quadBefore[2] | 1 << 3);
                    quadAfter[2] = (byte)(quadAfter[2] | 1 << 3);
                    break;
                case 5:
                    quadBefore[0] = (byte)(quadBefore[0] | 1);
                    quadAfter[0] = (byte)(quadAfter[0] | 1);
                    quadBefore[1] = (byte)(quadBefore[1] | 1 << 2);
                    quadAfter[1] = (byte)(quadAfter[1] | 1 << 2);
                    break;
                case 6:
                    quadBefore[2] = (byte)(quadBefore[2] | 1 << 1);
                    quadAfter[2] = (byte)(quadAfter[2] | 1 << 1);
                    break;
                case 7:
                    quadBefore[2] = (byte)(quadBefore[2] | 1);
                    quadAfter[2] = (byte)(quadAfter[2] | 1);
                    quadBefore[1] = (byte)(quadBefore[1] | 1 << 1);
                    quadAfter[1] = (byte)(quadAfter[1] | 1 << 1);
                    break;
                default:
                    quadBefore[1] = (byte)(quadBefore[1] | 1);
                    quadAfter[1] = (byte)(quadAfter[1] | 1);
                    break;
            }
        }
        public static void BoundaryNeighboursIter(int currentPixel, byte[] imageData, bool[] accumulatedPixelMask, ref int nonBoundaryNeighbours, ref int nonBoundaryNeighboursHoriz, int edge, int neighbour4)
        {
            if ((neighbour4 != -1) && (accumulatedPixelMask[neighbour4]) && (imageData[neighbour4] <= imageData[currentPixel]))
            {
                nonBoundaryNeighbours++;
                if ((edge == 0) || (edge == 4))
                    nonBoundaryNeighboursHoriz++;
            }
        }

        public static void CalcNM2stats(IInputArray inputIm, ref ERStat stat)
        {
            using (Mat src = inputIm.GetInputArray().GetMat())
            {
                if (src.Depth != DepthType.Cv8U) { return; }
                using (Mat regionMask = new Mat(src.Rows + 2, src.Cols + 2, DepthType.Cv8U, 1))
                using (Mat regToFill = new Mat(src, new Rectangle(stat.rect.X, stat.rect.Y, stat.rect.Width, stat.rect.Height)))
                using (Mat region1 = new Mat(regionMask, new Rectangle(stat.rect.X, stat.rect.Y, stat.rect.Width + 2, stat.rect.Height + 2)))
                {
                    region1.SetTo(new MCvScalar(0));
                    int newVal = 255;
                    FloodFillType flags = (FloodFillType)(255 << 8) | FloodFillType.FixedRange | FloodFillType.MaskOnly;
                    Rectangle rect = new Rectangle();

                    CvInvoke.FloodFill(
                        regToFill,
                        region1,
                        new Point(stat.pixel % src.Cols - stat.rect.X, stat.pixel / src.Cols - stat.rect.Y),
                        new MCvScalar(newVal),
                        out rect,
                        new MCvScalar(stat.level),
                        new MCvScalar(0),
                        Connectivity.FourConnected,
                        flags);

                    rect.Width += 2;
                    rect.Height += 2;

                    using (Mat region = new Mat(region1, rect))
                    using (VectorOfVectorOfPoint contours = new VectorOfVectorOfPoint())
                    using (Mat hierarchy = new Mat())
                    {
                        CvInvoke.FindContours(region, contours, hierarchy, RetrType.Tree, ChainApproxMethod.ChainApproxNone, new Point(0, 0));

                        using (VectorOfPoint contourPoly = new VectorOfPoint())
                        {
                            CvInvoke.ApproxPolyDP(contours[0], contourPoly, (float)Math.Min(rect.Width, rect.Height) / 17, true);
                            bool wasConvex = true;
                            int numInflexionPoints = 0;
                            for (int p = 0; p < contourPoly.Size; p++)
                            {
                                int pPrev = p - 1;
                                int pNext = p + 1;
                                if (pPrev == -1) pPrev = contourPoly.Size - 1;
                                if (pNext == contourPoly.Size) pNext = 0;

                                double angleNext = Math.Atan2((contourPoly[pNext].Y - contourPoly[p].Y),
                                                                (contourPoly[pNext].X - contourPoly[p].X));
                                double anglePrev = Math.Atan2((contourPoly[pPrev].Y - contourPoly[p].Y),
                                                                (contourPoly[pPrev].X - contourPoly[p].X));

                                if (angleNext < 0) angleNext += 2d * Math.PI;

                                double angle = angleNext - anglePrev;

                                if (angle > 2d * Math.PI) angle -= 2d * Math.PI;
                                else if (angle < 0) angle = 2d * Math.PI + Math.Abs(angle);

                                if (p > 0)
                                    if (((angle > Math.PI) && (!wasConvex)) || ((angle < Math.PI) && (wasConvex))) numInflexionPoints++;

                                wasConvex = (angle > Math.PI);
                            }
                            stat.numInflexionPoints = (float)numInflexionPoints;
                        }                    
                        Rectangle trivial;
                        CvInvoke.FloodFill(region, null, new Point(0, 0), new MCvScalar(255), out trivial, new MCvScalar(), new MCvScalar());
                        int holesArea = region.Cols * region.Rows - CvInvoke.CountNonZero(region);
                        //VectorOfPoint hull = new VectorOfPoint();
                        //CvInvoke.ConvexHull(contours[0], hull, false);
                        //int hullArea = (int)CvInvoke.ContourArea(hull);
                        int hullArea = (int)Utils.CalcConvexHullArea(contours);
                        float contourArea = (float)Utils.CalcContourArea(contours);

                        stat.holeAreaRatio = (float)holesArea / stat.area;
                        stat.convexHullRatio = Utils.CalcConvexHullAreaRatio(hullArea, contourArea);
                    }                             
                }
            }
        }
        #endregion

        #region Classification Utils

        public static List<ERStat> Stage1SvmDecision(IInputArray image, List<ERStat> regions, String svmPath, bool oppositeChannel, String imageName)
        {
            Mat src = image.GetInputArray().GetMat();
            Mat bgr = new Mat(imageName, LoadImageType.Unchanged);
            if (oppositeChannel)
            {
                CvInvoke.BitwiseNot(bgr, bgr);
            }
            //Mat imColorSpace = new Mat();
            //ColorConversionWrapper.ColorConversions(bgr, ColorConversion.Bgr2Lab, ref imColorSpace);

            FileStorage fs;
            SVM svm = SvmUtils.LoadSVMFromFile(svmPath, out fs);

            Matrix<float> sample = new Matrix<float>(regions.Count(), 4);
            Matrix<float> response = new Matrix<float>(regions.Count(), 1);

            for (int i = 0; i < regions.Count; i++)
            {
                ERStat stat = regions[i];
                //float colorDistanceERBB = ERUtils.ColorDistanceERBB(src, imColorSpace, stat, LabColorDistance);
                sample[i, 0] = (float)(stat.rect.Width) / (stat.rect.Height);
                sample[i, 1] = (float)Math.Sqrt((float)(stat.area)) / stat.perimeter;
                sample[i, 2] = (float)(1 - stat.euler);
                sample[i, 3] = stat.medCrossings;
                //sample[i, 4] = colorDistanceERBB;
            }
            // get the class predictions
            svm.Predict(sample, response);

            Matrix<float> probs = new Matrix<float>(regions.Count, 1);
            svm.Predict(sample, probs, 1);
            for (int i = 0; i < probs.Rows; i++)
            {
                float exp = (float)Math.Exp(-2 * probs[i, 0]);
                probs[i, 0] = (float)1 - (float)1 / (1 + exp);
                regions[i].probability = 1 - probs[i, 0];
                regions[i].svmResponce = response[i, 0];
            }

            SvmUtils.UnloadModel(fs);
            sample.Dispose();
            response.Dispose();

            return regions;
        }
        public static List<ERStat> Stage2SvmDesicion(IInputArray image, List<ERStat> regions, String svmPath, bool oppositeChannel, String imageName)
        {
            Mat src = image.GetInputArray().GetMat();
            Mat bgr = new Mat(imageName, LoadImageType.Unchanged);
            if (oppositeChannel)
            {
                CvInvoke.BitwiseNot(bgr, bgr);
            }
            Mat imColorSpace = new Mat();
            ColorConversionWrapper.ColorConversions(bgr, ColorConversion.Bgr2Lab, ref imColorSpace);

            FileStorage fs;
            SVM svm = SvmUtils.LoadSVMFromFile(svmPath, out fs);

            Matrix<float> sample = new Matrix<float>(regions.Count(), 3);
            Matrix<float> response = new Matrix<float>(regions.Count(), 1);

            for (int i = 0; i < regions.Count; i++)
            {
                ERStat stat = regions[i];
                ERUtils.CalcNM2stats(src, ref stat); // calculate the nm2 features
                
                //float colorDistanceERBB = ERUtils.ColorDistanceERBB(src, imColorSpace, stat, LabColorDistance);
                sample[i, 0] = stat.holeAreaRatio;
                sample[i, 1] = stat.convexHullRatio;
                sample[i, 2] = stat.numInflexionPoints;
                //sample[i, 3] = colorDistanceERBB;
            }

            // get the class predictions
            svm.Predict(sample, response);

            Matrix<float> probs = new Matrix<float>(regions.Count, 1);
            svm.Predict(sample, probs, 1);
            for (int i = 0; i < probs.Rows; i++)
            {
                float exp = (float)Math.Exp(-2 * probs[i, 0]);
                probs[i, 0] = (float)1 - (float)1 / (1 + exp);
                regions[i].probability = 1 - probs[i, 0];
                regions[i].svmResponce = response[i, 0];
            }

            SvmUtils.UnloadModel(fs);
            sample.Dispose();
            response.Dispose();

            return regions;
        }

        public static Rectangle[] WordStatKaratzasPruning(WordStat[] wordStats, Rectangle[] rectangles, Size imSize, bool responsePruning, float minProbability = 0.5f)
        {
            if (wordStats.Length != rectangles.Length) throw new Exception("Gamw to spiti sou");
            KaratzasProbabilityCalculation karprocal = new KaratzasProbabilityCalculation();

            double[] probs = new double[wordStats.Length];

            for (int i = 0; i < wordStats.Length; i++)
            {
                probs[i] = karprocal.KaratzasProbability(wordStats[i].ERs, wordStats[i].features, imSize);
            }

            List<Rectangle> resultsRects = new List<Rectangle>();
            for (int i = 0; i < probs.Length; i++)
            {
                if (probs[i] > minProbability)
                    resultsRects.Add(rectangles[i]);
            }
            return resultsRects.ToArray();
        }

        public static Rectangle[] WordPruning<T>(BaseFeature[] wordStats, IStatModel model, Rectangle[] rectangles, bool responsePruning, Constants.FeatureType f3) where T : SVMExample, new()
        {
            T svmExample = new T();
            svmExample.SetFeatureType(f3);
            Matrix<float> sample = new Matrix<float>(wordStats.Length, svmExample.DecideFeatureLength(f3));

            for (int i = 0; i < wordStats.Length; i++)
            {
                svmExample.SetRowSample(wordStats[i], ref sample, i);
            }
            using (Matrix<float> response = new Matrix<float>(wordStats.Length, 1))
            {
                // get the class predictions
                model.Predict(sample, response);

                for (int i = 0; i < response.Rows; i++)
                {
                    wordStats[i].svmResponse = response[i, 0];
                }
            }

            List<Rectangle> resultRects = new List<Rectangle>();
            for (int i = 0; i < wordStats.Length; i++)
            {
                if (wordStats[i].svmResponse == 1) resultRects.Add(rectangles[i]);
            }
            sample.Dispose();

            return resultRects.ToArray();

        }

        public static Rectangle[] WordStatSvmPruning<T>(BaseFeature[] wordStats, String svmPath, Rectangle[] rectangles, Constants.FeatureType f3, bool responsePruning, float minProbability = 0.5f)
            where T : SVMExample, new()
        {
            FileStorage fs;
            SVM svm = SvmUtils.LoadSVMFromFile(svmPath, out fs);

            T svmExample = new T();
            svmExample.SetFeatureType(f3);

            Matrix<float> sample = new Matrix<float>(wordStats.Length, svmExample.DecideFeatureLength(f3));

            for (int i = 0; i < wordStats.Length; i++)
            {
                svmExample.SetRowSample(wordStats[i], ref sample, i);
            }
            using (Matrix<float> response = new Matrix<float>(wordStats.Length, 1))
            {
                // get the class predictions
                svm.Predict(sample, response);

                using (Matrix<float> probs = new Matrix<float>(wordStats.Length, 1))
                {
                    svm.Predict(sample, probs, 1);
                    for (int i = 0; i < response.Rows; i++)
                    {
                        float exp = (float)Math.Exp(-2 * probs[i, 0]);
                        probs[i, 0] = (float)1 - (float)1 / (1 + exp);
                        wordStats[i].probability = 1 - probs[i, 0];
                        wordStats[i].svmResponse = response[i, 0];
                    }
                }
            }

            SvmUtils.UnloadModel(fs);

            List<Rectangle> resultRects = new List<Rectangle>();
            for (int i = 0; i < wordStats.Length; i++)
            {
                if (responsePruning)
                {
                    if (wordStats[i].svmResponse == 0) resultRects.Add(rectangles[i]);
                }
                else
                {
                    if (wordStats[i].probability > minProbability) resultRects.Add(rectangles[i]);
                }
            }
            sample.Dispose();

            return resultRects.ToArray();
        }

        // TODO: allagh gia na epitrepei dtrees kai rtrees gia classification kai oxi mono svm
        public static List<ERStat> TwoClassSvmPruning(IInputArray imageChannel, IInputArray imageBgr, List<ERStat> regions, String svmPath, bool oppositeChannel, Constants.FeatureType featureType)
        {
            FileStorage fs;
            SVM svm = SvmUtils.LoadSVMFromFile(svmPath, out fs);

            Matrix<float> sample = new Matrix<float>(regions.Count, SVMFullFeatureExample.DecideFeatureLengthStatic(featureType));

            using (Mat src = imageChannel.GetInputArray().GetMat())
            using (Mat bgr = imageBgr.GetInputArray().GetMat())
            {
                if (oppositeChannel) { CvInvoke.BitwiseNot(bgr, bgr); }
                Mat pii = new Mat(), lab = new Mat();
                if (featureType.HasFlag(Constants.FeatureType.ColorDistPii))
                    ColorConversionWrapper.ColorConversions(bgr, ColorConversionWrapper.ManualConversions.Pii, ref pii);
                if (featureType.HasFlag(Constants.FeatureType.ColorDistLab) || featureType.HasFlag(Constants.FeatureType.ColorConsistencyLab))
                    ColorConversionWrapper.ColorConversions(bgr, ColorConversion.Bgr2Lab, ref lab);

                FullFeatureObject ffo = new FullFeatureObject();
                SVMFullFeatureExample svmExample = new SVMFullFeatureExample(featureType);
                for (int i = 0; i < regions.Count; i++)
                {
                    ERStat stat = regions[i];
                    if (stat.ffo != null) ffo = stat.ffo; //use for more info in WordRectangleForm
                    if (featureType.HasFlag(Constants.FeatureType.NM))
                    {
                        ERUtils.CalcNM2stats(src, ref stat); // calculate the nm2 features
                        NMFeatureObject nmfeature = new NMFeatureObject((float)(stat.rect.Width) / (stat.rect.Height), (float)Math.Sqrt((float)(stat.area)) / stat.perimeter, (float)(1 - stat.euler), stat.medCrossings, stat.holeAreaRatio, stat.convexHullRatio, stat.numInflexionPoints);
                        ffo.nmFeature = nmfeature;
                    }
                    if (featureType.HasFlag(Constants.FeatureType.Histogram))
                    {
                        using (Mat bb = new Mat(src, stat.rect), erMask = Utils.ErMaskFloodFill(src, stat, true))
                        {
                            HistogramCalculations hc = new HistogramCalculations(bb, erMask);
                            HistogramCalculations.HistFeatures hf = hc.calculateHistFeatures();
                            ffo.setHistFeaturesObject(hf);
                        }
                    }
                    if (featureType.HasFlag(Constants.FeatureType.ColorDistPii))
                    {
                        float colorDistanceERBBPii = ERUtils.ColorDistanceERBB(src, pii, stat, EuclidianColorDistance);
                        ffo.colorDistERBB = colorDistanceERBBPii;
                    }
                    if (featureType.HasFlag(Constants.FeatureType.ColorDistLab))
                    {
                        float colorDistanceERBBLab = ERUtils.ColorDistanceERBB(src, lab, stat, LabColorDistance);
                        ffo.colorDistERBBLab = colorDistanceERBBLab;
                    }
                    if (featureType.HasFlag(Constants.FeatureType.ColorDistBgr))
                    {
                        float colorDistanceERBBBgr = ERUtils.ColorDistanceERBB(src, bgr, stat, EuclidianColorDistance);
                        ffo.colorDistERBBBgr = colorDistanceERBBBgr;
                    }
                    if (featureType.HasFlag(Constants.FeatureType.ColorConsistencyLab))
                    { // TODO: If ever used again, na fix thn apodosh mnhmhs ths function
                        float colorConsistencyLab = ERUtils.ColorConsistencyER(src, lab, stat, LabColorDistance);
                        ffo.colorConsistencyLab = colorConsistencyLab;
                    }
                    if (featureType.HasFlag(Constants.FeatureType.SignificantPaths))
                    { // TODO: na ftiaksw thn apodosh se auta. 1) swsto er mask, 2) using gia ola ta "Mat" mesa stis functions
                        //float significantPaths = ERUtils.SignificantPaths(stat, src);
                        using (Mat erMask = Utils.ErMaskFloodFill(src, stat, false))
                        {
                            PathStats pathStats = ERUtils.SignificantPaths(erMask);
                            float contourSum = Utils.GetContourPointsSum(erMask);
                            ffo.significantPaths = pathStats.NumberOfPaths;
                            ffo.skelContourRatio = pathStats.GetTotalPixelsCount() / contourSum;
                            ffo.skelAreaRatio = pathStats.GetTotalPixelsCount() / (float)stat.area;
                            ffo.areaBbRatio = stat.area / (float)(stat.rect.Width * stat.rect.Height);
                        }                      
                    }
                    if (featureType.HasFlag(Constants.FeatureType.NumberOfHoles))
                    {
                        float numberOfHoles = (float)(1 - stat.euler);
                        ffo.nmFeature.numOfHoles = numberOfHoles;
                    }
                    if (featureType.HasFlag(Constants.FeatureType.ConvexityDefects) || featureType.HasFlag(Constants.FeatureType.StrokeRatio))
                    using (Mat erMask = Utils.ErMaskFloodFill(src, stat, false))
                    {
                        if (featureType.HasFlag(Constants.FeatureType.ConvexityDefects))
                        using (VectorOfVectorOfPoint contours = Utils.CalcContours(erMask))
                        if (contours.Size > 0)
                        using (VectorOfInt hullPoints = Utils.CalcConvexHullPoints(contours))
                        {
                            float ConvexityDefects = Utils.CalcConvexityDefects(contours, hullPoints);
                            if (float.IsNaN(ConvexityDefects)) ConvexityDefects = 0;
                            ffo.ConvexityDefects = ConvexityDefects;
                        }
                        if (featureType.HasFlag(Constants.FeatureType.StrokeRatio))
                        {
                            float StrokeWidth = StrokeUtils.MeanStrokeWidth(erMask);
                            if (float.IsNaN(StrokeWidth)) StrokeWidth = 0;
                            ffo.StrokeWidthERWidthRatio = StrokeWidth / stat.rect.Width;
                        }
                    }
                    stat.ffo = FullFeatureObject.FFOFactoryFromObject(ffo); // todo: make comment to save spacetime
                    
                    svmExample.SetRowSample(ffo, ref sample, i);
                }
                pii.Dispose();
                lab.Dispose();
            }
            using (Matrix<float> response = new Matrix<float>(regions.Count(), 1))
            {
                // get the class predictions
                svm.Predict(sample, response);
                using (Matrix<float> probs = new Matrix<float>(regions.Count, 1))
                {
                    svm.Predict(sample, probs, 1);
                    for (int i = 0; i < response.Rows; i++)
                    {
                        float exp = (float)Math.Exp(-2 * probs[i, 0]);
                        probs[i, 0] = (float)1 - (float)1 / (1 + exp);
                        regions[i].probability = 1 - probs[i, 0];
                        regions[i].svmResponce = response[i, 0];
                    }
                }
            }
            SvmUtils.UnloadModel(fs);                  
            sample.Dispose();
            return regions;
        }

        public static List<ERStat> OneClassSVMpruning(IInputArray image, List<ERStat> regions, String svmPath, bool positive)
        {
            Mat src = image.GetInputArray().GetMat();

            FileStorage fs;
            SVM svm = SvmUtils.LoadSVMFromFile(svmPath, out fs);

            // load the er stats as normalized samples
            Matrix<float> sample = new Matrix<float>(regions.Count(), 7);
            Matrix<float> response = new Matrix<float>(regions.Count(), 1);
            for (int i = 0; i < regions.Count; i++)
            {
                ERStat stat = regions[i];
                ERUtils.CalcNM2stats(src, ref stat); // calculate the nm2 features
                NMFeatureObject nmfeature = new NMFeatureObject((float)(stat.rect.Width) / (stat.rect.Height), (float)Math.Sqrt((float)(stat.area)) / stat.perimeter, (float)(1 - stat.euler), stat.medCrossings, stat.holeAreaRatio, stat.convexHullRatio, stat.numInflexionPoints);

                nmfeature.NormalizeL2();

                SVMFullFeatureExample.NMFeatureToMatrixSample(nmfeature, ref sample, i);
            }
            // get the class predictions
            svm.Predict(sample, response);

            // recreate the regions list from the accepted samples
            List<ERStat> tempRes = new List<ERStat>(regions);
            regions = new List<ERStat>(tempRes.Count);
            for (int i = 0; i < response.Rows; i++)
                if (positive && response[i, 0] == 1)
                {
                    regions.Add(tempRes[i]);
                }
                else if ((!positive) && response[i, 0] != 1)
                {
                    regions.Add(tempRes[i]);
                }

            SvmUtils.UnloadModel(fs);
            sample.Dispose();
            response.Dispose();

            return regions;
        }
        #endregion

        internal static ERStat SearchRegions(List<ERStat> regions, FullFeatureObject ffo)
        {
            foreach (ERStat r in regions)
            {
                if (r.pixel == ffo.startPixel && r.level == ffo.thresholdLevel && r.rect.Equals(ffo.rect))
                {
                    return r;
                }
            }
            return null;
        }

        /// <summary>
        /// Calculates the mean color per channel of the input image, for the region covered by <c>erMask</c> defined by the rectangle in <c>er</c>
        /// </summary>
        /// <param name="image">The image in the pii colorspace preferably</param>
        /// <param name="erMask">The pixel mask</param>
        /// <param name="er">The rectangle in the er</param>
        /// <returns></returns>
        /// 
        public static MCvScalar CalcMeanColor(Mat image, Mat erMask, Rectangle erRect)
        {
            using (Mat roi = new Mat())
            {
                using (Mat roiTemp = new Mat(image, erRect))
                    roiTemp.CopyTo(roi);
                MCvScalar erColor = CvInvoke.Mean(roi, erMask);
                return erColor;
            }
        }
        public static MCvScalar[] CalcMeanStdColor(Mat image, ref Mat erMask, ERStat er)
        {
            using (Mat roiTemp = new Mat(image, er.rect))
            {
                MCvScalar mean = new MCvScalar();
                MCvScalar std = new MCvScalar();
                using (Mat roi = new Mat())
                {
                    roiTemp.CopyTo(roi);
                    CvInvoke.MeanStdDev(roi, ref mean, ref std, erMask);
                }

                MCvScalar[] result = new MCvScalar[2];
                result[0] = mean;
                result[1] = std;

                return result; 
            }
        }

        public static float EuclidianColorDistance(MCvScalar color1, MCvScalar color2)
        {
            return (float)Math.Sqrt(Math.Pow(color1.V0 - color2.V0, 2.0) + Math.Pow(color1.V1 - color2.V1, 2.0) + Math.Pow(color1.V2 - color2.V2, 2.0));
        }
        public static float LabColorDistance(MCvScalar color1, MCvScalar color2)
        {
            double L1, L2, a1, a2, b1, b2;
            L1 = (float)color1.V0 * 100 / 255;
            L2 = (float)color2.V0 * 100 / 255;
            a1 = (float)color1.V1 - 128;
            a2 = (float)color2.V1 - 128;
            b1 = (float)color1.V2 - 128;
            b2 = (float)color2.V2 - 128;
            //L1 = lab1.X;
            //L2 = lab2.X;
            //a1 = lab1.Y;
            //a2 = lab2.Y;
            //b1 = lab1.Z;
            //b2 = lab2.Z;

            double kL = 1d, kC = 1d, kH = 1d;
            double C1ab, C2ab, CabMean, G, a1tonos, a2tonos, C1tonos, C2tonos, h1tonosDeg, h2tonosDeg, h1tonosRad, h2tonosRad, DLtonos, DCtonos, Dhtonos, DHtonos, Ltonos, Ctonos, hTonosDeg, Tdeg, dThetaDeg, dThetaRad, Rc, SL, SC, SH, Rt, de00;
            const double toRad = Math.PI / 180d;
            const double toDeg = 180d / Math.PI;
            // Calculate Ci', hi'
            C1ab = Math.Sqrt(a1 * a1 + b1 * b1);
            C2ab = Math.Sqrt(a2 * a2 + b2 * b2);
            CabMean = (C1ab + C2ab) / 2;
            G = 0.5 * (1 - Math.Sqrt(Math.Pow(CabMean, 7) / (Math.Pow(CabMean, 7) + Math.Pow(25, 7))));
            a1tonos = (1 + G) * a1;
            a2tonos = (1 + G) * a2;
            C1tonos = Math.Sqrt(a1tonos * a1tonos + b1 * b1);
            C2tonos = Math.Sqrt(a2tonos * a2tonos + b2 * b2);
            h1tonosRad = (b1 == 0 && a1tonos == 0) ? 0 : Math.Atan2(b1 * toRad, a1tonos * toRad);
            h2tonosRad = (b2 == 0 && a2tonos == 0) ? 0 : Math.Atan2(b2 * toRad, a2tonos * toRad);
            h1tonosRad = h1tonosRad < 0 ? h1tonosRad + 2 * Math.PI : h1tonosRad;
            h2tonosRad = h2tonosRad < 0 ? h2tonosRad + 2 * Math.PI : h2tonosRad;
            h1tonosDeg = h1tonosRad * toDeg;
            h2tonosDeg = h2tonosRad * toDeg;

            // Calculate ΔL', ΔC', ΔH'
            DLtonos = L2 - L1;
            DCtonos = C2tonos - C1tonos;
            if (C1tonos == 0 || C2tonos == 0)
            {
                Dhtonos = 0;
            }
            else
            {
                double diff = h2tonosDeg - h1tonosDeg;
                
                if (diff > 180)
                    Dhtonos = diff - 360;
                else if (diff < -180)
                    Dhtonos = diff + 360;
                else //if (Math.Abs(diff) <= 180)
                    Dhtonos = diff;
            }
            DHtonos = 2 * Math.Sqrt(C1tonos * C2tonos) * Math.Sin((Dhtonos / 2 * toRad));

            // Calculate CIEDE2000 Color Difference DE00
            Ltonos = (L1 + L2) / 2;
            Ctonos = (C1tonos + C2tonos) / 2;

            if (C1tonos == 0 || C2tonos == 0)
            {
                hTonosDeg = h1tonosDeg + h2tonosDeg;
            }
            else
            {
                double diff = h1tonosDeg - h2tonosDeg;
                double sum = h1tonosDeg + h2tonosDeg;
                if (Math.Abs(diff) > 180)
                {
                    hTonosDeg = sum < 360 ? (sum + 360) / 2 : (sum - 360) / 2;
                }
                else
                {
                    hTonosDeg = (sum) / 2;
                }
            }

            Tdeg = 1 - 0.17 * Math.Cos((hTonosDeg - 30) * toRad) + 0.24 * Math.Cos(2 * hTonosDeg * toRad) + 0.32 * Math.Cos((3 * hTonosDeg + 6) * toRad) - 0.2 * Math.Cos((4 * hTonosDeg - 63) * toRad);

            dThetaDeg = 30 * Math.Exp(-Math.Pow(((hTonosDeg - 275) / 25), 2));
            dThetaRad = dThetaDeg * toRad;

            Rc = 2 * Math.Sqrt(Math.Pow(Ctonos, 7) / (Math.Pow(Ctonos, 7) + Math.Pow(25, 7)));

            SL = 1 + (0.015 * Math.Pow(Ltonos - 50, 2)) / Math.Sqrt(20 + Math.Pow(Ltonos - 50, 2));
            SC = 1 + 0.045 * Ctonos;
            SH = 1 + 0.015 * Ctonos * Tdeg;
            
            Rt = -((Math.Sin(2 * dThetaRad)) * Rc);

            double temp0,temp1,temp2;
            temp0 = DLtonos / (kL * SL);
            temp1 = DCtonos / (kC * SC);
            temp2 = DHtonos / (kH * SH);
            de00 = Math.Sqrt(Math.Pow(temp0, 2) + Math.Pow(temp1, 2) + Math.Pow(temp2, 2) + Rt * temp1 * temp2);

            return (float)de00;
        }

        public delegate float ColorDistanceCalculator(MCvScalar color1, MCvScalar color2);
        public static float ColorDistanceERBB(Mat imGray, Mat imColor, ERStat stat, ColorDistanceCalculator calcColorDist)
        {
            // create the mask from the er pixels
            using (Mat erMask = Utils.ErMaskFloodFill(imGray, stat))
            {
                // calc the mean color for the er pixels for each channel
                MCvScalar meanColorEr = ERUtils.CalcMeanColor(imColor, erMask, stat.rect);
                // calc the mean color for the bb pixels that dont belong in the er for each channel
                CvInvoke.BitwiseNot(erMask, erMask);
                MCvScalar meanColorBB = ERUtils.CalcMeanColor(imColor, erMask, stat.rect);
                
                // use the delegate to calculate the color distance
                float colorDistance = calcColorDist(meanColorEr, meanColorBB);
                return colorDistance;
            }
        }
        public static float ColorDistance2Mats(Mat mat1, Mat mat2, Mat imColor, Rectangle erRect1, Rectangle erRect2, ColorDistanceCalculator calcColorDist)
        {
            MCvScalar meanColorMat1 = ERUtils.CalcMeanColor(imColor, mat1, erRect1);
            MCvScalar meanColorMat2 = ERUtils.CalcMeanColor(imColor, mat2, erRect2);

            float colorDistance = calcColorDist(meanColorMat1, meanColorMat2);
            return colorDistance;
        }
        public static float ColorDistanceMatValue(Mat mat, Mat imColor, Rectangle erRect, MCvScalar value, ColorDistanceCalculator calcColorDist)
        {
            MCvScalar meanColorMat = ERUtils.CalcMeanColor(imColor, mat, erRect);
            float colorDistance = calcColorDist(meanColorMat, value);
            return colorDistance;
        }
        public static float ColorConsistencyER(Mat imGray, Mat imColor, ERStat stat, ColorDistanceCalculator calcCololDist)
        {
            List<int> sMed = CalculateSmed(stat, false); // ta dominant color pixels
            Mat domMask = new Mat(), erMask = new Mat();
            Utils.BinaryMatFromERPixels(sMed, stat.rect, imGray, ref domMask, false); // se mask
            Utils.BinaryMatFromERPixels(stat, imGray, ref erMask, false); // to binary mask oloy toy er

            float colorDistanceERmean = ColorDistance2Mats(domMask, erMask, imColor, stat.rect, stat.rect, calcCololDist);
            domMask.Dispose();
            erMask.Dispose();

            return colorDistanceERmean;
        }
        public static List<int> CalculateSmed(ERStat cer, bool descending)
        {
            List<int> sMed = new List<int>(); // a sorted list of pixels

            int totalArea = cer.area;
            int threshLvl;
            if (descending) threshLvl = 0; else threshLvl = cer.thresholdsInER.Count - 1; // gia to reverse tree
            int remainingSpots = (int)Math.Ceiling(totalArea / 2.0); // 50% of pixels

            while (remainingSpots > 0)
            {
                if (cer.pixelsInEachThreshold[threshLvl].Count <= remainingSpots)
                {
                    sMed.AddRange(cer.pixelsInEachThreshold[threshLvl]);
                    remainingSpots -= cer.pixelsInEachThreshold[threshLvl].Count;
                    if (descending) threshLvl++; else threshLvl--;
                }
                else
                {
                    for (int i = 0; i < remainingSpots; i++)
                    {
                        sMed.Add(cer.pixelsInEachThreshold[threshLvl][i]);
                    }
                    remainingSpots = 0;
                }
            }
            return sMed;
        }
        public static PathStats SignificantPaths(Mat erPixels)
        {
            Mat skeleton = new Mat();
            ZhangSuenThinning tt = new ZhangSuenThinning();
            int steps = tt.Skeletonize(erPixels, ref skeleton);
            //StrokeUtils.RunCa(erPixels, ref skeleton);

            Mat neighbourMat = new Mat(skeleton.Rows, skeleton.Cols, DepthType.Cv8U, 1);
            StrokeUtils.CreateNeighbourMat(skeleton, ref neighbourMat);

            double minVal = -1, maxVal = 0;
            Point minLoc = new Point(), maxLoc = new Point();
            CvInvoke.MinMaxLoc(neighbourMat, ref minVal, ref maxVal, ref minLoc, ref maxLoc, skeleton);

            if (minVal > 0)
            {
                List<int>[] neighboursArr = StrokeUtils.GetNeighbourIndices(skeleton);
                StrokeUtils.SortNeighboursToDistance(ref neighboursArr, skeleton.Cols);

                PathStats pathStats = StrokeUtils.FindPaths(skeleton, neighboursArr, minLoc);

                return pathStats;
            }
            else
            {
                return new PathStats(); // dummy return value in case there is no skeleton found
            }
            

            // old paths
            //Mat res = new Mat(skeleton.Rows, skeleton.Cols, DepthType.Cv8U, 1);
            //List<int> pathLengths;
            //StrokeUtils.CreatePaths(skeleton, neighbourMat, minLoc, out pathLengths, ref res);
            //return pathLengths.Count(n => n > 3);
        }
    }
}
