﻿using Emgu.CV;
using Emgu.CV.CvEnum;
using Emgu.CV.Structure;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using WordLocRec.DataStructs;
using WordLocRec.Tools;

namespace WordLocRec
{
    public class StrokeUtils
    {

        /// <summary>
        /// Calculates the number of neighbours of all the pixels. Only the pixels that were initially true (val = 255) are saved.
        /// </summary>
        /// <param name="input">The input array with values 0 and 255 only - binary like</param>
        /// <param name="neighbourMat">The output array with a number between 0 and 8 in each cell.</param>
        public static void CreateNeighbourMat(Mat input, ref Mat neighbourMat)
        {
            // initialize the kernel to filter the image with
            float[] k = new float[9];
            for (int i = 0; i < 9; i++)
                k[i] = 1f / 255;
            k[4] = 0;
            Mat kernel = new Mat(3, 3, DepthType.Cv32F, 1);
            kernel.SetTo(k);

            // filter the image - can't use a mask
            CvInvoke.Filter2D(input, neighbourMat, kernel, new Point(-1, -1), 0, BorderType.Constant);

            // bitwise AND with the input image to keep only the positions that were initially true
            CvInvoke.BitwiseAnd(input, neighbourMat, neighbourMat);
        }

        /// <summary>
        /// Defines the 1-D indices for each of the neighbours of a pixel and saves them in a separate list for each pixel.
        /// <para />
        /// It filters the image with a clockwise binary pattern and depends on the sum to decide the location of each neighbour.
        /// </summary>
        /// <param name="skeleton">The input array</param>
        /// <returns>Outputs an array of lists. The lenght of the array equals the number of pixels in the input image and a lot of them contain empty lists.</returns>
        public static List<int>[] GetNeighbourIndices(Mat skeleton)
        {
            Mat pattern = new Mat(3, 3, DepthType.Cv8U, 1);
            ZhangSuenThinning.InitializeKernel(ref pattern);
            int width = skeleton.Cols;
            using (Mat sums = new Mat(skeleton.Rows, width, DepthType.Cv8U, 1))
            {
                using (Mat inputBinary = new Mat())
                {
                    // turn to 0 - 1 for the binary pattern
                    CvInvoke.Threshold(skeleton, inputBinary, 127, 1, ThresholdType.Binary); 
                    // filter and keep only the true values
                    CvInvoke.Filter2D(inputBinary, sums, pattern, new Point(-1, -1), 0, BorderType.Constant);
                    pattern.Dispose();
                }
                // bitwise AND with the input image to keep only the positions that were initially true
                CvInvoke.BitwiseAnd(sums, skeleton, sums);

                // TODO: investigate parallelization using opencv methods
                byte[] data = sums.GetData();
                List<int>[] neighbours = new List<int>[data.Length];

                // decode the binary pattern around each pixel
                for (int i = 0; i < data.Length; i++)
                {
                    neighbours[i] = new List<int>();
                    if (data[i] == 0) continue;

                    for (int j = 7; j >= 0; j--) // need to calculate the biggest values first because e.q. 64 can be found inside 128
                    {
                        byte locationSum = (byte)Math.Pow(2, j);
                        if (data[i] >= locationSum)
                        {
                            neighbours[i].Add(NeighbourIndexFromSum(i, locationSum, width));
                            data[i] -= locationSum;
                        }
                        if (data[i] == 0) break;
                    }
                }
                return neighbours;
            }    
        }
        
        /// <summary>
        /// Gets the 1-D index of a neighbour at certain <paramref name="location"/> around the <paramref name="center"/>.
        /// </summary>
        /// <param name="center">The 1-D index of the center pixel.</param>
        /// <param name="location">The binary pattern sum that defines the position at the 9-N that a neighbour pixel is found.</param>
        /// <param name="width">The width of the 2-D image to translate to a translate to the 1-D Mat.</param>
        /// <returns>The 1-D index of the neighbour.</returns>
        private static int NeighbourIndexFromSum(int center, byte location, int width)
        {
            int result;
            //this method is pattern variant i.e. with a different pattern kernel the positions inside the switch should change
            switch (location)
            {
                case 1: result = center - width; break;
                case 2: result = center - width + 1; break;
                case 4: result = center + 1; break;
                case 8: result = center + width + 1; break;
                case 16: result = center + width; break;
                case 32: result = center + width - 1; break;
                case 64: result = center - 1; break;
                case 128: result = center - width - 1; break;
                default: result = -1; break; // it should never get here hopefully :)
            }
            return result;
        }

        /// <summary>
        /// Each neighbour list is sorted (descending) on the shortest mahalanobis distance, i.e. if the dist == 1 the neighbour is either east or west and if the dist == <paramref name="width"/> it is north or south and all are closer than sqrt(2) which are the corners
        /// </summary>
        /// <param name="neighboursArr">The array of the neighbour lists.</param>
        /// <param name="width">Width of the original matrix in order to translate between 1-D and 2-D coordinates.</param>
        public static void SortNeighboursToDistance(ref List<int>[] neighboursArr, int width)
        {
            for (int i = 0; i < neighboursArr.Length; i++)
            {
                var query = from n in neighboursArr[i]
                            orderby Math.Abs(i - n) != 1 && Math.Abs(i - n) != width, n
                            select n;
                neighboursArr[i] = query.ToList();
            }
            
        }

        /// <summary>
        /// Extract the paths from a binary image that has been thinned to 1 pixel width. The binary image should contain only one connected component. If more than one connected components are found in the input image, the method will onle calculate the paths of the cc that contains the starting point.
        /// </summary>
        /// <param name="skeleton">The thinned image.</param>
        /// <param name="neighboursArr">The neighbours for each pixel</param>
        /// <param name="startingPoint">The starting point.</param>
        /// <returns></returns>
        public static PathStats FindPaths(Mat skeleton, List<int>[] neighboursArr, Point startingPoint)
        {        
            int width = skeleton.Width;
            int height = skeleton.Height;
          
            // translate to 1-D coordinates
            int currentPixel = startingPoint.Y * width + startingPoint.X;

            // save the traversed positions to avoid recalculating them
            bool[] accumulatedPixelMask = new bool[neighboursArr.Length];
            accumulatedPixelMask[currentPixel] = true;

            Queue<int> queuedPixels = new Queue<int>(); // a pixel que for found but not accumulated pixel positions
            PathStats pathStats = new PathStats();
            pathStats.AddNewPath();
            pathStats.IncreaseLastPathLength();

            bool pathContinues = true;
            while (pathContinues)
            {
                int possibleNext = -1;
                // search among the neighbours of the current pixel
                for (int i = 0; i < neighboursArr[currentPixel].Count; i++)
                {
                    possibleNext = neighboursArr[currentPixel][i];
                    if (accumulatedPixelMask[possibleNext]) { possibleNext = -1; continue; }
                    // at this point possibleNext is the closest or tied to, to the current pixel
                    i++;
                    // save all the remaining neighbours to the que
                    while (i < neighboursArr[currentPixel].Count)
                    {
                        queuedPixels.Enqueue(neighboursArr[currentPixel][i]);
                        //queuedPixels.Add(neighboursArr[currentPixel][i]);
                        i++;
                    }
                }
                // in case the current pixel has no accecible neighbours take one from the que and add a path
                while (possibleNext == -1 && queuedPixels.Count > 0)
                {
                    // search the queue
                    //possibleNext = queuedPixels[0];
                    //queuedPixels.RemoveAt(0);
                    possibleNext = queuedPixels.Dequeue();
                    if (accumulatedPixelMask[possibleNext])
                    {
                        possibleNext = -1;
                        continue;
                    }
                    else
                    {
                        pathStats.AddNewPath();
                    }
                }
                if (possibleNext == -1) 
                {
                    // this if clause is defined for some very rare cases
                    if (pathStats.PathLengths[pathStats.NumberOfPaths - 1] == 0)
                        pathStats.RemoveLastPath(); 
                    pathContinues = false; 
                    break; 
                }
                currentPixel = possibleNext;
                pathStats.IncreaseLastPathLength();
                accumulatedPixelMask[currentPixel] = true;
            }

            return pathStats;
        }

        /// <summary>
        /// Calculates the mean stroke width of the er pixels. Also considers the fact that Stroke Width Transform gives half the width.
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public static float MeanStrokeWidth(IInputArray input)
        {
            Mat result = new Mat();
            StrokeWidthTransform(input, ref result);
            //input.GetInputArray().GetMat().SaveToStringFile("C:\\Users\\George\\Desktop\\SWT\\" + "input.txt");
            //result.SaveToStringFile("C:\\Users\\George\\Desktop\\SWT\\" + "result.txt");
            float mean = 2 * (float)CvInvoke.Mean(result, result).V0;
            result.Dispose();

            return mean;
        }

        /// <summary>
        /// Create the Stroke Width Transform of an image based on the algorithm found at "ROBUST TEXT DETECTION IN NATURAL IMAGES WITH EDGE-ENHANCED MAXIMALLY STABLE EXTREMAL REGIONS" from "Huizhong Chen, Sam S. Tsai, Georg Schroth, David M. Chen, Radek Grzeszczuk and Bernd Girod". It replaces the values of the pixels from the distance transform to half the stroke width.
        /// </summary>
        /// <param name="input"></param>
        /// <param name="result"></param>
        public static void StrokeWidthTransform(IInputArray input, ref Mat result)
        {
            using (Mat src1 = input.GetInputArray().GetMat())
            {
                using (Mat distanceTransform = new Mat())
                {
                    using (Mat src = new Mat())
                    {
                        // Create the distance transform of the input image and use a border for consistency
                        CvInvoke.CopyMakeBorder(src1, src, 5, 5, 5, 5, BorderType.Constant, new MCvScalar(0));
                        //src1.SaveToStringFile("C:\\Users\\George\\Desktop\\SWT\\" + "src1.txt");
                        //src.SaveToStringFile("C:\\Users\\George\\Desktop\\SWT\\" + "src.txt");
                        CvInvoke.DistanceTransform(src, distanceTransform, null, DistType.L2, 3);
                    }
                    distanceTransform.ConvertTo(distanceTransform, DepthType.Cv8U);
                    //distanceTransform.SaveToStringFile("C:\\Users\\George\\Desktop\\SWT\\" + "dist.txt");
                    // Create the local binary pattern of the input image
                    Mat lbpImage = new Mat(distanceTransform.Rows, distanceTransform.Cols, DepthType.Cv8U, 1);
                    LBPImage(distanceTransform, ref lbpImage);

                    // Get the neighbours for the specified pattern
                    List<int>[] neighboursMat = GetNeighbourIndices(lbpImage);
                    lbpImage.Dispose();

                    // Get the maximum height of a peak
                    double minVal = 0d, maxVal = 0d;
                    CvInvoke.MinMaxIdx(distanceTransform, out minVal, out maxVal, null, null);
                    byte maxStroke = (byte)maxVal;

                    // Create the result data storage; here the values of distance transform will be modified to stroke values
                    byte[] data = (byte[])distanceTransform.GetData().Clone();
                    while (maxStroke >= 1)
                    {
                        using (Mat temp = new Mat(distanceTransform.Rows, distanceTransform.Cols, DepthType.Cv8U, 1))
                        {
                            using (Mat loot = new Mat(256, 1, DepthType.Cv8U, 1))
                            {
                                loot.SetTo(ZhangSuenThinning.CreateArbitraryLoot(maxStroke)); // it is used to find the stroke indices
                                CvInvoke.LUT(distanceTransform, loot, temp); // temp contains only the pixels that have the value of maxStroke
                            }
                            List<int> strokeIndices = Utils.FindNonZeroIndices1D(temp).ToList(); // indices have the 1D indices of the non zero pixels of temp
                            List<int> neighbourIndices = new List<int>();

                            foreach (int i in strokeIndices)
                            {
                                neighbourIndices.AddRange(new List<int>(neighboursMat[i]));
                                neighboursMat[i] = new List<int>(); // delete the neighbours that will be accounted to avoid iterating through them again.
                            }

                            for (int i = 0; i < neighbourIndices.Count; i++)
                            {
                                data[neighbourIndices[i]] = data[neighbourIndices[i]] < maxStroke ? maxStroke : data[neighbourIndices[i]];
                                neighbourIndices.AddRange(new List<int>(neighboursMat[neighbourIndices[i]]));
                                neighboursMat[neighbourIndices[i]] = new List<int>();
                            }
                            maxStroke--;
                        }
                    }
                    result = new Mat(distanceTransform.Size, DepthType.Cv8U, 1);
                    result.SetTo(data);
                }
            }
        }

        /// <summary>
        /// Calculates the local binary pattern of an image. i.e. the 8bit value for every non-zero pixel that denotes the pattern of non-zero neighbours with value less (but greater than zero) than that of the pixel in question.
        /// </summary>
        /// <param name="src"></param>
        /// <param name="dst"></param>
        public static void LBPImage(Mat src, ref Mat dst)
        {
            byte[] result = new byte[src.Rows * src.Cols];

            byte localResult = 0;

            byte[] srcData = src.GetData();
            byte[,] src2D = new byte[src.Rows, src.Cols];

            for (int i = 0; i < src.Rows; i++)
            {
                for (int j = 0; j < src.Cols; j++)
                {
                    src2D[i, j] = srcData[i * src.Cols + j];
                }
            }
            // for column = 0 and column = max i.e. the sides
            for (int i = 1; i < src.Rows - 1; i++)
            {
                int j = 0;
                byte center = src2D[i, j];
                localResult = 0;
                localResult = src2D[i - 1, j] != 0 && src2D[i - 1, j] < center ? (byte)(localResult | 1 << 0) : localResult;
                localResult = src2D[i - 1, j + 1] != 0 && src2D[i - 1, j + 1] < center ? (byte)(localResult | 1 << 1) : localResult;
                localResult = src2D[i, j + 1] != 0 && src2D[i, j + 1] < center ? (byte)(localResult | 1 << 2) : localResult;
                localResult = src2D[i + 1, j + 1] != 0 && src2D[i + 1, j + 1] < center ? (byte)(localResult | 1 << 3) : localResult;
                localResult = src2D[i + 1, j] != 0 && src2D[i + 1, j] < center ? (byte)(localResult | 1 << 4) : localResult;
                result[i * src.Cols + j] = localResult;

                j = src.Cols - 1;
                center = src2D[i, j];
                localResult = 0;
                localResult = src2D[i - 1, j] != 0 && src2D[i - 1, j] < center ? (byte)(localResult | 1 << 0) : localResult;
                localResult = src2D[i + 1, j] != 0 && src2D[i + 1, j] < center ? (byte)(localResult | 1 << 4) : localResult;
                localResult = src2D[i + 1, j - 1] != 0 && src2D[i + 1, j - 1] < center ? (byte)(localResult | 1 << 5) : localResult;
                localResult = src2D[i, j - 1] != 0 && src2D[i, j - 1] < center ? (byte)(localResult | 1 << 6) : localResult;
                localResult = src2D[i - 1, j - 1] != 0 && src2D[i - 1, j - 1] < center ? (byte)(localResult | 1 << 7) : localResult;
                result[i * src.Cols + j] = localResult;
            }
            // for row = 0 and row = max i.e. top and bottom
            for (int j = 1; j < src.Cols - 1; j++)
            {
                int i = 0;
                byte center = src2D[i, j];
                localResult = 0;
                localResult = src2D[i, j + 1] != 0 && src2D[i, j + 1] < center ? (byte)(localResult | 1 << 2) : localResult;
                localResult = src2D[i + 1, j + 1] != 0 && src2D[i + 1, j + 1] < center ? (byte)(localResult | 1 << 3) : localResult;
                localResult = src2D[i + 1, j] != 0 && src2D[i + 1, j] < center ? (byte)(localResult | 1 << 4) : localResult;
                localResult = src2D[i + 1, j - 1] != 0 && src2D[i + 1, j - 1] < center ? (byte)(localResult | 1 << 5) : localResult;
                localResult = src2D[i, j - 1] != 0 && src2D[i, j - 1] < center ? (byte)(localResult | 1 << 6) : localResult;
                result[i * src.Cols + j] = localResult;

                i = src.Rows - 1;
                center = src2D[i, j];
                localResult = 0;
                localResult = src2D[i, j - 1] != 0 && src2D[i, j - 1] < center ? (byte)(localResult | 1 << 6) : localResult;
                localResult = src2D[i - 1, j - 1] != 0 && src2D[i - 1, j - 1] < center ? (byte)(localResult | 1 << 7) : localResult;
                localResult = src2D[i - 1, j] != 0 && src2D[i - 1, j] < center ? (byte)(localResult | 1 << 0) : localResult;
                localResult = src2D[i - 1, j + 1] != 0 && src2D[i - 1, j + 1] < center ? (byte)(localResult | 1 << 1) : localResult;
                localResult = src2D[i, j + 1] != 0 && src2D[i, j + 1] < center ? (byte)(localResult | 1 << 2) : localResult;
                result[i * src.Cols + j] = localResult;
            }
            // the 4 corners
            int y = 0, x = 0;
            byte centerXY = src2D[y, x];
            localResult = 0;
            localResult = src2D[y, x + 1] != 0 && src2D[y, x + 1] < centerXY ? (byte)(localResult | 1 << 2) : localResult;
            localResult = src2D[y + 1, x + 1] != 0 && src2D[y + 1, x + 1] < centerXY ? (byte)(localResult | 1 << 3) : localResult;
            localResult = src2D[y + 1, x] != 0 && src2D[y + 1, x] < centerXY ? (byte)(localResult | 1 << 4) : localResult;
            result[y * src.Cols + x] = localResult;

            x = src.Cols - 1;
            centerXY = src2D[y, x];
            localResult = 0;
            localResult = src2D[y + 1, x] != 0 && src2D[y + 1, x] < centerXY ? (byte)(localResult | 1 << 4) : localResult;
            localResult = src2D[y + 1, x - 1] != 0 && src2D[y + 1, x - 1] < centerXY ? (byte)(localResult | 1 << 5) : localResult;
            localResult = src2D[y, x - 1] != 0 && src2D[y, x - 1] < centerXY ? (byte)(localResult | 1 << 6) : localResult;
            result[y * src.Cols + x] = localResult;

            y = src.Rows - 1;
            centerXY = src2D[y, x];
            localResult = 0;
            localResult = src2D[y, x - 1] != 0 && src2D[y, x - 1] < centerXY ? (byte)(localResult | 1 << 6) : localResult;
            localResult = src2D[y - 1, x - 1] != 0 && src2D[y - 1, x - 1] < centerXY ? (byte)(localResult | 1 << 7) : localResult;
            localResult = src2D[y - 1, x] != 0 && src2D[y - 1, x] < centerXY ? (byte)(localResult | 1 << 0) : localResult;
            result[y * src.Cols + x] = localResult;

            x = 0;
            centerXY = src2D[y, x];
            localResult = 0;
            localResult = src2D[y - 1, x] != 0 && src2D[y - 1, x] < centerXY ? (byte)(localResult | 1 << 0) : localResult;
            localResult = src2D[y - 1, x + 1] != 0 && src2D[y - 1, x + 1] < centerXY ? (byte)(localResult | 1 << 1) : localResult;
            localResult = src2D[y, x + 1] != 0 && src2D[y, x + 1] < centerXY ? (byte)(localResult | 1 << 2) : localResult;
            result[y * src.Cols + x] = localResult;

            // the rest of the pixels
            for (int i = 1; i < src.Rows - 1; i++)
            {
                for (int j = 1; j < src.Cols - 1; j++)
                {
                    byte center = src2D[i, j];
                    localResult = 0;
                    localResult = src2D[i - 1, j] != 0 && src2D[i - 1, j] < center ? (byte)(localResult | 1 << 0) : localResult;
                    localResult = src2D[i - 1, j + 1] != 0 && src2D[i - 1, j + 1] < center ? (byte)(localResult | 1 << 1) : localResult;
                    localResult = src2D[i, j + 1] != 0 && src2D[i, j + 1] < center ? (byte)(localResult | 1 << 2) : localResult;
                    localResult = src2D[i + 1, j + 1] != 0 && src2D[i + 1, j + 1] < center ? (byte)(localResult | 1 << 3) : localResult;
                    localResult = src2D[i + 1, j] != 0 && src2D[i + 1, j] < center ? (byte)(localResult | 1 << 4) : localResult;
                    localResult = src2D[i + 1, j - 1] != 0 && src2D[i + 1, j - 1] < center ? (byte)(localResult | 1 << 5) : localResult;
                    localResult = src2D[i, j - 1] != 0 && src2D[i, j - 1] < center ? (byte)(localResult | 1 << 6) : localResult;
                    localResult = src2D[i - 1, j - 1] != 0 && src2D[i - 1, j - 1] < center ? (byte)(localResult | 1 << 7) : localResult;
                    result[i * src.Cols + j] = localResult;
                }
            }
            dst.SetTo(result);
            CvInvoke.BitwiseAnd(dst, dst, dst, src);
        }
    }
}
