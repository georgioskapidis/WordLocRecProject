﻿using Emgu.CV;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WordLocRec.Tools
{
    static class CannyWrapper
    {
        /// <summary>
        /// Apply Canny thresholding at an input one-channel array. Thresholds created automatically based on mean pixel intensity.
        /// </summary>
        /// <param name="image">A one-channel array</param>
        public static void CannyAutoThreshMean(IInputArray image, ref Mat imageAfterCanny)
        {
            int meanPixelIntensity = Utils.CalculateMean(image);
            double thresh1 = Math.Max(0, (1 - 0.5) * meanPixelIntensity);
            double thresh2 = Math.Min(255, (1 + 0.1) * meanPixelIntensity);

            ApplyCanny(image, ref imageAfterCanny, thresh1, thresh2);
        }
        /// <summary>
        /// Apply Canny thresholding at an input one-channel array. Thresholds are created automatically based on median of pixel intensity.
        /// </summary>
        /// <param name="image">A one-channel array</param>
        public static void CannyAutoThreshMedian(IInputArray image, ref Mat imageAfterCanny)
        {
            int medianPixelIntensity = Utils.CalculateMedian(image);
            double thresh1 = Math.Max(0, (1 - 0.5) * medianPixelIntensity);
            double thresh2 = Math.Min(255, (1 + 0.2) * medianPixelIntensity);

            ApplyCanny(image, ref imageAfterCanny, thresh1, thresh2);
        }
        /// <summary>
        /// Apply Canny thresholding at an input one-channel array. Thresholds are derived from user input.
        /// </summary>
        /// <param name="image">A one-channel array</param>
        /// <param name="thresh1">The first threshold</param>
        /// <param name="thresh2">The second threshold</param>
        public static void CannyManualThresh(IInputArray image, ref Mat imageAfterCanny, double thresh1, double thresh2)
        {
            ApplyCanny(image, ref imageAfterCanny, thresh1, thresh2);
        }
        private static void ApplyCanny(IInputArray image, ref Mat imageAfterCanny, double thresh1 = 30, double thresh2 = 100)
        {
            CvInvoke.Canny(image, imageAfterCanny, thresh1, thresh2);
        }
    }
}
