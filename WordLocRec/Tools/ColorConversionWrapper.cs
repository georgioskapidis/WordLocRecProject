﻿using Emgu.CV;
using Emgu.CV.CvEnum;
using Emgu.CV.Structure;
using Emgu.CV.Util;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace WordLocRec.Tools
{
    public class ColorConversionWrapper
    {
        public enum ManualConversions
        {
            MGradient,
            MSobelGradient, // M is for multichannel
            Pii,
            Gradient,
            Bgr2Bgr, // dummy
            Bgr2LabTest
        };
        public static void ColorConversions(IInputArray input, Enum code, ref Mat result)
        {
            if (code.GetType().Equals(typeof(Emgu.CV.CvEnum.ColorConversion)))
            {
                ColorConversionCV(input, (ColorConversion)code, ref result);
            }
            else if (code.GetType().Equals(typeof(ManualConversions)))
            {
                ColorConversionManual(input, (ManualConversions)code, ref result);
            }
            else
            {
                result = input.GetInputArray().GetMat().Clone().GetOutputArray().GetMat();
            }
            GC.Collect();
        }

        /// <summary>
        /// Wrapper method to CvtColor that results in a Mat object, not on the fly conversion.
        /// Supports all the OpenCv Colorspace conversions
        /// </summary>
        /// <param name="input">The input array</param>
        /// <param name="convId">Emgu.CV.CvEnum.ColorConversion object</param>
        /// <returns>The resulting Mat object from the conversion</returns>
        private static void ColorConversionCV(IInputArray input, Emgu.CV.CvEnum.ColorConversion convId, ref Mat result)
        {            
            CvInvoke.CvtColor(input, result, convId);
        }

        /// <summary>
        /// Wrapper for manual conversions. Input should be in BGR
        /// </summary>
        /// <param name="input"></param>
        /// <param name="convId"></param>
        /// <returns></returns>
        private static void ColorConversionManual(IInputArray input, ManualConversions convId, ref Mat result)
        {
            switch (convId)
            {
                case ManualConversions.Bgr2Bgr: result = input.GetInputArray().GetMat().Clone(); break;
                case ManualConversions.Gradient:
                    {
                        Mat gray = new Mat();
                        ColorConversionCV(input, ColorConversion.Bgr2Gray, ref gray);
                        GradientMagnitude(gray, ref result);
                        gray.Dispose();
                        //Bgr2PillCleaner(input, ref result);
                        break;
                    }
                case ManualConversions.Pii: Bgr2PillMonicaCleanest(input, ref result);
                    break;
                case ManualConversions.MGradient: case ManualConversions.MSobelGradient: 
                        MultiChannelGradient(input, convId, ref result); break;
                case ManualConversions.Bgr2LabTest: Bgr2LabDistances(input, ref result); break;
                
            }
            //return result;
        }

        #region MyTransformations
        public static void Bgr2LabDistances(IInputArray input, ref Mat result)
        {
            Mat src = input.GetInputArray().GetMat();
            if (src.NumberOfChannels != 3) return;
            Mat lab = new Mat();
            {
                ColorConversionWrapper.ColorConversionManual(src, ManualConversions.Pii, ref lab);
                
                
                using (VectorOfMat channels = new VectorOfMat())
                {
                    CvInvoke.Split(lab, channels);
                    int numberOfPixels = src.Rows * src.Cols;
                    byte[] ch1 = channels[0].GetData();
                    byte[] ch2 = channels[1].GetData();
                    byte[] ch3 = channels[2].GetData();
                    double[] minVal = new double[3], maxVal = new double[3];
                    Point[] minLoc = new Point[3], maxLoc = new Point[3];
                    CvInvoke.MinMaxLoc(channels[0], ref minVal[0], ref maxVal[0], ref minLoc[0], ref maxLoc[0]);
                    CvInvoke.MinMaxLoc(channels[1], ref minVal[1], ref maxVal[1], ref minLoc[1], ref maxLoc[1]);
                    CvInvoke.MinMaxLoc(channels[2], ref minVal[2], ref maxVal[2], ref minLoc[2], ref maxLoc[2]);
                    MCvScalar zero = new MCvScalar(minVal[0], minVal[1], minVal[2]);
                    float[] res = new float[numberOfPixels];
                    for (int i = 0; i < numberOfPixels; i++)
                    {
                        //res[i] = ERUtils.LabColorDistance(new MCvScalar(ch1[i], ch2[i], ch3[i]), zero);
                        res[i] = ERUtils.EuclidianColorDistance(new MCvScalar(ch1[i], ch2[i], ch3[i]), zero);
                    }
                    using (Mat resultDistances = new Mat(src.Rows, src.Cols, DepthType.Cv32F, 1))
                    {
                        resultDistances.SetTo(res);
                        CvInvoke.Normalize(resultDistances, resultDistances, 0, 255, NormType.MinMax);
                        resultDistances.ConvertTo(result, DepthType.Cv8U);
                    }
                }
            }
            
        }
        
        public static void Bgr2PillClean(IInputArray input, ref Mat result)
        {
            // 3 * 3 pinakas gia tis times toy B
            float[,] dataB = new float[3, 3];
            dataB[0, 0] = 0.9465229f; dataB[0, 1] = 0.2946927f; dataB[0, 2] = -0.1313419f;
            dataB[1, 0] = -0.1179179f; dataB[1, 1] = 0.9929960f; dataB[1, 2] = 0.007371554f;
            dataB[2, 0] = 0.09230461f; dataB[2, 1] = -0.04645794f; dataB[2, 2] = 0.9946464f;
            Matrix<float> B = new Matrix<float>(dataB);

            // 3 * 3 pinakas gia tis times toy A
            float[,] dataA = new float[3, 3];
            dataA[0, 0] = 27.07439f; dataA[0, 1] = -22.80783f; dataA[0, 2] = -1.806681f;
            dataA[1, 0] = -5.646736f; dataA[1, 1] = -7.722125f; dataA[1, 2] = 12.86503f;
            dataA[2, 0] = -4.163133f; dataA[2, 1] = -4.579428f; dataA[2, 2] = -4.576049f;
            Matrix<float> A = new Matrix<float>(dataA);

            // Tha kanw th metatroph se pii ousiastika allazontas tis times toy xyz (opws sto paper)
            Mat bgrInput = input.GetInputArray().GetMat();
            Mat xyz = new Mat();
            CvInvoke.CvtColor(bgrInput, xyz, ColorConversion.Bgr2Xyz);

            bgrInput.Dispose();
            int rows = xyz.Rows; int cols = xyz.Cols; int numOfChannels = xyz.NumberOfChannels;
            // metatrepw to xyz se float giati an to afhsw se byte mhdenizontai oi arnhtikes times
            Matrix<float> temp = new Matrix<float>(rows, cols, numOfChannels);
            xyz.ConvertTo(temp, DepthType.Cv32F);
            xyz.Dispose();

            // reshape gia na kanw toys pollaplasiasmous pinakwn
            Matrix<float> reshaped1;
            Matrix<float> reshaped2;
            Matrix<float> transposed;
            Matrix<float> mulB, mulA;
            Matrix<float> transposedInv;
            Matrix<float> reshapedInv;

            reshaped1 = temp.Reshape(1, rows);
            temp.Dispose();
            reshaped2 = reshaped1.Reshape(1, rows * cols);
            reshaped1.Dispose();
            transposed = reshaped2.Transpose();
            reshaped2.Dispose();

            mulB = B * transposed;
            transposed.Dispose();

            CvInvoke.Log(mulB, mulB);

            mulA = A * mulB;
            mulB.Dispose();

            transposedInv = mulA.Transpose();
            mulA.Dispose();
            reshapedInv = transposedInv.Reshape(numOfChannels, rows);
            transposedInv.Dispose();

            // tha apothikeusw kathe kanali ksexwrista gia normalization sto 0-255
            VectorOfMat pii = new VectorOfMat();
            CvInvoke.Split(reshapedInv, pii);
            reshapedInv.Dispose();
            // normalization kai pisw se byte
            for (int i = 0; i < pii.Size; i++)
            {
                CvInvoke.Normalize(pii[i], pii[i], 255, 0, NormType.MinMax, DepthType.Cv32F, null);
                pii[i].ConvertTo(pii[i], DepthType.Cv8U);
            }

            // merge se ena mat gia eksodo
            //Mat result = new Mat();
            CvInvoke.Merge(pii, result);
            //CvInvoke.CvtColor(result, result, ColorConversion.Xyz2Bgr);
            //CvInvoke.CvtColor(result, result, ColorConversion.Bgr2Gray);
            pii.Dispose();
            //temp.Dispose(); xyz.Dispose(); 
            A.Dispose(); B.Dispose();
            //return result;
            //GC.Collect();
        }
        public static void Bgr2PillCleaner(IInputArray input, ref Mat result)
        {
            // 3 * 3 pinakas gia tis times toy B
            float[,] dataB = new float[3, 3];
            dataB[0, 0] = 0.9465229f; dataB[0, 1] = 0.2946927f; dataB[0, 2] = -0.1313419f;
            dataB[1, 0] = -0.1179179f; dataB[1, 1] = 0.9929960f; dataB[1, 2] = 0.007371554f;
            dataB[2, 0] = 0.09230461f; dataB[2, 1] = -0.04645794f; dataB[2, 2] = 0.9946464f;
            Matrix<float> B = new Matrix<float>(dataB);

            // 3 * 3 pinakas gia tis times toy A
            float[,] dataA = new float[3, 3];
            dataA[0, 0] = 27.07439f; dataA[0, 1] = -22.80783f; dataA[0, 2] = -1.806681f;
            dataA[1, 0] = -5.646736f; dataA[1, 1] = -7.722125f; dataA[1, 2] = 12.86503f;
            dataA[2, 0] = -4.163133f; dataA[2, 1] = -4.579428f; dataA[2, 2] = -4.576049f;
            Matrix<float> A = new Matrix<float>(dataA);

            // Tha kanw th metatroph se pii ousiastika allazontas tis times toy xyz (opws sto paper)
            Mat bgrInput = input.GetInputArray().GetMat();
            Mat xyz = new Mat();
            CvInvoke.CvtColor(bgrInput, xyz, ColorConversion.Bgr2Xyz);

            bgrInput.Dispose();
            int rows = xyz.Rows; int cols = xyz.Cols; int numOfChannels = xyz.NumberOfChannels;
            // metatrepw to xyz se float giati an to afhsw se byte mhdenizontai oi arnhtikes times
            Matrix<float> temp = new Matrix<float>(rows, cols, numOfChannels);
            xyz.ConvertTo(temp, DepthType.Cv32F);
            xyz.Dispose();

            // reshape gia na kanw toys pollaplasiasmous pinakwn
            Matrix<float> reshaped1;
            Matrix<float> reshaped2;
            Matrix<float> transposed;
            Matrix<float> mulB, mulA;
            Matrix<float> transposedInv;
            Matrix<float> reshapedInv;

            reshaped1 = temp.Reshape(1, rows);
            temp.Dispose();
            reshaped2 = reshaped1.Reshape(1, rows * cols);
            reshaped1.Dispose();
            transposed = reshaped2.Transpose();
            reshaped2.Dispose();
            GC.Collect();
            mulB = B * transposed;
            transposed.Dispose();
            GC.Collect();
            CvInvoke.Log(mulB, mulB);
            mulA = A * mulB;
            mulB.Dispose();
            GC.Collect();
            transposedInv = mulA.Transpose();
            mulA.Dispose();
            GC.Collect();
            reshapedInv = transposedInv.Reshape(numOfChannels, rows);
            transposedInv.Dispose();

            // tha apothikeusw kathe kanali ksexwrista gia normalization sto 0-255
            VectorOfMat pii = new VectorOfMat();
            CvInvoke.Split(reshapedInv, pii);
            reshapedInv.Dispose();
            // normalization kai pisw se byte
            for (int i = 0; i < pii.Size; i++)
            {
                CvInvoke.Normalize(pii[i], pii[i], 255, 0, NormType.MinMax, DepthType.Cv32F, null);
                pii[i].ConvertTo(pii[i], DepthType.Cv8U);
            }
            CvInvoke.Merge(pii, result);
            pii.Dispose();
            A.Dispose(); B.Dispose();
        }
        
        // wrong output because of gemm.
        public static void Bgr2PillMonicaCleaner(IInputArray input, ref Mat result)
        {
            // 3 * 3 pinakas gia tis times toy B
            float[,] dataB = new float[3, 3];
            dataB[0, 0] = 0.9465229f; dataB[0, 1] = 0.2946927f; dataB[0, 2] = -0.1313419f;
            dataB[1, 0] = -0.1179179f; dataB[1, 1] = 0.9929960f; dataB[1, 2] = 0.007371554f;
            dataB[2, 0] = 0.09230461f; dataB[2, 1] = -0.04645794f; dataB[2, 2] = 0.9946464f;
            Matrix<float> B = new Matrix<float>(dataB);

            // 3 * 3 pinakas gia tis times toy A
            float[,] dataA = new float[3, 3];
            dataA[0, 0] = 27.07439f; dataA[0, 1] = -22.80783f; dataA[0, 2] = -1.806681f;
            dataA[1, 0] = -5.646736f; dataA[1, 1] = -7.722125f; dataA[1, 2] = 12.86503f;
            dataA[2, 0] = -4.163133f; dataA[2, 1] = -4.579428f; dataA[2, 2] = -4.576049f;
            Matrix<float> A = new Matrix<float>(dataA);

            // Tha kanw th metatroph se pii ousiastika allazontas tis times toy xyz (opws sto paper)
            Mat bgrInput = input.GetInputArray().GetMat();
            Mat xyz = new Mat();
            CvInvoke.CvtColor(bgrInput, xyz, ColorConversion.Bgr2Xyz);

            bgrInput.Dispose();
            int rows = xyz.Rows; int cols = xyz.Cols; int numOfChannels = xyz.NumberOfChannels;
            // metatrepw to xyz se float giati an to afhsw se byte mhdenizontai oi arnhtikes times
            Matrix<float> temp = new Matrix<float>(rows, cols, numOfChannels);
            xyz.ConvertTo(temp, DepthType.Cv32F);
            xyz.Dispose();

            temp = temp.Reshape(1, rows);
            temp = temp.Reshape(1, rows * cols);
            CvInvoke.Transpose(temp, temp);
            
            CvInvoke.Gemm(B, temp, 1d, null, 0d, temp, GemmType.Default);
            CvInvoke.Log(temp, temp);
            CvInvoke.Gemm(A, temp, 1d, null, 0d, temp, GemmType.Default);

            CvInvoke.Transpose(temp, temp);

            temp = temp.Reshape(numOfChannels, rows);

            // tha apothikeusw kathe kanali ksexwrista gia normalization sto 0-255
            VectorOfMat pii = new VectorOfMat();
            CvInvoke.Split(temp, pii);
            temp.Dispose();
            // normalization kai pisw se byte
            for (int i = 0; i < pii.Size; i++)
            {
                CvInvoke.Normalize(pii[i], pii[i], 255, 0, NormType.MinMax, DepthType.Cv32F, null);
                pii[i].ConvertTo(pii[i], DepthType.Cv8U);
            }
            CvInvoke.Merge(pii, result);
            pii.Dispose();
            A.Dispose(); B.Dispose();
        }
        public static void Bgr2PillMonicaCleanest(IInputArray input, ref Mat result)
        {
            // 3 * 3 pinakas gia tis times toy B
            float[,] dataB = new float[3, 3];
            dataB[0, 0] = 0.9465229f; dataB[0, 1] = 0.2946927f; dataB[0, 2] = -0.1313419f;
            dataB[1, 0] = -0.1179179f; dataB[1, 1] = 0.9929960f; dataB[1, 2] = 0.007371554f;
            dataB[2, 0] = 0.09230461f; dataB[2, 1] = -0.04645794f; dataB[2, 2] = 0.9946464f;
            Matrix<float> B = new Matrix<float>(dataB);

            // 3 * 3 pinakas gia tis times toy A
            float[,] dataA = new float[3, 3];
            dataA[0, 0] = 27.07439f; dataA[0, 1] = -22.80783f; dataA[0, 2] = -1.806681f;
            dataA[1, 0] = -5.646736f; dataA[1, 1] = -7.722125f; dataA[1, 2] = 12.86503f;
            dataA[2, 0] = -4.163133f; dataA[2, 1] = -4.579428f; dataA[2, 2] = -4.576049f;
            Matrix<float> A = new Matrix<float>(dataA);

            // tha apothikeusw kathe kanali ksexwrista gia normalization sto 0-255
            using (VectorOfMat pii = new VectorOfMat())
            {
                // Tha kanw th metatroph se pii ousiastika allazontas tis times toy xyz (opws sto paper)
                Mat bgrInput = input.GetInputArray().GetMat();
                int rows = bgrInput.Rows; int cols = bgrInput.Cols; int numOfChannels = bgrInput.NumberOfChannels;
                using (Matrix<float> temp = new Matrix<float>(rows, cols, numOfChannels))
                {
                    using (Mat xyz = new Mat())
                    {
                        CvInvoke.CvtColor(bgrInput, xyz, ColorConversion.Bgr2Xyz);
                        bgrInput.Dispose();
                        xyz.ConvertTo(temp, DepthType.Cv32F); // metatrepw to xyz se float giati an to afhsw se byte mhdenizontai oi arnhtikes times
                    }
                    using (Matrix<float> res1 = temp.Reshape(1, rows * cols))
                    {
                        //temp.Dispose();
                        using (Mat tt = new Mat())
                        {
                            CvInvoke.Transpose(res1, tt);

                            CvInvoke.Gemm(B, tt, 1d, null, 0d, tt, GemmType.Default);
                            CvInvoke.Log(tt, tt);
                            CvInvoke.Gemm(A, tt, 1d, null, 0d, tt, GemmType.Default);

                            CvInvoke.Transpose(tt, res1);
                        }
                        using (Matrix<float> res2 = res1.Reshape(numOfChannels, rows))
                            CvInvoke.Split(res2, pii);
                    }
                }
                // normalization kai pisw se byte
                for (int i = 0; i < pii.Size; i++)
                {
                    CvInvoke.Normalize(pii[i], pii[i], 255, 0, NormType.MinMax, DepthType.Cv32F, null);
                    pii[i].ConvertTo(pii[i], DepthType.Cv8U);
                }
                CvInvoke.Merge(pii, result);

                A.Dispose(); B.Dispose();
            }
            GC.Collect();
        }

        private void Bgr2PillDirty(IInputArray input, ref Mat result)
        {
            // 3 * 3 pinakas gia tis times toy B
            float[,] dataB = new float[3, 3];
            dataB[0, 0] = 0.9465229f; dataB[0, 1] = 0.2946927f; dataB[0, 2] = -0.1313419f;
            dataB[1, 0] = -0.1179179f; dataB[1, 1] = 0.9929960f; dataB[1, 2] = 0.007371554f;
            dataB[2, 0] = 0.09230461f; dataB[2, 1] = -0.04645794f; dataB[2, 2] = 0.9946464f;
            Matrix<float> B = new Matrix<float>(dataB);

            // 3 * 3 pinakas gia tis times toy A
            float[,] dataA = new float[3, 3];
            dataA[0, 0] = 27.07439f; dataA[0, 1] = -22.80783f; dataA[0, 2] = -1.806681f;
            dataA[1, 0] = -5.646736f; dataA[1, 1] = -7.722125f; dataA[1, 2] = 12.86503f;
            dataA[2, 0] = -4.163133f; dataA[2, 1] = -4.579428f; dataA[2, 2] = -4.576049f;
            Matrix<float> A = new Matrix<float>(dataA);
            
            // Tha kanw th metatroph se pii ousiastika allazontas tis times toy xyz (opws sto paper)
            Mat bgrInput = input.GetInputArray().GetMat();
            Mat xyz = new Mat();
            ColorConversionCV(bgrInput, ColorConversion.Bgr2Xyz, ref xyz);
            bgrInput.Dispose();
            int rows = xyz.Rows; int cols = xyz.Cols; int numOfChannels = xyz.NumberOfChannels;
            // metatrepw to xyz se float giati an to afhsw se byte mhdenizontai oi arnhtikes times
            Matrix<float> temp = new Matrix<float>(rows, cols, numOfChannels);
            xyz.ConvertTo(temp, DepthType.Cv32F);
            xyz.Dispose();

            // reshape gia na kanw toys pollaplasiasmous pinakwn
            temp = temp.Reshape(1, rows);
            temp = temp.Reshape(1, rows * cols).Transpose();
            temp = B * temp;
            CvInvoke.Log(temp, temp);
            temp = A * temp;
            temp = temp.Transpose().Reshape(numOfChannels, rows);
            
            // tha apothikeusw kathe kanali ksexwrista gia normalization sto 0-255
            VectorOfMat pii = new VectorOfMat();
            CvInvoke.Split(temp, pii);
            temp.Dispose();
            // normalization kai pisw se byte
            for (int i = 0; i < pii.Size; i++)
            {
                CvInvoke.Normalize(pii[i], pii[i], 255, 0, NormType.MinMax, DepthType.Cv32F, null);
                pii[i].ConvertTo(pii[i], DepthType.Cv8U);
            }

            // merge se ena mat gia eksodo
            //Mat result = new Mat();
            CvInvoke.Merge(pii, result);

            pii.Dispose(); 
            //temp.Dispose(); xyz.Dispose(); 
            A.Dispose(); B.Dispose();
            //return result;
        }
        public static Mat XYZToIll(Mat xyzMat)
        {
            float[,] dataB = new float[3, 3];
            dataB[0, 0] = 0.9465229f; dataB[0, 1] = 0.2946927f; dataB[0, 2] = -0.1313419f;
            dataB[1, 0] = -0.1179179f; dataB[1, 1] = 0.9929960f; dataB[1, 2] = 0.007371554f;
            dataB[2, 0] = 0.09230461f; dataB[2, 1] = -0.04645794f; dataB[2, 2] = 0.9946464f;
            Matrix<float> B = new Matrix<float>(dataB);

            float[,] dataA = new float[3, 3];
            dataA[0, 0] = 27.07439f; dataA[0, 1] = -22.80783f; dataA[0, 2] = -1.806681f;
            dataA[1, 0] = -5.646736f; dataA[1, 1] = -7.722125f; dataA[1, 2] = 12.86503f;
            dataA[2, 0] = -4.163133f; dataA[2, 1] = -4.579428f; dataA[2, 2] = -4.576049f;
            Matrix<float> A = new Matrix<float>(dataA);

            Matrix<float> ill = new Matrix<float>(xyzMat.Rows, xyzMat.Cols, xyzMat.NumberOfChannels);
            xyzMat.ConvertTo(ill, DepthType.Cv32F);

            ill = ill.Reshape(1, xyzMat.Rows * xyzMat.Cols);
            ill = ill.Transpose();

            ill = B * ill;
            CvInvoke.Log(ill, ill);
            ill = A * ill;

            ill = ill.Transpose();
            ill = ill.Reshape(3, xyzMat.Rows);

            Mat illMat = new Mat(xyzMat.Size, DepthType.Cv8U, xyzMat.NumberOfChannels);
            ill.Mat.ConvertTo(illMat, DepthType.Cv8U);

            return ill.Mat;
        }

        #region Gradient Magnitude

        private static void MultiChannelGradient(IInputArray input, ManualConversions mGradType, ref Mat result)
        {
            // % Get src and split in channels % //
            Mat src = input.GetInputArray().GetMat();

            //CvInvoke.CvtColor(src, src, ColorConversion.Bgr2Lab);
            //Bgr2PillClean(src, ref src);
            //GC.Collect();
            if (src.NumberOfChannels != 3)
            {
                return;
            }
            Size size = src.Size;
            int rows = src.Rows;
            int cols = src.Cols;

            VectorOfMat channels = new VectorOfMat();
            CvInvoke.Split(src, channels);
            src.Dispose();
            // % END % //

            // % Create X, Y gradients for the 3 channels according to user input % //
            Mat kernelHor, kernelVer;
            MakeXYKernels(mGradType, out kernelHor, out kernelVer);

            Mat Ch1X = new Mat(size, DepthType.Cv32F, 1), Ch1Y = new Mat(size, DepthType.Cv32F, 1),
                Ch2X = new Mat(size, DepthType.Cv32F, 1), Ch2Y = new Mat(size, DepthType.Cv32F, 1),
                Ch3X = new Mat(size, DepthType.Cv32F, 1), Ch3Y = new Mat(size, DepthType.Cv32F, 1);

            Point p = new Point(-1, -1);
            CvInvoke.Filter2D(channels[0], Ch1X, kernelHor, p);
            CvInvoke.Filter2D(channels[0], Ch1Y, kernelVer, p);

            CvInvoke.Filter2D(channels[1], Ch2X, kernelHor, p);
            CvInvoke.Filter2D(channels[1], Ch2Y, kernelVer, p);

            CvInvoke.Filter2D(channels[2], Ch3X, kernelHor, p);
            CvInvoke.Filter2D(channels[2], Ch3Y, kernelVer, p);
            channels.Dispose();
            // % END % //

            // % Calculate gradient squares and x*y inter products % //
            Mat ch1xSq = new Mat(size, DepthType.Cv32F, 1)
               , ch1ySq = new Mat(size, DepthType.Cv32F, 1)
               , ch1xy = new Mat(size, DepthType.Cv32F, 1);
            CvInvoke.Multiply(Ch1X, Ch1X, ch1xSq);
            CvInvoke.Multiply(Ch1Y, Ch1Y, ch1ySq);
            CvInvoke.Multiply(Ch1X, Ch1Y, ch1xy);
            Ch1X.Dispose();
            Ch1Y.Dispose();

            Mat ch2xSq = new Mat(size, DepthType.Cv32F, 1)
               , ch2ySq = new Mat(size, DepthType.Cv32F, 1)
               , ch2xy = new Mat(size, DepthType.Cv32F, 1);
            CvInvoke.Multiply(Ch2X, Ch2X, ch2xSq);
            CvInvoke.Multiply(Ch2Y, Ch2Y, ch2ySq);
            CvInvoke.Multiply(Ch2X, Ch2Y, ch2xy);
            Ch2X.Dispose();
            Ch2Y.Dispose();

            Mat ch3xSq = new Mat(size, DepthType.Cv32F, 1)
               , ch3ySq = new Mat(size, DepthType.Cv32F, 1)
               , ch3xy = new Mat(size, DepthType.Cv32F, 1);
            CvInvoke.Multiply(Ch3X, Ch3X, ch3xSq);
            CvInvoke.Multiply(Ch3Y, Ch3Y, ch3ySq);
            CvInvoke.Multiply(Ch3X, Ch3Y, ch3xy);
            Ch3X.Dispose();
            Ch3Y.Dispose();
            // % END % //

            // % Calculate G tensors % //
            Mat Gxx = new Mat(size, DepthType.Cv32F, 1)
              , Gyy = new Mat(size, DepthType.Cv32F, 1)
              , Gxy = new Mat(size, DepthType.Cv32F, 1);

            CvInvoke.Add(ch1xSq, ch2xSq, Gxx);
            CvInvoke.Add(Gxx, ch3xSq, Gxx);
            ch1xSq.Dispose();
            ch2xSq.Dispose();
            ch3xSq.Dispose();

            CvInvoke.Add(ch1ySq, ch2ySq, Gyy);
            CvInvoke.Add(Gyy, ch3ySq, Gyy);
            ch1ySq.Dispose();
            ch2ySq.Dispose();
            ch3ySq.Dispose();

            CvInvoke.Add(ch1xy, ch2xy, Gxy);
            CvInvoke.Add(Gxy, ch3xy, Gxy);
            ch1xy.Dispose();
            ch2xy.Dispose();
            ch3xy.Dispose();
            // % END % //

            // % Modify Tensor values for F and theta calculations % //
            Mat two = new Mat(size, DepthType.Cv32F, 1);
                two.SetTo(new MCvScalar(2));
            Mat ninety = new Mat(size, DepthType.Cv32F, 1);
                ninety.SetTo(new MCvScalar(90));
            Mat Gxy2 = new Mat(size, DepthType.Cv32F, 1)
               ,Gxx_Gyy = new Mat(size,DepthType.Cv32F, 1)
               ,GxxAndGyy = new Mat(size, DepthType.Cv32F, 1);

            CvInvoke.Multiply(Gxy, two, Gxy2);
            CvInvoke.Subtract(Gxx, Gyy, Gxx_Gyy);
            CvInvoke.Add(Gxx, Gyy, GxxAndGyy);

            Gxx.Dispose();
            Gyy.Dispose();
            Gxy.Dispose();
            // % END % //

            // % Calculate theta and theta +- pi/2 using thresholding and masks! % //
            Mat magnitude = new Mat(size, DepthType.Cv32F, 1);
            Matrix<float> theta = new Matrix<float>(rows, cols, 1)
            , doubleTheta = new Matrix<float>(rows, cols, 1)
            , thetapi2 = new Matrix<float>(rows, cols, 1)
            , doubleThetapi2 = new Matrix<float>(rows, cols, 1);

            CvInvoke.CartToPolar(Gxy2, Gxx_Gyy, magnitude, doubleTheta, true);
            magnitude.Dispose();

            CvInvoke.Divide(doubleTheta, two, theta);

            Mat thetaMaskPlus = new Mat(size, DepthType.Cv8U, 1) // periexei '255' stis perioxes tou pinaka theta opoy theta < 90
              , thetaMaskMinus = new Mat(size, DepthType.Cv8U, 1); // periexei '255' stis perioxes toy pinaka theta opoy theta >= 90

            CvInvoke.Threshold(theta, thetaMaskPlus, 90, 255, ThresholdType.BinaryInv); // if theta < 90 add pi / 2
            CvInvoke.Threshold(theta, thetaMaskMinus, 90.00001, 255, ThresholdType.Binary); // if theta >= 90 subtract pi / 2

            thetaMaskPlus.ConvertTo(thetaMaskPlus, DepthType.Cv8U);
            thetaMaskMinus.ConvertTo(thetaMaskMinus, DepthType.Cv8U);

            CvInvoke.Add(theta, ninety, thetapi2, thetaMaskPlus);
            theta.Dispose();
            CvInvoke.Subtract(thetapi2, ninety, thetapi2, thetaMaskMinus);
            ninety.Dispose();
            thetaMaskPlus.Dispose();
            thetaMaskMinus.Dispose();

            CvInvoke.Multiply(thetapi2, two, doubleThetapi2);
            thetapi2.Dispose();
            // % END % //

            // % Calculate the parts of F(theta) and F(theta +- pi / 2) % //
            Matrix<float> cos2theta = new Matrix<float>(rows, cols, 1)
                        , sin2theta = new Matrix<float>(rows, cols, 1)
                        , cos2theta2 = new Matrix<float>(rows, cols, 1)
                        , sin2theta2 = new Matrix<float>(rows, cols, 1);

            for (int i = 0; i < rows; i++)
            {
                for (int j = 0; j < cols; j++)
                {
                    cos2theta[i, j] = (float)Math.Cos(doubleTheta[i, j]);
                    sin2theta[i, j] = (float)Math.Sin(doubleTheta[i, j]);
                    cos2theta2[i, j] = (float)Math.Cos(doubleThetapi2[i, j]);
                    sin2theta2[i, j] = (float)Math.Sin(doubleThetapi2[i, j]);
                }
            }
            doubleTheta.Dispose();
            doubleThetapi2.Dispose();
            // % END % //

            // % Calculate F(theta) and F(theta +- pi / 2) % //
            // F(theta) = 1/2 * ((Gxx + Gyy) + (Gxx - Gyy) * cos(2θ) + 2Gxy * sin(2θ)
            Matrix<float> F1 = new Matrix<float>(rows, cols, 1)
                        , F2 = new Matrix<float>(rows, cols, 1)
                        , tempf1 = new Matrix<float>(rows, cols, 1)
                        , tempf2 = new Matrix<float>(rows, cols, 1)
                        , tempf3 = new Matrix<float>(rows, cols, 1);

            CvInvoke.Multiply(Gxx_Gyy, cos2theta, tempf1);
            cos2theta.Dispose();
            CvInvoke.Multiply(Gxy2, sin2theta, tempf2);
            sin2theta.Dispose();
            CvInvoke.Add(tempf1, tempf2, tempf3);
            CvInvoke.Add(tempf3, GxxAndGyy, tempf3);
            CvInvoke.Divide(tempf3, two, F1);

            CvInvoke.Multiply(Gxx_Gyy, cos2theta2, tempf1);
            Gxx_Gyy.Dispose();
            cos2theta2.Dispose();
            CvInvoke.Multiply(Gxy2, sin2theta2, tempf2);
            Gxy2.Dispose();
            sin2theta2.Dispose();
            CvInvoke.Add(tempf1, tempf2, tempf3);
            CvInvoke.Add(tempf3, GxxAndGyy, tempf3);
            GxxAndGyy.Dispose();
            CvInvoke.Divide(tempf3, two, F2);
            two.Dispose();
            tempf1.Dispose();
            tempf2.Dispose();
            tempf3.Dispose();
            // % END % //

            // % Get the Square Root of F1 and F2 % //
            CvInvoke.Sqrt(F1, F1);
            CvInvoke.Sqrt(F2, F2);
            // % END % //

            // % Get the maximum of F1 and F2 for each pixel % //
            Mat FMax = new Mat(size, DepthType.Cv32F, 1);
            CvInvoke.Max(F1, F2, FMax);
            F1.Dispose();
            F2.Dispose();
            // % END % //

            // % Relaxed MinMax norm and exit % //
            RangeF range = FMax.GetValueRange();
            double fMax = range.Max > 255 ? 255 : range.Max;
            double fMin = range.Max > 255 ? range.Max - range.Min < 255 ? range.Max - 255 : 0 : range.Min;
            fMin = fMin < 0 ? 0 : fMin;

            CvInvoke.Normalize(FMax, FMax, fMax, fMin, NormType.MinMax);

            FMax.ConvertTo(result, DepthType.Cv8U);
            FMax.Dispose();
            // % END % //
        }


        private static void MakeXYKernels(ManualConversions mGradType, out Mat kernelHor, out Mat kernelVer)
        {
            switch (mGradType)
            {
                case ManualConversions.MGradient: CreateXYGrads(out kernelHor, out kernelVer); break;
                case ManualConversions.MSobelGradient: CreateSobelGrads(out kernelHor, out kernelVer); break;
                default: kernelHor = new Mat(); kernelVer = new Mat(); break;
            }
        }
        private static void CreateXYGrads(out Mat kernelHor, out Mat kernelVer)
        {
            float[] kernelDataHor = new float[3] { -1, 0, 1 };
            float[,] kernelDataVer = new float[3, 1];

            kernelDataVer[0, 0] = -1; kernelDataVer[1, 0] = 0; kernelDataVer[2, 0] = 1;
            GCHandle handleHor = GCHandle.Alloc(kernelDataHor, GCHandleType.Pinned);
            GCHandle handleVer = GCHandle.Alloc(kernelDataVer, GCHandleType.Pinned);

            kernelHor = new Mat(1, 3, DepthType.Cv32F, 1, handleHor.AddrOfPinnedObject(), 12);
            kernelVer = new Mat(3, 1, DepthType.Cv32F, 1, handleVer.AddrOfPinnedObject(), 4);
        }
        private static void CreateSobelGrads(out Mat kernelHor, out Mat kernelVer)
        {
            float[,] kernelDataHor = new float[3, 3];
            float[,] kernelDataVer = new float[3, 3];
            kernelDataHor[0, 0] = -1; kernelDataHor[0, 1] = 0; kernelDataHor[0, 2] = 1;
            kernelDataHor[1, 0] = -2; kernelDataHor[1, 1] = 0; kernelDataHor[1, 2] = 2;
            kernelDataHor[2, 0] = -1; kernelDataHor[2, 1] = 0; kernelDataHor[2, 2] = 1;

            kernelDataVer[0, 0] = -1; kernelDataVer[0, 1] = -2; kernelDataVer[0, 2] = -1;
            kernelDataVer[1, 0] = 0; kernelDataVer[1, 1] = 0; kernelDataVer[1, 2] = 0;
            kernelDataVer[2, 0] = 1; kernelDataVer[2, 1] = 2; kernelDataVer[2, 2] = 1;

            GCHandle handleHor = GCHandle.Alloc(kernelDataHor, GCHandleType.Pinned);
            GCHandle handleVer = GCHandle.Alloc(kernelDataVer, GCHandleType.Pinned);

            kernelHor = new Mat(3, 3, DepthType.Cv32F, 1, handleHor.AddrOfPinnedObject(), 12);
            kernelVer = new Mat(3, 3, DepthType.Cv32F, 1, handleVer.AddrOfPinnedObject(), 12);
        }



        //calculates gradient from Moore neighborhood (8 pixels)
        private static Image<Gray, Byte> TurnToGradientSpace(Image<Gray, Byte> image)
        {
            Image<Gray, Byte> tempIm = new Image<Gray, byte>(image.Size);

            Byte[, ,] original = image.Data;
            Byte[, ,] temp = tempIm.Data;

            for (int i = 0; i < image.Rows; i++)
            {
                for (int j = 0; j < image.Cols; j++)
                {
                    int minNumI = -1;
                    int maxNumI = 1;
                    int minNumJ = -1;
                    int maxNumJ = 1;
                    if (i == 0) minNumI = 0;
                    if (i == image.Height - 1)
                        maxNumI = 0;
                    if (j == 0) minNumJ = 0;
                    if (j == image.Width - 1) maxNumJ = 0;
                    double maxDifference = 0;
                    for (int k = minNumI; k <= maxNumI; k++)
                    {
                        for (int l = minNumJ; l <= maxNumJ; l++)
                        {
                            if (k == 0 && l == 0) continue;
                            int num = Math.Abs(original[i, j, 0] - original[i + k, j + l, 0]);
                            if (num > maxDifference)
                            {
                                maxDifference = num;
                            }
                        }
                    }
                    temp[i, j, 0] = (byte)maxDifference;
                }
            }
            tempIm.Data = temp;
            return tempIm;
        }
        //calculates Gradient channel according to Von Neumann neighborhood (4 pixels)
        private static Image<Gray, Byte> TurnToGradientSpace2(Image<Gray, Byte> image)
        {
            Image<Gray, Byte> tempIm = new Image<Gray, byte>(image.Size);
            Byte[, ,] original = image.Data;
            Byte[, ,] temp = tempIm.Data;

            for (int i = 0; i < image.Rows; i++)
            {
                for (int j = 0; j < image.Cols; j++)
                {
                    int minNumI = -1;
                    int maxNumI = 1;
                    int minNumJ = -1;
                    int maxNumJ = 1;
                    if (i == 0) minNumI = 0;
                    if (i == image.Height - 1) maxNumI = 0;
                    if (j == 0) minNumJ = 0;
                    if (j == image.Width - 1) maxNumJ = 0;
                    double maxDifference = 0;
                    for (int k = minNumI; k <= maxNumI; k++)
                    {
                        for (int l = minNumJ; l <= maxNumJ; l++)
                        {
                            if ((k == 0 && l == 0) || (k == -1 && l == -1) || (k == -1 && l == 1) || (k == 1 && l == -1) || (k == 1 && l == 1)) continue;
                            int num = Math.Abs(original[i, j, 0] - original[i + k, j + l, 0]);
                            if (num > maxDifference)
                            {
                                maxDifference = num;
                            }
                        }
                    }
                    temp[i, j, 0] = (byte)maxDifference;
                }
            }
            tempIm.Data = temp;
            return tempIm;
        }
        private static void GradientMagnitude(IInputArray grayImag, ref Mat result)
        {
            Mat image = grayImag.GetInputArray().GetMat();

            float[] kernelDataHor = new float[3] { -1, 0, 1 };
            float[,] kernelDataVer = new float[3, 1];
            kernelDataVer[0, 0] = -1; kernelDataVer[1, 0] = 0; kernelDataVer[2, 0] = 1;

            GCHandle handleHor = GCHandle.Alloc(kernelDataHor, GCHandleType.Pinned);
            GCHandle handleVer = GCHandle.Alloc(kernelDataVer, GCHandleType.Pinned);

            Mat kernelHor = new Mat(1, 3, DepthType.Cv32F, 1, handleHor.AddrOfPinnedObject(), 12);
            Mat kernelVer = new Mat(3, 1, DepthType.Cv32F, 1, handleVer.AddrOfPinnedObject(), 4);

            Mat gradX = new Mat(image.Size, DepthType.Cv32F, 1), gradY = new Mat(image.Size, DepthType.Cv32F, 1);

            CvInvoke.Filter2D(image, gradX, kernelHor, new Point(-1, -1));
            CvInvoke.Filter2D(image, gradY, kernelVer, new Point(-1, -1));

            CalcMagnitudeCV(gradX, gradY, ref result, true);
            gradX.Dispose();
            gradY.Dispose();
            image.Dispose();
        }
        private static Mat CalcMagnitude(Mat img1, Mat img2)
        {
            // assume img1 and img2 have same size            

            byte[,] magn = new byte[img1.Rows, img1.Cols];
            GCHandle handle = GCHandle.Alloc(magn, GCHandleType.Pinned);
            Mat result = new Mat(img1.Rows, img1.Cols, DepthType.Cv8U, 1, handle.AddrOfPinnedObject(), img1.Width);

            Image<Gray, float> im1 = img1.ToImage<Gray, float>();
            Image<Gray, float> im2 = img2.ToImage<Gray, float>();

            for (int y = 0; y < img1.Height; y++)
            {
                for (int x = 0; x < img1.Width; x++)
                {
                    magn[y, x] = (byte)Math.Sqrt(im1.Data[y, x, 0] * im1.Data[y, x, 0] + im2.Data[y, x, 0] * im2.Data[y, x, 0]);
                }
            }
            im1.Dispose(); im2.Dispose();
            return result;
        }
        private static void CalcMagnitudeCV(Mat gradX, Mat gradY, ref Mat magnitude, bool normType)
        {
            Mat floatMagnitude = new Mat();
            Mat phase = new Mat();
            CvInvoke.CartToPolar(gradX, gradY, floatMagnitude, phase);
            phase.Dispose();
            double[] min, max;
            Point[] minloc, maxloc;
            floatMagnitude.MinMax(out min, out max, out minloc, out maxloc);

            double fMin, fMax;
            if (normType)
            {
                fMax = max[0] > 255 ? 255 : max[0];
                fMin = max[0] > 255 ? max[0] - min[0] < 255 ? max[0] - 255 : 0 : min[0];
                fMin = fMin < 0 ? 0 : fMin;
            }
            else
            {
                fMin = 0;
                fMax = 255;
            }
            CvInvoke.Normalize(floatMagnitude, magnitude, fMax, fMin, NormType.MinMax, DepthType.Cv8U);
            floatMagnitude.Dispose();
        }
        #endregion

        #endregion
    }
}
