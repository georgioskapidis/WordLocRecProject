﻿using Emgu.CV;
using Emgu.CV.CvEnum;
using Emgu.CV.ML;
using Emgu.CV.Structure;
using Emgu.CV.Util;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WordLocRec.DataStructs;
using WordLocRec.SVMStuff;

namespace WordLocRec.Tools
{
    public class Cropper
    {
        public bool groundTruthNeeded;

        ERExtractor.ExtractorParameters ep;
        //filter variables
        int thresholdDelta = 1;
        List<ERStat> regions;
        int width, height;

        private String imageFullPath;
        private String currentChannel; //e.g. Pii0F, BgrGrayscaleB, HSVHueB etc. model = [colorspace][channelID][Forward-Backward]
        //private bool typeOfExample; // positive or negative
        private String outputXmlPath;
        Rectangle[] groundTruth;
        public Cropper(String imagename, Rectangle[] groundTruth, String outputXmlPath, bool gtNeeded, ERExtractor.ExtractorParameters ep)
        {
            // na dw ti allages exoun ginei sto merge apo thn prosthiki toy er mask calculation prin treksw oti xreiazetai
            this.imageFullPath = imagename;
            this.groundTruth = groundTruth; 
            this.outputXmlPath = outputXmlPath;
            groundTruthNeeded = gtNeeded;
            this.ep = ep;
        }
        /// <summary>
        /// extract regions from the input image. if typeOfExample == true then it accepts ers from the regions defined by the gt rectangles. 
        /// <para />
        /// if typeOfExample == false it accepts ers from regions that are not included in the ground truth rectangles
        /// <para /> channelId is the channel identifier from which the er is first found. It is used for retrieval.
        /// </summary>
        /// <param name="inputImage"></param>
        /// <param name="typeOfExample"></param>
        /// <param name="channelId"></param>
        public void RunCropper(IInputArray inputImage, bool typeOfExample, String channelId, bool oppositeChannel)
        {
            if (groundTruthNeeded && groundTruth.Length == 0)
            {
                throw new Exception("no gt for the image");
            }
            this.currentChannel = channelId; // save the channel name
            
            try
            {
                regions = erExtract(inputImage, typeOfExample); // extract whatever ers
            }
            catch (OutOfMemoryException oome)
            {
                Debug.WriteLine(imageFullPath + " " + channelId);
                Debug.WriteLine(oome);
                regions = new List<ERStat>();
                return;
            }
            // Cer pruning
            CerPruning();

            // nd suppression
            NearDuplicateSuppression(false);

            Mat mGray = inputImage.GetInputArray().GetMat();
            List<ERStat> chosen = new List<ERStat>();
            
            VectorOfMat erMats = new VectorOfMat(); // get the segmented regions for later viewing
            Utils.MultipleMatsFromERPixelsFloodFill(mGray, regions, ref erMats, true);

            // use the form to view and decide which ones the user wants to include in the training set
            WordLocRec.Forms.ChooseERForm cform = new Forms.ChooseERForm(regions, erMats, typeOfExample);
            if (regions.Count > 0) cform.ShowDialog();
            erMats.Dispose();
            if (cform.toReturn.Count > 0)
                chosen = new List<ERStat>(cform.toReturn);
           
            if (chosen.Count > 0)
            using (Mat bgr = new Mat(imageFullPath, LoadImageType.Unchanged))
            {
                if (oppositeChannel) CvInvoke.BitwiseNot(bgr, bgr);
                Mat pii = new Mat(), lab = new Mat();
                ColorConversionWrapper.ColorConversions(bgr, ColorConversionWrapper.ManualConversions.Pii, ref pii);
                ColorConversionWrapper.ColorConversions(bgr, ColorConversion.Bgr2Lab, ref lab);
                int counter = 0;
                for (int i = 0; i < chosen.Count; i++) // create an nmfeatureobject for each selected er
                {
                    FullFeatureObject ffo = new FullFeatureObject();
                    ERStat stat = chosen[i];

                    // calculate the nm2 features
                    ERUtils.CalcNM2stats(inputImage, ref stat); 
                    NMFeatureObject nmfeature = new NMFeatureObject((float)(stat.rect.Width) / (stat.rect.Height), (float)Math.Sqrt((float)(stat.area)) / stat.perimeter, (float)(1 - stat.euler), stat.medCrossings, stat.holeAreaRatio, stat.convexHullRatio, stat.numInflexionPoints);
                    ffo.nmFeature = nmfeature;

                    // calculate the histogram features
                    using (Mat roi = new Mat(inputImage.GetInputArray().GetMat(), stat.rect))
                    using (Mat erMask = Utils.ErMaskFloodFill(mGray, stat, true))
                    {
                        HistogramCalculations.HistFeatures hf = new HistogramCalculations(roi, erMask).calculateHistFeatures();
                        ffo.setHistFeaturesObject(hf);
                    }

                    float colorDistanceERBBPii = ERUtils.ColorDistanceERBB(mGray, pii, stat, ERUtils.EuclidianColorDistance);
                    ffo.colorDistERBB = colorDistanceERBBPii;
                    float colorDistanceERBBLab = ERUtils.ColorDistanceERBB(mGray, lab, stat, ERUtils.LabColorDistance);
                    ffo.colorDistERBBLab = colorDistanceERBBLab;
                    float colorDistanceERBBBgr = ERUtils.ColorDistanceERBB(mGray, bgr, stat, ERUtils.EuclidianColorDistance);
                    ffo.colorDistERBBBgr = colorDistanceERBBBgr;
                    float colorConsistencyLab = ERUtils.ColorConsistencyER(mGray, lab, stat, ERUtils.LabColorDistance);
                    ffo.colorConsistencyLab = colorConsistencyLab;

                    using (Mat erMask = Utils.ErMaskFloodFill(mGray, stat, false))
                    {
                        PathStats pathStats = ERUtils.SignificantPaths(erMask);
                        float contourSum = Utils.GetContourPointsSum(erMask);
                        ffo.significantPaths = pathStats.NumberOfPaths;
                        ffo.skelContourRatio = pathStats.GetTotalPixelsCount() / contourSum;
                        ffo.skelAreaRatio = pathStats.GetTotalPixelsCount() / (float)stat.area;
                        ffo.areaBbRatio = stat.area / (float)(stat.rect.Width * stat.rect.Height);

                        using (VectorOfVectorOfPoint contours = Utils.CalcContours(erMask))
                            if (contours.Size > 0)
                                using (VectorOfInt hullPoints = Utils.CalcConvexHullPoints(contours))
                                {
                                    float ConvexityDefects = Utils.CalcConvexityDefects(contours, hullPoints);
                                    if (float.IsNaN(ConvexityDefects)) ConvexityDefects = 0;
                                    ffo.ConvexityDefects = ConvexityDefects;
                                }
                        float StrokeWidth = StrokeUtils.MeanStrokeWidth(erMask);
                        if (float.IsNaN(StrokeWidth)) StrokeWidth = 0;
                        ffo.StrokeWidthERWidthRatio = StrokeWidth / stat.rect.Width;
                    }

                    ffo.exampleType = typeOfExample;
                    ffo.imageName = imageFullPath;
                    ffo.rect = stat.rect;
                    ffo.startPixel = stat.pixel;
                    ffo.imWidth = mGray.Cols;
                    ffo.imHeight = mGray.Rows;
                    ffo.thresholdLevel = stat.level;
                    ffo.channel = channelId;               

                    // save the feature object
                    String filepath = Path.Combine(outputXmlPath, Path.GetFileNameWithoutExtension(imageFullPath) + typeOfExample.ToString() + counter.ToString() + ".xml");
                    if (File.Exists(filepath))
                    {
                        Random r = new Random();
                        filepath = Path.Combine(outputXmlPath, Path.GetFileNameWithoutExtension(imageFullPath) + typeOfExample.ToString() + counter.ToString() + r.Next(10).ToString() + r.Next(10).ToString() + r.Next(10).ToString() + ".xml");
                    }
                    ffo.ToXML(filepath);

                    counter++;
                }
                pii.Dispose();
                lab.Dispose();
            }                               
        }

        private void CerPruning()
        {
            List<ERStat> temp2 = new List<ERStat>(regions);
            regions = new List<ERStat>(temp2.Count);
            erNonCerSuppression(temp2[0], null, null);
        }

        private void NearDuplicateSuppression(bool repeated)
        {
            int prevRegCountND = -1;
            int counterNd = 0;
            while (regions.Count != prevRegCountND)
            {
                counterNd++;
                prevRegCountND = regions.Count;

                List<ERStat> tempND = new List<ERStat>(regions);
                regions = new List<ERStat>(tempND.Count);
                erNDSearch(tempND[0], null, null);

                tempND = new List<ERStat>(regions);
                regions = new List<ERStat>(tempND.Count);
                erDeleteNearDuplicate(tempND[0], null, null);
                if (counterNd > 0 && (!repeated))  break;
            }
        }

        private List<ERStat> erExtract(IInputArray inputImage, bool typeOfExample = false)
        {
            Mat src = inputImage.GetInputArray().GetMat();
            if (src.NumberOfChannels > 1) throw new Exception("More than one channels of src image at erExtract");
            if (src.Depth != Emgu.CV.CvEnum.DepthType.Cv8U) throw new Exception("Expected byte image; found " + src.Depth + " instead");

            // apply the step in the image, beforehand
            ERUtils.ConvertToDelta(ref src, thresholdDelta);

            // initialize algorithm variables
            int thresholdLevel = (255 / thresholdDelta) + 1;
            width = src.Cols; height = src.Rows;
            byte[,] quads = ERUtils.InitializeQuads();
            bool[] accessiblePixelMask = new bool[width * height];
            bool[] accumulatedPixelMask = new bool[width * height];
            List<int>[] boundaryPixels = new List<int>[256];
            List<int>[] boundaryEdges = new List<int>[256];
            for (int i = 0; i < boundaryPixels.Length; i++)
            {
                boundaryPixels[i] = new List<int>();
                boundaryEdges[i] = new List<int>();
            }


            regions = new List<ERStat>();
            List<ERStat> erStack = new List<ERStat>();           
            //add an empty ERStat as the root of the ERTree
            erStack.Add(new ERStat());

            Byte[] imageData = src.GetData();

            int currentPixel = 0;
            int currentEdge = 0;
            int currentLevel = imageData[0];
            accessiblePixelMask[0] = true;

            bool pushNewComponent = true;
            //initialization ends here
            byte[] currentBinaryMask = new byte[width * height];
            for (; ; )
            {
                int x = currentPixel % width;
                int y = currentPixel / width;

                if (pushNewComponent)
                {
                    erStack.Add(new ERStat(currentLevel, currentPixel, x, y));
                    pushNewComponent = false;
                }

                pushNewComponent = ERUtils.SearchNeighbours(x, y, ref currentPixel, ref currentEdge, ref currentLevel, ref thresholdLevel, width, height, ref imageData, ref accessiblePixelMask, ref boundaryPixels, ref boundaryEdges);

                if (pushNewComponent) continue;

                IncrementalyComputableDescriptors icd = new IncrementalyComputableDescriptors();

                icd = ERUtils.ComputeDescriptors(x, y, width, height, currentPixel, ref imageData, accumulatedPixelMask, quads);

                ERStat parent = erStack[erStack.Count - 1];
                ERUtils.erAddPixel(ref parent, x, y, icd, currentPixel, true);
                
                accumulatedPixelMask[currentPixel] = true;

                if (thresholdLevel == (255/thresholdDelta) + 1) // if all possible threshold levels have been processed signal the end
                {
                    ERStat child = erStack[erStack.Count - 1];
                    ERUtils.IncludeNewChildPixels(ref child);

                    erSaveCersOnly(erStack[erStack.Count - 1], null, null, typeOfExample);

                    return regions;
                }

                currentPixel = boundaryPixels[thresholdLevel][boundaryPixels[thresholdLevel].Count - 1];
                boundaryPixels[thresholdLevel].RemoveAt(boundaryPixels[thresholdLevel].Count - 1);
                currentEdge = boundaryEdges[thresholdLevel][boundaryEdges[thresholdLevel].Count - 1];
                boundaryEdges[thresholdLevel].RemoveAt(boundaryEdges[thresholdLevel].Count - 1);

                while ((thresholdLevel < (255 / thresholdDelta) + 1) && boundaryPixels[thresholdLevel].Count == 0)
                {
                    thresholdLevel++;
                }

                int newLevel = imageData[currentPixel];
                if (newLevel != currentLevel)
                {
                    currentLevel = newLevel;

                    while (erStack[erStack.Count - 1].level < newLevel)
                    {
                        ERStat er = erStack[erStack.Count - 1];
                        erStack.RemoveAt(erStack.Count - 1);

                        if (newLevel < erStack[erStack.Count - 1].level)// auto uparxei gia thn periptwsh emfoleumenwn er poy apotelountai apo 1 mono pixel
                        {
                            erStack.Add(new ERStat(newLevel, currentPixel, currentPixel % width, currentPixel / width));

                            erMerge(erStack[erStack.Count - 1], er, true, typeOfExample);
                            break;
                        }
                        erMerge(erStack[erStack.Count - 1], er, true, typeOfExample);
                    }
                }
            }
        }

        private void erMerge(ERStat parent, ERStat child, bool savePixels, bool typeOfExample)
        {
            if (savePixels) ERUtils.CombineParentChildPixels(ref parent, ref child);

            ERUtils.AddAreaPerimeterEuler(ref parent, ref child);

            ERUtils.MergeCrossings(ref parent, ref child);

            ERUtils.ReshapeBoundingBox(ref parent, child.rect.X, child.rect.Y, child.rect.Right - 1, child.rect.Bottom - 1);

            ERUtils.ConsumeMoments(ref parent, (int)child.rawMoments[0], (int)child.rawMoments[1], (int)child.centralMoments[0], (int)child.centralMoments[1], (int)child.centralMoments[2]);

            ERUtils.CalcMedCrossings(ref child); 

            child.level = child.level * thresholdDelta;            

            if (ERUtils.SizeRestrictionsArea(child, ep) && typeDecisionGroundTruth(typeOfExample, child))
            {
                //if (typeDecisionGroundTruth(typeOfExample, child))
                ERUtils.AcceptChild(ref parent, ref child);
            }
            else
            {
                ERUtils.RejectChild(ref parent, ref child);
            }               
        }

        /// <summary>
        /// if we are looking for positive samples look inside the groundtruth for ers
        /// if we are looking for negative samples look explicitly outside the groundtruth for ers
        /// </summary>
        /// <param name="typeOfExample"></param>
        /// <param name="child"></param>
        /// <returns></returns>
        private bool typeDecisionGroundTruth(bool typeOfExample, ERStat child)
        {
            return (typeOfExample && ChildInsideGt(typeOfExample, child) || ((!typeOfExample) && !ChildInsideGt(typeOfExample, child)));
        }
        private bool ChildInsideGt(bool typeOfExample, ERStat child)
        {
            if (!groundTruthNeeded) return typeOfExample;
            Rectangle chRect = child.rect;

            foreach (Rectangle r in groundTruth)
            {
                if (chRect.IntersectsWith(r)) return true;
            }
            return false;
        }

        #region Recursive shit with global variable
              
        private ERStat erSaveCersOnly(ERStat er, ERStat parent, ERStat prev, bool typeOfExample)
        {
            regions.Add(er);
            regions[regions.Count - 1].parent = parent;

            if (prev != null) prev.next = regions[regions.Count - 1];
            else if (parent != null) parent.child = regions[regions.Count - 1];

            ERStat oldPrev = null;
            ERStat thisER = regions[regions.Count - 1];


            //if (!typeOfExample) // include CerCalc only for random examples
            //{
                // cer decision
                if (thisER.parent == null)
                {
                    thisER.areaVariation = 1f;
                    thisER.isCer = false;
                }
                else
                {
                    ERStat tempParent = ERUtils.FindAGoodParent(ref thisER); // finds a parent with > level + 3

                    if (tempParent.parent != null) // allagh wste na mhn sugrinei ta ERs poy einai monadika me to root ER kai xanei plhroforia
                    {
                        thisER.areaVariation = (float)(tempParent.area - thisER.area) / thisER.area;
                        thisER.isCer = thisER.areaVariation < 0.5 ? true : false;
                    }
                    else
                    {
                        thisER.areaVariation = 1f;
                        thisER.isCer = true;
                    }
                }
                //end cer decision
            //}

            for (ERStat child = er.child; child != null; child = child.next)
                oldPrev = erSaveCersOnly(child, thisER, oldPrev, typeOfExample);

            return thisER;
        }
        private ERStat erNDSearch(ERStat er, ERStat parent, ERStat prev)
        {
            regions.Add(er);
            regions[regions.Count - 1].parent = parent;

            if (prev != null) prev.next = regions[regions.Count - 1];
            else if (parent != null) parent.child = regions[regions.Count - 1];

            ERStat oldPrev = null;
            ERStat thisER = regions[regions.Count - 1];

            // test for near duplicate decision
            if (thisER.parent == null)
            {
                thisER.nearDuplicate = false;
            }
            else
            {
                float areaVariation = (float)(parent.area - thisER.area) / thisER.area;
                if (areaVariation < 0.1) // parent and thisER are near duplicate
                {
                    if (thisER.areaVariation > parent.areaVariation)
                    {
                        thisER.nearDuplicate = true;
                    }
                    else
                    {
                        parent.nearDuplicate = true;
                    }
                }
            }

            for (ERStat child = er.child; child != null; child = child.next)
                oldPrev = erNDSearch(child, thisER, oldPrev);

            return thisER;
        } // swzei ta non near duplicates
        private ERStat erNonCerSuppression(ERStat stat, ERStat parent, ERStat prev)
        {
            if (stat.isCer || stat.parent == null)
            {
                ERStat demostat = ERStat.DeepCopy(stat);
                regions.Add(demostat);
                // prosthetei to neo komvo diagrafontas opoiadhpote sxesh me aderfia klp
                // gi ayto kanei tis sundeseis-aposundeseis sta plaisia ths listas "regions"
                // wste auth na apotelei to neo pruned dentro
                regions[regions.Count - 1].parent = parent;
                regions[regions.Count - 1].next = null;
                regions[regions.Count - 1].child = null;

                if (prev != null) prev.next = regions[regions.Count - 1];
                else if (parent != null) parent.child = regions[regions.Count - 1];

                ERStat oldPrev = null;
                ERStat thisER = regions[regions.Count - 1];

                for (ERStat child = stat.child; child != null; child = child.next)
                    oldPrev = erNonCerSuppression(child, thisER, oldPrev);

                return thisER;
            }
            else
            {
                ERStat oldPrev = prev;

                for (ERStat child = stat.child; child != null; child = child.next)
                    oldPrev = erNonCerSuppression(child, parent, oldPrev);

                return oldPrev;
            }
        }
        private ERStat erDeleteNearDuplicate(ERStat stat, ERStat parent, ERStat prev)
        {
            if (!stat.nearDuplicate || stat.parent == null)
            {
                ERStat demostat = ERStat.DeepCopy(stat);
                regions.Add(demostat);
                // prosthetei to neo komvo diagrafontas opoiadhpote sxesh me aderfia klp
                // gi ayto kanei tis sundeseis-aposundeseis sta plaisia ths listas "regions"
                // wste auth na apotelei to neo pruned dentro
                regions[regions.Count - 1].parent = parent;
                regions[regions.Count - 1].next = null;
                regions[regions.Count - 1].child = null;

                if (prev != null) prev.next = regions[regions.Count - 1];
                else if (parent != null) parent.child = regions[regions.Count - 1];

                ERStat oldPrev = null;
                ERStat thisER = regions[regions.Count - 1];

                for (ERStat child = stat.child; child != null; child = child.next)
                    oldPrev = erDeleteNearDuplicate(child, thisER, oldPrev);

                return thisER;
            }
            else
            {
                ERStat oldPrev = prev;

                for (ERStat child = stat.child; child != null; child = child.next)
                    oldPrev = erDeleteNearDuplicate(child, parent, oldPrev);

                return oldPrev;
            }
        }

        #endregion
    }
}
