﻿using Emgu.CV;
using Emgu.CV.CvEnum;
using Emgu.CV.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace WordLocRec.Tools
{
    class ColorQuantizationWrapper
    {

        /// <summary>
        /// Quantizes an image from 256 to <paramref name="levels"/> number of levels using Lloyd-Max color clustering algorithm. Image can have one channel or three channels. In case of three channels, each one is processed independently and the final result is the the merge of the three.
        /// </summary>
        /// <param name="image">The input image</param>
        /// <param name="levels">The resulting number of quantized levels</param>
        /// <param name="result">The reference of the Mat object to save the result.</param>
        public static void ColorQuantizationControler(IInputArray image, int levels, ref Mat result)
        {
            Mat src = image.GetInputArray().GetMat();
            // convert image to 8 bit representation 
            if (src.Depth != DepthType.Cv8U) src.ConvertTo(src, DepthType.Cv8U);
            int numberOfChannels = src.NumberOfChannels;
            if (numberOfChannels == 0) throw new Exception("Empty object");
            if (numberOfChannels > 1)
            {
                // split and process individually
                VectorOfMat channels = new VectorOfMat(), resultChannels = new VectorOfMat();
                CvInvoke.Split(src, channels);
                for (int i = 0; i < numberOfChannels; i++)
                {
                    resultChannels.Push(ColorQuantization(channels[i], levels));
                }
                CvInvoke.Merge(resultChannels, result);
                channels.Dispose();
                resultChannels.Dispose();
            }
            else
            {
                result = ColorQuantization(src, levels);
            }
        }

        /// <summary>
        /// Function that quantizes the image to the wanted levels.
        /// </summary>
        /// <param name="channel">The input one channel image</param>
        /// <param name="levels">The resulting nubmer of quantized levels</param>
        /// <returns></returns>
        private static Mat ColorQuantization(IInputArray channel, int levels)
        {
            Mat src = channel.GetInputArray().GetMat();

            Byte[] pixelData = src.GetData();

            //create a histogram from the data and find the minimum and the maximum
            int[] histogram = new int[256];
            for (int i = 0; i < pixelData.Length; i++) 
                histogram[pixelData[i]]++;
            
            int maxValue = 255;
            while (histogram[maxValue] == 0)
            {
                maxValue--;
            }
            int minValue = 0;
            while (histogram[minValue] == 0)
            {
                minValue++;
            }

            // randomly assing for each level a centroid value inside the image range
            double[] centroids = new double[levels];
            double[] oldCentroids = new double[levels];
            int[] clusters = new int[levels];
            Random r = new Random();
            for (int i = 0; i < levels; i++) 
            {
                clusters[i] = 0;
                centroids[i] = r.Next(minValue, maxValue);
                oldCentroids[i] = 0;
            }
            Array.Sort(centroids);
            Array.Sort(oldCentroids);

            while (!hasConverged(centroids, oldCentroids))
            {
                oldCentroids = centroids;
                // assign all histogram counts to clusters and reevaluate centroids
                centroids = PixelsToClusters(pixelData, oldCentroids);
            }

            // get the rounded value of each centroid 
            byte[] roundedCentroids = new byte[levels];
            for (int i = 0; i < levels; i++)
                roundedCentroids[i] = (byte)Math.Round(centroids[i]);

            // create the quantized image assigning each pixel to the closest cluster
            Byte[] quantImgData = new Byte[pixelData.Length];
            GCHandle h = GCHandle.Alloc(quantImgData, GCHandleType.Pinned);
            int bestCentroidIndex;
            for (int i = 0; i < pixelData.Length; i++)
            {
                bestCentroidIndex = FindNearestCluster(pixelData[i], centroids);

                quantImgData[i] = roundedCentroids[bestCentroidIndex];
            }

            Mat m = new Mat(src.Size, DepthType.Cv8U, 1, h.AddrOfPinnedObject(), src.Width);
            return m;

        }

        /// <summary>
        /// Assigns every pixel to a cluster and uses the mean of every cluster as the centroid
        /// </summary>
        /// <param name="pixelData">Array of pixel data</param>
        /// <param name="centroids">Values of the centroids</param>
        /// <returns></returns>
        private static double[] PixelsToClusters(byte[] pixelData, double[] centroids)
        {
            int[] clusterLengths = new int[centroids.Length];
            double[] clusterValues = new double[centroids.Length];
            for (int i = 0; i < clusterLengths.Length; i++)
            {
                clusterLengths[i] = 0;
                clusterValues[i] = 0d;
            }
            int bestCentroidIndex;
            for (int i = 0; i < pixelData.Length; i++)
            {
                bestCentroidIndex = FindNearestCluster(pixelData[i], centroids);
                clusterLengths[bestCentroidIndex]++;
                clusterValues[bestCentroidIndex] += pixelData[i];
            }
            for (int i = 0; i < clusterLengths.Length; i++)
                clusterValues[i] = Double.IsNaN(clusterValues[i] / clusterLengths[i]) ? 0 : clusterValues[i] / clusterLengths[i];

            return clusterValues;

        }

        /// <summary>
        /// Find the closest cluster index to a pixel value
        /// </summary>
        /// <param name="pixelValue">The value of the pixel</param>
        /// <param name="centroids">The array of the centroids</param>
        /// <returns></returns>
        private static int FindNearestCluster(byte pixelValue, double[] centroids)
        {
            int bestCentroidIndex = -1;
            double minDistance = 256;
            for (int j = 0; j < centroids.Length; j++)
            {
                double newDistance = Math.Abs(pixelValue - centroids[j]);
                if (newDistance < minDistance)
                {
                    minDistance = newDistance;
                    bestCentroidIndex = j;
                }
            }
            return bestCentroidIndex;
        }

        /// <summary>
        /// Calculates the difference between each corresponding centroid value
        /// </summary>
        /// <param name="centroids">The array of the current centroids</param>
        /// <param name="oldCentroids">The array of the old centroids</param>
        /// <returns></returns>
        private static bool hasConverged(double[] centroids, double[] oldCentroids)
        {
            double epsilon = Constants.EPSILON;
            for (int i = 0; i < centroids.Length; i++)
                if (Math.Abs(centroids[i] - oldCentroids[i]) > epsilon)
                    return false;
            return true;
        }
    }
}
