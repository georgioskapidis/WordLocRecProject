﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WordLocRec.Tools
{
    public static class RecursiveFunctions
    {
        public static int counter = 0, counterDifferent = 0;
        [ThreadStatic]
        private static List<ERStat> regions;
        private static float probability;
        public delegate ERStat PruneDesicitonFunction(ERStat stat, ERStat parent, ERStat prev);
        public static List<ERStat> SuppressNonMaximal(List<ERStat> regs, PruneDesicitonFunction fun)
        {
            regions = regs;

            List<ERStat> tempNMS = new List<ERStat>(regions);
            regions = new List<ERStat>(tempNMS.Count);
            fun(tempNMS[0], null, null);

            List<ERStat> temp = new List<ERStat>(regions);
            regions = new List<ERStat>(temp.Count);
            erTreeNonmaxSuppression(temp[0], null, null);

            List<ERStat> result = new List<ERStat>(regions);
            regions.Clear();
            return result;
        }
        public static List<ERStat> SuppressWrong(List<ERStat> regs, PruneDesicitonFunction prune, float prob = 0.2f)
        {
            regions = regs;
            probability = prob;
            List<ERStat> temp = new List<ERStat>(regions);
            regions = new List<ERStat>(temp.Count);
            prune(temp[0], null, null);

            List<ERStat> result = new List<ERStat>(regions);
            regions.Clear();
            return result;
        }
        public static List<ERStat> SuppressCorrect(List<ERStat> regs, PruneDesicitonFunction prune)
        {
            regions = regs;

            List<ERStat> temp = new List<ERStat>(regions);
            regions = new List<ERStat>(temp.Count);
            prune(temp[0], null, null);

            List<ERStat> result = new List<ERStat>(regions);
            regions.Clear();
            return result;
        }

        public static List<ERStat> NonCerSuppression(List<ERStat> regs)
        {
            regions = regs;

            List<ERStat> temp2 = new List<ERStat>(regions);
            regions = new List<ERStat>(temp2.Count);
            erNonCerSuppression(temp2[0], null, null);

            List<ERStat> result = new List<ERStat>(regions);
            regions.Clear();
            return result;
        }
        public static List<ERStat> FillRateSuppression(List<ERStat> regs)
        {
            regions = regs;

            List<ERStat> temp = new List<ERStat>(regions);
            regions = new List<ERStat>(temp.Count);
            erPruneFillRate(temp[0], null, null);

            temp = new List<ERStat>(regions);
            regions = new List<ERStat>(temp.Count);
            erDeleteNearDuplicate(temp[0], null, null);

            List<ERStat> result = new List<ERStat>(regions);
            regions.Clear();
            return result;
        }
        public static List<ERStat> SaveCersAndSuppress(List<ERStat> regs)
        {
            regions = regs;

            List<ERStat> temp = new List<ERStat>(regions);
            regions = new List<ERStat>(temp.Count);
            erSaveCersOnly(temp[0], null, null);

            temp = new List<ERStat>(regions);
            regions = new List<ERStat>(temp.Count);
            erNonCerSuppression(temp[0], null, null);

            List<ERStat> result = new List<ERStat>(regions);
            regions.Clear();
            return result;
        }

        public static List<ERStat> NearDuplicateSuppression(List<ERStat> regs, bool repeated)
        {
            regions = regs;
            int prevRegCountND = -1;
            int counterNd = 0;
            while (regions.Count != prevRegCountND)
            {
                counterNd++;
                prevRegCountND = regions.Count;

                List<ERStat> tempND = new List<ERStat>(regions);
                regions = new List<ERStat>(tempND.Count);
                erNDSearch(tempND[0], null, null);

                tempND = new List<ERStat>(regions);
                regions = new List<ERStat>(tempND.Count);
                erDeleteNearDuplicate(tempND[0], null, null);
                if (counterNd > 0 && (!repeated)) break;
            }
            List<ERStat> result = new List<ERStat>(regions);
            regions.Clear();
            return result;
        }

        #region NMS methods
        public static ERStat erSaveNMSOnly(ERStat er, ERStat parent, ERStat prev)
        {
            regions.Add(er);
            regions[regions.Count - 1].parent = parent;

            if (prev != null) prev.next = regions[regions.Count - 1];
            else if (parent != null) parent.child = regions[regions.Count - 1];

            ERStat oldPrev = null;
            ERStat thisER = regions[regions.Count - 1];
            if (thisER.parent == null)
            {
                thisER.probability = 0;
                thisER.maxProbabilityAncestor = thisER;
                thisER.minProbabilityAncestor = thisER;
            }
            else
            {
                thisER.maxProbabilityAncestor = (thisER.probability > parent.maxProbabilityAncestor.probability) ? thisER : parent.maxProbabilityAncestor;
                thisER.minProbabilityAncestor = (thisER.probability < parent.minProbabilityAncestor.probability) ? thisER : parent.minProbabilityAncestor;

                if ((thisER.maxProbabilityAncestor.probability > 0.2) && (thisER.maxProbabilityAncestor.probability - thisER.minProbabilityAncestor.probability > 0.1))
                {
                    thisER.maxProbabilityAncestor.local_maxima = true;
                    if ((thisER.parent.local_maxima) && (thisER.maxProbabilityAncestor == thisER))
                        thisER.parent.local_maxima = false;
                }
                else if (thisER.probability < thisER.parent.probability)
                    thisER.minProbabilityAncestor = thisER;
                else if (thisER.probability > thisER.parent.probability)
                    thisER.maxProbabilityAncestor = thisER;
            }

            for (ERStat child = er.child; child != null; child = child.next)
                oldPrev = erSaveNMSOnly(child, thisER, oldPrev);

            return thisER;
        }// idio me to arxiko erSave
        public static ERStat erSaveNMSOnlyStrict(ERStat er, ERStat parent, ERStat prev) 
        {
            regions.Add(er);
            regions[regions.Count - 1].parent = parent;

            if (prev != null) prev.next = regions[regions.Count - 1];
            else if (parent != null) parent.child = regions[regions.Count - 1];

            ERStat oldPrev = null;
            ERStat thisER = regions[regions.Count - 1];
            if (thisER.parent == null)
            {
                thisER.probability = 0;
                thisER.maxProbabilityAncestor = thisER;
                thisER.minProbabilityAncestor = thisER;
            }
            else if (CheckRects(thisER))
            {
                thisER.maxProbabilityAncestor = thisER;
                thisER.minProbabilityAncestor = thisER;
                thisER.local_maxima = true;
            }
            else
            {
                thisER.maxProbabilityAncestor = (thisER.probability > parent.maxProbabilityAncestor.probability) ? thisER : parent.maxProbabilityAncestor;
                thisER.minProbabilityAncestor = (thisER.probability < parent.minProbabilityAncestor.probability) ? thisER : parent.minProbabilityAncestor;

                if ((thisER.maxProbabilityAncestor.probability > 0.2) && (thisER.maxProbabilityAncestor.probability - thisER.minProbabilityAncestor.probability > 0.00001))
                {
                    thisER.maxProbabilityAncestor.local_maxima = true;
                    if ((thisER.parent.local_maxima) && (thisER.maxProbabilityAncestor == thisER))
                        thisER.parent.local_maxima = false;
                }
                else if (thisER.probability < thisER.parent.probability)
                    thisER.minProbabilityAncestor = thisER;
                else if (thisER.probability > thisER.parent.probability)
                    thisER.maxProbabilityAncestor = thisER;
            }

            for (ERStat child = er.child; child != null; child = child.next)
                oldPrev = erSaveNMSOnlyStrict(child, thisER, oldPrev);

            return thisER;
        }
        private static bool CheckRects(ERStat thisER)
        {
            Rectangle thisRect = thisER.rect;
            int thisRectArea = thisRect.Width * thisRect.Height;
            Rectangle parentRect = thisER.parent.rect;
            int parentRectArea = parentRect.Width * parentRect.Height;
            if (thisRectArea > 0.5 * parentRectArea)
                return false;
            return true;
        }

        private static ERStat erTreeNonmaxSuppression(ERStat stat, ERStat parent, ERStat prev)
        {
            if ((stat.local_maxima || stat.parent == null))
            {
                ERStat demoStat = ERStat.DeepCopy(stat);
                regions.Add(demoStat);
                regions[regions.Count - 1].parent = parent;
                regions[regions.Count - 1].next = null;
                regions[regions.Count - 1].child = null;

                if (prev != null) prev.next = regions[regions.Count - 1];
                else if (parent != null) parent.child = regions[regions.Count - 1];

                ERStat oldPrev = null;
                ERStat thisER = regions[regions.Count - 1];

                for (ERStat child = stat.child; child != null; child = child.next)
                    oldPrev = erTreeNonmaxSuppression(child, thisER, oldPrev);

                return thisER; // kathe stat epistrefei ton eauto tou gia na exoun apo poy na deixnontai ta aderfia toy, an yparxoun
            }
            else
            {
                ERStat oldPrev = prev;

                for (ERStat child = stat.child; child != null; child = child.next)
                    oldPrev = erTreeNonmaxSuppression(child, parent, oldPrev);

                return oldPrev;
            }
        }
        public static ERStat erResponsePruningOpposite(ERStat stat, ERStat parent, ERStat prev)
        {
            if (stat.svmResponce == 2 || stat.parent == null)
            {
                ERStat demoStat = ERStat.DeepCopy(stat);
                regions.Add(demoStat);
                regions[regions.Count - 1].parent = parent;
                regions[regions.Count - 1].next = null;
                regions[regions.Count - 1].child = null;

                if (prev != null) prev.next = regions[regions.Count - 1];
                else if (parent != null) parent.child = regions[regions.Count - 1];

                ERStat oldPrev = null;
                ERStat thisER = regions[regions.Count - 1];

                for (ERStat child = stat.child; child != null; child = child.next)
                    oldPrev = erResponsePruningOpposite(child, thisER, oldPrev);

                return thisER; // kathe stat epistrefei ton eauto tou gia na exoun apo poy na deixnontai ta aderfia toy, an yparxoun
            }
            else
            {
                ERStat oldPrev = prev;

                for (ERStat child = stat.child; child != null; child = child.next)
                    oldPrev = erResponsePruningOpposite(child, parent, oldPrev);

                return oldPrev;
            }
        }
        public static ERStat erResponsePruning(ERStat stat, ERStat parent, ERStat prev)
        {
            if (stat.svmResponce == 1 || stat.parent == null)
            {
                ERStat demoStat = ERStat.DeepCopy(stat);
                regions.Add(demoStat);
                regions[regions.Count - 1].parent = parent;
                regions[regions.Count - 1].next = null;
                regions[regions.Count - 1].child = null;

                if (prev != null) prev.next = regions[regions.Count - 1];
                else if (parent != null) parent.child = regions[regions.Count - 1];

                ERStat oldPrev = null;
                ERStat thisER = regions[regions.Count - 1];

                for (ERStat child = stat.child; child != null; child = child.next)
                    oldPrev = erResponsePruning(child, thisER, oldPrev);

                return thisER; // kathe stat epistrefei ton eauto tou gia na exoun apo poy na deixnontai ta aderfia toy, an yparxoun
            }
            else
            {
                ERStat oldPrev = prev;

                for (ERStat child = stat.child; child != null; child = child.next)
                    oldPrev = erResponsePruning(child, parent, oldPrev);

                return oldPrev;
            }
        }
        public static ERStat erProbabilityPruning(ERStat stat, ERStat parent, ERStat prev)
        {
            if (stat.probability >= probability || stat.parent == null)
            {
                ERStat demoStat = ERStat.DeepCopy(stat);
                regions.Add(demoStat);
                regions[regions.Count - 1].parent = parent;
                regions[regions.Count - 1].next = null;
                regions[regions.Count - 1].child = null;

                if (prev != null) prev.next = regions[regions.Count - 1];
                else if (parent != null) parent.child = regions[regions.Count - 1];

                ERStat oldPrev = null;
                ERStat thisER = regions[regions.Count - 1];

                for (ERStat child = stat.child; child != null; child = child.next)
                    oldPrev = erProbabilityPruning(child, thisER, oldPrev);

                return thisER; // kathe stat epistrefei ton eauto tou gia na exoun apo poy na deixnontai ta aderfia toy, an yparxoun
            }
            else
            {
                ERStat oldPrev = prev;
                for (ERStat child = stat.child; child != null; child = child.next)
                    oldPrev = erProbabilityPruning(child, parent, oldPrev);

                return oldPrev;
            }
        }
        #endregion

        private static ERStat erPruneFillRate(ERStat er, ERStat parent, ERStat prev)
        {
            regions.Add(er);
            regions[regions.Count - 1].parent = parent;

            if (prev != null) prev.next = regions[regions.Count - 1];
            else if (parent != null) parent.child = regions[regions.Count - 1];

            ERStat oldPrev = null;
            ERStat thisER = regions[regions.Count - 1];

            if (thisER.parent != null)
            {
                if (CheckRectsFillRateLinear(thisER)) thisER.parent.nearDuplicate = true;
                else if (CheckRectsFillRateHierarchical(thisER)) thisER.parent.nearDuplicate = true;
            }

            for (ERStat child = er.child; child != null; child = child.next)
                oldPrev = erPruneFillRate(child, thisER, oldPrev);

            return thisER;
        }
        private static bool CheckRectsFillRateLinear(ERStat thisER)
        {
            int thisArea = thisER.area;
            int thisBBarea = thisER.rect.Width * thisER.rect.Height;
            float thisFillRate = (float)thisArea / thisBBarea;

            int parentBBarea = thisER.parent.rect.Width * thisER.parent.rect.Height;
            float DeltaFillRate = 1;
            if (parentBBarea != thisBBarea)
                DeltaFillRate = (float)(thisER.parent.area - thisArea) / (parentBBarea - thisBBarea);

            if ((float)thisArea / thisER.parent.area > 0.5 && DeltaFillRate < 0.5 * thisFillRate)
                return true;
            return false;
        }
        private static bool CheckRectsFillRateHierarchical(ERStat thisER)
        {
            if (thisER.next == null) return false;
            int numSiblings = 1;
            int totalChildrenArea = thisER.area;
            int totalChildrenBB = thisER.rect.Width * thisER.rect.Height;
            float avgFillRate = (float)thisER.area / (thisER.rect.Width * thisER.rect.Height);
            ERStat next = thisER.next;
            while (next != null)
            {
                numSiblings++;
                totalChildrenArea += next.area;
                totalChildrenBB += next.rect.Width * next.rect.Height;
                avgFillRate += (float)next.area / (next.rect.Width * next.rect.Height);
                next = next.next;
            }
            avgFillRate /= numSiblings;
            int parentBBarea = thisER.parent.rect.Width * thisER.parent.rect.Height;
            float DeltaFillRate = 1;
            if (parentBBarea != totalChildrenBB)
                DeltaFillRate = (float)(thisER.parent.area - totalChildrenArea) / (parentBBarea - totalChildrenBB);

            if (DeltaFillRate < 0.8 * avgFillRate)
                return true;
            return false;

        }

        public static ERStat TreeSanityCheck(ERStat thisER)
        {
            if (thisER.parent != null)
            {
                if (thisER.parent.child == null)
                {

                }
            }
            for (ERStat child = thisER.child; child != null; child = child.next)
            {
                TreeSanityCheck(child);
            }
            return thisER;
        }

        #region CerSuppression methods
        private static ERStat erSaveCersOnly(ERStat er, ERStat parent, ERStat prev)
        {
            regions.Add(er);
            regions[regions.Count - 1].parent = parent;

            if (prev != null) prev.next = regions[regions.Count - 1];
            else if (parent != null) parent.child = regions[regions.Count - 1];

            ERStat oldPrev = null;
            ERStat thisER = regions[regions.Count - 1];

            // cer decision
            if (thisER.parent == null)
            {
                thisER.areaVariation = 1f;
                thisER.isCer = false;
            }
            else
            {
                ERStat tempParent = ERUtils.FindAGoodParent(ref thisER); // finds a parent with > level + 3

                if (tempParent.parent != null) // allagh wste na mhn sugrinei ta ERs poy einai monadika me to root ER kai xanei plhroforia
                {
                    thisER.areaVariation = (float)(tempParent.area - thisER.area) / thisER.area;
                    thisER.isCer = thisER.areaVariation < 0.5 ? true : false;
                }
                else
                {
                    thisER.areaVariation = 1f;
                    thisER.isCer = true;
                }
            }
            //end cer decision

            for (ERStat child = er.child; child != null; child = child.next)
                oldPrev = erSaveCersOnly(child, thisER, oldPrev);

            return thisER;
        } // swzei mono tis plhrofories gia ta CERs
        private static ERStat erNonCerSuppression(ERStat stat, ERStat parent, ERStat prev)
        {
            if (stat.isCer || stat.parent == null)
            {
                ERStat demostat = ERStat.DeepCopy(stat);
                regions.Add(demostat);
                // prosthetei to neo komvo diagrafontas opoiadhpote sxesh me aderfia klp
                // gi ayto kanei tis sundeseis-aposundeseis sta plaisia ths listas "regions"
                // wste auth na apotelei to neo pruned dentro
                regions[regions.Count - 1].parent = parent;
                regions[regions.Count - 1].next = null;
                regions[regions.Count - 1].child = null;

                if (prev != null) prev.next = regions[regions.Count - 1];
                else if (parent != null) parent.child = regions[regions.Count - 1];

                ERStat oldPrev = null;
                ERStat thisER = regions[regions.Count - 1];

                for (ERStat child = stat.child; child != null; child = child.next)
                    oldPrev = erNonCerSuppression(child, thisER, oldPrev);

                return thisER;
            }
            else
            {
                ERStat oldPrev = prev;

                for (ERStat child = stat.child; child != null; child = child.next)
                    oldPrev = erNonCerSuppression(child, parent, oldPrev);

                return oldPrev;
            }
        }
        #endregion

        #region NearDuplicateSuppression methods
        private static ERStat erNDSearch(ERStat er, ERStat parent, ERStat prev)
        {
            regions.Add(er);
            regions[regions.Count - 1].parent = parent;

            if (prev != null) prev.next = regions[regions.Count - 1];
            else if (parent != null) parent.child = regions[regions.Count - 1];

            ERStat oldPrev = null;
            ERStat thisER = regions[regions.Count - 1];

            // test for near duplicate decision
            if (thisER.parent == null)
            {
                thisER.nearDuplicate = false;
            }
            else
            {
                float areaVariation = (float)(parent.area - thisER.area) / thisER.area;
                if (areaVariation < 0.1) // parent and thisER are near duplicate
                {
                    if (thisER.areaVariation > parent.areaVariation)
                    {
                        thisER.nearDuplicate = true;
                    }
                    else
                    {
                        parent.nearDuplicate = true;
                    }
                }
            }

            for (ERStat child = er.child; child != null; child = child.next)
                oldPrev = erNDSearch(child, thisER, oldPrev);

            return thisER;
        } // swzei ta non near duplicates
        private static ERStat erDeleteNearDuplicate(ERStat stat, ERStat parent, ERStat prev)
        {
            if (!stat.nearDuplicate || stat.parent == null)
            {
                ERStat demostat = ERStat.DeepCopy(stat);
                regions.Add(demostat);
                // prosthetei to neo komvo diagrafontas opoiadhpote sxesh me aderfia klp
                // gi ayto kanei tis sundeseis-aposundeseis sta plaisia ths listas "regions"
                // wste auth na apotelei to neo pruned dentro
                regions[regions.Count - 1].parent = parent;
                regions[regions.Count - 1].next = null;
                regions[regions.Count - 1].child = null;

                if (prev != null) prev.next = regions[regions.Count - 1];
                else if (parent != null) parent.child = regions[regions.Count - 1];

                ERStat oldPrev = null;
                ERStat thisER = regions[regions.Count - 1];

                for (ERStat child = stat.child; child != null; child = child.next)
                    oldPrev = erDeleteNearDuplicate(child, thisER, oldPrev);

                return thisER;
            }
            else
            {
                ERStat oldPrev = prev;

                for (ERStat child = stat.child; child != null; child = child.next)
                    oldPrev = erDeleteNearDuplicate(child, parent, oldPrev);

                return oldPrev;
            }
        }
        #endregion
    }
}
