﻿using Emgu.CV;
using Emgu.CV.CvEnum;
using Emgu.CV.Structure;
using Emgu.CV.Util;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WordLocRec.Tools
{
    public class ImageProcessingUtils
    {
        #region Smoothing
        public delegate void FilteringFunction(IInputArray input, ref Mat result, double sizeX, double sizeY);
        /// <summary>
        /// Offers in-place filtering. Depending on the filtering function the size param has different meaning. If filteringFunction = FilterGaussianC then sizeX and sizeY are treated as doubles and are the standard deviation of the kernel on each corresponding direction. If filteringFunction = FilterWienerC then sizeX and sizeY are treated as integers and define the filtering kernel size (i.e. 3 * 3)
        /// </summary>
        /// <param name="mImage">The Mat to be transformed</param>
        /// <param name="sizeX">The size of dimension X</param>
        /// <param name="sizeY">The size of dimension Y</param>
        /// <param name="filteringFunction">One of FilterGaussianC and FilterWienerC atm.</param>
        public static void FilterOneLine(ref Mat mImage, double sizeX, double sizeY, FilteringFunction filteringFunction)
        {
            Mat temp = new Mat();
            filteringFunction(mImage, ref temp, sizeX, sizeY);
            mImage = temp.Clone();
            temp.Dispose();
        }
        // C is for clean memory Wise
        public static void FilterGaussianC(IInputArray image, ref Mat result, double sigmaX = 1.414, double sigmaY = 1.414)
        {
            Size size = new Size(0, 0);
            CvInvoke.GaussianBlur(image, result, size, sigmaX, sigmaY, BorderType.Default);
        }
        public static void FilterWienerC(IInputArray input, ref Mat result, double sizeX = 5, double sizeY = 5)
        {
            // input depth supposed to be byte, variemai na grapsw elegxous
            int inputChannels = input.GetInputArray().GetChannels();

            if (inputChannels == 1)
            {
                Wiener1Channel(input, ref result, (int)sizeX, (int)sizeY);
            }
            else if (inputChannels == 3)
            {
                VectorOfMat channels = new VectorOfMat();
                CvInvoke.Split(input, channels);

                Mat[] res = new Mat[3];
                res[0] = new Mat(); res[1] = new Mat(); res[2] = new Mat();
                Wiener1Channel(channels[0], ref res[0], (int)sizeX, (int)sizeY);
                Wiener1Channel(channels[1], ref res[1], (int)sizeX, (int)sizeY);
                Wiener1Channel(channels[2], ref res[2], (int)sizeX, (int)sizeY);
                channels.Dispose();

                VectorOfMat temp = new VectorOfMat(res);
                CvInvoke.Merge(temp, result);
                temp.Dispose();
                for (int i = 0; i < res.Length; i++)
                    res[i].Dispose();
            }

        }
        private static void Wiener1Channel(IInputArray input, ref Mat result, int sizeX, int sizeY)
        {
            Mat src = input.GetInputArray().GetMat();

            int rows = src.Rows;
            int cols = src.Cols;

            Mat srcStub = new Mat(rows, cols, DepthType.Cv32F, 1);
            src.ConvertTo(srcStub, DepthType.Cv32F);

            Mat pKernel = new Mat(sizeY, sizeX, DepthType.Cv32F, 1);

            Mat pTemp1 = new Mat(rows, cols, DepthType.Cv32F, 1), pTemp2 = new Mat(rows, cols, DepthType.Cv32F, 1), pTemp3 = new Mat(rows, cols, DepthType.Cv32F, 1), pTemp4 = new Mat(rows, cols, DepthType.Cv32F, 1);

            Mat noisePower = new Mat(rows, cols, DepthType.Cv32F, 1);

            MCvScalar temp = new MCvScalar(1d / (sizeX * sizeY));
            pKernel.SetTo(temp);

            // local mean of input
            CvInvoke.Filter2D(srcStub, pTemp1, pKernel, new Point(-1, -1));

            // local variance of input
            CvInvoke.Multiply(srcStub, srcStub, pTemp2); // input ^ 2
            CvInvoke.Filter2D(pTemp2, pTemp3, pKernel, new Point(-1, -1));
            pKernel.Dispose();

            // subtract local mean^2 from local variance
            CvInvoke.Multiply(pTemp1, pTemp1, pTemp4); // local mean ^ 2
            CvInvoke.Subtract(pTemp3, pTemp4, pTemp3); // filtered(input^2) - localMean^2 = localVariance

            // estimate noise power (mean of variance)
            noisePower.SetTo(CvInvoke.Mean(pTemp3));

            // result = localMean + ( max(0, localVariance - noise) ./ max(localVariance, noise) ) .* (input - localMean)

            CvInvoke.Subtract(srcStub, pTemp1, result); // input - localMean
            srcStub.Dispose();
            CvInvoke.Max(pTemp3, noisePower, pTemp2); // max(localVariance, noise)

            CvInvoke.Subtract(pTemp3, noisePower, pTemp3); // localVariance - noise
            noisePower.SetTo(new MCvScalar(0));
            CvInvoke.Max(pTemp3, noisePower, pTemp3); // max(0, localVariance - noise)

            CvInvoke.Divide(pTemp3, pTemp2, pTemp3); // max(0, localVariance - noise) ./ max(localVariance, noise)

            CvInvoke.Multiply(pTemp3, result, result);
            CvInvoke.Add(result, pTemp1, result);

            pTemp1.Dispose(); pTemp2.Dispose(); pTemp3.Dispose(); pTemp4.Dispose(); noisePower.Dispose();

            result.ConvertTo(result, DepthType.Cv8U);
        }

        #endregion

        #region Binarization Otsu
        public static void BinarizeOtsuC(IInputArray input, ref Mat result)
        {
            CvInvoke.Threshold(input, result, 0, 255, ThresholdType.Otsu | ThresholdType.Binary);
        }

        #endregion

        #region GammaCorrection
        public static void CorrectionOneLine(ref Mat mImage, double value)
        {
            Mat temp = new Mat();
            Correction(mImage, ref temp, value);
            mImage = temp.Clone();
            temp.Dispose();
        }

        public static void Correction(Mat input, ref Mat result, double correctionValue)
        {
            Mat labIm = new Mat();
            CvInvoke.CvtColor(input, labIm, ColorConversion.Bgr2Lab);
            VectorOfMat vecLab = new VectorOfMat();
            CvInvoke.Split(labIm, vecLab);
            Mat m = vecLab[0];
            m.ConvertTo(m, DepthType.Cv32F);
            CvInvoke.Pow(m, correctionValue, m);
            CvInvoke.Normalize(m, m, 255, 0, NormType.MinMax);
            m.ConvertTo(m, DepthType.Cv8U);
            VectorOfMat labVec2 = new VectorOfMat();
            labVec2.Push(m);
            labVec2.Push(vecLab[1]);
            labVec2.Push(vecLab[2]);

            CvInvoke.Merge(labVec2, result);
            CvInvoke.CvtColor(result, result, ColorConversion.Lab2Bgr);
            labIm.Dispose();
            vecLab.Dispose();
            m.Dispose();
            labVec2.Dispose();
        }
        #endregion

        public static void ApplyPyramidDown(IInputArray input, ref VectorOfMat result, int levels = 1)
        {
            Mat src = input.GetInputArray().GetMat();
            Mat tmpDst = new Mat(src.Height / 2, src.Width / 2, src.Depth, src.NumberOfChannels);
            for (int i = 0; i < levels; i++)
            {
                CvInvoke.PyrDown(src, tmpDst, BorderType.Default);
                result.Push(tmpDst.Clone());
                src = tmpDst.Clone();
                tmpDst = new Mat(src.Height / 2, src.Width / 2, src.Depth, src.NumberOfChannels);
            }
            src.Dispose();
            tmpDst.Dispose();
        }
    }
}
