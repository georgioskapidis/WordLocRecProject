﻿using Emgu.CV;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WordLocRec.Tools
{
    public static class MatExtension
    {
        public static double[] MatToDouble(this Mat src)
        {
            byte[] data = src.GetData();
            double[] converted = new double[src.Rows * src.Cols];
            for (int i = 0; i < converted.Length; i++)
            {
                converted[i] = BitConverter.ToDouble(data, i * 8);
            }
            return converted;
        }
        public static float[] MatToFloat(this Mat src)
        {
            byte[] data = src.GetData();
            float[] converted = new float[src.Rows * src.Cols];
            for (int i = 0; i < converted.Length; i++)
            {
                converted[i] = BitConverter.ToSingle(data, i * 4);
            }
            return converted;
        }
        public static int[] MatToInt32(this Mat src)
        {
            byte[] data = src.GetData();
            int[] converted = new int[src.Rows * src.Cols];
            for (int i = 0; i < converted.Length; i++)
            {
                converted[i] = BitConverter.ToInt32(data, i * 4);
            }
            return converted;
        }
    }
}
