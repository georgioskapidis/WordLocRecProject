﻿using Emgu.CV;
using Emgu.CV.Structure;
using Emgu.CV.Util;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WordLocRec.Tools
{
    public class ZhangSuenThinning
    {
        
        public ZhangSuenThinning()
        {

        }

        public int Skeletonize(IInputArray input, ref Mat result /*, ref VectorOfMat vec */)
        {
            Mat binaryImage = input.GetInputArray().GetMat();

            int steps = 0;
            int rows = binaryImage.Rows;
            int cols = binaryImage.Cols;

            Mat cellGrid = new Mat(rows, cols, Emgu.CV.CvEnum.DepthType.Cv8U, 1); // boolean 0 - 1
            Mat prevCellGrid = new Mat(rows, cols, Emgu.CV.CvEnum.DepthType.Cv8U, 1); // boolean 0 - 1

            Mat temp = new Mat(rows, cols, Emgu.CV.CvEnum.DepthType.Cv8U, 1);
            Mat alphas = new Mat(rows, cols, Emgu.CV.CvEnum.DepthType.Cv8U, 1);
            Mat sums = new Mat(rows, cols, Emgu.CV.CvEnum.DepthType.Cv8U, 1);
            Mat mul1 = new Mat(rows, cols, Emgu.CV.CvEnum.DepthType.Cv8U, 1);
            Mat mul2 = new Mat(rows, cols, Emgu.CV.CvEnum.DepthType.Cv8U, 1);
            Mat state = new Mat(rows, cols, Emgu.CV.CvEnum.DepthType.Cv8U, 1);

            Mat kernel = new Mat(3, 3, Emgu.CV.CvEnum.DepthType.Cv8U, 1);

            Mat lutAlpha = new Mat(256, 1, Emgu.CV.CvEnum.DepthType.Cv8U, 1),
                lutSums = new Mat(256, 1, Emgu.CV.CvEnum.DepthType.Cv8U, 1),
                lut246 = new Mat(256, 1, Emgu.CV.CvEnum.DepthType.Cv8U, 1),
                lut468 = new Mat(256, 1, Emgu.CV.CvEnum.DepthType.Cv8U, 1),
                lut248 = new Mat(256, 1, Emgu.CV.CvEnum.DepthType.Cv8U, 1),
                lut268 = new Mat(256, 1, Emgu.CV.CvEnum.DepthType.Cv8U, 1);

            lutAlpha.SetTo(CreateAlphaLoot());
            lutSums.SetTo(CreateSumLoot());
            lut246.SetTo(CreateLootXXX(0, 2, 4));
            lut468.SetTo(CreateLootXXX(2, 4, 6));
            lut248.SetTo(CreateLootXXX(0, 2, 6));
            lut268.SetTo(CreateLootXXX(0, 4, 6));
        
            InitializeKernel(ref kernel);

            Point anchor = new Point(-1, -1);

            CvInvoke.Threshold(binaryImage, cellGrid, 127, 1, Emgu.CV.CvEnum.ThresholdType.Binary);
            while (!cellGrid.Equals(prevCellGrid))
            {
                ++steps;
                cellGrid.CopyTo(prevCellGrid); // krata tis palies times gia sugkrish

                CvInvoke.Filter2D(cellGrid, temp, kernel, anchor, 0, Emgu.CV.CvEnum.BorderType.Constant);

                CvInvoke.LUT(temp, lutAlpha, alphas);
                CvInvoke.LUT(temp, lutSums, sums);
                CvInvoke.LUT(temp, lut246, mul1); 
                CvInvoke.LUT(temp, lut468, mul2);
              
                CvInvoke.BitwiseAnd(mul1, mul2, temp);
                CvInvoke.BitwiseAnd(temp, sums, temp);
                CvInvoke.BitwiseAnd(temp, alphas, temp);

                CvInvoke.BitwiseNot(temp, state);
                CvInvoke.BitwiseAnd(cellGrid, state, cellGrid);

                //CvInvoke.Threshold(cellGrid, result, 0, 255, Emgu.CV.CvEnum.ThresholdType.Binary);
                //vec.Push(result.Clone());

                // 2nd subiteration           

                CvInvoke.Filter2D(cellGrid, temp, kernel, anchor, 0, Emgu.CV.CvEnum.BorderType.Constant);

                CvInvoke.LUT(temp, lutAlpha, alphas);
                CvInvoke.LUT(temp, lutSums, sums);
                CvInvoke.LUT(temp, lut248, mul1);
                CvInvoke.LUT(temp, lut268, mul2);
                
                CvInvoke.BitwiseAnd(mul1, mul2, temp);
                CvInvoke.BitwiseAnd(temp, sums, temp);
                CvInvoke.BitwiseAnd(temp, alphas, temp);

                CvInvoke.BitwiseNot(temp, state);
                CvInvoke.BitwiseAnd(cellGrid, state, cellGrid);

                //CvInvoke.Threshold(cellGrid, result, 0, 255, Emgu.CV.CvEnum.ThresholdType.Binary);
                //vec.Push(result.Clone());
            }
            CvInvoke.Threshold(cellGrid, result, 0, 255, Emgu.CV.CvEnum.ThresholdType.Binary);

            cellGrid.Dispose(); prevCellGrid.Dispose(); state.Dispose(); temp.Dispose(); alphas.Dispose(); sums.Dispose(); mul1.Dispose(); mul2.Dispose(); lutSums.Dispose(); lutAlpha.Dispose(); lut246.Dispose(); lut248.Dispose(); lut268.Dispose(); lut468.Dispose(); kernel.Dispose(); binaryImage.Dispose();
            return steps;
        }
        #region Deprecated to test and delete
        private static byte[] CreateLoot246()
        {
            byte[] loot = new byte[256];
            for (int i = 0; i < 256; i++)
            {
                loot[i] = 255;
                List<char> arr = IntegerTo8BitBinary(i);

                if (arr[0] == '1' && arr[2] == '1' && arr[4] == '1')
                    loot[i] = 0;           
            }           
            return loot;
        }
        private static byte[] CreateLoot468()
        {
            byte[] loot = new byte[256];
            for (int i = 0; i < 256; i++)
            {
                loot[i] = 255;
                List<char> arr = IntegerTo8BitBinary(i);

                if (arr[2] == '1' && arr[4] == '1' && arr[6] == '1')
                    loot[i] = 0;      
            }
            return loot;
        }
        private static byte[] CreateLoot248()
        {
            byte[] loot = new byte[256];
            for (int i = 0; i < 256; i++)
            {
                loot[i] = 255;
                List<char> arr = IntegerTo8BitBinary(i);

                if (arr[0] == '1' && arr[2] == '1' && arr[6] == '1')
                    loot[i] = 0;      
            }
            return loot;
        }
        private static byte[] CreateLoot268()
        {
            byte[] loot = new byte[256];
            for (int i = 0; i < 256; i++)
            {
                loot[i] = 255;
                List<char> arr = IntegerTo8BitBinary(i);

                if (arr[0] == '1' && arr[4] == '1' && arr[6] == '1')
                    loot[i] = 0;      
            }
            return loot;
        }
        #endregion

        public static byte[] CreateArbitraryLoot(byte val)
        {
            byte[] loot = new byte[256];
            loot[val] = 255;
            return loot;
        }
        private static byte[] CreateLootXXX(int x1, int x2, int x3)
        {
            byte[] loot = new byte[256];
            for (int i = 0; i < 256; i++)
            {
                loot[i] = 255;
                List<char> arr = IntegerTo8BitBinary(i);

                if (arr[x1] == '1' && arr[x2] == '1' && arr[x3] == '1')
                    loot[i] = 0;
            }
            return loot;
        }
        private static List<char> IntegerTo8BitBinary(int i)
        {
            List<char> arr = Convert.ToString(i, 2).ToCharArray().Reverse().ToList();
            while (arr.Count < 8)
                arr.Add('0');
            return arr;
        }
        private static byte[] CreateAlphaLoot()
        {
            byte[] loot = new byte[256];
            int id, idJ;
            for (int i = 0; i < 8; i++)
            {
                id = (int)Math.Pow(2, i);
                loot[id] = 255;
                for (int j = i + 1; j < i + 7; j++)
                {
                    idJ = j % 8;
                    id += (int)Math.Pow(2, idJ);
                    loot[id] = 255;
                }
            }
            return loot;
        }
        private static byte[] CreateSumLoot()
        {
            byte[] sumloot = new byte[256];
            for (int i = 0; i < 256; i++)
            {
                sumloot[i] = EvalInt(Convert.ToString(i, 2).ToCharArray().Count(bit => bit == '1')) ? (byte)255 : (byte)0;
            }
            return sumloot;
        }
        public static bool EvalInt(int n)
        {
            if (n >= 2 && n <= 6)
                return true;
            return false;
        }
        public static void InitializeKernel(ref Mat Pattern)
        {
            byte[] nData = new byte[9];

            nData[0] = 128; nData[1] = 1; nData[2] = 2;
            nData[3] = 64; nData[4] = 0; nData[5] = 4;
            nData[6] = 32; nData[7] = 16; nData[8] = 8;


            Pattern.SetTo(nData);          
        }
    }
}
