﻿using Emgu.CV;
using Emgu.CV.Util;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace WordLocRec
{
    public static class IOUtils
    {
        public static bool CreateSVMInfoFile(String fullpath)
        {
            if (File.Exists(fullpath)) return true;
            else
            {
                try
                {
                    File.AppendAllText(fullpath, SVMInfoFileFormat());
                    return true;
                }
                catch (Exception)
                {
                    return false;
                }
            }
        }
        private static string SVMInfoFileFormat()
        {
            StringBuilder sb = new StringBuilder();
            int columnLength = 20;

            sb.Append(CreateColumn("Name", 2 * columnLength));

            sb.Append(CreateColumn("Feature Type", columnLength));

            sb.Append(CreateColumn("Example Type", columnLength));

            sb.Append(CreateColumn("Training %", columnLength));
            sb.Append(CreateColumn("Test %", columnLength));
            sb.Append(CreateColumn("Support Vectors #", columnLength));
            sb.Append(CreateColumn("C", columnLength));
            sb.Append(CreateColumn("Gamma", columnLength));
            sb.Append(CreateColumn("Iterations", columnLength));
            sb.AppendLine();

            return sb.ToString();
        }
        public static string WordStatFileFormat()
        {
            StringBuilder sb = new StringBuilder();
            int columnLength = 20;
            sb.Append(CreateColumn("FG intensities STD", columnLength, "_"));
            sb.Append(CreateColumn("BG intensities STD", columnLength, "_"));
            sb.Append(CreateColumn("Major Axis CoV", columnLength, "_"));
            sb.Append(CreateColumn("Stroke Widths CoV", columnLength, "_"));
            sb.Append(CreateColumn("Mean Gradient STD", columnLength, "_"));
            sb.Append(CreateColumn("Aspect Ratio CoV", columnLength, "_"));
            sb.Append(CreateColumn("Hu Mom Avg Eucl Dist", columnLength, "_"));
            sb.Append(CreateColumn("ConvexHullCompMean", columnLength, "_"));
            sb.Append(CreateColumn("ConvexHullCompStd", columnLength, "_"));
            sb.Append(CreateColumn("ConvexityDefects CoV", columnLength, "_"));
            sb.Append(CreateColumn("MST Angles Mean", columnLength, "_"));
            sb.Append(CreateColumn("MST Angles STD", columnLength, "_"));
            sb.Append(CreateColumn("MST EdgeWidths CoV", columnLength, "_"));
            sb.Append(CreateColumn("MST EdgeDistances", columnLength, "_"));
            return sb.ToString();
        }
        public static string SimpleWordStatFileFormat()
        {
            StringBuilder sb = new StringBuilder();
            int columnLength = 20;
            sb.Append(CreateColumn("FG int. MCoV", columnLength, "_"));
            sb.Append(CreateColumn("Outer Bound.Int.MCoV", columnLength, "_"));
            sb.Append(CreateColumn("FGBG Lab Dist MCoV", columnLength, "_"));
            sb.Append(CreateColumn("Perim. Gradient MCov", columnLength, "_"));
            sb.Append(CreateColumn("Stroke Width MCoV", columnLength, "_"));
            sb.Append(CreateColumn("ConvexHullCompMean", columnLength, "_"));
            sb.Append(CreateColumn("ConvexHullCompStd", columnLength, "_"));
            sb.Append(CreateColumn("Vert CenterDist.CoV", columnLength, "_"));
            sb.Append(CreateColumn("Horiz CenterDist.CoV", columnLength, "_"));
            return sb.ToString();
        }
        public static string LineWordStatFileFormat()
        {
            StringBuilder sb = new StringBuilder();
            int columnLength = 20;
            sb.Append(CreateColumn("Stroke Width MCoV", columnLength, "_"));
            sb.Append(CreateColumn("FGBG Lab Dist MCoV", columnLength, "_"));
            sb.Append(CreateColumn("ERs Lab Consistency", columnLength, "_"));
            sb.Append(CreateColumn("Max Vertical Error", columnLength, "_"));
            sb.Append(CreateColumn("Width CoV", columnLength, "_"));
            sb.Append(CreateColumn("Horiz. Overlap %", columnLength, "_"));
            sb.Append(CreateColumn("Horiz. Uncovered %", columnLength, "_"));
            sb.Append(CreateColumn("Hu Mom Avg Eucl Dist", columnLength, "_"));
            
            return sb.ToString();
        }
        public static String CreateColumn(string str, int columnLength, String extraCharacter = " ")
        {
            String p = str.Length < columnLength ? str : str.Substring(0, columnLength);
            AddToStringTillLength(ref p, columnLength, extraCharacter);
            p += " *|* ";

            return p;
        }
        public static void AddToStringTillLength(ref string str, int wantedLength, String character = " ")
        {
            int curLength = str.Length;
            if (curLength < wantedLength) for (int i = 0; i < wantedLength - curLength; i++) str += character;
        }

        public static void CreateResultsFile(String fullpath, Rectangle[] wordRects, string tryName)
        {
            StringBuilder data = new StringBuilder();
            for (int i = 0; i < wordRects.Length; i++)
            {
                data.Append(wordRects[i].Left).Append(',')
                    .Append(wordRects[i].Top).Append(',')
                    .Append(wordRects[i].Right).Append(',')
                    .Append(wordRects[i].Bottom).Append(Environment.NewLine);
            }
            String path = "C:\\resultsNM\\" + tryName;
            String name = "res_" + System.IO.Path.GetFileNameWithoutExtension(fullpath);
            String filePath = path + "\\" + name + ".txt";
            new FileInfo(filePath).Directory.Create();
            System.IO.File.WriteAllText(filePath, data.ToString());
        }
        public static void SaveResultsFile(String imagePath, Rectangle[] wordRects, String outputFolder)
        {
            StringBuilder data = new StringBuilder();
            for (int i = 0; i < wordRects.Length; i++)
            {
                data.Append(wordRects[i].Left).Append(',')
                    .Append(wordRects[i].Top).Append(',')
                    .Append(wordRects[i].Right).Append(',')
                    .Append(wordRects[i].Bottom).Append(Environment.NewLine);
            }
            if (!Directory.Exists(Path.GetDirectoryName(outputFolder)))
                Directory.CreateDirectory(Path.GetDirectoryName(outputFolder));
            File.WriteAllText(Path.Combine(outputFolder, Path.GetFileNameWithoutExtension(imagePath) + ".txt"), data.ToString());
        }
        public static void SaveExceptionFile(String imagePath, String message, String outputFolder)
        {
            if (!Directory.Exists(Path.GetDirectoryName(outputFolder)))
                Directory.CreateDirectory(Path.GetDirectoryName(outputFolder));
            File.WriteAllText(Path.Combine(outputFolder, "_" + Path.GetFileNameWithoutExtension(imagePath) + ".txt"), message);
        }

        public static List<Rectangle> LoadFileToRectList(string filename, char lineSeparator = ' ')
        {
            List<Rectangle> mList = new List<Rectangle>();
            string[] lines = System.IO.File.ReadAllLines(filename);
            foreach (string line in lines)
            {
                string[] contents = line.Split(lineSeparator);
                mList.Add(ExactRectFromStringLine(contents));
            }
            return mList;
        }
        public static List<RectangleF> LoadFileToRectListF(string filename, char lineSeparator = ' ')
        {
            List<RectangleF> mList = new List<RectangleF>();
            string[] lines = System.IO.File.ReadAllLines(filename);
            foreach (string line in lines)
            {
                string[] contents = line.Split(lineSeparator);
                mList.Add(RectFromStringLine(contents));
            }
            return mList;
        }
        private static Rectangle ExactRectFromStringLine(String[] lineContents)
        {
            return Rectangle.FromLTRB(
                        int.Parse(lineContents[0]),
                        int.Parse(lineContents[1]),
                        int.Parse(lineContents[2]),
                        int.Parse(lineContents[3]));
        }
        public static RectangleF RectFromStringLine(String[] lineContents)
        {
            return RectangleF.FromLTRB(
                        int.Parse(lineContents[0]) - 0.5f,
                        int.Parse(lineContents[1]) - 0.5f,
                        int.Parse(lineContents[2]) + 0.5f,
                        int.Parse(lineContents[3]) + 0.5f);
        }

        public static void AppendVectorOfByteMatToTrainFile(VectorOfMat vom, String trainfile)
        {
            String[] strs = new String[vom.Size];
            for (int i = 0; i < vom.Size; i++)
            {
                Mat current = vom[i];
                strs[i] = MatToString(current);
            }
            System.IO.File.AppendAllLines(trainfile, strs);
        }
        public static void AppendVectorOfByteMatToFile(VectorOfMat vom, String trainfile, int rows, int cols)
        {
            String[] strs = new String[vom.Size];
            for (int i = 0; i < vom.Size; i++)
            {
                Mat current = vom[i];
                StringBuilder sb = new StringBuilder();
                if (rows < 10)
                    sb.Append('0').Append('0').Append('0').Append(rows.ToString());
                else if (rows < 100)
                    sb.Append('0').Append('0').Append(rows.ToString());
                else if (rows < 1000)
                    sb.Append('0').Append(rows.ToString());
                else
                    sb.Append(rows.ToString());
                if (cols < 10)
                    sb.Append('0').Append('0').Append('0').Append(cols.ToString());
                else if (cols < 100)
                    sb.Append('0').Append('0').Append(cols.ToString());
                else if (cols < 1000)
                    sb.Append('0').Append(cols.ToString());
                else
                    sb.Append(cols.ToString());

                strs[i] = sb.ToString() + MatToStringNotBinary(current);
            }
            System.IO.File.AppendAllLines(trainfile, strs);
        }
        public static void SaveToStringFile(this Mat m, String fullpath)
        {
            int rows = m.Rows;
            int cols = m.Cols;

            StringBuilder sb = new StringBuilder();
            if (rows < 10)
                sb.Append('0').Append('0').Append('0').Append(rows.ToString());
            else if (rows < 100)
                sb.Append('0').Append('0').Append(rows.ToString());
            else if (rows < 1000)
                sb.Append('0').Append(rows.ToString());
            else
                sb.Append(rows.ToString());
            if (cols < 10)
                sb.Append('0').Append('0').Append('0').Append(cols.ToString());
            else if (cols < 100)
                sb.Append('0').Append('0').Append(cols.ToString());
            else if (cols < 1000)
                sb.Append('0').Append(cols.ToString());
            else
                sb.Append(cols.ToString());

            String s = sb.ToString() + MatToStringNotBinary(m);
            if (!Directory.Exists(Path.GetDirectoryName(fullpath)))
                Directory.CreateDirectory(Path.GetDirectoryName(fullpath));
            File.AppendAllLines(fullpath, new String[] { s });
        }
        private static String MatToString(Mat m)
        {
            StringBuilder sb = new StringBuilder();
            Byte[] data = m.GetData();
            foreach (Byte b in data)
                sb.Append(b == 0 ? '0' : '1');
            return sb.ToString();
        }
        private static String MatToStringNotBinary(Mat m)
        {
            StringBuilder sb = new StringBuilder();
            Byte[] data = m.GetData();
            foreach (Byte b in data)
            {
                if (b < 10)
                {
                    sb.Append('0').Append('0').AppendFormat(b.ToString());
                }
                else if (b < 100)
                    sb.Append('0').AppendFormat(b.ToString());
                else
                    sb.Append(b.ToString());
            }
            return sb.ToString();
        }
        private static String[] LoadTrainFile(String trainfile)
        {
            String[] lines = System.IO.File.ReadAllLines(trainfile);

            return lines;
        }

        public static void ToXML<T>(this T t, String fullpath)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(T));
            if (!Directory.Exists(Path.GetDirectoryName(fullpath)))
                Directory.CreateDirectory(Path.GetDirectoryName(fullpath));
            TextWriter textWriter = new StreamWriter(fullpath);

            serializer.Serialize(textWriter, t);
            textWriter.Close();
        }
        public static T FromXml<T>(String fullpath)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(T));
            FileStream fs = new FileStream(fullpath, FileMode.Open, FileAccess.Read, FileShare.Read);
            T t = (T)serializer.Deserialize(fs);
            fs.Close();
            return t;
        }

        public static List<T> LoadMultipleObjectsFromXML<T>(String[] paths)
        {
            List<T> objects = new List<T>();

            for (int i = 0; i < paths.Length; i++)
            {
                T t = FromXml<T>(paths[i]);
                objects.Add(t);
            }
            return objects;
        }
    }
}
