﻿using Emgu.CV;
using Emgu.CV.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WordLocRec.Tools
{
    public static class Disposer
    {
        public static void DisposeMatVectorOfMatRegions(ref Mat mImage, ref VectorOfMat channels, ref List<ERStat>[] regions)
        {
            for (int i = 0; i < regions.Length; i++)
                regions[i].Clear();
            for (int i = 0; i < channels.Size; i++)
            {
                channels[i].Bitmap.Dispose();
                channels[i].Dispose();
            }
            channels.Dispose();
            mImage.Bitmap.Dispose();
            mImage.Dispose();
        }
        public static void DisposeMatVectorOfMatRegions(ref Mat mImage, ref VectorOfMat channels)
        {
            for (int i = 0; i < channels.Size; i++)
            {
                channels[i].Bitmap.Dispose();
                channels[i].Dispose();
            }
            channels.Dispose();
            mImage.Bitmap.Dispose();
            mImage.Dispose();
        }
    }
}
