﻿using Emgu.CV;
using Emgu.CV.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WordLocRec.Tools
{
    class CA
    {
        Boolean[,] image, cellgrid, prevCellgrid, multips1, multips2, state;
        int[,] alphas, sums;
        int rows, cols;

        public CA(bool[,] im)
        {
            image = im;
            cols = image.GetLength(0); //width
            rows = image.GetLength(1); //height
            cellgrid = new bool[cols + 4, rows + 4];
            prevCellgrid = (bool[,])cellgrid.Clone();
            multips1 = (bool[,])cellgrid.Clone();
            multips2 = (bool[,])cellgrid.Clone();
            state = (bool[,])cellgrid.Clone();
            alphas = new int[cols + 4, rows + 4];
            sums = (int[,])alphas.Clone();

            for (int i = 0; i < cols; i++)
            {
                for (int j = 0; j < rows; j++)
                {
                    cellgrid[i + 2, j + 2] = image[i, j];
                }
            }

        }

        public bool[,] Skeletonize(/*ref VectorOfMat gif*/)
        {
            byte[,] tempCell = new byte[cellgrid.GetLength(0), cellgrid.GetLength(1)];
            Mat temp = new Mat();

            int counter = 0;
            while (!isEqual(cellgrid, prevCellgrid))
            {
                counter++;
                prevCellgrid = (bool[,])cellgrid.Clone();
                //1st subiteration
                for (int i = 2; i < cols + 1; i++)
                {
                    for (int j = 2; j < rows + 1; j++)
                    {
                        sums[i, j] = (cellgrid[i - 1, j - 1] ? 1 : 0) + (cellgrid[i - 1, j] ? 1 : 0) + (cellgrid[i - 1, j + 1] ? 1 : 0) +
                                    (cellgrid[i, j + 1] ? 1 : 0) + (cellgrid[i + 1, j + 1] ? 1 : 0) + (cellgrid[i + 1, j] ? 1 : 0) +
                                    (cellgrid[i + 1, j - 1] ? 1 : 0) + (cellgrid[i, j - 1] ? 1 : 0);
                        multips1[i, j] = cellgrid[i - 1, j] && cellgrid[i, j + 1] && cellgrid[i + 1, j]; //2,4,6
                        multips2[i, j] = cellgrid[i, j + 1] && cellgrid[i + 1, j] && cellgrid[i, j - 1]; //4,6,8
                        alphas[i, j] = ((!cellgrid[i - 1, j]) && cellgrid[i - 1, j + 1] ? 1 : 0) +  //2, 3
                            ((!cellgrid[i - 1, j + 1]) && cellgrid[i, j + 1] ? 1 : 0) +               //3, 4
                            ((!cellgrid[i, j + 1]) && cellgrid[i + 1, j + 1] ? 1 : 0) +               //4, 5
                            ((!cellgrid[i + 1, j + 1]) && cellgrid[i + 1, j] ? 1 : 0) +               //5, 6
                            ((!cellgrid[i + 1, j]) && cellgrid[i + 1, j - 1] ? 1 : 0) +               //6, 7
                            ((!cellgrid[i + 1, j - 1]) && cellgrid[i, j - 1] ? 1 : 0) +               //7, 8
                            ((!cellgrid[i, j - 1]) && cellgrid[i - 1, j - 1] ? 1 : 0) +               //8, 9
                            ((!cellgrid[i - 1, j - 1]) && cellgrid[i - 1, j] ? 1 : 0);
                        state[i, j] = (sums[i, j] <= 6) && (sums[i, j] >= 2) && (!multips1[i, j]) && (!multips2[i, j]) && (alphas[i, j] == 1);
                    }
                }
                //1st subiteration add result
                for (int i = 2; i < cols + 1; i++)
                {
                    for (int j = 2; j < rows + 1; j++)
                    {
                        cellgrid[i, j] = false || (cellgrid[i, j] && !state[i, j]);
                        tempCell[i, j] = cellgrid[i, j] ? (byte)255 : (byte)0;
                    }
                }

                //temp.SetTo(tempCell);
                //gif.Push(temp.Clone());

                for (int i = 2; i < cols + 1; i++)
                {
                    for (int j = 2; j < rows + 1; j++)
                    {
                        sums[i, j] = (cellgrid[i - 1, j - 1] ? 1 : 0) + (cellgrid[i - 1, j] ? 1 : 0) + (cellgrid[i - 1, j + 1] ? 1 : 0) +
                                    (cellgrid[i, j + 1] ? 1 : 0) + (cellgrid[i + 1, j + 1] ? 1 : 0) + (cellgrid[i + 1, j] ? 1 : 0) +
                                    (cellgrid[i + 1, j - 1] ? 1 : 0) + (cellgrid[i, j - 1] ? 1 : 0);
                        multips1[i, j] = cellgrid[i - 1, j] && cellgrid[i, j + 1] && cellgrid[i, j - 1];
                        multips2[i, j] = cellgrid[i - 1, j] && cellgrid[i + 1, j] && cellgrid[i, j - 1];
                        alphas[i, j] = ((!cellgrid[i - 1, j]) && cellgrid[i - 1, j + 1] ? 1 : 0) +  //2, 3
                            ((!cellgrid[i - 1, j + 1]) && cellgrid[i, j + 1] ? 1 : 0) +               //3, 4
                            ((!cellgrid[i, j + 1]) && cellgrid[i + 1, j + 1] ? 1 : 0) +               //4, 5
                            ((!cellgrid[i + 1, j + 1]) && cellgrid[i + 1, j] ? 1 : 0) +               //5, 6
                            ((!cellgrid[i + 1, j]) && cellgrid[i + 1, j - 1] ? 1 : 0) +               //6, 7
                            ((!cellgrid[i + 1, j - 1]) && cellgrid[i, j - 1] ? 1 : 0) +               //7, 8
                            ((!cellgrid[i, j - 1]) && cellgrid[i - 1, j - 1] ? 1 : 0) +               //8, 9
                            ((!cellgrid[i - 1, j - 1]) && cellgrid[i - 1, j] ? 1 : 0);
                        state[i, j] = (sums[i, j] <= 6) && (sums[i, j] >= 2) && (!multips1[i, j]) && (!multips2[i, j]) && (alphas[i, j] == 1);

                    }
                }
                for (int i = 2; i < cols + 1; i++)
                {
                    for (int j = 2; j < rows + 1; j++)
                    {
                        cellgrid[i, j] = false || (cellgrid[i, j] && !state[i, j]);
                        tempCell[i, j] = cellgrid[i, j] ? (byte)255 : (byte)0;
                    }
                }

                //temp.SetTo(tempCell);
                //gif.Push(temp.Clone());
            }
            bool[,] result = new bool[cols, rows];
            for (int i = 0; i < cols; i++)
            {
                for (int j = 0; j < rows; j++)
                {
                    result[i, j] = cellgrid[i + 2, j + 2];
                }
            }
            return result;
        }

        private bool isEqual(bool[,] arr1, bool[,] arr2)
        {
            return
                arr1.Rank == arr2.Rank &&
                Enumerable.Range(0, arr1.Rank).All(dimension => arr1.GetLength(dimension) == arr2.GetLength(dimension)) &&
                arr1.Cast<bool>().SequenceEqual(arr2.Cast<bool>());
        }
    }
    
}
