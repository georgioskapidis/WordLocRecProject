﻿using Emgu.CV;
using Emgu.CV.CvEnum;
using Emgu.CV.Structure;
using Emgu.CV.Util;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using WordLocRec.DataStructs;
using WordLocRec.Tools;
using System.IO;
using BumpKit;
using WordLocRec.Forms;
using WordLocRec.Grouping;

namespace WordLocRec
{
    static class Utils
    {
        #region Resize
        private static Image<TColor, TDepth> ResizeTo<TColor, TDepth>(Image<TColor, TDepth> mImage, double scale)
            where TColor : struct, IColor
            where TDepth : new()
        {
            return mImage.Resize(scale, Emgu.CV.CvEnum.Inter.Linear);
        }
        private static Mat ResizeTo(Mat mImage, double scale)
        {
            CvInvoke.Resize(mImage, mImage, new Size((int)(scale * mImage.Width), (int)(scale * mImage.Height)));
            return mImage;
        }
        private static Bitmap Resize2(Bitmap original, double scale)
        {
            Image<Bgr, Byte> temp = new Image<Bgr, Byte>(original);
            return temp.Resize(scale, Emgu.CV.CvEnum.Inter.Linear).Bitmap;
        }
        public static void Resize3(Bitmap original, double scale, out Bitmap result)
        {
            Image<Bgr, byte> orim = new Image<Bgr,byte>(original);
            Image<Bgr, byte> resizedIm = orim.Resize(scale, Inter.Linear);

            result = (Bitmap)resizedIm.Bitmap;
        }
        #endregion

        #region Mat2Image
        private static Image<Bgr, byte> MatToBgrImage(Mat bgrMat)
        {
            Image<Bgr, Byte> im = bgrMat.ToImage<Bgr, Byte>();
            return im;
        }
        private static Image<Gray, byte> MatToGrayImage(Mat mat)
        {
            Image<Gray, byte> im = mat.ToImage<Gray, Byte>();
            return im;
        }
        public static void MatToBgrImageC(Mat input, out Image<Bgr, Byte> result)
        {
            result = input.ToImage<Bgr, byte>();
        }
        public static void MatToGrayImageC(Mat input, out Image<Gray, byte> result)
        {
            result = input.ToImage<Gray, byte>();
        }

        #endregion

        #region Statistics

        public static List<Tuple<float, float>> MeanStdInDataset(List<List<float>> data)
        {
            List<Tuple<float,float>> results = new List<Tuple<float,float>>();

            for (int i = 0; i < data[0].Count; i++)
            {
                float[] colData = new float[data.Count];
                for (int j = 0; j < data.Count; j++)
                {
                    if (i == 7 && data[j][i] < 0)
                    {
                        // avg hu distance < 0 ????
                    }
                    if (float.IsNaN(data[j][i]))
                    {
                        data[j][i] = 0;
                    }
                    colData[j] = data[j][i];
                }
                results.Add(Utils.ComputeMeanStd(colData));
            }
            return results;
        }
        public static List<float>[] MeanStdInDatasetToLists(List<List<float>> data)
        {
            List<float>[] results = new List<float>[2];
            results[0] = new List<float>(); results[1] = new List<float>();

            for (int i = 0; i < data[0].Count; i++)
            {
                float[] colData = new float[data.Count];
                for (int j = 0; j < data.Count; j++)
                {
                    if (i == 7 && data[j][i] < 0)
                    {
                        // avg hu distance < 0 ????
                    }
                    if (float.IsNaN(data[j][i]))
                    {
                        data[j][i] = 0;
                    }
                    colData[j] = data[j][i];
                }
                Tuple<float,float> tt = Utils.ComputeMeanStd(colData);
                results[0].Add(tt.Item1);
                results[1].Add(tt.Item2);
            }
            return results;
        }
        /// <summary>
        /// Inputs a List of lists of data. The outer list has a different parameter on every row. The inner list has the values of the according parameter to find the max and the min from. Results in a list of Tuple-float, float-. The list has as many elements as the number of parameters. The first Item of the tuple is the min and the second item is the max.
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public static List<Tuple<float, float>> MinMaxInDataset(List<List<float>> data)
        {
            List<Tuple<float, float>> results = new List<Tuple<float, float>>(data[0].Count);

            for (int i = 0; i < data[0].Count; i++) // for every feature
            {
                float minValue = float.MaxValue;
                float maxValue = float.MinValue;
                for (int j = 0; j < data.Count; j++)
                {
                    if (data[j][i] < minValue) minValue = data[j][i];
                    else if (data[j][i] > maxValue) maxValue = data[j][i];
                }
                results.Add(new Tuple<float, float>(minValue, maxValue));
            }
            return results;
        }
        public static List<float>[] MinMaxInDatasetToLists(List<List<float>> data)
        {
            List<float>[] results = new List<float>[2];
            results[0] = new List<float>();
            results[1] = new List<float>();

            for (int i = 0; i < data[0].Count; i++) // for every feature
            {
                float minValue = float.MaxValue;
                float maxValue = float.MinValue;
                for (int j = 0; j < data.Count; j++)
                {
                    if (data[j][i] < minValue) minValue = data[j][i];
                    else if (data[j][i] > maxValue) maxValue = data[j][i];
                }
                results[0].Add(minValue);
                results[1].Add(maxValue);
            }
            return results;
        }

        public static Tuple<List<float>, List<float>, List<float>, List<float>> CalcStatistics<T>(String[] filenames) where T : BaseFeature
        { 
            //Tuple 1 : mean, 2 : std, 3 : min, 4 : max
            // for base feature files
            List<List<float>> data = new List<List<float>>();
            for (int i = 0; i < filenames.Length; i++)
            {
                data.Add(IOUtils.FromXml<T>(filenames[i]).ToFloatList());
            }

            List<float>[] minmax = Utils.MinMaxInDatasetToLists(data);
            List<float>[] meanstd = Utils.MeanStdInDatasetToLists(data);

            return Tuple.Create<List<float>, List<float>, List<float>, List<float>>(meanstd[0], meanstd[1], minmax[0], minmax[1]);
        }

        public static float ComputeCoeffOfVariation(float[] values)
        {
            Tuple<float, float> results = ComputeMeanStd(values);
            return results.Item2 / results.Item1;
        }
        public static float ComputeMean(float[] values)
        {
            return ComputeMeanStd(values).Item1;
        }
        public static float ComputeStd(float[] values)
        {
            return ComputeMeanStd(values).Item2;
        }
        public static Tuple<float, float> ComputeMeanStd(float[] values)
        {
            using (Mat m = new Mat(values.Length, 1, Emgu.CV.CvEnum.DepthType.Cv32F, 1))
            {
                m.SetTo(values);
                MCvScalar mean = new MCvScalar(), std = new MCvScalar();
                CvInvoke.MeanStdDev(m, ref mean, ref std);
                return Tuple.Create<float, float>((float)mean.V0, (float)std.V0);
            }
        }

        public static float CalcHMean(float r, float p)
        {
            return (2f * r * p) / (r + p);
        }
        #endregion

        #region HuMoments
        /// <summary>
        /// Calculates the HuMoments of a binary mask using Emgu CV's function for moments and huMoments and packs them in a double[] of length 7.
        /// </summary>
        /// <param name="erMask">The mask of pixels.</param>
        /// <returns>The array of length 7 and type double that contains the hu moments.</returns>
        public static double[] CalcHuMoments(Mat erMask)
        {
            MCvMoments moments = CvInvoke.Moments(erMask, true);
            using (Mat huMoments = new Mat())
            {
                CvInvoke.HuMoments(moments, huMoments);

                double[] humoments = huMoments.MatToDouble();
                //for (int i = 0; i < humoments.Length; i++)
                //{
                //    if (humoments[i] >= 1) humoments[i] = 0.99999;
                //    else if (humoments[i] <= -1) humoments[i] = -0.99999;
                //}
                return humoments;
            }
        }
        public static double[] CalcImprovedHuMoments(Mat erMask)
        {
            double[] hu = CalcHuMoments(erMask);
            double[] improvedHuMoments = new double[6];
            improvedHuMoments[0] = Math.Sqrt(hu[1]) / hu[0];
            improvedHuMoments[1] = hu[1] / (hu[0] * hu[0] + hu[1]);
            improvedHuMoments[2] = Math.Sqrt(hu[3] / hu[2]);
            improvedHuMoments[3] = hu[2] / Math.Pow(hu[0], 3);
            improvedHuMoments[4] = hu[4] / (hu[2] * hu[2]);
            improvedHuMoments[5] = hu[5] / (hu[0] * hu[2]);

            for (int i = 0; i < improvedHuMoments.Length; i++)
            {
                if (improvedHuMoments[i] >= 1) improvedHuMoments[i] = 0.99999;
                else if (improvedHuMoments[i] <= -1) improvedHuMoments[i] = -0.99999;
            }

            return improvedHuMoments;
        }

        // distance functions
        public static double soDistance(double[] momentsA, double[] momentsB)
        {
            double avgDistance = 0d;
            double[] momA = new double[momentsA.Length], momB = new double[momentsB.Length];

            for (int i = 0; i < momentsA.Length; i++)
            {
                momA[i] = -Math.Sign(momentsA[i]) * Math.Log10(Math.Abs(momentsA[i]));
                momB[i] = -Math.Sign(momentsB[i]) * Math.Log10(Math.Abs(momentsB[i]));
            }

            for (int i = 0; i < momentsA.Length; i++)
            {
                //avgDistance += Math.Pow(momA[i] - momB[i], 2);
                avgDistance += Math.Abs(momA[i] - momB[i]);
            }
            avgDistance /= momentsA.Length;
            //avgDistance = Math.Sqrt(avgDistance);

            return avgDistance;
        }
        public static double WeirdDistance(double[] momentsA, double[] momentsB)
        {
            double avgDistance = 0d;

            for (int i = 0; i < momentsA.Length; i++)
            {
                double ama = Math.Abs(momentsA[i]);
                double amb = Math.Abs(momentsB[i]);

                double sma = ama > 0 ? 1 : ama < 0 ? -1 : 0;
                double smb = amb > 0 ? 1 : amb < 0 ? -1 : 0;
                if (ama > 1e-5 && amb > 1e-5)
                {
                    ama = sma / Math.Log10(ama);
                    amb = smb / Math.Log10(amb);

                    avgDistance += Math.Abs(amb - ama);
                }

            }
            return avgDistance / 2;
        }
        // Similarity distance from here: "Application of Improved HU Moments in Object Recognition" Lei Zhang,Fei Xiang, Jiexin Pu and Zhiyong Zhang
        public static double SimilarityDistance(double[] momentsA, double[] momentsB)
        {
            double[] maLog = new double[momentsA.Length], mbLog = new double[momentsB.Length];
            for (int i = 0; i < momentsA.Length; i++)
            {
                if (Math.Abs(momentsA[i]) > 1e-5 && Math.Abs(momentsB[i]) > 1e-5)
                {
                    maLog[i] = Math.Log10(Math.Abs(momentsA[i]));
                    mbLog[i] = Math.Log10(Math.Abs(momentsB[i]));
                }
            }
            double simMeas = SimilarityMeasure(maLog, mbLog);
            return simMeas;
        }

        public static double SimilarityMeasure(double[] momentsA, double[] momentsB)
        {
            double similarity = 0d;
            int counter = 0;
            for (int i = 0; i < momentsA.Length; i++)
            {
                if (momentsB[i] != 0)
                {
                    similarity += EfToyThita(momentsA[i] / momentsB[i]);
                    counter++;
                }
            }

            return Double.IsNaN(similarity / counter) | Double.IsInfinity(similarity / counter) ? 0 : similarity / counter;
        }
        /// <summary>
        /// Fixed the equation to provide boundary in case theta is < -1 (not only for theta > 1) and in case theta == 0.
        /// </summary>
        /// <param name="theta"></param>
        /// <returns></returns>
        public static double EfToyThita(double theta)
        {
            return theta == 0 ? 0 : Math.Abs(theta) < 1 ? theta : 1.0 / theta;
        }
        #endregion

        public static List<T[]> TransformArrayOfListToListArray<T>(List<T>[] arrayOfLists)
        {
            List<T[]> listOfArrays = new List<T[]>();

            for (int i = 0; i < arrayOfLists.Length; i++)
            {
                listOfArrays.Add(arrayOfLists[i].ToArray());
            }
            return listOfArrays;
        }

        /// <summary>
        /// Wrapper class to turn an image to binary. If the input image has more than 1 channel, it is turned to grayscale first.
        /// </summary>
        /// <param name="input">The input image.</param>
        /// <param name="result">The resulting binary image.</param>
        public static void TurnToBinary(IInputArray input, ref Mat result)
        {
            Mat src = input.GetInputArray().GetMat();
            if (src.NumberOfChannels > 1)
            {
                CvInvoke.CvtColor(src, src, Emgu.CV.CvEnum.ColorConversion.Bgr2Gray);
            }
            CvInvoke.Threshold(src, result, 127, 255, ThresholdType.BinaryInv);
        }

        #region Contours
        /// <summary>
        /// Wrapper for OpenCVs FindContourTree. Uses a binary erMask to extract the contours from.
        /// </summary>
        /// <param name="erMask">The mask containing the pixels of an ER</param>
        /// <returns>A VectorOfVectorOfPoint of the contours of the ER. contours[0] has the outermost er. contours[1], contours[2] etc. are possible contours inside the connected component, or null.</returns>
        public static VectorOfVectorOfPoint CalcContours(Mat erMask)
        {
            VectorOfVectorOfPoint contours = new VectorOfVectorOfPoint();
            CvInvoke.FindContourTree(erMask, contours, ChainApproxMethod.ChainApproxSimple);

            return contours;
        }

        public static double CalcContourArea(VectorOfVectorOfPoint contours)
        {
            return CvInvoke.ContourArea(contours[0]);
        }

        public static int GetContourPointsSum(IInputArray input)
        {
            using (Mat src = input.GetInputArray().GetMat())
            {
                using (VectorOfVectorOfPoint contours = new VectorOfVectorOfPoint())
                {
                    int[,] hierar = CvInvoke.FindContourTree(src, contours, ChainApproxMethod.ChainApproxNone);
                    int sumContourPoints = 0;

                    for (int i = 0; i < hierar.GetLength(0); i++)
                    {
                        sumContourPoints += contours[i].Size;
                    }

                    return sumContourPoints;
                }
            }
        }

        #endregion

        #region ConvexHull
        public static VectorOfInt CalcConvexHullPoints(VectorOfVectorOfPoint contours)
        {
            VectorOfInt hullIdx = new VectorOfInt();
            CvInvoke.ConvexHull(contours[0], hullIdx, true, false);
            return hullIdx;
        }

        public static double CalcConvexHullArea(VectorOfVectorOfPoint contours)
        {
            using (VectorOfPoint hull = new VectorOfPoint())
            {
                CvInvoke.ConvexHull(contours[0], hull, true);
                return CvInvoke.ContourArea(hull);
            }
        }

        public static float CalcConvexHullAreaRatio(float hullArea, float contourArea)
        {
            return hullArea / contourArea;
        }

        #endregion

        #region ConvexityDefects

        public static int CalcConvexityDefects(VectorOfVectorOfPoint contours, VectorOfInt hullIdx)
        {
            using (Mat defects = new Mat())
            {
                int convexities = 0;
                if (hullIdx.Size > 2)
                    if (contours[0].Size > 3)
                    {
                        CvInvoke.ConvexityDefects(contours[0], hullIdx, defects);
                        //convexities = (int)defects.Rows;
                    }
                if (defects.Rows > 0)
                    using (Matrix<int> def = new Matrix<int>(defects.Rows, defects.Cols, defects.NumberOfChannels))
                    {
                        defects.CopyTo(def);

                        for (int i = 0; i < def.Height; i++)
                        {
                            int startIdx = def.Data[i, 0];
                            int endIdx = def.Data[i, 1];
                            int farthestPtIdx = def.Data[i, 2];
                            double fixPtDepth = def.Data[i, 3] / 256.0;
                            if (fixPtDepth > 1) convexities++;
                        }
                    }

                return convexities;
            }       
        }
        #endregion
        
        public static int[] FindNonZeroIndices1D(Mat src)
        {
            using (VectorOfPoint res = new VectorOfPoint())
            {
                CvInvoke.FindNonZero(src, res);

                int[] indices = new int[res.Size];

                for (int i = 0; i < res.Size; i++)
                    indices[i] = res[i].Y * src.Cols + res[i].X;

                return indices;
            }
        }

        public static float Standardize(this float rawValue, float meanValue, float stdValue)
        {
            if (stdValue == 0) stdValue = float.Epsilon;
            float standarizedValue = (rawValue - meanValue) / (stdValue);
            if (standarizedValue > 1) standarizedValue = 1;
            else if (standarizedValue < -1) standarizedValue = -1;

            
            return standarizedValue;
        }

        /// <summary>
        /// Uses a part of the original set as a train set and another part as a test set.
        /// <para />
        /// The percentage of the original set has the default value of 20% but it can be defined.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="lines"></param>
        /// <param name="trainSet"></param>
        /// <param name="testSet"></param>
        /// <param name="perc"></param>
        public static void PickPercAtRandom<T>(T[] lines, out T[] trainSet, out T[] testSet, float perc = 0.2f)
        {
            if (perc < 0 || perc > 1) throw new Exception("Olo malakies @ PickPercAtRandom when splitting datasets to train and test set, you used wrong value for percentage of the test set.");
            List<T> all = lines.ToList();
            int swarm = (int)(lines.Length * perc);
            List<T> test = new List<T>(swarm);
            Random r = new Random();
            while (swarm > 0)
            {
                int num = r.Next(all.Count);
                test.Add(all[num]);
                all.RemoveAt(num);
                swarm--;
            }
            trainSet = all.ToArray();
            testSet = test.ToArray();
        }

        public static int CalculateMedian(IInputArray originalImageGrayscale)
        {
            // TODO: change to O(n)

            List<Byte> dataList = originalImageGrayscale.GetInputArray().GetMat().GetData().ToList<Byte>();
            dataList.Sort(compareBytes);
            int count = dataList.Count;
            // count is always an even number so the median is the 
            return (int)(dataList[(count - 1) / 2] + dataList[count / 2]) / 2;
        }
        public static int CalculateMean(IInputArray originalImageGrayscale)
        {
            List<Byte> dataList = originalImageGrayscale.GetInputArray().GetMat().GetData().ToList<byte>();
            int sum = dataList.Sum(num => num);
            double mean = sum / dataList.Count;
            return (int)Math.Round(mean);
        }
        private static int compareBytes(Byte b1, Byte b2)
        {
            return b1.CompareTo(b2);
        }

        /// <summary>
        /// Extract pieces of the input image defined by the provided rectangles.
        /// </summary>
        /// <param name="rects">An array of Rectangle objects</param>
        /// <param name="image">Image to cut from</param>
        /// <returns>A VectorOfMat containing the pieces of the image defined by each rect</returns>
        public static void RoiMatsFromWordRects(Rectangle[] rects, IInputArray image, ref VectorOfMat result)
        {
            foreach (Rectangle rect in rects)
            {
                Mat temp = new Mat(image.GetInputArray().GetMat(), rect);
                result.Push(temp);
                temp.Dispose();
            }
        }

        public static void ByteMatFromBoolValues(bool[,] values, ref Mat result)
        {
            byte[,] data = new byte[values.GetLength(0), values.GetLength(1)];
            for (int i = 0; i < values.GetLength(0); i++)
            {
                for (int j = 0; j < values.GetLength(1); j++)
                {
                    data[i, j] = values[i, j] ? (byte)255 : (byte)0;
                }
            }

            Matrix<byte> temp = new Matrix<byte>(data);
            result.SetTo(temp);
        }
        public static void BinaryMatFromERPixels(List<int> erPixels, Rectangle erRect, IInputArray image, ref Mat result, bool keepOriginalPixelValues = false)
        {
            Mat src = image.GetInputArray().GetMat();

            byte[] wholeImage = new byte[src.Rows * src.Cols];
            byte[] originalValues = src.GetData();
            if (keepOriginalPixelValues)
                foreach (int pixel in erPixels)
                    wholeImage[pixel] = originalValues[pixel];
            else
                foreach (int pixel in erPixels)
                    wholeImage[pixel] = 255;

            Mat temp = new Mat(src.Size, DepthType.Cv8U, 1);
            temp.SetTo(wholeImage);

            Mat temp3 = new Mat(temp, erRect);
            temp3.CopyTo(result);

            temp.Dispose();
            temp3.Dispose();
            src.Dispose();    

        }
        public static void BinaryMatFromERPixels(ERStat er, IInputArray image, ref Mat result, bool keepOriginalPixelValues = false)
        {
            Mat src = image.GetInputArray().GetMat();
            
            byte[] wholeImage = new byte[src.Rows * src.Cols];
            byte[] originalValues = src.GetData();
            if (!keepOriginalPixelValues)
            {
                for (int i = 0; i < er.pixelsInEachThreshold.Count; i++)
                {
                    for (int j = 0; j < er.pixelsInEachThreshold[i].Count; j++)
                    {
                        int pixel = er.pixelsInEachThreshold[i][j];
                        wholeImage[pixel] = 255;
                    }
                }
            }
            else
            {
                for (int i = 0; i < er.pixelsInEachThreshold.Count; i++)
                {
                    for (int j = 0; j < er.pixelsInEachThreshold[i].Count; j++)
                    {
                        int pixel = er.pixelsInEachThreshold[i][j];
                        wholeImage[pixel] = originalValues[pixel];
                    }
                }
            }
            Mat temp = new Mat(src.Size, DepthType.Cv8U, 1);
            temp.SetTo(wholeImage);
         
            Mat temp3 = new Mat(temp, er.rect);
            temp3.CopyTo(result);

            temp.Dispose();
            temp3.Dispose();
            src.Dispose();
            originalValues = null;
            wholeImage = null;
        }
        public static Mat BinaryMat(ERStat er, IInputArray image, bool keepOriginalPixelValues = false)
        {
            using (Mat src = image.GetInputArray().GetMat())
            {
                byte[] wholeImage = new byte[src.Rows * src.Cols];
                byte[] originalValues = src.GetData();
                if (!keepOriginalPixelValues)
                {
                    for (int i = 0; i < er.pixelsInEachThreshold.Count; i++)
                    {
                        for (int j = 0; j < er.pixelsInEachThreshold[i].Count; j++)
                        {
                            int pixel = er.pixelsInEachThreshold[i][j];
                            wholeImage[pixel] = 255;
                        }
                    }
                }
                else
                {
                    for (int i = 0; i < er.pixelsInEachThreshold.Count; i++)
                    {
                        for (int j = 0; j < er.pixelsInEachThreshold[i].Count; j++)
                        {
                            int pixel = er.pixelsInEachThreshold[i][j];
                            wholeImage[pixel] = originalValues[pixel];
                        }
                    }
                }
                using (Mat temp = new Mat(src.Size, DepthType.Cv8U, 1))
                {
                    temp.SetTo(wholeImage);
                    using (Mat temp3 = new Mat(temp, er.rect))
                    {
                        Mat result = new Mat(temp3.Size, DepthType.Cv8U, 1);
                        temp3.CopyTo(result);

                        originalValues = null;
                        wholeImage = null;

                        return result;
                    }
                }
            }
        }

        public static Mat ErMaskFloodFill(IInputArray image, ERStat er, bool keepOriginalPixelValues = false)
        {
            using (Mat src = image.GetInputArray().GetMat())
            {
                if (src.Depth != DepthType.Cv8U) throw new Exception("8-bit, 1 channel image only allowed");
                using (Mat srcMask = new Mat(src.Rows + 2, src.Cols + 2, DepthType.Cv8U, 1))
                using (Mat srcRoi = new Mat(src, er.rect))
                using (Mat tempResult = new Mat(srcMask, new Rectangle(er.rect.X, er.rect.Y, er.rect.Width + 2, er.rect.Height + 2)))
                {
                    tempResult.SetTo(new MCvScalar(0));

                    FloodFillType flags = (FloodFillType)(255 << 8) | FloodFillType.FixedRange | FloodFillType.MaskOnly;
                    Rectangle minCCRect = new Rectangle();

                    CvInvoke.FloodFill(srcRoi, tempResult, new Point(er.pixel % src.Cols - er.rect.X, er.pixel / src.Cols - er.rect.Y), new MCvScalar(255), out minCCRect, new MCvScalar(er.level), new MCvScalar(0), Connectivity.FourConnected, flags);

                    minCCRect.X++; // adjust the mask
                    minCCRect.Y++;

                    Mat result = new Mat(tempResult, minCCRect);
                    if (keepOriginalPixelValues)
                        CvInvoke.BitwiseAnd(srcRoi, result, result);

                    return result;
                }
            }
        }
        public static void GetOuterBoundary(Mat erMask, ref Mat result)
        {
            using (Mat element = CvInvoke.GetStructuringElement(ElementShape.Rectangle, new Size(5, 5), new Point(2, 2)))
            {
                // to boundary isoutai me: dilate(img) - img 
                MCvScalar defBorderValue = CvInvoke.MorphologyDefaultBorderValue;
                CvInvoke.Dilate(erMask, result, element, new Point(-1, -1), 1, BorderType.Constant, defBorderValue);
                CvInvoke.AbsDiff(result, erMask, result);
            }
        }
        public static void GetPerimetricBoundary(Mat erMask, ref Mat result)
        {
            using (Mat element = CvInvoke.GetStructuringElement(ElementShape.Rectangle, new Size(5, 5), new Point(2, 2)))
            using (Mat tmp = new Mat())
            {
                MCvScalar defBorderValue = CvInvoke.MorphologyDefaultBorderValue;
                CvInvoke.Dilate(erMask, tmp, element, new Point(-1, -1), 1, BorderType.Constant, defBorderValue);
                CvInvoke.Erode(erMask, result, element, new Point(-1, -1), 1, BorderType.Constant, defBorderValue);
                CvInvoke.AbsDiff(tmp, result, result);
            }        
        }
        private static byte[] ArrayCopyBigToSmall(byte[] input, int widthInput, Rectangle target)
        {
            byte[] result = new byte[target.Width * target.Height];
            for (int i = 0; i < target.Height; i++)
            {
                Array.Copy(input, (i + target.Y) * widthInput + target.X, result, i * target.Width, target.Width);
            }
            return result;
        }
        private static byte[] ArrayCopySmallToBig(byte[] input, Rectangle origin, Rectangle dest)
        {
            byte[] result = new byte[dest.Width * dest.Height];
            for (int i = 0; i < origin.Height; i++)
            {
                Array.Copy(input, i * origin.Width, result, (i + origin.Y) * dest.Width + origin.X, origin.Width);
            }
            return result;
        }

        public static void MultipleMatsFromERPixels(Mat channel, List<ERStat> regions, ref VectorOfMat erMats, bool keepOriginalPixelValues)
        {
            for (int i = 0; i < regions.Count; i++)
            {
                Mat m = new Mat();
                Utils.BinaryMatFromERPixels(regions[i], channel, ref m, keepOriginalPixelValues);
                erMats.Push(m.Clone());
                m.Dispose();
            }
        }
        public static void MultipleMatsFromERPixelsFloodFill(Mat channel, List<ERStat> regions, ref VectorOfMat erMats, bool keepOriginalPixelValues)
        {
            for (int i = 0; i < regions.Count; i++)
            {
                using (Mat m = Utils.ErMaskFloodFill(channel, regions[i], keepOriginalPixelValues))
                {
                    erMats.Push(m.Clone());
                }
            }
        }

        public static void FFFeatureToMatrixSample(FullFeatureObject ffObj, ref Matrix<float> sample, int row = 0)
        {
            sample[row, 0] = ffObj.nmFeature.aspectRatio;
            sample[row, 1] = ffObj.nmFeature.compactness;
            sample[row, 2] = ffObj.nmFeature.numOfHoles;
            sample[row, 3] = ffObj.nmFeature.medianOfCrossings;
            sample[row, 4] = ffObj.nmFeature.holeAreaRatio;
            sample[row, 5] = ffObj.nmFeature.convexHullRatio;
            sample[row, 6] = ffObj.nmFeature.numInflextionPoints;
            sample[row, 7] = ffObj.dist2MainIntensitiesBB;
            sample[row, 8] = ffObj.percBBPixelsAt2MainIntensities;
            sample[row, 9] = ffObj.erbbIntensitiesRatio;
            sample[row, 10] = ffObj.bbStdDev;
        }

        public static void GetRoiPixels(Mat channels, Mat erMask, ERStat er, ref Mat result)
        {
            using (Mat roi = new Mat(channels, er.rect))
            {
                result = new Mat(erMask.Rows, erMask.Cols, DepthType.Cv8U, roi.NumberOfChannels);
                VectorOfMat vec = new VectorOfMat();
                CvInvoke.Split(roi, vec);
                for (int i = 0; i < roi.NumberOfChannels; i++)
                    CvInvoke.BitwiseAnd(vec[i], erMask, vec[i]);
                CvInvoke.Merge(vec, result);
            }
        }

        //#region floodfill
        //public static int FloodFill(
        //   IInputOutputArray src,
        //   IInputOutputArray mask,
        //   Point seedPoint,
        //   MCvScalar newVal,
        //   out Rectangle rect,
        //   MCvScalar loDiff,
        //   MCvScalar upDiff,
        //   Emgu.CV.CvEnum.Connectivity connectivity = Emgu.CV.CvEnum.Connectivity.FourConnected,
        //   Emgu.CV.CvEnum.FloodFillType flags = Emgu.CV.CvEnum.FloodFillType.Default)
        //{
        //    rect = new Rectangle();
        //    using (InputOutputArray ioaSrc = src.GetInputOutputArray())
        //    using (InputOutputArray ioaMask = mask == null ? InputOutputArray.GetEmpty() : mask.GetInputOutputArray())
        //        return cveFloodFill(
        //           ioaSrc,
        //           ioaMask,
        //           ref seedPoint, ref newVal,
        //           out rect,
        //           ref loDiff, ref upDiff, (int)connectivity | (int)flags);
        //}
        //[DllImport("MyEMGU.dll", CallingConvention = CvInvoke.CvCallingConvention)]
        //public static extern int cveFloodFill(
        //            IntPtr src,
        //            IntPtr mask,
        //            ref Point seedPoint,
        //            ref MCvScalar newVal,
        //            out Rectangle rect,
        //            ref MCvScalar loDiff,
        //            ref MCvScalar upDiff,
        //            int flags);
        //#endregion

        #region CreateChannels
        public static void CreateAnyChannelCombination(ref VectorOfMat finalChannels, IInputArray originalImage, List<ColorChannel> channels, out double[] scales, out bool[] forwardORInverse, bool doScaling)
        {
            List<double> scale = new List<double>();
            List<bool> type = new List<bool>();
            foreach (ColorChannel cc in channels)
            {
                Mat m = new Mat();

                if (cc.Space.Equals(ColorChannel.AvailableColorSpaces.Grayscale))
                    ColorConversionWrapper.ColorConversions(originalImage, ColorConversion.Bgr2Gray, ref m);
                else if (cc.Space.Equals(ColorChannel.AvailableColorSpaces.Distances))
                    ColorConversionWrapper.ColorConversions(originalImage, ColorConversionWrapper.ManualConversions.Bgr2LabTest, ref m);
                else if (cc.Space.Equals(ColorChannel.AvailableColorSpaces.SingleGrad))
                    ColorConversionWrapper.ColorConversions(originalImage, ColorConversionWrapper.ManualConversions.Gradient, ref m);
                else if (cc.Space.Equals(ColorChannel.AvailableColorSpaces.MultiGrad))
                    ColorConversionWrapper.ColorConversions(originalImage, ColorConversionWrapper.ManualConversions.MGradient, ref m);
                else if (cc.Space.Equals(ColorChannel.AvailableColorSpaces.MultiSobelGrad))
                    ColorConversionWrapper.ColorConversions(originalImage, ColorConversionWrapper.ManualConversions.MSobelGradient, ref m);
                else if (cc.Space.Equals(ColorChannel.AvailableColorSpaces.HSV))
                    ColorConversionWrapper.ColorConversions(originalImage, ColorConversion.Bgr2Hsv, ref m);
                else if (cc.Space.Equals(ColorChannel.AvailableColorSpaces.LAB))
                    ColorConversionWrapper.ColorConversions(originalImage, ColorConversion.Bgr2Lab, ref m);
                else if (cc.Space.Equals(ColorChannel.AvailableColorSpaces.XYZ))
                    ColorConversionWrapper.ColorConversions(originalImage, ColorConversion.Bgr2Xyz, ref m);
                else if (cc.Space.Equals(ColorChannel.AvailableColorSpaces.Pii))
                    ColorConversionWrapper.ColorConversions(originalImage, ColorConversionWrapper.ManualConversions.Pii, ref m);
                else // bgr
                    ColorConversionWrapper.ColorConversions(originalImage, ColorConversionWrapper.ManualConversions.Bgr2Bgr, ref m);
             
                VectorOfMat splitChannels = new VectorOfMat();
                CvInvoke.Split(m, splitChannels);
                switch (cc.Kanali)
                {
                    case ColorChannel.Channel.Channel1:
                        m = splitChannels[0];
                        break;
                    case ColorChannel.Channel.Channel2:
                        m = splitChannels[1];
                        break;
                    case ColorChannel.Channel.Channel3:
                        m = splitChannels[2];
                        break;
                }
                if (doScaling && cc.Scale != 1d)
                    CvInvoke.Resize(m, m, new Size((int)(cc.Scale * m.Width), (int)(cc.Scale * m.Height)));

                if (cc.ForwardProjection)
                {
                    finalChannels.Push(m.Clone());
                    scale.Add(cc.Scale);
                    type.Add(true);
                }

                if (cc.InverseProjection)
                {
                    CvInvoke.BitwiseNot(m, m);
                    finalChannels.Push(m.Clone());
                    scale.Add(cc.Scale);
                    type.Add(false);
                }
                m.Dispose();   
            }
            scales = scale.ToArray();
            forwardORInverse = type.ToArray();
        }

        public static void CreateXXChannels(ref VectorOfMat finalChannels, IInputArray originalImage, Constants.ColorSpace flagColorSpace, Constants.VisChannels flagChannels, bool fromRGB, Constants.Gradients flagGradient = Constants.Gradients.Single)
        {
            Mat colorChannels = new Mat();
            switch (flagColorSpace)
            {
                case Constants.ColorSpace.BGR:
                    colorChannels = originalImage.GetInputArray().GetMat().Clone().GetOutputArray().GetMat();
                    break;
                case Constants.ColorSpace.HSV:
                    if (fromRGB)
                        ColorConversionWrapper.ColorConversions(originalImage, ColorConversion.Rgb2Hsv, ref colorChannels);
                    else
                        ColorConversionWrapper.ColorConversions(originalImage, ColorConversion.Bgr2Hsv, ref colorChannels);
                    break;
                case Constants.ColorSpace.LAB:
                    if (fromRGB)
                        ColorConversionWrapper.ColorConversions(originalImage, ColorConversion.Rgb2Lab, ref colorChannels);
                    else
                        ColorConversionWrapper.ColorConversions(originalImage, ColorConversion.Bgr2Lab, ref colorChannels);
                    break;
                case Constants.ColorSpace.XYZ:
                    if (fromRGB)
                        ColorConversionWrapper.ColorConversions(originalImage, ColorConversion.Rgb2Xyz, ref colorChannels);
                    else
                        ColorConversionWrapper.ColorConversions(originalImage, ColorConversion.Bgr2Xyz, ref colorChannels);
                    break;
                case Constants.ColorSpace.Pii:
                    ColorConversionWrapper.ColorConversions(originalImage, ColorConversionWrapper.ManualConversions.Pii, ref colorChannels);
                    break;
                case Constants.ColorSpace.Grayscale:
                    if (fromRGB)
                        ColorConversionWrapper.ColorConversions(originalImage, ColorConversion.Rgb2Gray, ref colorChannels);
                    else
                        ColorConversionWrapper.ColorConversions(originalImage, ColorConversion.Bgr2Gray, ref colorChannels);
                    break;
                case Constants.ColorSpace.Distances:
                        ColorConversionWrapper.ColorConversions(originalImage, ColorConversionWrapper.ManualConversions.Bgr2LabTest, ref colorChannels);
                    break;
                default:
                    break;
                    //nada
            }
            if (colorChannels.NumberOfChannels > 1) // false mono gia Grayscale kai distances
            {
                VectorOfMat channels = new VectorOfMat();
                CvInvoke.Split(colorChannels, channels);
                if (flagChannels.HasFlag(Constants.VisChannels.Channel1))
                {
                    finalChannels.Push(channels[0]);
                    if (flagChannels.HasFlag(Constants.VisChannels.Channel1Opp))
                    {
                        Mat m = new Mat();
                        CvInvoke.BitwiseNot(channels[0], m);
                        finalChannels.Push(m);
                        m.Dispose();
                    }
                }
                if (flagChannels.HasFlag(Constants.VisChannels.Channel2))
                {
                    finalChannels.Push(channels[1]);
                    if (flagChannels.HasFlag(Constants.VisChannels.Channel2Opp))
                    {
                        Mat m = new Mat();
                        CvInvoke.BitwiseNot(channels[1], m);
                        finalChannels.Push(m);
                        m.Dispose();
                    }
                }
                if (flagChannels.HasFlag(Constants.VisChannels.Channel3))
                {
                    finalChannels.Push(channels[2]);
                    if (flagChannels.HasFlag(Constants.VisChannels.Channel3Opp))
                    {
                        Mat m = new Mat();
                        CvInvoke.BitwiseNot(channels[2], m);
                        finalChannels.Push(m);
                        m.Dispose();
                    }
                }
                channels.Dispose();
            }
            else // gia grayscale kai distances
            {
                if (flagColorSpace.Equals(Constants.ColorSpace.Grayscale) || flagColorSpace.Equals(Constants.ColorSpace.Distances))
                {
                    finalChannels.Push(colorChannels);
                    if (flagChannels.HasFlag(Constants.VisChannels.Channel1Opp))
                    {
                        Mat m = new Mat();
                        CvInvoke.BitwiseNot(colorChannels, m);
                        finalChannels.Push(m);
                        m.Dispose();
                    }
                }
            }
            colorChannels.Dispose();
            if (flagChannels.HasFlag(Constants.VisChannels.Channel4))
            {
                Mat grad = new Mat();
                switch (flagGradient)
                {
                    case Constants.Gradients.Single: ColorConversionWrapper.ColorConversions(originalImage, ColorConversionWrapper.ManualConversions.Gradient, ref grad); break;
                    case Constants.Gradients.Multi: ColorConversionWrapper.ColorConversions(originalImage, ColorConversionWrapper.ManualConversions.MGradient, ref grad);
                        ImageProcessingUtils.FilterOneLine(ref grad, 1.414, 1.414, ImageProcessingUtils.FilterGaussianC);
                        break;
                    case Constants.Gradients.MultiSobel: ColorConversionWrapper.ColorConversions(originalImage, ColorConversionWrapper.ManualConversions.MSobelGradient, ref grad);
                        ImageProcessingUtils.FilterOneLine(ref grad, 1.414, 1.414, ImageProcessingUtils.FilterGaussianC);
                        break;
                    default: ColorConversionWrapper.ColorConversions(originalImage, ColorConversionWrapper.ManualConversions.Gradient, ref grad); break;
                }
                //ColorConversionWrapper.ColorConversions(originalImage, ColorConversionWrapper.ManualConversions.Gradient, ref grad);
                finalChannels.Push(grad);
                
                if (flagChannels.HasFlag(Constants.VisChannels.Channel4Opp))
                {
                    Mat m = new Mat();
                    CvInvoke.BitwiseNot(grad, m);
                    finalChannels.Push(m);
                    m.Dispose();
                }
                grad.Dispose();
            }
        }

        public static void CreateTempChannels(ref VectorOfMat finalChannels, IInputArray originalImage)
        {
            //Mat gray = new Mat();
            //ColorConversionWrapper.ColorConversions(originalImage, ColorConversion.Bgr2Gray, ref gray);
            //finalChannels.Push(gray);
            //Mat m = new Mat();
            //CvInvoke.BitwiseNot(gray, m);
            //finalChannels.Push(m);
            //m.Dispose();
            //gray.Dispose();

            Mat grad = new Mat();
            ColorConversionWrapper.ColorConversions(originalImage, ColorConversionWrapper.ManualConversions.Gradient, ref grad);
            finalChannels.Push(grad);
            Mat m = new Mat();
            CvInvoke.BitwiseNot(grad, m);
            finalChannels.Push(m);
            m.Dispose();
            grad.Dispose();
            
            grad = new Mat();
            ColorConversionWrapper.ColorConversions(originalImage, ColorConversionWrapper.ManualConversions.MSobelGradient, ref grad);
            ImageProcessingUtils.FilterOneLine(ref grad, 1.414, 1.414, ImageProcessingUtils.FilterGaussianC);
            finalChannels.Push(grad);
            m = new Mat();
            CvInvoke.BitwiseNot(grad, m);
            finalChannels.Push(m);
            m.Dispose();
            grad.Dispose();
        }

        /// <summary>
        /// Fills the @combinedHueChannels VectorOfMat with the hue channels.
        /// </summary>
        /// <param name="image">BGR image to get channels from. </param>
        /// <param name="combinedHueChannels">Container VectorOfMat to fill</param>
        /// <param name="opposite">True to include opposites, false otherwise</param>
        public static void CombineHueChannels(Mat bgrImage, ref VectorOfMat combinedHueChannels, bool opposite)
        {
            Mat correctHsv = new Mat(), wrongHsv = new Mat();
            ColorConversionWrapper.ColorConversions(bgrImage, ColorConversion.Bgr2Hsv, ref correctHsv);
            VectorOfMat hsvBgr = new VectorOfMat(), hsvRgb = new VectorOfMat();
            CvInvoke.Split(correctHsv, hsvBgr);            
            CvInvoke.Split(wrongHsv, hsvRgb);
            
            combinedHueChannels.Push(hsvBgr[0]);
            combinedHueChannels.Push(hsvRgb[0]);
            if (opposite)
            {
                Mat m = new Mat();
                CvInvoke.BitwiseNot(hsvBgr[0], m);
                combinedHueChannels.Push(m);
                CvInvoke.BitwiseNot(hsvRgb[0], m);
                combinedHueChannels.Push(m);
                m.Dispose();
            }
            correctHsv.Dispose(); wrongHsv.Dispose();
            hsvBgr.Dispose(); hsvRgb.Dispose();
        }

        #endregion

        #region connectedComponents
        public static Rectangle[] FindRegions(ref Image<Gray, Byte> im)
        {
            int width = im.Cols; int height = im.Rows;
            int length = width * height;
            Byte[] data = im.GetInputOutputArray().GetMat().GetData();
            List<Rectangle> rects = new List<Rectangle>();

            CvInvoke.Erode(im, im, CvInvoke.GetStructuringElement(ElementShape.Cross, new Size(3, 3), new Point(-1, -1)), new Point(-1, -1), 1, BorderType.Default, new MCvScalar(0));

            VectorOfVectorOfPoint contours = new VectorOfVectorOfPoint();
            Mat hierarchy = new Mat();
            CvInvoke.FindContours(im.Clone(), contours, hierarchy, RetrType.Tree, ChainApproxMethod.ChainApproxNone);

            int count = contours.Size;
            for (int i = 0; i < count; i++)
            {
                VectorOfPoint contour = contours[i];
                VectorOfPoint approxContour = new VectorOfPoint();
                CvInvoke.ApproxPolyDP(contour, approxContour, CvInvoke.ArcLength(contour, true) * 0.05, true);

                double contourArea = CvInvoke.ContourArea(approxContour);
                Debug.WriteLine("Contour area " + i + ": " + contourArea);

                Rectangle r = CvInvoke.BoundingRectangle(approxContour);

                int rHeight = r.Height;
                int rWidth = r.Width;
                double whRatio = (double)rWidth / (double)rHeight;
                //if (rWidth < width * 0.005 || rHeight < height * 0.004)
                //continue;
                if (rWidth * rHeight <= width * height * 0.0001)
                {
                    for (int y = r.Y; y < r.Bottom; y++)
                    {
                        for (int x = r.X; x < r.Right; x++)
                        {
                            int index = y * width + x;
                            data[index] = 0;
                        }
                    }
                }
                if (whRatio < 1d)
                    rects.Add(r);
            }
            GCHandle h = GCHandle.Alloc(data, GCHandleType.Pinned);
            im = new Image<Gray, byte>(width, height, width, h.AddrOfPinnedObject());
            return rects.ToArray();
        }
        public static void RectConnectedComponent(int[,] binaryArr, out List<ConnComp> connectedComponents){
            int width = binaryArr.GetLength(0); int height = binaryArr.GetLength(1);
            int length = width * height;

            int[,] labels = new int[height, width];
            for (int i = 0; i < height; i++)
                for (int j = 0; j < width; j++)
                    labels[i,j] = 0;
            int totalCCPixels = 0;
            int maxLabel = 0;
            connectedComponents = new List<ConnComp>();
            connectedComponents.Add(new ConnComp(0, Color.Black));
            Random random = new Random();

            for (int y = 0; y < height; y++)
            {
                for (int x = 0; x < width; x++) //assume 8-connected
                {
                    if (binaryArr[y, x] == 0) continue; // do nothing as there is no component part in this pixel

                    int minX = -1; int maxX = 1;
                    if (x == 0) minX = 0;
                    else if (x == width - 1) maxX = 0;

                    if (y > 0)
                    {
                        int y2 = y - 1;
                        for (int x2 = x + minX; x2 <= x + maxX; x2++)
                        {
                            if (labels[y2, x2] == 0) continue;
                            totalCCPixels++;
                            //meta apo edw shmainei oti yparxei geitoniko component poy exei arxikopoihthei
                            if (labels[y, x] == 0)
                            {
                                labels[y, x] = labels[y2, x2]; //dinei thn timh toy geitona
                                connectedComponents.ElementAt(labels[y2, x2]).AddPoint(x, y);
                            }
                            else if (labels[y2, x2] > labels[y, x])
                            {
                                connectedComponents.ElementAt(labels[y, x]).AddPointList(connectedComponents.ElementAt(labels[y2, x2]).GetPointList());
                                connectedComponents.ElementAt(labels[y2, x2]).RemoveAllPoints();
                                setLabelToNeighbours(ref labels, y2, x2, width, height, labels[y2, x2], labels[y, x]);
                            }
                            else if (labels[y2, x2] < labels[y, x])
                            {
                                connectedComponents.ElementAt(labels[y2, x2]).AddPointList(connectedComponents.ElementAt(labels[y, x]).GetPointList());
                                connectedComponents.ElementAt(labels[y, x]).RemoveAllPoints();
                                setLabelToNeighbours(ref labels, y, x, width, height, labels[y, x], labels[y2, x2]);
                            }
                        }
                    }
                    if (x > 0)
                    {
                        if (labels[y, x - 1] != 0) //antistoixo me to continue apla gia thn apo katw grammh tou pinaka
                        {
                            if (labels[y, x] == 0)
                            {
                                labels[y, x] = labels[y, x - 1];
                                connectedComponents.ElementAt(labels[y, x - 1]).AddPoint(x, y);
                            }
                            else if (labels[y, x - 1] > labels[y, x])
                            {
                                connectedComponents.ElementAt(labels[y, x]).AddPointList(connectedComponents.ElementAt(labels[y, x - 1]).GetPointList());
                                connectedComponents.ElementAt(labels[y, x - 1]).RemoveAllPoints();
                                setLabelToNeighbours(ref labels, y, x - 1, width, height, labels[y, x - 1], labels[y, x]);
                            }
                            else if (labels[y, x - 1] < labels[y, x])
                            {
                                connectedComponents.ElementAt(labels[y, x - 1]).AddPointList(connectedComponents.ElementAt(labels[y, x]).GetPointList());
                                connectedComponents.ElementAt(labels[y, x]).RemoveAllPoints();
                                setLabelToNeighbours(ref labels, y, x, width, height, labels[y, x], labels[y, x - 1]);
                            }
                        }
                    }
                    //den exei vrethei geitonas opote prepei na oristei neo component
                    if (labels[y, x] == 0)
                    {
                        labels[y, x] = ++maxLabel;
                        connectedComponents.Add(new ConnComp(maxLabel, GetRandomColor(random)));
                        connectedComponents.ElementAt(connectedComponents.Count - 1).AddPoint(x, y);
                    }
                }
            }
        }

        public static int[,] ConnectedComponentLabeling(Mat binaryIm, out List<ConnComp> connectedComponents, out int totalCCPixels)
        {
            int width = binaryIm.Width; int height = binaryIm.Height;
            int length = width * height;
            // never(!) use the size of the Data array because it has different dimensions,
            // katalathos to ekana kala edw me ta width kai height
            Byte[] data = binaryIm.GetData();
            //Byte[, ,] data = binaryIm.Data;
            int[,] labels = new int[height, width];
            for (int i = 0; i < height; i++)
            {
                for (int j = 0; j < width; j++)
                {
                    labels[i,j] = 0; // initialize the label
                }
            }
            totalCCPixels = 0;
            int maxLabel = 0;
            connectedComponents = new List<ConnComp>();
            connectedComponents.Add(new ConnComp(0, Color.Black)); // just a dummy first component
            Random random = new Random();

            for (int y = 0; y < height; y++)
            {
                for (int x = 0; x < width; x++) //assume 8-connected
                {
                    if (data[y * width + x] == 0) continue; // do nothing as there is no component part in this pixel

                    int minX = -1; int maxX = 1;
                    if (x == 0) minX = 0;
                    else if (x == width - 1) maxX = 0;

                    if (y > 0)
                    {
                        int y2 = y - 1;
                        for (int x2 = x + minX; x2 <= x + maxX; x2++)
                        {
                            if (labels[y2, x2] == 0) continue;
                            totalCCPixels++;
                            //meta apo edw shmainei oti yparxei geitoniko component poy exei arxikopoihthei
                            if (labels[y, x] == 0)
                            {
                                labels[y, x] = labels[y2, x2]; //dinei thn timh toy geitona
                                connectedComponents.ElementAt(labels[y2, x2]).AddPoint(x, y);
                            }
                            else if (labels[y2, x2] > labels[y, x])
                            {
                                connectedComponents.ElementAt(labels[y, x]).AddPointList(connectedComponents.ElementAt(labels[y2, x2]).GetPointList());
                                connectedComponents.ElementAt(labels[y2, x2]).RemoveAllPoints();
                                setLabelToNeighbours(ref labels, y2, x2, width, height, labels[y2, x2], labels[y, x]);
                            }
                            else if (labels[y2, x2] < labels[y, x])
                            {
                                connectedComponents.ElementAt(labels[y2, x2]).AddPointList(connectedComponents.ElementAt(labels[y, x]).GetPointList());
                                connectedComponents.ElementAt(labels[y, x]).RemoveAllPoints();
                                setLabelToNeighbours(ref labels, y, x, width, height, labels[y, x], labels[y2, x2]);
                            }
                        }
                    }
                    if (x > 0)
                    {
                        if (labels[y, x - 1] != 0) //antistoixo me to continue apla gia thn apo katw grammh tou pinaka
                        {
                            if (labels[y, x] == 0)
                            {
                                labels[y, x] = labels[y, x - 1];
                                connectedComponents.ElementAt(labels[y, x - 1]).AddPoint(x, y);
                            }
                            else if (labels[y, x - 1] > labels[y, x])
                            {
                                connectedComponents.ElementAt(labels[y, x]).AddPointList(connectedComponents.ElementAt(labels[y, x - 1]).GetPointList());
                                connectedComponents.ElementAt(labels[y, x - 1]).RemoveAllPoints();
                                setLabelToNeighbours(ref labels, y, x - 1, width, height, labels[y, x - 1], labels[y, x]);
                            }
                            else if (labels[y, x - 1] < labels[y, x])
                            {
                                connectedComponents.ElementAt(labels[y, x - 1]).AddPointList(connectedComponents.ElementAt(labels[y, x]).GetPointList());
                                connectedComponents.ElementAt(labels[y, x]).RemoveAllPoints();
                                setLabelToNeighbours(ref labels, y, x, width, height, labels[y, x], labels[y, x - 1]);
                            }
                        }
                    }
                    //den exei vrethei geitonas opote prepei na oristei neo component
                    if (labels[y, x] == 0)
                    {
                        labels[y, x] = ++maxLabel;
                        connectedComponents.Add(new ConnComp(maxLabel, GetRandomColor(random)));
                        connectedComponents.ElementAt(connectedComponents.Count - 1).AddPoint(x, y);
                    }
                }
            }
            return labels;
        }

        private static void setLabelToNeighbours(ref int[,] labels, int y, int x, int width, int height, int labelValueOld, int labelValueNew)
        {
            if (labels[y, x] == labelValueOld)
            {
                int minX = -1; int maxX = 1;
                if (x == 0) minX = 0;
                else if (x == width - 1) maxX = 0;
                if (y > 0)
                {
                    int yRec = y - 1;
                    for (int xRec = x + minX; xRec <= x + maxX; xRec++)
                    {
                        setLabelToNeighbours(ref labels, yRec, xRec, width, height, labelValueOld, labelValueNew);
                    }
                }
                if (x > 0)
                {
                    setLabelToNeighbours(ref labels, y, x - 1, width, height, labelValueOld, labelValueNew);
                    //setLabelToNeighbours(ref labels, y, x + 1, width, labelValueOld, labelValueNew);
                }
                labels[y, x] = labelValueNew;
                if (x < width - 1)
                {
                    setLabelToNeighbours(ref labels, y, x + 1, width, height, labelValueOld, labelValueNew);
                }
                if (y < height - 1)
                {
                    int yRec = y + 1;
                    for (int xRec = x + minX; xRec <= x + maxX; xRec++)
                    {
                        setLabelToNeighbours(ref labels, yRec, xRec, width, height, labelValueOld, labelValueNew);
                    }
                }                
            }
        }

        private static Color GetRandomColor(Random random)
        {
            return Color.FromArgb(random.Next(0, 255), random.Next(0, 255), random.Next(0, 255));
        }

        public static int CountInflexionPoints(List<Point> points)
        {
            return 0;
        }
        public static bool MoreThanOneCCs(ref int[,] labels, List<Point> points)
        {
            for (int i = 0; i < points.Count; i++)
                labels[points[i].Y, points[i].X] = 0;

            int sum = 0;
            for (int i = 0; i < labels.GetLength(0); i++)
                for (int j = 0; j < labels.GetLength(1); j++)
                    sum = labels[i, j] > 0 ? sum + 1 : sum;

            return sum > 0;
        }
        #endregion
        
        public static Color PickColor(int i)
        {
            Color c = new Color();
            switch (i)
            {
                case 0:
                    c = Color.Blue;
                    break;
                case 1:
                    c = Color.Green;
                    break;
                case 2:
                    c = Color.Red;
                    break;
                case 3:
                    c = Color.Purple;
                    break;
                case 4:
                    c = Color.DarkBlue;
                    break;
                case 5:
                    c = Color.DarkGreen;
                    break;
                case 6:
                    c = Color.DarkRed;
                    break;
                case 7:
                    c = Color.DarkViolet;
                    break;
                default:
                    c = Color.Black;
                    break;
            }
            return c;
        }
        private static void CompareRectangles(List<Rectangle[]> rectsOfT, List<Rectangle[]> rectsOfH)
        {
            Rectangle[] tRects = rectsOfT[1], hRects = rectsOfH[1];
            if (tRects.Length != hRects.Length)
            {
                Debug.WriteLine(" Different number of rectangles!!");
                return;
            }
            int counter = 0;
            for (int i = 0; i < tRects.Length; i++)
            {
                if (tRects[i].Equals(hRects[i]))
                {
                    counter++;
                    Debug.WriteLine(tRects[i] + " is the same as: " + hRects[i] + " " + i);
                }
            }
            Debug.WriteLine("**** " + counter + " equal rectangles ****");
        }

        public static void CreateGifFromVecMat(VectorOfMat vecMat, String name = "skel", int delay = 150)
        {
            String path = "C:\\gifs\\" + name;
            using (var gif = File.OpenWrite(path + ".gif"))
            using (var encoder = new GifEncoder(gif))
            {
                encoder.FrameDelay += TimeSpan.FromMilliseconds(delay);
                for (int i = 0; i < vecMat.Size; i++)
                {
                    String tempPath = path + i + ".png";
                    vecMat[i].Save(tempPath);
                    var image = Image.FromFile(tempPath);
                    encoder.AddFrame(image);
                }
                encoder.Dispose();
            }
        }
    }
}
