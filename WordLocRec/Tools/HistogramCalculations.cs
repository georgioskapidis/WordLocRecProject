﻿using Emgu.CV;
using Emgu.CV.Structure;
using Emgu.CV.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WordLocRec.Tools
{
    public class HistogramCalculations
    {
        private Mat bb, erMask;
        
        public class HistFeatures
        {
            public float distanceIntensityBB;
            public float percOfBBPixelsAt2MainIntensities;
            public float erbbIntensityRatio;
            public float stdDevFullRange;
            public HistFeatures()
            {

            }
            public HistFeatures(float distanceIntensityBB, float percOfBBPixelsAt2MainIntensities, float erbbIntensityRatio, float stdDevFullRange)
            {
                this.distanceIntensityBB = distanceIntensityBB;
                this.percOfBBPixelsAt2MainIntensities = percOfBBPixelsAt2MainIntensities;
                this.erbbIntensityRatio = erbbIntensityRatio;
                this.stdDevFullRange = stdDevFullRange;
            }
        }

        public HistogramCalculations(Mat bb, Mat erMask)
        {
            this.bb = bb.Clone();
            this.erMask = erMask.Clone();
        }

        public HistFeatures calculateHistFeatures()
        {
            HistFeatures hf = new HistFeatures();
            Mat histBB = new Mat(), hist255 = new Mat(), histER = new Mat();
            int[] histSize = new int[] { 10 }; // hist size for hist10 and histER
            float[] ranges = new float[] { 0, 255 };
            VectorOfMat mats = new VectorOfMat();
            mats.Push(bb);
            CvInvoke.CalcHist(mats, new int[] { 0 }, null, histBB, histSize, ranges, false); // histogram of all the pixels in the bb in 10 levels
            CvInvoke.CalcHist(mats, new int[] { 0 }, null, hist255, new int[] { 256 }, ranges, false); // same as before but with 256 levels
            CvInvoke.CalcHist(mats, new int[] { 0 }, erMask, histER, histSize, ranges, false); // histgram of all the pixels in the er in 10 levels

            List<float> dataList = FloatMatToList(histBB); // copy the mat to a list

            float maxValInd = -1, maxValInd2 = -1, maxVal = -1, maxVal2 = -1;
            FindMaxValInd12(dataList, ref maxValInd, ref maxValInd2, ref maxVal, ref maxVal2); // find the top two values
            
            // top 2 intensities distance feature
            hf.distanceIntensityBB = Math.Abs((255 * maxValInd) / 10) - ((255 * maxValInd2) / 10);

            // top 2 intensities pixel percentage
            hf.percOfBBPixelsAt2MainIntensities = (dataList[(int)maxValInd] + dataList[(int)maxValInd2]) / (bb.Rows * bb.Cols);

            // calculate the number of intensities needed to get the 85% of the pixels of the bb
            int intensityCounterBB = intensityCounter(histSize, dataList);
            // same for the 85% of the pixels of the er
            dataList = FloatMatToList(histER);
            int intensityCounterER = intensityCounter(histSize, dataList);

            // er/bb intensity counts ratio
            hf.erbbIntensityRatio = intensityCounterER / (float)intensityCounterBB;

            //stddev of the full histogram range
            MCvScalar mean = new MCvScalar(), stdDev = new MCvScalar();
            CvInvoke.MeanStdDev(hist255, ref mean, ref stdDev);

            hf.stdDevFullRange = (float)stdDev.V0;

            return hf;
        }

        private static int intensityCounter(int[] histSize, List<float> dataList)
        {
            int intensityCounter = dataList.Count;
            float sumPixels85 = dataList.Sum() * 0.85f;
            dataList.Sort();
            float pixelSum = 0;
            while (pixelSum < sumPixels85)
            {
                pixelSum += dataList[--intensityCounter];
            }
            return histSize[0] - intensityCounter;
        }

        private static void FindMaxValInd12(List<float> dataList, ref float maxValInd, ref float maxValInd2, ref float maxVal, ref float maxVal2)
        {
            for (int i = 0; i < dataList.Count; i++)
            {
                if (dataList[i] > maxVal2) // add new large value to 2nd
                {
                    maxVal2 = dataList[i]; maxValInd2 = (float)i;
                    if (maxVal2 > maxVal) // swap first with 2nd if needed
                    {
                        float temp = maxVal;
                        maxVal = maxVal2;
                        maxVal2 = temp;
                        maxValInd2 = maxValInd;
                        maxValInd = (float)i;
                    }
                }
            }
        }

        public List<float> FloatMatToList(Mat input)
        {
            float[] objects = new float[input.Rows];
            input.CopyTo(objects);
            return objects.ToList();
        }
    }
}
